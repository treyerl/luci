Prerequisites:
- JDK 1.8+, "normal" JRE is not enough, (http://www.oracle.com/technetwork/java/javase/downloads/index.html)
- PostGIS installation (http://www.enterprisedb.com/products-services-training/pgdownload; don’t forget the postgis extension after postgres installation)

Setup PostGIS:

* assuming the postgis extension for postgres is installed open up pgAdminIII, the standard postgres management tool
* + ![pgadmin.png](https://bitbucket.org/repo/egg5ab/images/3851361554-pgadmin.png)
* create a new login role in postgres
* ![create database user](https://bitbucket.org/repo/egg5ab/images/3907483651-Screen%20Shot%202015-01-23%20at%2018.31.50.png)
* this will be the username through which Luci establishes any connections to the database; password is set under "Definition"
* +  ![new role](https://bitbucket.org/repo/egg5ab/images/2090206706-Screen%20Shot%202015-01-23%20at%2018.32.02.png)
* create a new database
* + ![create a new database](https://bitbucket.org/repo/egg5ab/images/1494831152-Screen%20Shot%202015-01-23%20at%2018.32.11.png)
* the name you use here must appear in luci's config file (as well as db username as password)
* + ![database name](https://bitbucket.org/repo/egg5ab/images/3708391573-Screen%20Shot%202015-01-23%20at%2018.32.20.png)
* add postgis extensions
* + ![Extensions](https://bitbucket.org/repo/egg5ab/images/1273942133-Screen%20Shot%202015-01-23%20at%2018.32.38.png)
* postgis
* + ![postgis](https://bitbucket.org/repo/egg5ab/images/542496448-Screen%20Shot%202015-01-23%20at%2018.32.54.png)
* postgis topology
* + ![postgis_topology](https://bitbucket.org/repo/egg5ab/images/3466810898-Screen%20Shot%202015-01-23%20at%2018.33.17.png)
* postgis tablefunc

Installation:

* From the Download section download the zipped binaries, unzip and move them to your Application/Programs folder
* Double clicking the app should open Luci as a Service application only visible in the System-Tray.
* if you encounter any database errors, you might need to edit the config file:
* ![config file](https://bitbucket.org/repo/egg5ab/images/2948077212-Screen%20Shot%202015-01-26%20at%2017.51.14.png)
* when you run Luci with an empty database you also have to create a first Luci user:
* ![client console](https://bitbucket.org/repo/egg5ab/images/3336557173-Screen%20Shot%202015-01-26%20at%2012.06.25.png)
* here is an example how you would set up a Luci user through it's integrated console:
* ![setup user](https://bitbucket.org/repo/egg5ab/images/667059085-Screen%20Shot%202015-01-26%20at%2012.05.11.png)
* for more documentation on how to call json actions from within the console refer to the specification which is being created live / depending on the loaded actions
* ![print spec](https://bitbucket.org/repo/egg5ab/images/1652683178-Screen%20Shot%202015-01-26%20at%2017.55.55.png)


Create Eclipse Project:

* in Terminal/Console navigate to the folder where you want Luci’s project folder to be created and copy paste ‚git clone https://yourusername@bitbucket.org/treyerl/luci.git' or use Eclipse’s git plugin (or download the repo as a zip etc.)
* In Eclipse create a new Java project with File-> New -> Java Project
* in the dialog uncheck „Use default location“ if you didn’t download or copy luci to your workspace and navigate to the luci folder using „Browse“.
* Eclipse will create the src folders automatically for you: luci-src, plugins-src, client-console-src
* make sure the default output folder is set to "luci/bin" (should be default)
* press "Finish"

All libraries should be included when cloning the git repository; no installation of mvn or other java build tools necessary.
Reference to relink native osx (and linux) dylibs in the mosquitto binaries: https://blogs.oracle.com/dipol/entry/dynamic_libraries_rpath_and_mac



