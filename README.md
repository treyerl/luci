# IMPORTANT: 
## This repository is considered as deprecated. Refer to Luci's refactored version instead: [Luci2](https://bitbucket.org/treyerl/luci2)

# LUCI: Lightweight Urban Computation Interchange #
### A middleware (network server) to plugin (urban) analysis and simulation tools to your geometry.###

Luci consists of a geometry (GIS) repository and the necessary tools to orchestrate urban simulations
like radiation analyses, micro climate / CFD simulations etc. With its extendable plugin architecture the available 
simulations can be configured dynamically: simluations can be registered over the network using a simple json syntax or 
as java plugins typically consisting of a Java file. With its focus on simplicity it aims to integrate as 
many design tools as possible in a simple fashion. Explained with a computer graphics metaphor:
Luci is like a network render server for simulation-models depending on geometry. The geometry, all its attributes and
the simulation inputs have a timestamp parameter that allows for simple versioning.

* Version 1.0

##Featured Plugins##
* Isovist calculation
* simple HeatMap calculation
* economic potentials calculation
* to be extended!!


## Features ##

* publish/subscribe event system based on MQTT (grown famous with / developed for "the internet of things")
* TCP: messages are json-strings delimited by newline characters; possible to send also binary data if specified in the json message (read as json is only a header)
* HTTP: under development 
* client connectors in C#, Java, Python
* connected CAD / 3D applications: Blender

## INSTALLATION##
Prerequisites:

* JDK 1.8+, "normal" JRE is not enough, (http://www.oracle.com/technetwork/java/javase/downloads/index.html);
* on linux at the moment this translates to: 

```
sudo add-apt-repository ppa:webupd8team/java
sudo apt-get update
sudo apt-get install oracle-java8-installer


```
* PostGIS installation (http://www.enterprisedb.com/products-services-training/pgdownload; don’t forget the postgis extension after postgres installation)
* on Windows a Visual Studio 2010 Redistributable is needed to run mosquitto ([google if link is broken](http://www.microsoft.com/en-us/download/details.aspx?id=5555))



for installation see [INSTALL.md](https://bitbucket.org/treyerl/luci/src/8b28ecab1ef4dd3631e29a4e9c658f02eb445d39/INSTALL.md?at=master)


##First Steps##

As a reference open the specification PDF as shown below.

* ![spec.png](https://bitbucket.org/repo/egg5ab/images/3085403673-spec.png)

Developers who want to create Luci plugin for their urban planning and/or CAD app can test the example snippets with the console. The console as well as the examples can be opened with the system tray menu as shown below.

* ![cons.png](https://bitbucket.org/repo/egg5ab/images/2838024506-cons.png)

In the clients folder there are some example clients some of which are able to register as a service: 

* platform independent: there is a somoclu python client which uses an existing somoclu installation via command line
* C#/windows: there are visual studio projects for a heat emission and an isovist service.

All of these services are available through Luci once registered. In the console (as described at the top, when starting up the console) you can list the services or the available action with "service list", or "action list". 

## Use Luci Client Libraries to create plugins for your code##
* python: in the clients/python/ folder you can find 'LuciConnect.py' which you can use as a library for either clients and/or remote services. Refer to the 'somoclu' and 'ecoPot' examples as a reference.
* C#: in the clients/C# folder there are 4 examples: HeatEmission, LineService, Isovist, CollabSample all of which can be downloaded for Windows from the download section.
* Java: The simple command line console described above is written in Java using two classes as a "library" (atm the old one is still linked, since the new one is not tested through out platforms yet). Very similar in design to the python classes you can use two classes 'LuciConnect' and 'LuciRemoteService' either as a delegate or as a parent of your class. Since it they share only very few code with the luci server application, there is also a standalone version available in the download section. To run the console using the standalone distribute, unpack the zip file, navigate to the folder in a terminal/console and run `java -jar console.jar -classpath console_lib/* org.luci.console.ClientConsole` (with Luci server running).

## ToDo ##

* 1.0:
* DONE: add test files to repo and make TestConsole work with relative paths
* DONE: call console from the Luci menu
* DONE: osx: make mosquitto work with all libraries it needs (otool, install_name_tool)
* DONE: version: macs with no xcode tools always ask to install git, once git is called for the version; find a better solution for version number
* DONE: ClassLoader: compile folders first and load them alltogether as a package (remaining TODO: define package, but classes can import others)
* DONE: MQTT field in service instance table so that services can be chained (and not only their inputs)
* DONE(not with CountDownLatch, but 3 HashMaps --> get Review): add a CountDownLatch with the count of MQTT dependent inputs that waits for all inputs to be notified through mqtt
* 1.1:
* web interface and web connections through websockets
* + DONE: refactor actions: JSON Objects as output, socket.println() is being moved to TCPClientHandlers
* + DONE: action don't implement Callable(JSONObject) anymore but are executed directly in the ClientHandler's thread
* + DONE: LastAction is being replaced with interface ISwitchClientHandler{ ClientHandler switch(ClientHandler);}
* + add HTTPClientHandler (and jetty) checking all json objects for streaminfos and replacing them with URLs
* 1.2
* ping/progress visitor
* console wrapper service 
* + php: aria air pollution service
* + DONE: R: statistics
* + DONE: somoclu
* make unit converters available as services (converters)
* run converters as calls (like services with supervisor, i.e. mqtt events used for starting the conversion and publish notification upon its end)
* 1.3:
* implement Matrix transformation attribute
* node editor of Blender or web library to visualize service chaining
* org.postgis.ewkb to let java and postgres communicate in binary mode
* DXF converter
* add prj support to shp converter
* Unity viewer
* replace json with jackson lib
* H2GIS support


## Clients / Libraries ##
* Java: console (text only) client bundled with Luci (src/clients), [Processing library](https://bitbucket.org/treyerl/luci-p5-processing)
* Python: library bundled with Luci (src/clients), [Blender Addon](https://bitbucket.org/treyerl/luci-blenderluci)
* C#: examples bundled with Luci (src/clients)

## Why a middleware? ##

![l1.png](https://bitbucket.org/repo/egg5ab/images/413372964-l1.png)
![l3.png](https://bitbucket.org/repo/egg5ab/images/432641486-l3.png)
![l4.png](https://bitbucket.org/repo/egg5ab/images/244786615-l4.png)