__author__ = 'treyerl'

import os
from subprocess import check_output, call

dir = os.path.dirname( os.path.realpath( __file__ ) )
allfiles = set([])
missingfiles = set([])

def print_check(dirpath, file, pre):
    if "." in file:
        if not file.endswith(".dylib"):
            return
    abspath = dirpath+"/"+file
    response = check_output(["otool", "-L", abspath], universal_newlines=True)
    call(["chmod", "755", abspath])
    i = 0
    for line in response.splitlines():
        if i == 0:
            i += 1
            continue
        line = line.split(" ")[0].replace("\t", "")
        name = line.split("/")[-1]

        #check general - uncomment when running the first time
        print(line)

        #check missing files - uncomment when running the second time
        #copy over missing files manually
        #redo this until you have no missing files anymore

        # if name not in allfiles:
        #     print("MISSING:" +line)


        # relink - final run(s)

        if "@executable_path" not in line:
            if name != file:
                command = ["install_name_tool", "-change", line, line.replace("/usr/lib", "@rpath"), abspath]
                command = ["install_name_tool", "-change", line, line.replace("@loader_path", "@executable_path"), abspath]
                call(command)
                print(command)
            else:
                command = ["install_name_tool", "-id", line.replace("/usr/lib", "@rpath"), abspath]
                command = ["install_name_tool", "-id", line.replace("@loader_path", "@executable_path"), abspath]
                call(command)
                print(command)

def load_files(dir):
    for file in os.listdir(dir):
        if file.startswith("."):
            continue
        elif "." in file:
            if not file.endswith(".dylib"):
                continue
        abspath = os.path.join(dir, file)
        if os.path.isfile(abspath):
            allfiles.add(file)
        else:
            load_files(abspath)

def do_folder(dir, r):
    for file in os.listdir(dir):
        abspath = os.path.join(dir, file)
        if os.path.isfile(abspath):
            print_check(dir, file, "  "*r)
        else:
            if not file.startswith("."):
                do_folder(abspath, r+1)

load_files(dir)
print(allfiles)
do_folder(dir, 0)