﻿COPY (SELECT row_to_json(featcoll)
   FROM 
    (SELECT 'FeatureCollection' As type, array_to_json(array_agg(feat)) As features
     FROM (SELECT 'Feature' As type,
        ST_AsGeoJSON(tbl.geom)::json As geometry
  FROM lc_geometry As tbL   
   WHERE scid = 2   
 )  As feat 
)  As featcoll) TO '/Users/treyerl/Desktop/LuciClientConsole/scid2.json';