package actions;

import java.util.List;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;
import org.luci.User;
import org.luci.comm.action.ISwitchClientHandler;
import org.luci.comm.clienthandlers.AuthenticatedClientHandler;
import org.luci.comm.clienthandlers.ClientHandler;
import org.luci.connect.LcInputStream;
import org.luci.connect.LcJSONObject;
import org.luci.connect.LcMetaJSONObject;
import org.luci.connect.LcOutputStream;
import org.luci.factory.observer.FactoryObserver;

import actions.common.GetUser;

public class authenticate extends GetUser implements ISwitchClientHandler {
	private User user;
	public authenticate(FactoryObserver o, ClientHandler h, LcJSONObject json) {
		super(o, h, json);
	}
	
	@Override
	public LcMetaJSONObject getInputIndex() {
		String inputjson = ""
				+ "{	'action':'"+this.getClass().getSimpleName()+"',"
				+ "		'username':'string',"
				+ "		'userpasswd':'string',"
				+ "		'subject':'Users'"
				+ "}";
		return new LcMetaJSONObject(inputjson);
	}
	
	@Override
	public JSONObject perform(List<LcInputStream> inputStreams, List<LcOutputStream> outputStreams, 
			Set<String> unknownKeys) throws Exception{
		JSONArray errors = new JSONArray();
		user = authenticate(request, errors);
		if (user != null){
			return new JSONObject().put("result", "User '"+request.getString("username")+"' authenticated!");
		} else {
			return new JSONObject().put("error", errors.get(0));
		}
	}

	@Override
	public LcMetaJSONObject getOutputIndex() {
		String outputjson = ""
				+ "	{'XOR result': 'string',"
				+ "	 'XOR error': ['json','list','string']"
				+ "}";
		return new LcMetaJSONObject(outputjson);
	}

	@Override
	public boolean shouldSwitch() {
		return user != null;
	}

	@Override
	public ClientHandler switchClientHandler() {
		return new AuthenticatedClientHandler(h, o, user);
	}
}
