package actions;

import java.util.List;
import java.util.Set;

import org.json.JSONObject;
import org.luci.comm.action.Action;
import org.luci.comm.clienthandlers.ClientHandler;
import org.luci.connect.LcInputStream;
import org.luci.connect.LcJSONObject;
import org.luci.connect.LcMetaJSONObject;
import org.luci.connect.LcOutputStream;
import org.luci.factory.ServiceFactoryThread;
import org.luci.factory.observer.FactoryObserver;

public class cancel extends Action {

	public cancel(FactoryObserver o, ClientHandler h, LcJSONObject json) {
		super(o, h, json);
	}

	@Override
	public LcMetaJSONObject getInputIndex() {
		String inputjson = ""
				+ "{	'action':'"+this.getClass().getSimpleName()+"',"
				+ "		'SObjID':'number',"
				+ "		'input_hash':'number',"
				+ "		'subject':'Service Instance (SObjID)'"
				+ "}";
		return new LcMetaJSONObject(inputjson);
	}

	@Override
	public JSONObject perform(List<LcInputStream> inputStreams, List<LcOutputStream> outputStreams, 
			Set<String> unknownKeys) throws Exception{
		long SObjID = request.getLong("SObjID");
		long input_hash = request.getLong("input_hash");
		for (String s: o.getServiceSet()){
			ServiceFactoryThread sf = o.getServiceFactory(s);
			if (sf.cancelExecution(SObjID, input_hash)) {
				return new JSONObject().put("result",SObjID+" successfully canceled.");
			}
		}
		return new JSONObject().put("error",SObjID+" not running.'}");
	}

	@Override
	public LcMetaJSONObject getOutputIndex() {
		String outputjson = ""
				+ "	{'XOR result': 'string',"
				+ "	 'XOR error': ['json','list','string']"
				+ "}";
		return new LcMetaJSONObject(outputjson);
	}

}
