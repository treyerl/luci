package actions.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.luci.comm.clienthandlers.ClientHandler;
import org.luci.connect.DirectStreamWriter;
import org.luci.connect.LcInputStream;
import org.luci.connect.LcJSONObject;
import org.luci.connect.LcOutputStream;
import org.luci.connect.LcString;
import org.luci.connect.StreamInfo;
import org.luci.converters.geometry.GeometrySelector;
import org.luci.converters.geometry.GeometryWriter;
import org.luci.converters.geometry.IGeoJSONWriter;
import org.luci.factory.observer.FactoryObserver;
import org.luci.utilities.Projection;
import org.luci.utilities.TAB;

public abstract class GetScenario extends GetService{
	
	public GetScenario(FactoryObserver o, ClientHandler h, LcJSONObject json){
		super(o, h, json);
		
	}
	
	public String getScenarioName() throws SQLException{
		String t = TAB.getTableName(TAB.SCENARIOS);
		String sql = "SELECT name FROM "+t+" AS scns WHERE id = ? "+trs.getSQL();
		PreparedStatement ps = con.prepareStatement(sql);
		ps.setLong(1, ScID);
		trs.setTimestampSQL(ps, 2);
		ResultSet rs = ps.executeQuery();
		String name = null;
		if (rs.next()) name = rs.getString(1);
		else throw new SQLException("No scenario found with ID "+ScID);
		return name;
	}
	
	public JSONObject getScenarioProjection() 
			throws InstantiationException, FileNotFoundException, SQLException {
		Projection noProj = new Projection();
		IGeoJSONWriter g = o.getGeoJSON_FromDb(noProj);
		String t = TAB.getTableName(TAB.SCENARIOS);
		JSONArray center = null;
		JSONArray bbox = null;
		String crs = null;
		JSONObject result = new JSONObject();
		/**Correlated sub-query SQL string using 'outer' and 'innerSQL' aliases for the same table */
		String sql = "SELECT bbox, center, crs, id FROM "+t+" AS scns WHERE id = ? "+trs.getSQL();
		PreparedStatement ps = con.prepareStatement(sql);
		ps.setLong(1, ScID);
		trs.setTimestampSQL(ps, 2);
		ResultSet rs = ps.executeQuery();
		if (rs.next()){
			JSONObject b = g.geometryFromDb(rs.getObject(1));
			if (b != null) bbox = b.getJSONArray("coordinates");
			JSONObject c = g.geometryFromDb(rs.getObject(2));
			if (c != null) center = c.getJSONArray("coordinates");
			crs = rs.getString(3);
		}
		if (bbox != null) result.put("bbox", bbox);
		if (center != null) result.put("center", center);
		if (crs != null) if (!crs.equals("")) result.put("crs", crs);
		return result;
	}
	
	public JSONObject getScenarioInfo() throws Exception{
		Projection noProj = new Projection();
		IGeoJSONWriter g = o.getGeoJSON_FromDb(noProj);
//		GetService s = new GetService(o);
		JSONObject scn = new JSONObject();
		String t = TAB.getTableName(TAB.SCENARIOS);
		/**Correlated sub-query SQL string using 'outer' and 'innerSQL' aliases for the same table */
		String sql = "SELECT name, timestamp, bbox, center FROM "+t+" AS scns WHERE id = ? "+trs.getSQL();
		PreparedStatement ps = con.prepareStatement(sql);
		ps.setLong(1, ScID);
		trs.setTimestampSQL(ps, 2);
		ResultSet rs = ps.executeQuery();
		if(rs.next()){
			scn.put("name", rs.getString(1));
			scn.put("ScID", ScID);
			scn.put("timestamp", rs.getTimestamp(2).getTime());
			JSONObject bbox = g.geometryFromDb(rs.getObject(3));
			if (bbox != null) scn.put("bbox", bbox.getJSONArray("coordinates"));
			JSONObject center = g.geometryFromDb(rs.getObject(4));
			if (bbox != null) scn.put("center", center.getJSONArray("coordinates"));
						
		}
		scn.put("service_instances", getServiceInstanceInfoList(ScID));
		scn.put("service_factories", getServiceFactoryInfoList(ScID));
		return scn;
	}
		
	public int resolveScenarioName(String name) throws SQLException{
		String sql = "SELECT COUNT(id), id, array_agg(id) FROM "
					 + TAB.getTableName(TAB.SCENARIOS)+" WHERE name = ? group by id";
		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, name);
		ResultSet rs = ps.executeQuery();
		if (rs.next()){
			long count = rs.getLong(1);
			if (count == 1){
				return rs.getInt(2);
			} else if (count > 1){
				JSONArray scids = new JSONArray(rs.getArray(3));
				throw new SQLException(name+" is not a unique scenario name. Found ScID's: "+scids.toString());
			}
		}
		throw new SQLException("Scenario '"+name+"' does not exist.");
	}

	public String getServiceScenarioName(long SObjID) {	
		Connection con = null;
		try{
			con = this.o.getDatabaseConnection();
			String t = TAB.getTableName(TAB.SCENARIOS);
			String s = TAB.getTableName(TAB.SERVICES);
			String sql = "SELECT sc.scenarioname FROM "+t+" AS sc LEFT OUTER JOIN "
					+ s + " AS s ON (sc.scid = s.scid) WHERE s.ID = ?;";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setLong(1, SObjID);
			ResultSet res = ps.executeQuery();
			res.next();
			con.close();
			return res.getString(1);
		} catch (SQLException e){
			try {
				if (con != null) con.close();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			this.log.error("Error while reading scenarioname: "+e.getMessage());
		}
		
		return null;
	}
	
	public List<String> getScenarioNameList() throws SQLException{
		try{
			String sql = "SELECT name FROM "+TAB.getTableName(TAB.SCENARIOS) + " GROUP BY name";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			List<String> scs = new ArrayList<String>();
			while(rs.next()){
				scs.add(rs.getString(1));
			}
			return scs;
		} catch (SQLException e){
			con.close();
			throw e;
		}
	}
	
	public List<Long> getScenarioIDList() throws SQLException{
		try {
			String sql = "SELECT id FROM "+TAB.getTableName(TAB.SCENARIOS) + " GROUP BY id";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			List<Long> scs = new ArrayList<Long>();
			while(rs.next()){
				scs.add(rs.getLong(1));
			}
			return scs;
		} catch (SQLException e){
			con.close();
			throw e;
		}
	}
	
	public JSONObject getScenarioList() throws SQLException{
		try {
			String sql = "SELECT id, name FROM "+TAB.getTableName(TAB.SCENARIOS) + " GROUP BY id, name";
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			JSONObject scs = new JSONObject();
			while(rs.next()){
				scs.put(rs.getString(2), rs.getLong(1));
			}
			return scs;
		} catch (SQLException e){
			con.close();
			throw e;
		}
	}
	
	public JSONObject getDetailsOfScenario(long ScID){
		// TODO: getDetailsOfScenario
		return new JSONObject();
	}
	
	public LcJSONObject getScenarioGeometry(List<LcOutputStream>  outputStreams) 
			throws SQLException, InstantiationException, FileNotFoundException, 
			InterruptedException, ExecutionException {
		String scnName = getScenarioName();
		LcJSONObject result = new LcJSONObject();
		result.put("ScID", ScID);
		result.put("timerange", trs.getTimerangeJSON());
		
		String format = "GeoJSON";
		boolean geojsonfile = false;
		Projection pc = new Projection();
		if (request.has("format_request")){
			format = request.getString("format_request");
			if (format.equalsIgnoreCase("geojson")) geojsonfile = true;
		}
		boolean geojsonNoFile = format.equalsIgnoreCase("geojson") && !geojsonfile;
		boolean withAttachments = request.optBoolean("withAttachments", true);
		
		JSONObject proj = getScenarioProjection();
		if (request.has("crs")){
			if (proj.has("center")){
				JSONArray jC = proj.getJSONArray("center");
				double[] c = new double[]{jC.getDouble(0), jC.getDouble(1)};
				pc = new Projection(c, request.getString("crs"));
			} else if (proj.has("crs"))
				pc = new Projection(proj.getString("crs"), request.getString("crs"));
		} else {
			if (proj.has("center")){
				JSONArray jC = proj.getJSONArray("center");
				double[] c = new double[]{jC.getDouble(0), jC.getDouble(1)};
				pc = new Projection(c);
			} else if (proj.has("crs"))
				pc = new Projection(proj.getString("crs"));
		}
		
		if (request.has("dimensions")){
			pc.setDim(request.getInt("dimensions"));
		}
		
		if (request.optBoolean("switchLatLon", false)) pc.switchLatLon();
		
		ExecutorService pool = o.getThreadPool();
		JSONArray errors = new JSONArray(); // TODO: needs to be moved to CallSupervisor once converters are being treated as calls
		
		int start = outputStreams.size();
		int end = 0;
		
		// GeometrySelector
		int batchID = request.optInt("batchID", 0);
		String layer = request.optString("layer", null);
		JSONArray longs = request.optJSONArray("geomIDs");
		long[] geomIDs = new long[0];
		if (longs != null){
			geomIDs = new long[longs.length()];
			for (int i = 0; i < longs.length(); i++){
				geomIDs[i] = longs.getLong(i);
			}
		}
		JSONObject attConstraints = request.optJSONObject("attributeConstraints");
		Map<String, String> attributes = null;
		if (attConstraints != null){
			attributes = new HashMap<String, String>();
			Set<String> keys = attConstraints.keySet();
			for (String key: keys){
				attributes.put(key, attConstraints.getString(key));
			}
		}
		JSONArray os = request.optJSONArray("orderBy");
		String[] orderBy = null;
		if (os != null){
			int numOs = os.length();
			orderBy = new String[numOs];
			for (int i = 0; i < numOs; i++){
				orderBy[i] = os.getString(i);
			}
		}
		int offset = request.optInt("offset", 0);
		int limit = request.optInt("limit", 0);
		boolean inclHistory = request.optBoolean("history");
		String version = request.optString("version", null);
		GeometrySelector gSel = new GeometrySelector(o, ScID, batchID, layer, geomIDs, attributes, 
				orderBy, offset, limit, trs, inclHistory, version);
		
		GeometryWriter gc = o.getGeometryWriter(format, gSel, pc, outputStreams, errors);
		JSONObject geometry = pool.submit(gc).get();
		JSONArray attachments = new JSONArray();
		
		// add a writer to all inner attachments like images attached as attributes to geometry
		// or remove them if they are explicitly excluded from the request
		for(int i = start; i < outputStreams.size(); ){
			LcOutputStream los = outputStreams.get(i);
			if (!los.hasWriter() && withAttachments){
				String filename = los.getChecksum()+"."+los.format;
				File f = new File(org.luci.Luci.getDataRoot(), filename);
				FileInputStream fis = new FileInputStream(f);
				LcInputStream lis = new LcInputStream(los, fis);
				lis.setReady();
				o.getThreadPool().submit(new DirectStreamWriter(lis, los, true, false, false, false));
				attachments.put(los.getJSON());
				i++;
			} else if (!los.hasWriter() && !withAttachments) {
				outputStreams.remove(i);
			}
		}
//		
//		if (!withAttachments){
//			while(outputStreams.size() > start){
//				outputStreams.remove(start);
//			}
//		} else {
//			for(int i = start; i < outputStreams.size(); i++){
//				LcOutputStream los = outputStreams.get(i);
//				if (!los.hasWriter() && withAttachments){
//					String filename = los.getChecksum()+"."+los.format;
//					File f = new File(org.luci.Luci.getDataRoot(), filename);
//					FileInputStream fis = new FileInputStream(f);
//					LcInputStream lis = new LcInputStream(los, fis);
//					lis.setReady();
//					o.getThreadPool().submit(new DirectStreamWriter(lis, los, true, false, false, false));
//					attachments.put(los.getJSON());
//				}
//			}
//		}
		if (geojsonfile){
			try {
				@SuppressWarnings("unchecked")
				Set<String> ks = geometry.keySet();
				String key = ks.toArray(new String[1])[0];
				JSONObject geojson = geometry.getJSONObject(key);
				ByteBuffer bb = ByteBuffer.wrap(geojson.getJSONObject("geometry").toString(4).getBytes("UTF-8"));
				String csum = LcString.calcChecksum(bb);
				int i = outputStreams.size() + 1;
				StreamInfo si = new StreamInfo(csum, "json", scnName, i, bb.capacity(), pc.getEPSG(), null);
				DirectStreamWriter sw = DirectStreamWriter.create(si, bb);
				LcOutputStream out = sw.getLcOutputStream();
				pool.submit(sw);
				outputStreams.add(out);
				geometry.put(out.getName(), out.getJSON());
				geometry.remove(key);
				end = 1;
			} catch (UnsupportedEncodingException | JSONException e) {
				e.printStackTrace();
			}
		}
		if (geometry != null){
			// if we send the geometry as jsonfile (where the streaminfos are hidden
			// from the receiving json parser) and not as a json object in the header we need to add
			// attachments additional to 
			if (!geojsonNoFile && attachments.length() > 0){
				geometry.put("attachments", attachments);
			}
			result.put("geometry", geometry);
			if (proj.has("crs")) result.put("crs", proj.getString("crs"));
			if (proj.has("bbox")) result.put("bbox", proj.getJSONArray("bbox"));
			return result;
		} else {
			result.put("warnings", new JSONArray().put("Could not get scenario geometry for '"
					+ScID+"'! Using '"+format+"' converter. "));
			return result;
		}
	}
	
	public JSONObject getScenarioGeoReference(long scid) throws Exception{
		Connection con = this.o.getDatabaseConnection();
		try {
			Projection noProj = new Projection();
			IGeoJSONWriter g = o.getGeoJSON_FromDb(noProj);
			String t = TAB.getTableName(TAB.SCENARIOS);
			String sql = "SELECT center FROM "+t+" WHERE id = ?";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setLong(1, scid);
			ResultSet rs = ps.executeQuery();
			JSONObject p = null;
			if (rs.next()){
				p = g.geometryFromDb(rs.getObject(1));
			}
			con.close();
			return p;
		} catch (SQLException e){
			con.close();
			throw e;
		}
	}
	
	public boolean hasGeometry() throws SQLException{
		Connection con = this.o.getDatabaseConnection();
		String t = TAB.getScenarioTable(ScID, TAB.SCN);
		try{
			String sql = "SELECT count(geomID) > 1 FROM "+t;
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setLong(1, ScID);
			ResultSet rs = ps.executeQuery();
			if (rs.next()){
				return rs.getBoolean(1);
			}
			con.close();
			return false;
		} catch (Exception e){
			con.close();
			return false;
		}
	}
}
