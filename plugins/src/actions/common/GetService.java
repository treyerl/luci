package actions.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;

import org.json.JSONArray;
import org.json.JSONObject;
import org.luci.Luci;
import org.luci.comm.action.Action;
import org.luci.comm.action.IServiceAction;
import org.luci.comm.clienthandlers.ClientHandler;
import org.luci.connect.LcJSONObject;
import org.luci.factory.RemoteServiceFactoryThread;
import org.luci.factory.ServiceFactoryThread;
import org.luci.factory.observer.FactoryObserver;
import org.luci.utilities.TAB;
import org.luci.utilities.TimeRangeSelection;

public abstract class GetService extends Action implements IServiceAction{
	protected long SObjID;
	protected int ScID;
	protected long flag;
	protected TimeRangeSelection trs;
	protected JSONObject formatRequest;
	protected Connection con;
	protected int iteration;
	protected ExecutorService pool = this.o.getThreadPool();
	
	public GetService(FactoryObserver o, ClientHandler h, LcJSONObject json) {
		super(o, h, json);
		trs = TimeRangeSelection.NULL();
	}

	public String getServiceStatus(long SObjID) throws SQLException{
		Connection con = this.o.getDatabaseConnection();
		String sql = "SELECT status FROM "+TAB.getTableName(TAB.SERVICES)+" WHERE id = ?";
		PreparedStatement ps = con.prepareStatement(sql);
		ps.setLong(1, SObjID);
		ResultSet rs = ps.executeQuery();
		if(rs.next()){
			return rs.getString(1);
		}
		con.close();
		return null;
	}
		
	public long getServiceScID(long SObjID) throws SQLException {
		//TKey stfk = new TKey(Luci.SERVICE, Luci.BASE);
		if (SObjID > 0) {
			Connection con = this.o.getDatabaseConnection();
			String sql = "select scid from "+TAB.getTableName(TAB.SERVICES)+" where id = ?;";
			PreparedStatement ps = null;
			try{
				ps = con.prepareStatement(sql);
				ps.setLong(1, SObjID);
				ResultSet res = ps.executeQuery();
				res.next();
				con.close();
				return res.getLong(1);
			} catch (Exception e){
				sql = (ps != null) ? ps.toString(): "";
				this.log.error("Error while reading scenarioname: "+e.getMessage()+"; "+sql);
			}
			con.close();
		}
		return 0;
	}
	
	/**checks whether a SObjID exists
	 * @param SObjID
	 * @return
	 * @throws SQLException
	 */
	public boolean has(long SObjID) throws SQLException {
		this.SObjID = SObjID;
		return getServiceName() != null;
	}
	
	@Override
	public IServiceAction setCon(Connection con) {
		this.con = con;
		return this;
	}

	/* (non-Javadoc)
	 * @see org.luci.comm.action.ISetAction#setFlag(long)
	 */
	@Override
	public IServiceAction setFlag(long flag) {
		this.flag = flag;
		return this;
	}

	/* (non-Javadoc)
	 * @see org.luci.comm.action.ISetAction#setSObjID(long)
	 */
	@Override
	public IServiceAction setSObjID(long SObjID) {
		this.SObjID = SObjID;
		return this;
	}

	/* (non-Javadoc)
	 * @see org.luci.comm.action.ISetAction#setScID(long)
	 */
	@Override
	public IServiceAction setScID(int ScID) {
		this.ScID = ScID;
		return this;
	}

	/* (non-Javadoc)
	 * @see org.luci.comm.action.ISetAction#setTimeRangeSelection(org.luci.utilities.TimeRangeSelection)
	 */
	@Override
	public IServiceAction setTimeRangeSelection(TimeRangeSelection trs) {
		this.trs = trs;
		return this;
	}
	
	
	public String getServiceName() throws SQLException {
		String t = TAB.getTableName(TAB.SERVICES);
		String sql = "SELECT name FROM "+t+" AS s WHERE id = ? "+trs.getSQL();
		PreparedStatement ps = con.prepareStatement(sql);
		ps.setLong(1, SObjID);
		ResultSet rs = ps.executeQuery();
		String name = null;
		if (rs.next()){
			name = rs.getString(1);
		}
		return name;
	}	

	public List<Long> getServiceInstanceList(String ServiceName) throws SQLException{
		List<Long> list = new ArrayList<Long>();
		String t = TAB.getTableName(TAB.SERVICES);
		String sql = "SELECT id FROM "+t+" AS services WHERE name = ? AND BITAND(flag,?) = 0 " + trs.getSQL();
		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, ServiceName);
		ps.setLong(2, TAB.flagDELETED);
		ResultSet rs = ps.executeQuery();
		while(rs.next()){
			list.add(rs.getLong(1));
		}
		return list;
	}
	
	public Map<Long, Long> getServiceInstanceMap(String ServiceName) throws SQLException{
		Map<Long, Long> list = new HashMap<Long, Long>();
		String t = TAB.getTableName(TAB.SERVICES);
		String sql = "SELECT id, scid FROM "+t+" AS services WHERE name = ? AND BITAND(flag,?) = 0 " + trs.getSQL();
		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, ServiceName);
		ps.setLong(2, TAB.flagDELETED);
		ResultSet rs = ps.executeQuery();
		while(rs.next()){
			list.put(rs.getLong(1), rs.getLong(2));
		}
		return list;
	}
	
	public List<Long> getServiceInstanceList(String ServiceName, String status) throws SQLException {
		String statusSQL = " AND status = ? ";
		if (status == null) statusSQL = "";
		
		List<Long> list = new ArrayList<Long>();
		String sql = "SELECT id FROM "+TAB.getTableName(TAB.SERVICES)+" AS inst WHERE name = ? "
				+ "AND (SELECT count(sobjid) FROM "
				+ TAB.getTableName(TAB.SERVICECALLS)+" WHERE sobjid = inst.id) > 0 " + statusSQL;
		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, ServiceName);
		if (status != null) ps.setString(2, status);
		ResultSet rs = ps.executeQuery();
		while(rs.next()){
			list.add(rs.getLong(1));
		}
		return list;
	}
	
	public List<Long> getServiceInstanceList(long ScID) throws SQLException{
		List<Long> list = new ArrayList<Long>();
		String sql = "SELECT id FROM "+TAB.getTableName(TAB.SERVICES)+" WHERE scid = ?";
		PreparedStatement ps = con.prepareStatement(sql);
		ps.setLong(1, ScID);
		ResultSet rs = ps.executeQuery();
		while(rs.next()){
			list.add(rs.getLong(1));
		}
		return list;
	}
	
	public LcJSONObject getServiceInstanceInfo() throws SQLException{
		return getServiceInstanceInfo(false);
	}
	
	public LcJSONObject getServiceInstanceInfo(boolean extended) throws SQLException{
		LcJSONObject result = new LcJSONObject();
		
		String topic = "";
		String topic_pre = Luci.getConfig().getProperty("mqtt_pre", "/Luci/");
		String t = TAB.getTableName(TAB.SERVICES);
		PreparedStatement ps = null;
		try {
			String sql = "SELECT name, status, flag, timestamp, scid, uid, service_subscriptions FROM "+t
					+" AS s WHERE id = ? "+trs.getSQL();
			ps = con.prepareStatement(sql);
			ps.setLong(1, SObjID);
			
			trs.setTimestampSQL(ps, 2);
			ResultSet rs = ps.executeQuery();
			if(rs.next()){
				long scid = rs.getLong(5);
				topic = topic_pre+scid+"/"+SObjID+"/";
				String classname = rs.getString(1);
				String status = rs.getString(2);
				long flag = rs.getLong(3);
				if ((flag & TAB.flagDELETED) == 1){
					throw new SQLException("Service '"+classname+"(ID:"+SObjID+")' has been deleted.");
				}
				String subscriptions = rs.getString(7);
				result.put("classname", classname);
				result.put("status", status);
				result.put("timestamp", rs.getTime(4).getTime());
				result.put("mqtt_topic", topic);
				result.put("ScID", scid);
				result.put("uID", rs.getLong(6));
				if (subscriptions != null) result.put("subscriptions", new JSONArray(subscriptions));
				
				ServiceFactoryThread f = this.o.getServiceFactory(classname);
				if (extended && f != null) {
					result.put("inputIndex", f.getInputIndex());
					result.put("outputIndex", f.getOutputIndex());
				}
				return result;
				
			} else {
				throw new SQLException("No such service "+SObjID);
			}
		} catch (Exception e) {
			con.close();
			e.printStackTrace();
			if (ps != null) this.log.debug(ps.toString());
			throw e;
		}
	}
	
	public JSONObject getServiceFactoryInfo(String classname){
		ServiceFactoryThread sf = this.o.getServiceFactory(classname);
		if (sf != null) {
			JSONObject service = new JSONObject();
			service.put("tasks", new JSONArray(sf.getTasksList()));
			service.put("pending", new JSONArray(sf.getRunningCalls()));
			service.put("inputs", sf.getInputIndex());
			service.put("outputs", sf.getOutputIndex());
			if (RemoteServiceFactoryThread.class.isAssignableFrom(sf.getClass())){
				RemoteServiceFactoryThread rsf = (RemoteServiceFactoryThread) sf;
				int[] i = rsf.getNodeInfo();
				int all = i[0]+i[1];
				service.put("status", "run by "+all+" nodes; "+i[1]+" busy, "+i[0]+" idle.");
				JSONArray providers = new JSONArray(rsf.getProviders());
				service.put("providers", providers);
			}
			return service;
		}
		return new JSONObject().put("status", "unavailable");
	}
	
	public JSONArray getServiceInstanceInfoList(long ScID) throws Exception{
		JSONArray services = new JSONArray();
		List<Long> serviceIDs = this.getServiceInstanceList(ScID);
		for (Long id: serviceIDs){
			SObjID = id;
			services.put(getServiceInstanceInfo());
		}
		return services;
	}
	
	public String[] getServiceFactoryList(long ScID) throws Exception{
		HashSet<String> classes = new HashSet<String>();
		List<Long> serviceIDs = this.getServiceInstanceList(ScID);
		for (Long id: serviceIDs){
			SObjID = id;
			classes.add(getServiceInstanceInfo().getString("classname"));
		}
		return classes.toArray(new String[classes.size()]);
	}
	
	public JSONArray getServiceFactoryInfoList(long ScID) throws Exception{
		JSONArray infoList = new JSONArray();
		String[] factories = getServiceFactoryList(ScID);
		for (String factory: factories){
			infoList.put(this.getServiceFactoryInfo(factory));
		}
		return infoList;
	}	
}
