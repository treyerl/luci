package actions.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.json.JSONObject;
import org.luci.Luci;
import org.luci.comm.clienthandlers.ClientHandler;
import org.luci.connect.DirectStreamWriter;
import org.luci.connect.LcInputStream;
import org.luci.connect.LcJSONObject;
import org.luci.connect.LcOutputStream;
import org.luci.connect.StreamInfo;
import org.luci.converters.SQL2JSON;
import org.luci.converters.geometry.IGeoJSONWriter;
import org.luci.factory.observer.FactoryObserver;
import org.luci.utilities.Projection;
import org.luci.utilities.TAB;

public abstract class GetServiceInputs extends GetService {
	
	public GetServiceInputs(FactoryObserver o, ClientHandler h, LcJSONObject json) {
		super(o, h, json);
	}
	
	
	/** checks if the input of a SObjID exists
	 * @param SObjID
	 * @param keyname
	 * @return
	 * @throws SQLException
	 * @throws ExecutionException 
	 * @throws FileNotFoundException 
	 * @throws InstantiationException 
	 */
	public boolean has(String keyname) 
			throws SQLException, ExecutionException, FileNotFoundException, InstantiationException {
		return getServiceInput(keyname, null, null) != null;
	}
	
	public Object getServiceInput(String keyname, List<LcInputStream> inputStreams, 
			List<LcOutputStream> outputStreams) 
			throws SQLException, FileNotFoundException, InstantiationException {
		IGeoJSONWriter g = o.getGeoJSON_FromDb(Projection.NULL());
		String t = TAB.getTableName(TAB.SERVICEINPUTS);
		String sql = "SELECT format, text, number, checksum, geom FROM "+t+" AS si WHERE sobjid = ? "
				+ "AND keyname = ? "+trs.getSQL();
		PreparedStatement ps = con.prepareStatement(sql);
		ps.setLong(1, SObjID);
		ps.setString(2, keyname);
		trs.setTimestampSQL(ps, 3);
		ResultSet rs = ps.executeQuery();

		while (rs.next()){
			String checksum = rs.getString("checksum");
			String format = rs.getString("format");
			String text = rs.getString("text");
			BigDecimal number = rs.getBigDecimal("number");
			Object geom = rs.getObject("geom");
			StreamInfo si = null;
			if (checksum != null){
				
				if (outputStreams != null){
					int order = outputStreams.size() + 1;
					File file = new File(Luci.getDataRoot(), checksum+"."+format);
					si = new StreamInfo(checksum, format, keyname, order, (int) file.length());
					DirectStreamWriter sw = DirectStreamWriter.create(si, file);
					LcOutputStream o = sw.getLcOutputStream();
					pool.submit(sw);
					outputStreams.add(o);
				} else if (inputStreams != null){
					int order = inputStreams.size() + 1;
					File file = new File(Luci.getDataRoot(), checksum+"."+format);
					si = new StreamInfo(checksum, format, keyname, order, (int) file.length());
					LcInputStream i = new LcInputStream(si, new FileInputStream(file));
					si = i;
					inputStreams.add(i);
				}
				
			}
			Object j = SQL2JSON.toJSONValue(format, text, number, si, g.geometryFromDb(geom));
			return j;
		}
		return null;
	}
	
	public LcJSONObject getServiceInputs(boolean getSubscription, 
			List<LcInputStream> inputStreams, List<LcOutputStream> outputStreams) 
			throws SQLException, FileNotFoundException, InstantiationException {

		Projection pc = new Projection();
		IGeoJSONWriter g = o.getGeoJSON_FromDb(pc);
		LcJSONObject inputs = new LcJSONObject();
		short tt = TAB.SERVICEINPUTS;
		String t = TAB.getTableName(tt);
		
		String sql = "SELECT keyname, format, text, number, checksum, geom, output_subscription FROM "
					+ t + " AS s WHERE sobjid = ? "
					+ trs.getSQL()+";";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setLong(1, SObjID);
		trs.setTimestampSQL(ps, 2);
		ResultSet rs = ps.executeQuery();
		
		while (rs.next()){
			String keyname = rs.getString(1);
			String format = rs.getString(2);
			String text = rs.getString(3);
			BigDecimal number = rs.getBigDecimal(4);
			String checksum = rs.getString(5);
			JSONObject geometry = g.geometryFromDb(rs.getObject(6));
			String output_subscription = rs.getString(7);
			StreamInfo si = null;
			if (checksum != null){
				if (outputStreams != null) {
					int order = outputStreams.size() + 1;
					File file = new File(Luci.getDataRoot(), checksum+"."+format);
					si = new StreamInfo(checksum, format, keyname, order, (int) file.length());
					DirectStreamWriter sw = DirectStreamWriter.create(si, file);
					LcOutputStream o = sw.getLcOutputStream();
					pool.submit(sw);
					outputStreams.add(o);
				} else if (inputStreams != null){
					int order = inputStreams.size() + 1;
					File file = new File(Luci.getDataRoot(), checksum+"."+format);
					si = new StreamInfo(checksum, format, keyname, order, (int) file.length());
					LcInputStream i = new LcInputStream(si, new FileInputStream(file));
					si = i;
					inputStreams.add(i);
				}
			}
			
			if (output_subscription != null && getSubscription) inputs.put(keyname, output_subscription);
			else inputs.put(keyname, SQL2JSON.toJSONValue(format, text, number, si, geometry));
		}
		return inputs;	
	}
}
