package actions.common;

import java.io.File;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.json.JSONException;
import org.json.JSONObject;
import org.luci.Luci;
import org.luci.comm.action.ISetServiceOutputs;
import org.luci.comm.clienthandlers.ClientHandler;
import org.luci.connect.DirectStreamWriter;
import org.luci.connect.LcJSONObject;
import org.luci.connect.LcOutputStream;
import org.luci.connect.StreamInfo;
import org.luci.connect.StreamWriter;
import org.luci.converters.SQL2JSON;
import org.luci.factory.observer.FactoryObserver;
import org.luci.services.ServiceCall;
import org.luci.utilities.TAB;

public abstract class GetServiceOutputs extends GetService {
	protected Timestamp input_hash;
	
	public ISetServiceOutputs setInputHashcode(Timestamp t){
		input_hash = t;
		return (ISetServiceOutputs) this;
	}
	
	public GetServiceOutputs(FactoryObserver o, ClientHandler h, LcJSONObject json) throws SQLException{
		super(o, h, json);
	}
	
	public GetServiceOutputs(FactoryObserver o, ClientHandler h, LcJSONObject json, ServiceCall call) 
			throws SQLException{
		super(o, h, json);
		this.SObjID = call.getID();
		this.input_hash = call.getTimestamp();
	}
	
	public LcJSONObject getServiceOutputs(List<LcOutputStream> outputStreams) 
			throws FileNotFoundException, InstantiationException, SQLException, ExecutionException, 
			InterruptedException{
		String sql = "";
		PreparedStatement ps = null;
		
		try {
			short tt = TAB.SERVICEOUTPUTS;
			String t = TAB.getTableName(tt);
			
			LcJSONObject outputs = new LcJSONObject();
			short flag = TAB.flagDELETED&TAB.flagEXPORTED;
			/**Correlated sub-query SQL string using 'outer' and 'innerSQL' aliases for the same table */
			sql = "SELECT keyname, format, text, number, checksum " + " FROM "+t+" AS s"
					+ " WHERE sobjid = ? AND input_hashcode = ? AND flag & "+flag+" = 0"
					+ trs.getSQL()+";";
			ps = con.prepareStatement(sql);
			ps.setLong(1, SObjID);
			ps.setTimestamp(2, input_hash);
			trs.setTimestampSQL(ps, 3);
			ResultSet rs = ps.executeQuery();


			while (rs.next()){
				outputs.put(rs.getString("keyname"), resolve(rs, outputStreams));
			}
			return outputs;
			
		} catch (SQLException | InterruptedException | ExecutionException e) {
			con.close();
			e.printStackTrace();
			if (ps != null){
				this.log.error(e.getClass()+": SQL: "+ps.toString());
			} else {
				this.log.debug(e.getClass()+": "+sql);
			}
			con.close();
			throw e;
		}
	}	
	
	private Object resolve(ResultSet rs, List<LcOutputStream> outputStreams) throws FileNotFoundException, InstantiationException, 
			SQLException, ExecutionException, InterruptedException{
		String keyname = rs.getString(1);
		String format = rs.getString(2);
		String text = rs.getString(3);
		BigDecimal number = rs.getBigDecimal(4);
		String checksum = rs.getString(5);
		
		LcOutputStream output = null;
		if (checksum != null && outputStreams != null){
			int order = outputStreams.size() + 1;
			if (formatRequest != null){
				String rformat = formatRequest.optString(keyname);
				if (!format.equals(rformat)){
					checksum = getChecksumOfExport(keyname, format);
					if (checksum == null){
						File file = new File(Luci.getDataRoot(), checksum+"."+format);
						StreamInfo si = new StreamInfo(checksum, format, keyname, order, (int) file.length());
						StreamWriter sw = DirectStreamWriter.create(si, file);
						LcOutputStream o = sw.getLcOutputStream();
						pool.submit(sw);
						outputStreams.add(o);
					}
				}
			}
			outputStreams.add(output);
		}
		return SQL2JSON.toJSONValue(format, text, number, output);
	}
	
	private void putExport(String keyname, String format, String checksum){
		// TODO putExport
	}
	

	public boolean has(String keyname) 
			throws SQLException, JSONException, FileNotFoundException, InstantiationException, 
			ExecutionException, InterruptedException{
		return getServiceOutput(keyname, null, null) != null;
	}
	
	public JSONObject getServiceOutput(String keyname, String reqFormat, List<LcOutputStream> attachments) 
			throws SQLException, JSONException, FileNotFoundException, InstantiationException, 
			ExecutionException, InterruptedException{
		String ut = TAB.getTableName(TAB.USERS);
		String t = TAB.getTableName(TAB.SERVICEOUTPUTS);
		
		String userSQL = " (SELECT username FROM "+ut+" WHERE "+ut+".id = outputs.uid) AS username ";
		String sql = "SELECT keyname, format, text, number, checksum, timestamp, size, "+ userSQL +" ," 
				+ " machinename, serviceVersion, input_hashcode "
				+ " FROM "+t+" AS outputs WHERE sobjid = ? AND keyname = ? AND input_hashcode = ? AND iteration = ?;";
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setLong(1, SObjID);
			ps.setString(2, keyname);
			ps.setTimestamp(3, input_hash);
			ps.setInt(4, iteration);
			ResultSet rs = ps.executeQuery();
			
			if(rs.next()){
				JSONObject entry = new JSONObject();
				entry.put("value", resolve(rs, attachments));
				entry.put("user", rs.getString("username"));
				entry.put("machinename", rs.getString("machinename"));
				entry.put("serviceVersion", rs.getString("serviceVersion"));
				entry.put("timestamp", rs.getTimestamp("timestamp"));
				return entry;
			}
		} catch (SQLException e){
			con.close();
			throw e;
		}
		
		return null;
	}
	
	public String getChecksumOfExport(String keyname, String format) throws SQLException{
		String sql = "SELECT checksum FROM "+TAB.getTableName(TAB.SERVICEOUTPUTS)+" WHERE sobjid = ? AND format = ? AND input_hashcode = ?";
		PreparedStatement ps = con.prepareStatement(sql);
		ps.setLong(1, SObjID);
		ps.setString(2, format);
		ps.setTimestamp(4, input_hash);
		ResultSet rs = ps.executeQuery();
		if(rs.next()){
			return rs.getString(1);
		}
		return null;
	}
	
	public Timestamp getLatestCall(long SObjID) throws SQLException {
		String t = TAB.getTableName(TAB.SERVICECALLS);
		String sql = "SELECT MAX(t_call) FROM "+t+" WHERE sobjid = ?;";
		PreparedStatement ps = con.prepareStatement(sql);
		ps.setLong(1, SObjID);
		ResultSet rs = ps.executeQuery();
		if (rs.next()){
			return rs.getTimestamp(1);
		}
		return null;
	}
	
	public void conClose() throws SQLException{
		con.close();
	}
}
