package actions.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.luci.Luci;
import org.luci.User;
import org.luci.comm.action.Action;
import org.luci.comm.clienthandlers.ClientHandler;
import org.luci.connect.LcJSONObject;
import org.luci.factory.observer.FactoryObserver;
import org.luci.utilities.TAB;

public abstract class GetUser extends Action {
	public GetUser(FactoryObserver o, ClientHandler h, LcJSONObject json){
		super(o, h, json);
	}
	
	public User authenticate(JSONObject json, JSONArray errors) throws Exception{
		Connection con = this.o.getDatabaseConnection(); 
		String sql = "";
		try {
			String ut = TAB.getTableName(TAB.USERS);
			sql = "SELECT * FROM "+ ut + " AS users WHERE username = ? AND timestamp = "
					+ "(SELECT MAX(timestamp) FROM "+ut+" AS innerSQL"
					+ " WHERE users.id = innerSQL.id);";

			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, json.getString("username"));
			ResultSet rs = ps.executeQuery();
			if (rs.next()){
				String pwd = rs.getString("userpasswd");
				long uid = rs.getLong("id");
				String username = rs.getString("username");
				String email = rs.getString("email");
				rs.close();
				if (pwd.equals(json.getString("userpasswd"))){
					this.log.info("User '"+username+"' authenticated!");
					JSONObject u = new JSONObject();
					u.put("id", uid);
					u.put("username", username);
					String prefix = Luci.getConfig().getProperty("mqtt_pre", "/Luci/");
					publish(prefix+"logon/", Long.toString(uid));
					con.close();
					return new User(uid, username, email);
				} else {
					errors.put("Wrong password!");
					con.close();
					return null;
				}
				
			} else {
				String usercheck = "SELECT count(*) FROM "+ut+";";
				ps = con.prepareStatement(usercheck);
				rs = ps.executeQuery();
				rs.next();
				int usercount = rs.getInt(1);
				rs.close();
				if (usercount == 0){
					errors.put("No users. Create one with 'action':'create_user'");
				} else {
					errors.put("No username '"+json.getString("username")+"'");
				}
			}
			con.close();
			return null;
			
		} catch (SQLException e) {
			errors.put("authenticate: "+e.toString());
			this.log.error(e.toString());
			this.log.debug(sql);
		}
		con.close();
		return null;
	}
	
	public List<Long> getUserList() throws SQLException{
		Connection con = this.o.getDatabaseConnection(); 
		String sql = "SELECT id FROM luci.users;";
		PreparedStatement ps = con.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		List<Long> usrs = new ArrayList<Long>();
		while(rs.next()){
			usrs.add(rs.getLong(1));
		}
		con.close();
		return usrs;
	}
	
	public List<String> getUsernameList() throws SQLException{
		Connection con = this.o.getDatabaseConnection(); 
		String sql = "SELECT username FROM "+TAB.getTableName(TAB.USERS);
		PreparedStatement ps = con.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		List<String> usrs = new ArrayList<String>();
		while(rs.next()){
			usrs.add(rs.getString(1));
		}
		con.close();
		return usrs;
	}

	public JSONObject getUser(long SObjID) throws SQLException {
		Connection con = this.o.getDatabaseConnection(); 
		try{
			String ut = TAB.getTableName(TAB.USERS);
			String st = TAB.getTableName(TAB.SERVICES);
			String sql = "select username, email from "+ut+" as u join "+st+" as s on (u.id = s.uid) where s.id = ?;";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setLong(1, SObjID);
			ResultSet res = ps.executeQuery();
			res.next();
			JSONObject user = new JSONObject();
			user.put("uid", new Long(res.getLong("uid")).toString());
			user.put("username", res.getString("username"));
			user.put("userpasswd", res.getString("userpasswd"));
			user.put("email", res.getString("email"));
			res.close();
			return user;
		} catch (SQLException e){
			this.log.error("Error while reading username: "+e.getMessage());
		}
		con.close();
		return null;
	}
	
}
