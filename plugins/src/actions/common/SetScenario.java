package actions.common;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;

import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.luci.Luci;
import org.luci.comm.clienthandlers.ClientHandler;
import org.luci.connect.GeometryInfo;
import org.luci.connect.LcInputStream;
import org.luci.connect.LcJSONObject;
import org.luci.converters.IMultiStreamReader;
import org.luci.converters.IStreamReader;
import org.luci.converters.geometry.GeometryReader;
import org.luci.converters.geometry.IGeoJSONReader;
import org.luci.converters.geometry.IGeoJSONWriter;
import org.luci.converters.geometry.JSONGeometryReader;
import org.luci.factory.observer.FactoryObserver;
import org.luci.factory.observer.IDatabase;
import org.luci.utilities.Projection;
import org.luci.utilities.TAB;

public abstract class SetScenario extends GetScenario{
	public class AsyncGeometryReader implements Callable<List<Long>>{
		private Collection<GeometryReader> readers;
		private Timestamp st;
		private long ScID;
		private ExecutorService pool;
		
		public AsyncGeometryReader(Collection<GeometryReader> readers, Timestamp st, long ScID, 
				ExecutorService pool){
			this.readers = readers;
			this.st = st;
			this.ScID = ScID;
			this.pool = pool;
		}
		/* (non-Javadoc)
		 * @see java.lang.Runnable#run()
		 */
		@Override
		public List<Long> call() throws MqttPersistenceException, MqttException, InterruptedException, ExecutionException {
			String prefix = Luci.getConfig().getProperty("mqtt_pre", "/Luci/");
			String mqtt = prefix + ScID;
			List<Long> new_ids = new ArrayList<Long>();
			for (GeometryReader gc : readers) {
				long t1 = System.nanoTime();
				if (gc instanceof GeometryReader){
					// TODO: add a cyclic barrier
					List<Long> freshIDs = pool.submit(gc).get();
					if (freshIDs != null) new_ids.addAll(freshIDs);
				}
				double t2 = (double)(System.nanoTime() - t1) / 1000000000;
				log.trace(gc.getClass().getName()+": ScID "+ScID+": "+t2+" seconds");
			}
			publish(mqtt + "/" + st, new_ids.toString());
			return null;
		}
		
	}
	
	public SetScenario(FactoryObserver o, ClientHandler h, LcJSONObject header) {
		super(o, h, header);
	}

	public JSONObject create_scenario(List<LcInputStream> inputStreams, JSONArray errors) 
			throws Exception {		
		Object bbox = null, center = null;
		String crs = null;
		String name = getUniqueScenarioName(request.getString("name"));
		request.put("name", name);
		
		String t = TAB.getTableName(TAB.SCENARIOS);
		String sql = "INSERT INTO "+t+"(timestamp, name, bbox, center, crs, status, flag) "
				+ "VALUES(?,?,?,?,?,?,?);";
		
		Projection pc = new Projection();
		
		if (request.has("projection")){
			JSONObject proj = request.getJSONObject("projection");
			crs = proj.optString("crs");
			if (crs.equals("")) crs = null;
			if (crs != null){
				pc = new Projection(crs);
			} else if (proj.has("bbox")){
				JSONArray b = request.getJSONObject("projection").optJSONArray("bbox");
				double[] c = bboxCenter(b);
				IGeoJSONReader g = o.getGeoJSON_ToDb(new Projection("EPSG:4326"));
				bbox = g.geometryToDb(bbox2MultiPoint(b));
				center = g.geometryToDb(center2Point(c));
				pc = new Projection(c);
			}
		} else if (request.has("geometry")){
			crs = request.getJSONObject("geometry").optString("crs");
			if (crs.equals("")) crs = null;
			if (crs != null){
				pc = new Projection(crs);
			} else {
				pc = new Projection();
			}
		} else {
			pc = new Projection();
		}
		try {
			Timestamp stamp = new Timestamp(new Date().getTime());
			PreparedStatement ps = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			ps.setTimestamp(1, stamp);
			ps.setString(2, name);
			ps.setObject(3, bbox);
			ps.setObject(4, center);
			ps.setString(5, crs);
			ps.setString(6, "INIT");
			ps.setLong(7, 0);
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			rs.next();
			ScID = rs.getInt(1);
			IDatabase db = o.dataModelImplementation();
			db.createScenarioTables(con, log, ScID, /*srid = */ 0 );

			
			List<Long> new_ids = null;
			if (request.has("geometry")){
				new_ids = saveScenarioGeometry(request.getJSONObject("geometry"), inputStreams,
						pc, stamp, errors);
			}
			
			//this.log.info("CREATE SCENARIO: ScID("+scid+"), UniqueName("+name+")");
			String prefix = Luci.getConfig().getProperty("mqtt_pre", "/Luci/");
			String mqtt = prefix + ScID + "/";
			JSONObject result = new JSONObject();
			result.put("ScID", ScID);
			result.put("name", name);
			result.put("mqtt_topic", mqtt);
			result.putOpt("new_ids", new_ids);
			result.put("timestamp", stamp.getTime());
			publish(prefix+"new_scenario/", result.toString());
			return result;
			
		} catch (SQLException e1) {
			con.close();
			throw e1;
		}
	}
	
	private JSONObject bbox2MultiPoint(JSONArray bbox){
		if (bbox != null){
			JSONObject mp = new JSONObject();
			mp.put("type", "MultiPoint");
			mp.put("coordinates", bbox);
			return mp;
		} else return null;
		
	}
	
	private JSONObject center2Point(double[] c){
		if (c != null){
			JSONObject center = new JSONObject();
			center.put("type", "Point");
			center.put("coordinates", new JSONArray(c));
			return center;
		} else return null;
		
	}
	
	public double[] bboxCenter(JSONArray bbox){
		if (bbox != null){
			JSONArray TL = bbox.getJSONArray(0);
			JSONArray BR = bbox.getJSONArray(1);
			return new double[]{(TL.getDouble(0) + BR.getDouble(0))/2, 
								(TL.getDouble(1) + BR.getDouble(1))/2 };
		} else return null;
	}
	
	public Timestamp updateScenario(List<LcInputStream> inputStreams, List<Long> new_ids, 
			JSONArray errors) throws Exception{
		ScID = request.getInt("ScID");
		con = o.getDatabaseConnection();
		Timestamp stamp = new Timestamp(new Date().getTime());
		String t = TAB.getTableName(TAB.SCENARIOS);
		String crs = null, name = null, status = "NONE";
		JSONArray bbox = null;
		
		// name & boundary
		if (request.has("name")){
			name = request.optString("name", null);
			if (name != null){
				List<String> scenarios = getScenarioNameList();
				if (scenarios.contains(name)){
					errors.put("Scenario name '" + name + "' already taken. Renaming cancelled.");
					name = null;
				}
			}
		}
		/** a scenario can update its name, bbox but not its center or crs (since if center is not null it serves 
		  * as the center for the projection and therefore a replacement for the coordinate reference system (crs) */
		if (name != null || request.has("bbox")){
			IGeoJSONReader g = o.getGeoJSON_ToDb(new Projection("EPSG:4326"));
			String sql = "INSERT INTO "+t+"(id, timestamp, name, bbox, center, status, flag) "
					+ "SELECT ?, ?, COALESCE(?, name), COALESCE(?,bbox), COALESCE(?,center), ?, ? "
					+ "FROM "+t+" WHERE id = ? AND timestamp = (SELECT MAX(timestamp) FROM "+t+" WHERE id = ?);";

			PreparedStatement ps = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			bbox = request.optJSONArray("bbox");
			status = "UPDT";
			ps.setLong(1, ScID);
			ps.setTimestamp(2, stamp);
			ps.setString(3, name);
			ps.setObject(4, g.geometryToDb(bbox2MultiPoint(bbox)));
			ps.setObject(5, g.geometryToDb(center2Point(bboxCenter(bbox))));
			ps.setString(6, status);
			ps.setLong(7, 0);
			ps.setLong(8, ScID);
			ps.setLong(9, ScID);
			ps.executeUpdate();
			String sql_get_crs = "SELECT crs FROM "+t+" WHERE id = ? AND STATUS = \'INIT\'";
			ps = con.prepareStatement(sql_get_crs);
			ps.setLong(1,ScID);
			ResultSet rs = ps.executeQuery();
			rs.next();
			//TODO crs is rs.getString(1) in H2GIS
			crs = rs.getString(1);
		} else {
			Projection pc = new Projection();
			IGeoJSONWriter g = o.getGeoJSON_FromDb(pc);
			String sql = "SELECT name, crs, bbox, center, status FROM "+t+" AS scns WHERE id = ? AND timestamp = "
					+ "(SELECT MAX(timestamp) FROM "+t+" WHERE id = ?);";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setLong(1, ScID);
			ps.setLong(2, ScID);
			ResultSet rs = ps.executeQuery();
			rs.next();
			name = rs.getString(1);
			JSONObject bbox_geojson = g.geometryFromDb(rs.getObject(2));
			if (bbox_geojson != null)
				bbox = bbox_geojson.getJSONArray("coordinates");
			crs = rs.getString(2);
		}
		
		
		// update geometry --> saveScenarioGeometry()
		if (request.has("geometry")){
			// ? tkey.tableType = TKey.SCENARIOGEOMETRY;
			Projection pc;
			if (bbox != null)
				pc = new Projection(bboxCenter(bbox));
			else if (crs != null)
				pc = new Projection(crs);
			else
				pc = new Projection();
			if (request.optBoolean("switchLatLon", false)) pc.switchLatLon();
			List<Long> freshIDs = saveScenarioGeometry(request.getJSONObject("geometry"), inputStreams, pc, stamp, errors);
			if (freshIDs != null) new_ids.addAll(freshIDs);
		}
		con.close();
		return stamp;
	}
	
	private String getUniqueScenarioName(String name) throws Exception {
		Connection con = this.o.getDatabaseConnection();
		String sql = "SELECT name FROM "+TAB.getTableName(TAB.SCENARIOS)+" WHERE name like ? ;";
		PreparedStatement ps = con.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
		ps.setString(1, name+"%");
		ResultSet names = ps.executeQuery();
		names.last();
		if (names.getRow() > 0){
			try {
				String lastName = names.getString(1);
				int i = new Integer(lastName.substring(lastName.length()-3));
				i++;
				// instead of calling java.text.DecimalFormat with lots of constructors etc.
				// we just take an if:
				if (i<10) name = name.concat(".00"+i);
				else if (i < 100) name = name.concat(".0"+i);
				else name = name.concat("."+i);
			} catch (NumberFormatException e){
				name = name.concat(".001");
			}
		}
		con.close();
		return name;
	}
	
	/**
	 * @return Map < keyname-string, Map < format-string, streaminfo-JSONObject > > 
	 */
	private Map<String, Map<String, GeometryInfo>> groupGeometryBySimilarKeys(JSONObject j,
			List<LcInputStream> inputStreams){
		/** since there is no recursion we can create an index by keys */
		Map<String, LcInputStream> inputsByKey = new HashMap<String, LcInputStream>();
		for (LcInputStream input: inputStreams) inputsByKey.put(input.getName(), input);
		Map<String, Map<String, GeometryInfo>> res = new HashMap<String, Map<String, GeometryInfo>>();
		@SuppressWarnings("unchecked")
		Set<String> keySet = j.keySet();
		for (String key: keySet){
			Object value = j.get(key);
			if (value instanceof JSONObject){
				JSONObject jvalue = ((JSONObject) value);
				String format = jvalue.optString("format");
				if (format != null){
					String groupname = key.replace("."+format,"");
					if (jvalue.has("geometry")){
						Map<String, GeometryInfo> similar = (res.containsKey(groupname)) ? 
								res.get(groupname) : new TreeMap<String, GeometryInfo>(String.CASE_INSENSITIVE_ORDER);
						similar.put(format, new GeometryInfo(groupname, jvalue));
						res.put(groupname, similar);
					} else if (jvalue.has("streaminfo")){
						LcInputStream lis = inputsByKey.remove(key);
						Map<String, GeometryInfo> similar = (res.containsKey(groupname)) ? 
								res.get(groupname) : new TreeMap<String, GeometryInfo>(String.CASE_INSENSITIVE_ORDER);
						similar.put(lis.format, lis);
						res.put(groupname, similar);
					}
				}
			}
		}
		return res;
	}
	
	private List<Long> saveScenarioGeometry(JSONObject geometries, List<LcInputStream> inputStreams,
			Projection scnPc, Timestamp t, JSONArray errors) throws Exception{
		try{
			if (geometries.keySet().size() == 0) 
				throw new JSONException("Invalid geometry description format: no json object found!");
			ExecutorService pool = o.getThreadPool();
			Map<Integer, GeometryReader> gcs = new TreeMap<Integer, GeometryReader>();
			if (inputStreams.size() > 0) lastInput = inputStreams.get(inputStreams.size()-1);
			Set<Entry<String, Map<String, GeometryInfo>>> grouped = 
					groupGeometryBySimilarKeys(geometries, inputStreams).entrySet();

			for (Entry<String, Map<String, GeometryInfo>> en: grouped){
				Projection pc = scnPc;
				Map<String, GeometryInfo> group = en.getValue();
				Set<String> formats = group.keySet();
				String mainFormat = o.getMultiFileMainFormat(formats);

				if (mainFormat == null){
					String error = "Geometry '"+en.getKey()+"' could not be saved. "
							+ "Format / format combination "+formats+" not available!";
					log.error(error);
					errors.put(error);
				} else {
					Class<?> reader = o.getGeometryReaderClass(mainFormat);
					if (JSONGeometryReader.class.isAssignableFrom(reader)){
						
					}
					GeometryInfo geom = group.get(mainFormat);
					if (geom.crs != null){
						if (scnPc.getSRID() != 0){
							if (!geom.crs.equalsIgnoreCase(scnPc.getEPSG()))
								pc = new Projection(geom.crs, scnPc.getEPSG());
							else pc = scnPc;
						} else {
							pc = new Projection(geom.crs, scnPc.getCenter());
							if (scnPc.getCenter() == null)
								errors.put("Saving geo-referenced geometry to a scenario with "
										+ "no geo reference!");
						}
					}
					long uid = h.getUser().getUid();
					if (geom instanceof LcInputStream){
						GeometryReader gr = o.getGeometryReader(mainFormat, ScID, uid, t, pc, errors);
						if (gr instanceof IMultiStreamReader){
							for (GeometryInfo g: group.values()){
								gcs.put(((LcInputStream) g).getOrder(), gr);
							}
						} else gcs.put(((LcInputStream) geom).getOrder(), gr);
					} else {
						int maxOrder = gcs.size() > 0 ? Collections.max(gcs.keySet()) : 0;
						gcs.put(Math.max(inputStreams.size(), maxOrder + 1),
								o.getJSONGeometryReader(mainFormat, ScID, uid, t, pc, errors, geom));
					}
				}
			}

			for (LcInputStream lis: inputStreams){
				int o = lis.getOrder();
				if (gcs.containsKey(o)){
					GeometryReader gr = gcs.get(o);
					try {
						this.readInputStream((IStreamReader) gr, lis);
					} catch (Exception e){
						errors.put(e.toString());
						return new ArrayList<Long>();
					}
				} else {
					this.readInputStreamToFile(lis);
				}
			}

			this.waitForAllInputStreamsToBeRead();
			
			// removes geometry readers that are listed in gcs multiple times to read all streams
			Set<GeometryReader> readersToCall = new HashSet<GeometryReader>();
			for (GeometryReader gr: gcs.values()) readersToCall.add(gr);
			
			String prefix = Luci.getConfig().getProperty("mqtt_pre", "/Luci/");
			String mqtt = prefix + ScID;
			String timestamp = Long.toString(t.getTime());
			publish(mqtt + "/new_hash", timestamp);
			/* 	If converters should become supervised calls like service calls this would be point where the threads split.
				The method would then return converter ids instead of newly created geometry ids. */
			return pool.submit(new AsyncGeometryReader(readersToCall, t, ScID, pool)).get();
		} catch (SecurityException | IllegalArgumentException | InstantiationException | 
				InterruptedException | ExecutionException | MqttException | IOException e){
			if (log.isDebugEnabled()) e.printStackTrace();
			log.error(e.getMessage());
			errors.put(e.getMessage());
		}
		return null;
	}

		
}
