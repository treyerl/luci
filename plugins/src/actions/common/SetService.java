package actions.common;

import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.luci.Luci;
import org.luci.comm.action.ISetServiceOutputs;
import org.luci.comm.clienthandlers.ClientHandler;
import org.luci.connect.LcInputStream;
import org.luci.connect.LcJSONObject;
import org.luci.converters.JSON2SQL;
import org.luci.converters.geometry.IGeoJSONReader;
import org.luci.factory.ServiceFactoryThread;
import org.luci.factory.observer.FactoryObserver;
import org.luci.services.ServiceCall;
import org.luci.utilities.Projection;
import org.luci.utilities.TAB;
import org.luci.utilities.TimeRangeSelection;

public abstract class SetService extends GetServiceOutputs implements ISetServiceOutputs{
	protected Timestamp ts;
	public SetService(FactoryObserver o, ClientHandler h, LcJSONObject json) throws SQLException {
		super(o, h, json);
	}
	
	public void setServiceStatus(ServiceCall call, String status) throws SQLException{
		long SObjID = call.getSObjID();
		String t = TAB.getTableName(TAB.SERVICES);
		TimeRangeSelection trs = new TimeRangeSelection().setUntil(call.getTimestamp());
		String sql = "UPDATE "+t+" AS s SET status = ? WHERE id = ? AND status <> 'DLTD' "+trs.getSQL(); 
		log.trace(SObjID+": set status: "+status);
		Connection con = this.o.getDatabaseConnection();
		try {
			
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, status);
			ps.setLong(2, SObjID);
			trs.setTimestampSQL(ps, 3);
			if (ps.executeUpdate() == 0){
				sql = "SELECT status FROM "+t+" WHERE id = ?;";
				ps = con.prepareStatement(sql);
				ps.setLong(1, SObjID);
				ResultSet rs = ps.executeQuery();
				if (rs.next()){
					con.close();
					throw new SQLException("Service instance "+SObjID+" has been deleted.");
				} else {
					con.close();
					throw new SQLException("Service instance "+SObjID+" does not exist.");
				}
			}
			con.close();
		} catch (SQLException e1) {
			con.close();
			log.error("setServiceStatus: "+e1.toString());
			throw new SQLException("setServiceStatus: "+e1.toString());
		}
	}
	
	/**in synchronous mode inputs will not be stored / should be stored at service runtime in order 
	 * to be able to directly forward any attachments to remote services
	 * @param unknowns a set of strings being filled during input validation
	 * @param async boolean indicating wheater or not the service should be registered in mqtt and 
	 * inputs should be saved
	 * @return
	 * @throws Exception
	 */
	public JSONObject create_service(Set<String> unknowns, boolean async) 
			throws Exception {
		String name = request.getString("classname");
		long uid = h.getUser().getUid();
		if (o.getServiceFactory(name) == null){
			throw new ClassNotFoundException(name+" not a service!");
		}

		ServiceFactoryThread sf = o.getServiceFactory(name);
		if (sf != null) {
			LcJSONObject inputs = new LcJSONObject();
			if (request.has("inputs"))
				inputs = new LcJSONObject(request.getJSONObject("inputs"));
			
			if (sf.validateInput(inputs, unknowns)) {
				Connection con = o.getDatabaseConnection();
				
				String t = TAB.getTableName(TAB.SERVICES);
				ScID = request.optInt("ScID", 0);
				ts = new Timestamp(new Date().getTime());
				JSONArray subscriptions = request.optJSONArray("run_on");
				
				String sql = "INSERT INTO "+t+"(id,name,scid,status,uid,timestamp,service_subscriptions,flag) "
						+ "VALUES((SELECT COALESCE(max(id)+1,1) FROM "+t+"),?,?,'INIT',?,?,?,0)";
				PreparedStatement ps = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				ps.setString(1, name);
				ps.setLong(2, ScID);
				ps.setLong(3, uid);
				ps.setTimestamp(4, ts);
				if (subscriptions == null) ps.setNull(5, Types.VARCHAR);
				else ps.setString(5, subscriptions.toString());
				try {
					ps.executeUpdate();
					ResultSet rs = ps.getGeneratedKeys();
					rs.next();
					SObjID = rs.getLong("id");
				} catch (Exception e){
					con.close();
					this.log.debug(e.getClass()+": "+ps.toString());
					throw e;
				}
				
				String mqtt_pre = Luci.getConfig().getProperty("mqtt_pre", "/Luci/");
				con.close();
				
				JSONObject result = new JSONObject();
				
				if (async){
					
					// INPUTS
					updateServiceInputs();
					
					// MQTT
					String myMqttTopic = mqtt_pre+ScID+"/"+SObjID;
					List<String> topics = new ArrayList<String>();
					topics.add(myMqttTopic);
					if (subscriptions != null){
						for (int i = 0; i < subscriptions.length(); i++){
							Object sub = subscriptions.get(i);
							if (sub instanceof Integer) topics.add(mqtt_pre+"+/"+sub);
							else if (sub instanceof String) topics.add((String) sub);
						}
					}
					sf.updateNonBlockingAndMQTT(topics, SObjID);
					result.put("mqtt_topic", myMqttTopic);
				}
				
				result.put("SObjID", SObjID);
				result.put("ScID", ScID);
				result.put("input_hashcode", ts.getTime());
				result.put("classname", name);
				return result;
			}
			return new JSONObject().put("error", "Invalid inputs!");
		} else {
			throw new InstantiationException("Known service '"+name+" returned no factory!");
		}
	}
	
	public void updateServiceInputs() throws Exception{
		try {
			if (request.has("inputs")) {
				String name = request.getString("classname");
				ServiceFactoryThread sf = o.getServiceFactory(name);
				IGeoJSONReader g = o.getGeoJSON_ToDb(new Projection());
				
				JSONObject inputs = request.getJSONObject("inputs");
				String t = TAB.getTableName(TAB.SERVICEINPUTS);
				
				@SuppressWarnings("unchecked")
				Set<String> keySet = inputs.keySet();
				for (String key: keySet){
					Object value = inputs.get(key);
					String sql = "INSERT INTO "+t+"(sobjid,keyname,format,text,number,geom,output_subscription,checksum,timestamp,uid) "
							+ "VALUES(?,?,?,?,?,?,?,?,?,?)";
					PreparedStatement ps = con.prepareStatement(sql);
					ps.setLong(1, SObjID);
					ps.setString(2, key);
					JSON2SQL.ServiceInput(ps, key, value, 3, g);
					ps.setTimestamp(9, ts);
					ps.setLong(10, h.getUser().getUid());
					ps.executeUpdate();
				}
				
				// MQTT
				String mqtt_pre = Luci.getConfig().getProperty("mqtt_pre", "/Luci/");
				String myMqttTopic = mqtt_pre+ScID+"/"+SObjID;
				publish(myMqttTopic+"/new_hash", ""+ts.getTime());
				
				sf.subscribe(SObjID, inputs);
			}
		} catch (SQLException e){
			e.printStackTrace();
			if (con != null) con.close();
			throw e;
		}
	}
	
	public JSONArray updateServiceOutputs(List<LcInputStream> binaryOutputs) throws SQLException, 
		FileNotFoundException, JSONException, InstantiationException, ExecutionException, 
		InterruptedException, MqttPersistenceException, MqttException{
		if (binaryOutputs.size() > 0) lastInput = binaryOutputs.get(binaryOutputs.size()-1);
		readAllInputStreamsToFile(binaryOutputs);
		try {
			if (!has(SObjID)) throw new SQLException("Service instance with ID '"+SObjID+"' does not exist");
			JSONArray errors = new JSONArray();
			JSONObject outputs = request.getJSONObject("outputs");
			String machinename = request.optString("machinename");
			String serviceversion = request.optString("serviceversion");
			Timestamp t = new Timestamp(new Date().getTime());
			
			List<String> outputTopicKeynames = new ArrayList<String>();
			
			@SuppressWarnings("unchecked")
			Set<String> keySet = outputs.keySet();
			for (String keyname: keySet){
				/* checking for existing outputs with the given input_hashcode (SObjID, keyname)
				 * --> not allowed to overwrite existing outputs; 
				 * also no mqtt notification on "../SObjID/outputs/keyname" */ 
				if (has(keyname)) {
					errors.put("keyname '"+keyname+"' has already been set for service '"+SObjID+"', "
							+ "input_hashcode '"+input_hash+"'. Skipping.");
					continue;
				}
				String tt = TAB.getTableName(TAB.SERVICEOUTPUTS);
				String sql = "INSERT INTO "+tt+"(sobjid,input_hashcode,iteration,keyname,timestamp,uid,machinename,"
						+ "serviceversion,format,text,number,checksum,flag) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)";
				PreparedStatement ps = con.prepareStatement(sql);
				ps.setLong(1, SObjID);
				ps.setTimestamp(2, input_hash);
				ps.setInt(3, iteration);
				ps.setString(4, keyname);
				ps.setTimestamp(5, t);
				ps.setLong(6, (h != null) ? h.getUser().getUid() : 0);
				ps.setString(7, machinename);
				ps.setString(8, serviceversion);
				JSON2SQL.ServiceOutputOrGeometryAttribute(ps, outputs.get(keyname), 9);
				ps.setLong(13, flag);
				ps.executeUpdate();
				outputTopicKeynames.add(keyname);
			}
			
			String topic = getServiceMqttTopic(SObjID);
			
			// update subscribed inputs & PUBLISH updated output event
			Map<String, String> topics = new HashMap<String, String>();
			for (String key: outputTopicKeynames){
				String subscriptionKey = topic + "outputs/" + key;
				updateSubscribers(subscriptionKey, outputs.get(key), t, errors);
				topics.put(subscriptionKey, "update:"+new JSONObject().put("timestamp", t).put("iteration", iteration));
			}
			topics.put(topic+"iteration", ""+iteration);
			publish(topics);
			waitForAllInputStreamsToBeRead();
			return errors;
		} catch (SQLException e){
			e.printStackTrace();
			con.close();
			throw e;
		}
		
	}
	
	private String getServiceMqttTopic(long SObjID) throws SQLException{
		String pre = Luci.getConfig().getProperty("mqtt_pre", "/Luci/");
		long scid = getServiceScID(SObjID);
		return pre + scid + "/" + SObjID + "/";
	}
	
	private void updateSubscribers(String subscriptionKey, Object value, Timestamp timestamp, JSONArray errors) throws SQLException{
		String t = TAB.getTableName(TAB.SERVICEINPUTS);
		Connection con1 = null;
		Connection con2 = null;
		String sql = "NO SQL STATEMENTS AVAILABLE";
		try {
			con1 = o.getDatabaseConnection();
			con2 = o.getDatabaseConnection();
			String where = "WHERE output_subscription = ? ";
			sql = "SELECT sobjid, format FROM "+t+" AS si "+where;
			PreparedStatement ps1 = con1.prepareStatement(sql);
			ps1.setString(1, subscriptionKey);
			ResultSet rs = ps1.executeQuery();
			while(rs.next()){
				long SObjID = rs.getLong(1);
				String format = rs.getString(2);
				sql = "UPDATE "+t+" SET timestamp = ?, text = ?, number = ? "+" as si "+where+" format = "+format;
				PreparedStatement ps2 = con2.prepareStatement(sql);
				ps2.setTimestamp(1, timestamp);
				try{
					JSON2SQL.updateSubscribedField(ps2, value, format, 2);
					ps2.executeUpdate();
				} catch (RuntimeException e){
					errors.put(SObjID+": "+e.getMessage());
					if (log.isDebugEnabled()) e.printStackTrace();
					con1.close();
					con2.close();
				}
			}
			con1.close();
			con2.close();
		} catch (Exception e) {
			log.debug(sql + "  --  "+ subscriptionKey);
			if (con1 != null) con1.close();
			if (con2 != null) con2.close();
			throw e;
		}
		
	}
}
