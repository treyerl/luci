package actions.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.luci.Luci;
import org.luci.comm.action.Action;
import org.luci.comm.clienthandlers.ClientHandler;
import org.luci.connect.LcJSONObject;
import org.luci.converters.geometry.IGeoJSONReader;
import org.luci.factory.observer.FactoryObserver;
import org.luci.utilities.Projection;
import org.luci.utilities.TAB;

public abstract class SetUser extends Action {

	public SetUser(FactoryObserver o, ClientHandler h, LcJSONObject json) {
		super(o, h, json);
	}

	public long create_user(boolean first, JSONObject json, JSONObject result, JSONArray errors) throws Exception {
		Connection con = this.o.getDatabaseConnection();
		String t = TAB.getTableName(TAB.USERS);
		if (first){
			try {
				String sql = "SELECT count(*) FROM "+t+";";
				PreparedStatement ps = con.prepareStatement(sql);
				ResultSet rs = ps.executeQuery();
				rs.next();
				int i = rs.getInt(1);
				if (i != 0){
					sql = "SELECT username FROM "+t+";";
					ps = con.prepareStatement(sql);
					rs = ps.executeQuery();
					List<String> usernames = new ArrayList<String>();
					while(rs.next()){
						usernames.add(rs.getString(1));
					}
					errors.put("Authenticate with one of the existing users before creating new users, please!");
					errors.put(usernames);
					return 0;
				}
			} catch (SQLException e) {
				con.close();
				this.log.debug(e.toString());
				errors.put("create first user: " + e.toString());
				return 0;
			}
		}
		con.close();
		return this.create_user(json, result, errors);
	}
	
	private long create_user(JSONObject request, JSONObject result, JSONArray errors) throws Exception{
		Connection con = this.o.getDatabaseConnection();
		String t = TAB.getTableName(TAB.USERS);
		String d = TAB.getTableName(TAB.USERDETAILS);
		long uID;
		String username = (String) request.remove("username");
		PreparedStatement ps = null;
		try {
			// insert user
			Timestamp stamp = new Timestamp(new Date().getTime());
			request.remove("action");
			String sql = "INSERT INTO "+t+"(id, timestamp, username, userpasswd, email,flag) "
					+ "VALUES ((SELECT COALESCE(max(id)+1,1) FROM "+t+"),?,?,?,?,0)";
			String name = getUniqueUsername(username, errors);
			ps = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			ps.setTimestamp(1, stamp);
			ps.setString(2, name);
			ps.setString(3, (String) request.remove("userpasswd"));
			ps.setString(4, (String) request.remove("email"));
			this.log.debug(ps.toString());
			ps.executeUpdate();
			ResultSet rs = ps.getGeneratedKeys();
			if (rs.next())
				uID = rs.getLong(1);
			else {
				this.log.debug(ps.toString());
				throw new Exception("DB did not return user ID!");
			}
			
			// insert the details into the user_details table
			String det = "INSERT INTO "+d+"(uid,timestamp, keyname, value, location,flag) VALUES(?,?,?,?,?,0);";
			ps = con.prepareStatement(det);
			if (request.length() > 0){
				Projection pc = new Projection();
				IGeoJSONReader g = o.getGeoJSON_ToDb(pc);
				for (String key: JSONObject.getNames(request)){
					JSONObject value = request.getJSONObject(key);
					ps.setLong(1, uID);
					ps.setTimestamp(2, stamp);
					ps.setString(3, key);
					ps.setString(4, value.getString("value"));
					JSONArray latlon = value.optJSONArray("location");
					JSONObject location = null;
					if (latlon != null){
						location = new JSONObject();
						location.put("type", "Point");
						location.put("coordinates", latlon);
					}
					ps.setObject(5, g.geometryToDb(location));
					ps.executeUpdate();
					ps.clearParameters();
				}
			}
			JSONObject u = new JSONObject();
			u.put("id", uID);
			u.put("username", name);
			String prefix = Luci.getConfig().getProperty("mqtt_pre", "/Luci/");
			publish(prefix+"new/user/", Long.toString(uID));
			result.put("ID", uID);
			result.put("username", name);
			con.close();
			return uID;
		} catch (SQLException e) {
			this.log.error("User creation failed!"+e.toString());
			if (ps != null) this.log.debug(ps.toString());
			errors.put("User creation failed! "+e.toString());
			con.close();
			throw e;
		}
	}
	
	public void updateUser(JSONObject json, JSONArray errors) {
		// TODO updateUser();
	}
	
	public String getUniqueUsername(String name, JSONArray errors) throws SQLException{
		Connection con = this.o.getDatabaseConnection(); 
		try {
			String ut = TAB.getTableName(TAB.USERS);
			String max = "SELECT max(id) FROM " +ut;
			PreparedStatement ps = con.prepareStatement(max);
			ResultSet rs = ps.executeQuery();
			if (rs.next()){
				String sql = "SELECT username FROM "+ut+" WHERE username = ?";
				ps = con.prepareStatement(sql);
				ps.setString(1, name);
				rs = ps.executeQuery();
				if (rs.next()){
					throw new SQLException("username '"+name+"' already taken");
				}
			}			
		} catch (SQLException e) {
			errors.put("getUniqueUsername: "+e.toString());
			e.printStackTrace();
		}
		con.close();
		return name;
		
	}
}
