package actions;

import java.util.List;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;
import org.luci.Luci;
import org.luci.comm.clienthandlers.ClientHandler;
import org.luci.connect.LcInputStream;
import org.luci.connect.LcJSONObject;
import org.luci.connect.LcMetaJSONObject;
import org.luci.connect.LcOutputStream;
import org.luci.factory.observer.FactoryObserver;

import actions.common.SetScenario;

public class create_scenario extends SetScenario {
	
	public create_scenario(FactoryObserver o, ClientHandler h, LcJSONObject json) {
		super(o, h, json);
	}
	
	@Override
	public LcMetaJSONObject getInputIndex() {
		int srid = Luci.getSRID();
		String proj = "";
		String proj_comment = "The default projection of your database is set to EPSG:" + srid + ". "
				+ "All geometry will be converted if necessary. Configure Luci\\\'s srid to \\\"None\\\" "
				+ "if you prefer to use seperate projections per scenario.";
		
		if (srid == 0){
			proj    = "     'OPT projection':{"
					+ "			'XOR bbox':[['TL_LAT', 'TL_LON'], ['BR_LAT', 'BR_LON']],"
					+ "			'XOR crs':'string'"
					+ "		},";
			proj_comment = "Coordinates in meters if no CRS is given; CRS formatted like EPSG:4326."; 
		}
		
		String inputjson = ""
				+ "{	'action':'"+this.getClass().getSimpleName()+"',"
				+ "		'name':'string',"
				+ proj
				+ "		'OPT geometry': {"
				+ "			'KEYNAME':['streaminfo','geometry']"
				+ "		},"
				+ "		'OPT switchLatLon':'boolean',"
				+ "		'subject':'Scenarios',"
				+ "		'comment':'" + proj_comment + "; (KEYNAME: e.g. xy.shp, xy.dbf, or scenarioname.json)'"
				+ "}";
		return new LcMetaJSONObject(inputjson);
	}
	
	@Override
	public JSONObject perform(List<LcInputStream> inputStreams, List<LcOutputStream> outputStreams, 
			Set<String> unknownKeys) throws Exception{
		JSONArray errors = new JSONArray();
		long id = 0;
		
		JSONObject result = new JSONObject();
		con = o.getDatabaseConnection();
		result.put("result", create_scenario(inputStreams, errors));
		con.close();
		
		if (id != 0)
			result.put("message",  "scenario with ID "+id+" has been created");
		if (errors.length() != 0)
			result.put("errors", errors);
		
		return result; //new JSONObject().put("result", result);
	}

	@Override
	public LcMetaJSONObject getOutputIndex() {
		String outputjson = ""
				+ "	{'XOR result': {"
				+ "		'name':'string',"
				+ "		'new_ids':'list',"
				+ "		'ScID':'number',"
				+ "		'mqtt_topic':'string',"
				+ "		'message':'string',"
				+ "		'timestamp':'number'"
				+ "		},"
				+ "	 'XOR error': ['json','list','string']"
				+ "}";
		return new LcMetaJSONObject(outputjson);
	}

}
