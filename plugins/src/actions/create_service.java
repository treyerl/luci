package actions;

import java.sql.SQLException;
import java.util.List;
import java.util.Set;

import org.json.JSONObject;
import org.luci.comm.clienthandlers.ClientHandler;
import org.luci.connect.LcInputStream;
import org.luci.connect.LcJSONObject;
import org.luci.connect.LcMetaJSONObject;
import org.luci.connect.LcOutputStream;
import org.luci.factory.observer.FactoryObserver;

import actions.common.SetService;

public class create_service extends SetService {
	
	public create_service(FactoryObserver o, ClientHandler h, LcJSONObject json) throws SQLException {
		super(o, h, json);
	}
	
	@Override
	public LcMetaJSONObject getInputIndex() {
		String inputjson = ""
				+ "{	'action':'"+this.getClass().getSimpleName()+"',"
				+ "		'classname':'string',"
				+ "		'OPT ScID':'number',"
				+ "		'OPT inputs':{"
				+ "			'KEYNAME':"+LcMetaJSONObject.jsontypes
				+ "		},"
				+ "		'OPT run_on': 'list',"
				+ "		'subject':'Services'"
				+ "}";
		return new LcMetaJSONObject(inputjson);
	}
	
	@Override
	public JSONObject perform(List<LcInputStream> inputStreams, List<LcOutputStream> outputStreams, 
			Set<String> unknownKeys) throws Exception{
		if (inputStreams.size() > 0) lastInput = inputStreams.get(inputStreams.size()-1);
		con = o.getDatabaseConnection();
		try{
			readAllInputStreamsToFile(inputStreams);
			JSONObject result = create_service(unknownKeys, true);
			result.put("message",  "Service with ID "+result.getLong("SObjID")+" has been created");
			waitForAllInputStreamsToBeRead();
			return new JSONObject().put("result", result);
		} catch (Exception e){
			con.close();
			throw e;
		}
		
	}


	@Override
	public LcMetaJSONObject getOutputIndex() {
		String outputjson = ""
				+ "	{'XOR result': {"
				+ "		'SObjID':'number',"
				+ "		'ScID':'number',"
				+ "		'input_hashcode':'number',"
				+ "		'mqtt_topic':'string',"
				+ "		'classname':'string',"
				+ "		},"
				+ "	 'XOR error': ['json','list','string']"
				+ "}";
		return new LcMetaJSONObject(outputjson);
	}

}
