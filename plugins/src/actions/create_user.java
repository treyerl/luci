package actions;

import java.util.List;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;
import org.luci.comm.clienthandlers.ClientHandler;
import org.luci.connect.LcInputStream;
import org.luci.connect.LcJSONObject;
import org.luci.connect.LcMetaJSONObject;
import org.luci.connect.LcOutputStream;
import org.luci.factory.observer.FactoryObserver;

import actions.common.SetUser;

public class create_user extends SetUser {
	
	public create_user(FactoryObserver o, ClientHandler h, LcJSONObject json) {
		super(o, h, json);
	}

	@Override
	public LcMetaJSONObject getInputIndex() {
		String inputjson = ""
				+ "{	'action':'"+this.getClass().getSimpleName()+"',"
				+ "		'username':'string',"
				+ "		'userpasswd':'string',"
				+ "		'email':'string',"
				+ "		'OPT KEYNAME':{"
				+ "			'value':"+LcMetaJSONObject.jsontypes+","
				+ "			'OPT location':[LAT, LON]"
				+ "		},"
				+ "		'subject':'Users'"

				+ "}";
		return new LcMetaJSONObject(inputjson);
	}
	
	@Override
	public JSONObject perform(List<LcInputStream> inputStreams, List<LcOutputStream> outputStreams, 
			Set<String> unknownKeys) throws Exception{
		JSONArray errors = new JSONArray();
		long id = 0;
		
		JSONObject result = new JSONObject();
		if (!this.h.isAuthenticated()) id = create_user(true, this.request, result, errors);
		else id = create_user(false, this.request, result, errors);
		if (errors.length() != 0) result.put("errors", errors);
		else result.put("message",  "user with ID "+id+" has been created");
		return new JSONObject().put("result", result);
	}

	@Override
	public LcMetaJSONObject getOutputIndex() {
		String outputjson = ""
				+ "	{'XOR result': {"
				+ "		'id':'number',"
				+ "		'username':'string',"
				+ "		},"
				+ "	 'XOR error': ['json','list','string']"
				+ "}";
		return new LcMetaJSONObject(outputjson);
	}

}
