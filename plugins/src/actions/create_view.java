package actions;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.List;
import java.util.Set;

import org.json.JSONObject;
import org.luci.comm.action.Action;
import org.luci.comm.clienthandlers.ClientHandler;
import org.luci.connect.LcInputStream;
import org.luci.connect.LcJSONObject;
import org.luci.connect.LcMetaJSONObject;
import org.luci.connect.LcOutputStream;
import org.luci.factory.observer.FactoryObserver;
import org.luci.utilities.TAB;
import org.luci.utilities.TimeRangeSelection;

public class create_view extends Action {

	public create_view(FactoryObserver o, ClientHandler h, LcJSONObject json) {
		super(o, h, json);
	}

	@Override
	public LcMetaJSONObject getInputIndex() {
		String inputjson = ""
				+ "{	'action':'"+this.getClass().getSimpleName()+"',"
				+ "		'name':'string',"
				+ "		'ScID':'number',"
				+ "		'OPT timerange':'timerange',"
				+ "		'OPT layer':'string',"
				+ "		'subject':'Database'"
				+ "}";
		return new LcMetaJSONObject(inputjson);
	}

	@Override
	public LcMetaJSONObject getOutputIndex() {
		String outputjson = ""
				+ "	{'XOR result': {"
				+ "  	'message':'string',"
				+ "		'sql':'string'"
				+ "  },"
				+ "	 'XOR error': ['json','list','string']"
				+ "}";
		return new LcMetaJSONObject(outputjson);
	}

	@Override
	public JSONObject perform(List<LcInputStream> inputStreams, List<LcOutputStream> outputStreams, 
			Set<String> unknownKeys) throws Exception{
		String name = request.getString("name");
		long ScID = request.getLong("ScID");
		TimeRangeSelection trs = null;
		if (request.has("timerange")) trs = new TimeRangeSelection(request.getJSONObject("timerange"));
		Connection con = o.getDatabaseConnection();
		String layer = request.optString("layer", null);
		String tab = TAB.getScenarioTable(ScID, TAB.SCN);
		String sql = "CREATE VIEW "+name+" AS SELECT ST_Force_2d(geom), * FROM "+tab;
		PreparedStatement ps = null;
		if (trs != null && layer != null) {
			sql += " WHERE "+trs.getSQL()+" AND layer = ?";
			ps = con.prepareStatement(sql);
			int i = trs.setTimestampSQL(ps, 1);
			ps.setString(i, layer);
		} else if (trs != null) {
			sql += " WHERE "+trs.getSQL();
			ps = con.prepareStatement(sql);
			trs.setTimestampSQL(ps, 1);
		} else if (layer != null) {
			sql += " WHERE layer = "+layer;
			ps = con.prepareStatement(sql);
			ps.setString(1, layer);
		}
		ps.executeUpdate();
		con.close();
		JSONObject result = new JSONObject().put("message", "View "+name+" successfully created or replaced").put("sql", sql);
		return new JSONObject().put("result", result);
	}

}
