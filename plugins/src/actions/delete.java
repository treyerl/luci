package actions;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;
import org.luci.comm.action.Action;
import org.luci.comm.clienthandlers.ClientHandler;
import org.luci.connect.LcInputStream;
import org.luci.connect.LcJSONObject;
import org.luci.connect.LcMetaJSONObject;
import org.luci.connect.LcOutputStream;
import org.luci.factory.observer.FactoryObserver;
import org.luci.factory.observer.IDatabase;
import org.luci.utilities.TAB;
import org.luci.utilities.TimeRangeSelection;

public class delete extends Action {

	public delete(FactoryObserver o, ClientHandler h, LcJSONObject json) {
		super(o, h, json);
	}

	@Override
	public LcMetaJSONObject getInputIndex() {
		String inputjson = ""
				+ "{	'action':'"+this.getClass().getSimpleName()+"',"
				+ "		'XOR ScID':'number',"
				+ "		'XOR SObjID':'number',"
				+ "		'XOR trash':'boolean',"
				+ "		'OPT attributes':'list',"
				+ "		'OPT geomIDs':'list',"
				+ "		'OPT timerange':'timerange',"
				+ "		'OPT history':'boolean',"
				+ "		'subject':'Service Instance (SObjID) XOR Scenario'"
				+ "}";
		return new LcMetaJSONObject(inputjson);
	}

	@Override
	public JSONObject perform(List<LcInputStream> inputStreams, List<LcOutputStream> outputStreams, 
			Set<String> unknownKeys) throws Exception{
		int ScID = request.optInt("ScID", 0);
		long SObjID = request.optLong("SObjID", 0);
		boolean history = request.optBoolean("history", false);
		boolean trash = request.optBoolean("trash", false);
		Connection con = o.getDatabaseConnection();
		IDatabase db = o.dataModelImplementation();
		Timestamp t_deleted = new Timestamp(new Date().getTime());
		TimeRangeSelection trs = TimeRangeSelection.NULL();
		if (request.has("timerange")) trs = new TimeRangeSelection(request.getJSONObject("timerange"));
		JSONObject result = new JSONObject();
		try {
			if (request.has("ScID")){
				result.put("ScID", ScID);
				if (request.has("attributes")){
					JSONArray atts = request.getJSONArray("attributes");
					String[] ats = new String[atts.length()];
					for (int i = 0; i < atts.length(); i++) ats[i] = atts.getString(i);
					int j = db.dropColumns(con, ScID, ats);
					if (j > 0) result.put("message", "Successfully removed '"+j+"' attributes.");
					else result.put("message", "No attributes have been removed.");
				} else {
					String tab = TAB.getScenarioTable(ScID, TAB.SCN);
					String hst = TAB.getScenarioTable(ScID, TAB.SCN_HISTORY);
					String g = (request.has("geomIDs")) ? " AND geomID = ? " : "";
					
					String sql;
					if (history){
						sql = "DELETE * FROM "+hst+" WHERE 1=1 "+g+trs.getSQL()+";";
//								+ "DELETE * FROM "+tab+" WHERE 1=1 "+g+trs.getSQL()+";";
					} else {
						sql = "INSERT INTO "+hst+" SELECT ? , * FROM "+tab+" WHERE 1=1 "+g+trs.getSQL()+";"
								+ "DELETE FROM "+tab+" WHERE 1=1 "+g+trs.getSQL()+";";
					}
					PreparedStatement ps = con.prepareStatement(sql);
					if (request.has("geomIDs")){
						JSONArray geomIDs = request.getJSONArray("geomIDs");
						
						for (int i = 0; i < geomIDs.length(); i++){
							int j = 1;
							if (history){
								ps.setLong(j++, geomIDs.getLong(i));
								trs.setTimestampSQL(ps, j);
							} else {
								ps.setTimestamp(j++, t_deleted);
								ps.setLong(j++, geomIDs.getLong(i));
								j = trs.setTimestampSQL(ps, j);
								ps.setLong(j++, geomIDs.getLong(i));
								j = trs.setTimestampSQL(ps, j);
							}
							ps.addBatch();
						}
						ps.executeBatch();
						result.put("message", "Successfully deleted '"+geomIDs.length()+"' geometry elements");
					} else {
						if (history) trs.setTimestampSQL(ps, 1);
						else {
							ps.setTimestamp(1, t_deleted);
							int j = trs.setTimestampSQL(ps, 2);
							trs.setTimestampSQL(ps, j);
						}
						
						ps.executeUpdate();
						sql = "UPDATE "+TAB.getTableName(TAB.SCENARIOS)+" SET flag = bitor(flag,?) WHERE id = ?";
						ps = con.prepareStatement(sql);
						ps.setLong(1, TAB.flagDELETED);
						ps.setInt(2, ScID);
						ps.execute();
						if (history) result.put("message", "History entries successfully removed.");
						else result.put("message", "Scenario successfully trashed. History remains.");
					}
				}
			} else if (request.has("SObjID")){
				String sin = TAB.getTableName(TAB.SERVICEINPUTS);
				String sout = TAB.getTableName(TAB.SERVICEOUTPUTS);
				String sinh = TAB.getTableName(TAB.SRV_IN_HISTORY);
				String south = TAB.getTableName(TAB.SRV_OUT_HISTORY);
				String sql;
				if (history){
					sql = "DELETE * FROM "+sinh+" WHERE sobjid = ? "+trs.getSQL()+";"
//							+ "DELETE * FROM "+sin+" WHERE sobjid = ? "+trs.getSQL()+";"
						+ "DELETE * FROM "+south+" WHERE sobjid = ? "+trs.getSQL()+";";
//							+ "DELETE * FROM "+sout+" WHERE sobjid = ? "+trs.getSQL();
				} else {
					sql = "INSERT INTO "+sinh+" SELECT ?, * FROM "+sin+" WHERE sobjid = ? "+trs.getSQL()+";"
							+ "DELETE * FROM "+sin+" WHERE sobjid = ? "+trs.getSQL()+";"
						+ "INSERT INTO "+south+" SELECT ?, * FROM "+sout+" WHERE sobjid = ? "+trs.getSQL()+";"
							+ "DELETE * FROM "+sout+" WHERE sobjid = ? "+trs.getSQL();
				}
				PreparedStatement ps = con.prepareStatement(sql);
				
				if (!history) {
					for (int i = 0, j = 1; i < 2; i++){
						ps.setLong(j++, SObjID);
						j = trs.setTimestampSQL(ps, j);
					}
				} else {
					for (int i = 0, j = 1; i < 2; i++){
						ps.setTimestamp(j++, t_deleted);
						ps.setLong(j++, SObjID);
						j = trs.setTimestampSQL(ps, j);
						ps.setLong(j++, SObjID);
						j = trs.setTimestampSQL(ps, j);
					}
				}
				ps.execute();
			} else if (trash){
				String tab = TAB.getTableName(TAB.SCENARIOS);
				String sql = "SELECT id FROM "+tab+" WHERE bitand(flag, ?) = 1 AND STATUS = 'INIT'";
				PreparedStatement ps = con.prepareStatement(sql);
				ps.setLong(1, TAB.flagDELETED);
				ResultSet rs = ps.executeQuery();
				JSONArray scids = new JSONArray();
				while(rs.next()){
					int SCID = rs.getInt(1);
					db.dropScenario(con, SCID);
					scids.put(SCID);
				}
				if (scids.length() > 0){
					result.put("ScIDs", scids);
					result.put("message", "Deleted "+scids.length()+" scenarios permanently!");
					result.put("TODO", "implement a procedure to delete trashed service instances");
				}
				result.put("ScIDrestart", db.updateAutoIDSequence(con, tab, "id"));
			}
		} catch (SQLException e){
//			e.getNextException().printStackTrace();
			con.close();
			throw e;
		}
		
		con.close();
		
		if (result.length() > 0) return new JSONObject().put("result", result);
		else return new JSONObject().put("error", "Nothing deleted");
	}

	@Override
	public LcMetaJSONObject getOutputIndex() {
		String outputjson = ""
				+ "	{'XOR result': 'string',"
				+ "	 'XOR error': ['json','list','string']"
				+ "}";
		return new LcMetaJSONObject(outputjson);
	}

}
