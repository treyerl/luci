package actions;

import java.util.List;
import java.util.Set;

import org.json.JSONObject;
import org.luci.comm.clienthandlers.ClientHandler;
import org.luci.connect.LcInputStream;
import org.luci.connect.LcJSONObject;
import org.luci.connect.LcMetaJSONObject;
import org.luci.connect.LcOutputStream;
import org.luci.factory.observer.FactoryObserver;

import actions.common.GetScenario;
import actions.common.GetUser;

public class exists extends GetScenario {
	
	public exists(FactoryObserver o, ClientHandler h, LcJSONObject json) {
		super(o, h, json);
	}

	@Override
	public LcMetaJSONObject getInputIndex() {
		String inputjson = ""
				+ "{	'action':'"+this.getClass().getSimpleName()+"',"
				+ "		'XOR username':'string',"
				+ "		'XOR servicename':'string',"
				+ "		'XOR scenarioname':'string',"
				+ "		'XOR actionname':'string',"
				+ "		'XOR ScID':'number',"
				+ "		'subject':'Username XOR Servicename XOR Scenarioname'"
				+ "}";
		return new LcMetaJSONObject(inputjson);
	}

	@Override
	public JSONObject perform(List<LcInputStream> inputStreams, List<LcOutputStream> outputStreams, 
			Set<String> unknownKeys) throws Exception{
		con = o.getDatabaseConnection();
		if (this.request.has("username")){
			GetUser au = new authenticate(o, h, request);
			List<String> users = au.getUsernameList();
			con.close();
			if (users == null) return new JSONObject("{'error':'SQLException while getting userlist.'}");
			if (users.contains(this.request.getString("username"))){
				return new JSONObject("{'result':true}");
			} else {
				return new JSONObject("{'result':false}");
			}
		}
		if (this.request.has("scenarioname")){
			List<String> scns = getScenarioNameList();
			con.close();
			if (scns == null) return new JSONObject("{'error':'SQLException while getting scenario list.'}");
			if (scns.contains(this.request.getString("scenarioname"))){
				return new JSONObject("{'result':true}");
			} else {
				return new JSONObject("{'result':false}");
			}
		}
		if (this.request.has("ScID")){
			List<Long> ids = getScenarioIDList();
			con.close();
			if (ids == null) return new JSONObject("{'error':'SQLException while getting scenario list.'}");
			if (ids.contains(this.request.getLong("ScID"))){
				return new JSONObject("{'result':true}");
			} else {
				return new JSONObject("{'result':false}");
			}
		}
		if (this.request.has("servicename")){
			Set<String> services = o.getServiceSet();
			con.close();
			if (services.contains(this.request.getString("servicename"))){
				return new JSONObject("{'result':true}");
			} else {
				return new JSONObject("{'result':false}");
			}
		}
		if (this.request.has("actionname")){
			List<String> actions = this.h.getAllowedActions();
			con.close();
			if (actions.contains(this.request.getString("actionname"))){
				return new JSONObject("{'result':true}");
			} else {
				return new JSONObject("{'result':false}");
			}
		}
		con.close();
		return null;
	}

	@Override
	public LcMetaJSONObject getOutputIndex() {
		String outputjson = ""
				+ "	{'XOR result': 'boolean',"
				+ "	 'XOR error': ['json','list','string']"
				+ "}";
		return new LcMetaJSONObject(outputjson);
	}

}
