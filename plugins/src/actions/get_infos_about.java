package actions;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;
import org.luci.Luci;
import org.luci.comm.clienthandlers.ClientHandler;
import org.luci.connect.LcInputStream;
import org.luci.connect.LcJSONObject;
import org.luci.connect.LcMetaJSONObject;
import org.luci.connect.LcOutputStream;
import org.luci.factory.ActionFactory;
import org.luci.factory.observer.FactoryObserver;

import actions.common.GetScenario;

public class get_infos_about extends GetScenario {
	
	public get_infos_about(FactoryObserver o, ClientHandler h, LcJSONObject json) {
		super(o, h, json);
	}

	@Override
	public LcMetaJSONObject getInputIndex() {
		String inputjson = ""
				+ "{	'action':'"+this.getClass().getSimpleName()+"',"
				+ "		'OPT my':['server','actions'],"
				+ "		'OPT servicename':'string',"
				+ "		'OPT actionname':'string',"
				+ "		'OPT scenarioname':'string',"
				+ "		'OPT SObjID':'number',"
				+ "		'OPT ScID':'number',"
				+ "		'OPT uID':'number',"
				+ "		'subject':'(my server XOR actions) AND/OR servicename AND/OR actionname AND/OR Service Instance (SObjID) AND/OR Scenario (ScID) AND/OR User (UID)'"
				+ "}";
		return new LcMetaJSONObject(inputjson);
	}

	@Override
	public JSONObject perform(List<LcInputStream> inputStreams, List<LcOutputStream> outputStreams, 
			Set<String> unknownKeys) throws Exception{
		con = o.getDatabaseConnection();
		
		JSONObject result = new JSONObject();
		if (request.has("my")){
			JSONArray mya = this.request.getJSONArray("my");
			List<String> my = new ArrayList<String>();
			for (int i = 0; i < mya.length(); i++ ){
				my.add(mya.getString(i));
			}
			if (my.contains("server")){
				JSONObject server = new JSONObject();
				try {
					String mosq = Luci.getMosquittoInfo(new File(new File(Luci.getMosquittoBin()).getParentFile(), "versionOutput.txt"));
					String inexternal = Luci.getConfig().getProperty("mqtt_broker", "INTERNAL");
					String mqttAddress = Luci.getConfig().getProperty("mqtt_broker_address", "tcp://localhost:1883");
					server.put("mqtt-broker", new JSONObject().put("version", mosq).put("running", inexternal).put("address", mqttAddress));
				} catch (Exception e){
					server.put("mqtt-broker", "EXTERNAL");
				}

				server.put("luci", new JSONObject().put("version", Luci.getVersion())
						.put("port", Luci.getConfig().getProperty("portNo")));
				server.put("database", o.getDbInfo());
				result.put("server", server);
			}
			if (my.contains("actions")){
				List<String> actionkeys = this.h.getAllowedActions();
				JSONObject actions = new JSONObject();
				for (String akey: actionkeys){
					ActionFactory a = o.getActionFactory(akey);
					actions.put(akey, a.getIndexDescription());
				}
				result.put("actions", actions);
			}
		}
		if (request.has("servicename")){
			String servicename = request.getString("servicename");
			result.put(servicename, getServiceFactoryInfo(servicename));
		}
		if (request.has("actionname")){
			String actionname = request.getString("actionname");
			ActionFactory a = o.getActionFactory(actionname);
			if (a != null){
				JSONObject i = a.getInputIndex();
				JSONObject inputs = new JSONObject(i, JSONObject.getNames(i));
				inputs.remove("action");
				inputs.remove("subject");
				inputs.remove("comment");
				result.put(actionname,inputs);
				// TODO: get infos about actionname: also print outputIndex
			} else {
				result.put(actionname, "No action '"+actionname+"' available!");
			}
		}
		if (this.request.has("scenarioname")){
			ScID = resolveScenarioName(request.getString("scenarioname"));
			result = getScenarioInfo();
		}
		if (this.request.has("SObjID")){
			SObjID = this.request.getLong("SObjID");			
			result = getServiceInstanceInfo();
		}
		if (this.request.has("ScID")){
			result = getScenarioInfo();
		}		
		if (this.request.has("user")){
			String username = this.request.getString("user");
			result.put(username, "Not Implemented Yet");
		}
		con.close();
		return new JSONObject().put("result", result);
	}

	@Override
	public LcMetaJSONObject getOutputIndex() {
		String outputjson = ""
				+ "	{'XOR result': 'json',"
				+ "	 'XOR error': ['json','list','string']"
				+ "}";
		return new LcMetaJSONObject(outputjson);
	}

}
