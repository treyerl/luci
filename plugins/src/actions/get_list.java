package actions;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;
import org.luci.comm.clienthandlers.ClientHandler;
import org.luci.connect.LcInputStream;
import org.luci.connect.LcJSONObject;
import org.luci.connect.LcMetaJSONObject;
import org.luci.connect.LcOutputStream;
import org.luci.factory.observer.FactoryObserver;

import actions.common.GetScenario;
import actions.common.GetUser;

public class get_list extends GetScenario {
	
	public get_list(FactoryObserver o, ClientHandler h, LcJSONObject json) {
		super(o, h, json);
	}
	
	@Override
	public LcMetaJSONObject getInputIndex() {
		String inputjson = ""
				+ "{"
				+ "		'action':'"+this.getClass().getSimpleName()+"',"
				+ "		'of':['actions','scenarios','services','converters','users','scenarioInfos'],"
				+ "		'subject':'Actions AND/OR Services AND/OR Scenarios AND/OR Users'"
				+ "}";
		return new LcMetaJSONObject(inputjson);
	}

	@Override
	public JSONObject perform(List<LcInputStream> inputStreams, List<LcOutputStream> outputStreams, 
			Set<String> unknownKeys) throws Exception{
		GetUser gu = new authenticate(o, h, request);
		con = o.getDatabaseConnection();
		JSONArray jargs = this.request.getJSONArray("of");
		List<String> args = new ArrayList<String>();
		for (int i = 0; i < jargs.length(); i++){
			args.add(jargs.getString(i));
		}
		
		JSONObject result = new JSONObject();
		if (args.contains("actions")){
			result.put("actions", this.h.getAllowedActions());
		}
		if (args.contains("services")){
			result.put("services", o.getServiceSet());
		}
		if (args.contains("converters")){
			result.put("converters", o.getConverterSet());
		}
		
		if (args.contains("scenarios")){
			List<Long> scs = getScenarioIDList() ;
			if (scs != null){
				result.put("scenarios", scs);
			} else {
				con.close();
				return new JSONObject().put("error", "SQLException while getting scenario list.");
			}
		}
		if (args.contains("users")){
			if (this.h.isAuthenticated()){
				List<String> usrs = gu.getUsernameList();
				if (usrs != null){
					result.put("users", usrs);
				} else {
					con.close();
					return new JSONObject().put("error", "SQLException while getting user list.");

				}
			} else {
				result.put("users", "Not authenticated. Please log in first.");
			}
		}
		
		if (args.contains("scenarioInfos")){
			JSONObject scs = getScenarioList() ;
			if (scs != null){
				result.put("scenarioInfos", scs);
			} else {
				con.close();
				return new JSONObject().put("error", "SQLException while getting scenario list.");
			}
		}
		con.close();
		return new JSONObject().put("result", result);
	}

	@Override
	public LcMetaJSONObject getOutputIndex() {
		String outputjson = ""
				+ "	{'XOR result': {"
				+ "		'OPT actions':'list',"
				+ "		'OPT scenarios':'list',"
				+ "		'OPT services':'list',"
				+ "		'OPT converters':'list',"
				+ "		'OPT users':'list',"
				+ "		'OPT scenarioInfos':'list'"
				+ "	},"
				+ "	 'XOR error': ['json','list','string']"
				+ "}";
		return new LcMetaJSONObject(outputjson);
	}
}
