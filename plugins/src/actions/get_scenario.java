package actions;

import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import org.json.JSONException;
import org.json.JSONObject;
import org.luci.comm.action.IGetScenario;
import org.luci.comm.clienthandlers.ClientHandler;
import org.luci.connect.LcInputStream;
import org.luci.connect.LcJSONObject;
import org.luci.connect.LcMetaJSONObject;
import org.luci.connect.LcOutputStream;
import org.luci.factory.observer.FactoryObserver;
import org.luci.utilities.TimeRangeSelection;

import actions.common.GetScenario;

public class get_scenario extends GetScenario implements IGetScenario{
	
	public get_scenario(FactoryObserver o, ClientHandler h, LcJSONObject json) {
		super(o, h, json);
	}

	@Override
	public LcMetaJSONObject getInputIndex() {
		String inputjson = ""
				+ "{	'action':'"+this.getClass().getSimpleName()+"',"
				+ "		'XOR ScID':number,"
				+ "		'XOR scenarioname':'string',"
				+ "		'OPT geomIDs':'list',"
				+ "		'OPT batchID':'number',"
				+ "		'OPT attributeConstraints':'json',"
				+ "		'OPT orderBy':'list',"
				+ "		'OPT offset':'number',"
				+ "		'OPT limit':'number',"
				+ "		'OPT format_request':'string',"
				+ "		'OPT crs':'string',"
				+ "		'OPT timerange':'timerange',"
				+ "		'OPT layer':'string',"
				+ "		'OPT dimensions':'number',"
				+ "		'OPT switchLatLon':'boolean',"
				+ "		'OPT withAttachments':'boolean',"
				+ "		'OPT history':'boolean',"
				+ "		'OPT version':'string',"
				+ "		'subject':'Scenario (ScID)',"
				+ "		'comment':'timerange.all defaults to false = \"newest\"; crs like EPSG:1234; attributeConstraints = KeyValuePair<String,String>'"
				+ "}";
		return new LcMetaJSONObject(inputjson);
	}

	@Override
	public JSONObject perform(List<LcInputStream> inputStreams,List<LcOutputStream> outputStreams, 
			Set<String> unknownKeys) throws SQLException, JSONException, InstantiationException, 
			FileNotFoundException, InterruptedException, ExecutionException  {
		con = o.getDatabaseConnection();
		if (request.has("ScID")) ScID = request.getInt("ScID");
		else ScID = resolveScenarioName(request.getString("scenarioname"));
		
		if (request.has("timerange")) trs = new TimeRangeSelection(request.getJSONObject("timerange"));
		try {
			JSONObject result = new JSONObject().put("result", getScenarioGeometry(outputStreams));
			con.close();
			return result;
		} catch (SQLException e){
			con.close();
			throw e;
		}
	}

	@Override
	public LcMetaJSONObject getOutputIndex() {
		String outputjson = ""
				+ "	{'XOR result': {"
				+ "		'geometry':{"
				+ "			'KEYNAME':['streaminfo','geometry'],"
				+ "			'OPT attachments':'list'"
				+ "		},"
				+ "		'ScID':'number',"
				+ "		'OPT timerange':'timerange',"
				+ "		'OPT crs':'string',"
				+ "		'OPT errors':'list'"
				+ "	},"
				+ "	 'XOR error': ['json','list','string']"
				+ "}";
		return new LcMetaJSONObject(outputjson);
	}

}
