package actions;

import java.util.List;
import java.util.Set;

import org.json.JSONObject;
import org.luci.comm.action.IGetServiceInputs;
import org.luci.comm.clienthandlers.ClientHandler;
import org.luci.connect.LcInputStream;
import org.luci.connect.LcJSONObject;
import org.luci.connect.LcMetaJSONObject;
import org.luci.connect.LcOutputStream;
import org.luci.factory.observer.FactoryObserver;
import org.luci.services.ServiceCall;
import org.luci.utilities.TimeRangeSelection;

import actions.common.GetServiceInputs;

public class get_service_inputs extends GetServiceInputs implements IGetServiceInputs{
	
	public get_service_inputs(FactoryObserver o, ClientHandler h, LcJSONObject json) {
		super(o, h, json);
	}

	@Override
	public LcMetaJSONObject getInputIndex() {
		String inputjson = ""
				+ "{	'action':'"+this.getClass().getSimpleName()+"',"
				+ "		'SObjID':number,"
				+ "		'OPT get_subscriptions':'boolean',"
				+ "		'OPT timerange':'timerange',"
				+ "		'subject':'Service Instance (SObjID)',"
				+ "		'comment':'timerange.all defaults to false = \"newest\".'"
				+ "}";
		return new LcMetaJSONObject(inputjson);
	}

	@Override
	public JSONObject perform(List<LcInputStream> inputStreams, List<LcOutputStream> outputStreams, 
			Set<String> unknownKeys) throws Exception {
		con = o.getDatabaseConnection();
		SObjID = request.getLong("SObjID");
		if (request.has("timerange")){
			trs = new TimeRangeSelection(request.getJSONObject("timerange"));
		} else {
			ServiceCall call = new ServiceCall(SObjID, h.getUser().getUid(), o);
			trs = new TimeRangeSelection().setUntil(call.getTimestamp());
		}
		LcJSONObject result = getServiceInstanceInfo();
		result.put("inputs", getServiceInputs(
				request.optBoolean("get_subscriptions", true), null, outputStreams));
		con.close();
		return result;
	}
	
	@Override
	public LcMetaJSONObject getOutputIndex() {
		String outputjson = ""
				+ "	{'XOR result': {"
				+ "		'classname':'string',"
				+ "		'status':'string',"
				+ "		'ScID':'number',"
				+ "		'mqtt_topic':'string',"
				+ "		'uID':'number',"
				+ "		'timestamp':'number',"
				+ "		'OPT errors':'list',"
				+ "		'OPT subscriptions':'list',"
				+ "		'OPT inputs':'json'"
				+ "	},"
				+ "	 'XOR error': ['json','list','string']"
				+ "}";
		return new LcMetaJSONObject(outputjson);
	}


}
