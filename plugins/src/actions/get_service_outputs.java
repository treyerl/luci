package actions;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

import org.json.JSONObject;
import org.luci.comm.clienthandlers.ClientHandler;
import org.luci.connect.LcInputStream;
import org.luci.connect.LcJSONObject;
import org.luci.connect.LcMetaJSONObject;
import org.luci.connect.LcOutputStream;
import org.luci.factory.observer.FactoryObserver;
import org.luci.utilities.TimeRangeSelection;

import actions.common.GetServiceOutputs;

public class get_service_outputs extends GetServiceOutputs {
	
	public get_service_outputs(FactoryObserver o, ClientHandler h, LcJSONObject json) throws SQLException {
		super(o, h, json);
	}

	@Override
	public LcMetaJSONObject getInputIndex() {
		String inputjson = ""
				+ "{	'action':'"+this.getClass().getSimpleName()+"',"
				+ "		'SObjID':number,"
				+ "		'OPT format_request':{'KEYNAME':'FORMAT'},"
				+ "		'OPT input_hash':string,"
				+ "		'OPT timerange':'timerange',"
				+ "		'OPT iteration':'number',"
				+ "		'subject':'Service Instance (SObjID)',"
				+ "		'comment':'timerange.all defaults to false = \"newest\". iteration 0 = all iterations'"
				+ "}";
		return new LcMetaJSONObject(inputjson);
	}

	@Override
	public JSONObject perform(List<LcInputStream> inputStreams,List<LcOutputStream> outputStreams, 
			Set<String> unknownKeys) throws Exception {
		con = o.getDatabaseConnection();
		SObjID = request.getLong("SObjID");
		try {
			if (has(SObjID)){
				if (request.has("input_hash")) input_hash = new Timestamp(request.getLong("input_hash"));
				else input_hash = getLatestCall(SObjID);
				if (request.has("timerange")) trs = new TimeRangeSelection(request.getJSONObject("timerange"));
				LcJSONObject outputs = getServiceOutputs(outputStreams);
				return new JSONObject().put("result", new JSONObject().put("outputs", outputs));
			} else {
				return new JSONObject().put("error", "No service instance with "+SObjID);
			}
		} catch (SQLException e){
			if (con != null) con.close();
			throw e;
		} finally {
			if (con != null) con.close();
		}
	}
	
	@Override
	public LcMetaJSONObject getOutputIndex() {
		String outputjson = ""
				+ "	{'XOR result': {"
				+ "		'classname':'string',"
				+ "		'status':'string',"
				+ "		'ScID':'number',"
				+ "		'mqtt_topic':'string',"
				+ "		'uID':'number',"
				+ "		'timestamp':'number',"
				+ "		'OPT errors':'list',"
				+ "		'OPT subscriptions':'list',"
				+ "		'OPT outputs':'json'"
				+ "	},"
				+ "	 'XOR error': ['json','list','string']"
				+ "}";
		return new LcMetaJSONObject(outputjson);
	}
}
