package actions;

import java.util.List;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;
import org.luci.comm.clienthandlers.ClientHandler;
import org.luci.connect.LcInputStream;
import org.luci.connect.LcJSONObject;
import org.luci.connect.LcMetaJSONObject;
import org.luci.connect.LcOutputStream;
import org.luci.factory.observer.FactoryObserver;

import actions.common.GetService;

public class get_status_of extends GetService {
	
	public get_status_of(FactoryObserver o, ClientHandler h, LcJSONObject json) {
		super(o, h, json);
	}

	@Override
	public LcMetaJSONObject getInputIndex() {
		String inputjson = ""
				+ "{	'action':'"+this.getClass().getSimpleName()+"',"
				+ "		'SObjID':number,"
				+ "		'subject':'Service Instance (SObjID)'"
				+ "}";
		return new LcMetaJSONObject(inputjson);
	}

	@Override
	public JSONObject perform(List<LcInputStream> inputStreams, List<LcOutputStream> outputStreams, 
			Set<String> unknownKeys) throws Exception{
	
		long SObjID = this.request.getLong("SObjID");
		try{
			return new JSONObject().put("result", getServiceStatus(SObjID));
		} catch (Exception e) {
			return new JSONObject().put("error", new JSONArray().put(e.toString()));
		}
	}

	@Override
	public LcMetaJSONObject getOutputIndex() {
		String outputjson = ""
				+ "	{'XOR result': 'string',"
				+ "	 'XOR error': ['json','list','string']"
				+ "}";
		return new LcMetaJSONObject(outputjson);
	}

}
