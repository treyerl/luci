package actions;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.json.JSONObject;
import org.luci.Luci;
import org.luci.comm.clienthandlers.ClientHandler;
import org.luci.connect.LcInputStream;
import org.luci.connect.LcJSONObject;
import org.luci.connect.LcMetaJSONObject;
import org.luci.connect.LcOutputStream;
import org.luci.factory.observer.FactoryObserver;
import org.luci.utilities.TAB;

import actions.common.GetScenario;

public class get_subscribers_of extends GetScenario {
	
	public get_subscribers_of(FactoryObserver o, ClientHandler h, LcJSONObject json) {
		super(o, h, json);
	}

	@Override
	public LcMetaJSONObject getInputIndex() {
		String inputjson = ""
				+ "{	'action':'"+this.getClass().getSimpleName()+"',"
				+ "		'XOR service':number,"
				+ "		'XOR scenario':'string',"
				+ "		'subject':'Service XOR Scenario'"
				+ "}";
		return new LcMetaJSONObject(inputjson);
	}

	@Override
	public JSONObject perform(List<LcInputStream> inputStreams, List<LcOutputStream> outputStreams, 
			Set<String> unknownKeys) throws Exception{
		Connection con = o.getDatabaseConnection();
//		GetService gs = new GetService(o);
//		GetScenario gsc = new GetScenario(o);
		String mqtt = Luci.getConfig().getProperty("mqtt_pre", "/Luci/");
		String sql = "";
		String t = TAB.getTableName(TAB.SERVICEINPUTS);
		
		if (this.request.has("service")){
			long SObjID = this.request.getLong("service");
			long scid = getServiceScID(SObjID);
			mqtt = mqtt + scid + "/" +SObjID;
			sql = "SELECT sobjid FROM "+t+" WHERE output_subscription = "+mqtt+";";
		}
		
		if (this.request.has("scenario")){
			long scid = resolveScenarioName(this.request.getString("scenario"));
			mqtt = mqtt + scid+"/";
			sql = "SELECT sobjid FROM "+t+" WHERE output_subscription = "+mqtt+";";	
		}
		PreparedStatement ps = con.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		Set<Long> subscribers = new HashSet<Long>();
		while(rs.next()){
			subscribers.add(rs.getLong(1));
		}
		return new JSONObject().put("result", subscribers);
	}

	@Override
	public LcMetaJSONObject getOutputIndex() {
		String outputjson = ""
				+ "	{'XOR result': 'list',"
				+ "	 'XOR error': ['json','list','string']"
				+ "}";
		return new LcMetaJSONObject(outputjson);
	}

}
