package actions;

import java.util.List;
import java.util.Set;

import org.json.JSONObject;
import org.luci.comm.action.Action;
import org.luci.comm.action.ISwitchClientHandler;
import org.luci.comm.clienthandlers.ClientHandler;
import org.luci.comm.clienthandlers.PublicClientHandler;
import org.luci.comm.clienthandlers.RemoteServiceClientHandler;
import org.luci.connect.LcInputStream;
import org.luci.connect.LcJSONObject;
import org.luci.connect.LcMetaJSONObject;
import org.luci.connect.LcOutputStream;
import org.luci.factory.observer.FactoryObserver;

public class logout extends Action implements ISwitchClientHandler {

	public logout(FactoryObserver o, ClientHandler h, LcJSONObject json) {
		super(o, h, json);
	}

	@Override
	public LcMetaJSONObject getInputIndex() {
		return new LcMetaJSONObject("{'action':'logout','subject':'User'}");
	}

	@Override
	public LcMetaJSONObject getOutputIndex() {
		String outputjson = ""
				+ "	{'XOR result': 'boolean',"
				+ "	 'XOR error': ['json','list','string']"
				+ "}";
		return new LcMetaJSONObject(outputjson);
	}

	@Override
	public JSONObject perform(List<LcInputStream> inputStreams, List<LcOutputStream> outputStreams, 
			Set<String> unknownKeys) throws Exception{
		// atm no action required
		log.info(h.getUser().getUsername()+" logged out");
		return new JSONObject("{'result':true}");
	}

	@Override
	public boolean shouldSwitch() {
		// not really necessary; logout is not allowed for clients registered as a service
		if (h instanceof RemoteServiceClientHandler) return false;
		return true;
	}

	@Override
	public ClientHandler switchClientHandler() {
		return new PublicClientHandler(h.getSocket(), o, h.getClientHandlerObserver(), h.getID());
	}

}
