package actions;

import java.util.List;
import java.util.Set;

import org.json.JSONObject;
import org.luci.comm.action.Action;
import org.luci.comm.clienthandlers.ClientHandler;
import org.luci.comm.clienthandlers.RemoteServiceClientHandler;
import org.luci.connect.LcInputStream;
import org.luci.connect.LcJSONObject;
import org.luci.connect.LcMetaJSONObject;
import org.luci.connect.LcOutputStream;
import org.luci.factory.RemoteServiceFactoryThread;
import org.luci.factory.observer.FactoryObserver;
import org.luci.services.ServiceCall;

public class remote_cancel extends Action {
	
	public remote_cancel(FactoryObserver o, ClientHandler h, LcJSONObject json) {
		super(o, h, json);
	}

	@Override
	public LcMetaJSONObject getInputIndex() {
		return new LcMetaJSONObject("{'action':'remote_cancel','subject':'Service'}");
	}

	@Override
	public JSONObject perform(List<LcInputStream> inputStreams, List<LcOutputStream> outputStreams, 
			Set<String> unknownKeys) throws Exception{
		RemoteServiceClientHandler rsch = (RemoteServiceClientHandler) h;
		ServiceCall call = rsch.getServiceCall();
		if (call != null){
			RemoteServiceFactoryThread rsf = rsch.getFactory();
			rsf.cancelExecution(call);
			rsch.notifyRemoteService();
			return new JSONObject().put("result", call.getID()+" cancelled");
		} else {
			return new JSONObject().put("warning","Nothing to cancel!");
		}
	}

	@Override
	public LcMetaJSONObject getOutputIndex() {
		String outputjson = ""
				+ "	{'XOR result': 'string',"
				+ "	 'XOR error': ['json','list','string']"
				+ "}";
		return new LcMetaJSONObject(outputjson);
	}

}
