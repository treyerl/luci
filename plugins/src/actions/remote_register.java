package actions;

import java.util.List;
import java.util.Set;

import org.json.JSONObject;
import org.luci.comm.action.Action;
import org.luci.comm.action.ISwitchClientHandler;
import org.luci.comm.clienthandlers.ClientHandler;
import org.luci.comm.clienthandlers.RemoteServiceClientHandler;
import org.luci.connect.LcInputStream;
import org.luci.connect.LcJSONObject;
import org.luci.connect.LcMetaJSONObject;
import org.luci.connect.LcOutputStream;
import org.luci.factory.observer.FactoryObserver;

public class remote_register extends Action implements ISwitchClientHandler {
	private RemoteServiceClientHandler regHandler;
	
	public remote_register(FactoryObserver o, ClientHandler h, LcJSONObject json) {
		super(o, h, json);
	}
	
	@Override
	public LcMetaJSONObject getInputIndex() {
		String inputjson = ""
				+ "{	'action':'"+this.getClass().getSimpleName()+"',"
				+ "		'service':{"
				+ "			'classname':'string',"
				+ "			'version':'string',"
				+ "			'description':'string',"
				+ "			'OPT inputs':{"
				+ "				'KEYNAME':"+LcMetaJSONObject.jsontypes
				+ "			},"
				+ "			'OPT outputs':{"
				+ "				'KEYNAME':"+LcMetaJSONObject.jsontypes
				+ "			}"
				+ "		},"
				+ "		'machinename':'string',"
				+ "		'subject':'Service'"
				+ "}";
		return new LcMetaJSONObject(inputjson);
	}
	
	@Override
	public JSONObject perform(List<LcInputStream> inputStreams, List<LcOutputStream> outputStreams, 
			Set<String> unknownKeys) throws Exception{
		regHandler = o.registerRemoteService(h, request);
		String machinename = request.getString("machinename");
		JSONObject result = new JSONObject();
		long SObjID = request.optLong("SObjID", 0);
		String forID = "";
		if (SObjID != 0) forID = " for "+SObjID;
		result.put("message", "Service "+regHandler.getServiceName()+" [version:"+regHandler.getVersion()+"] successfully registered as "+machinename+forID);
		result.put("servicename", regHandler.getServiceName());
		result.put("version", regHandler.getVersion());
		return new JSONObject().put("result", result);
	}

	@Override
	public LcMetaJSONObject getOutputIndex() {
		String outputjson = ""
				+ "	{'XOR result': {"
				+ "		'message':'string',"
				+ "		'servicename':'string',"
				+ "		'version':'string'"
				+ "		},"
				+ "	 'XOR error': ['json','list','string']"
				+ "}";
		return new LcMetaJSONObject(outputjson);
	}

	@Override
	public boolean shouldSwitch() {
		return regHandler != null;
	}

	@Override
	public ClientHandler switchClientHandler() {
		return regHandler;
	}
}
