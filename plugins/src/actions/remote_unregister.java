package actions;

import java.util.List;
import java.util.Set;

import org.json.JSONException;
import org.json.JSONObject;
import org.luci.comm.action.Action;
import org.luci.comm.action.ISwitchClientHandler;
import org.luci.comm.clienthandlers.AuthenticatedClientHandler;
import org.luci.comm.clienthandlers.ClientHandler;
import org.luci.comm.clienthandlers.RemoteServiceClientHandler;
import org.luci.connect.LcInputStream;
import org.luci.connect.LcJSONObject;
import org.luci.connect.LcMetaJSONObject;
import org.luci.connect.LcOutputStream;
import org.luci.factory.observer.FactoryObserver;

public class remote_unregister extends Action implements ISwitchClientHandler{
	private AuthenticatedClientHandler ch;
	public remote_unregister(FactoryObserver o, ClientHandler h, LcJSONObject json) {
		super(o, h, json);
	}

	@Override
	public LcMetaJSONObject getInputIndex() {
		return new LcMetaJSONObject("{'action':'unregister','subject':'Service'}");
	}

	@Override
	public JSONObject perform(List<LcInputStream> inputStreams, List<LcOutputStream> outputStreams, 
			Set<String> unknownKeys) throws Exception{
		long SObjID = this.request.getLong("SObjID");
		String serviceName = this.request.getString("servicename");
		unknownKeys.remove("SObjID");
		unknownKeys.remove("servicename");
		if (SObjID == 0){
			RemoteServiceClientHandler rsch = (RemoteServiceClientHandler) h;
			long reservedSObjID = rsch.SObjIDreservation();
			String reservationText = "";
			if (reservedSObjID != 0) reservationText= " reserved for "+reservedSObjID;
			ch = new AuthenticatedClientHandler(h, o, h.getUser());
			/* THE ACTUAL DEREGISTRATION IS HANDLED IN THE BREAKEXCEPTION-HANDLER OF THE REMOTESERVICECLIENTHANDLER
			 * (IN ORDER TO UNREGISTER THE SERVICE ALSO IN CASE THE CONNECTION IS LOST)*/
			return new JSONObject().put("result", "Unregistered from '"+serviceName+reservationText+"'.");
		} else {
			throw new JSONException("Busy with service '"+SObjID+"'. Cancel first!");
		}
	}

	@Override
	public LcMetaJSONObject getOutputIndex() {
		String outputjson = ""
				+ "	{'XOR result':'string',"
				+ "	 'XOR error': ['json','list','string']"
				+ "}";
		return new LcMetaJSONObject(outputjson);
	}

	@Override
	public boolean shouldSwitch() {
		return ch != null;
	}

	@Override
	public ClientHandler switchClientHandler() {
		return ch;
	} 
}
