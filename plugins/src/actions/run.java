package actions;

import java.sql.SQLException;
import java.util.List;
import java.util.Set;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.json.JSONObject;
import org.luci.Luci;
import org.luci.comm.clienthandlers.ClientHandler;
import org.luci.connect.LcInputStream;
import org.luci.connect.LcJSONObject;
import org.luci.connect.LcMetaJSONObject;
import org.luci.connect.LcOutputStream;
import org.luci.factory.ServiceFactoryThread;
import org.luci.factory.observer.FactoryObserver;
import org.luci.services.ServiceCall;
import actions.common.SetService;

/**Shortcut function for create service instance and run service.
 * @author Lukas Treyer <treyer@arch.ethz.ch>
 *
 */
public class run extends SetService {
	private MqttClient mqtt;
	public run(FactoryObserver o, ClientHandler h, LcJSONObject json) throws SQLException {
		super(o, h, json);
		this.mqtt = o.getMqttClient();
	}
	
	@Override
	public LcMetaJSONObject getInputIndex() {
		String inputjson = ""
				+ "{	'action':'"+this.getClass().getSimpleName()+"',"
				+ "		'XOR ScID':number,"
				+ "		'XOR service':{"
				+ "			'classname':'string',"
				+ "			'OPT ScID':'number'," 
				+ "			'OPT inputs':{"
				+ "				'KEYNAME':"+LcMetaJSONObject.jsontypes
				+ "			},"
				+ "			'OPT format_request':{'KEYNAME':'FORMAT'},"
				+ "		},"
				+ "		'XOR SObjID':'number',"
				+ "		'OPT iteration':'number',"
				+ "		'subject':'Services XOR Scenarios',"
				+ "		'comment':'iteration denotes which iteration should be sent; 0 = all iterations "
				+ "			--- if run is sent with \"service\" the action runs synchronously, "
				+ "			which means that the service gets fired immediately and not through mqtt"
				+ "			and all binary attachments are directly forwarded to any remote nodes "
				+ "			(if the service is provided remotely)'"
				+ "}";
		return new LcMetaJSONObject(inputjson);
	}

	/**isSync == true means that the service gets fired immediately and not through mqtt 
	 * and all binary attachments are directly forwarded
	 */
	@Override
	public JSONObject perform(List<LcInputStream> inputStreams,List<LcOutputStream> outputStreams, 
			Set<String> unknownKeys) throws Exception {
		
		if (request.has("service")){
//			con = o.getDatabaseConnection();
			iteration = request.optInt("iteration");
			LcJSONObject service = new LcJSONObject(request.getJSONObject("service"));
			if (iteration > 0) service.put("iteration",iteration);
			request = service;
			String classname = request.getString("classname");
			JSONObject inputs = service.getJSONObject("inputs");
			int scId = 0;
			if (service.has("ScID"))
				scId = service.getInt("ScID");
			ServiceFactoryThread sf = o.getServiceFactory(classname);
			if (sf == null) throw new RuntimeException(classname+" not a service!");
			ServiceCall call = new ServiceCall(inputs, inputStreams, scId, h);
			long start = System.currentTimeMillis();
			Object outputs = sf.runAndWaitFor(call);
			long duration = System.currentTimeMillis() - start;
			LcJSONObject result = new LcJSONObject();
			result.put("classname", classname);
			result.put("status", "DONE");
			result.put("timestamp", call.getTimestamp());
			result.put("mqtt_topic", "NONE");
			result.put("ScID", scId);
			result.put("SObjID", 0);
			result.put("uID", call.getUID());
			result.put("outputs", outputs);
			result.put("duration", duration);
			List<LcOutputStream> callOutputStreams = call.getOutputStreams();
			if (callOutputStreams != null){
				for (LcOutputStream los: callOutputStreams){
					outputStreams.add(los);
				}
			}
//			new delete(o, h, request).trashService(call.getSObjID(), new JSONArray());
//			con.close();
			return new JSONObject().put("result", result);
		} else if (request.has("SObjID")) {
			SObjID = request.getLong("SObjID");
			iteration = request.optInt("iteration", 1);
			ServiceCall call = new ServiceCall(SObjID, h.getUser().getUid(), o);
			input_hash = call.getTimestamp();
			String className = getServiceName();
			ServiceFactoryThread sf = o.getServiceFactory(className);
			sf.runAndWaitFor(call);
			LcJSONObject result = getServiceInstanceInfo();
			result.put("outputs", getServiceOutputs(outputStreams));

			return new JSONObject().put("result", result);
			
		} else if (this.request.has("ScID")){
			String topic_pre = Luci.getConfig().getProperty("mqtt_pre", "/Luci/");
			String topic = topic_pre + this.request.getLong("ScID");
			mqtt.publish(topic, "RUN".getBytes("UTF8"), Luci.qos_0_at_most_once, false);
			return new JSONObject().put("result", topic + " RUN");
		}
		return null;
	}

	@Override
	public LcMetaJSONObject getOutputIndex() {
		String outputjson = ""
				+ "	{'XOR result':'string',"
				+ "	 'XOR error': ['json','list','string']"
				+ "}";
		return new LcMetaJSONObject(outputjson);
	}

}
