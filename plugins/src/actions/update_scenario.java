package actions;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;
import org.luci.comm.clienthandlers.ClientHandler;
import org.luci.connect.LcInputStream;
import org.luci.connect.LcJSONObject;
import org.luci.connect.LcMetaJSONObject;
import org.luci.connect.LcOutputStream;
import org.luci.factory.observer.FactoryObserver;

import actions.common.SetScenario;

public class update_scenario extends SetScenario {
	
	public update_scenario(FactoryObserver o, ClientHandler h, LcJSONObject json) {
		super(o, h, json);
	}
	
	@Override
	public LcMetaJSONObject getInputIndex() {
		String inputjson = ""
				+ "{	'action':'"+this.getClass().getSimpleName()+"',"
				+ "		'ScID':'number',"
				+ "		'OPT name':'string',"
				+ "		'OPT bbox':[['TL_LAT', 'TL_LON'], ['BR_LAT', 'BR_LON']],"
				+ "		'OPT geometry': {"
				+ "			'KEYNAME':['streaminfo','geometry']"
				+ "		},"
				+ "		'OPT switchLatLon':'boolean',"
				+ "		'subject':'Scenarios (ScID)',"
				+ "		'comment':'Coordinates in meters if no CRS is given; CRS formatted like EPSG:1234; "
				+ "				   KEYNAME: e.g. xy.shp, xy.dbf, or scenarioname.json etc.'"
				+ "}";
		return new LcMetaJSONObject(inputjson);
	}
	@Override
	public JSONObject perform(List<LcInputStream> inputStreams, List<LcOutputStream> outputStreams, 
			Set<String> unknownKeys) throws Exception{
		JSONArray errors = new JSONArray();
		List<Long> newIDs = new ArrayList<Long>();
		Timestamp t = updateScenario(inputStreams, newIDs, errors);
		if (errors.length() != 0) 
			return new JSONObject().put("error", errors.length() == 1 ? errors.get(0).toString() : errors.toString());
		else {
			JSONObject result = new JSONObject();
			result.put("message", "Scenario successfully updated!");
			result.put("new_ids", newIDs);
			result.put("timestamp", t.getTime());
			result.put("ScID", request.getLong("ScID"));
			return new JSONObject().put("result", result);
		}
	}

	@Override
	public LcMetaJSONObject getOutputIndex() {
		String outputjson = ""
				+ "	{'XOR result':'string',"
				+ "	 'XOR error': ['json','list','string']"
				+ "}";
		return new LcMetaJSONObject(outputjson);
	}
}
