package actions;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.json.JSONObject;
import org.luci.comm.clienthandlers.ClientHandler;
import org.luci.connect.LcInputStream;
import org.luci.connect.LcJSONObject;
import org.luci.connect.LcMetaJSONObject;
import org.luci.connect.LcOutputStream;
import org.luci.factory.observer.FactoryObserver;

import actions.common.SetService;

public class update_service_inputs extends SetService {
	
	public update_service_inputs(FactoryObserver o, ClientHandler h, LcJSONObject json) throws SQLException {
		super(o, h, json);
	}
	
	@Override
	public LcMetaJSONObject getInputIndex() {
		String inputjson = ""
				+ "{	'action':'"+this.getClass().getSimpleName()+"',"
				+ "		'SObjID':number,"
//				+ "		'OPT ScID':number,"
//				+ "		'OPT servicename':'string',"
				+ "		'inputs':{'KEYNAME':"+LcMetaJSONObject.jsontypes+"},"
				+ "		'subject':'Service Instance (SObjID)'"
				+ "}";
		return new LcMetaJSONObject(inputjson);
	}
	@Override
	public JSONObject perform(List<LcInputStream> inputStreams, List<LcOutputStream> outputStreams, 
			Set<String> unknownKeys) throws Exception{
		lastInput = inputStreams.get(inputStreams.size()-1);
		readAllInputStreamsToFile(inputStreams);
		SObjID = request.getLong("SObjID");
		ScID = request.optInt("ScID");
		flag = 0;
//		request.put("uid", this.h.getUser().getUid());
		con = o.getDatabaseConnection();
		ts = new Timestamp(new Date().getTime());
		updateServiceInputs();
		con.close();
		waitForAllInputStreamsToBeRead();
		return new JSONObject().put("result", "Output of service instance '"+SObjID+"' successfully updated!");
	}
	
	@Override
	public LcMetaJSONObject getOutputIndex() {
		String outputjson = ""
				+ "	{'XOR result':'string',"
				+ "	 'XOR error': ['json','list','string']"
				+ "}";
		return new LcMetaJSONObject(outputjson);
	}
}
