package actions;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;
import org.luci.comm.clienthandlers.ClientHandler;
import org.luci.connect.LcInputStream;
import org.luci.connect.LcJSONObject;
import org.luci.connect.LcMetaJSONObject;
import org.luci.connect.LcOutputStream;
import org.luci.factory.observer.FactoryObserver;

import actions.common.SetService;

public class update_service_outputs extends SetService{
	
	public update_service_outputs(FactoryObserver o, ClientHandler h, LcJSONObject json) throws SQLException {
		super(o, h, json);
	}
	
	@Override
	public LcMetaJSONObject getInputIndex() {
		List<String> outputTypes = new ArrayList<String>(LcMetaJSONObject.jsontypes);
		outputTypes.remove("geometry");
		String inputjson = ""
				+ "{	'action':'"+this.getClass().getSimpleName()+"',"
				+ "		'SObjID':number,"
				+ "		'input_hashcode':'number',"
				+ "		'remoteMachinename':'string',"
				+ "		'remoteVersion':'string',"
				+ "		'OPT iteration':'number',"
				+ "		'outputs':{'KEYNAME':"+outputTypes+"},"
				+ "		'subject':'Service Instance (SObjID)'"
				+ "}";
		return new LcMetaJSONObject(inputjson);
	}
	@Override
	public JSONObject perform(List<LcInputStream> inputStreams, List<LcOutputStream> outputStreams, 
			Set<String> unknownKeys) throws Exception{
		SObjID = request.getLong("SObjID");
		iteration = request.optInt("iteration", 1);
		input_hash = new Timestamp(request.getLong("input_hashcode"));
		request.put("uid", this.h.getUser().getUid());
		con = o.getDatabaseConnection();
		JSONArray errors = updateServiceOutputs(inputStreams);
		con.close();
		if (errors.length() > 0){
			return new JSONObject().put("error", errors);
		} else {
			return new JSONObject().put("result", "Output of service instance '"+SObjID+"' successfully updated!");
		}
	}
	
	@Override
	public LcMetaJSONObject getOutputIndex() {
		String outputjson = ""
				+ "	{'XOR result':'string',"
				+ "	 'XOR error': ['json','list','string']"
				+ "}";
		return new LcMetaJSONObject(outputjson);
	}
	
}
