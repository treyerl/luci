/**
 * The MIT License (MIT)
 *
 * Copyright (c) <year> <copyright holders>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package actions;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONObject;
import org.luci.comm.clienthandlers.ClientHandler;
import org.luci.connect.LcInputStream;
import org.luci.connect.LcJSONObject;
import org.luci.connect.LcMetaJSONObject;
import org.luci.connect.LcOutputStream;
import org.luci.factory.observer.FactoryObserver;
import org.luci.utilities.TAB;

import actions.common.SetService;

/**
 * 
 * @author Lukas Treyer
 *
 */
public class update_service_scenario extends SetService{

	/**
	 * @throws SQLException 
	 * 
	 */
	
	public update_service_scenario(FactoryObserver o, ClientHandler h, LcJSONObject json) throws SQLException {
		super(o, h, json);
	}

	/* (non-Javadoc)
	 * @see org.luci.factory.ITask#getInputIndex()
	 */
	@Override
	public LcMetaJSONObject getInputIndex() {
		String inputjson = ""
				+ "{	'action':'"+this.getClass().getSimpleName()+"',"
				+ "		'SObjID':'number',"
				+ "		'ScID':'number',"
				+ "		'subject':'Service Instance (SObjID)'"
				+ "}";
		return new LcMetaJSONObject(inputjson);
	}

	/* (non-Javadoc)
	 * @see org.luci.factory.ITask#getOutputIndex()
	 */
	@Override
	public LcMetaJSONObject getOutputIndex() {
		String outputjson = ""
				+ "	{'XOR result':'string',"
				+ "	 'XOR error': ['json','list','string']"
				+ "}";
		return new LcMetaJSONObject(outputjson);
	}

	/* (non-Javadoc)
	 * @see org.luci.comm.action.Action#perform(java.util.Set, java.util.List)
	 */
	@Override
	public JSONObject perform(List<LcInputStream> inputStreams, List<LcOutputStream> outputStreams, 
			Set<String> unknownKeys) throws Exception{
		SObjID = request.getLong("SObjID");
		ScID = request.getInt("ScID");
		long oldScID = 0;
		Set<String> oldSubscriptions = new HashSet<String>();
		String t = TAB.getTableName(TAB.SERVICES);
		con = o.getDatabaseConnection();
		
		String sql = "SELECT scid, service_subscriptions FROM "+t+" as s WHERE id = ? ";
//		System.out.println("1"+oldSubscriptions);
		try{
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setLong(1, SObjID);
			trs.setTimestampSQL(ps, 2);
			ResultSet rs = ps.executeQuery();
			if (rs.next()){
				oldScID = rs.getLong(1);
				String subs = rs.getString(2);
				if (subs != null){
					JSONArray oldSubs = new JSONArray(subs);
					for (int i = 0; i < oldSubs.length(); i++) 
						oldSubscriptions.add(oldSubs.getString(i));
				}
			}
		} catch (SQLException e){
			con.close();
			throw e;
		}
		if (oldScID != ScID){
//			System.out.println("2"+oldSubscriptions);
			String mqtt_pre = org.luci.Luci.getConfig().getProperty("mqtt_pre", "/Luci/");
			String myMqttTopic = mqtt_pre+ScID+"/"+SObjID;
			
			Set<String> newSubs = new HashSet<String>();
			newSubs.add(myMqttTopic);
			Pattern p = Pattern.compile(mqtt_pre+"\\d+\\/(\\d+)");
			for (String sub: oldSubscriptions){
				Matcher m = p.matcher(sub);
				newSubs.add(m.replaceFirst(mqtt_pre+ScID+"/$1"));
			}
			JSONArray newSubscriptions = new JSONArray(newSubs);
			System.out.println("3"+newSubscriptions);
			
			LcJSONObject up_sub = new LcJSONObject();
			up_sub.put("action", "update_service_subscriptions");
			up_sub.put("SObjID", SObjID);
			up_sub.put("run_on", newSubscriptions);
			try{
				update_service_subscriptions up_subs = 
						(update_service_subscriptions) o.getAction("update_service_subscriptions", h, up_sub);
				con.close();
				/* should not update; service factories are subscribed to /Luci/+/SObjID */
				JSONObject result = up_subs.perform(ScID, true /*mqtt*/);
				result.getJSONObject("result").put("oldScID", oldScID);
//				System.out.println(result);
				String myOldMqttTopic = mqtt_pre+oldScID+"/"+SObjID;
				publish(myOldMqttTopic+"/new_ScID", ""+ScID);
				return result;
			} catch (Exception e){
				e.printStackTrace();
				con.close();
				throw e;
			} 
		}
		JSONObject result = new JSONObject();
		result.put("newScID", ScID);
		result.put("oldScID", oldScID);
		result.put("message", "no change for ScID");
		return new JSONObject().put("result", result);
	}

}
