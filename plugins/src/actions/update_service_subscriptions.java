package actions;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;
import org.luci.Luci;
import org.luci.comm.action.Action;
import org.luci.comm.clienthandlers.ClientHandler;
import org.luci.connect.LcInputStream;
import org.luci.connect.LcJSONObject;
import org.luci.connect.LcMetaJSONObject;
import org.luci.connect.LcOutputStream;
import org.luci.factory.ServiceFactoryThread;
import org.luci.factory.observer.FactoryObserver;
import org.luci.utilities.TAB;

public class update_service_subscriptions extends Action {

	public update_service_subscriptions(FactoryObserver o, ClientHandler h, LcJSONObject json) {
		super(o, h, json);
	}

	@Override
	public LcMetaJSONObject getInputIndex() {
		String inputjson = ""
				+ "{ 'action':'"+getClass().getSimpleName()+"',"
				+ "		'SObjID':'number',"
				+ "		'run_on':'list',"
				+ "		'subject':'Services'"
				+ "}";
		return new LcMetaJSONObject(inputjson);
	}
	
	@Override
	public JSONObject perform(List<LcInputStream> inputStreams, List<LcOutputStream> outputStreams, 
			Set<String> unknownKeys) throws Exception{
		return perform(0, true);
	}
	
	protected JSONObject perform(long ScID, boolean inclMqtt) throws Exception {
		long SObjID = request.getLong("SObjID");
		JSONArray subscriptions = request.getJSONArray("run_on");
		Connection con = o.getDatabaseConnection();
		String t = TAB.getTableName(TAB.SERVICES);
		String sql = "INSERT INTO "+t+"(id, name, scid, status, uid, timestamp, service_subscriptions, flag)"
				+ " SELECT id, name, COALESCE(?,scid), status, uid, ?, ?, flag FROM "+t+" as s WHERE id = ?";
		
		JSONObject result = new JSONObject();
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			if (ScID != 0) ps.setLong(1, ScID);
			else ps.setNull(1, java.sql.Types.BIGINT);
			ps.setTimestamp(2, new Timestamp(new Date().getTime()));
			ps.setString(3, subscriptions.toString());
			ps.setLong(4, SObjID);

			ps.executeUpdate();
			ps = con.prepareStatement("SELECT name, scid FROM "+t+" AS s WHERE id = ? AND timestamp ");
			ps.setLong(1, SObjID);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				String serviceName = rs.getString(1);
				ServiceFactoryThread sf = o.getServiceFactory(serviceName);
				if (sf != null){ // = for remote services: only do something if the service factory is available
					if (ScID == 0) ScID = rs.getInt(2);
					Collection<String> newTopics = new ArrayList<String>();
					String mqtt_pre = Luci.getConfig().getProperty("mqtt_pre", "/Luci/");
					for (int i = 0; i < subscriptions.length(); i++){
						Object sub = subscriptions.get(i);
						if (sub instanceof Integer) newTopics.add(mqtt_pre+"+/"+ScID+"/"+sub);
						else if (sub instanceof String) newTopics.add((String) sub);				
					}
					if (inclMqtt) sf.updateNonBlockingAndMQTT(newTopics, SObjID);
					else sf.updateNonBlockingSubscriptions(newTopics, SObjID);
				} else {
					result.put("warning", "Service '"+serviceName+"' not available!");
				}
			}
		} catch (SQLException e){
			con.close();
			throw e;
		}
		con.close();
		
		if (ScID != 0) result.put("newScID", ScID);
		result.put("newTopics", subscriptions);
		return new JSONObject().put("result", result);
	}
	@Override
	public LcMetaJSONObject getOutputIndex() {
		String outputjson = ""
				+ "	{'XOR result':'string',"
				+ "	 'XOR error': ['json','list','string']"
				+ "}";
		return new LcMetaJSONObject(outputjson);
	}

}
