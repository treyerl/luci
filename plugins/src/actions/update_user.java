package actions;

import java.util.List;
import java.util.Set;

import org.json.JSONObject;
import org.luci.comm.clienthandlers.ClientHandler;
import org.luci.connect.LcInputStream;
import org.luci.connect.LcJSONObject;
import org.luci.connect.LcMetaJSONObject;
import org.luci.connect.LcOutputStream;
import org.luci.factory.observer.FactoryObserver;

import actions.common.SetUser;

public class update_user extends SetUser {

	public update_user(FactoryObserver o, ClientHandler h, LcJSONObject json) {
		super(o, h, json);
	}
	
	@Override
	public LcMetaJSONObject getInputIndex() {
		String inputjson = ""
				+ "{	'action':'"+this.getClass().getSimpleName()+"',"
				+ "		'uID':number,"
				+ "		'userdetails':{"
				+ "			'OPT username':'string',"
				+ "			'OPT email':'string',"
				+ "			'OPT passwd':'string',"
				+ "			'OPT KEYNAME':{"
				+ "				'value':"+LcMetaJSONObject.jsontypes+","
				+ "				'OPT location':[LAT, LON]"
				+ "			}"
				+ "		},"
				+ "		'subject':'Users'"
				+ "}";
		return new LcMetaJSONObject(inputjson);
	}
	@Override
	public JSONObject perform(List<LcInputStream> inputStreams, List<LcOutputStream> outputStreams, 
			Set<String> unknownKeys) throws Exception{
		
		return null;
	}

	@Override
	public LcMetaJSONObject getOutputIndex() {
		String outputjson = ""
				+ "	{'XOR result': 'boolean',"
				+ "	 'XOR error': ['json','list','string']"
				+ "}";
		return new LcMetaJSONObject(outputjson);
	}
}
