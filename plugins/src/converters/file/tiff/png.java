package converters.file.tiff;

import org.luci.connect.LcInputStream;
import org.luci.connect.LcOutputStream;
import org.luci.converters.IVariableFormatSuffix;
import org.luci.converters.StreamConverter;
import org.luci.factory.observer.FactoryObserver;

public class png extends StreamConverter implements IVariableFormatSuffix{

	public png(FactoryObserver o, LcInputStream in, LcOutputStream out, 
			boolean closeIn, boolean closeOut) {
		super(o, in, out, closeIn, closeOut, false);
	}

	@Override
	public Void call() throws Exception {
		// TODO tiff to jpg conversion
		return null;
	}

	@Override
	public String[] getSuffixVariants() {
		return new String[]{"png"};
	}

}
