/**
 * The MIT License (MIT)
 *
 * Copyright (c) <year> <copyright holders>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package converters.geometry.GeoJSON;

import java.util.Iterator;
import java.lang.Number;

import org.json.JSONArray;

/**
 * 
 * @author Lukas Treyer
 *
 */
@SuppressWarnings("rawtypes")
public class JNumberArray implements Iterator{
	private JSONArray jsonArray;
	private int i;
	private int length;
	
	public JNumberArray(JSONArray j){
		jsonArray = j;
		i = 0;
		length = j.length();
	}
	
	@Override
	public boolean hasNext() {
		return i < length;
	}

	@Override
	public Object next() {
		Object item = jsonArray.get(i++);
		if (item instanceof JSONArray) return new JNumberArray((JSONArray) item);
		else {
			if (item instanceof Number) return ((Number) item).doubleValue();
		}
		throw new IllegalArgumentException(item+" no number");
	}
	
	public static JSONArray iteratorToJSONArray(Iterator iter){
		JSONArray ja = new JSONArray();
		while(iter.hasNext()){
			Object ne = iter.next();
			if (ne instanceof Iterator) ja.put(iteratorToJSONArray((Iterator) ne));
			else ja.put(ne);
		}
		return ja;
	}
	
	@Override
	public void remove(){
		throw new UnsupportedOperationException();
	}
}
