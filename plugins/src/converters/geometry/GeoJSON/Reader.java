package converters.geometry.GeoJSON;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.luci.connect.GeometryInfo;
import org.luci.converters.IStreamReader;
import org.luci.converters.geometry.IGeoJSONReader;
import org.luci.converters.geometry.JSONGeometryReader;
import org.luci.factory.observer.FactoryObserver;
import org.luci.factory.observer.IDatabase;
import org.luci.utilities.Projection;
import org.luci.utilities.TAB;

public class Reader extends JSONGeometryReader implements IGeoJSONReader, IStreamReader {

	/**
	 * @param o FactoryObserver
	 * @param geometry JSONObject
	 * @param ScID long
	 * @param uid long
	 * @param t Timestamp (SQL)
	 * @param pc Projection
	 * @param errors JSONArray
	 * @throws InstantiationException 
	 */
	public Reader(FactoryObserver o, GeometryInfo geometry, int ScID, long uid, Timestamp st,  
			Projection pc, JSONArray errors) throws Exception {
		super(o, geometry, ScID, uid, st, pc, errors);

	}

	@Override
	public List<Long> call() throws Exception {
		if (geometry == null){
			throw new NullPointerException("Geometry must not be null!");
		}
		Timestamp now = new Timestamp(new Date().getTime());
		Connection con = o.getDatabaseConnection();
		JSONArray collection = collect(geometry);
		List<Long> new_ids = new ArrayList<Long>();
		boolean anyNonAutoID = false;

		String del = "INSERT INTO "+history+" SELECT ?, * FROM "+data+" WHERE geomid = ?;"
				+ "DELETE * FROM "+data+" WHERE geomid = ?;";
		
		String ID = attributeMap.getOrDefault("geomID", "geomID"); // JAVA8
		String LAYER = attributeMap.getOrDefault("layer", "layer"); // JAVA8
		String BATCHID = attributeMap.getOrDefault("batchID", "batchID"); // JAVA8
		String NURB = attributeMap.getOrDefault("nurb", "nurb"); // JAVA8
		String TRANSFORM = attributeMap.getOrDefault("transform", "transform"); // JAVA8
		String TIMESTAMP = attributeMap.getOrDefault("timestamp", "timestamp"); // JAVA8
		String UID = attributeMap.getOrDefault("uid", "uid"); // JAVA8
//		System.out.println(attributeMap);

		Map<String, Short> allAttributes = new HashMap<String, Short>();
		int numRecords = collection.length();
		for (int i = 0, k = 1; i < numRecords; i++, k = 1){
			PreparedStatement ps = null;
			Map<String, Short> attributes = new LinkedHashMap<String, Short>();
			try{				
				JSONObject g = collection.getJSONObject(i);
//				System.out.println("g: "+g);
				long geomID = 0;
				String layer = null;
				int batchID = 0;
				int nurb = 0;
				long uid = 0;
				boolean isAutoID = true;
				double[][] transform = null;
				Timestamp time = null;
				JSONObject p = null;
				if (g.getString("type").equals("Feature")){
					p = g.optJSONObject("properties");
					if (p != null) {
						// added to a Feature with no geometry
						if (p.has("deleted_geomIDs")){
							JSONArray delete_list = p.getJSONArray("deleted_geomIDs");
							if (delete_list.length() > 0){
								ps = con.prepareStatement(del);
								for(int j = 1;j < delete_list.length(); j++) {
									ps.setTimestamp(1, now);
							        ps.setLong(2, delete_list.getLong(i));
							        ps.setLong(3, delete_list.getLong(i));
							        ps.addBatch();
							    }
								ps.executeBatch();
								ps.close();
								continue;
							}
							p.remove("deleted_geomIDs");
						}
						
						if (p.has(ID)){
							geomID = p.getLong(ID);
							p.remove(ID);
							isAutoID = false;
							anyNonAutoID = true;
						}
						
						if (p.has(TIMESTAMP)){
							Object t = p.get(TIMESTAMP);
							p.remove(TIMESTAMP);
							try {
								if (t instanceof Number) time = new Timestamp((Long) t);
								else if (t instanceof String) time = Timestamp.valueOf((String) t);
							} catch (RuntimeException e){
								// leave timestamp untouched
								if (org.luci.Luci.DEBUG) e.printStackTrace();
							}
						}
						
						if (p.has(BATCHID)){
							batchID = p.getInt(BATCHID);
							p.remove(BATCHID);
						}
						if (p.has(LAYER)){
							layer = p.getString(LAYER);
							p.remove(LAYER);
						}
						if (p.has(NURB)){
							nurb = p.getInt(NURB);
							p.remove(NURB);
						}
						if (p.has(TRANSFORM)){
							JSONArray t = p.getJSONArray(TRANSFORM);
							p.remove(TRANSFORM);
							transform = new double[4][];
							for (int l = 0; l < 4; l++){
								JSONArray tt = t.getJSONArray(l);
								double[] tf = transform[l] = new double[4];
								for (int n = 0; n < 4; n++){
									tf[n] = tt.getDouble(n);
								}
							}
						}
						if (p.has(UID)){
							uid = p.getLong(UID);
							p.remove(UID);
						}

						@SuppressWarnings("unchecked")
						Set<String> keySet = p.keySet();
						for (String key: keySet) attributes.put(key, TAB.JSONType2SQL(p.get(key)));
					}
					g = g.optJSONObject("geometry");
				}
				
				
/**prototypical SQL code intended as a memo
 * UPSERT / MERGE (Postgres example): 
 * INSERT INTO lc_sc_1_h SELECT NULL, * FROM lc_sc_1 WHERE geomid = x [set in getInsertSQL since its a number] AND nurb is NOT null AND transform is NOT null;
 * WITH upsert AS (UPDATE table SET timestamp = NOW() WHERE geomid = x [to make things easy also this one is being set directly] RETURNING *) 
 * INSERT INTO lc_sc_1 SELECT * FROM upsert WHERE NOT EXISTS (SELECT * FROM upsert);
 * 
 * AUTO ID: INSERT INTO 
 * table(timestamp, batchID, layer, nurb, transform, uid, flag, geom, some, custom, attributes) 
 * VALUES(?,?,?,?,?,?,?,?,?,?,?);**/
//				System.out.println("geomID: "+geomID);
				IDatabase DB = o.dataModelImplementation();
				DB.addColumns(con, ScID, TAB.merge(allAttributes, attributes));
				String sql = db.getInsertSQL(geomID, attributes.keySet(), data, history, con); 

				if (isAutoID) ps = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
				else ps = con.prepareStatement(sql);

				if (time == null) ps.setTimestamp(k++, st);
				else ps.setTimestamp(k++, time);
				ps.setInt(k++, batchID); // sets batchID to 0 by default;
				if (layer != null) ps.setString(k++, layer);
				else ps.setNull(k++, Types.VARCHAR);
				if (nurb <= 0) ps.setNull(k++, Types.INTEGER);
				else ps.setInt(k++, nurb);
				if (transform == null) ps.setNull(k++, Types.ARRAY);
				else ps.setArray(k++, new TAB.Transform(transform).getSQLArray(con));
				ps.setLong(k++, uid);
				ps.setNull(k++, Types.INTEGER); //flag
				if (g != null) ps.setObject(k++, geometryToDb(g));
				else ps.setNull(k++, Types.OTHER);
				
				if (p != null) {
					for (String key: attributes.keySet()){
//						System.out.println(k+", "+key);
						switch(attributes.get(key)){
						case TAB.NUMERIC: 
							Number n = (Number) p.get(key);
							if (n instanceof Integer) ps.setInt(k++, (Integer) n);
							else if (n instanceof Long) ps.setLong(k++, (Long) n);
							else ps.setBigDecimal(k++, new BigDecimal((Double) n)); 
							break;
						case TAB.STRING: ps.setString(k++, p.getString(key));break;
						case TAB.BOOLEAN: ps.setBoolean(k++, p.getBoolean(key));break;
						case TAB.CHECKSUM: 
							JSONObject info = p.getJSONObject(key);
							String chck = info.getJSONObject("streaminfo").getString("checksum");
							String frmt = info.getString("format");
							db.setChecksum(ps, k++, frmt, chck, con);
							break;
						case TAB.JSON: ps.setString(k++, p.getJSONObject(key).toString());break;
						case TAB.LIST: ps.setString(k++, p.getJSONArray(key).toString());break;
						}
					}
					
				}

				ps.execute();
//				System.out.println("ScID: "+ScID);

				if (isAutoID){

					ResultSet rs = ps.getGeneratedKeys();
					while(rs.next()){
						geomID = rs.getLong(1);
						new_ids.add(geomID);
					}
				}
			} catch (SQLException e){
//				log.debug("GeoJSON.save(): "+e.toString()+", "+ps.toString());
				con.close();
				throw e;
				//return false;
			} catch (JSONException e){
				log.debug(e.toString()+", "+collection.toString());
				con.close();
				throw e;
			}
		}
		// make a guess; maybe add a converter parameter in the future telling where to start with 
		// auto ids with 0 preventing this converter to make any alter sequence calls
		if (anyNonAutoID) db.updateAutoIDSequence(con, data, "geomID");
		con.close();
		return new_ids;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public Object geometryToDb(JSONObject geo){
		if (geo != null){
			String type = geo.getString("type").toLowerCase();
			JSONArray c = geo.getJSONArray("coordinates");
			switch(type){
			case "point": return dbG.toPointObject(new JNumberArray(c), projection);
			case "linestring": return dbG.toLineStringObject(new JNumberArray(c), projection);
			case "polygon": return  dbG.toPolygonObject(new JNumberArray(c), projection);
			case "multipoint": return dbG.toMultiPointObject(new JNumberArray(c), projection);
			case "multilinestring": return dbG.toMultiLineStringObject(new JNumberArray(c), projection);
			case "multipolygon": return dbG.toMultiPolygonObject(new JNumberArray(c), projection);
			default: 
				throw new IllegalArgumentException("No PostGIS type defined. Could not save "+type+" !");
			}
		}
		return null;
	}
	
	protected JSONArray collect(JSONObject json){
		JSONArray jsone = new JSONArray();
		String t = json.getString("type");
		if (t.equals("FeatureCollection"))
			jsone = json.getJSONArray("features");
		else if (t.equals("GeometryCollection"))
			jsone = json.getJSONArray("geometries");
		else jsone.put(json);
		return jsone;
	}

	

}
