package converters.geometry.GeoJSON;

import java.io.File;
import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;
import org.luci.connect.LcOutputStream;
import org.luci.connect.StreamInfo;
import org.luci.converters.OGISGeometry;
import org.luci.converters.geometry.AbstractGeometryIterator;
import org.luci.converters.geometry.GeometrySelector;
import org.luci.converters.geometry.GeometryWriter;
import org.luci.converters.geometry.IGeoJSONWriter;
import org.luci.factory.observer.FactoryObserver;
import org.luci.utilities.Projection;
import org.luci.utilities.TAB;
import org.luci.utilities.TimeRangeSelection;

public class Writer extends GeometryWriter implements IGeoJSONWriter {
	
	public class PrettyPrintingMap<K, V> {
	    private Map<K, V> map;

	    public PrettyPrintingMap(Map<K, V> map) {
	        this.map = map;
	    }

	    @Override
		public String toString() {
	        StringBuilder sb = new StringBuilder();
	        Iterator<Entry<K, V>> iter = map.entrySet().iterator();
	        while (iter.hasNext()) {
	            Entry<K, V> entry = iter.next();
	            sb.append(entry.getKey());
	            sb.append('=').append('"');
	            sb.append(entry.getValue());
	            sb.append('"');
	            if (iter.hasNext()) {
	                sb.append(',').append(' ');
	            }
	        }
	        return sb.toString();

	    }
	}

	/**
	 * @param o FactoryObserver
	 * @param ScID Scenario ID int
	 * @param batchID Batch ID int
	 * @param layer Layer name String
	 * @param geomID Geometry ID long
	 * @param trs TimeRangeSelection
	 * @param inclHistory boolean
	 * @param version String (like 1.1.2.3.4)
	 * @param pc Projection
	 * @param outputStreams List< LcOutputStream >
	 * @param errors JSONArray
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	public Writer(FactoryObserver o, GeometrySelector selector,
			Projection pc, List<LcOutputStream> outputStreams, JSONArray errors)
			throws InstantiationException, IllegalAccessException {
		super(o, selector, pc, outputStreams, errors);
	}
	
	@Override
	public JSONObject call() throws Exception {
		return write();
	}
	
	private JSONObject write() throws Exception{
		Connection con  = o.getDatabaseConnection();
		// if the scenario gets altered / a new attribute is being added between here and select(con)
		// the new attributes will not get exported; this call makes the selection on the attributes
		LinkedHashMap<String, Short> types = new LinkedHashMap<String, Short>();
		JSONArray features = new JSONArray();
		
		ResultSet rs = selector.select(con, types);
		Set<Entry<String, Short>> entries = new LinkedHashSet<Entry<String, Short>>();
		Iterator<Entry<String, Short>> it = types.entrySet().iterator();
		int i = 0;
		while(i++ < 9) it.next();
		while(it.hasNext()) entries.add(it.next());
//		System.out.println("types: "+types);
//		System.out.println("entries: "+entries);
		int j = Math.min(outputStreams.size(), 1);
		
		while(rs.next()){
			// fix properties
			JSONObject properties = new JSONObject();
			properties.put("geomID", rs.getLong(1));
			properties.put("timestamp", rs.getTimestamp(2).getTime());
			int batchID = rs.getInt(3);
			if (batchID > 0) properties.put("batchID", batchID);
			properties.put("layer", rs.getString(4));
			int nurb = rs.getInt(5);
			if (nurb != 0) properties.put("nurb", nurb);
			properties.putOpt("transform", new TAB.Transform(rs.getArray(6)).t);
			properties.put("uid", rs.getLong(7));
			
			// feature & geometry
			JSONObject feature = new JSONObject();
			feature.put("type", "Feature");
			feature.put("geometry", geometryFromDb(rs.getObject("geom")));
//			System.out.println(types);
			
			// scenario specific properties
			int k = 10;
			for (Entry<String, Short> e: entries){
				String keyname = e.getKey();
				short type = e.getValue();
				String s = null;
//				System.out.println(keyname+", "+TAB.TYPE_NAMES.get(type)+": "+rs.getObject(k));
				switch (type){
				case TAB.NUMERIC: properties.putOpt(keyname, rs.getBigDecimal(k++)); break;
				case TAB.BOOLEAN: properties.putOpt(keyname, rs.getBoolean(k++)); break;
				case TAB.STRING: properties.putOpt(keyname, rs.getString(k++)); break;
				case TAB.JSON: 
					s = rs.getString(k++);
					if (s != null) properties.put(keyname, new JSONObject(s));
					break;
				case TAB.LIST: 
					s = rs.getString(k++);
					if (s != null) properties.put(keyname, new JSONArray(s)); 
					break;
				case TAB.NUMERICARRAY: 
					Array a = rs.getArray(k++);
					if (a != null) properties.put(keyname, new JSONArray((Number[])a.getArray()));
				case TAB.CHECKSUM: 
					Array pointer = rs.getArray(k++);
					if (pointer != null){
						TAB.Checksum chck = new TAB.Checksum(pointer);
						String filename = chck.md5+"."+chck.format;
						File f = new File(org.luci.Luci.getDataRoot(), filename);
						if (f.exists()){
							StreamInfo si = new StreamInfo(chck.md5, chck.format, keyname, j++, (int)f.length(), null, null);
							LcOutputStream los = new LcOutputStream(si);
							outputStreams.add(los);
							properties.put(keyname, los.getJSON());
						}
					}
					break;
				}
			}
			feature.put("properties", properties);
			features.put(feature);
		}
		
		// deleted geomIDs
		TimeRangeSelection trs = selector.getTimeRangeSelection();
		long ScID = selector.getScID();
		if (trs.isBetween() || trs.isAfter() || trs.isFrom()){
			String hsc = TAB.getScenarioTable(ScID, TAB.SCN_HISTORY);
			String sql = "SELECT geomID FROM "+hsc+" WHERE timestamp_deleted IS NOT NULL "+trs.getSQL();
			PreparedStatement ps = con.prepareStatement(sql);
			trs.setTimestampSQL(ps, 1);
			JSONObject feature = new JSONObject();
			JSONObject properties = new JSONObject();
			Set<Long> geomids = new HashSet<Long>();
			
			ps = con.prepareStatement(sql);
			ps.setLong(1, ScID);
			trs.setTimestampSQL(ps, 2);
			trs.setTimestampSQL(ps, 3);
			rs = ps.executeQuery();
			while(rs.next()) geomids.add(rs.getLong(1));
			properties.put("deleted_geomIDs", geomids);
			feature.put("type", "Feature");
			feature.put("properties", properties);
			features.put(feature);
		}
		con.close();
		
		JSONObject fc = new JSONObject();
		fc.put("type", "FeatureCollection");
		fc.put("features", features);
		return new JSONObject().put("GeoJSON", new JSONObject().put("format", "GeoJSON").put("geometry", fc));
	}

	@Override
	public JSONObject geometryFromDb(Object geometry){
		JSONObject geom = null;
		if (geometry != null){
			geom = new JSONObject();
			@SuppressWarnings("rawtypes")
			AbstractGeometryIterator gi = dbG.fromObject(geometry, projection);
			int t = gi.getType();
			if (!(t == OGISGeometry.TRIANGLE || t == OGISGeometry.TIN || t == OGISGeometry.POLYHEDRALSURFACE)){
				geom.put("type", OGISGeometry.ALLTYPES[t]);
				geom.put("coordinates", JNumberArray.iteratorToJSONArray(gi.iterator()));
			}
		}
		return geom;
	}

}
