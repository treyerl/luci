package converters.geometry.OSMJSON;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.luci.connect.GeometryInfo;
import org.luci.converters.IStreamReader;
import org.luci.converters.geometry.JSONGeometryReader;
import org.luci.factory.observer.FactoryObserver;
import org.luci.factory.observer.IDatabase;
import org.luci.utilities.Projection;
import org.luci.utilities.TAB;

public class Reader extends JSONGeometryReader implements IStreamReader{
	HashMap<Integer, JSONObject> points = new HashMap<Integer, JSONObject>();
	HashMap<Integer, JSONObject> ways = new HashMap<Integer, JSONObject>();
	HashMap<Integer, JSONArray> multipolygons = new HashMap<Integer, JSONArray>();
	
	HashMap<Integer, Boolean> points_use = new HashMap<Integer, Boolean>();
	HashMap<Integer, Boolean> ways_use = new HashMap<Integer, Boolean>();

	class Node implements Iterator<Double> {
		int next_counter = 0;
		int ref = 0;
		
		public Node(int ref) {
			this.ref = ref;
			points_use.replace(ref, true);
		}

		@Override
		public boolean hasNext() {
			return next_counter<2;
		}

		@Override
		public Double next() {
			next_counter++;
			if(next_counter==1) return points.get(ref).getDouble("lon");
			else if(next_counter==2) return points.get(ref).getDouble("lat");
			return null;
		}
	}
	
	class LineString implements Iterator<Iterator<Double>>{
		int way_counter = 0;
		int node_counter = 0;
		int next_counter = 0;
		int n_ways = 0;
		int length = 0;
		JSONArray ref_ways = new JSONArray();
		
		public LineString(JSONArray ref_ways) {
			this.ref_ways = ref_ways;
			n_ways = ref_ways.length();
			for(int i = 0; i < n_ways; i++) {
				length += ways.get(ref_ways.get(i)).getJSONArray("nodes").length();
				ways_use.replace(ref_ways.getInt(i), true);
			}
		}
		
		@Override
		public boolean hasNext() {
			return next_counter<length;
		}

		@Override
		public Node next() {
			next_counter++;
			if(ways.get(ref_ways.get(way_counter)).getJSONArray("nodes").length() > node_counter) {
				node_counter++;
			}
			else {
				node_counter = 0;
				way_counter++;
			}
			int node_ref = ways.get(ref_ways.get(way_counter)).getJSONArray("nodes").getInt(node_counter-1);
			return new Node(node_ref);
		}		
	}
	
	class Polygon_way implements Iterator<Iterator<Iterator<Double>>> {
		int next_counter = 0;
		int length = 0;
		JSONArray ref = new JSONArray();

		public Polygon_way(int ref) {
			this.ref.put(ref);
			length = 1;
		}

		@Override
		public boolean hasNext() {
			return next_counter<length;
		}

		@Override
		public LineString next() {
			next_counter++;
			return new LineString(ref);
		}
	}
	
	class Polygon_multipolygon implements Iterator<Iterator<Iterator<Double>>> {
		int ref_relation;
		int ref_polygon;
		int next_counter = 0;
		int length = 0;
		
		public Polygon_multipolygon(int ref_relation, int ref_polygon) {
			this.ref_relation = ref_relation;
			this.ref_polygon = ref_polygon;

			for(int i = 0; i < multipolygons.get(ref_relation).getJSONArray(ref_polygon).length(); i++) {
				length += multipolygons.get(ref_relation).getJSONArray(ref_polygon).getJSONArray(i).length();
			}
		}

		@Override
		public boolean hasNext() {
			return next_counter<length;
		}

		@Override
		public LineString next() {
			next_counter++;
			JSONArray way_ref = new JSONArray();
			for(int i = 0; i < multipolygons.get(ref_relation).getJSONArray(ref_polygon).getJSONArray(next_counter-1).length(); i++) {
				way_ref.put(multipolygons.get(ref_relation).getJSONArray(ref_polygon).getJSONArray(next_counter-1).getJSONObject(i).getInt("ref"));
			}
			return new LineString(way_ref);
		}
	}
	
	class Multipolygon implements Iterator<Iterator<Iterator<Iterator<Double>>>> {
		int ref_relation;
		int next_counter = 0;
		int length = 0;
		JSONArray areas;

		public Multipolygon(int ref_relation) {
			this.ref_relation = ref_relation;
			length = multipolygons.get(ref_relation).length();
		}

		@Override
		public boolean hasNext() {
			return next_counter<length;
		}

		@Override
		public Polygon_multipolygon next() {
			next_counter++;
			int polygon_ref = next_counter-1;
			return new Polygon_multipolygon(ref_relation, polygon_ref);
		}
	}
	
	
	public Reader(FactoryObserver o, GeometryInfo geometry, int ScID, long uid, Timestamp st,  
			Projection pc, JSONArray errors) throws Exception {
		super(o, geometry, ScID, uid, st, pc, errors);
	}
	
	public List<Long> call() throws Exception {
				
		Connection con = o.getDatabaseConnection();
		String tab = TAB.getScenarioTable(ScID, TAB.SCN);

		String insert = "INSERT INTO "+tab+"(timestamp, geom, layer, flag) VALUES(?,?,?,0);";
		String update = "UPDATE "+tab+" SET relation = ? WHERE geomID = ?";
//		StringBuilder del = new StringBuilder("INSERT INTO "+tab+"(scid, id, timestamp, flag) VALUES (?,?,?,"+TAB.flagDELETED+")");
		
		PreparedStatement ps_tab = null;
		PreparedStatement ps_update = null;
		
//		ps_att = con.prepareStatement(attString);
		
		IDatabase dbM = o.dataModelImplementation();
		LinkedHashMap<String, Short> columnstypes = dbM.getColumnNamesAndTypes(con, ScID);
        if(!columnstypes.containsKey("relation")) {
        	Map<String, Short> attributes = new HashMap<String, Short>();
        	attributes.put("relation", TAB.NUMERIC);
        	dbM.addColumns(con, ScID, attributes);
	    }
		
		List<Long> new_ids = new ArrayList<Long>();
		
		JSONArray elements = geometry.getJSONArray("elements");
				
		for(int i = 0; i < elements.length(); i++) {
			JSONObject element_i = elements.getJSONObject(i);
			if(check_value(element_i, "type", "node")) {
				points.put(element_i.getInt("id"), element_i);
				points_use.put(element_i.getInt("id"), false);
			}
			else if(check_value(element_i, "type", "way")) {
				ways.put(element_i.getInt("id"), element_i);
				ways_use.put(element_i.getInt("id"), false);
			}
			else if(check_value(element_i, "type", "relation") 
					&& check_value(element_i.optJSONObject("tags"), "type", "multipolygon")) {
				JSONArray polygons = get_polygons(element_i);
				multipolygons.put(element_i.getInt("id"), polygons);
			}	
		}		
		
		long id = 0;
		
		try {
			Iterator<Entry<Integer, JSONArray>> it_multipolygons = multipolygons.entrySet().iterator();
//			int batch_size=100;
//			int batch_i=0;
			ps_tab = con.prepareStatement(insert, Statement.RETURN_GENERATED_KEYS);
			ps_update = con.prepareStatement(update, Statement.RETURN_GENERATED_KEYS);

			while(it_multipolygons.hasNext()) {				
				Map.Entry<Integer, JSONArray> entry = (Map.Entry<Integer, JSONArray>)it_multipolygons.next();

				ps_tab.setTimestamp(1, st);
				String layer = "default";
				if(entry.getValue().length()<2) {
					ps_tab.setObject(2, dbG.toPolygonObject(new Polygon_multipolygon(entry.getKey(), 0), projection));
				}
				else
					ps_tab.setObject(2, dbG.toMultiPolygonObject(new Multipolygon(entry.getKey()), projection));
				ps_tab.setString(3, layer);
				ps_tab.execute();
				
				
//				ps_att.setLong(1, ScID);
				ResultSet rs = ps_tab.getGeneratedKeys();
				rs.next();
				
				id = rs.getLong(1);
				new_ids.add(id);
				
				ps_update.setInt(1, entry.getKey());
				ps_update.setLong(2, id);
				ps_update.execute();
								
//				attribute_map.put("relation", entry.getKey());
//				ps_att.setLong(2, id);
//				ps_att.setTimestamp(3, st);
//				ps_att.setString(4, "relation");
//				JSON2SQL.ServiceOutputOrGeometryAttribute(ps_att, entry.getKey(), 5);
//				ps_att.setLong(9, uid);
//				ps_att.execute();
//				ps_tab.addBatch();
//				if(batch_i >= batch_size) {
//					ps_tab.executeBatch();
//					batch_i=0;
//					ps_tab.clearBatch();
//				}

			}		
//			if(batch_i != 0) {
//				ps_tab.executeBatch();
//				ps_tab.clearBatch();
//			}
			
			ps_tab.close();
			
			Iterator<Entry<Integer, JSONObject>> it_ways = ways.entrySet().iterator();
			while(it_ways.hasNext()) {
				Map.Entry<Integer, JSONObject> entry = (Map.Entry<Integer, JSONObject>)it_ways.next();
				if(!ways_use.get(entry.getKey())) {
					ps_tab = con.prepareStatement(insert, Statement.RETURN_GENERATED_KEYS);
					ps_tab.setTimestamp(1, st);
					if(check_closed(entry.getValue())) {
						ps_tab.setObject(2, dbG.toPolygonObject(new Polygon_way(entry.getKey()), projection));
					}
					else {
						JSONArray way = new JSONArray();
						way.put(entry.getKey());
						ps_tab.setObject(2, dbG.toLineStringObject(new LineString(way), projection));
					}
					String layer = "default";
					ps_tab.setString(3, layer);
					ps_tab.execute();					
					ps_tab.close();
				}
			}
			
			Iterator<Entry<Integer, JSONObject>> it_points = points.entrySet().iterator();
			while(it_points.hasNext()) {
				Map.Entry<Integer, JSONObject> entry = (Map.Entry<Integer, JSONObject>)it_points.next();
				if(!points_use.get(entry.getKey())) {
					ps_tab = con.prepareStatement(insert, Statement.RETURN_GENERATED_KEYS);
					ps_tab.setTimestamp(1, st);
					ps_tab.setObject(2, dbG.toPointObject(new Node(entry.getKey()), projection));
					String layer = "default";
					ps_tab.setString(3, layer);
					ps_tab.execute();			
					ps_tab.close();
				}
			} 
		} catch (SQLException e){
			this.log.debug("OSMJSON.save(): "+e.toString()+", "+ps_tab.toString());
			con.close();
			throw e;
			//return false;
		} catch (JSONException e){
			this.log.debug(e.toString()+", "+points.toString()+ways.toString()+multipolygons.toString());
			con.close();
			throw e;
		}

		con.close();		
		return new_ids;
	}
	
	//check if JSONObject has a specific key/value pair
	private boolean check_value(JSONObject object, String key, Object value) {
		if(object!=null && object.has(key)) {
			return object.get(key).equals(value);
		}
		else return false;
	}

	//check if the way is an area
	private boolean check_closed(JSONObject way) {
		if(way.getJSONArray("nodes").getInt(0)==way.getJSONArray("nodes").getInt(way.getJSONArray("nodes").length()-1)){
			return true;
		}
		else return false;
	}
	
	//returns the connection point of two ways:
	//0 for no connection
	//1 for connection of the last point of way_1 and the first point of way_2
	//2 for connection of the last point of way_1 and the last point of way_2
	//3 for connection of the first point of way_1 and the first point of way_2
	//4 for connection of the first point of way_1 and the last point of way_2
	private int check_connection(JSONObject way_1, JSONObject way_2) {
		JSONArray nodes_1 = way_1.getJSONArray("nodes");
		JSONArray nodes_2 = way_2.getJSONArray("nodes");

		if(nodes_1.getInt(nodes_1.length()-1)==nodes_2.getInt(0)) {
			return 1;
		}
		else if(nodes_1.getInt(nodes_1.length()-1)==nodes_2.getInt(nodes_2.length()-1)) {
			return 2;
		}
		else if(nodes_1.getInt(0)==nodes_2.getInt(0)) {
			return 3;
		}
		else if(nodes_1.getInt(0)==nodes_2.getInt(nodes_2.length()-1)) {
			return 4;
		}
		else return 0;
	}
	
	private void insert_json(int index, JSONArray object, JSONArray array) {
		 if(array.length()!=0) {
			 for(int i = array.length() - 1; i < index; i--) {
				Object object_tmp = array.get(i);
				array.put(i+1, object_tmp);
			 }
		}
		array.put(index, object);
	}
	
	//returns an array of polygons in which the references to the connected ways are stored
	//if multiple outer areas exist, it puts them in a different polygon space
	//outer areas are in the beginning
	//TODO consider the type of connection
	private JSONArray get_polygons(JSONObject relation) {
		JSONArray polygons = new JSONArray();
		JSONArray areas = new JSONArray();
		JSONArray members = new JSONArray();
		members = relation.getJSONArray("members");
		int n_outer_areas = 0;
		boolean b_outer = false;
		for(int i = 0; i < members.length(); i++) {
			JSONObject way_i = members.getJSONObject(i);
			if(check_closed(ways.get(way_i.getInt("ref")))) {
				JSONArray way_closed = new JSONArray();
				way_closed.put(way_i);
				b_outer = way_closed.getJSONObject(0).get("role").equals("outer");
				if(b_outer) n_outer_areas++;
				if(n_outer_areas<=1 && b_outer) insert_json(0,way_closed,areas);
				else if(n_outer_areas<=1 && !b_outer) areas.put(way_closed);
				else if(n_outer_areas>1) {
					polygons.put(areas);
					areas = new JSONArray();
					n_outer_areas = 0;
				}
				members.remove(i);
				i--;
			}
		}
		if(members.length()>1) {
			for(int i = 0; i < members.length(); i++) {
				JSONObject way_i = members.getJSONObject(i);	
				JSONArray ways_connected = new JSONArray();
				ways_connected.put(way_i);
				for(int j = i+1; j < members.length(); j++) {
					JSONObject way_j = members.getJSONObject(j);
					int connection = check_connection(ways.get(way_i.getInt("ref")), ways.get(way_j.getInt("ref")));
					if(connection!=0) {
						ways_connected.put(way_j);
					}
				}
				b_outer = ways_connected.getJSONObject(0).get("role").equals("outer");
				if(b_outer) n_outer_areas++;
				if(n_outer_areas<=1 && b_outer) insert_json(0,ways_connected,areas);
				else if(n_outer_areas<=1 && !b_outer) areas.put(ways_connected);
				else if(n_outer_areas>1) {
					polygons.put(areas);
					areas = new JSONArray();
					n_outer_areas = 0;
				}
			}
		}
		if(n_outer_areas!=0) polygons.put(areas);
		return polygons;
	}


	
}
