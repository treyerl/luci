package converters.geometry.OSMJSON;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.luci.connect.LcOutputStream;
import org.luci.converters.geometry.GeometrySelector;
import org.luci.converters.geometry.GeometryWriter;
import org.luci.factory.observer.FactoryObserver;
import org.luci.utilities.Projection;

public class Writer extends GeometryWriter {
	/**
	 * @param o FactoryObserver
	 * @param ScID Scenario ID int
	 * @param batchID Batch ID int
	 * @param layer Layer name String
	 * @param geomID Geometry ID long
	 * @param trs TimeRangeSelection
	 * @param inclHistory boolean
	 * @param version String (like 1.1.2.3.4)
	 * @param pc Projection
	 * @param outputStreams List< LcOutputStream >
	 * @param errors JSONArray
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	public Writer(FactoryObserver o, GeometrySelector selector,
			Projection pc, List<LcOutputStream> outputStreams, JSONArray errors)
			throws InstantiationException, IllegalAccessException {
		super(o, selector, pc, outputStreams, errors);
	}

	public JSONObject call() throws Exception {
		return null;
	}
}
	
	