/**
 * The MIT License (MIT)
 *
 * Copyright (c) <year> <copyright holders>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package converters.geometry.dxf.Entities;

import org.kabeja.common.DraftEntity;
import org.kabeja.entities.Face3D;
import org.luci.utilities.Projection;

import converters.geometry.dxf.Reader;
import converters.geometry.dxf.Writer;

/**
 * 
 * @author Lukas Treyer
 *
 */
public class Convert3DFace extends ConvertSolid {

	/**
	 * @param p
	 */
	public Convert3DFace(Projection p, Reader r, Writer w) {
		super(p, r, w);
	}
	
	
	@Override
	public DraftEntity convert(Object o) {
		Face3D f = new Face3D();
//		double[][][] p = this.fromPolygon((Polygon) o, proj);
//		f.setPoint1(new Point3D(p[0][0][0], p[0][0][1], p[0][0][2]));
//		f.setPoint2(new Point3D(p[1][0][0], p[1][0][1], p[1][0][2]));
//		f.setPoint3(new Point3D(p[2][0][0], p[2][0][1], p[2][0][2]));
//		f.setPoint4(new Point3D(p[3][0][0], p[3][0][1], p[3][0][2]));
		return f;
	}

}
