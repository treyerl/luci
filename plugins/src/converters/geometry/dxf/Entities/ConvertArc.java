/**
 * The MIT License (MIT)
 *
 * Copyright (c) <year> <copyright holders>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package converters.geometry.dxf.Entities;

import org.kabeja.common.DraftEntity;
import org.kabeja.entities.Arc;
import org.luci.utilities.Projection;

import converters.geometry.dxf.IEntityConverter;
import converters.geometry.dxf.Reader;
import converters.geometry.dxf.Writer;

/**
 * 
 * @author Lukas Treyer
 *
 */
public class ConvertArc implements IEntityConverter {
	
	protected Projection proj;
	protected Reader r;
	protected Writer w;
	
	/**
	 * 
	 */
	public ConvertArc(Projection p, Reader r, Writer w) {
		this.proj = p;
		this.r = r;
		this.w = w;
	}

	/* (non-Javadoc)
	 * @see converters.geometry.dxf.IEntityConverter#convert(org.kabeja.common.DraftEntity)
	 */
	@Override
	public Object convert(DraftEntity e) {
		Arc a = (Arc) e;
		
		return null;
	}

	/* (non-Javadoc)
	 * @see converters.geometry.dxf.IEntityConverter#convert(java.lang.Object)
	 */
	@Override
	public DraftEntity convert(Object o) {
		// TODO Auto-generated method stub
		return null;
	}

}
