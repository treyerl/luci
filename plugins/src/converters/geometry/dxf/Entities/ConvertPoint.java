package converters.geometry.dxf.Entities;

import java.util.Iterator;

import org.kabeja.common.DraftEntity;
import org.kabeja.math.Point3D;
import org.luci.utilities.Projection;

import converters.geometry.dxf.IEntityConverter;
import converters.geometry.dxf.Reader;
import converters.geometry.dxf.Writer;


public class ConvertPoint implements IEntityConverter {
	
	protected Projection proj;
	protected Reader r;
	protected Writer w;
	
	@SuppressWarnings("rawtypes")
	public class IterPoint implements Iterator {
		
		private Point3D point;
		private int i;
		
		public IterPoint(Point3D p){
			point = p;
			i = 0;
		}
		
		@Override
		public boolean hasNext() {
			return i < 3;
		}

		@Override
		public Object next() {
			if (i++ == 0) return new Double(point.getX());
			if (i++ == 1) return new Double(point.getY());
			if (i++ == 2) return new Double(point.getZ());
			return null;
		}
		
		@Override
		public void remove(){
			throw new UnsupportedOperationException();
		}

	}
	
	public ConvertPoint(Projection proj, Reader r, Writer w){
		this.proj = proj;
		this.r = r;
		this.w = w;
	}
	
	@Override
	public Object convert(DraftEntity e) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DraftEntity convert(Object o) {
		// TODO Auto-generated method stub
		return null;
	}

}
