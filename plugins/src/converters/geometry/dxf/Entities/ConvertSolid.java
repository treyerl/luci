package converters.geometry.dxf.Entities;

import java.util.Iterator;

import org.kabeja.common.DraftEntity;
import org.kabeja.entities.Solid;
import org.luci.utilities.Iter;
import org.luci.utilities.Projection;

import converters.geometry.dxf.IEntityConverter;
import converters.geometry.dxf.Reader;
import converters.geometry.dxf.Writer;

public class ConvertSolid extends ConvertPoint implements IEntityConverter {
	@SuppressWarnings("rawtypes")
	public class IterSolid implements Iterator{
		private int i ;
		private Solid s;
		public IterSolid(Solid solid){
			s = solid;
			i = 0;
		}
		
		@Override
		public boolean hasNext() {
			return i < 4;
		}

		@Override
		public Iterator next() {
			if (i++ == 0) return new IterPoint(s.getPoint1());
			if (i++ == 1) return new IterPoint(s.getPoint2());
			if (i++ == 2) return new IterPoint(s.getPoint3());
			if (i++ == 3) return new IterPoint(s.getPoint4());
			return null;
		}
		
		@Override
		public void remove(){
			throw new UnsupportedOperationException();
		}
		
	}
	protected Projection proj;
	
	public ConvertSolid(Projection proj, Reader r, Writer w){
		super(proj, r, w);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object convert(DraftEntity e) {
		Solid s = (Solid) e;	
		return r.getDB().toPolygonObject(new Iter(new IterSolid(s)), proj);		
	}

	@Override
	public DraftEntity convert(Object o) {
		Solid s = new Solid();
//		double[][][] p = this.fromPolygon((Polygon) o, proj);
//		s.setPoint1(new Point3D(p[0][0][0], p[0][0][1], p[0][0][2]));
//		s.setPoint2(new Point3D(p[1][0][0], p[1][0][1], p[1][0][2]));
//		s.setPoint3(new Point3D(p[2][0][0], p[2][0][1], p[2][0][2]));
//		s.setPoint4(new Point3D(p[3][0][0], p[3][0][1], p[3][0][2]));
		return s;
	}

}
