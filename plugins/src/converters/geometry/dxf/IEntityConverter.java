package converters.geometry.dxf;

import org.kabeja.common.DraftEntity;

public interface IEntityConverter{
	public Object convert(DraftEntity e);
	public DraftEntity convert(Object o);
}
