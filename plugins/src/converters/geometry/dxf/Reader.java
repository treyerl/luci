package converters.geometry.dxf;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.kabeja.DraftDocument;
import org.kabeja.common.DraftEntity;
import org.kabeja.common.Layer;
import org.kabeja.common.Type;
import org.kabeja.dxf.parser.DXFParserBuilder;
import org.kabeja.parser.ParseException;
import org.kabeja.parser.Parser;
import org.luci.connect.LcInputStream;
import org.luci.converters.IStreamReader;
import org.luci.converters.geometry.GeometryReader;
import org.luci.converters.geometry.IDatabaseGeometryObjectReader;
import org.luci.factory.observer.FactoryObserver;
import org.luci.utilities.Projection;
import org.luci.utilities.TAB;

import converters.geometry.dxf.Entities.Convert3DFace;
import converters.geometry.dxf.Entities.ConvertArc;
import converters.geometry.dxf.Entities.ConvertPoint;
import converters.geometry.dxf.Entities.ConvertSolid;

public class Reader extends GeometryReader implements IStreamReader{	
	protected DraftDocument doc;
	
	public Reader(FactoryObserver o, int ScID, long uid, Timestamp t, 
			Projection pc, JSONArray errors) throws InstantiationException {
		super(o, ScID, uid, t, pc, errors);
	}

	/* (non-Javadoc)
	 * @see org.luci.converters.IStreamReader#readStream(org.luci.connect.LcInputStream)
	 */
	@Override
	public void readStream(LcInputStream lis) throws ParseException {
		Map<String, Object> fileProperties = new HashMap<String, Object>();
		Parser parser = DXFParserBuilder.createDefaultParser();
		doc = parser.parse(lis.getInputStream(), fileProperties);
	}
	
	@Override
	public List<Long> call() throws Exception {
		Connection con = o.getDatabaseConnection();
		String tab = TAB.getScenarioTable(ScID, TAB.SCN);

		String sql = "INSERT INTO "+tab+"(scid, id, timestamp, geom, layer, nurb, transform, uid, flag) VALUES (?,?,?,?,?,?,?,?,0)";
		List<Long> ids = new ArrayList<Long>();
		List<String> layers = new ArrayList<String>();
		List<Integer> nurbTypes = new ArrayList<Integer>();
		List<double[][]> transformations = new ArrayList<double[][]>();
		List<Object> geoms = convert(layers, ids, nurbTypes, transformations);
		for (int i = 0; i < geoms.size(); i++){
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setLong(1, ScID);
			ps.setLong(2, ids.get(i));
			ps.setTimestamp(3, st);
			ps.setObject(4, geoms.get(i));
			ps.setString(5, layers.get(i));
			ps.setInt(6, nurbTypes.get(i));
			ps.setObject(7, transformations.get(i));
			ps.setLong(8, uid);
			ps.executeUpdate();
		}
		
		
		return null;
	}
	
	public List<Object> convert(List<String> layers, List<Long> ids, List<Integer> nurbTypes, List<double[][]> transformations) {
		List<Object> geoms = new ArrayList<Object>();
		for (Layer layer: doc.getLayers()){
			for (Type<?> type: layer.getEntityTypes()){
				IEntityConverter c = getConverter(type);
				for (DraftEntity e: layer.getEntitiesByType(type)){
					geoms.add(c.convert(e));
				}
			}
		}
		return geoms;
	}
	
	IEntityConverter getConverter(Type<?> type){
		switch(type.getName()){
		case "3DFace": return new Convert3DFace(projection, this, null);
		case "3DSolid": return new ConvertSolid(projection, this, null);
		case "Arc": return new ConvertArc(projection, this, null);
//		case "Attdef": return new ConvertAttdef(projection);
//		case "Attrib": return new ConvertAttrib(projection);
//		case "Body": return new ConvertBody(projection);
//		case "Circle": return new ConvertCircle(projection);
//		case "Dimension": return new ConvertDimension(projection);
////		case "Entity": return new ConvertEntity(projection);
//		case "Ellipse": return new ConvertEllipse(projection);
//		case "Hatch": return new ConvertHatch(projection);
//		case "Image": return new ConvertImage(projection);
//		case "Insert": return new ConvertInsert(projection);
//		case "Leader":
//		case "Line":
//		case "LWPolyline":
//		case "MLine":
//		case "MText":
		case "Point": return new ConvertPoint(projection, this, null);
//		case "Polyline":
//		case "Ray":
//		case "Region":
//		case "Shape":
//		case "Solid":
//		case "Spline":
//		case "Text":
//		case "Tolerance":
//		case "Trace":
//		case "Vertex":
//		case "Viewport":
//		case "XLine":
			default: return null;
		}
	}

	public IDatabaseGeometryObjectReader getDB() {
		return dbG;
	}	
}