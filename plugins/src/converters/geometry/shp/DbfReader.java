package converters.geometry.shp;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.MathContext;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.luci.connect.LcInputStream;
import org.luci.connect.LcString;
import org.luci.utilities.TAB;

public class DbfReader{
	public class DbfRecord{
		private ByteBuffer record;
		private long JanuaryFirst1970;
		public DbfRecord(ByteBuffer record){
			this.record = record;
			this.record.order(ByteOrder.LITTLE_ENDIAN);
			GregorianCalendar gc = new GregorianCalendar(1,1,1);
			JanuaryFirst1970 = -gc.getTimeInMillis();
		}
		
		private int getStartPosition(int index){
			int i = 1;
			for (int j = 0; j < index; j++) i += fieldLengths[j];
			return i;
		}
		
		public byte getFlag(){
			return record.get(0);
		}
		
		public short getLcFlag(){
			if (record.get(0) == dbfDeleted) return TAB.flagDELETED;
			else return 0;
		}
		
		public String getString(String name){return getString(columnNames.indexOf(name));}
		public String getString(int index){
			if ((columnTypes[index] & stringFormats) == 0)
				throw new RuntimeException("Column '"+columnNames.get(index)+"' with type '"+getTypeName(columnTypes[index])+"' not convertible to string!");
			int len = fieldLengths[index];
			byte[] s = new byte[len];
			record.position(getStartPosition(index));
			record.get(s, 0, len);
			return new String(s).trim();
		}
		
		public String getStringWithByteArrayIndex(int i, int index){
			if ((columnTypes[index] & stringFormats) == 0)
				throw new RuntimeException("Column '"+columnNames.get(index)+"' with type '"+getTypeName(columnTypes[index])+"' not convertible to string!");
			int len = fieldLengths[index];
			byte[] s = new byte[len];
			record.position(i);
			record.get(s, 0, len);
			try {
				return new String(s, "UTF-8").trim().replace("\\x00", " ");
//				return "test";
			} catch (UnsupportedEncodingException e) {
				return "UTF-8 not supported strange world";
			}
		}
		
		public Date getDate(String name) {return getDate(columnNames.indexOf(name));}
		public Date getDate(int index){
			String YYYYMMDD = getString(index);
			int year = Integer.parseInt(YYYYMMDD.substring(1,4));
			int month= Integer.parseInt(YYYYMMDD.substring(5,6));
			int day  = Integer.parseInt(YYYYMMDD.substring(7,8));
			GregorianCalendar gc = new GregorianCalendar(year, month, day);
			return gc.getTime();
		}
		
		public BigDecimal getNumber(String name) {return getNumber(columnNames.indexOf(name));}
		public BigDecimal getNumber(int index){
			if (columnTypes[index] == O){
				return getDecimal(index);
			} else if(columnTypes[index] == A){
				return new BigDecimal(getTimestamp(index).getTime());
			}
			return new BigDecimal(getString(index));
		}
		
		public BigDecimal getDecimal(int index){
			return new BigDecimal(record.getDouble(getStartPosition(index)), new MathContext(decimalCount[index]));
		}
		
		private BigDecimal getDecimalWithByteArrayIndex(int i, int index){
			return new BigDecimal(record.getDouble(i), new MathContext(decimalCount[index]));
		}
		
		public long getLong(String name) {return getLong(columnNames.indexOf(name));}
		public long getLong(int index){
			if ((columnTypes[index] & stringFormats) == 0){
				return record.getLong(getStartPosition(index));
			} else {
				String number = getString(index);
				if (number.length() > 0) return Long.parseLong(number);
				else return 0;
			}
		}
		
		public float getFloat(String name) {return getFloat(columnNames.indexOf(name));}
		public float getFloat(int index){
			String number = getString(index);
			if (number.length() > 10) throw new RuntimeException(number+" has more than 10 digits, therefore cannot be parsed to a float!");
			if (number.length() > 0) return Float.parseFloat(number);
			else return 0;
		}
		
		public int getInt(String name) {return getInt(columnNames.indexOf(name));}
		public int getInt(int index){
			String number = getString(index);
			if (number.length() > 10) throw new RuntimeException(number+" has more than 10 digits, therefore cannot be parsed to an int!");
			if (number.length() > 0) return Integer.parseInt(number);
			else return 0;
		}
		
		public boolean getBoolean(String name) {return getBoolean(columnNames.indexOf(name));}
		public boolean getBoolean(int index){
			char bool = (char) record.get(getStartPosition(index));
			switch(bool){
			case 'T': return true;
			case 'F': return false;
			default: return false;
			}
		}
		
		public Timestamp getTimestamp(String name) {return getTimestamp(columnNames.indexOf(name));}
		public Timestamp getTimestamp(int index){
			// as stated on several websites a dbf timestamp is not related to 01/01/4713BC but a double
			// denoting the milliseconds since 01/01/0001
			return new Timestamp((long) record.getDouble(getStartPosition(index)) - JanuaryFirst1970);
		}
		
		private String listOfQ(int i){
			if (i > 0){
				StringBuilder list = new StringBuilder("?");
				for (int j = 0; j < i-1; j++) list.append(", ?");
				return list.toString();
			} else {
				return "";
			}
		}
		
		private void setNumberString(String numberString, PreparedStatement ps, int i1, int i2,  int i3) throws SQLException{
			try{
				BigDecimal number = new BigDecimal(numberString);
				ps.setString(i1, "number");
				ps.setNull(i2, Types.VARCHAR);
				ps.setBigDecimal(i3, number);
			} catch (NumberFormatException e){
				ps.setString(i1, "string");
				ps.setString(i2, numberString);
				ps.setNull(i3, Types.NUMERIC);
				dbfErrors.add(e.getMessage());
			}
		}
		
		private void setNull(PreparedStatement ps, int i1, int i2, int i3) throws SQLException{
			ps.setNull(i1, Types.VARCHAR);
			ps.setNull(i2, Types.VARCHAR);
			ps.setNull(i3, Types.DOUBLE);
		}
		
		/**
		 * @param ps PreparedStatement
		 * @param fields
		 * @param attributeMap JSONObject describing which dbf column names should be mapped to 
		 * luci's geometry attributes such as geomid, timestamp or uid
		 * @return geomid if present else 0
		 * @throws SQLException
		 * @throws UnsupportedEncodingException 
		 */
		public void transferTo(long geomid, Timestamp st, long uid, Connection con) throws SQLException, UnsupportedEncodingException{
			String tab = TAB.getScenarioTable(ScID, TAB.SCN);
			List<String> fields = Arrays.asList(
					new String[]{"scid", "geomid", "timestamp", "keyname", "format", "text", "number", "uid", "flag"});
			StringBuilder sql = new StringBuilder("INSERT INTO "+tab+"("+LcString.trim(fields.toString(), 1)+") VALUES");
			
//			int numAttrColumns = attrColumns.size();
			int numFields = fields.size();
//			String coma = "";
//			for (int i = 0; i < numColumns; i++) {
//				sql.append(coma + "("+listOfQ(numFields)+")");
//				if (i == 0) coma = ", ";
//			}
//			sql.append(";");
//			String test = new String(sql.getValue(), "UTF-8");
			PreparedStatement ps = con.prepareStatement(sql.toString());

//			System.out.println(numAttrColumns);
//			System.out.println(numColumns);
			
			int f = fields.indexOf("format") + 1;
			int t = fields.indexOf("text") + 1;
			int n = fields.indexOf("number") + 1;
			int jj = 0;
			for (int i = 1, j = 0; j <  numColumns; i += fieldLengths[j++]){
				if (!attrColumns.contains(j)) continue;
				String attr = columnNames.get(j);
				ps.setLong(		jj+1, ScID);
				ps.setLong(		jj+2, geomid);
				ps.setTimestamp(jj+3, st);
				ps.setString(	jj+4, attr);

				switch(columnTypes[j]){
				case C:
					ps.setString(jj+t, getStringWithByteArrayIndex(i, j));
					ps.setString(jj+f, "string");
					ps.setNull(jj+n, Types.NUMERIC);
					ps.setBigDecimal(jj+n, new BigDecimal(0));
					break;
				case D:
					ps.setString(jj+f, "string");
					ps.setString(jj+t, getStringWithByteArrayIndex(i, j));
					ps.setNull(jj+n, Types.NUMERIC);
					break;
				case N:
					setNumberString(getStringWithByteArrayIndex(i, j), ps, jj+f,jj+t,jj+n);
					//setNull(ps, jj+f, jj+t, jj+n);
					break;
				case F:
					setNumberString(getStringWithByteArrayIndex(i, j), ps, jj+f,jj+t,jj+n);
					//setNull(ps, jj+f, jj+t, jj+n);
					break;
				case L:
					char b = (char) record.get(i);
					ps.setString(jj+f, "bool");
					ps.setNull(jj+t, Types.VARCHAR);
					ps.setInt(jj+n, (b == 'T') ? 1 : 0);
					break;
				case A:
					dbfErrors.add("'"+attr+"' is of type '"+getTypeName(A)+"' but not listed in the attributeMap!");
					setNull(ps, jj+f, jj+t, jj+n);
					break;
				case I:
					ps.setString(jj+f, "number");
					ps.setNull(jj+t, Types.VARCHAR);
					ps.setInt(jj+n, record.getInt(i));
					break;
				case O:
					ps.setString(jj+f, "number");
					ps.setNull(jj+t, Types.VARCHAR);
					ps.setBigDecimal(jj+n, getDecimalWithByteArrayIndex(i, j));
					break;
				}
				ps.setLong(jj+8, uid);
				ps.setLong(jj+9, 0); //getLcFlag());
				
				jj += numFields;
			}
			try{
				if (numColumns > 0) ps.execute();
			} catch (SQLException e){
//				throw e;
				
				e.printStackTrace();
				throw new SQLException(e.getMessage() +" "+ ps.toString() +" "+ columnNames);
			}
		}
		
		public Object get(String keyname){
			return get(columnNames.indexOf(keyname));
		}
		
		public Object get(int i){
			switch(columnTypes[i]){
			case C: return getString(i);
			case D: return getDate(i);
			case N: return getNumber(i);
			case F: return getNumber(i);
			case L:	return ((char) record.get(getStartPosition(i)) == 'T') ? true : false;
			case A: return getTimestamp(i);
			case I: return record.getInt(getStartPosition(i));
			case O: return getDecimal(i);
			default: return null;
			}
		}
	}
	protected Set<String> dbfErrors;
	protected int numRecords;
	protected int recordSize;
	protected int numColumns;
	protected List<String> columnNames;
	protected List<Integer> attrColumns;
	protected short[] columnTypes;
	protected short[] fieldLengths;
	protected short[] decimalCount;
	protected int headerLength;
	protected byte version;
	public final static short B = 0b000000000001; // DBT block numbers not covered here
	public final static short C = 0b000000000010;
	public final static short D = 0b000000000100;
	public final static short N = 0b000000001000;
	public final static short L = 0b000000010000;
	public final static short M = 0b000000100000; // DBT block numbers not covered here
	public final static short A = 0b000001000000; // stands for @, Timestamp
	public final static short I = 0b000010000000;
	public final static short P = 0b000100000000; // stands for +, Autoincrement
	public final static short F = 0b001000000000;
	public final static short O = 0b010000000000;
	public final static short G = 0b100000000000; // DBT block numbers not covered here
	public final static short stringFormats = B|C|D|F|N|M|G;
	public final byte dbfDeleted = 0x2A;
	public final byte dbfLevel5 = 0b011;
	public final byte dbfLevel7 = 0b100;
	public final int dbf5HeaderLength = 32;
	public final int dbf7HeaderLength = 68;
	public final int dbf5FieldDescriptor = 32;
	public final int dbf7FieldDescriptor = 48;
	
	public final static Map<Short, Short> dbf2TAB_types = new HashMap<Short, Short>();
	static{
		dbf2TAB_types.put(B, TAB.STRING);
		dbf2TAB_types.put(C, TAB.STRING);
		dbf2TAB_types.put(D, TAB.TIMESTAMP);
		dbf2TAB_types.put(N, TAB.NUMERIC);
		dbf2TAB_types.put(L, TAB.BOOLEAN);
		dbf2TAB_types.put(M, TAB.STRING);
		dbf2TAB_types.put(A, TAB.TIMESTAMP);
		dbf2TAB_types.put(I, TAB.INTEGER);
		dbf2TAB_types.put(P, TAB.NUMERIC);
		dbf2TAB_types.put(F, TAB.NUMERIC);
		dbf2TAB_types.put(O, TAB.NUMERIC);
		dbf2TAB_types.put(G, TAB.STRING);
	}
	
	/* dbfLevel7: http://dbase.com/Knowledgebase/INT/db7_file_fmt.htm
	 * dbfLevel5: http://ulisse.elettra.trieste.it/services/doc/dbase/DBFstruct.htm
	 * 
	 * Symbol	Data Type			Description
	 * B		Binary, a string	10 digits representing a .DBT block number. The number is stored as a string, right justified and padded with blanks.
	 * C		Character			All OEM code page characters - padded with blanks to the width of the field.
	 * D		Date				8 bytes - date stored as a string in the format YYYYMMDD.
	 * N		Numeric				Number stored as a string, right justified, and padded with blanks to the width of the field. 
	 * L		Logical				1 byte - initialized to 0x20 (space) otherwise T or F.
	 * M		Memo, a string		10 digits (bytes) representing a .DBT block number. The number is stored as a string, right justified and padded with blanks.
	 * @		Timestamp			8 bytes - two longs, first for date, second for time.  The date is the number of days since  01/01/4713 BC. Time is hours * 3600000L + minutes * 60000L + Seconds * 1000L
	 * I		Long				4 bytes. Leftmost bit used to indicate sign, 0 negative.
	 * +		Autoincrement		Same as a Long
	 * F		Float				Number stored as a string, right justified, and padded with blanks to the width of the field. 
	 * O		Double				8 bytes - no conversions, stored as a double.
	 * G		OLE					10 digits (bytes) representing a .DBT block number. The number is stored as a string, right justified and padded with blanks.
	 * */
	
	protected List<ByteBuffer> dbfData;
	protected Map<String, String> attributeMap;
	protected long ScID;
	
//	public DbfReader(List<ByteBuffer> bytes, Map<String, String> attributeMap, long ScID){
//		dbfData = bytes;
//		this.attributeMap = attributeMap;
//		this.ScID = ScID;
//		readHeader();
//		setAttributeColumnIndices();
//	}
	
	public DbfReader(LcInputStream lis, long ScID) throws IOException{
//		ByteArrayInputStream in = new ByteArrayInputStream(bb.array());
		dbfData = new ArrayList<ByteBuffer>();
		dbfErrors = new HashSet<String>();
		this.attributeMap = lis.attributeMap;
		this.ScID = ScID;
		version = getDBFVersion(lis.read()); // read 1 byte to detect version
		headerLength = (version == dbfLevel5) ? dbf5HeaderLength : dbf7HeaderLength;
		
		byte[] headerB = new byte[headerLength];
		lis.read(headerB, 1, headerLength - 1);
		ByteBuffer header = ByteBuffer.wrap(headerB);
		header.order(ByteOrder.LITTLE_ENDIAN);
		header.position(8);
		int numBytes = header.getShort();
		header.rewind();
		
		byte[] columsB = new byte[numBytes-headerLength];
		lis.read(columsB, 0, numBytes-headerLength);
		ByteBuffer columns = ByteBuffer.wrap(columsB);
		
		readHeader(header, columns);
		dbfData = new ArrayList<ByteBuffer>(numRecords + 2);
		dbfData.add(0, header);
		dbfData.add(1, columns);
		setAttributeColumnIndices();
		for (int i = 2; i < numRecords + 2; i++){
			byte[] rec = new byte[recordSize];
			lis.read(rec, 0, recordSize);
			dbfData.add(i,ByteBuffer.wrap(rec));
		}
		lis.read(); // EOF
	}
	
	private byte getDBFVersion(int firstByte){
		if ((firstByte & dbfLevel5) == dbfLevel5) return dbfLevel5;
		if ((firstByte & dbfLevel7) == dbfLevel7) return dbfLevel7;
		return 0;
	}
	
	private void setAttributeColumnIndices(){
		List<String> attrColumnNames = new ArrayList<String>(columnNames);
		attrColumnNames.removeAll(attributeMap.values());
		if (hasName("id")) attrColumnNames.remove("id");
		if (hasName("uid")) attrColumnNames.remove("uid");
		if (hasName("layer")) attrColumnNames.remove("layer");
		if (hasName("timestamp")) attrColumnNames.remove("timestamp");
		attrColumns = new ArrayList<Integer>();
		for (String columnName: attrColumnNames) attrColumns.add(columnNames.indexOf(columnName));
	}
	
	private short getType(byte b){
		switch((char)b){
		case 'B': return B;
		case 'C': return C;
		case 'D': return D;
		case 'N': return N;
		case 'L': return L;
		case 'M': return M;
		case '@': return A;
		case 'I': return I;
		case '+': return P;
		case 'F': return F;
		case 'O': return O;
		case 'G': return G;
		default: return 0;
		}
	}
	
	protected String getTypeName(short t){
		switch(t){
		case B: return "(B)Binary";
		case C: return "(C)Character";
		case D: return "(D)Date";
		case N: return "(N)Numeric";
		case L: return "(L)Logical";
		case M: return "(M)Memo";
		case A: return "(@)Timestamp";
		case I: return "(I)Long";
		case P: return "(+)Autoincrement";
		case F: return "(F)Float";
		case O: return "(O)Double";
		case G: return "(G)OLE";
		default: return "unknown";
		}
	}
	
	private void readHeader(ByteBuffer header, ByteBuffer columnHeader){
		numRecords = header.getInt(4);
		int numBytes = header.getShort(8);
		recordSize = header.getShort(10);
		
		int columnsLength = numBytes - headerLength - 1;
		int fieldDescLength = (version == dbfLevel5) ? 32 : 48;
		numColumns =  columnsLength/fieldDescLength;
		
		columnNames = new ArrayList<String>();
		columnTypes = new short[numColumns];
		fieldLengths= new short[numColumns];
		decimalCount= new short[numColumns];
//		System.out.println("numRecords: "+ numRecords);
//		System.out.println("numBytes: "+ numBytes);
//		System.out.println("recordSize: "+ recordSize);
//		System.out.println("numColumns: "+ numColumns);
		
		if (version == dbfLevel5){
			for (int i = 0, j = 0; i < columnsLength; i += fieldDescLength, j++){
				byte[] nameBytes = new byte[11];
				columnHeader.position(i);
				columnHeader.get(nameBytes);
				try {
					columnNames.add(new String(nameBytes, "ASCII").trim());
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				columnTypes[j]  = getType(columnHeader.get(i + 11));
				fieldLengths[j] = columnHeader.get(i + 16);
				decimalCount[j] = columnHeader.get(i + 17);
			}
		} else if (version == dbfLevel7){
			for (int i = 0, j = 0; i < columnsLength; i += fieldDescLength, j++){
				byte[] nameBytes = new byte[32];
				((ByteBuffer)columnHeader.position(i)).get(nameBytes);
				columnNames.add(new String(nameBytes));
				columnTypes[j]  = getType(columnHeader.get(i + 32));
				fieldLengths[j] = columnHeader.get(i + 33);
				decimalCount[j] = columnHeader.get(i + 34);
			}
		}
//		System.out.println("columnNames: "+ columnNames);
//		System.out.println("columnTypes: "+ Arrays.toString(columnTypes));
//		System.out.println("fieldLengths: "+ Arrays.toString(fieldLengths));
//		System.out.println("decimalCount: "+ Arrays.toString(decimalCount));
	}
	
	public boolean hasType(short type){
		for (short t: columnTypes) if (type == t) return true;
		return false;
	}
	
	public boolean hasName(String name){
		return columnNames.contains(name);
	}

	public DbfRecord getRecord(int index){
		// first two byte arrays  belong to header
		return new DbfRecord(dbfData.get(index+2));
	}
	
	public List<ByteBuffer> getDbfData(){
		return dbfData;
	}
	
	public int getNumRecords(){
		return numRecords;
	}
	
	public Set<String> getErrors(){
		return dbfErrors;
	}


	public Short getTABType(int i) {
		return dbf2TAB_types.get(columnTypes[i]);
	}
	
	public Short getType(String col){
		return columnTypes[columnNames.indexOf(col)];
	}
}
