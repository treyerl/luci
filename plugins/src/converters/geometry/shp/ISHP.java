package converters.geometry.shp;


public interface ISHP {
	public final int SHPNullShape = 0;
	public final int SHPPoint = 1;
	public final int SHPPolyLine = 3;
	public final int SHPPolygon = 5; 
	public final int SHPMultiPoint = 8;
	public final int SHPPointZ = 11;
	public final int SHPPolyLineZ = 13;
	public final int SHPPolygonZ = 15;
	public final int SHPMultiPointZ = 18;
	public final int SHPPointM = 21; 
	public final int SHPPolyLineM = 23; 
	public final int SHPPolygonM = 25;
	public final int SHPMultiPointM = 28;
	public final int SHPMultiPatch = 31;
	
	public final static int TriangleStrip = 0;
	public final static int TriangleFan = 1;
	public final static int OuterRing = 2;
	public final static int InnerRing = 3;
	public final static int FirstRing = 4;
	public final static int Ring = 5;
	
	public final static int ZM = 4;
	public final static int M  = 3;
	public final static int D2 = 2;
	
	public final int RINGSIterator = 0;
	public final int LINESIterator = 1;
	public final int POINTSIterator = 2;
	public final int POINTIterator = 3;
	public final int POLYGONSIterator = 4;
	
}
