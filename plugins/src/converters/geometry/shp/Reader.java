package converters.geometry.shp;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import org.eclipse.jetty.util.ByteArrayOutputStream2;
import org.json.JSONArray;
import org.json.JSONException;
import org.luci.connect.DirectStreamWriter;
import org.luci.connect.LcInputStream;
import org.luci.connect.LcOutputStream;
import org.luci.converters.IMultiStreamReader;
import org.luci.converters.geometry.GeometryReader;
import org.luci.factory.observer.FactoryObserver;
import org.luci.utilities.Projection;
import org.luci.utilities.TAB;

import converters.geometry.shp.iterators.MultiPatch;
import converters.geometry.shp.iterators.MultiPoint;
import converters.geometry.shp.iterators.MultiPointM;
import converters.geometry.shp.iterators.MultiPointZ;
import converters.geometry.shp.iterators.Point;
import converters.geometry.shp.iterators.PointM;
import converters.geometry.shp.iterators.PointZ;
import converters.geometry.shp.iterators.Polygon;
import converters.geometry.shp.iterators.PolygonM;
import converters.geometry.shp.iterators.PolygonZ;
import converters.geometry.shp.iterators.Polyline;
import converters.geometry.shp.iterators.PolylineM;
import converters.geometry.shp.iterators.PolylineZ;

public class Reader extends GeometryReader implements IMultiStreamReader, ISHP {
	
	// shp file header info
	protected long shapefileLength16bit;
	protected long shapefileLength8bit;
	protected int shapetype;
	protected double[] bbox;
	protected final int shpFileHeaderSize8bit = 100;
	protected final int fileHeaderSize16bit = 50;
	
	// luci
	protected String filename;
	protected int numRecords;
	protected ByteBuffer shp;
	protected String prj;
	protected List<byte[]> shx;
	protected List<byte[]> dbf;
	protected List<ByteBuffer> shpData;
	protected DbfReader dbfReader;
	
	public Reader(FactoryObserver o, int ScID, long uid, Timestamp t, Projection pc, 
			JSONArray errors) throws InstantiationException {
		super(o, ScID, uid, t, pc, errors);
	}
	
	@Override
	public Set<String> fileFormatSuffixes() {
		HashSet<String> f = new HashSet<String>();
		f.add("shp");
		f.add("dbf");
		f.add("shx");
		return f;
	}

	@Override
	public String getMainFormatSuffix() {
		return "shp";
	}

	/* (non-Javadoc)
	 * @see org.luci.converters.IMultiStreamReader#readStreams(java.util.List)
	 */
	@Override
	public void readStream(LcInputStream lis) throws Exception {
		switch(lis.format){
		case "shp": readShpStream(lis); break;
		case "shx": readShxStream(lis); break;
		case "dbf": readDbfStream(lis); break;
		}
	}
	
	private List<ByteBuffer> readDbfStream(LcInputStream lis) throws IOException, InterruptedException{
		if (!lis.waitUntilReady(20, TimeUnit.SECONDS)) System.out.println("timeout on DBF read");
		dbfReader = new DbfReader(lis, ScID);
		lis.setDone();
		return dbfReader.getDbfData();
	}
	
	private void readShpStream(LcInputStream lis) throws InterruptedException, ExecutionException, IOException{
		ByteArrayOutputStream2 bos = new ByteArrayOutputStream2(lis.getLength());
		LcOutputStream los = new LcOutputStream(lis, bos);
		los.setReady();
		o.getThreadPool().submit(new DirectStreamWriter(lis, los, false, false, true, true)).get();
		shp = ByteBuffer.wrap(bos.getBuf());
		los.close();
	}
	
	private void readShxStream(LcInputStream lis) throws IOException, InterruptedException{
		if (!lis.waitUntilReady(20, TimeUnit.SECONDS)) System.out.println("timeout on SHX read");
		long lcLength = lis.getLength();
		byte[] header = new byte[shpFileHeaderSize8bit];
		lis.read(header, 0, shpFileHeaderSize8bit);
		ByteBuffer headerBuf = ByteBuffer.wrap(header);
		int shxFileLength16bit = headerBuf.getInt(24); 
		int shxFileLength8bit = shxFileLength16bit*2;
		if (lcLength != shxFileLength8bit) 
			errors.put("Filesize recorded in shxfile header ("+shxFileLength8bit+") does not meet the given file size("+lcLength+")!");
		long length = shpFileHeaderSize8bit;
		int numRecords = (int) ((lcLength - length) / 8);
		shx = new ArrayList<byte[]>(numRecords + 1);
		shx.add(0,header);
		this.numRecords = numRecords / 2;
		
		for (int i = 1; i < numRecords + 1; i++){
			byte[] rec = new byte[8];
			lis.read(rec, 0, 8);
			shx.add(i, rec);
		}
		lis.setDone();
	}
	
	public byte[] getShxRecord(int index){
		// first byte array is header; every shape has a record header
		return shx.get(2 + 2 * index);
	}
	
	protected double[] read2DBBoxLittleEndian(ByteBuffer bb, int i){
		double[] bbox = new double[4];
		read2DBBoxLittleEndian(bb, i, bbox);
		return bbox;
	}
	
	protected void read2DBBoxLittleEndian(ByteBuffer bb, int i, double[] bbox){
		assert bbox.length == 4;
		bb.order(ByteOrder.LITTLE_ENDIAN);
		bbox[0] = bb.getDouble(i);
		bbox[1] = bb.getDouble(i + 8);
		bbox[2] = bb.getDouble(i + 16);
		bbox[3] = bb.getDouble(i + 24);
	}
	
	protected String readHex(byte[] b, int i, int len){
		StringBuilder sb = new StringBuilder(len);
		for (int j = 0; j < len; j++){
			sb.append(String.format("%x", b[i + j] & 0xff));
		}
		return sb.toString();
	}
	
	public int[] getPartEnds(int[] partStartIndices, int numParts, int numPoints){
		// [[0,5],[5,9],[9,13],[13,18]] --> startIndices = [0,5,9,13] | endIndices = [5,9,13,18]
		int[] partEndIndices = new int[numParts];
		for (int i = 1; i < numParts; i++){
			partEndIndices[i-1] = partStartIndices[i]-1;
		}
		partEndIndices[numParts-1] = numPoints-1;
		return partEndIndices;
	}
	
	private List<ByteBuffer> readShp() throws IOException{
		shpData = new ArrayList<ByteBuffer>(shx.size());
		shp.rewind();
		shp.limit(shpFileHeaderSize8bit);
		ByteBuffer header = shp.slice();
		shpData.add(0,header);
		readShpHeader(header);
		long lcLength = shp.capacity();
		if (lcLength != shapefileLength8bit) 
			errors.put("Filesize recorded in shapefile header ("+shapefileLength8bit+") does not meet the given file size("+lcLength+")!");
		long length = 0;
		shp.position(shpFileHeaderSize8bit);
		
		int i = 1;
		while(length < lcLength - shpFileHeaderSize8bit){
			shp.limit(shp.position() + 8);
			ByteBuffer recordHeader = shp.slice();
			int contentLength = recordHeader.getInt(4) * 2;
			shp.position(shp.limit());
			shp.limit(shp.position() + contentLength);
			ByteBuffer content = shp.slice();
			shpData.add(i++,recordHeader);
			shpData.add(i++,content);
			length += contentLength + 8;
			shp.position(shp.limit());
		}
		return shpData;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private Object polylineSwitch(Polyline pl){
		if (pl.getNumParts() == 1) return dbG.toLineStringObject(pl.next(), projection);
		else return dbG.toMultiLineStringObject(pl, projection);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private Object polygonSwitch(Polygon p){
		if( p.isMulti()) return dbG.toMultiPolygonObject(p, projection);
		return dbG.toPolygonObject(p, projection);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private byte[] polylineSwitchB(Polyline pl){
		if (pl.getNumParts() == 1) return dbG.toLineStringBytes(pl.next(), projection);
		else return dbG.toMultiLineStringBytes(pl, projection);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private byte[] polygonSwitchB(Polygon p){
		if( p.isMulti()) return dbG.toMultiPolygonBytes(p, projection);
		return dbG.toPolygonBytes(p, projection);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked", "unused" })
	private Object readShape(ByteBuffer bb) throws IOException, SQLException, NoSuchMethodException, SecurityException{
		switch(shapetype){
		case SHPPoint: 	 	return dbG.toPointObject(new Point(bb), projection);
		case SHPPolyLine: 	return polylineSwitch(new Polyline(bb));
		case SHPPolygon: 	return polygonSwitch(new Polygon(bb)); 
		case SHPMultiPoint: return dbG.toMultiPointObject(new MultiPoint(bb), projection);
		case SHPPointZ: 	return dbG.toPointObject(new PointZ(bb), projection);
		case SHPPolyLineZ:  return polylineSwitch(new PolylineZ(bb));
		case SHPPolygonZ:   return polygonSwitch(new PolygonZ(bb));
		case SHPMultiPointZ:return dbG.toMultiPointObject(new MultiPointZ(bb), projection);
		case SHPPointM: 	return dbG.toPointObject(new PointM(bb), projection);
		case SHPPolyLineM:  return polylineSwitch(new PolylineM(bb));
		case SHPPolygonM:   return polygonSwitch(new PolygonM(bb));
		case SHPMultiPointM:return dbG.toMultiPointObject(new MultiPointM(bb), projection);
		case SHPMultiPatch: 
			MultiPatch pa = new MultiPatch(bb);
			if (pa.isAllRings()){
				if (pa.isSinglePoly()) return dbG.toPolygonObject(pa, projection);
				return dbG.toMultiPolygonObject(pa, projection);
			}
			return dbG.toGeometryCollectionObject(pa, projection);
		default: throw new NoSuchMethodException(shapeTypeName());
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private byte[] readShapeB(ByteBuffer bb) throws IOException, SQLException, NoSuchMethodException, SecurityException{
		switch(shapetype){
		case SHPPoint: 	 	return dbG.toPointBytes(new Point(bb), projection);
		case SHPPolyLine: 	return polylineSwitchB(new Polyline(bb));
		case SHPPolygon: 	return polygonSwitchB(new Polygon(bb)); 
		case SHPMultiPoint: return dbG.toMultiPointBytes(new MultiPoint(bb), projection);
		case SHPPointZ: 	return dbG.toPointBytes(new PointZ(bb), projection);
		case SHPPolyLineZ:  return polylineSwitchB(new PolylineZ(bb));
		case SHPPolygonZ:   return polygonSwitchB(new PolygonZ(bb));
		case SHPMultiPointZ:return dbG.toMultiPointBytes(new MultiPointZ(bb), projection);
		case SHPPointM: 	return dbG.toPointBytes(new PointM(bb), projection);
		case SHPPolyLineM:  return polylineSwitchB(new PolylineM(bb));
		case SHPPolygonM:   return polygonSwitchB(new PolygonM(bb));
		case SHPMultiPointM:return dbG.toMultiPointBytes(new MultiPointM(bb), projection);
		case SHPMultiPatch: 
			MultiPatch pa = new MultiPatch(bb);
			if (pa.isAllRings()){
				if (pa.isSinglePoly()) return dbG.toPolygonBytes(pa, projection);
				return dbG.toMultiPolygonBytes(pa, projection);
			}
			return dbG.toGeometryCollectionBytes(pa, projection);
		default: throw new NoSuchMethodException(shapeTypeName());
		}
	}
	
	protected String shapeTypeName(){
		switch(shapetype){
		case SHPPoint: 		return "Point";
		case SHPPolyLine: 		return "PolyLine";
		case SHPPolygon: 		return "Polygon"; 
		case SHPMultiPoint: 	return "MultiPoint";
		case SHPPointZ: 		return "PointZ";
		case SHPPolyLineZ: 	return "PolyLineZ";
		case SHPPolygonZ: 		return "PolygonZ";
		case SHPMultiPointZ: 	return "MultiPointZ";
		case SHPPointM: 		return "PointM"; 
		case SHPPolyLineM: 	return "PolyLineM"; 
		case SHPPolygonM: 		return "PolygonM";
		case SHPMultiPointM: 	return "MultiPointM";
		case SHPMultiPatch: 	return "MultiPatch";
		default: 			return "NULL";
		}
	}
	
	private void readShpHeader(ByteBuffer header) throws IOException{
		header.rewind();
		header.order(ByteOrder.BIG_ENDIAN);
//		int filecode = header.getInt(0);
		shapefileLength16bit = header.getInt(24); // file length is in 16bit words
		shapefileLength8bit = shapefileLength16bit*2;
		
		header.order(ByteOrder.LITTLE_ENDIAN);
//		int version = header.getInt(28);
		shapetype = header.getInt(32);
		bbox = new double[8];
		bbox[0] = header.getDouble(36); // xmin 0
		bbox[1] = header.getDouble(44); // ymin 1
		bbox[2] = header.getDouble(52); // xmax 2
		bbox[3] = header.getDouble(60); // ymax 3
		bbox[4] = header.getDouble(68); // zmin 4
		bbox[5] = header.getDouble(76); // zmax 5
		bbox[6] = header.getDouble(84); // mmin 6
		bbox[7] = header.getDouble(92); // mmax 7
	}
	
	
	private ByteBuffer getShapeRecord(int index){
		// first byte array is header; every shape has a record header
		return shpData.get(2 + 2 * index);
	}
	private List<Long> doCall(Connection con) throws Exception {
		if (shx == null) throw new JSONException("shx file missing");
		if (shp == null) throw new JSONException("shp file missing");
		readShp();
//		System.out.println(shapetype);
		List<Long> newIds = new ArrayList<Long>();
		
		String insert = "INSERT INTO "+data+"(geomID, timestamp, batchID, layer, nurb, transform, uid, flag, geom";
		String insertAutoID = "INSERT INTO "+data+"(timestamp, batchID, layer, nurb, transform, uid, flag, geom ";
		
		String ID = dbfReader.attributeMap.getOrDefault("geomID", "geomID"); // JAVA8
		String LAYER = dbfReader.attributeMap.getOrDefault("layer", "layer"); // JAVA8
		String BATCHID = dbfReader.attributeMap.getOrDefault("batchID", "batchID"); // JAVA8
		String NURB = dbfReader.attributeMap.getOrDefault("nurb", "nurb"); // JAVA8
		String TRANSFORM = dbfReader.attributeMap.getOrDefault("transform", "transform"); // JAVA8
		String TIMESTAMP = dbfReader.attributeMap.getOrDefault("timestamp", "timestamp"); // JAVA8
		SimpleDateFormat sdf = new SimpleDateFormat(dbfReader.attributeMap.getOrDefault("timestampformat", "YYMMDD"));
		
		List<String> lcAttributes = Arrays.asList(LAYER,BATCHID,NURB,TRANSFORM,TIMESTAMP);
		boolean isAutoID = !dbfReader.hasName(ID);
		StringBuilder sql;
		int colCount = 0;
		if (!isAutoID) {
			sql = new StringBuilder(insert);
			colCount = 9;
		} else {
			sql = new StringBuilder(insertAutoID);
			colCount = 8;
		}
		
		Map<String, Short> attributes = new LinkedHashMap<String, Short>();
		int c = 0;
		for (String col: dbfReader.columnNames){
			if (!lcAttributes.contains(col)) {
				attributes.put(col, dbfReader.getTABType(c++));
				sql.append(", "+col);
			}
			else c++;
		}
		colCount += attributes.size();
		sql.append(") VALUES(?");
		sql.append(org.luci.connect.LcString.fillQuantity(colCount-1, ",?")+")");

		db.addColumns(con, ScID, attributes);
		PreparedStatement ps ;
		if (isAutoID) ps = con.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
		else ps = con.prepareStatement(sql.toString());
		
		long recordCount = (shpData.size() - 1 ) / 2;
		if (dbfReader != null) if (dbfReader.getNumRecords() != recordCount) 
			throw new RuntimeException("Number of shapefile records does not fit number of dbf records; "+recordCount+", "+dbfReader.numRecords);
		String timestampError = null;
		Exception timestampE = null;
		for (int i = 0; i < recordCount; i++){
			DbfReader.DbfRecord dbfRecord = null;
			long geomid = 0;
			String layer = null;
			int j = 1;
			if (dbfReader != null) dbfRecord = dbfReader.getRecord(i);
			if (!isAutoID) ps.setLong(j++, dbfRecord.getLong(ID));
			if (dbfReader != null){
				if (dbfReader.hasName(TIMESTAMP)){
					short type = dbfReader.getType(TIMESTAMP);
					if ((type & DbfReader.stringFormats) != 0) {
						ps.setTimestamp(j++, new Timestamp(sdf.parse(dbfRecord.getString(TIMESTAMP)).getTime()));
					} else {
						ps.setTimestamp(j++, dbfRecord.getTimestamp(TIMESTAMP));
					}
				} else ps.setTimestamp(j++, st);
				if (dbfReader.hasName(BATCHID)) ps.setInt(j++, dbfRecord.getInt(BATCHID));
				else ps.setInt(j++, 1);
				if (dbfReader.hasName(LAYER)) ps.setString(j++, dbfRecord.getString(LAYER));
				else ps.setNull(j++, Types.VARCHAR);
				if (dbfReader.hasName(NURB)) ps.setInt(j++, dbfRecord.getInt(NURB));
				else ps.setNull(j++, Types.INTEGER);
				if (dbfReader.hasName(TRANSFORM)) ps.setArray(j++, new TAB.Transform(new double[4][]).getSQLArray(con));
				else ps.setNull(j++, Types.ARRAY);
				ps.setLong(j++, uid);
				ps.setNull(j++, Types.INTEGER); //flag
			}
			
			ps.setBytes(j++, readShapeB(getShapeRecord(i)));		
			
		    for (Map.Entry<String, Short> attribute_entry : attributes.entrySet()) {
		    	String key = new String(dbfRecord.getString(dbfReader.columnNames.indexOf(attribute_entry.getKey())));
		    	if(attribute_entry.getValue()==TAB.NUMERIC) {
		    		if(key == null || key.isEmpty()) ps.setNull(j++, Types.NUMERIC);
		    		else {
						BigDecimal att = dbfRecord.getNumber(attribute_entry.getKey());
						ps.setBigDecimal(j++, att);
		    		}
				}
				else if(attribute_entry.getValue()==TAB.INTEGER) {
		    		if(key == null || key.isEmpty()) ps.setNull(j++, Types.INTEGER);
		    		else {
						long att = dbfRecord.getLong(attribute_entry.getKey());
						ps.setLong(j++, att);
		    		}
				}
				else if(attribute_entry.getValue()==TAB.STRING) {
		    		if(key == null || key.isEmpty()) ps.setNull(j++, Types.LONGVARCHAR);
		    		else {
						String att = dbfRecord.getString(attribute_entry.getKey());
						ps.setString(j++, att);
		    		}
				}
		    }					
		     
			ps.execute();
		    
			if (geomid == 0){
				ResultSet rs = ps.getGeneratedKeys();
				while(rs.next()){
					geomid = rs.getLong(1);
					newIds.add(geomid);
				}
			}
		    
//			if (dbfRecord != null) dbfRecord.transferTo(geomid, st, uid, con);
//			if (n == 315)
//				System.out.println(n);
			
		}
		ps.close();
		
		if (timestampError != null) errors.put(timestampError);
		if (dbfReader != null) 
			for (String error: dbfReader.getErrors()) errors.put(error);
		if (timestampE != null) timestampE.printStackTrace();
		return newIds;
	}
	@Override
	public List<Long> call() throws Exception {
		Connection con = o.getDatabaseConnection();
//		long t = System.currentTimeMillis();
		try{
			List<Long> newIds = doCall(con);
			con.close();
//			System.err.println((System.currentTimeMillis() - t)+"ms");
			return newIds;
		} catch (Exception e){
			con.close();
			String m = e.getMessage();
			if (m != null) System.err.println(m);
			throw e;
		}
	}
	
	
	
}
