/**
 * The MIT License (MIT)
 *
 * Copyright (c) <year> <copyright holders>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package converters.geometry.shp;

import java.nio.ByteBuffer;

/**
 * 
 * @author Lukas Treyer
 *
 */
public class SHP {

	public static int[] getPartEnds(ByteBuffer bb, int numParts, int numPoints, int[] partStartIndices){
		// [[0,5],[5,9],[9,13],[13,18]] --> startIndices = [0,5,9,13] | endIndices = [5,9,13,18]
		int[] partEndIndices = new int[numParts];
		partStartIndices[0] = bb.getInt();
		for (int i = 1; i < numParts; i++){
			partStartIndices[i] = bb.getInt();
			partEndIndices[i-1] = partStartIndices[i]-1;
		}
		partEndIndices[numParts-1] = numPoints-1;
		return partEndIndices;
	}
	
	public static ByteBuffer finalize(double[] bbox, int pointStartPosition, ByteBuffer ... buffs){
		double zmin = bbox[4];
		double zmax = bbox[5];
		double mmin = bbox[6];
		double mmax = bbox[7];
		ByteBuffer bb = buffs[0];
		
		if (buffs.length > 1){
			ByteBuffer z = buffs[1];
			bb.putDouble(zmin);
			bb.putDouble(zmax);
			z.rewind();
			bb.put(z);
		}
		if (buffs.length > 2){
			ByteBuffer m = buffs[2];
			bb.putDouble(mmin);
			bb.putDouble(mmax);
			m.rewind();
			bb.put(m);
		}
		
		// part starts
		if (buffs.length > 3){
			ByteBuffer partStarts = buffs[3];
			bb.position(pointStartPosition);
			partStarts.rewind();
			bb.put(partStarts);
		}
		
		// part starts
		if (buffs.length > 4){
			ByteBuffer partTypes = buffs[4];
			partTypes.rewind();
			bb.put(partTypes);
		}
		
		return bb;
	}

}
