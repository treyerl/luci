package converters.geometry.shp;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;

import org.json.JSONArray;
import org.json.JSONObject;
import org.luci.Luci;
import org.luci.comm.action.IGetScenario;
import org.luci.connect.DirectStreamWriter;
import org.luci.connect.LcOutputStream;
import org.luci.connect.LcString;
import org.luci.connect.StreamInfo;
import org.luci.connect.StreamWriter;
import org.luci.converters.OGISGeometry;
import org.luci.converters.geometry.AbstractGeometryIterator;
import org.luci.converters.geometry.GeometrySelector;
import org.luci.converters.geometry.GeometryWriter;
import org.luci.converters.geometry.IDatabaseGeometryObjectWriter;
import org.luci.factory.observer.FactoryObserver;
import org.luci.utilities.Projection;
import org.luci.utilities.TAB;
import org.luci.utilities.TimeRangeSelection;

import converters.geometry.shp.iterators.MultiPatch;
import converters.geometry.shp.iterators.MultiPointZ;
import converters.geometry.shp.iterators.Point;
import converters.geometry.shp.iterators.PolylineZ;

public class Writer extends GeometryWriter implements ISHP {
	protected ByteBuffer shpBuffer;
	protected ByteArrayOutputStream dbfBuffer;
	protected ByteBuffer shxBuffer;
	protected HashMap<Integer, SortedMap<Long, Record>> records;
	protected HashMap<Integer, List<double[]>> bboxes;
	protected List<Map<String,String>> attrStreamInfos;
	protected int streamIndexStart = 0;
	protected int dbfRecordSize;
	protected int ScID;
	protected TimeRangeSelection trs;
	
	protected List<String> dbfColumnNames;
	protected List<Byte> dbfLengths;
	protected List<Short> dbfTypes;
	protected List<Integer> dbfDecimalCounts;
	
	IDatabaseGeometryObjectWriter db;
	
	/* DBF CODES
	 * from http://dbase.com/Knowledgebase/INT/db7_file_fmt.htm
	 * 
	 * Symbol	Data Type			Description
	 * B		Binary, a string	10 digits representing a .DBT block number. The number is stored as a string, right justified and padded with blanks.
	 * C		Character			All OEM code page characters - padded with blanks to the width of the field.
	 * D		Date				8 bytes - date stored as a string in the format YYYYMMDD.
	 * N		Numeric				Number stored as a string, right justified, and padded with blanks to the width of the field. 
	 * L		Logical				1 byte - initialized to 0x20 (space) otherwise T or F.
	 * M		Memo, a string		10 digits (bytes) representing a .DBT block number. The number is stored as a string, right justified and padded with blanks.
	 * @		Timestamp			8 bytes - two longs, first for date, second for time.  The date is the number of days since  01/01/4713 BC. Time is hours * 3600000L + minutes * 60000L + Seconds * 1000L
	 * I		Long				4 bytes. Leftmost bit used to indicate sign, 0 negative.
	 * +		Autoincrement		Same as a Long
	 * F		Float				Number stored as a string, right justified, and padded with blanks to the width of the field. 
	 * O		Double				8 bytes - no conversions, stored as a double.
	 * G		OLE					10 digits (bytes) representing a .DBT block number. The number is stored as a string, right justified and padded with blanks.
	 * */
	public final short B = 0b000000000001; // DBT block numbers not covered here
	public final short C = 0b000000000010;
	public final short D = 0b000000000100;
	public final short N = 0b000000001000;
	public final short L = 0b000000010000;
	public final short M = 0b000000100000; // DBT block numbers not covered here
	public final short A = 0b000001000000; // stands for @, Timestamp
	public final short I = 0b000010000000;
	public final short P = 0b000100000000; // stands for +, Autoincrement
	public final short F = 0b001000000000;
	public final short O = 0b010000000000;
	public final short G = 0b100000000000; // DBT block numbers not covered here
	public final short stringTypes = B|C|D|F|N|M|G;
	public final byte dbfDeleted = 0x2A;
	public final byte dbfUnDeleted = 0x20;
	
	private byte getTypeLength(short type) {
		switch(type){
		case D: return 8;
		case L:	return 1;
		case A: return 8;
		case I: return 4;
		case O: return 8;
		default: return 0;
		}
	}
	
	private char getTypeChar(short type){
		switch(type){
		case B: return 'B';
		case C: return 'C';
		case D: return 'D';
		case N: return 'N';
		case L:	return 'L';
		case M: return 'M';
		case A: return '@';
		case I: return 'I';
		case P: return '+';
		case F: return 'F';
		case O: return 'O';
		case G: return 'G';
		default: return 0;
		}
	}
	
	protected String shapeTypeName(int shapetype){
		switch(shapetype){
		case SHPPoint: 		return "Point";
		case SHPPolyLine: 		return "PolyLine";
		case SHPPolygon: 		return "Polygon"; 
		case SHPMultiPoint: 	return "MultiPoint";
		case SHPPointZ: 		return "PointZ";
		case SHPPolyLineZ: 	return "PolyLineZ";
		case SHPPolygonZ: 		return "PolygonZ";
		case SHPMultiPointZ: 	return "MultiPointZ";
		case SHPPointM: 		return "PointM"; 
		case SHPPolyLineM: 	return "PolyLineM"; 
		case SHPPolygonM: 		return "PolygonM";
		case SHPMultiPointM: 	return "MultiPointM";
		case SHPMultiPatch: 	return "MultiPatch";
		default: 			return "NULL";
		}
	}
	
	protected class Record{
		AbstractGeometryIterator<?> geom;
		private HashMap<String, Object> properties;
		private HashMap<String, Short> pTypes;
		byte delFlag = dbfUnDeleted;
		
		public Record(AbstractGeometryIterator<?> geom){
			this.geom = geom;
			properties = new HashMap<String, Object>();
			pTypes = new HashMap<String, Short>();
		}
		
		private Record(AbstractGeometryIterator<?> geom, HashMap<String,Object> properties, HashMap<String, Short> pTypes){
			this.geom = geom;
			this.properties = properties;
			this.pTypes = pTypes;
		}
		
		public void addProperty(String name, Object value, short type){
			properties.put(name, value);
			pTypes.put(name,type);
		}
		
		public void setDeletedFlag(byte flag){
			delFlag = flag;
		}
		
		public byte getDeletedFlag(){
			return delFlag;
		}
		
		public AbstractGeometryIterator<?> getGeometry() {
			return geom;
		}
		
		public Map<String, Object> getProperties(){
			return properties;
		}
		
		public boolean hasProperty(String name){
			return properties.containsKey(name);
		}
		
		public Object getProperty(String name){
			return properties.get(name);
		}
		
		public Map<String,Short> getTypes(){
			return pTypes;
		}
		
		public short getType(String name){
			return pTypes.get(name);
		}
		
		public Record withNewGeometry(AbstractGeometryIterator<?> geom){
			return new Record(geom, new HashMap<String, Object>(properties), new HashMap<String, Short>(pTypes));
		}
	}
	
	/**
	 * @param o FactoryObserver
	 * @param ScID Scenario ID int
	 * @param batchID Batch ID int
	 * @param layer Layer name String
	 * @param geomID Geometry ID long
	 * @param trs TimeRangeSelection
	 * @param inclHistory boolean
	 * @param version String (like 1.1.2.3.4)
	 * @param pc Projection
	 * @param outputStreams List< LcOutputStream >
	 * @param errors JSONArray
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	public Writer(FactoryObserver o, GeometrySelector selector, Projection pc, 
			List<LcOutputStream> outputStreams, JSONArray errors)
			throws InstantiationException, IllegalAccessException {
		super(o, selector, pc, outputStreams, errors);
		records = new HashMap<Integer, SortedMap<Long, Record>>();
		bboxes = new HashMap<Integer, List<double[]>>();
//		shpBuffer = new ArrayList<byte[]>();
//		dbfBuffer = new ArrayList<byte[]>();
//		shxBuffer = new ArrayList<byte[]>();
		attrStreamInfos = new ArrayList<Map<String,String>>();
		if (outputStreams != null) streamIndexStart = outputStreams.size();
		db = o.getDatabaseGeometryObjectWriter();
		ScID = selector.getScID();
		trs = selector.getTimeRangeSelection();
	}
	
	protected byte[] writeBBoxLittleEndian(double[] d){
		ByteBuffer bb = ByteBuffer.allocate(32);
		bb.order(ByteOrder.LITTLE_ENDIAN);
		bb.putDouble(0, d[0]);
		bb.putDouble(8, d[1]);
		bb.putDouble(16, d[2]);
		bb.putDouble(24, d[3]);
		return bb.array();
	}
	
	private void loadRecords() throws Exception{
		Connection con  = o.getDatabaseConnection();
		String g = TAB.getScenarioTable(ScID, TAB.SCN);
		String a = null;
		PreparedStatement ps  = null;
	
		try {
			String sql = "SELECT g.id, g.geom, g.layer, g.timestamp, a.timestamp, a.keyname, a.format, a.checksum, a.number, a.text, g.flag "
					   + "FROM "+g+" g LEFT OUTER JOIN "+a+" a ON (g.id = a.geomid AND g.scid = a.scid) WHERE g.scid = ? "
					   //+ "AND g.flag & "+TAB.flagDELETED+" = 0 AND COALESCE(a.flag,0) & "+TAB.flagDELETED+" = 0 "
					   + "AND g.timestamp " + trs.getSQL();
			ps = con.prepareStatement(sql);
			ps.setLong(1, ScID);
			int next = trs.setTimestampSQL(ps, 2);
			trs.setTimestampSQL(ps, next);
			ResultSet rs = ps.executeQuery();
			
			
			Map<File, StreamInfo> streaminfos = new HashMap<File, StreamInfo>();
			while (rs.next()){
				long geomid = rs.getLong(1);
				Object geometry = rs.getObject(2);
				AbstractGeometryIterator<?> gi = db.fromObject(geometry, projection);
				int shapetype = gi.getType();
				if (!records.containsKey(shapetype)) records.put(shapetype,	new TreeMap<Long,Record>());
				SortedMap<Long, Record> recordsPerType = records.get(shapetype);
				if (!recordsPerType.containsKey(geomid)) {
					Record record = new Record(gi);
					record.addProperty("id", geomid, I);
					record.addProperty("layer", rs.getString(3), C);
					// dbfLevel5: date format = D
					// dbfLevel7: also has a timestamp: milliseconds since 1970 (even though not well documented)
					record.addProperty("timestamp", rs.getTimestamp(4), D);
					recordsPerType.put(geomid, record);
				}
				String keyname = rs.getString(6);
				if (keyname != null){
					Record record = recordsPerType.get(geomid);
					String format = rs.getString(7);
					String checksum = rs.getString(8);
					BigDecimal number = rs.getBigDecimal(9);
					String text = rs.getString(10);
					long flag = rs.getLong(11);
					// N = Numeric, in dbfLevel5 this is stored as a string; 
					// O = double precision with a decimal count; only available in dbfLevel7
					if (format.equalsIgnoreCase("number") && number != null) record.addProperty(keyname, number, N);
					else if (format.equalsIgnoreCase("text") && text != null) record.addProperty(keyname, text, C);
					else if (checksum != null) {
						File file = new File(Luci.getDataRoot(), checksum+"."+format);
						if (file.exists())
							streaminfos.put(file, new StreamInfo(checksum, format, keyname, 0, (int) file.length()));
					}
					if ((flag & TAB.flagDELETED) != 0) record.setDeletedFlag(dbfDeleted);;
				}
			}
			
			int i = outputStreams.size() + 1;
			for (Entry<File, StreamInfo> en: streaminfos.entrySet()){
				DirectStreamWriter sw = DirectStreamWriter.create(en.getValue(), en.getKey());
				LcOutputStream out = sw.getLcOutputStream();
				out.setOrder(i++);
				o.getThreadPool().submit(sw);
				outputStreams.add(out);
			}
			con.close();
		} catch (SQLException e){
			if (ps != null) this.log.error(e.toString()+", "+ps.toString());
			con.close();
			throw e;
		}
	}
		
	private JSONObject write() throws SQLException, IOException, NoSuchMethodException, InstantiationException {
		JSONObject streamInfos = new JSONObject();
		IGetScenario getScn = o.actionGetScenario();
		Connection con = o.getDatabaseConnection();
		getScn.setCon(con);
		getScn.setScID(ScID);
		getScn.setTimeRangeSelection(trs);
		String keyname = getScn.getScenarioName();
		con.close();
		for (int shapeType: records.keySet()){
			ByteBuffer dbfBuf = writeDbf(shapeType);
			allocateShx(shapeType);
			writeShp(shapeType);
			
			String keyShp = keyname+"_"+shapeTypeName(shapeType)+".shp";
			String keyDbf = keyname+"_"+shapeTypeName(shapeType)+".dbf";
			String keyShx = keyname+"_"+shapeTypeName(shapeType)+".shx";
			
			
			int i = outputStreams.size();
			String shpChecksum = LcString.calcChecksum(shpBuffer); 
			StreamInfo shpInfo = new StreamInfo(shpChecksum, "shp", keyShp, i++, shpBuffer.capacity(), projection.getEPSG(), null);
			StreamWriter swShp = DirectStreamWriter.create(shpInfo, shpBuffer);
			LcOutputStream shpLos = swShp.getLcOutputStream();
			shpLos.setOrder(i++);
			o.getThreadPool().submit(swShp);
			outputStreams.add(shpLos);
			streamInfos.put(keyShp, shpLos.getJSON());
			
			
			String dbfChecksum = LcString.calcChecksum(dbfBuf);
			StreamInfo dbfInfo = new StreamInfo(dbfChecksum, "dbf", keyDbf, i++, dbfBuf.capacity(), null, null);
			StreamWriter swDbf = DirectStreamWriter.create(dbfInfo, dbfBuf);
			LcOutputStream dbfLos = swDbf.getLcOutputStream();
			dbfLos.setOrder(i++);
			o.getThreadPool().submit(swDbf);
			outputStreams.add(dbfLos);
			streamInfos.put(keyDbf, dbfLos.getJSON());
			
			
			String shxChecksum = LcString.calcChecksum(shxBuffer);
			StreamInfo shxInfo = new StreamInfo(shxChecksum, "shx", keyShx, i++, shxBuffer.capacity(), null, null);
			StreamWriter swShx = DirectStreamWriter.create(shxInfo, shxBuffer);
			LcOutputStream shxLos = swShx.getLcOutputStream();
			shxLos.setOrder(i++);
			o.getThreadPool().submit(swShx);
			outputStreams.add(shxLos);
			streamInfos.put(keyShx, shxLos.getJSON());
		}
		for (Map<String,String> i: attrStreamInfos){
			JSONObject streamInfo = new JSONObject(i).put("order", streamIndexStart++);
			streamInfos.put(i.get("keyname"), 
					new JSONObject().put("format", i.get("format")).put("streaminfo", streamInfo));
		}
		return streamInfos;
	}

	@Override
	public JSONObject call() throws Exception {
		loadRecords();
		return write();
	}
	
	
	private ByteBuffer writeDbf(int shapetype) throws IOException{
		// implementing dbfLevel5 http://ulisse.elettra.trieste.it/services/doc/dbase/DBFstruct.htm
		// should implement dbfLevel7 to support timestamps...{
		dbfBuffer = new ByteArrayOutputStream();
		dbfColumnNames = new ArrayList<String>();
		dbfLengths = new ArrayList<Byte>();
		dbfTypes = new ArrayList<Short>();
		dbfDecimalCounts = new ArrayList<Integer>();
		writeDbfHeader(shapetype);
		writeDbfContent(shapetype);
		return ByteBuffer.wrap(dbfBuffer.toByteArray());
	}
	
	private void writeDbfHeader(int shapetype) throws IOException {
		HashMap<String, Byte> fieldLengths = new HashMap<String, Byte>();
		HashMap<String, Short> fieldTypes = new HashMap<String, Short>();
		HashMap<String, Integer> deciCounts = new HashMap<String, Integer>();
		for (Entry<Long,Record> entry: records.get(shapetype).entrySet()){
			Record record = entry.getValue();
			for (Entry<String, Short> typeEntry: record.getTypes().entrySet()){
				String name = typeEntry.getKey();
				short type = typeEntry.getValue();
				Object property = record.getProperty(name);
				if ((type & stringTypes) != 0){
					int len = (property != null) ? property.toString().length(): 1;
					// in dbf fieldLengths are indicated by a single byte therefore Math.min()
					if (!fieldLengths.containsKey(name)) fieldLengths.put(name, (byte) len);
					else if (len > fieldLengths.get(name)) fieldLengths.put(name, (byte) Math.min(len, 255)); 
				} else fieldLengths.put(name, getTypeLength(type));
				fieldTypes.put(name, type);
				if (property instanceof BigDecimal) {
					int p = ((BigDecimal) property).precision();
					if (!deciCounts.containsKey(name)) deciCounts.put(name, p);
					else if (p < deciCounts.get(name)) deciCounts.put(name, p);
				} else deciCounts.put(name, 0);
			}
		}
		dbfRecordSize = 1; // 1 byte for the delete flag
		for (int fieldLength: fieldLengths.values()) dbfRecordSize += fieldLength;
		ByteBuffer header = ByteBuffer.allocate(32);
		header.order(ByteOrder.LITTLE_ENDIAN);
		header.put((byte) 3); // version = dbfLevel5
		
		String lastMod = new SimpleDateFormat("YYYYMMDD").format(new Date());
		header.put((byte) (Integer.parseInt(lastMod.substring(0, 4)) - 1970));
		header.put(Byte.parseByte(lastMod.substring(4,6)));
		header.put(Byte.parseByte(lastMod.substring(6,8)));
		
		// number of records
		header.putInt(records.get(shapetype).size()); 
		// number of bytes in header
		short numBytes = (short) ((fieldLengths.size() + 1) * 32 + 1);
		header.putShort(numBytes);
		// number of bytes in the record (= record size)
		header.putShort((short) dbfRecordSize);
//		dbfBuffer.add(header.array());
		dbfBuffer.write(header.array());
		ByteBuffer columns = ByteBuffer.allocate(numBytes - 32);
		columns.order(ByteOrder.LITTLE_ENDIAN);
		int i = 0;
		for (String key: fieldLengths.keySet()){
			byte len = fieldLengths.get(key);
			short type = fieldTypes.get(key);
			int   deci = deciCounts.get(key);
			columns.put(key.getBytes(), 0, Math.min(key.length(), 11));
			columns.position(i+11);
			columns.put((byte) getTypeChar(type));
			columns.position(i+16);
			columns.put(len);
			columns.put((byte) deci); 
			columns.position(i+32);
			
			// copy to lists so that we have a persistent order
			dbfColumnNames.add(key);
			dbfLengths.add(len);
			dbfTypes.add(type);
			dbfDecimalCounts.add(deci);
			i += 32;
		}
		columns.put((byte) '\r'); // = 0DH
		dbfBuffer.write(columns.array());
	}

	private void writeDbfContent(int shapetype) throws IOException{
		for (Entry<Long,Record> entry: records.get(shapetype).entrySet()){
			ByteBuffer rec = ByteBuffer.allocate(dbfRecordSize);
			rec.order(ByteOrder.LITTLE_ENDIAN);
			long geomid = entry.getKey();
			Record record = entry.getValue();
			rec.put(record.getDeletedFlag());
			int i = -1;
			for (String column: dbfColumnNames){
				short type = dbfTypes.get(++i);
				byte len = dbfLengths.get(i);
				Object property = record.getProperty(column);
				if (column.equals("id")) property = geomid;
				if ((type & stringTypes) != 0) {
					String val = (property != null) ? property.toString(): "";
					if (val.length() > len) throw new IndexOutOfBoundsException("length:"+len);
					String pad = LcString.fill(Math.max(len - val.length(), 0), " ");
					String paddedValue = pad.concat(val.substring(0, Math.min(val.length(), len)));
					rec.put(paddedValue.getBytes());
				} else {
					if (property == null) type = -1;
					switch(type){
					case D: 
						if (len != 6) throw new IndexOutOfBoundsException("expected: 6, got:"+len);
						rec.put(new SimpleDateFormat("YYMMDD").format(property).getBytes());
						break;
					case L:	
						if (len != 1) throw new IndexOutOfBoundsException("expected: 1, got:"+len);
						rec.put((byte) (((boolean) property) ? 'T':'F') ); 
						break;
					case A: 
						if (len != 8) throw new IndexOutOfBoundsException("expected: 8, got:"+len);
						rec.putLong(((Timestamp) property).getTime());
						break;
					case I: 
						if (len != 4) throw new IndexOutOfBoundsException("expected: 4, got:"+len);
						try{
							rec.putInt((int) property);
						} catch (ClassCastException e){
							rec.putInt(((Number) property).intValue());
						}
						break;
					case O: 
						if (len != 8) throw new IndexOutOfBoundsException("expected: 8, got:"+len);
						rec.putDouble(((BigDecimal) property).doubleValue());
						break;
					default: 
						rec.put(new byte[len]); 
						break;
					}
				}
			}
			dbfBuffer.write(rec.array());
		}
		dbfBuffer.write(new byte[]{26}); // important! EOF
	}
	
	private void writeShp(int shapetype) throws NoSuchMethodException, SQLException, IOException {
		ByteBuffer content = writeShpContent(shapetype);
		ByteBuffer header = writeShpHeader(shapetype, content.capacity());
		shpBuffer = ByteBuffer.allocate(header.capacity() + content.capacity());
		shpBuffer.put(header);
		shpBuffer.put(content);
	}
	
	private ByteBuffer writeShpHeader(int shapetype, long len){
		ByteBuffer shp = ByteBuffer.allocate(72);
		shp.order(ByteOrder.LITTLE_ENDIAN);
		ByteBuffer shpBig = ByteBuffer.allocate(28);
		shpBig.putInt(9994);
		shpBig.position(24);
		shpBig.putInt((int) (len/2)); // 16bit words
		shp.putInt(1000);
		shp.putInt(shapetype);
		double[] bbox = new double[]{Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY, Double.NEGATIVE_INFINITY, Double.NEGATIVE_INFINITY,
									 Double.POSITIVE_INFINITY, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY,  Double.NEGATIVE_INFINITY,};
		for (double[] b: bboxes.get(shapetype)){
			if (b[0] < bbox[0]) bbox[0] = b[0]; // X MIN
			if (b[1] < bbox[1]) bbox[1] = b[1]; // Y MIN
			if (b[2] > bbox[2]) bbox[2] = b[2]; // X MAX
			if (b[3] > bbox[3]) bbox[3] = b[3]; // Y MAX
			if (b[4] < bbox[4]) bbox[4] = b[4]; // Z MIN
			if (b[5] > bbox[5]) bbox[5] = b[5]; // Z MAX
			if (b[6] < bbox[6]) bbox[6] = b[6]; // M MIN
			if (b[7] > bbox[7]) bbox[7] = b[7]; // M MAX
		}
		for (double d: bbox) shp.putDouble(d);
		byte[] header = new byte[100];
		shpBig.position(0);
		shpBig.get(header, 0, 28);
		shp.position(0);
		shp.get(header,28,72);
		return ByteBuffer.wrap(header);
	}
	
	private ByteBuffer writeShpContent(int shapetype) throws NoSuchMethodException, SQLException, IOException{
		ByteArrayOutputStream shpBuffer = new ByteArrayOutputStream();
		int i = 1;
		int offset = 50;
		if (!bboxes.containsKey(shapetype)) bboxes.put(shapetype, new ArrayList<double[]>());
		List<double[]> boxes = bboxes.get(shapetype);
		for (Record r: records.get(shapetype).values()){
			ByteBuffer record = getShape(r.getGeometry());
			ByteBuffer recordHeader = ByteBuffer.allocate(8);
			int recordLength = record.capacity()/2;
			recordHeader.putInt(i++);
			recordHeader.putInt(recordLength); // 16bit word length
			shxBuffer.putInt(offset);
			shxBuffer.putInt(recordLength);
			offset += recordLength;
			shpBuffer.write(recordHeader.array());
			shpBuffer.write(record.array());
			double[] bbox = new double[]{Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY, Double.NEGATIVE_INFINITY, Double.NEGATIVE_INFINITY,
					 					 Double.POSITIVE_INFINITY, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, Double.NEGATIVE_INFINITY,};
			record.position(4);
			record.order(ByteOrder.LITTLE_ENDIAN);
			bbox[0] = record.getDouble();
			bbox[1] = record.getDouble();
			bbox[2] = record.getDouble();
			bbox[3] = record.getDouble();
			bbox[4] = record.getDouble();
			bbox[5] = record.getDouble();
			bbox[6] = record.getDouble();
			bbox[7] = record.getDouble();
			boxes.add(bbox);
		}
		return ByteBuffer.wrap(shpBuffer.toByteArray());
	}
	
	private void allocateShx(int shapetype){
		int numRecords = records.get(shapetype).size();
		ByteBuffer header = ByteBuffer.allocate(100);
		int len = numRecords * 8 + 100;
		header.putInt(len / 2, 24);
		shxBuffer = ByteBuffer.allocate(len);
		shxBuffer.put(header);

	}
	
	@SuppressWarnings("unchecked")
	protected ByteBuffer getShape(AbstractGeometryIterator<?> gi) throws SQLException, NoSuchMethodException {
		int shapetype = mapTypeNumberToShapeTypeNumber(gi.getType());
		switch(shapetype){
		case SHPPointZ: 	return Point.shp((AbstractGeometryIterator<Double>) gi);
		case SHPPolyLineZ:  return PolylineZ.shp(gi); 
		case SHPMultiPointZ:return MultiPointZ.shp(gi);
		case SHPMultiPatch: return MultiPatch.shp(gi);
		default: throw new NoSuchMethodException(OGISGeometry.ALLTYPES[gi.getType()]);
		}
	}
	
	private int mapTypeNumberToShapeTypeNumber(int type){
		switch(type){
		case OGISGeometry.POINT: return SHPPointZ;
		case OGISGeometry.MULTIPOINT: return SHPMultiPointZ;
		case OGISGeometry.LINESTRING: return SHPPolyLineZ;
		case OGISGeometry.MULTILINESTRING: return SHPPolyLineZ;
		case OGISGeometry.POLYGON: return SHPMultiPatch;
		case OGISGeometry.MULTIPOLYGON: return SHPMultiPatch;
		case OGISGeometry.TRIANGLE: return SHPMultiPatch;
		case OGISGeometry.TIN: return SHPMultiPatch;
		case OGISGeometry.POLYHEDRALSURFACE: return SHPMultiPatch;
		case OGISGeometry.GEOMETRYCOLLECTION: return SHPMultiPatch;
		default: return SHPNullShape;
		}
	}
	
}
