/**
 * The MIT License (MIT)
 *
 * Copyright (c) <year> <copyright holders>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package converters.geometry.shp.iterators;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.luci.utilities.TypedIterator;

import converters.geometry.shp.ISHP;

/**
 * 
 * @author Lukas Treyer
 *
 */
public class LinePointsIterator<E> implements Iterator<Iterator<Double>>, ISHP, TypedIterator {
	private ByteBuffer bb;
	private int numPoints;
	private int type;
	private int i = 0;
	private int startPosition;
	private int stop1;
	private int stop2;

	public LinePointsIterator(ByteBuffer bb, int numPoints, int type) {
		this.bb = bb;
		this.numPoints = numPoints;
		this.type = type;
		startPosition = bb.position();
		stop1 = startPosition + 16 + numPoints * 16;
		stop2 = startPosition + 32 + numPoints * 16 + numPoints * 8;
	}


	@Override
	public boolean hasNext() {
		return i < numPoints;
	}


	@Override
	public Iterator<Double> next() {
		List<Double> point = new ArrayList<Double>();
		point.add(bb.getDouble());
		point.add(bb.getDouble());
		int p = bb.position();
		switch(type){
		case ZM:
			bb.position(stop1 + i * 8);
			point.add(bb.getDouble());
			bb.position(stop2 + i * 8);
			point.add(bb.getDouble());
			break;
		case M:
			bb.position(stop1 + i * 8);
			point.add(bb.getDouble());
		}
		i++;
		bb.position(p);
		return point.iterator();
	}

	@Override
	public int getType() {
		return LINESIterator;
	}


	
	@Override
	public void remove(){
		throw new UnsupportedOperationException();
	}

}
