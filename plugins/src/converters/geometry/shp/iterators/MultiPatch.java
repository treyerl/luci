/**
 * The MIT License (MIT)
 *
 * Copyright (c) <year> <copyright holders>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package converters.geometry.shp.iterators;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.luci.converters.geometry.AbstractGeometryIterator;
import org.luci.utilities.IntArray;
import org.luci.utilities.TypedIterator;

import com.vividsolutions.jts.geom.CoordinateSequence;
import com.vividsolutions.jts.geom.impl.CoordinateArraySequence;

import converters.geometry.shp.ISHP;
import converters.geometry.shp.SHP;
import static org.luci.converters.OGISGeometry.*;

/**
 * 
 * @author Lukas Treyer
 * @param <E>
 *
 */
public class MultiPatch<E> implements Iterator<E>, ISHP {
	
	class DetermineOuterRingIterator<GEN> implements Iterator<Iterator<Iterator<Iterator<Double>>>>, TypedIterator{
		
		class PointIterator<GENE> implements Iterator<Double>{
			com.vividsolutions.jts.geom.Point point;
			int i = 0;
			int dim;
			public PointIterator(com.vividsolutions.jts.geom.Point p){
				point = p;
				dim = hasM ? 4 : 3;
			}
			@Override
			public boolean hasNext() {
				return i < dim;
			}

			@Override
			public Double next() {
				if (i++ == 0) return point.getX();
				if (i == 1) return point.getY();
				if (i == 2) return bb.getDouble(Y + pointPosition * 8);
				if (i == 3) return bb.getDouble(Z + pointPosition * 8);
				return 0d;
			}
			@Override
			public void remove(){
				throw new UnsupportedOperationException();
			}
		}
		
		class LinearRingIterator<GENE> implements Iterator<Iterator<Double>>{
			com.vividsolutions.jts.geom.LinearRing ring;
			int n = 0;
			public LinearRingIterator(com.vividsolutions.jts.geom.LinearRing ring){
				this.ring = ring;
			}
			
			@Override
			public boolean hasNext() {
				return n < ring.getNumPoints();
			}

			@SuppressWarnings({ "unchecked", "rawtypes" })
			@Override
			public Iterator<Double> next() {
				pointPosition++;
				return new PointIterator(ring.getPointN(n++));
			}
			
			@Override
			public void remove(){
				throw new UnsupportedOperationException();
			}
		}
		
		class RingListIterator<GENE> implements Iterator<Iterator<Iterator<Double>>>{
			List<com.vividsolutions.jts.geom.LinearRing> poly;
			int i = 0;
			public RingListIterator(List<com.vividsolutions.jts.geom.LinearRing> poly){
				this.poly = poly;
			}
			@Override
			public boolean hasNext() {
				return i < poly.size();
			}

			@SuppressWarnings({ "unchecked", "rawtypes" })
			@Override
			public Iterator<Iterator<Double>> next() {
				return new LinearRingIterator(poly.get(i++));
			}
			@Override
			public void remove(){
				throw new UnsupportedOperationException();
			}
		}
		
		int end;
		List<Iterator<Iterator<Iterator<Double>>>> polys = new ArrayList<Iterator<Iterator<Iterator<Double>>>>();
		
		public DetermineOuterRingIterator(){
			end = partTypes.findNextByIndex(i, true);
			List<com.vividsolutions.jts.geom.LinearRing> rings = new ArrayList<com.vividsolutions.jts.geom.LinearRing>();
			while(i < end){
				int start = partStarts[i];
				int END = partEnds[i++];
				CoordinateSequence coSeq = new CoordinateArraySequence(END - start);
				for (int n = start; n < END; n++) {
					coSeq.setOrdinate(n, 0, bb.getDouble());
					coSeq.setOrdinate(n, 1, bb.getDouble());
					
				}
				rings.add(new com.vividsolutions.jts.geom.LinearRing(coSeq, null));
			}
			int n = rings.size();
			int m = 0;
			while(m < n){
				List<com.vividsolutions.jts.geom.LinearRing> poly = new ArrayList<com.vividsolutions.jts.geom.LinearRing>();
				com.vividsolutions.jts.geom.LinearRing r = rings.remove(0);
				for(com.vividsolutions.jts.geom.LinearRing i: rings){
					if (r.contains(i)) {
						poly.add(i);
					}
				}
				if (poly.size() == 0) rings.add(r);
				else {
					poly.add(0, r);
					polys.add(new RingListIterator<Iterator<Iterator<Double>>>(poly));
				}
				m++;
			}
			for (com.vividsolutions.jts.geom.LinearRing r: rings){
				List<com.vividsolutions.jts.geom.LinearRing> poly = new ArrayList<com.vividsolutions.jts.geom.LinearRing>();
				poly.add(r);
				polys.add(new RingListIterator<Iterator<Iterator<Double>>>(poly));
			}
		}
		
		@Override
		public int getType() {
			return POLYGONSIterator;
		}

		@Override
		public boolean hasNext() {
			return i < end;
		}

		@Override
		public Iterator<Iterator<Iterator<Double>>> next() {
			return polys.get(i++);
		}

		
		@Override
		public void remove(){
			throw new UnsupportedOperationException();
		}
		
	}
	class OuterRingIterator<GEN> implements Iterator<Iterator<Iterator<Double>>>, TypedIterator{
		int end;
		public OuterRingIterator(){
			end = partTypes.findNextByIndex(i, true);
		}
		@Override
		public boolean hasNext() {
			return i < end;
		}

		@Override
		public Iterator<Iterator<Double>> next() {
			return new RingIterator<Iterator<Iterator<Double>>>(i++);
		}

		@Override
		public int getType() {
			return RINGSIterator;
		}
		
		@Override
		public void remove(){
			throw new UnsupportedOperationException();
		}
	}
	
	class RingIterator<GEN> implements Iterator<Iterator<Double>>, TypedIterator{
		int i;
		public RingIterator(int i){
			this.i = i;
		}
		
		@Override
		public int getType() {
			return POINTSIterator;
		}

		@Override
		public boolean hasNext() {
			return pointPosition < partStarts[i];
		}

		@Override
		public Iterator<Double> next() {
			return new PlainPointIterator<Double>(pointPosition++);
		}
		
		@Override
		public void remove(){
			throw new UnsupportedOperationException();
		}
	}
	
	class PlainPointIterator<GEN> implements Iterator<Double>, TypedIterator{
		int i = 0;
		int p;
		int jump = -1;
		public PlainPointIterator(int p){
			this.p = p;
		}
		
		public PlainPointIterator(int p, int jump){
			this.p = p;
			this.jump = jump;
		}
		@Override
		public int getType() {
			return POINTIterator;
		}

		@Override
		public boolean hasNext() {
			return hasM ? i < 4: i < 3;
		}

		@Override
		public Double next() {
			if (i++ == 3) return bb.getDouble(Z + p * 8);
			if (i == 2) return bb.getDouble(Y + p * 8);
			if (jump > 0) {
				if (i == 1) return bb.getDouble(X + jump + 8);
				return bb.getDouble(X + jump);
			}
			return bb.getDouble();
		}
		
		@Override
		public void remove(){
			throw new UnsupportedOperationException();
		}
	}
	
	class StripTriangleIterator<GEN> implements Iterator<Iterator<Double>>{
		int i = 0;
		@Override
		public boolean hasNext() {
			return i < 4;
		}

		@Override
		public Iterator<Double> next() {
			if (i++ == 4) new PlainPointIterator<Double>(pointPosition - 2, pointPosition - 2);
			return new PlainPointIterator<Double>(pointPosition++);
		}
		@Override
		public void remove(){
			throw new UnsupportedOperationException();
		}
	}
	
	class FanTriangleIterator<GEN> implements Iterator<Iterator<Double>>{
		int i = 0;
		@Override
		public boolean hasNext() {
			return i < 4;
		}

		@Override
		public Iterator<Double> next() {
			if (i++ == 0) new PlainPointIterator<Double>(pointPosition - 1, pointPosition - 1);
			if (i == 4) new PlainPointIterator<Double>(pointPosition - 4, pointPosition - 4);
			return new PlainPointIterator<Double>(pointPosition++);
		}
		@Override
		public void remove(){
			throw new UnsupportedOperationException();
		}
	}
	
	class TStripIterator implements Iterator<Iterator<Iterator<Double>>>{
		int i;
		public TStripIterator(int i){
			this.i = i;
		}
		@Override
		public boolean hasNext() {
			return pointPosition < partStarts[i] - 2;
		}

		@Override
		public Iterator<Iterator<Double>> next() {
			return new StripTriangleIterator<Iterator<Iterator<Double>>>();
		}
		@Override
		public void remove(){
			throw new UnsupportedOperationException();
		}
	}
	
	class TFanIterator implements Iterator<Iterator<Iterator<Double>>>{
		public TFanIterator(){
			bb.position(bb.position() + 16);
			pointPosition++;
		}
		
		@Override
		public boolean hasNext() {
			return pointPosition < partStarts[i];
		}

		@Override
		public Iterator<Iterator<Double>> next() {
			return new FanTriangleIterator<Iterator<Iterator<Double>>>();
		}
		@Override
		public void remove(){
			throw new UnsupportedOperationException();
		}
	}
	
	ByteBuffer bb;
	IntArray partTypes;
	int numParts;
	int numPoints;
	int[] partStarts;
	int[] partEnds;
	int W;	// start of partTypes
	int X;	// start of points
	int Y;	// start of z values
	int Z;  // start of m values
	boolean hasM;
	int i = 0;
	int pointPosition = 0;
	public MultiPatch(ByteBuffer bb) {
		this.bb = bb;
		bb.order(java.nio.ByteOrder.LITTLE_ENDIAN);
		bb.position(36);
		numParts = bb.getInt();
		numPoints = bb.getInt();
		W = 44 + 4 * numParts;
		X = W + 4 * numParts;
		Y = X + 16 * numPoints;
		Z = Y + 16 + 8 * numPoints;
		hasM = Z + 16 + 8 * numPoints == bb.limit();
		
		partStarts = new int[numParts];
		partEnds = SHP.getPartEnds(bb, numParts, numPoints, partStarts);
		partTypes = IntArray.wrap(getPartTypes());
	}

	private int[] getPartTypes() {
		int[] partTypes = new int[numParts];
		for (int i = 0; i < numParts; i++) partTypes[i] = bb.getInt();
		return partTypes;
	}
	
	public boolean isAllRings(){
		int numTStrips = partTypes.count(TriangleStrip);
		int numTFans = partTypes.count(TriangleFan);
		return numTStrips + numTFans == 0;
	}
	
	public boolean isSinglePoly(){
		return partTypes.count(OuterRing) == 1 || partTypes.count(FirstRing) == 1 || partTypes.length() == 1;
	}

	@Override
	public boolean hasNext() {
		return i < partTypes.length();
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public E next() {
		switch(partTypes.get(i++)){
		case OuterRing: return (E) new OuterRingIterator();
		case FirstRing: return (E) new DetermineOuterRingIterator();
		case Ring: return (E) new RingIterator(i++);
		case TriangleStrip: return (E) new TStripIterator(i++);
		case TriangleFan: return (E) new TFanIterator();
		}
		return null;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static ByteBuffer shp(AbstractGeometryIterator gi){
		double[] bbox = gi.getBBox();
		
		ByteBuffer bb = ByteBuffer.allocate(76 + (gi.numPoints() + 1) * 32 + gi.numParts() * 8);
		ByteBuffer z = ByteBuffer.allocate(8 * gi.numPoints());
		ByteBuffer m = null;
		if (gi.hasM()){
			m = ByteBuffer.allocate(8 * gi.numPoints());
			m.order(ByteOrder.LITTLE_ENDIAN);
		}
		bb.order(ByteOrder.LITTLE_ENDIAN);
		z.order(ByteOrder.LITTLE_ENDIAN);
				
		int pointStartPosition = 44 + gi.numParts() * 8;
		ByteBuffer partStarts = null;
		ByteBuffer partTypes = null;
		
		// shapetype
		bb.putInt(SHPMultiPatch);
		
		// bbox
		for (int i = 0; i < 4; i++) bb.putDouble(bbox[i]);
		
		// nums
		bb.putInt(gi.numParts());
		bb.putInt(gi.numPoints());
		
		// part starts & part types: skipped, will be added at the end
		bb.position(pointStartPosition);
		
		if (gi.getType() == POLYGON) {
			partStarts = ByteBuffer.allocate(gi.numParts() * 4);
			partTypes = ByteBuffer.allocate(gi.numParts() * 4);
			Polygon.write(gi.iterator(), bb, z, m, partStarts, partTypes);
		}
		if (gi.getType() == MULTIPOLYGON) {
			Iterator<Iterator<Iterator<Iterator<Double>>>> geom = gi.iterator();
			List<Integer> _partStarts = new ArrayList<Integer>();
			List<Integer> _partTypes = new ArrayList<Integer>();
			int i = 0;
			while(geom.hasNext()) {
				Iterator<Iterator<Iterator<Double>>> poly = geom.next();
				_partTypes.add(OuterRing);
				boolean inner = false;
				while(poly.hasNext()){
					_partStarts.add(i);
					if (inner) _partTypes.add(InnerRing);
					inner = true;
					Iterator<Iterator<Double>> ring = poly.next();
					while(ring.hasNext()){
						i++;
						Iterator<Double> point = ring.next();
						bb.putDouble(point.next());
						bb.putDouble(point.next());
						if (z != null){
							if (point.hasNext()) z.putDouble(point.next());
							else z.putDouble(0d);
						}
						if (m != null){
							if (point.hasNext()) m.putDouble(point.next());
							else m.putDouble(0d);
						}
					}
				}
			}
			partStarts = ByteBuffer.allocate(_partStarts.size() * 4);
			partTypes = ByteBuffer.allocate(_partTypes.size() * 4);
		}
		
		/* TODO: shapefile/MultiPatch: GEOMETRY COLLECTION, TIN, POLYHEDRALSURFACE*/
		
		return SHP.finalize(bbox, pointStartPosition, bb, z, m, partStarts, partTypes);
	}

	@Override
	public void remove(){
		throw new UnsupportedOperationException();
	}
}