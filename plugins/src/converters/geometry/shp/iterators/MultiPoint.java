/**
 * The MIT License (MIT)
 *
 * Copyright (c) <year> <copyright holders>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package converters.geometry.shp.iterators;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Iterator;

import org.luci.converters.geometry.AbstractGeometryIterator;

import converters.geometry.shp.ISHP;

/**
 * 
 * @author Lukas Treyer
 * @param <E>
 *
 */
public class MultiPoint<E> implements Iterator<Iterator<Double>>, ISHP {
	private ByteBuffer bb;

	public MultiPoint(ByteBuffer bb) {
		this.bb = bb;
		bb.position(40); // 4 bytes for shapetype, first 32 bytes for bbox, 4 bytes numPoints
	}

	@Override
	public boolean hasNext() {
		return bb.hasRemaining();
	}

	@Override
	public Iterator<Double> next() {
		return new Point<Double>(bb);
	}
	
	@Override
	public void remove(){
		throw new UnsupportedOperationException();
	}
	
	@SuppressWarnings("rawtypes")
	public static ByteBuffer shp(AbstractGeometryIterator gi){
		return multiPoint_ZM(gi, SHPMultiPoint);
	}
	
	@SuppressWarnings("rawtypes")
	static ByteBuffer multiPoint_ZM(AbstractGeometryIterator gi, int type) {
		double[] bbox = gi.getBBox();
		double zmin = bbox[4];
		double zmax = bbox[5];
		double mmin = bbox[6];
		double mmax = bbox[7];
		
		ByteBuffer bb;
		ByteBuffer z = null;
		ByteBuffer m = null;
		
		switch(type){
		case SHPMultiPointZ:
			bb = ByteBuffer.allocate(72 + (gi.numPoints() + 1) * 32);
			z = ByteBuffer.allocate(8 * gi.numPoints());
			m = ByteBuffer.allocate(8 * gi.numPoints());
			bb.order(ByteOrder.LITTLE_ENDIAN);
			z.order(ByteOrder.LITTLE_ENDIAN);
			m.order(ByteOrder.LITTLE_ENDIAN);
			bb.putInt(SHPMultiPointZ);
		case SHPMultiPointM:
			bb = ByteBuffer.allocate(56 + (gi.numPoints() + 1) * 24);
			m = ByteBuffer.allocate(8 * gi.numPoints());
			bb.order(ByteOrder.LITTLE_ENDIAN);
			m.order(ByteOrder.LITTLE_ENDIAN);
			bb.putInt(SHPMultiPointM);
		default:
			bb = ByteBuffer.allocate(40 + (gi.numPoints() + 1) * 16);
			bb.order(ByteOrder.LITTLE_ENDIAN);
			bb.putInt(SHPMultiPointZ);
		}
		
		// shapetype
		bb.putInt(type);
		
		// bbox
		for (int i = 0; i < 4; i++) bb.putDouble(bbox[i]);
		
		// points
		bb.putInt(gi.numPoints());
		@SuppressWarnings("unchecked")
		Iterator<Iterator<Double>> it = gi.iterator();
		while(it.hasNext()){
			Iterator<Double> itd = it.next();
			bb.putDouble(itd.next());
			bb.putDouble(itd.next());
			if (itd.hasNext()) z.putDouble(itd.next());
			if (itd.hasNext()) m.putDouble(itd.next());
		}
		if (z != null){
			bb.putDouble(zmin);
			bb.putDouble(zmax);
			z.rewind();
			bb.put(z);
		}
		if (m != null){
			bb.putDouble(mmin);
			bb.putDouble(mmax);
			m.rewind();
			bb.put(m);
		}
		return bb;
	}	
}