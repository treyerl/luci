/**
 * The MIT License (MIT)
 *
 * Copyright (c) <year> <copyright holders>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package converters.geometry.shp.iterators;

import java.nio.ByteBuffer;
import java.util.Iterator;

import org.luci.converters.geometry.AbstractGeometryIterator;

import converters.geometry.shp.ISHP;

/**
 * 
 * @author Lukas Treyer
 * @param <E>
 *
 */
public class MultiPointZ<E> implements Iterator<Iterator<Double>>, ISHP {
	private ByteBuffer bb;

	public MultiPointZ(ByteBuffer bb) {
		this.bb = bb;
		bb.position(72); // 4 bytes for shapetype, first 64 bytes for bbox, 4 bytes numPoints
	}

	@Override
	public boolean hasNext() {
		return bb.hasRemaining();
	}

	@Override
	public Iterator<Double> next() {
		return new PointZ<Double>(bb);
	}
	
	@Override
	public void remove(){
		throw new UnsupportedOperationException();
	}
	
	@SuppressWarnings("rawtypes")
	public static ByteBuffer shp(AbstractGeometryIterator gi){
		return converters.geometry.shp.iterators.MultiPoint.multiPoint_ZM(gi, SHPMultiPointZ);
	}

}
