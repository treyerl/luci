/**
 * The MIT License (MIT)
 *
 * Copyright (c) <year> <copyright holders>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package converters.geometry.shp.iterators;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Iterator;

import org.luci.converters.geometry.AbstractGeometryIterator;

import converters.geometry.shp.ISHP;

/**
 * 
 * @author Lukas Treyer
 *
 */
public class Point<E> implements Iterator<Double>, ISHP{
	double[] d = new double[2];
	int i = 0;

	/**
	 * 
	 * @param bb
	 * 		ByteBuffer being positioned at the beginning of the point such as 
	 * 		we can read the coordinates as follows:
	 * 			bb.position(bb.position() + 4); // for the shape type
	 * 			double x = gg.getDouble();
	 * 			double y = gg.getDouble();
	 * 		with p being the current position before any read operation
	 * @param proj
	 */
	public Point(ByteBuffer bb) {
//		if (bb.order() != ByteOrder.LITTLE_ENDIAN) throw new IllegalStateException();
		bb.position(bb.position() + 4);
		d[0] = bb.getDouble();
		d[1] = bb.getDouble();
	}

	@Override
	public boolean hasNext() {
		return i < 2;
	}

	@Override
	public Double next() {
		return d[i++];
	}
	
	@Override
	public void remove(){
		throw new UnsupportedOperationException();
	}
	public static ByteBuffer shp(AbstractGeometryIterator<Double> gi){
		Iterator<Double> p = gi.iterator();
		ByteBuffer point = ByteBuffer.allocate(20);
		point.order(ByteOrder.LITTLE_ENDIAN);
		point.putInt(SHPPoint);
		point.putDouble(p.next());
		point.putDouble(p.next());
		return point;
	}
	
	
}