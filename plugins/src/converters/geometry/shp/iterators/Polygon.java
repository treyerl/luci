/**
 * The MIT License (MIT)
 *
 * Copyright (c) <year> <copyright holders>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package converters.geometry.shp.iterators;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.luci.converters.geometry.AbstractGeometryIterator;
import org.luci.utilities.IntArray;
import org.luci.utilities.TypedIterator;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LinearRing;
import com.vividsolutions.jts.geom.impl.CoordinateArraySequence;

import converters.geometry.shp.ISHP;
import converters.geometry.shp.SHP;

/**
 * 
 * @author Lukas Treyer
 * @param <E>
 *
 */
public class Polygon<E> implements Iterator<E>, ISHP {
	GeometryFactory GF = new GeometryFactory(); // TODO: JTS GeometryFactory needs precision and SRID
	
	class PolyDetermineOuterRingIterator<GEN> implements Iterator<Iterator<Iterator<Iterator<Double>>>>, TypedIterator{
		
		class JTSPointIterator<GENE> implements Iterator<Double>{
			com.vividsolutions.jts.geom.Point point;
			int i = 0;
			int dim;
			public JTSPointIterator(com.vividsolutions.jts.geom.Point p){
				point = p;
				dim = hasM ? 4 : 3;
			}
			@Override
			public boolean hasNext() {
				return i < dim;
			}

			@Override
			public Double next() {
				switch (i){
				case 0:
					i++;
					return point.getX();
				case 1:
					i++;
					return point.getY();
				case 2:
					i++;
					return 0d;
//					throw new UnsupportedOperationException(" z and m not yet supported...");
				case 3:
					i++;
					return 0d;
//					throw new UnsupportedOperationException(" z and m not yet supported...");
				}
				throw new IllegalArgumentException("point dimension > 4 !?");
			}
			
			@Override
			public void remove(){
				throw new UnsupportedOperationException();
			}
		}
		
		class JTSLinearRingIterator<GENE> implements Iterator<Iterator<Double>>{
			com.vividsolutions.jts.geom.LinearRing ring;
			int n = 0;
			public JTSLinearRingIterator(com.vividsolutions.jts.geom.LinearRing ring){
				this.ring = ring;
			}
			
			@Override
			public boolean hasNext() {
				return n < ring.getNumPoints();
			}

			@SuppressWarnings({ "unchecked", "rawtypes" })
			@Override
			public Iterator<Double> next() {
				pointPosition++;
				return new JTSPointIterator(ring.getPointN(n++));
			}
			@Override
			public void remove(){
				throw new UnsupportedOperationException();
			}
		}
		
		class RingListIterator<GENE> implements Iterator<Iterator<Iterator<Double>>>{
			List<com.vividsolutions.jts.geom.LinearRing> rings;
			int i = 0;
			public RingListIterator(List<com.vividsolutions.jts.geom.LinearRing> poly){
				this.rings = poly;
			}
			@Override
			public boolean hasNext() {
				return i < rings.size();
			}

			@SuppressWarnings({ "unchecked", "rawtypes" })
			@Override
			public Iterator<Iterator<Double>> next() {
				return new JTSLinearRingIterator(this.rings.get(i++));
			}
			@Override
			public void remove(){
				throw new UnsupportedOperationException();
			}
		}
		
		int i = 0;
		List<List<LinearRing>> polys;
		List<List<List<Integer>>> zList;
		List<List<List<Integer>>> mList;
		int tmpPointPosition = 0;
		
		public PolyDetermineOuterRingIterator(){
			List<LinearRing> inners = new ArrayList<LinearRing>();
			polys = new ArrayList<List<LinearRing>>();
			int j = 0;
			for (int o: orientations){
				if (o == COUNTERCLOCKWISE) {
					List<LinearRing> poly = new ArrayList<LinearRing>();
					poly.add(getJTSRing(new PointsIterator<Iterator<Double>>(j)));
					polys.add(poly);
				} else {
					inners.add(getJTSRing(new PointsIterator<Iterator<Double>>(j)));
				}
				j++;
			}
			Iterator<LinearRing> innerIt = inners.iterator();
			while(innerIt.hasNext()){
				LinearRing r = innerIt.next();
				for (List<LinearRing> p: polys){
					if (r != null){
						if (new com.vividsolutions.jts.geom.Polygon(p.get(0), null, GF)
								.contains(new com.vividsolutions.jts.geom.Polygon(r,null,GF))) {
							p.add(r);
							innerIt.remove();
							break;
						}
					}
				}
			}
			
			// remaining rings / rings not found
			for (LinearRing r: inners){
				List<LinearRing> poly = new ArrayList<LinearRing>();
				poly.add(r);
				polys.add(poly);
			}
		}
		
		private com.vividsolutions.jts.geom.LinearRing getJTSRing(Iterator<Iterator<Double>> ring){
			List<Coordinate> cos = new ArrayList<Coordinate>();
			while(ring.hasNext()){
				Coordinate c = new Coordinate();
				Iterator<Double> co = ring.next();
				c.x = co.next();
				c.y = co.next();
				if (co.hasNext()) {
					c.z = co.next();
				}
				else c.z = 0;
				
				cos.add(c);
			}
			
			Coordinate c1 = cos.get(0);
			Coordinate cn = cos.get(cos.size()-1);
			if (!c1.equals(cn)) cos.add(c1);
			
			return new LinearRing(new CoordinateArraySequence(cos.toArray(new Coordinate[cos.size()])), GF);
		}
		
		public int getNumPolys(){
			return polys.size();
		}
		
		@Override
		public int getType() {
			return POLYGONSIterator;
		}

		@Override
		public boolean hasNext() {
			return i < polys.size();
		}
		
		@Override
		public void remove(){
			throw new UnsupportedOperationException();
		}
		
		@Override
		public Iterator<Iterator<Iterator<Double>>> next() {
			return new RingListIterator<Iterator<Iterator<Double>>>(polys.get(i++));
		}
	}
	public final static int CLOCKWISE = 0;
	public final static int COUNTERCLOCKWISE = 1;
	
	private int[] getRingOrientations(){
		int[] orientations = new int[numParts];

		for (int i = 0; i< numParts; i++){
			double ax = bb.getDouble(X + partStarts[i]);
			double ay = bb.getDouble(X + partStarts[i] + 8);
			double bx = bb.getDouble(X + partStarts[i] + 16);
			double by = bb.getDouble(X + partStarts[i] + 32);
			double angle = Math.atan2(ax*by - ay*bx, ax*by + ay*by);
			if (angle > 0) orientations[i++] = COUNTERCLOCKWISE;
			else orientations[i++] = CLOCKWISE;
		}
		return orientations;
	}
	
	
	
	class SinglePolyRingsIterator<GEN> implements Iterator<Iterator<Iterator<Double>>>, TypedIterator{
		int i = 0;
		PointsIterator<Iterator<Iterator<Double>>> ring;
		public SinglePolyRingsIterator(PointsIterator<Iterator<Iterator<Double>>> ring){
			this.ring = ring;
		}
		@Override
		public int getType() {
			return RINGSIterator;
		}
		
		@Override
		public boolean hasNext() {
			return i < 1;
		}

		@Override
		public Iterator<Iterator<Double>> next() {
			i++;
			return ring;
		}
		@Override
		public void remove(){
			throw new UnsupportedOperationException();
		}
	}
	
	class PointsIterator<GEN> implements Iterator<Iterator<Double>>, TypedIterator{
		int i;
		int j;
		public PointsIterator(int i){
			this.i = i;
			this.j = partStarts[i];
		}
		
		@Override
		public int getType() {
			return POINTSIterator;
		}

		@Override
		public boolean hasNext() {
			return j < partEnds[i];
		}

		@Override
		public Iterator<Double> next() {
			return new PointIterator<Double>(j++);
		}
		@Override
		public void remove(){
			throw new UnsupportedOperationException();
		}
	}
	
	class PointIterator<GEN> implements Iterator<Double>, TypedIterator{
		int i = 0;
		int p;
		public PointIterator(int p){
			if (p >= numPoints) {
//				System.out.println(p+"; "+numPoints);
				throw new IndexOutOfBoundsException();
			}
			this.p = p;
		}
		
		@Override
		public int getType() {
			return POINTIterator;
		}

		@Override
		public boolean hasNext() {
//			System.out.println("polyType: "+polyType+", i: "+i);
			switch(dimensions){
			case ZM: return hasM ? i < 4: i < 3;
			case M: return hasM ? i < 3: i < 2;
			default: return i < 2;
			}
		}

		@Override
		public Double next() {
			switch (i){
			case 3:
				i++;
				return bb.getDouble(Z + (p * 8));
			case 2:
				i++;
				return bb.getDouble(Z + (p * 8));
			case 1:
				i++;
				return bb.getDouble(X + (p * 16) + 8);
			default:
				i++;
				return bb.getDouble(X + (p * 16));
			}
		}
		@Override
		public void remove(){
			throw new UnsupportedOperationException();
		}
	}
	
	ByteBuffer bb;
	IntArray partTypes;
	int numParts;
	int numPoints;
	int[] partStarts;
	int[] partEnds;
	int X;	// start of points
	int Y;	// start of z values
	int Z;  // start of m values
	boolean hasM;
	int i = 0;
	int pointPosition = 0;
	IntArray orientations;
	int numOuters;
	PolyDetermineOuterRingIterator<Iterator<Iterator<Iterator<Double>>>> polys = null;
	Iterator<Iterator<Iterator<Double>>> single = null;
	int dimensions = D2;
	
	public Polygon(ByteBuffer bb){
		this(bb, D2);
	}
	
	public Polygon(ByteBuffer bb, int dim) {
		dimensions = dim;
		this.bb = bb;
		bb.order(ByteOrder.LITTLE_ENDIAN);
		bb.position(36); // 4 bytes shapetype, 32 bytes bbox
		numParts = bb.getInt();
		numPoints = bb.getInt();
		X = 44 + 4 * numParts;
		Y = X + 16 * numPoints;
		Z = Y + 16 + 8 * numPoints;
		switch(dim){
		case ZM: hasM = Z + 16 + 8 * numPoints == bb.limit(); break;
		case M:  hasM = Y + 16 + 8 * numPoints == bb.limit(); break;
		case D2: hasM = false; break;
		}
		
//		if (hasM) System.out.println("HAS M");
		
		
		partStarts = new int[numParts];
		partEnds = SHP.getPartEnds(bb, numParts, numPoints, partStarts);
//		for(int i: partStarts) System.out.print(i+", ");
//		System.out.println();
//		for(int i: partEnds) System.out.print(i+", ");
//		System.out.println();
		orientations = IntArray.wrap(getRingOrientations());
		numOuters = orientations.count(CLOCKWISE);
		
		if (numParts > 1 && numOuters != numParts) {
			polys = new PolyDetermineOuterRingIterator<Iterator<Iterator<Iterator<Double>>>>();
			if (!isMulti()) single = polys.next();
		}
	}
	
	public boolean isMulti(){
		if (polys != null) return polys.getNumPolys() > 1;
		return numParts > 1;
	}
	
	public int getNumParts(){
		if (polys != null) return polys.getNumPolys();
		return numParts;
	}
	
	@Override
	public boolean hasNext() {
		if (isMulti() && polys != null) return polys.hasNext();
		else if (single != null) single.hasNext();
		return i < numParts;
	}
	
	@Override
	public void remove(){
		throw new UnsupportedOperationException();
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public E next() {
		if (isMulti()) {
//			System.out.println("Poly: SinglePolyRingsIterator");
			if (polys != null) return (E) polys.next();
			else return (E) new SinglePolyRingsIterator(new PointsIterator(i++));
		} else if (single != null) single.next();
		return (E) new PointsIterator(i++);
		
	}
	
	public static List<Integer> write(ByteBuffer bb, ByteBuffer z, ByteBuffer m, 
			Iterator<Iterator<Iterator<Double>>> geom){
		List<Integer> partStarts = new ArrayList<Integer>();
		int i = 0;
		while(geom.hasNext()){
			partStarts.add(i);
			Iterator<Iterator<Double>> ring = geom.next();
			while(ring.hasNext()){
				i++;
				Iterator<Double> point = ring.next();
				bb.putDouble(point.next());
				bb.putDouble(point.next());
				if (z != null){
					if (point.hasNext()) z.putDouble(point.next());
					else z.putDouble(0d);
				}
				if (m != null){
					if (point.hasNext()) m.putDouble(point.next());
					else m.putDouble(0d);
				}
			}
		}
		return partStarts;
	}
	
	public static int write(Iterator<Iterator<Iterator<Double>>> geom, ByteBuffer bb, ByteBuffer z, 
			ByteBuffer m, ByteBuffer partStarts, ByteBuffer partTypes) {
		List<Integer> _partStarts = Polygon.write(bb, z, m, geom);
		partTypes.putInt(OuterRing);
		for (Integer i: _partStarts) partStarts.putInt(i);
		for (int i = 1; i < _partStarts.size(); i++) partTypes.putInt(InnerRing);
		return _partStarts.size();
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static ByteBuffer shp(AbstractGeometryIterator gi, int type){
		double[] bbox = gi.getBBox();
		
		ByteBuffer bb = ByteBuffer.allocate(76 + (gi.numPoints() + 1) * 32 + gi.numParts() * 4);
		ByteBuffer m = null;
		bb.order(ByteOrder.LITTLE_ENDIAN);
		if (gi.hasM()){
			m = ByteBuffer.allocate(8 * gi.numPoints());
			m.order(ByteOrder.LITTLE_ENDIAN);
			bb.putInt(SHPPolygonM);
		} else {
			bb.putInt(SHPPolygon);
		}
		
		int pointStartPosition = 44 + gi.numParts() * 4;
		ByteBuffer partStarts = ByteBuffer.allocate(gi.numParts() * 4);
		
		// shapetype
		bb.putInt(type);
		
		// bbox
		for (int i = 0; i < 4; i++) bb.putDouble(bbox[i]);
		
		// nums
		bb.putInt(gi.numParts());
		bb.putInt(gi.numPoints());
		
		// part starts: skipped, will be added at the end
		bb.position(pointStartPosition);
		// points
		for (Integer i : Polygon.write(bb, null /* z */, m, gi.iterator())) partStarts.putInt(i);
		return SHP.finalize(bbox, pointStartPosition, bb, null /* z */, m, partStarts);
	}
}