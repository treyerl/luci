/**
 * The MIT License (MIT)
 *
 * Copyright (c) <year> <copyright holders>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package converters.geometry.shp.iterators;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Iterator;

import org.luci.converters.geometry.AbstractGeometryIterator;

import converters.geometry.shp.ISHP;
import converters.geometry.shp.SHP;

/**
 * 
 * @author Lukas Treyer
 * @param <E>
 *
 */
public class Polyline<E> implements Iterator<Iterator<Iterator<Double>>>, ISHP {
	ByteBuffer bb;
	int numParts;
	int numPoints;
	int[] partStarts;
	int[] partEnds;
	int i = 0;
	public Polyline(ByteBuffer bb) {
		this.bb = bb;
		bb.order(ByteOrder.LITTLE_ENDIAN);
		bb.position(36); // 4 bytes shapetype, 32 bytes bbox
		numParts = bb.getInt();
		numPoints = bb.getInt();
		partStarts = new int[numParts];
		partEnds = SHP.getPartEnds(bb, numParts, numPoints, partStarts);
	}
	
	public int getNumParts(){
		return numParts;
	}

	@Override
	public boolean hasNext() {
		return i < numParts;
	}
	
	@Override
	public void remove(){
		throw new UnsupportedOperationException();
	}
	
	@Override
	public Iterator<Iterator<Double>> next() {
		int numPoints = partEnds[i]-partStarts[i++] + 1;
		return new LinePointsIterator<Iterator<Iterator<Double>>>(bb, numPoints, D2);
	}
	
	@SuppressWarnings("rawtypes")
	public static ByteBuffer shp(AbstractGeometryIterator gi){
		return polyLine_ZM(gi, SHPPolyLine);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	static ByteBuffer polyLine_ZM(AbstractGeometryIterator gi, int type) {
		double[] bbox = gi.getBBox();
		
		ByteBuffer bb;
		ByteBuffer z = null;
		ByteBuffer m = null;
		
		switch(type){
		case SHPPolyLineZ:
			bb = ByteBuffer.allocate(76 + (gi.numPoints() + 1) * 32 + gi.numParts() * 4);
			z = ByteBuffer.allocate(8 * gi.numPoints());
			bb.order(ByteOrder.LITTLE_ENDIAN);
			z.order(ByteOrder.LITTLE_ENDIAN);
			if (gi.hasM()){
				m = ByteBuffer.allocate(8 * gi.numPoints());
				m.order(ByteOrder.LITTLE_ENDIAN);
			}
			bb.putInt(SHPMultiPointZ);
		case SHPPolyLineM:
			bb = ByteBuffer.allocate(60 + (gi.numPoints() + 1) * 24 + gi.numParts() * 4);
			m = ByteBuffer.allocate(8 * gi.numPoints());
			bb.order(ByteOrder.LITTLE_ENDIAN);
			m.order(ByteOrder.LITTLE_ENDIAN);
			bb.putInt(SHPMultiPointM);
		default:
			bb = ByteBuffer.allocate(44 + (gi.numPoints() + 1) * 16 + gi.numParts() * 4);
			bb.order(ByteOrder.LITTLE_ENDIAN);
			bb.putInt(SHPMultiPointZ);
		}
		int pointStartPosition = 44 + gi.numParts() * 4;
		ByteBuffer partStarts = ByteBuffer.allocate(gi.numParts() * 4);
		
		// shapetype
		bb.putInt(type);
		
		// bbox
		for (int i = 0; i < 4; i++) bb.putDouble(bbox[i]);
		
		// nums
		bb.putInt(gi.numParts());
		bb.putInt(gi.numPoints());
		
		// part starts: skipped, will be added at the end
		bb.position(pointStartPosition);
		
		// points
		for (Integer i : Polygon.write(bb, z, m, gi.iterator())) partStarts.putInt(i);
		return SHP.finalize(bbox, pointStartPosition, bb, z, m, partStarts);
	}
}