package converters.unit;

import org.luci.converters.UnitConverter;
import org.luci.factory.observer.FactoryObserver;

public class ByteUnit extends UnitConverter {

	public ByteUnit(FactoryObserver o, String literal) {
		super(o, literal);
	}

	public ByteUnit(FactoryObserver o, double amount, String unitSign) {
		super(o, amount, unitSign);
	}
	
	@Override
	protected double getCommonAmount(double amount, String unitSign){
		switch(unitSign.toLowerCase()){
		case "b":    break;
		case "bit":  amount /= 8; break;
		case "kb":   amount *= Math.pow(2, 10); break;
		case "kbit": amount *= Math.pow(2, 7);  break;
		case "mb": 	 amount *= Math.pow(2, 20); break;
		case "mbit": amount *= Math.pow(2, 17); break;
		case "gb":   amount *= Math.pow(2, 30); break;
		case "gbit": amount *= Math.pow(2, 27); break;
		case "tb": 	 amount *= Math.pow(2, 40); break;
		case "pb":   amount *= Math.pow(2, 50); break;
		case "eb":   amount *= Math.pow(2, 60); break;
		case "zb":   amount *= Math.pow(2, 70); break;
		case "yb":   amount *= Math.pow(2, 80); break;
		default: throw new IllegalArgumentException(unitSign + "not recognized!");
		}
		return amount;
	}

	@Override
	public String getUnitName(String unitSign) {
		switch(unitSign.toLowerCase()){
		case "bit":  return "Bit";
		case "kb":   return "KiloByte";
		case "kbit": return "KiloBit";
		case "mb": 	 return "MegaByte";
		case "mbit": return "MegaBit";
		case "gb":   return "GigaByte";
		case "gbit": return "GigaBit";
		case "tb": 	 return "TeraByte";
		case "pb":   return "PetaByte";
		case "eb":   return "ExaByte";
		case "zb":   return "ZettaByte";
		case "yb":   return "YottaByte";
		default: return "Byte";
		}
	}

	@Override
	public double getInUnit(String unitSign) {
		switch(unitSign.toLowerCase()){
		case "bit":  return amount * 8;
		case "kb": 	 return amount / Math.pow(2, 10);
		case "kbit": return amount / Math.pow(2, 7);
		case "mb": 	 return amount / Math.pow(2, 20);
		case "mbit": return amount / Math.pow(2, 17);
		case "gb":   return amount / Math.pow(2, 30);
		case "gbit": return amount / Math.pow(2, 27);
		case "tb": 	 return amount / Math.pow(2, 40);
		case "pb":   return amount / Math.pow(2, 50);
		case "eb":   return amount / Math.pow(2, 60);
		case "zb":   return amount / Math.pow(2, 70);
		case "yb":   return amount / Math.pow(2, 80);
		default: 	 return amount;
		}
	}
}
