package converters.unit;

import org.luci.converters.UnitConverter;
import org.luci.factory.observer.FactoryObserver;

public class DistanceUnit extends UnitConverter {

	public DistanceUnit(FactoryObserver o, String literal) {
		super(o, literal);
	}

	public DistanceUnit(FactoryObserver o, double amount, String unitSign) {
		super(o, amount, unitSign);
	}

	@Override
	public String getUnitName(String unitSign) {
		switch(unitSign.toLowerCase()){
		// SI units
		case "nm": return "nanometer";
		case "µm": return "mikrometer";
		case "mm": return "millimeter";
		case "cm": return "centimeter";
		case "dm": return "decimeter";
		case "m": return "meter";
		case "hm": return "hectometer";
		case "km": return "kilometer";
		
		// anglo saxon
		case "in": return "inch";
		case "ft": return "foot";
		case "yd": return "yard";
		case "mi": return "mile";
		case "nmi": return "nautic mile";
		
		// astromoic
		case "ly": return "light year";
		case "pc": return "parsec";
		default: return "[unknown distance unit]";
		}
	}

	@Override
	public double getInUnit(String unitSign) {
		switch(unitSign.toLowerCase()){
		// SI units
		case "nm": return amount * Math.pow(10, 9);
		case "µm": return amount * Math.pow(10, 6);
		case "mm": return amount * 1000;
		case "cm": return amount * 100;
		case "dm": return amount * 10;
		case "m":  return amount;
		case "hm": return amount / 100;
		case "km": return amount / 1000;
		
		// anglo saxon
		case "in": return amount * 5000 / 127;
		case "ft": return amount * 1250 / 381.0;
		case "yd": return amount * 1250 / 1143;
		case "mi": return amount * 125  / 201168;
		case "nmi": return amount * 1 / 1852;
		
		// astromoic
		case "ly": return amount / (299792458 * 60 * 60 * 24 * 365);
		case "pc": return amount / (3.2615668 * 299792458 * 60 * 60 * 24 * 365);
		default: return amount;
		}
	}

	@Override
	protected double getCommonAmount(double amount, String unitSign) {
		switch(unitSign.toLowerCase()){
		// SI units
		case "nm": amount *= Math.pow(10, -9); break;
		case "µm": amount *= Math.pow(10, -6); break;
		case "mm": amount *= 0.001; break;
		case "cm": amount *= 0.01; break;
		case "dm": amount *= 0.1; break;
		case "m": break;
		case "hm": amount *= 100; break;
		case "km": amount *= 1000; break;
		
		// anglo saxon
		case "in": amount *= 127.0 / 5000; break;
		case "ft": amount *= 381.0 / 1250; break;
		case "yd": amount *= 1143  / 1250; break;
		case "mi": amount *= 1609.344; break;
		case "nmi": amount *= 1852; break;
		
		// astromoic
		case "ly": amount *= 299792458 * 60 * 60 * 24 * 365; break;
		case "pc": amount *= 3.2615668 * 299792458 * 60 * 60 * 24 * 365; break;
		}
		return amount;
	}

}
