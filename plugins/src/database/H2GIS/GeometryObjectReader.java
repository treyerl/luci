package database.H2GIS;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.luci.converters.geometry.IDatabaseGeometryObjectReader;
import org.luci.utilities.Projection;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryCollection;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.LinearRing;
import com.vividsolutions.jts.geom.MultiLineString;
import com.vividsolutions.jts.geom.MultiPoint;
import com.vividsolutions.jts.geom.MultiPolygon;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.io.WKBWriter;

public class GeometryObjectReader implements IDatabaseGeometryObjectReader{

	WKBWriter bw = new WKBWriter();
	
	private static GeometryFactory GF = new GeometryFactory();
	
	public double NaN_to_0(double d){
		if (Double.isNaN(d)) return 0d;
		else return d;
	}
	
	public Point toPoint(Iterator<Double> c, Projection proj){
		Coordinate coord = new Coordinate(NaN_to_0(c.next()), NaN_to_0(c.next()), (c.hasNext())?c.next():0.0);
		double[] co = proj.to_scenario(coord.x, coord.y, coord.z);
		Point point = GF.createPoint(new Coordinate(co[0], co[1], (co.length > 2) ? co[2] : 0));
		if (proj.shouldStoreSRID()) point.setSRID(proj.getSRID());
//		System.out.println(point);
		return point;
	}
	
	public Object toPointObject(Iterator<Double> c, Projection proj){
		return toPoint(c, proj);
	}
	
	public byte[] toPointBytes(Iterator<Double> c, Projection proj){
		return bw.write(toPoint(c, proj));
	}
	
	public LinearRing toLinearRing(Iterator<Iterator<Double>> points, Projection proj) {
		List<Coordinate> coords = new ArrayList<Coordinate>();
		while(points.hasNext()){
			coords.add(toPoint(points.next(), proj).getCoordinate());
		}
		// quick check for ring
		if (coords.size() < 3){
			throw new IllegalArgumentException("More than 3 points needed for a valid ring!");
		}
		Coordinate p1 = coords.get(0);
		Coordinate pn = coords.get(coords.size()-1);
		if (!p1.equals(pn)){
			coords.add(p1);
		}
		
		Coordinate[] co = new Coordinate[coords.size()];
		LinearRing lr = GF.createLinearRing(coords.toArray(co));
//		System.out.println("LinearRing: "+lr);
		if (proj.shouldStoreSRID()) lr.setSRID(proj.getSRID());
		return lr;
	}
	
	public Object toLinearRingObject(Iterator<Iterator<Double>> points, Projection proj){
		return toLinearRing(points, proj);
	}
	
	public byte[] toLinearRingBytes(Iterator<Iterator<Double>> points, Projection proj){
		return bw.write(toLinearRing(points, proj));
	}
	
	public LineString toLineString(Iterator<Iterator<Double>> points, Projection proj) {
		List<Coordinate> coords = new ArrayList<Coordinate>();
		while(points.hasNext()){
			coords.add(toPoint(points.next(), proj).getCoordinate());
		}
		Coordinate[] pts = new Coordinate[coords.size()];
		LineString ls = GF.createLineString(coords.toArray(pts));
		if (proj.shouldStoreSRID()) ls.setSRID(proj.getSRID());
		return ls;
	}
	
	public Object toLineStringObject(Iterator<Iterator<Double>> points, Projection proj){
		return toLineString(points, proj);
	}
	
	public byte[] toLineStringBytes(Iterator<Iterator<Double>> points, Projection proj){
		return bw.write(toLineString(points, proj));
	}
	
	public Polygon toPolygon(Iterator<Iterator<Iterator<Double>>> rings, Projection proj){
		List<LinearRing> pg_lrs = new ArrayList<LinearRing>();
		while(rings.hasNext()){
			pg_lrs.add(toLinearRing(rings.next(), proj));
		}
		
		LinearRing shell = pg_lrs.get(0);
		LinearRing[] holes = new LinearRing[pg_lrs.size()];
		for(int i = 0; i < pg_lrs.size(); i++) {
			holes[i] = pg_lrs.get(i);
		}
		
		Polygon p = GF.createPolygon(shell, holes);
		if (proj.shouldStoreSRID()) p.setSRID(proj.getSRID());
		return p;
	}
	
	public Object toPolygonObject(Iterator<Iterator<Iterator<Double>>> rings, Projection proj){
		return toPolygon(rings, proj);
	}
	
	public byte[] toPolygonBytes(Iterator<Iterator<Iterator<Double>>> rings, Projection proj){
		return bw.write(toPolygon(rings, proj));
	}
	
	public MultiPoint toMultiPoint(Iterator<Iterator<Double>> points, Projection proj){
		List<Point> pg_points = new ArrayList<Point>();
		while(points.hasNext()){
			pg_points.add(toPoint(points.next(), proj));
		}
		Point[] pts = new Point[pg_points.size()];
		MultiPoint mp = GF.createMultiPoint(pg_points.toArray(pts));
		if (proj.shouldStoreSRID()) mp.setSRID(proj.getSRID());
		return mp;
	}
	
	public Object toMultiPointObject(Iterator<Iterator<Double>> points, Projection proj){
		return toMultiPoint(points, proj);
	}
	
	public byte[] toMultiPointBytes(Iterator<Iterator<Double>> points, Projection proj){
		return bw.write(toMultiPoint(points, proj));
	}
	
	public MultiLineString toMultiLineString(Iterator<Iterator<Iterator<Double>>> lineCollection, Projection proj){
		List<LineString> pg_lines = new ArrayList<LineString>();
		while(lineCollection.hasNext()){
			pg_lines.add(toLineString(lineCollection.next(), proj));
		}
		LineString[] lns = new LineString[pg_lines.size()];
		lns = pg_lines.toArray(lns);
		MultiLineString mls = GF.createMultiLineString(lns);
		if (proj.shouldStoreSRID()) mls.setSRID(proj.getSRID());
		return mls;
	}
	
	public Object toMultiLineStringObject(Iterator<Iterator<Iterator<Double>>> lineCollection, Projection proj){
		return toMultiLineString(lineCollection, proj);
	}
	
	public byte[] toMultiLineStringBytes(Iterator<Iterator<Iterator<Double>>> lineCollection, Projection proj){
		return bw.write(toMultiLineString(lineCollection, proj));
	}
	
	public MultiPolygon toMultiPolygon(Iterator<Iterator<Iterator<Iterator<Double>>>> polyCollection, Projection proj){
		List<Polygon> pg_polys = new ArrayList<Polygon>();
		while(polyCollection.hasNext()){
			pg_polys.add(toPolygon(polyCollection.next(), proj));
		}
		Polygon[] polys = new Polygon[pg_polys.size()];
		MultiPolygon mpl = GF.createMultiPolygon(pg_polys.toArray(polys));
		if (proj.shouldStoreSRID()) mpl.setSRID(proj.getSRID());
		return mpl;
	}
	
	public Object toMultiPolygonObject(Iterator<Iterator<Iterator<Iterator<Double>>>> polyCollection, Projection proj){
		return toMultiPolygon(polyCollection, proj);
	}
	
	public byte[] toMultiPolygonBytes(Iterator<Iterator<Iterator<Iterator<Double>>>> polyCollection, Projection proj){
		return bw.write(toMultiPolygon(polyCollection, proj));
	}
	
	@SuppressWarnings("rawtypes")
	public GeometryCollection toGeometryCollection(Iterator collection, Projection proj){
		throw new UnsupportedOperationException();
	}
	
	@SuppressWarnings("rawtypes")
	public Object toGeometryCollectionObject(Iterator collection, Projection proj){
		throw new UnsupportedOperationException();
	}
	
	@SuppressWarnings("rawtypes")
	public byte[] toGeometryCollectionBytes(Iterator collection, Projection proj){
		throw new UnsupportedOperationException();
	}
}
