package database.H2GIS;

import java.util.Iterator;
//import java.util.PrimitiveIterator; //JAVA 1.8


import org.luci.converters.geometry.AbstractGeometryIterator;
import org.luci.converters.geometry.IDatabaseGeometryObjectWriter;
import org.luci.utilities.Projection;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.geom.MultiPoint;
import com.vividsolutions.jts.geom.MultiLineString;
import com.vividsolutions.jts.geom.MultiPolygon;
import com.vividsolutions.jts.geom.GeometryCollection;
import com.vividsolutions.jts.geom.Triangle;
import com.vividsolutions.jts.io.ParseException;
import com.vividsolutions.jts.io.WKBReader;

public class GeometryObjectWriter implements IDatabaseGeometryObjectWriter{

	WKBReader br = new WKBReader();
	GeometryFactory GF = new GeometryFactory();
	@SuppressWarnings("rawtypes")
	public class GeometryIterator extends AbstractGeometryIterator {
		private Iterator<?> it;
		private Geometry geom;
		public GeometryIterator(Iterator<?> it, Geometry geom){
			this.it = it;
			this.geom = geom;
		}
		/* (non-Javadoc)
		 * @see java.lang.Iterable#iterator()
		 */
		@Override
		public Iterator iterator() {
			return it;
		}
		
		public int getType(){
			return java.util.Arrays.asList(ALLTYPES).indexOf(geom.getGeometryType());
		}
		
		public Geometry getGeometry(){
			return geom;
		}
		
		public int numParts(){
			switch(geom.getGeometryType()){
			case "Point" : return 1;
	        case "Linestring" : return geom.getNumPoints();
	        case "Polygon" : return ((Polygon)geom).getNumInteriorRing()+1;
	        case "MultiPoint" : return ((MultiPoint)geom).getNumPoints();
	        case "MultiLineString" : return ((MultiLineString)geom).getNumGeometries();
	        case "MultiPolygon" : return ((MultiPolygon)geom).getNumGeometries();
	        case "GeometryCollection" : return ((GeometryCollection)geom).getNumGeometries();
	        default: return 0;
			}
		}
			
		public int numPoints(){
			switch(geom.getGeometryType()){
			case "Point" : return 1;
	        case "Linestring" : return geom.getNumPoints();
	        case "Polygon" : return ((Polygon)geom).getNumPoints();
	        case "MultiPoint" : return ((MultiPoint)geom).getNumPoints();
	        case "MultiLineString" : return ((MultiLineString)geom).getNumPoints();
	        case "MultiPolygon" : return ((MultiPolygon)geom).getNumPoints();
	        case "GeometryCollection" : return ((GeometryCollection)geom).getNumPoints();
	        default: return 0;
			}
		}
		
		public double[] getBBox(){
			if (geom.getGeometryType() == "Point") return new double[]{0,0,0,0,0,0};
			double[] bbox = getInfiniteBoundingBox();
			subGeomBBox(geom, bbox);
			return bbox;
		}
		
		private void subGeomBBox(Geometry g, double[] bbox){
			if (g instanceof Point) subBox(g, bbox);
			else {
				Geometry cg = g;
				for (int i = 0; i < cg.getNumGeometries(); i++) subGeomBBox(cg.getGeometryN(i), bbox);
			}
		}
		
		private void subBox(Geometry g, double[] bbox){
			for (Coordinate co: g.getCoordinates()){
				if (co.x < bbox[0]) bbox[0] = co.x; // X MIN
				if (co.y < bbox[1]) bbox[1] = co.y; // Y MIN
				if (co.x > bbox[2]) bbox[2] = co.x; // X MAX
				if (co.y > bbox[3]) bbox[3] = co.y; // Y MAX
				if (co.z < bbox[4]) bbox[4] = co.z; // Z MIN
				if (co.z > bbox[5]) bbox[5] = co.z; // Z MAX
			}
		}
		
		
		public int[] getSubTypes(){
			if (geom.getGeometryType() == "Point") return new int[]{POINT};
			Geometry g = (Geometry) geom;
			int[] types = new int[g.getNumGeometries()];
			for (int i = 0; i < g.getNumGeometries(); i++) types[i] = java.util.Arrays.asList(ALLTYPES).indexOf(g.getGeometryType());
			return types;
		}
		@Override
		public boolean hasM() {
			return false;
		}
	}
	
	public GeometryIterator fromBytes(byte[] b, Projection proj){
		Geometry geom = null;
		try {
			geom = br.read(b);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return new GeometryIterator(switchGeom(geom, proj), geom);
	}
	
	public GeometryIterator fromObject(Object geom, Projection proj){
		Geometry g = (Geometry) geom;
		return new GeometryIterator(switchGeom(g, proj), g);
	}

	public Iterator<?> switchGeom(Geometry geom, Projection proj) throws IllegalArgumentException {
		switch(geom.getGeometryType()){
		case "Point" : return fromPoint((Point) geom, proj);
        case "LineString" : return fromLineString((LineString) geom, proj);
        case "Polygon" : return fromPolygon((Polygon) geom, proj);
        case "MultiPoint" : return fromMultiPoint((MultiPoint) geom, proj);
        case "MultiLineString" : return fromMultiLineString((MultiLineString) geom, proj);
        case "MultiPolygon" : return fromMultiPolygon((MultiPolygon) geom, proj);
        case "GeometryCollection" : return fromCollection((GeometryCollection) geom, proj);
        default: throw new IllegalArgumentException("Unkown type "+geom.getGeometryType());
		}
	}
	
	@SuppressWarnings("rawtypes")
	public Iterator fromCollection(final GeometryCollection coll, final Projection proj){
		class IterG implements Iterator{
			private int i = 0;
			@Override
			public boolean hasNext() {
				return i < coll.getNumGeometries();
			}

			@Override
			public Object next() {
				return switchGeom(coll.getGeometryN(i++), proj);
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException();
			}
		}
		return new IterG();
	}
	
	public Iterator<Double> fromPoint(Point pt, Projection proj){
		final double[] d;
		d = new double[]{pt.getX(), pt.getY(), (pt.getDimension() > 2) ? pt.getCoordinate().z : 0d};	
		class IterDouble implements Iterator<Double>{
			private int i = 0;
			@Override
			public boolean hasNext() {
				return i < d.length;
			}
			@Override
			public Double next() {
				return d[i++];
			}

			@Override
			public void remove(){
				throw new UnsupportedOperationException();
			}
		}
		return new IterDouble(); 
	}
	
	public Iterator<Iterator<Double>> fromLineString(final LineString ls, final Projection proj){
		class PointIterator implements Iterator<Iterator<Double>>{
			private int i = 0;
			
			@Override
			public boolean hasNext() {
				return i < ls.getNumPoints();
			}

			@Override
			public Iterator<Double> next() {
				return fromPoint((Point)ls.getPointN(i++), proj);
			}
			
			@Override
			public void remove(){
				throw new UnsupportedOperationException();
			}
		}
		return new PointIterator();
	}
	
	public Iterator<Iterator<Iterator<Double>>> fromPolygon(final Polygon poly, final Projection proj){
		class RingIterator implements Iterator<Iterator<Iterator<Double>>>{
			private int i = 0;
			
			@Override
			public boolean hasNext() {
				return i < poly.getNumInteriorRing()+1;
			}

			@Override
			public Iterator<Iterator<Double>> next() {
				if(i==0) {
					i++;
					return fromLineString(poly.getExteriorRing(), proj);
				}
				else {
					i++;
					return fromLineString(poly.getInteriorRingN(i-2), proj);
				}
			}
			@Override
			public void remove(){
				throw new UnsupportedOperationException();
			}
		}
		return new RingIterator();
	}
	
	public Iterator<Iterator<Double>> fromMultiPoint(final MultiPoint mp, final Projection proj){
		class MultiPointIterator implements Iterator<Iterator<Double>>{
			private int i = 0;
			
			@Override
			public boolean hasNext() {
				return i < mp.getNumPoints();
			}

			@Override
			public Iterator<Double> next() {
				return fromPoint((Point)mp.getGeometryN(i++), proj);
			}
			@Override
			public void remove(){
				throw new UnsupportedOperationException();
			}
		}
		return new MultiPointIterator();
	}
	
	public Iterator<Iterator<Iterator<Double>>> fromMultiLineString(final MultiLineString ml, final Projection proj){
		class LineIterator implements Iterator<Iterator<Iterator<Double>>>{
			private int i = 0;
			
			@Override
			public boolean hasNext() {
				return i < ml.getNumGeometries();
			}

			@Override
			public Iterator<Iterator<Double>> next() {
				return fromLineString((LineString)ml.getGeometryN(i++), proj);
			}
			@Override
			public void remove(){
				throw new UnsupportedOperationException();
			}
		}
		return new LineIterator();
	}
	
	public Iterator<Iterator<Iterator<Iterator<Double>>>> fromMultiPolygon(final MultiPolygon mpoly, final Projection proj){
		class PolygonIterator implements Iterator<Iterator<Iterator<Iterator<Double>>>>{
			private int i = 0;
			
			@Override
			public boolean hasNext() {
				return i < mpoly.getNumGeometries();
			}

			@Override
			public Iterator<Iterator<Iterator<Double>>> next() {
				return fromPolygon((Polygon)mpoly.getGeometryN(i++), proj);
			}
			@Override
			public void remove(){
				throw new UnsupportedOperationException();
			}
		}
		return new PolygonIterator();
	}
	
	
	public Iterator<Iterator<Double>> fromTriangle(final Triangle triangle, final Projection proj) {
		class TriangleIterator implements Iterator<Iterator<Double>>{
			private int i = 0;
			
			@Override
			public boolean hasNext() {
				return i <= 3;
			}

			@Override
			public Iterator<Double> next() {
				i++;
				if(i==1) return fromPoint(GF.createPoint(triangle.p0), proj);
				else if(i==2) return fromPoint(GF.createPoint(triangle.p1), proj);
				else return fromPoint(GF.createPoint(triangle.p2), proj);
				
			}
			@Override
			public void remove(){
				throw new UnsupportedOperationException();
			}
		}
		return new TriangleIterator();
	}
}
