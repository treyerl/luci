package database.H2GIS;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.h2gis.h2spatialext.CreateSpatialExtension;
import org.json.JSONObject;
import org.luci.Luci;
import org.luci.connect.LcString;
import org.luci.factory.observer.IDatabase;
import org.luci.utilities.TAB;
import org.slf4j.Logger;

public class H2GIS implements IDatabase {

	public String getDataModelVersion(){
		return "1.0";
	}

	public String getJdbcUrl(String dbHost, String dbPort, String dbName) {
		return "jdbc:h2:./data/database;";
	}

	public String getJdbcClassname() {
		return "org.h2.Driver";
	}

	public JSONObject getDBInfo(Connection con) throws SQLException{
		ResultSet rs = con.createStatement().executeQuery("SELECT postgis_version();");
		rs.next();
		JSONObject info = new JSONObject().put("H2GIS", rs.getString(1));
		con.close();
		return info;
	}

	public void setupTables(Connection con, Logger log) throws SQLException{
		
		CreateSpatialExtension.initSpatialExtension(con);
				
		String service_tables = ""
				+ "CREATE TABLE IF NOT EXISTS "+TAB.getTableName(TAB.SERVICES)+"( "
				+ "id bigint auto_increment,"
				+ "timestamp timestamp NOT NULL,"
				+ "scid bigint,"
				+ "name character varying(32),"
				+ "status character(4),"
				+ "uid bigint,"
				+ "service_subscriptions varchar,"
				+ "flag bigint, "
				+ "PRIMARY KEY (id, timestamp, scid)"
				+ ");"
				
				+ "CREATE TABLE IF NOT EXISTS "+TAB.getTableName(TAB.SERVICECALLS)+"( "
				+ "sobjid bigint auto_increment," 
				+ "input_hashcode timestamp NOT NULL,"
				+ "uid bigint, "
				+ "flag bigint, "
				+ "PRIMARY KEY (sobjid, input_hashcode)"
				+ ");"
				
				+ "CREATE TABLE IF NOT EXISTS "+TAB.getTableName(TAB.SERVICEINPUTS)+"("
				+ "sobjid bigint auto_increment,"
				+ "timestamp timestamp NOT NULL,"
				+ "keyname character varying(32) NOT NULL,"
				+ "format character varying(32) NOT NULL,"
				+ "uID bigint,"
				+ "checksum character(32)," 
				+ "text varchar,"
				+ "number numeric,"
				+ "geom geometry, "
				+ "output_subscription varchar, "
				+ "size bigint,"
				+ "flag bigint, "
				+ "PRIMARY KEY (sobjid, keyname)" 
				+ ");"
				
				+ "CREATE TABLE IF NOT EXISTS "+TAB.getTableName(TAB.SRV_IN_HISTORY)+"("
				+ "sobjid bigint auto_increment,"
				+ "timestamp timestamp NOT NULL,"
				+ "keyname character varying(32) NOT NULL,"
				+ "format character varying(32) NOT NULL,"
				+ "uID bigint,"
				+ "checksum character(32)," 
				+ "text text,"
				+ "number numeric,"
				+ "geom geometry, "
				+ "output_subscription text, "
				+ "size bigint,"
				+ "flag bigint, "
				+ "PRIMARY KEY (sobjid, timestamp, keyname)" 
				+ ");"
				
				+ "CREATE TABLE IF NOT EXISTS "+TAB.getTableName(TAB.SERVICEOUTPUTS)+"("
				+ "sobjid bigint auto_increment,"
				+ "input_hashcode timestamp NOT NULL,"
				+ "keyname character varying(32) NOT NULL,"
				+ "iteration int NOT NULL,"
				+ "timestamp timestamp NOT NULL,"
				+ "format character varying(32) NOT NULL,"
				+ "uID bigint,"
				+ "machinename character varying(32),"
				+ "serviceVersion character varying(32),"
				+ "checksum character(32)," 
				+ "text varchar,"
				+ "number numeric,"
				+ "size bigint,"
				+ "flag bigint, "
				+ "PRIMARY KEY(sobjid, keyname, iteration)"
				+ ");"
				
				+ "CREATE TABLE IF NOT EXISTS "+TAB.getTableName(TAB.SRV_OUT_HISTORY)+"("
				+ "sobjid bigint auto_increment,"
				+ "input_hashcode timestamp NOT NULL,"
				+ "keyname character varying(32) NOT NULL,"
				+ "iteration int NOT NULL,"
				+ "timestamp timestamp NOT NULL,"
				+ "format character varying(32) NOT NULL,"
				+ "uID bigint,"
				+ "machinename character varying(32),"
				+ "serviceVersion character varying(32),"
				+ "checksum character(32)," 
				+ "text text,"
				+ "number numeric,"
				+ "size bigint,"
				+ "flag bigint, "
				+ "PRIMARY KEY(sobjid, input_hashcode, keyname, iteration)"
				+ ");";
		
		String sequence_name = getSequenceNameScenarios();
		String scenario_tables = ""
				+ "CREATE SEQUENCE IF NOT EXISTS "+sequence_name+" START WITH 1 INCREMENT BY 1;"
				
				+ "CREATE TABLE IF NOT EXISTS "+TAB.getTableName(TAB.SCENARIOS)+"("
				+ "id bigint default "+sequence_name+".nextval,"
				+ "timestamp timestamp NOT NULL,"
				+ "name character varying(256) NOT NULL,"
				+ "bbox geometry,"
				+ "center geometry,"
				+ "status character(4),"		// DLTD = scenarios in trashbin
				+ "crs character varying(256),"
				+ "flag bigint, "
				+ "PRIMARY KEY (id, timestamp)"
				+ ");"
				
				+ "CREATE TABLE IF NOT EXISTS "+TAB.getTableName(TAB.SCN_STR_TYPES)+"("
				+ "scid bigint,"
				+ "att_name varchar,"
				+ "format varchar(32),"
				+ "PRIMARY KEY (scid, att_name)"
				+ ");";
		
		String userSQL = ""
				+ "CREATE TABLE IF NOT EXISTS "+TAB.getTableName(TAB.USERS)+"( "
				+ "id bigint auto_increment,"
				+ "timestamp timestamp NOT NULL,"
				+ "username character varying(32) NOT NULL,"
				+ "userpasswd character varying(32),"
				+ "email character varying(32),"
				+ "flag bigint, "
				+ "PRIMARY KEY (id, timestamp)" 
				+ ");"
				
				+ "CREATE TABLE IF NOT EXISTS "+TAB.getTableName(TAB.USERDETAILS)+"( "
				+ "uid bigint NOT NULL,"
				+ "timestamp timestamp NOT NULL,"
				+ "keyname character varying(32),"
				+ "format character varying(32),"
				+ "value varchar,"
				+ "location geometry,"
				+ "flag bigint, "
				+ "PRIMARY KEY (uid, timestamp, keyname)"
				+ ");";
		
		String aggregate = "DROP AGGREGATE IF EXISTS array_agg;"
				+ "CREATE AGGREGATE array_agg FOR \"database.H2GIS.array_agg\";";
		
		
		Locale.setDefault(new Locale ("en", "US"));
		
		String[] tables = {service_tables, scenario_tables, userSQL, aggregate};
		int i = -1;
		try {
			for (String table : tables) {
				i++;
				PreparedStatement ps = con.prepareStatement(table);
				ps.executeUpdate();
			}
		} catch (SQLException e) {
			log.error("Database setup failed ("+i+")!");
			log.debug(e.toString());
			log.debug("SQL String: "+tables[i]);
			con.close();
			throw e;
		}
		con.close();
	}

	@Override
	public String resetTables(Connection con, Logger log) throws SQLException {
		/* delete views first otherwise we will get an error message upon deleting the tables */
		List<String> views = new ArrayList<String>();
		String viewsSQL = ""
				+ "select table_name from information_schema.views "
				+ "where table_catalog = '"+Luci.getConfig().getProperty("dbName")+"' AND table_schema = 'public' "
				+ "AND table_name not in ('geography_columns', 'geometry_columns', 'raster_columns', 'raster_overviews')";
		PreparedStatement ps = con.prepareStatement(viewsSQL);
		ResultSet rs = ps.executeQuery();
		while(rs.next()){
			String view = rs.getString(1);
			con.prepareStatement("DROP VIEW "+view).executeUpdate();
			views.add(view);
		}
		views.remove(TAB.getTableName(TAB.VIEW));
		String sql = ""
				+ "DROP TABLE IF EXISTS "+TAB.getTableName(TAB.SCENARIOS)+";"
				+ "DROP TABLE IF EXISTS "+TAB.getTableName(TAB.SERVICECALLS)+";"
				+ "DROP TABLE IF EXISTS "+TAB.getTableName(TAB.SERVICEINPUTS)+";"
				+ "DROP TABLE IF EXISTS "+TAB.getTableName(TAB.SRV_IN_HISTORY)+";"
				+ "DROP TABLE IF EXISTS "+TAB.getTableName(TAB.SERVICES)+";"
				+ "DROP TABLE IF EXISTS "+TAB.getTableName(TAB.SERVICEOUTPUTS)+";"
				+ "DROP TABLE IF EXISTS "+TAB.getTableName(TAB.SRV_OUT_HISTORY)+";"
				+ "DROP TABLE IF EXISTS "+TAB.getTableName(TAB.USERDETAILS)+";";
				
		ps = con.prepareStatement(sql);
		ps.executeUpdate();
		setupTables(con, log);
		return "Recreated 10 Luci tables, 1 view '"+TAB.getTableName(TAB.VIEW)+"'; deleted views: "+views;
	}
	
	@Override
	public void createScenarioTables(Connection con, Logger log, int ScID, int srid) throws SQLException {
		String sequence_name = getSequenceNameGeomID(ScID);
		
		String sql_create_tables = ""
				+ "CREATE SEQUENCE "+sequence_name+" START WITH 1 INCREMENT BY 1;"			
		
				+ "CREATE TABLE "+TAB.getScenarioTable(ScID, TAB.SCN)+" ("
				+ "geomID bigint default "+sequence_name+".nextval, "
				+ "timestamp timestamp NOT NULL, "
				+ "batchID bigint,"
				+ "layer character varying(256),"
				+ "nurb int,"
				+ "transform float,"
				+ "uid bigint,"
				+ "flag int, "
				+ "geom geometry,"
				+ "PRIMARY KEY (geomID)"
				+ ");"
		
				+ "CREATE TABLE "+TAB.getScenarioTable(ScID, TAB.SCN_HISTORY)+" ("
				+ "timestamp_deleted timestamp, "
				+ "geomID bigint, "
				+ "timestamp timestamp NOT NULL, "
				+ "batchID bigint,"
				+ "layer character varying(256),"
				+ "nurb int,"
				+ "transform float,"
				+ "uid bigint,"
				+ "flag int, "
				+ "geom geometry,"
				+ "PRIMARY KEY (geomID, timestamp)"
				+ ");"

				+ "CREATE TABLE IF NOT EXISTS "+TAB.getScenarioTable(ScID, TAB.SCN_VARIANT_MASK)+"("
				+ "geomID bigint auto_increment, "
				+ "PRIMARY KEY(geomID)"
				+ ");"
				// versions are being added dynamically here
				
				+ "CREATE TABLE IF NOT EXISTS "+TAB.getScenarioTable(ScID, TAB.SCN_VARIANTS)+"("
				+ "versionID bigint auto_increment,"
				+ "versionKey text, "
				+ "PRIMARY KEY (versionID)"
				+ ");"; // like 1.2.1.1.2.3
		try {
			PreparedStatement ps = con.prepareStatement(sql_create_tables);
			ps.execute();
		} catch (SQLException e) {
			log.error("create scenario failed!");
			log.debug(e.toString());
			log.debug("SQL String: "+sql_create_tables);
			con.close();
			throw e;
		}
	}
	
	private String getType(short tableType){
		switch(tableType){
		case(TAB.INTEGER): return "bigint";
		case(TAB.NUMERIC): return "numeric";
		case(TAB.STRING): return "varchar";
		case(TAB.BOOLEAN): return "boolean";
		case(TAB.CHECKSUM): return "array";
		case(TAB.TIMESTAMP): return "timestamp";
		case(TAB.GEOMETRY): return "geometry";
		case(TAB.NUMERICARRAY): return "array";
		case(TAB.JSON): return "varchar";
		case(TAB.LIST): return "varchar";
		default: return null;
		}
	}
	
	private short getType(String h2Type){
		switch(h2Type){
		case "BIGINT": return TAB.INTEGER;
		case "INTEGER": return TAB.INTEGER;
		case "DECIMAL": return TAB.NUMERIC;
		case "FLOAT": return TAB.NUMERIC;
		case "DOUBLE": return TAB.NUMERIC;
		case "NUMERIC": return TAB.NUMERIC;
		case "SMALLINT": return TAB.INTEGER;
		case "ARRAY": return TAB.CHECKSUM;
		case "VARCHAR": return TAB.STRING;
		case "CHAR": return TAB.STRING;
		case "CLOB": return TAB.STRING;
		case "BOOLEAN": return TAB.BOOLEAN;
		case "JSON": return TAB.JSON;
		case "LIST": return TAB.LIST;
		case "STRING": return TAB.STRING;
		case "GEOMETRY": return TAB.GEOMETRY;
		case "TIMESTAMP": return TAB.TIMESTAMP;
		default: return 0;
		}
	}
	
	private String getSequenceNameGeomID(long ScID) {
		return TAB.getScenarioTable(ScID, TAB.SCN)+"_GEOMID_SEQ";
	}
	
	private String getSequenceNameScenarios() {
		return TAB.getTableName(TAB.SCENARIOS)+"_ID_SEQ";
	}
	
//	private String getString(int sqltype){
//		switch(sqltype){
//		case 2003: return "ARRAY";
//		case -5: return "BIGINT";
//		case -2: return "BINARY";
//		case -7: return "BIT";
//		case 2004: return "BLOB";
//		case 16: return "BOOLEAN";
//		case 1: return "CHAR";
//		case 2005: return "CLOB";
//		case 70: return "DATALINK";
//		case 91: return "DATE";
//		case 3: return "DECIMAL";
//		case 2001: return "DISTINCT";
//		case 8: return "DOUBLE";
//		case 6: return "FLOAT";
//		case 4: return "INTEGER";
//		case 2000: return "JAVA_OBJECT";
//		case -16: return "LONGNVARCHAR";
//		case -4: return "LONGVARBINARY";
//		case -1: return "LONGVARCHAR";
//		case -15: return "NCHAR";
//		case 2011: return "NCLOB";
//		case 0: return "NULL";
//		case 2: return "NUMERIC";
//		case -9: return "NVARCHAR";
//		case 1111: return "OTHER";
//		case 7: return "REAL";
//		case 2006: return "REF";
//		case -8: return "ROWID";
//		case 5: return "SMALLINT";
//		case 2009: return "SQLXML";
//		case 2002: return "STRUCT";
//		case 92: return "TIME";
//		case 93: return "TIMESTAMP";
//		case -6: return "TINYINT";
//		case -3: return "VARBINARY";
//		case 12: return "VARCHAR";
//		default: return null;
//		}
//	}
	
	@Override
	public LinkedHashMap<String,Short> getColumnNamesAndTypes(Connection con, int ScID) throws SQLException {
		String tableName = TAB.getScenarioTable(ScID, TAB.SCN);
		String strTypes = TAB.getTableName(TAB.SCN_STR_TYPES);
		String sql = "SELECT t.column_name, "
				+ "(CASE WHEN t.data_type = "+Types.VARCHAR+" THEN "
				+ "COALESCE((SELECT format FROM "+strTypes+" WHERE scid = "+ScID+" AND ATT_NAME = UCASE(t.column_name)), t.type_name) "
				+ "ELSE  t.type_name END) "
				+ "AS TYPE FROM INFORMATION_SCHEMA.COLUMNS AS t WHERE TABLE_NAME = '"+tableName.toUpperCase()+"'";
		PreparedStatement ps = con.prepareStatement(sql);
		ps.execute();
		ResultSet rs = ps.getResultSet();
		LinkedHashMap<String, Short> stringMap = new LinkedHashMap<String, Short>();

		while(rs.next()){
			stringMap.put(rs.getString(1), getType(rs.getString(2)));
		}
//		ResultSetMetaData rsmd = rs.getMetaData();
//		int columnsNumber = rsmd.getColumnCount();
//		for(int i = 1; i <= columnsNumber; i++) stringSet.add(rs.getString(i));
		
		return stringMap;
	}
	
//	@Override
//	public Set<String> getColumnNames(Connection con, String tableName) throws SQLException {
//		String sql = "SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME='"+tableName.toUpperCase()+"'";
//		PreparedStatement ps = con.prepareStatement(sql);
//		ps.execute();
//		ResultSet rs = ps.getResultSet();
//		
//		Set<String> stringSet = new HashSet<String>();
//
//		while(rs.next()){
//			stringSet.add(rs.getString(1));
//		}
////		ResultSetMetaData rsmd = rs.getMetaData();
////		int columnsNumber = rsmd.getColumnCount();
////		for(int i = 1; i <= columnsNumber; i++) stringSet.add(rs.getString(i));
//		
//		return stringSet;
//	}


	/* (non-Javadoc)
	 * @see org.luci.factory.observer.IDatabase#dropColumn(java.sql.Connection, long, java.lang.String)
	 */
	@Override
	public int dropColumns(Connection con, int ScID, String[] columnNames)
			throws SQLException {
		String sql = "ALTER TABLE "+TAB.getScenarioTable(ScID, TAB.SCN)+" DROP COLUMN ?";
		PreparedStatement ps = con.prepareStatement(sql);
		int i=0;
		for(String column:columnNames) {
			ps.setString(1, column);
			ps.executeQuery();
			i++;
		}
		return i;
	}

	/* (non-Javadoc)
	 * @see org.luci.factory.observer.IDatabase#dropScenario(java.sql.Connection, int)
	 */
	@Override
	public void dropScenario(Connection con, int ScID) throws SQLException {
		String sequence_name =  getSequenceNameGeomID(ScID);
		String sql = "DROP TABLE "+TAB.getScenarioTable(ScID, TAB.SCN)+"; "
					+ "DROP TABLE "+TAB.getScenarioTable(ScID, TAB.SCN_HISTORY)+"; "
					+ "DROP TABLE "+TAB.getScenarioTable(ScID, TAB.SCN_VARIANT_MASK)+"; "
					+ "DROP TABLE "+TAB.getScenarioTable(ScID, TAB.SCN_VARIANTS)+"; "
					+ "DROP SEQUENCE "+sequence_name+"; ";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.execute();
		
		sql = "DELETE FROM "+TAB.getTableName(TAB.SCENARIOS)+" WHERE id = ?";
		ps = con.prepareStatement(sql);
		ps.setInt(1, ScID);
		ps.execute();
	}
	
	@Override
	public int addColumns(Connection con, int ScID, Map<String, Short> attributes) throws SQLException {
		// untested
		String tableName = TAB.getScenarioTable(ScID, TAB.SCN);
		String tableName_h = TAB.getScenarioTable(ScID, TAB.SCN_HISTORY);
		String tabStr = TAB.getTableName(TAB.SCN_STR_TYPES);
		String sql = "";
		String sql_h = "";
		Map<String, Short> stringTypes = new HashMap<String, Short>();
		for (Entry<String, Short> e: attributes.entrySet()){
			short t = e.getValue();
			sql = "ALTER TABLE "+tableName+" ADD COLUMN IF NOT EXISTS \""+e.getKey()+"\" "+getType(t)+";";
			sql_h = "ALTER TABLE "+tableName_h+" ADD COLUMN IF NOT EXISTS \""+e.getKey()+"\" "+getType(t)+";";
			PreparedStatement ps = con.prepareStatement(sql+sql_h);
			ps.execute();
			ps.close();
			if (TAB.TYPE_NAMES.containsKey(t)) stringTypes.put(e.getKey(), t);
		}		
		for (Entry<String, Short> e: stringTypes.entrySet()){
			sql = "INSERT INTO "+tabStr+" (scid, att_name, format) SELECT "+Long.toString(ScID)+", '"+e.getKey().toUpperCase()+"',"
					+ " '"+TAB.TYPE_NAMES.get(e.getValue()).toUpperCase()+"' WHERE NOT EXISTS ("
					+ "SELECT * FROM "+tabStr+" WHERE scid = "+Long.toString(ScID)+" AND att_name = '"+e.getKey().toUpperCase()+"');";
			PreparedStatement ps = con.prepareStatement(sql);
//			ps.setLong(1, ScID);
//			ps.setString(2, e.getKey());
//			ps.setString(3, TAB.TYPE_NAMES.get(e.getValue()));
//			ps.setLong(1, ScID);
//			ps.setString(2, e.getKey());
			ps.execute();
			ps.close();
		}
		return 0;
	}

	/* (non-Javadoc)
	 * @see org.luci.factory.observer.IDatabase#getNewestTimestampForService(java.sql.Connection, long)
	 */
	@Override
	public long[] getNewestTimestampForService(Connection con, long SObjID) throws SQLException {
		Statement ps = con.createStatement();
		String sql = "SELECT scid from lc_service_instances WHERE id = "+Long.toString(SObjID);
		ps.execute(sql);
		ResultSet rs = ps.getResultSet();
		int scid = rs.getInt(1);
		sql = "SELECT MAX(timestamp) FROM lc_service_inputs WHERE sobjid = "+Long.toString(SObjID);
		ps.execute(sql);
		rs = ps.getResultSet();
		rs.next();
		Timestamp maxI = rs.getTimestamp(1);
		Timestamp maxG = null;
		Timestamp maxT = maxI;
		if(scid > 0) {
			sql = "SELECT MAX(timestamp) FROM "+TAB.getScenarioTable(scid, TAB.SCN);
			ps.execute(sql);
			rs = ps.getResultSet();
			rs.next();
			maxG = rs.getTimestamp(1);
			if(maxI.compareTo(maxG)>0){
				maxT = maxI;
			}
			else maxT = maxG;		
			sql = "INSERT INTO lc_service_calls (sobjid, t_call, t_inputs, t_scn) \n"
					+"SELECT "+Long.toString(SObjID)+", "+maxT+", "+maxI+", "+maxG+" WHERE NOT EXISTS( \n"
					+"SELECT * FROM lc_service_calls WHERE sobjid = "+Long.toString(SObjID)+" AND t_call = "+maxT+" \n";
			ps.execute(sql);
		}
		else {
			sql = "INSERT INTO lc_service_calls (sobjid, t_call) \n"
					+ "SELECT "+SObjID+", "+maxI+" WHERE NOT EXISTS( \n"
					+ "SELECT * FROM lc_service_calls WHERE sobjid = "+SObjID+" AND t_call = "+maxT+" \n";
			ps.execute(sql);
		}
		rs = ps.getResultSet();
		rs.next();
		return new long[]{rs.getTimestamp(1).getTime(), rs.getLong(2)};
	}

	/* (non-Javadoc)
	 * @see org.luci.factory.observer.IDatabase#getInsertSQL(long, java.util.Set, java.lang.String, java.lang.String)
	 */
	@Override
	public String getInsertSQL(long geomID, Set<String> fields, String tableName, String historyTable, Connection con) throws SQLException {
		boolean isAutoID = geomID == 0;
		if (isAutoID) return getAutoInsertSQL(fields, tableName);
		else return getUpsertSQL(geomID, fields, tableName, historyTable, con);
	}

	private String getUpsertSQL(long geomID, Set<String> fields, String tableName, String historyTable, Connection con) throws SQLException {
		StringBuilder sql = new StringBuilder("INSERT INTO "+historyTable+" SELECT NULL, * FROM "
				+ tableName+" WHERE geomid = "+geomID);
		for (String field: fields) sql.append(" AND \""+field+"\" IS NOT NULL");
		sql.append("; ");
		Statement stmnt = con.createStatement();
		stmnt.execute(sql.toString());
		stmnt.close();
		
		sql = new StringBuilder("");
		sql.append("MERGE INTO "+tableName+"(geomID, timestamp, batchID, layer, nurb, transform, uid, flag, geom");		
		for (String field: fields) sql.append(", \""+field+"\"");
		sql.append(") KEY(geomID) VALUES("+Long.toString(geomID)+",?,?,?,?,?,?,?,?");
		for (int i = 1; i <= fields.size(); i++) sql.append(", ?");
		sql.append(")");
		return sql.toString();

//		sql.append("WITH entries AS (SELECT ?::timestamp with time zone AS _timestamp,"
//				+ " ? AS _batchID, ? AS _layer, ? AS _nurb, ?::double precision[] AS _transform,"
//				+ " ? AS _uid, ? AS _flag, ?::geometry AS _geom");
//		sql.append("), WITH upsert AS (UPDATE "+tableName+" SET timestamp = _timestamp, batchID = _batchID,"
//				+ "layer = COALESCE(_layer, layer), nurb = COALESCE(_nurb, nurb), transform = "
//				+ "COALESCE(_transform, transform), uid = _uid, flag = COALESCE(_flag, flag), "
//				+ "geom = COALESCE(_geom, geom)");
//		i = 1;
//		for (String field: fields) sql.append(", \""+field+"\" = a"+(i++));
//		sql.append(" FROM entries WHERE geomid = "+geomID+" RETURNING *) INSERT INTO "+tableName+"("
//				+ "geomID, timestamp, batchID, layer, nurb, transform, uid, flag, geom");
//		for (String field: fields) sql.append(", \""+field+"\"");
//		sql.append(") SELECT "+geomID+", _timestamp, _batchID, _layer, _nurb, _transform, _uid, "
//				+ "_flag, _geom");
//		for (i = 1; i <= fields.size(); i++) sql.append(", a"+(i));
//		sql.append(" FROM entries WHERE NOT EXISTS (SELECT * FROM upsert);");
//		return sql.toString();		
	}

	private String getAutoInsertSQL(Set<String> fields, String tableName) {
		StringBuilder sql = new StringBuilder("INSERT INTO "+tableName
				+" (timestamp, batchID, layer, nurb, transform, uid, flag, geom");
		for (String field: fields) sql.append(", "+field);
		sql.append(") VALUES(?"+LcString.multiply(7+fields.size(), ",?")+")");
		return sql.toString();
	}
	
	/* (non-Javadoc)
	 * @see org.luci.factory.observer.IDatabase#updateAutoIDSequence(java.sql.Connection, java.lang.String)
	 */
	@Override
	public long updateAutoIDSequence(Connection con, String tableName, String idName)
			throws SQLException {
		String sql = "ALTER SEQUENCE "+tableName+"_"+idName+"_seq RESTART WITH "
				+ "(SELECT coalesce(MAX("+idName+")+1,1) FROM "+tableName+")";
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			ps.execute();
			ps.close();
//			ResultSet rs = ps.getResultSet();
//			rs.next();
			return 0;
		} catch (SQLException e){
			con.close();
			throw e;
		}		
	}

	/* (non-Javadoc)
	 * @see org.luci.factory.observer.IDatabase#updateAutoIDSequence(java.sql.Connection, java.lang.String, long)
	 */
	@Override
	public void updateAutoIDSequence(Connection con, String tableName, String idName, long id)
			throws SQLException {
		String sql = "ALTER SEQUENCE "+tableName+"_"+idName+"_seq RESTART WITH "+id;
		con.prepareStatement(sql).execute();		
	}

	@Override
	public int setChecksum(PreparedStatement ps, int i, String format, String checksum, Connection con) throws SQLException {
		ps.setObject(i, new String[]{format, checksum});
		return i + 1;
	}
}