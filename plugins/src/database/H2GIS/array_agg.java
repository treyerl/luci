/**
 * The MIT License (MIT)
 *
 * Copyright (c) <year> <copyright holders>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package database.H2GIS;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.h2.api.AggregateFunction;

/**
 * 
 * @author treyerl
 *
 */
public class array_agg implements AggregateFunction {
	private List<Long> ids;

	public array_agg() {
		ids = new ArrayList<Long>();
	}

	/* (non-Javadoc)
	 * @see org.h2.api.AggregateFunction#add(java.lang.Object)
	 */
	@Override
	public void add(Object id) throws SQLException {
		ids.add((Long) id);
	}

	/* (non-Javadoc)
	 * @see org.h2.api.AggregateFunction#getResult()
	 */
	@Override
	public Object getResult() throws SQLException {
		// return ids.toString();
		return ids.toArray(new Object[ids.size()]);
	}

	/* (non-Javadoc)
	 * @see org.h2.api.AggregateFunction#getType(int[])
	 */
	@Override
	public int getType(int[] sqlTypeOfInput) throws SQLException {
		return 2003; //ARRAY
	}

	/* (non-Javadoc)
	 * @see org.h2.api.AggregateFunction#init(java.sql.Connection)
	 */
	@Override
	public void init(Connection con) throws SQLException {
		// no idea what to do here
	}

}
