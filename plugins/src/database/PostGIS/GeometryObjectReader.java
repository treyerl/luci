package database.PostGIS;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.luci.converters.geometry.IDatabaseGeometryObjectReader;
import org.luci.utilities.Projection;
import org.postgis.GeometryCollection;
import org.postgis.LineString;
import org.postgis.LinearRing;
import org.postgis.MultiLineString;
import org.postgis.MultiPoint;
import org.postgis.MultiPolygon;
import org.postgis.PGgeometry;
import org.postgis.Point;
import org.postgis.Polygon;
import org.postgis.binary.BinaryWriter;

public class GeometryObjectReader implements IDatabaseGeometryObjectReader{

	BinaryWriter bw = new BinaryWriter();
	
	public double NaN_to_0(double d){
		if (Double.isNaN(d)) return 0d;
		else return d;
	}
	
	public Point toPoint(Iterator<Double> c, Projection proj){
		Point p = new Point();
		p.setX(NaN_to_0(c.next()));
		p.setY(NaN_to_0(c.next()));
		if (c.hasNext()) p.setZ(NaN_to_0(c.next()));
		else p.setZ(0);
		double[] co = proj.to_scenario(p.x, p.y, p.z);
		Point point = new Point(co[0], co[1], (co.length > 2) ? co[2] : 0);
		if (c.hasNext()) point.setM(NaN_to_0(c.next()));
		else point.setM(0);
		if (proj.shouldStoreSRID()) point.setSrid(proj.getSRID());
//		System.out.println(point);
		return point;
	}
	
	@Override
	public Object toPointObject(Iterator<Double> c, Projection proj){
		return new PGgeometry(toPoint(c, proj));
	}
	
	@Override
	public byte[] toPointBytes(Iterator<Double> c, Projection proj){
		return bw.writeBinary(toPoint(c, proj));
	}
	
	public LinearRing toLinearRing(Iterator<Iterator<Double>> points, Projection proj) {
		List<Point> pg_points = new ArrayList<Point>();
		while(points.hasNext()){
			pg_points.add(toPoint(points.next(), proj));
		}
		// quick check for ring
		if (pg_points.size() < 3){
			throw new IllegalArgumentException("More than 3 points needed for a valid ring!");
		}
		Point p1 = pg_points.get(0);
		Point pn = pg_points.get(pg_points.size()-1);
		if (!p1.equals(pn)){
			pg_points.add(p1);
		}
		
		Point[] pts = new Point[pg_points.size()];
		LinearRing lr = new LinearRing(pg_points.toArray(pts));
//		System.out.println("LinearRing: "+lr);
		if (proj.shouldStoreSRID()) lr.setSrid(proj.getSRID());
		return lr;
	}
	
	@Override
	public Object toLinearRingObject(Iterator<Iterator<Double>> points, Projection proj){
		return new PGgeometry(toLinearRing(points, proj));
	}
	
	@Override
	public byte[] toLinearRingBytes(Iterator<Iterator<Double>> points, Projection proj){
		return bw.writeBinary(toLinearRing(points, proj));
	}
	
	public LineString toLineString(Iterator<Iterator<Double>> points, Projection proj) {
		List<Point> pg_points = new ArrayList<Point>();
		while(points.hasNext()){
			pg_points.add(toPoint(points.next(), proj));
		}
		Point[] pts = new Point[pg_points.size()];
		LineString ls = new LineString(pg_points.toArray(pts));
		if (proj.shouldStoreSRID()) ls.setSrid(proj.getSRID());
		return ls;
	}
	
	@Override
	public Object toLineStringObject(Iterator<Iterator<Double>> points, Projection proj){
		return new PGgeometry(toLineString(points, proj));
	}
	
	@Override
	public byte[] toLineStringBytes(Iterator<Iterator<Double>> points, Projection proj){
		return bw.writeBinary(toLineString(points, proj));
	}
	
	public Polygon toPolygon(Iterator<Iterator<Iterator<Double>>> rings, Projection proj){
		List<LinearRing> pg_lrs = new ArrayList<LinearRing>();
		while(rings.hasNext()){
			pg_lrs.add(toLinearRing(rings.next(), proj));
		}
		LinearRing[] lrs = new LinearRing[pg_lrs.size()];
		Polygon p = new Polygon(pg_lrs.toArray(lrs));
		if (proj.shouldStoreSRID()) p.setSrid(proj.getSRID());
		return p;
	}
	
	@Override
	public Object toPolygonObject(Iterator<Iterator<Iterator<Double>>> rings, Projection proj){
		return new PGgeometry(toPolygon(rings, proj));
	}
	
	@Override
	public byte[] toPolygonBytes(Iterator<Iterator<Iterator<Double>>> rings, Projection proj){
		return bw.writeBinary(toPolygon(rings, proj));
	}
	
	public MultiPoint toMultiPoint(Iterator<Iterator<Double>> points, Projection proj){
		List<Point> pg_points = new ArrayList<Point>();
		while(points.hasNext()){
			pg_points.add(toPoint(points.next(), proj));
		}
		Point[] pts = new Point[pg_points.size()];
		MultiPoint mp = new MultiPoint(pg_points.toArray(pts));
		if (proj.shouldStoreSRID()) mp.setSrid(proj.getSRID());
		return mp;
	}
	
	@Override
	public Object toMultiPointObject(Iterator<Iterator<Double>> points, Projection proj){
		return new PGgeometry(toMultiPoint(points, proj));
	}
	
	@Override
	public byte[] toMultiPointBytes(Iterator<Iterator<Double>> points, Projection proj){
		return bw.writeBinary(toMultiPoint(points, proj));
	}
	
	public MultiLineString toMultiLineString(Iterator<Iterator<Iterator<Double>>> lineCollection, Projection proj){
		List<LineString> pg_lines = new ArrayList<LineString>();
		while(lineCollection.hasNext()){
			pg_lines.add(toLineString(lineCollection.next(), proj));
		}
		LineString[] lns = new LineString[pg_lines.size()];
		lns = pg_lines.toArray(lns);
		MultiLineString mls = new MultiLineString(lns);
		if (proj.shouldStoreSRID()) mls.setSrid(proj.getSRID());
		return mls;
	}
	
	@Override
	public Object toMultiLineStringObject(Iterator<Iterator<Iterator<Double>>> lineCollection, Projection proj){
		return new PGgeometry(toMultiLineString(lineCollection, proj));
	}
	
	@Override
	public byte[] toMultiLineStringBytes(Iterator<Iterator<Iterator<Double>>> lineCollection, Projection proj){
		return bw.writeBinary(toMultiLineString(lineCollection, proj));
	}
	
	public MultiPolygon toMultiPolygon(Iterator<Iterator<Iterator<Iterator<Double>>>> polyCollection, Projection proj){
		List<Polygon> pg_polys = new ArrayList<Polygon>();
		while(polyCollection.hasNext()){
			pg_polys.add(toPolygon(polyCollection.next(), proj));
		}
		Polygon[] polys = new Polygon[pg_polys.size()];
		MultiPolygon mpl = new MultiPolygon(pg_polys.toArray(polys));
		if (proj.shouldStoreSRID()) mpl.setSrid(proj.getSRID());
		return mpl;
	}
	
	@Override
	public Object toMultiPolygonObject(Iterator<Iterator<Iterator<Iterator<Double>>>> polyCollection, Projection proj){
		return new PGgeometry(toMultiPolygon(polyCollection, proj));
	}
	
	@Override
	public byte[] toMultiPolygonBytes(Iterator<Iterator<Iterator<Iterator<Double>>>> polyCollection, Projection proj){
		return bw.writeBinary(toMultiPolygon(polyCollection, proj));
	}
	
	@SuppressWarnings("rawtypes")
	public GeometryCollection toGeometryCollection(Iterator collection, Projection proj){
		throw new UnsupportedOperationException();
	}
	
	@Override
	@SuppressWarnings("rawtypes")
	public Object toGeometryCollectionObject(Iterator collection, Projection proj){
		throw new UnsupportedOperationException();
	}
	
	@Override
	@SuppressWarnings("rawtypes")
	public byte[] toGeometryCollectionBytes(Iterator collection, Projection proj){
		throw new UnsupportedOperationException();
	}
}
