package database.PostGIS;

import java.util.Iterator;
//import java.util.PrimitiveIterator; //JAVA 1.8




import org.luci.converters.geometry.AbstractGeometryIterator;
import org.luci.converters.geometry.IDatabaseGeometryObjectWriter;
import org.luci.utilities.Projection;
import org.postgis.ComposedGeom;
import org.postgis.Geometry;
import org.postgis.GeometryCollection;
import org.postgis.LineString;
import org.postgis.MultiLineString;
import org.postgis.MultiPoint;
import org.postgis.MultiPolygon;
import org.postgis.PGgeometry;
import org.postgis.Point;
import org.postgis.PointComposedGeom;
import org.postgis.Polygon;
import org.postgis.PolyhedralSurface;
import org.postgis.Tin;
import org.postgis.Triangle;
import org.postgis.binary.BinaryParser;

public class GeometryObjectWriter implements IDatabaseGeometryObjectWriter{

	BinaryParser bp = new BinaryParser();
	
	@SuppressWarnings("rawtypes")
	public class GeometryIterator extends AbstractGeometryIterator {
		private Iterator<?> it;
		private Geometry geom;
		public GeometryIterator(Iterator<?> it, Geometry geom){
			this.it = it;
			this.geom = geom;
		}
		/* (non-Javadoc)
		 * @see java.lang.Iterable#iterator()
		 */
		@Override
		public Iterator iterator() {
			return it;
		}
		
		@Override
		public int getType(){
			return geom.type;
		}
		
		@Override
		public boolean hasM(){
			return geom.haveMeasure;
		}
		
		public Geometry getGeometry(){
			return geom;
		}
		
		@Override
		public int numParts(){
			switch(geom.type){
			case Geometry.POINT : return 1;
	        case Geometry.LINESTRING : return ((LineString) geom).numPoints();
	        case Geometry.POLYGON : return ((Polygon) geom).numRings();
	        case Geometry.MULTIPOINT : return ((MultiPoint) geom).numPoints();
	        case Geometry.MULTILINESTRING : return ((MultiLineString) geom).numLines();
	        case Geometry.MULTIPOLYGON : return ((MultiPolygon) geom).numPolygons();
	        case Geometry.GEOMETRYCOLLECTION : return ((GeometryCollection) geom).numGeoms();
	        default: return 0;
			}
		}
			
		@Override
		public int numPoints(){
			switch(geom.type){
			case Geometry.POINT : return 1;
	        case Geometry.LINESTRING : return ((LineString) geom).numPoints();
	        case Geometry.POLYGON : return ((Polygon) geom).numPoints();
	        case Geometry.MULTIPOINT : return ((MultiPoint) geom).numPoints();
	        case Geometry.MULTILINESTRING : return ((MultiLineString) geom).numPoints();
	        case Geometry.MULTIPOLYGON : return ((MultiPolygon) geom).numPoints();
	        case Geometry.GEOMETRYCOLLECTION : return ((GeometryCollection) geom).numPoints();
	        default: return 0;
			}
		}
		
		@Override
		public double[] getBBox(){
			if (geom.type == Geometry.POINT) return new double[]{0,0,0,0,0,0,0,0};
			double[] bbox = getInfiniteBoundingBox();
			subGeomBBox((ComposedGeom) geom, bbox);
			return bbox;
		}
		
		private void subGeomBBox(ComposedGeom g, double[] bbox){
			if (g instanceof PointComposedGeom) subBox((PointComposedGeom)g, bbox);
			else {
				ComposedGeom cg = g;
				for (int i = 0; i < cg.numGeoms(); i++) subGeomBBox((ComposedGeom) cg.getSubGeometry(i), bbox);
			}
		}
		
		private void subBox(PointComposedGeom g, double[] bbox){
			for (Point p: g.getPoints()){
				if (p.x < bbox[0]) bbox[0] = p.x; // X MIN
				if (p.y < bbox[1]) bbox[1] = p.y; // Y MIN
				if (p.x > bbox[2]) bbox[2] = p.x; // X MAX
				if (p.y > bbox[3]) bbox[3] = p.y; // Y MAX
				if (p.z < bbox[4]) bbox[4] = p.z; // Z MIN
				if (p.z > bbox[5]) bbox[5] = p.z; // Z MAX
				if (p.m < bbox[6]) bbox[6] = p.m; // M MIN
				if (p.m > bbox[7]) bbox[7] = p.m; // M MAX
			}
		}
		
		
		@Override
		public int[] getSubTypes(){
			if (geom.type == Geometry.POINT) return new int[]{Geometry.POINT};
			ComposedGeom g = (ComposedGeom) geom;
			int[] types = new int[g.numGeoms()];
			for (int i = 0; i < g.numGeoms(); i++) types[i] = g.getSubGeometry(i).getType();
			return types;
		}
	}
	
	@Override
	public GeometryIterator fromBytes(byte[] b, Projection proj){
		Geometry geom = bp.parse(b);
		return new GeometryIterator(switchGeom(geom, proj), geom);
	}
	
	@Override
	public GeometryIterator fromObject(Object geom, Projection proj){
		Geometry g = ((PGgeometry) geom).getGeometry();
		return new GeometryIterator(switchGeom(g, proj), g);
	}

	public Iterator<?> switchGeom(Geometry geom, Projection proj) throws IllegalArgumentException {
		switch(geom.type){
		case Geometry.POINT : return fromPoint((Point) geom, proj);
        case Geometry.LINESTRING : return fromComposedPoints((LineString) geom, proj);
        case Geometry.POLYGON : return fromPolygon((Polygon) geom, proj);
        case Geometry.MULTIPOINT : return fromMultiPoint((MultiPoint) geom, proj);
        case Geometry.MULTILINESTRING : return fromMultiLineString((MultiLineString) geom, proj);
        case Geometry.MULTIPOLYGON : return fromMultiPolygon((MultiPolygon) geom, proj);
        case Geometry.GEOMETRYCOLLECTION : return fromCollection((GeometryCollection) geom, proj);
        default: throw new IllegalArgumentException("Unkown type "+geom.type);
		}
	}
	
	@SuppressWarnings("rawtypes")
	public Iterator fromCollection(final GeometryCollection coll, final Projection proj){
		class IterG implements Iterator{
			private int i = 0;
			@Override
			public boolean hasNext() {
				return i < coll.numGeoms();
			}

			@Override
			public Object next() {
				return switchGeom(coll.getSubGeometry(i++), proj);
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException();
			}
		}
		return new IterG();
	}
	
	public Iterator<Double> fromPoint(Point pt, Projection proj){
		final double[] d = proj.from_scenario(
				new double[]{pt.getX(), pt.getY(), 
				(pt.dimension > 2) ? pt.getZ() : 0d,
				(pt.haveMeasure) ? pt.getM() : 0d});
		
		class IterDouble implements Iterator<Double>{
			private int i = 0;
			@Override
			public boolean hasNext() {
				return i < d.length;
			}
			@Override
			public Double next() {
				return d[i++];
			}

			@Override
			public void remove(){
				throw new UnsupportedOperationException();
			}
		}
		return new IterDouble(); 
	}
	
	public Iterator<Iterator<Double>> fromComposedPoints(final PointComposedGeom pg, final Projection proj){
		class PointIterator implements Iterator<Iterator<Double>>{
			private int i = 0;
			
			@Override
			public boolean hasNext() {
				return i < pg.numPoints();
			}

			@Override
			public Iterator<Double> next() {
				return fromPoint(pg.getPoint(i++), proj);
			}
			
			@Override
			public void remove(){
				throw new UnsupportedOperationException();
			}
		}
		return new PointIterator();
	}
	
	public Iterator<Iterator<Iterator<Double>>> fromPolygon(final Polygon poly, final Projection proj){
		class RingIterator implements Iterator<Iterator<Iterator<Double>>>{
			private int i = 0;
			
			@Override
			public boolean hasNext() {
				return i < poly.numRings();
			}

			@Override
			public Iterator<Iterator<Double>> next() {
				return fromComposedPoints(poly.getRing(i++), proj);
			}
			@Override
			public void remove(){
				throw new UnsupportedOperationException();
			}
		}
		return new RingIterator();
	}
	
	public Iterator<Iterator<Double>> fromMultiPoint(final MultiPoint mp, final Projection proj){
		class MultiPointIterator implements Iterator<Iterator<Double>>{
			private int i = 0;
			
			@Override
			public boolean hasNext() {
				return i < mp.numPoints();
			}

			@Override
			public Iterator<Double> next() {
				return fromPoint(mp.getPoint(i++), proj);
			}
			@Override
			public void remove(){
				throw new UnsupportedOperationException();
			}
		}
		return new MultiPointIterator();
	}
	
	public Iterator<Iterator<Iterator<Double>>> fromMultiLineString(final MultiLineString ml, final Projection proj){
		class LineIterator implements Iterator<Iterator<Iterator<Double>>>{
			private int i = 0;
			
			@Override
			public boolean hasNext() {
				return i < ml.numLines();
			}

			@Override
			public Iterator<Iterator<Double>> next() {
				return fromComposedPoints(ml.getLine(i++), proj);
			}
			@Override
			public void remove(){
				throw new UnsupportedOperationException();
			}
		}
		return new LineIterator();
	}
	
	public Iterator<Iterator<Iterator<Iterator<Double>>>> fromMultiPolygon(final MultiPolygon mpoly, final Projection proj){
		class PolygonIterator implements Iterator<Iterator<Iterator<Iterator<Double>>>>{
			private int i = 0;
			
			@Override
			public boolean hasNext() {
				return i < mpoly.numPolygons();
			}

			@Override
			public Iterator<Iterator<Iterator<Double>>> next() {
				return fromPolygon(mpoly.getPolygon(i++), proj);
			}
			@Override
			public void remove(){
				throw new UnsupportedOperationException();
			}
		}
		return new PolygonIterator();
	}
	
	
	public Iterator<Iterator<Double>> fromTriangle(final Triangle triangle, final Projection proj) {
		class TriangleIterator implements Iterator<Iterator<Double>>{
			private int i = 0;
			
			@Override
			public boolean hasNext() {
				return i < triangle.numPoints();
			}

			@Override
			public Iterator<Double> next() {
				return fromPoint(triangle.getPoint(i++), proj);
			}
			@Override
			public void remove(){
				throw new UnsupportedOperationException();
			}
		}
		return new TriangleIterator();
	}
	
	public Iterator<Iterator<Iterator<Double>>> fromTin(final Tin tin, final Projection proj) {
		class TinIterator implements Iterator<Iterator<Iterator<Double>>>{
			private int i = 0;
			
			@Override
			public boolean hasNext() {
				return i < tin.numTriangles();
			}

			@Override
			public Iterator<Iterator<Double>> next() {
				return fromComposedPoints(tin.getTriangle(i++), proj);
			}
			@Override
			public void remove(){
				throw new UnsupportedOperationException();
			}
		}
		return new TinIterator();
	}
	
	public Iterator<Iterator<Iterator<Double>>> fromPolyhedralSurface(final PolyhedralSurface phs, final Projection proj){
		class RingIterator implements Iterator<Iterator<Iterator<Double>>>{
			private int i = 0;
			
			@Override
			public boolean hasNext() {
				return i < phs.numRings();
			}

			@Override
			public Iterator<Iterator<Double>> next() {
				return fromComposedPoints(phs.getRing(i++), proj);
			}
			@Override
			public void remove(){
				throw new UnsupportedOperationException();
			}
		}
		return new RingIterator();
	}
	
	
}
