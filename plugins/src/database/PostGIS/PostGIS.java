package database.PostGIS;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.json.JSONObject;
import org.luci.Luci;
import org.luci.connect.LcString;
import org.luci.factory.observer.IDatabase;
import org.luci.utilities.TAB;
import org.slf4j.Logger;

public class PostGIS implements IDatabase {
	private static Map<Short, String> types = new HashMap<Short, String>();
	static{
		types.put(TAB.NUMERIC, "numeric");
		types.put(TAB.INTEGER, "numeric");
		types.put(TAB.STRING, "text");
		types.put(TAB.BOOLEAN, "boolean");
		types.put(TAB.CHECKSUM, "varchar[]");
		types.put(TAB.JSON, "text");
		types.put(TAB.LIST, "text");
	}
	
	@Override
	public String getDataModelVersion(){
		return "1.0";
	}

	@Override
	public String getJdbcUrl(String dbHost, String dbPort, String dbName) {
		return "jdbc:postgresql://" + dbHost + ":" + dbPort + "/" + dbName;
	}

	@Override
	public String getJdbcClassname() {
		return "org.postgresql.Driver";
	}

	@Override
	public JSONObject getDBInfo(Connection con) throws SQLException{
		ResultSet rs = con.createStatement().executeQuery("SELECT postgis_version();");
		rs.next();
		JSONObject info = new JSONObject().put("postgis", rs.getString(1));
		con.close();
		return info;
	}

	@Override
	public void setupTables(Connection con, Logger log) throws SQLException{
		String extensions = ""
				+ "CREATE EXTENSION IF NOT EXISTS postgis;";
		
		String service_tables = ""
				+ "CREATE TABLE IF NOT EXISTS "+TAB.getTableName(TAB.SERVICES)+"( "
				+ "id bigserial,"
				+ "timestamp timestamp with time zone NOT NULL,"
				+ "scid bigint,"
				+ "name character varying(32),"
				+ "status character(4),"
				+ "uid bigint,"
				+ "service_subscriptions text,"
				+ "flag bigint, "
				+ "CONSTRAINT s_pkey PRIMARY KEY (id, timestamp, scid)"
				+ ");"
				
				+ "CREATE TABLE IF NOT EXISTS "+TAB.getTableName(TAB.SERVICECALLS)+"( "
				+ "sobjid bigserial," 
				+ "t_call timestamp with time zone NOT NULL,"
				+ "t_inputs timestamp with time zone,"
				+ "t_scn timestamp with time zone,"
				+ "uid bigint, "
				+ "flag bigint, "
				+ "CONSTRAINT sc_key PRIMARY KEY (sobjid, t_call)"
				+ ");"
				
				+ "CREATE TABLE IF NOT EXISTS "+TAB.getTableName(TAB.SERVICEINPUTS)+"("
				+ "sobjid bigserial,"
				+ "timestamp timestamp with time zone NOT NULL,"
				+ "keyname character varying(32) NOT NULL,"
				+ "format character varying(32) NOT NULL,"
				+ "uID bigint,"
				+ "checksum character(32)," 
				+ "text text,"
				+ "number numeric,"
				+ "geom geometry(GeometryZM), "
				+ "output_subscription text, "
				+ "size bigint,"
				+ "flag bigint, "
				+ "CONSTRAINT si_pkey PRIMARY KEY (sobjid, keyname)" 
				+ ");"
								
				+ "CREATE TABLE IF NOT EXISTS "+TAB.getTableName(TAB.SRV_IN_HISTORY)+"("
				+ "timestamp_deleted timestamp with time zone,"
				+ "sobjid bigserial,"
				+ "timestamp timestamp with time zone NOT NULL,"
				+ "keyname character varying(32) NOT NULL,"
				+ "format character varying(32) NOT NULL,"
				+ "uID bigint,"
				+ "checksum character(32)," 
				+ "text text,"
				+ "number numeric,"
				+ "geom geometry(GeometryZM), "
				+ "output_subscription text, "
				+ "size bigint,"
				+ "flag bigint, "
				+ "CONSTRAINT si_h_pkey PRIMARY KEY (sobjid, timestamp, keyname)" 
				+ ");"
				
				+ "CREATE TABLE IF NOT EXISTS "+TAB.getTableName(TAB.SERVICEOUTPUTS)+"("
				+ "sobjid bigserial,"
				+ "input_hashcode timestamp with time zone NOT NULL,"
				+ "keyname character varying(32) NOT NULL,"
				+ "iteration int NOT NULL,"
				+ "timestamp timestamp with time zone NOT NULL,"
				+ "format character varying(32) NOT NULL,"
				+ "uID bigint,"
				+ "machinename character varying(32),"
				+ "serviceVersion character varying(32),"
				+ "checksum character(32)," 
				+ "text text,"
				+ "number numeric,"
				+ "size bigint,"
				+ "flag bigint, "
				+ "CONSTRAINT so_pkey PRIMARY KEY(sobjid, keyname, iteration)"
				+ ");"
				
				+ "CREATE TABLE IF NOT EXISTS "+TAB.getTableName(TAB.SRV_OUT_HISTORY)+"("
				+ "timestamp_deleted timestamp with time zone,"
				+ "sobjid bigserial,"
				+ "input_hashcode timestamp with time zone NOT NULL,"
				+ "keyname character varying(32) NOT NULL,"
				+ "iteration int NOT NULL,"
				+ "timestamp timestamp with time zone NOT NULL,"
				+ "format character varying(32) NOT NULL,"
				+ "uID bigint,"
				+ "machinename character varying(32),"
				+ "serviceVersion character varying(32),"
				+ "checksum character(32)," 
				+ "text text,"
				+ "number numeric,"
				+ "size bigint,"
				+ "flag bigint, "
				+ "CONSTRAINT so_h_pkey PRIMARY KEY(sobjid, input_hashcode, keyname, iteration)"
				+ ");";
		
		String scenario_tables = ""
				+ "CREATE TABLE IF NOT EXISTS "+TAB.getTableName(TAB.SCENARIOS)+"("
				+ "id bigserial,"
				+ "timestamp timestamp with time zone NOT NULL,"
				+ "name character varying(256) NOT NULL,"
				+ "bbox geometry(multipointZM, 4326),"
				+ "center geometry(pointZM, 4326),"
				+ "status character(4),"		// DLTD = scenarios in trashbin
				+ "crs character varying(256),"
				+ "flag bigint, "
				+ "CONSTRAINT sc_pkey PRIMARY KEY (id, timestamp)"
				+ ");"
				
				+ "CREATE TABLE IF NOT EXISTS "+TAB.getTableName(TAB.SCN_STR_TYPES)+"("
				+ "scid bigint,"
				+ "att_name varchar(32),"
				+ "format varchar(32),"
				+ "CONSTRAINT sc_str_pkey PRIMARY KEY (scid, att_name)"
				+ ")";
		
		String userSQL = ""
				+ "CREATE TABLE IF NOT EXISTS "+TAB.getTableName(TAB.USERS)+"( "
				+ "id bigserial,"
				+ "timestamp timestamp with time zone NOT NULL,"
				+ "username character varying(32) NOT NULL,"
				+ "userpasswd character varying(32),"
				+ "email character varying(32),"
				+ "flag bigint, "
				+ "CONSTRAINT users_pkey PRIMARY KEY (id, timestamp)" 
				+ ");"
				
				+ "CREATE TABLE IF NOT EXISTS "+TAB.getTableName(TAB.USERDETAILS)+"( "
				+ "uid bigint NOT NULL,"
				+ "timestamp timestamp with time zone NOT NULL,"
				+ "keyname character varying(32),"
				+ "format character varying(32),"
				+ "value text,"
				+ "location geometry(Point),"
				+ "flag bigint, "
				+ "CONSTRAINT userdetails_pkey PRIMARY KEY (uid, timestamp, keyname)"
				+ ");";
		
		String functions = ""
				+ "CREATE OR REPLACE FUNCTION bitand(a bigint, b bigint) \n"
				+ "RETURNS bigint AS $$ \n"
				+ "		SELECT a & b; \n"
				+ "$$ LANGUAGE SQL; \n"
				
				+ "CREATE OR REPLACE FUNCTION bitor(a bigint, b bigint) \n"
				+ "RETURNS bigint AS $$ \n"
				+ "		SELECT a | b; \n"
				+ "$$ LANGUAGE SQL; \n"
				
				+ "CREATE OR REPLACE FUNCTION dropTablesStartingWith(prefix text) \n "
				+ "RETURNS VOID AS $$ \n"
				+ "DECLARE _tbl text; \n"
				+ "BEGIN \n"
				+ "FOR _tbl  IN \n"
				+ "    SELECT quote_ident(table_schema) || '.' \n"
				+ "        || quote_ident(table_name)      -- escape identifier and schema-qualify! \n"
				+ "        FROM   information_schema.tables \n"
				+ "        WHERE  table_name LIKE prefix || '%'  -- your table name prefix \n"
				+ "        AND    table_schema NOT LIKE 'pg_%'     -- exclude system schemas \n"
				+ "        LOOP \n"
				+ "        -- RAISE NOTICE '%', \n"
				+ "        EXECUTE \n"
				+ "        'DROP TABLE ' || _tbl; \n"
				+ "        END LOOP; \n"
				+ "    END \n"
				+ "$$ LANGUAGE PLPGSQL; \n"
				
				+ "CREATE OR REPLACE FUNCTION addAllColumns(_tbl varchar, _names varchar[], _types varchar[]) \n"
				+ "RETURNS int AS $$ \n"
				+ "DECLARE eNames varchar(32)[] := array(SELECT column_name::varchar FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = _tbl); \n"
				+ "DECLARE eTypes varchar(32)[] := array(SELECT data_type::varchar FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = _tbl); \n"
				+ "DECLARE a int := array_length(_names,1); \n"
				+ "DECLARE n int := 0; \n"
				+ "DECLARE i int := 1; \n"
				+ "BEGIN \n"
				+ "		WHILE i <= a \n"
				+ "		LOOP \n"
				+ "			IF (_names[i] = ANY(eNames)) THEN \n"
				+ "				IF (_types[i] = (SELECT data_type FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = _tbl AND column_name = _names[i]))THEN \n"
				+ "					--DO NOTHING \n"
				+ "				ELSIF (EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = _tbl AND column_name = _names[i]||'_'||_types[i])) THEN \n"
				+ "					--DO NOTHING \n"
				+ "				ELSE \n"
				+ "					--RAISE NOTICE 'ALTER TABLE % ADD COLUMN % %', _tbl, quote_ident(_names[i])|| '_' || _types[i], _types[i]; \n"
				+ "					EXECUTE 'ALTER TABLE ' || _tbl || ' ADD COLUMN ' || quote_ident(_names[i]|| '_' || _types[i])  || ' ' || _types[i] ; \n"
				+ "					EXECUTE 'ALTER TABLE ' || _tbl || '_h ADD COLUMN ' || quote_ident(_names[i]|| '_' || _types[i])  || ' ' || _types[i] ; \n"
				+ "					n := n + 1; \n"
				+ "				END IF; \n"
				+ "			ELSE \n"
				+ "				--RAISE NOTICE 'ALTER TABLE % ADD COLUMN % %', _tbl, quote_ident(_names[i]), _types[i]; \n"
				+ "				EXECUTE 'ALTER TABLE ' || _tbl || ' ADD COLUMN ' || quote_ident(_names[i]) || ' ' || _types[i] ; \n"
				+ "				EXECUTE 'ALTER TABLE ' || _tbl || '_h ADD COLUMN ' || quote_ident(_names[i]) || ' ' || _types[i] ; \n"
				+ "				n := n + 1; \n"
				+ "			END IF; \n"
				+ "			i := i + 1; \n"
				+ "		END LOOP; \n"
				+ "		RETURN n; \n"
				+ "END \n"
				+ "$$ LANGUAGE PLPGSQL; \n"
				
				+ "CREATE OR REPLACE FUNCTION newestServiceTimestamp(_sobjid bigint) \n"
				+ "RETURNS record AS $$ \n"
				+ "DECLARE scid int := (SELECT scid from lc_service_instances WHERE id = _sobjid)::int; \n"
				+ "DECLARE maxI timestamp with time zone := (SELECT MAX(timestamp) FROM lc_service_inputs WHERE sobjid = _sobjid); \n"
				+ "DECLARE maxG timestamp with time zone := NULL; \n"
				+ "DECLARE maxT timestamp with time zone := maxI; \n"
				+ "DECLARE ret RECORD; \n"
				+ "BEGIN \n"
				+ "	IF (scid > 0) THEN \n"
				+ "		EXECUTE format('SELECT MAX(timestamp) FROM lc_sc_%s', scid::text) INTO maxG; \n"
				+ "		maxT := GREATEST( maxI, maxG); \n"
				+ "		INSERT INTO lc_service_calls (sobjid, t_call, t_inputs, t_scn) \n"
				+ "			SELECT _sobjid, maxT, maxI, maxG WHERE NOT EXISTS( \n"
				+ "			SELECT * FROM lc_service_calls WHERE sobjid = _sobjid AND t_call = maxT \n"
				+ "		); \n"
				+ "	ELSE 	INSERT INTO lc_service_calls (sobjid, t_call) \n"
				+ "			SELECT _sobjid, maxI WHERE NOT EXISTS( \n"
				+ "			SELECT * FROM lc_service_calls WHERE sobjid = _sobjid AND t_call = maxT \n"
				+ "		); \n"
				+ "	END IF; \n"
				+ "	SELECT maxT, scid INTO ret; \n"
				+ "	RETURN ret; \n"
				+ "END; \n"
				+ "$$ LANGUAGE PLPGSQL; \n"
				
				
				+ "CREATE OR REPLACE FUNCTION dropColumns(_tbl varchar, _cols varchar[]) RETURNS INT AS $$ \n"
				+ "DECLARE c int := 0; \n"
				+ "DECLARE l int := array_length(_cols, 1); \n"
				+ "DECLARE i int := 1; \n"
				+ "BEGIN \n"
				+ "WHILE i <= l LOOP \n"
				+ "	IF (SELECT udt_name IS NOT NULL FROM INFORMATION_SCHEMA.columns WHERE TABLE_NAME = _tbl AND COLUMN_NAME = _cols[i]) THEN \n"
				+ "		EXECUTE format('ALTER TABLE %s DROP COLUMN \"%s\"', _tbl, _cols[i]); \n"
				+ "		EXECUTE format('ALTER TABLE %s_h DROP COLUMN \"%s\"', _tbl, _cols[i]); \n"
				+ "		c := c + 1; \n"
				+ "	END IF; \n"
				+ "	i := i + 1; \n"
				+ "END LOOP; \n"
				+ "RETURN c; \n"
				+ "END; \n"
				+ "$$ LANGUAGE PLPGSQL;";
				

//				+ "CREATE OR REPLACE FUNCTION create_type() RETURNS integer AS $$ "
//				+ "DECLARE v_exists INTEGER; "
//				+ "BEGIN "
//				+ "    SELECT into v_exists (SELECT 1 FROM pg_type WHERE typname = 'typed_string'); "
//				+ "    IF v_exists IS NULL THEN "
//				+ "        CREATE TYPE typed_string AS (t int, str text); "
//				+ "    END IF; "
//				+ "    RETURN v_exists; "
//				+ "END; "
//				+ "$$ LANGUAGE plpgsql; "
//				
//				+ "SELECT create_type(); ";
				
		Locale.setDefault(new Locale ("en", "US"));
		
		String[] tables = {extensions, service_tables, scenario_tables, userSQL, functions };
		int i = -1;
		try {
			for (String table : tables) {
				i++;
				PreparedStatement ps = con.prepareStatement(table);
				ps.executeUpdate();
			}
		} catch (SQLException e) {
			log.error("Database setup failed ("+i+")!");
			log.debug(e.toString());
			log.debug("SQL String: "+tables[i]);
			con.close();
			throw e;
		}
		con.close();
	}

	@Override
	public String resetTables(Connection con, Logger log) throws SQLException {
		/* delete views first otherwise we will get an error message upon deleting the tables */
		List<String> views = new ArrayList<String>();
		String viewsSQL = ""
				+ "select table_name from information_schema.views "
				+ "where table_catalog = '"+Luci.getConfig().getProperty("dbName")+"' AND table_schema = 'public' "
				+ "AND table_name not in ('geography_columns', 'geometry_columns', 'raster_columns', 'raster_overviews')";
		PreparedStatement ps = con.prepareStatement(viewsSQL);
		ResultSet rs = ps.executeQuery();
		while(rs.next()){
			String view = rs.getString(1);
			con.prepareStatement("DROP VIEW "+view).executeUpdate();
			views.add(view);
		}
		views.remove(TAB.getTableName(TAB.VIEW));
		String sql = ""
				+ "SELECT dropScenarioTables(?);"
				+ "DROP TABLE IF EXISTS "+TAB.getTableName(TAB.SCENARIOS)+";"
				+ "DROP TABLE IF EXISTS "+TAB.getTableName(TAB.SERVICECALLS)+";"
				+ "DROP TABLE IF EXISTS "+TAB.getTableName(TAB.SERVICEINPUTS)+";"
				+ "DROP TABLE IF EXISTS "+TAB.getTableName(TAB.SRV_IN_HISTORY)+";"
				+ "DROP TABLE IF EXISTS "+TAB.getTableName(TAB.SERVICES)+";"
				+ "DROP TABLE IF EXISTS "+TAB.getTableName(TAB.SERVICEOUTPUTS)+";"
				+ "DROP TABLE IF EXISTS "+TAB.getTableName(TAB.SRV_OUT_HISTORY)+";"
				+ "DROP TABLE IF EXISTS "+TAB.getTableName(TAB.USERDETAILS)+";";
		ps.setString(1, TAB.scn_base);
		ps = con.prepareStatement(sql);
		ps.executeUpdate();
		setupTables(con, log);
		return "Recreated 10 Luci tables, 1 view '"+TAB.getTableName(TAB.VIEW)+"'; deleted views: "+views;
	}

	/* (non-Javadoc)
	 * @see org.luci.factory.observer.IDatabase#createScenarioTables(java.sql.Connection, org.slf4j.Logger, long)
	 */
	@Override
	public void createScenarioTables(Connection con, Logger log, int ScID, int srid) throws SQLException {
		String geometry_column;
		if (srid == 0) geometry_column = "geom geometry(GeometryZM) ";
		else geometry_column = "geom geometry(GeometryZM, "+srid+") ";
		
		String sql = ""
				+ "CREATE TABLE IF NOT EXISTS "+TAB.getScenarioTable(ScID, TAB.SCN)+"("
				+ "geomID bigserial primary key, "
				+ "timestamp timestamp with time zone NOT NULL, "
				+ "batchID int NOT NULL DEFAULT 0,"
				+ "layer varchar(64) DEFAULT '0',"
				+ "nurb int,"
				+ "transform float[][],"
				+ "uid bigint,"
				+ "flag int, "
				+  geometry_column
				// attributes follow dinamically here
				+ ");"
				
				+ "CREATE TABLE IF NOT EXISTS "+TAB.getScenarioTable(ScID, TAB.SCN_HISTORY)+"("
				+ "timestamp_deleted timestamp with time zone,"
				+ "geomID bigserial, "
				+ "timestamp timestamp with time zone NOT NULL, "
				+ "batchID int NOT NULL DEFAULT 0,"
				+ "layer varchar(64),"
				+ "nurb int,"
				+ "transform float[][],"
				+ "uid bigint,"
				+ "flag int, "
				+  geometry_column +","
				// attributes follow dinamically here
				+ "CONSTRAINT sc_h_"+ScID+"_pkey PRIMARY KEY (geomID, timestamp)" // composite key
				+ ");"
				
				+ "CREATE TABLE IF NOT EXISTS "+TAB.getScenarioTable(ScID, TAB.SCN_VARIANT_MASK)+"("
				+ "geomID bigserial primary key);"
				// versions are being added dinamically here
				
				+ "CREATE TABLE IF NOT EXISTS "+TAB.getScenarioTable(ScID, TAB.SCN_VARIANTS)+"("
				+ "versionID bigserial primary key,"
				+ "versionKey text)"; // like 1.2.1.1.2.3
		
		PreparedStatement ps = con.prepareStatement(sql);
		try {
			ps.executeUpdate();
		} catch (SQLException e){
			con.close();
			System.out.println(ps);
			throw e;
		}
	}
	
	
	private short getType(String pgType){
		switch(pgType){
		case "numeric": return TAB.NUMERIC;
		case "int8": return TAB.NUMERIC;
		case "int4": return TAB.NUMERIC;
		case "float8": return TAB.NUMERIC;
		case "float4": return TAB.NUMERIC;
		case "_numeric": return TAB.NUMERICARRAY;
		case "_int8": return TAB.NUMERICARRAY;
		case "_int4": return TAB.NUMERICARRAY;
		case "_float8": return TAB.NUMERICARRAY;
		case "_float4": return TAB.NUMERICARRAY;
		case "_varchar": return TAB.CHECKSUM;
		case "varchar": return TAB.STRING;
		case "text": return TAB.STRING;
		case "bool": return TAB.BOOLEAN;
		case "json": return TAB.JSON;
		case "list": return TAB.LIST;
		case "geometry": return TAB.GEOMETRY;
		case "timestamp": return TAB.TIMESTAMP;
		default: return 0;
		}
	}
	
	/* (non-Javadoc)
	 * @see org.luci.factory.observer.IDatabase#getColumnNames(java.sql.Connection, java.lang.String)
	 */
	@Override
	public LinkedHashMap<String, Short> getColumnNamesAndTypes(Connection con, int ScID) throws SQLException {
		String tableName = TAB.getScenarioTable(ScID, TAB.SCN);
		String strTypes = TAB.getTableName(TAB.SCN_STR_TYPES);
		String sql = "SELECT c.column_name, (CASE WHEN c.udt_name = 'text' THEN "
				+ "COALESCE((SELECT format FROM "+strTypes+" WHERE scid = ? AND att_name = c.column_name), 'text') "
				+ "ELSE c.udt_name END) AS type "
				+ "FROM INFORMATION_SCHEMA.COLUMNS AS c WHERE TABLE_NAME = ?";
		PreparedStatement ps = con.prepareStatement(sql);
		ps.setLong(1,ScID);
		ps.setString(2,tableName);
		try {
			ResultSet rs = ps.executeQuery();
			LinkedHashMap<String,Short> columnNames = new LinkedHashMap<String,Short>();
			while(rs.next()) columnNames.put(rs.getString(1), getType(rs.getString(2)));
			return columnNames;
		} catch (SQLException e){
			con.close();
			throw e;
		}
	}

	/* (non-Javadoc)
	 * @see org.luci.factory.observer.IDatabase#addColumn(java.sql.Connection, java.lang.String, java.lang.String, short)
	 */
	@Override
	public int addColumns(Connection con, int ScID, Map<String, Short> attributes) 
			throws SQLException {
		int created = 0;
		if (attributes.size() == 0) return created;
		String tableName = TAB.getScenarioTable(ScID, TAB.SCN);
		String tabStr = TAB.getTableName(TAB.SCN_STR_TYPES);
		List<String> attributeNames = new ArrayList<String>();
		List<String> attributeTypes = new ArrayList<String>();
		Map<String, Short> stringTypes = new HashMap<String, Short>();
		attributeNames.addAll(attributes.keySet());
		for (String name: attributeNames) {
			short t = attributes.get(name);
			attributeTypes.add(types.get(t));
			if (t == TAB.JSON || t == TAB.LIST) stringTypes.put(name, t);
		}
		String sql = "SELECT addAllColumns(?,?,?);";
		PreparedStatement ps = con.prepareStatement(sql);
		int aN = attributeNames.size();
		int aT = attributeTypes.size();
		ps.setString(1, tableName);
		ps.setArray(2, con.createArrayOf("varchar", attributeNames.toArray(new String[aN])));
		ps.setArray(3, con.createArrayOf("varchar", attributeTypes.toArray(new String[aT])));
		try {
			ResultSet rs = ps.executeQuery();
			if (rs.next()) created = rs.getInt(1);
		} catch (SQLException e){
			con.close();
			throw e;
		}
		
		
		sql = "INSERT INTO "+tabStr+" (scid, att_name, format) SELECT ?, ?, ? WHERE NOT EXISTS ("
				+ "SELECT * FROM "+tabStr+" WHERE scid = ? AND att_name = ? )  ;";
		ps = con.prepareStatement(sql);
		for (Entry<String, Short> e: stringTypes.entrySet()){
			ps.setLong(1, ScID);
			ps.setString(2, e.getKey());
			ps.setString(3, TAB.TYPE_NAMES.get(e.getValue()));
			ps.setLong(4, ScID);
			ps.setString(5, e.getKey());
			ps.addBatch();
		}
		ps.executeBatch();
		return created;
	}

	/* (non-Javadoc)
	 * @see org.luci.factory.observer.IDatabase#getNewestTimestampForService(long)
	 */
	@Override
	public long[] getNewestTimestampForService(Connection con, long SObjID) throws SQLException {
		// STORED PROCEDURE
		// returns max timestamp and scid (and inserts the call to the calls table if necessary)
		String sql = "SELECT * FROM newestServiceTimestamp(?) AS nst (t timestamp with time zone, scid int)"; 
		PreparedStatement ps = con.prepareStatement(sql);
		ps.setLong(1, SObjID);
		ResultSet rs = ps.executeQuery();
		rs.next();
		return new long[]{rs.getTimestamp(1).getTime(), rs.getInt(2)};
	}

	/* (non-Javadoc)
	 * @see org.luci.factory.observer.IDatabase#dropColumn(long, java.lang.String)
	 */
	@Override
	public int dropColumns(Connection con, int ScID, String[] columnNames) throws SQLException {
		String sql = "SELECT dropColumns(?,?)";
		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, TAB.getScenarioTable(ScID, TAB.SCN));
		ps.setArray(2, con.createArrayOf("varchar", columnNames));
		ResultSet rs = ps.executeQuery();
		rs.next();
		return rs.getInt(1);
	}

	/* (non-Javadoc)
	 * @see org.luci.factory.observer.IDatabase#dropScenario(java.sql.Connection, long)
	 */
	@Override
	public void dropScenario(Connection con, int ScID) throws SQLException {
		String sql = "SELECT dropTablesStartingWith(?)";
		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, TAB.scn_base+ScID);
		ps.execute();
		sql = "DELETE FROM "+TAB.getTableName(TAB.SCENARIOS)+" WHERE id = ?";
		ps = con.prepareStatement(sql);
		ps.setInt(1, ScID);
		ps.execute();
	}

	
	
	/* (non-Javadoc)
	 * @see org.luci.factory.observer.IDatabase#getInsertSQL(java.util.Set, boolean)
	 */
	@Override
	public String getInsertSQL(long geomID, Set<String> fields, String tableName, String historyTable, Connection con)  {
		boolean isAutoID = geomID == 0;
		if (isAutoID) return getAutoInsertSQL(fields, tableName);
		else return getUpsertSQL(geomID, fields, tableName, historyTable);
	}

/**
	 * @param geomID
	 * @param fields
	 * @param tableName
	 * @param historyTable
	 * @return
	 */
	private String getUpsertSQL(long geomID, Set<String> fields, String tableName, String historyTable) {
		int i;
		StringBuilder sql = new StringBuilder("INSERT INTO "+historyTable+" SELECT NULL, * FROM "
				+ tableName+" WHERE geomid = "+geomID);
		for (String field: fields) sql.append(" AND \""+field+"\" IS NOT NULL");
		sql.append(";\n");
		sql.append("WITH entries AS (SELECT ?::timestamp with time zone AS _timestamp,"
				+ " ? AS _batchID, ? AS _layer, ? AS _nurb, ?::double precision[] AS _transform,"
				+ " ? AS _uid, ? AS _flag, ?::geometry AS _geom");
		for (i = 1; i <= fields.size(); i++) sql.append(", ? AS a"+(i));
		sql.append("), upsert AS (UPDATE "+tableName+" SET timestamp = _timestamp, batchID = _batchID,"
				+ "layer = COALESCE(_layer, layer), nurb = COALESCE(_nurb, nurb), transform = "
				+ "COALESCE(_transform, transform), uid = _uid, flag = COALESCE(_flag, flag), "
				+ "geom = COALESCE(_geom, geom)");
		i = 1;
		for (String field: fields) sql.append(", \""+field+"\" = a"+(i++));
		sql.append(" FROM entries WHERE geomid = "+geomID+" RETURNING *) INSERT INTO "+tableName+"("
				+ "geomID, timestamp, batchID, layer, nurb, transform, uid, flag, geom");
		for (String field: fields) sql.append(", \""+field+"\"");
		sql.append(") SELECT "+geomID+", _timestamp, _batchID, _layer, _nurb, _transform, _uid, "
				+ "_flag, _geom");
		for (i = 1; i <= fields.size(); i++) sql.append(", a"+(i));
		sql.append(" FROM entries WHERE NOT EXISTS (SELECT * FROM upsert);");
		return sql.toString();
	}

	private String getAutoInsertSQL(Set<String> fields,	String tableName) {
		StringBuilder sql = new StringBuilder("INSERT INTO "+tableName
				+" (timestamp, batchID, layer, nurb, transform, uid, flag, geom");
		for (String field: fields) sql.append(", \""+field+"\"");
		sql.append(") VALUES(?"+LcString.multiply(7+fields.size(), ",?")+")");
		return sql.toString();
	}

	/* (non-Javadoc)
	 * @see org.luci.factory.observer.IDatabase#updateAutoIDSequence(java.sql.Connection, java.lang.String)
	 */
	@Override
	public long updateAutoIDSequence(Connection con, String tableName, String idName) throws SQLException {
		// "alter sequence "+tableName+"_"+idName+"_seq minvalue 0;"
		String sql = "SELECT setval('"+tableName+"_"+idName+"_seq', "
				+ "(SELECT coalesce(MAX("+idName+")+1,1) FROM "+tableName+"))";
		try {
			ResultSet rs = con.prepareStatement(sql).executeQuery();
			rs.next();
			return rs.getLong(1);
		} catch (SQLException e){
			con.close();
			System.out.println(sql);
			throw e;
		}
		
	}

	/* (non-Javadoc)
	 * @see org.luci.factory.observer.IDatabase#updateAutoIDSequence(java.sql.Connection, java.lang.String, long)
	 */
	@Override
	public void updateAutoIDSequence(Connection con, String tableName, String idName, long id) throws SQLException {
		String sql = "ALTER SEQUENCE "+tableName+"_"+idName+"_seq RESTART WITH "+id;
		con.prepareStatement(sql).execute();
	}

	@Override
	public int setChecksum(PreparedStatement ps, int i, String format, String checksum, Connection con) throws SQLException {
		ps.setArray(i, new TAB.Checksum(format, checksum).getSQLArray(con));
		return 0;
	}
}


