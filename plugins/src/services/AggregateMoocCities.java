/**
 * The MIT License (MIT)
 *
 * Copyright (c) <year> <copyright holders>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package services;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import org.json.JSONArray;
import org.json.JSONObject;
import org.luci.comm.action.Action;
import org.luci.connect.LcInputStream;
import org.luci.connect.LcJSONObject;
import org.luci.connect.LcMetaJSONObject;
import org.luci.connect.LcString;
import org.luci.connect.StreamInfo;
import org.luci.converters.geometry.GeometrySelector;
import org.luci.converters.geometry.IGeoJSONWriter;
import org.luci.factory.ServiceFactoryThread;
import org.luci.factory.observer.IDatabase;
import org.luci.services.Service;
import org.luci.services.ServiceCall;
import org.luci.utilities.Projection;
import org.luci.utilities.TimeRangeSelection;

/**
 * 
 * @author Lukas Treyer
 * service to aggregate the images based on their location (city attribute)
 * use: {'action':'run', 'service':{'classname':'aggregatemooccities', 'inputs':{'ScID':27,'switchLatLon':true,'selected':false}}}
 * switchLatLon to switch lat/lon to lon/lat (and vice versa; always tricky when using different sources)
 * selected: mooc application specific: aggregate only selected cities and images
 */
public class AggregateMoocCities extends Service{
	public AggregateMoocCities(ServiceCall call, ServiceFactoryThread factory, String machinename) 
			throws SQLException, InstantiationException, IOException, InterruptedException  {
		super(call, factory, machinename);
	}

	/* (non-Javadoc)
	 * @see org.luci.services.Service#getInputIndex()
	 */
	@Override
	public LcMetaJSONObject getInputIndex() {
		String inputjson = "{"
				+ "		'ScID':'number',"
				+ "		'OPT timerange':'timerange',"
				+ "		'OPT switchLatLon':'boolean',"
				+ "		'OPT selected':'boolean'"
				+ "	}";
		return new LcMetaJSONObject(inputjson);
	}

	/* (non-Javadoc)
	 * @see org.luci.services.Service#getOutputIndex()
	 */
	@Override
	public LcMetaJSONObject getOutputIndex() {
		return new LcMetaJSONObject("{'moocCities':'streaminfo'}");
	}

	/* (non-Javadoc)
	 * @see org.luci.services.Service#call()
	 */
	@Override
	public Object call() throws Exception {
		JSONObject in = getInputs();
		ScID = in.getLong("ScID");
		JSONObject joutput = new JSONObject();
		IDatabase db = o.dataModelImplementation();
		Connection con  = o.getDatabaseConnection();
		Projection pc = new Projection("EPSG:4326");
		if (in.optBoolean("switchLatLon", false)) pc.switchLatLon();
		boolean selectedOnly = in.optBoolean("selected", false);
		IGeoJSONWriter g = o.getGeoJSON_FromDb(pc);
		Map<String, Short> types = db.getColumnNamesAndTypes(con, (int) ScID);
		TimeRangeSelection trs = TimeRangeSelection.NULL();
		if (in.has("timerange")) trs = new TimeRangeSelection(in.getJSONObject("timerange"));
		
		Action exists = o.getAction("exists", null, new LcJSONObject("{'ScID':"+ScID+"}"));
		JSONObject result = exists.perform(null, null, null);
		if (result.has("result"))
			if (!result.getBoolean("result")) throw new RuntimeException("ScID "+ScID+" does not exists!");
		
		GeometrySelector gs = new GeometrySelector(o, (int)ScID, trs);
		String sql = gs.getSelectionSQL();

		boolean hasSelectedKey = types.containsKey("selected");
		if (selectedOnly && hasSelectedKey) sql += " AND selected ";
		ResultSet rs = con.prepareStatement(sql).executeQuery();
		if (!types.containsKey("city")) 
			throw new RuntimeException("Scenario geometry does not have a 'city' attribute!");
		if (!types.containsKey("country")) 
			throw new RuntimeException("Scenario geometry does not have a 'country' attribute!");
		
		
		joutput.put("ScID", ScID);
		TreeSet<String> selectedCities = new TreeSet<String>(new Comparator<String>(){

			@Override
			public int compare(String o1, String o2) {
				return o1.compareTo(o2);
			}});
		while(rs.next()){
			String cityName = rs.getString("city");
			if (cityName == null) continue;
			JSONObject city;
			if (joutput.has(cityName)) city = joutput.getJSONObject(cityName);
			else {
				city = new JSONObject();
				JSONObject point = g.geometryFromDb(rs.getObject("geom"));
				double lat = point.getJSONArray("coordinates").getDouble(0);
				double lon = point.getJSONArray("coordinates").getDouble(1);
				JSONObject location = new JSONObject();
				location.put("lat",lat);
				location.put("lon", lon);
				city.put("location", location);
				city.put("country", rs.getString("country"));
				joutput.put(cityName, city);
			}
			long geomid = rs.getLong("geomID");
			city.append("images", geomid);
			if (hasSelectedKey && !selectedOnly) {
				boolean selected = rs.getBoolean("selected");
				if (selected) {
					city.append("selectedImages", geomid);
					selectedCities.add(cityName);
				}
			}
		}
		
		joutput.put("selectedCities", new JSONArray(selectedCities));
		con.close();
		ByteBuffer bb = ByteBuffer.wrap(joutput.toString(4).getBytes("UTF-8"));
		String csum = LcString.calcChecksum(bb);
		StreamInfo si = new StreamInfo(csum, "json", "moocCities", 1, bb.capacity(), pc.getEPSG(), null);
		LcInputStream lis = new LcInputStream(si, new ByteArrayInputStream(bb.array()));
		lis.setReady();
		List<LcInputStream> streams = new ArrayList<LcInputStream>();
		streams.add(lis);
		JSONObject output = new JSONObject();
		output.put("moocCities", lis.getJSON());
		setOutputs(output, streams);
		return output;
	}

	/* (non-Javadoc)
	 * @see org.luci.services.Service#getVersion()
	 */
	@Override
	public String getVersion() {
		return "v0.1";
	}

}
