package services;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;
import org.luci.connect.LcInputStream;
import org.luci.connect.LcMetaJSONObject;
import org.luci.factory.ServiceFactoryThread;
import org.luci.services.Service;
import org.luci.services.ServiceCall;


public class FileEcho extends Service {
	
	public FileEcho(ServiceCall call, ServiceFactoryThread sf, String machinename) throws Exception{
		super(call, sf, machinename);
	}
	
	@Override
	public Object call() throws Exception {
//		long start = System.currentTimeMillis();
//		long dur = System.currentTimeMillis() - start;
//		System.out.println(dur);
		List<LcInputStream> inputStreams = new ArrayList<LcInputStream>(); 
		JSONObject inputs = getInputs(false, inputStreams);
		setOutputs(inputs, inputStreams);
		return inputs;
	}

	@Override
	public LcMetaJSONObject getInputIndex() {
		return new LcMetaJSONObject("{'KEYNAME':"+LcMetaJSONObject.streaminfo+"}");
	}

	@Override
	public LcMetaJSONObject getOutputIndex() {
		return new LcMetaJSONObject("{'KEYNAME':"+LcMetaJSONObject.streaminfo+"}");
	}

	@Override
	public String getVersion() {
		return "0.2";
	}
}
