/**
 * The MIT License (MIT)
 *
 * Copyright (c) <year> <copyright holders>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package services;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.json.JSONArray;
import org.json.JSONObject;
import org.luci.connect.LcMetaJSONObject;
import org.luci.connect.LcString;
import org.luci.factory.ServiceFactoryThread;
import org.luci.services.Service;
import org.luci.services.ServiceCall;
import org.luci.utilities.TAB;

/**
 * 
 * @author Lukas Treyer
 *
 */
public class HistoricRelation extends Service {

	/**
	 * @param call
	 * @param factory
	 * @param machinename
	 * @throws SQLException
	 * @throws InstantiationException
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public HistoricRelation(ServiceCall call, ServiceFactoryThread factory, String machinename) 
			throws SQLException, InstantiationException, IOException, InterruptedException {
		super(call, factory, machinename);
	}

	/* (non-Javadoc)
	 * @see org.luci.services.Service#getInputIndex()
	 */
	@Override
	public LcMetaJSONObject getInputIndex() {
		String inputjson = "{"
				+ "		'XOR ScID':'number',"
				+ "		'XOR SObjID':'number',"
				+ "		'relevantAttributes':'list',"
				+ "		'OPT geomIDs':'list',"
				+ "		'OPT timerange':'timerange'"
				+ "	}";
		return new LcMetaJSONObject(inputjson);
	}

	/* (non-Javadoc)
	 * @see org.luci.services.Service#getOutputIndex()
	 */
	@Override
	public LcMetaJSONObject getOutputIndex() {
		return new LcMetaJSONObject("{"
				+ "		'XOR geomIDs':[{"
				+ "			'geomID':'number',"
				+ "			'ATTRIBUTE':'list'"
				+ "			}],"
				+ "		'XOR serviceOutputs':{"
				+ "			'ATTRIBUTE':'list'"
				+ "		}"
				+ "}");
	}

	/* (non-Javadoc)
	 * @see org.luci.services.Service#call()
	 */
	@Override
	public Object call() throws Exception {
		JSONObject inputs = this.getInputs();
		JSONArray atts = inputs.getJSONArray("relevantAttributes");
		int numAtts = atts.length();
		Connection con = o.getDatabaseConnection();
		if (inputs.has("ScID")){
			ScID = inputs.getInt("ScID");
			String geomids = "";
			if (inputs.has("geomIDs")) {
				geomids = "("+LcString.trim(inputs.getJSONArray("geomIDs").toString(), 1)+")";
			}
			StringBuilder sql = new StringBuilder("SELECT geomID,");
			for (int i = 0; i < numAtts; i++){
				String att = atts.getString(i).split(";")[0];
				sql.append("(SELECT SUM(CASE WHEN h.\""+att+"\" > c.\""+att+"\" THEN 1 ELSE 0 END) || "
						+ "   ' ' || SUM(CASE WHEN h.\""+att+"\" < c.\""+att+"\" THEN 1 ELSE 0 END) "
						+ " FROM "+TAB.getScenarioTable(ScID, TAB.SCN_HISTORY)
						+ "       AS h WHERE h.geomid = c.geomid) AS \""+att+"\"");
			}
			sql.append(" FROM "+TAB.getScenarioTable(ScID, TAB.SCN)+" AS c WHERE geomid in "+geomids);
			try {
				ResultSet rs = con.prepareStatement(sql.toString()).executeQuery();
				JSONArray geomIDs_ = new JSONArray();
				while(rs.next()){
					JSONObject geomID = new JSONObject().put("geomID", rs.getInt(1));
					for (int i = 0; i < numAtts; i++){
						String att = atts.getString(i).split(";")[0];
						String str = rs.getString(i+2);
						if (str != null){
							String[] gt_lt_str = str.split(" ");
							JSONArray gt_lt = new JSONArray()
								.put(Integer.parseInt(gt_lt_str[0]))
								.put(Integer.parseInt(gt_lt_str[1]));
							geomID.put(att, gt_lt);
						}
					}
					geomIDs_.put(geomID);
				}
				con.close();
				return new JSONObject().put("geomIDs", geomIDs_);
			} catch (SQLException e){
				con.close();
				throw e;
			}
		}
		
		return null;
	}

	/* (non-Javadoc)
	 * @see org.luci.services.Service#getVersion()
	 */
	@Override
	public String getVersion() {
		return "0.1";
	}

}
