package services;

import java.io.File;

import org.json.JSONObject;
import org.luci.Luci;
import org.luci.connect.DirectStreamWriter;
import org.luci.connect.LcInputStream;
import org.luci.connect.LcMetaJSONObject;
import org.luci.connect.LcOutputStream;
import org.luci.factory.ServiceFactoryThread;
import org.luci.services.Service;
import org.luci.services.ServiceCall;


public class LuciLogo extends Service {
	
	public LuciLogo(ServiceCall sih, ServiceFactoryThread sf, String machinename) throws Exception{
		super(sih, sf, machinename);
	}

	@Override
	public Object call() throws Exception {
		File f = new File(Luci.getWorkingDir() + File.separator + "resources" + File.separator + "logo_bw.png");
		LcInputStream lis = LcInputStream.load(f);
		lis.setName("LuciLogo");
		LcOutputStream los = new LcOutputStream(lis);
		o.getThreadPool().submit(new DirectStreamWriter(lis, los, true, false, false, false));
		JSONObject outputs = new JSONObject().put("LuciLogo", los);
		setOutputs(outputs);
		return outputs;
	}

	@Override
	public LcMetaJSONObject getInputIndex() {
		return new LcMetaJSONObject();
	}

	@Override
	public LcMetaJSONObject getOutputIndex() {
		return new LcMetaJSONObject("{'logo':{'format':'png','streaminfo':{'checksum':'string','order':'number'}}}");
	}

	@Override
	public String getVersion() {
		return "v0.2";
	}
}
