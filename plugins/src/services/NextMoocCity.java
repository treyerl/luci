/**
 * The MIT License (MIT)
 *
 * Copyright (c) <year> <copyright holders>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package services;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.json.JSONObject;
import org.luci.connect.LcMetaJSONObject;
import org.luci.factory.ServiceFactoryThread;
import org.luci.services.Service;
import org.luci.services.ServiceCall;
import org.luci.utilities.TAB;

/**
 * 
 * @author Lukas Treyer
 *
 */
public class NextMoocCity extends Service {

	/**
	 * @param call
	 * @param factory
	 * @param machinename
	 * @throws SQLException
	 * @throws InstantiationException
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public NextMoocCity(ServiceCall call, ServiceFactoryThread factory,
			String machinename) throws SQLException, InstantiationException,
			IOException, InterruptedException {
		super(call, factory, machinename);
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see org.luci.services.Service#getInputIndex()
	 */
	@Override
	public LcMetaJSONObject getInputIndex() {
		return new LcMetaJSONObject("{'ScID':'number'}");
	}

	/* (non-Javadoc)
	 * @see org.luci.services.Service#getOutputIndex()
	 */
	@Override
	public LcMetaJSONObject getOutputIndex() {
		return new LcMetaJSONObject("{'geomID':'number','city':'string'}");
	}

	/* (non-Javadoc)
	 * @see org.luci.services.Service#call()
	 */
	@Override
	public Object call() throws Exception {
		JSONObject inputs = this.getInputs();
		ScID = inputs.getInt("ScID");
		String scn = TAB.getScenarioTable(ScID, TAB.SCN);
		String sql = "select \"city\", geomID from "+scn+" where \"selected\" AND (coalesce(\"self\",0) + coalesce(\"others\",0)) = "
				+ " (SELECT min(coalesce(\"self\",0) + coalesce(\"others\",0)) from lc_sc_1 where \"selected\")"
				+ " ORDER BY random() limit 1;";
//		System.out.println(sql);
		Connection con = o.getDatabaseConnection();
		try {
			ResultSet rs = con.prepareStatement(sql).executeQuery();
			if (rs.next()){
				JSONObject r = new JSONObject();
				r.put("geomID", rs.getInt("geomid"));
				r.put("city", rs.getString("city"));
				con.close();
				return r;
			} else {
				con.close();
				throw new RuntimeException("No Next City Found!");
			}
		} catch(SQLException e){
			con.close();
			throw e;
		}
	}

	/* (non-Javadoc)
	 * @see org.luci.services.Service#getVersion()
	 */
	@Override
	public String getVersion() {
		// TODO Auto-generated method stub
		return null;
	}

}
