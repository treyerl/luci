/**
 * The MIT License (MIT)
 *
 * Copyright (c) <year> <copyright holders>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package services;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;
import org.luci.comm.action.Action;
import org.luci.connect.LcJSONObject;
import org.luci.connect.LcMetaJSONObject;
import org.luci.factory.ServiceFactoryThread;
import org.luci.factory.observer.IDatabase;
import org.luci.services.Service;
import org.luci.services.ServiceCall;
import org.luci.utilities.TAB;

/**
 * 
 * @author treyerl
 *
 */
public class RankImages extends Service {

	public RankImages(ServiceCall call, ServiceFactoryThread factory, String machinename) 
			throws SQLException, InstantiationException, IOException, InterruptedException  {
		super(call, factory, machinename);
	}

	/* (non-Javadoc)
	 * @see org.lucy.services.Service#getInputIndex()
	 */
	@Override
	public LcMetaJSONObject getInputIndex() {
		return new LcMetaJSONObject("{'ScID':'number'}");
	}

	/* (non-Javadoc)
	 * @see org.lucy.services.Service#getOutputIndex()
	 */
	@Override
	public LcMetaJSONObject getOutputIndex() {
		return new LcMetaJSONObject("{'imageRank':'streaminfo', 'OPT byCity':'boolean'}");
	}

	/* (non-Javadoc)
	 * @see org.lucy.services.Service#call()
	 */
	@Override
	public Object call() throws Exception {
		JSONObject in = getInputs();
		ScID = in.getLong("ScID");
		IDatabase db = o.dataModelImplementation();
		Connection con  = o.getDatabaseConnection();
		
		// exists
		Action exists = o.getAction("exists", null, new LcJSONObject("{'ScID':"+ScID+"}"));
		JSONObject result = exists.perform(null, null, null);
		if (result.has("result"))
			if (!result.getBoolean("result")) 
				throw new RuntimeException("ScID "+ScID+" does not exists!");
		
		// missing attributes
		Set<String> missing = new HashSet<String>();
		Map<String, Short> types = db.getColumnNamesAndTypes(con, (int) ScID);
		if (!types.containsKey("selected")) missing.add("selected");
		if (!types.containsKey("self")) missing.add("self");
		if (!types.containsKey("others")) missing.add("others");
		if (!types.containsKey("city")) missing.add("city");
		if (missing.size() > 0) 
			throw new RuntimeException("Scenario "+ScID+" is missing attribute(s): " + missing);
		
		boolean byCity = (in.has("byCity")) ? in.getBoolean("byCity") : false;
		String tab = TAB.getScenarioTable(ScID, TAB.SCN);
		String sql = "";
		if (byCity){
			sql = "SELECT * FROM (SELECT \"city\", COALESCE(AVG(CASE WHEN (COALESCE(\"self\",0) + COALESCE(\"others\",0)) > 0 THEN"
					+ "	COALESCE(\"self\",0)::float/(COALESCE(\"self\",0) + COALESCE(\"others\",0))::float ELSE NULL END),0) as ratio,"
					+ " SUM(COALESCE(\"self\",0) + COALESCE(\"others\",0)) AS numGuesses "
					+ " FROM lc_sc_1 WHERE \"selected\" GROUP BY \"city\" ORDER by ratio desc, numGuesses desc, \"city\") "
					+ " as q WHERE \"city\" IS NOT NULL";
		} else {
			sql = "SELECT geomid, (CASE WHEN (COALESCE(\"self\",0) + COALESCE(\"others\",0)) > 0  THEN "
					+ " COALESCE(\"self\",0)::float/(COALESCE(\"self\",0) + COALESCE(\"others\",0))::float "
					+ " ELSE 0 END)::float as ratio, city , "
					+ " COALESCE(\"self\",0) + COALESCE(\"others\",0) AS numGuesses "
					+ " FROM "+ tab +" WHERE \"selected\" ORDER by ratio desc, numGuesses desc, \"city\"";
		}
		try {
			ResultSet rs = con.prepareStatement(sql).executeQuery();
			JSONArray ratios = new JSONArray();
			int maxGuesses = 0;
			if (byCity){
				while(rs.next()){
					int maxGuess = rs.getInt(3);
					ratios.put(new JSONObject()
					.put("city", rs.getString(1))
					.put("ratio", rs.getFloat(2))
					.put("numGuesses", maxGuess));
					maxGuesses = Math.max(maxGuesses, maxGuess);
				}
			} else {
				while(rs.next()){
					int maxGuess = rs.getInt(4);
					ratios.put(new JSONObject()
					.put("geomID", rs.getInt(1))
					.put("ratio", rs.getFloat(2))
					.put("city", rs.getString(3))
					.put("numGuesses", maxGuess));
					maxGuesses = Math.max(maxGuesses, maxGuess);
				}
			}
			con.close();
			return new JSONObject().put("ratios", ratios).put("maxNumGuesses", maxGuesses);
		} catch (SQLException e){
			con.close();
			throw e;
		}
	}

	/* (non-Javadoc)
	 * @see org.lucy.services.Service#getVersion()
	 */
	@Override
	public String getVersion() {
		return "v0.1";
	}

}
