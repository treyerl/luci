package services.VeryComplexService;

import org.json.JSONObject;
import org.luci.connect.LcMetaJSONObject;
import org.luci.factory.ServiceFactoryThread;
import org.luci.services.Service;
import org.luci.services.ServiceCall;

public class VeryComplexService extends Service{
	EncapsulatedComplexity ec;
	public VeryComplexService(ServiceCall call, ServiceFactoryThread factory, String machinename) throws Exception{
		super(call, factory, machinename);
		ec = new Mc2();
	}

	@Override
	public LcMetaJSONObject getInputIndex() {
		return new LcMetaJSONObject();
	}

	@Override
	public LcMetaJSONObject getOutputIndex() {
		return new LcMetaJSONObject("{'explanation':'string'}");
	}

	@Override
	public Object call() throws Exception {
		String result = "VEEEERY complex service, showing that the class loader is able to handle "
				+ "services that are implemented across several classes. By definition / convention the folder "
				+ "containing all these classes must have a class file with the same name as the folder "
				+ "which will be used as the name of the service. ";
		this.setOutputs(new JSONObject().put("explanation", result));
		return null;
	}

	@Override
	public String getVersion() {
		return "the speed of light";
	}

}
