package services;

import java.io.IOException;
import java.sql.SQLException;

import org.json.JSONArray;
import org.json.JSONObject;
import org.luci.connect.LcMetaJSONObject;
import org.luci.factory.ServiceFactoryThread;
import org.luci.services.Service;
import org.luci.services.ServiceCall;

public class fibonacci extends Service {

	public fibonacci(ServiceCall call, ServiceFactoryThread factory, String machinename) 
			throws SQLException, InstantiationException, IOException, InterruptedException {
		super(call, factory, machinename);
	}

	@Override
	public LcMetaJSONObject getInputIndex() {

		return new LcMetaJSONObject("{'amount':'number'}");
	}

	@Override
	public LcMetaJSONObject getOutputIndex() {
		return new LcMetaJSONObject("{'fibonacci_sequence':'list'}");
	}

	@Override
	public Object call() throws Exception {
		JSONObject in = getInputs();
		JSONObject outputs = new JSONObject();
		int amount = in.getInt("amount");
		JSONArray fibonacci_sequence = new JSONArray();
		fibonacci_sequence.put(0);
		fibonacci_sequence.put(1);
		if(amount > 1) {
			for(int i = 2; i < amount; i++) 
				fibonacci_sequence.put(fibonacci_sequence.getInt(i-2)+fibonacci_sequence.getInt(i-1));
		}
		outputs.put("fibonacci_sequence", fibonacci_sequence);
		setOutputs(outputs);
		return outputs;
	}

	@Override
	public String getVersion() {
		return "v0.1";
	}

}
