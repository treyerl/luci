package services;

import java.util.Random;

import org.json.JSONArray;
import org.json.JSONObject;
import org.luci.connect.LcMetaJSONObject;
import org.luci.factory.ServiceFactoryThread;
import org.luci.services.Service;
import org.luci.services.ServiceCall;


public class random_numbers extends Service {
	
	public random_numbers(ServiceCall sih, ServiceFactoryThread sf, String machinename) throws Exception{
		super(sih, sf, machinename);
	}

	@Override
	public Object call() throws Exception {
		Random r = new Random();
		JSONObject in = getInputs(false, null);
		JSONArray keynames = in.optJSONArray("keys");
		JSONObject outputs = new JSONObject();
		if (keynames != null){
			for (int i = 0; i < keynames.length(); i++) outputs.put(keynames.getString(i), Math.abs(r.nextInt()));
		} else {
			int amount = in.getInt("amount");
			JSONArray randomNumbers = new JSONArray();
			for (int i = 0; i < amount; i++) randomNumbers.put(Math.abs(r.nextInt()));
			outputs.put("randomNumbers", randomNumbers);
		}
		setOutputs(outputs);
		return outputs;
	}

	@Override
	public LcMetaJSONObject getInputIndex() {
		return new LcMetaJSONObject("{'XOR keys':'list', 'XOR amount':'number'}");
	}

	@Override
	public LcMetaJSONObject getOutputIndex() {
		return new LcMetaJSONObject("{'XOR randomNumbers':'list', 'XOR KEYNAME':'number'}");
	}

	@Override
	public String getVersion() {
		return "v0.1";
	}
}
