package services;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.luci.Luci;
import org.luci.connect.LcMetaJSONObject;
import org.luci.factory.ServiceFactoryThread;
import org.luci.services.Service;
import org.luci.services.ServiceCall;

public class rscript extends Service {

	public rscript(ServiceCall call, ServiceFactoryThread factory,
			String machinename) throws SQLException, SecurityException,
			NoSuchMethodException, IllegalArgumentException,
			InstantiationException, IllegalAccessException,
			InvocationTargetException, IOException, InterruptedException {
		super(call, factory, machinename);
	}

	@Override
	public LcMetaJSONObject getInputIndex() {
		String inputjson = "{'XOR expression':['string','list'],'XOR file':'streaminfo'}";
		return new LcMetaJSONObject(inputjson);
	}

	@Override
	public LcMetaJSONObject getOutputIndex() {
		return new LcMetaJSONObject("{'outputlines':'list'}");
	}

	@Override
	public Object call() throws Exception {
		ProcessBuilder rscript = null;
		JSONObject inputs = getInputs(false, null);
		
		if (inputs.has("expression")){
			String command = inputs.optString("expression");
			if (command != null) rscript = new ProcessBuilder("rscript", "-e", command);
			
			JSONArray expr = inputs.optJSONArray("expression");
			if (expr != null) {
				List<String> cmd = new ArrayList<String>();
				cmd.add("rscript");
				for (int i = 0; i < expr.length(); i++){
					cmd.add("-e");
					cmd.add(expr.getString(i));
				}
				rscript = new ProcessBuilder(cmd);
			}
		} else {
			System.out.println(inputs);
			JSONObject streaminfo = inputs.getJSONObject("file").getJSONObject("streaminfo");
			String filename = streaminfo.getString("checksum")+"."+inputs.getJSONObject("file").getString("format");
			File script = new File(new File(Luci.getWorkingDir(), "data"), filename);
			rscript = new ProcessBuilder("rscript", script.getAbsolutePath());
		}
		if (rscript != null){
			try {
				Process r = rscript.start();
				InputStream out = r.getInputStream();
				List<String> lines = new ArrayList<String>();
				BufferedReader buff = new BufferedReader(new InputStreamReader(out));
				String aux = "";
				while ((aux = buff.readLine()) != null) lines.add(aux);
				r.waitFor();
				setOutputs(new JSONObject().put("result", new JSONObject().put("lines", lines)));
			} catch (Exception e){
				setOutputs(new JSONObject().put("error", e.toString()));
				throw e;
			}
		}
		return null;
	}

	@Override
	public String getVersion() {
		return "0.1";
	}

}
