package org.luci;

public class BrokerShutdown implements Runnable {
	private Process pr;
	
	public BrokerShutdown(Process pr){
		this.pr = pr;
	}
	
	@Override
	public void run() {
		pr.destroy();
	}

}
