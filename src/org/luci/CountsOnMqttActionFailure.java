package org.luci;

import java.util.concurrent.CountDownLatch;

import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttToken;

public class CountsOnMqttActionFailure implements IMqttActionListener{
	private CountDownLatch cdl;
	public CountsOnMqttActionFailure(CountDownLatch cdl){
		this.cdl = cdl;
	}
	
	@Override
	public void onFailure(IMqttToken arg0, Throwable e) {
		cdl.countDown();
	}

	@Override
	public void onSuccess(IMqttToken arg0) {
		
	}
	
}
