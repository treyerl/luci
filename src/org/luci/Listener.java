package org.luci;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.luci.comm.clienthandlers.ClientHandlerObserver;
import org.luci.comm.clienthandlers.ClientProxy;
import org.luci.connect.LcSocket;
import org.luci.factory.observer.FactoryObserver;
import org.slf4j.Logger;

/**
 * Listener Class that will wait for client calls. Implementing some probably
 * hackish methods to stop the thread. --> Further reading on thread killing
 * necessary.
 * 
 * @author Lukas Treyer
 * 
 */
public class Listener implements Runnable {
	private int portNr;
	private Logger log;
	private ClientHandlerObserver co;
	private FactoryObserver fo;
	private ServerSocket serverSocket;
	private String hellomsg;
	private volatile boolean running;

	public Listener(int no, FactoryObserver fo, ClientHandlerObserver co) {
		super();
		portNr = no;
		this.co = co;
		this.fo = fo;
		log = Luci.createLoggerFor("Listener");
		running = true;
		
	}

	/**
	 * creates a ServerSocket listening on the given port number. running an
	 * endless loop waiting for clients. Upon client connection a ClientHandler
	 * will be created to take further care of the client.
	 * 
	 * @see java.lang.Thread#run(), java.net.ServerSocket
	 */
	@Override
	public void run() {
		try {
			serverSocket = new ServerSocket(portNr);
			InetAddress host = InetAddress.getLocalHost();
			this.hellomsg = "Luci ("+Luci.getVersion()+") listening at " + host.getHostAddress() + ":" + portNr 
					+ ", also known as: " + host.getHostName() + ":" + portNr + " - " 
					+ new SimpleDateFormat("yyyyMMdd HH:mm:ss").format(new Date()).toString();
			log.info(hellomsg);
			System.out.println(hellomsg);
			while (running) {
				Socket acceptSocket = serverSocket.accept();
				co.run(new ClientProxy(new LcSocket(acceptSocket), fo, co));
			}
		} catch (IOException e) {
			log.error(e.getMessage());
		}
		//pool.shutdown();
	}
	
	public String getInfo(){
		return this.hellomsg;
	}
	
	public void stop() throws IOException{
		this.serverSocket.close();
		this.running = false;
	}
}
