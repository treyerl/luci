package org.luci;
 
import java.awt.AWTException;
import java.awt.Desktop;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.print.PageFormat;
import java.awt.print.PrinterJob;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.net.URL;
import java.nio.charset.Charset;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.apache.activemq.broker.BrokerFactory;
import org.apache.activemq.broker.BrokerService;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPageable;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttAsyncClient;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.json.JSONException;
import org.luci.comm.clienthandlers.ClientHandlerObserver;
import org.luci.comm.clienthandlers.ClientProxy;
import org.luci.factory.observer.FactoryObserver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.FileAppender;
import ch.qos.logback.core.rolling.DefaultTimeBasedFileNamingAndTriggeringPolicy;
import ch.qos.logback.core.rolling.RollingFileAppender;
import ch.qos.logback.core.rolling.TimeBasedRollingPolicy;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

/**
 * Main class being called from static void main(String[] args) looking for a
 * config file given as a command line parameter by default looking for a file
 * called "config.properties" if no file at all is given, the middleware object
 * will run with hard coded values as for now.
 * 
 * @author Lukas Treyer
 * @version: $Revision: 147 $
 */
public class Luci {
	private JFrame LuciFrame = null; 
	
	private static final long serialVersionUID = -6475968613059531352L; //JFrame specific?

	public final static int KEYNAME = 0;
	public final static int CHECKSUM = 1;
	public final static int FORMAT = 2;
	public final static int FILENAME = 3;
	
	public final static int SIZEBYTES = 8; // the first 64bit indicate the length of the bytestream
	
	public final static int LINEWIDTH = 120;
	public final static int LINEHEIGHT = 7;
	
	public final static boolean DEBUG = true;
	
	public final static String LUCILOG = "luci.log";
	public final static String LUCICONF = "luci.conf";
	private static Logger logger;
	private static FileAppender<ILoggingEvent> luciLogFileAppender;
	private static Properties config;
	private static String workingDir;
	private static String logDir;
	private static File mosquittoBin;
	private static File jar;
	private static File configFile;
	private static Luci lu;
	public static File webroot;
	public static String webport;
	
	public final static String OSNAME = System.getProperty("os.name").toLowerCase();
	
	public final static int MAC_OS_X = 0;
	public final static int WINDOWS = 1;
	public final static int UNIX = 2;
	public final static int UNKOWN_OS = 3;
	
	public final static int OSTYPE = (OSNAME.contains("mac")) ? MAC_OS_X : (OSNAME.contains("win")) ? WINDOWS : 
		(OSNAME.contains("nix") || OSNAME.contains("nux") || OSNAME.contains("aix")) ? UNIX : UNKOWN_OS; 
	
	public final static int qos_0_at_most_once = 0;
	public final static int qos_1_at_least_once = 1;
	public final static int qos_2_exactly_once_with_handshake = 2;
	
	public final static int WSockTimeoutInMillis = 1000*60*120; //120min

	public static final int BUFFERSIZE = 8196;
	
	private static int MQTT_CONNECTION_TIMEOUT = 2;
	private static int SOCKET_TIMEOUT = 2;
	
	private static int SRID = 0;
	
	private static int clientID = 0;
	
	public static String getVersion(){
		Process p;
		String line;
		BufferedReader reader;
		String version = "0.9";
		try {
			p = Runtime.getRuntime().exec("man git");
			p.waitFor();
			reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
			line = reader.readLine();
			if (line.equals("No manual entry for git")) return version;
			p = Runtime.getRuntime().exec("git describe");
			p.waitFor();
		    reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
		    line = reader.readLine();
		    if (line.startsWith("fatal")) return version;
			return line;
		} catch (Exception e) {
			return version;
		}
	}
	
	public static Properties getConfig(){
		return Luci.config;
	}
	
	public static String getWorkingDir(){
		return Luci.workingDir;
	}
	
	public static String getLogDir(){
		return logDir;
	}
	
	public static File getDataRoot(){
		return new File(workingDir, "data");
	}
	
	public static String getMosquittoBin(){
		return mosquittoBin.getAbsolutePath();
	}
	
	public static int getMQTTConnectionTimeout(){
		return MQTT_CONNECTION_TIMEOUT;
	}
	
	public static Luci getLuciInstance(){
		return lu;
	}
	
	/**
	 * Creates (1) service observer and (2) connection listener. Listener is
	 * provided with the observer at creation time. The listener will create a
	 * ClientHandler object upon successful connection creation. The
	 * ClientHandler is provided with the service observer object, which is
	 * necessary for doing its job: negotiate with the client. (check whether
	 * requested services are available, if all the parameters are given and are
	 * given in the correct format etc.).
	 */
	public static void main(String[] args) {
		String origin = Luci.class.getResource("Luci.class").toString();
		File wDir = new File("");
		if (origin.contains("!")){
			jar = new File(origin.split("!")[0].replace("jar:file:", ""));
			switch(OSTYPE){
			case MAC_OS_X: wDir = new File(jar.getParentFile().getParentFile(), "Luci"); break;
			case WINDOWS: wDir = new File(jar.getParentFile(), "Luci"); break;
			default: wDir = jar.getParentFile();
			}
		}
		workingDir = wDir.getAbsolutePath();
		File log = new File(workingDir, "log");
		log.mkdir();
		logDir = log.getAbsolutePath();
		luciLogFileAppender = createFileAppender(new File(logDir, LUCILOG));
		logger = createLoggerFor("Luci");
		configFile = new File(wDir.getAbsolutePath() + File.separator + "config" + File.separator + "luci.conf");
		
		System.setProperty("luci.logPath", logDir);
		
		File data = new File(Luci.getWorkingDir(), "data");
		data.mkdir();

		switch (OSTYPE){
		case MAC_OS_X: mosquittoBin = new File(workingDir + "/lib/mosquitto/osx/mosquitto"); break;
		case WINDOWS: mosquittoBin = new File(workingDir + "\\lib\\mosquitto\\win\\mosquitto.exe"); break;
		case UNIX: mosquittoBin = new File(workingDir + "/lib/mosquitto/linux/mosquitto"); break;
		}
		
		loadConfig();
		
		webroot = new File(workingDir, config.getProperty("webroot", "web"));
		webport = config.getProperty("webport", "8080");
		
		lu = new Luci();
		if (!lu.headless) lu.load();
		lu.start();    
	}
	
	private static FileAppender<ILoggingEvent> createFileAppender(File file){
		LoggerContext lc = (LoggerContext) LoggerFactory.getILoggerFactory();
        PatternLayoutEncoder ple = new PatternLayoutEncoder();

        ple.setPattern("%date %level [%thread] %logger{10} [%file:%line] %msg%n");
        ple.setContext(lc);
        ple.start();
        
        DefaultTimeBasedFileNamingAndTriggeringPolicy<ILoggingEvent>
        timeBasedTriggeringPolicy = new DefaultTimeBasedFileNamingAndTriggeringPolicy<ILoggingEvent>();
        timeBasedTriggeringPolicy.setContext(lc);
        
        TimeBasedRollingPolicy<ILoggingEvent> 
        timeBasedRollingPolicy = new TimeBasedRollingPolicy<ILoggingEvent>();
        timeBasedRollingPolicy.setContext(lc);
        timeBasedRollingPolicy.setFileNamePattern(file.getAbsolutePath().replace(".log", "") + ".%d{YYYY-MM-DD}.log");
        
        timeBasedRollingPolicy.setTimeBasedFileNamingAndTriggeringPolicy(timeBasedTriggeringPolicy);
        timeBasedRollingPolicy.setMaxHistory(20);
        timeBasedTriggeringPolicy.setTimeBasedRollingPolicy(timeBasedRollingPolicy);
        
        RollingFileAppender<ILoggingEvent> fileAppender = new RollingFileAppender<ILoggingEvent>();
        fileAppender.setFile(file.getAbsolutePath());
        fileAppender.setEncoder(ple);
        fileAppender.setContext(lc);
        fileAppender.setRollingPolicy(timeBasedRollingPolicy);
        fileAppender.setTriggeringPolicy(timeBasedTriggeringPolicy);
        
        timeBasedRollingPolicy.setParent(fileAppender);
        timeBasedRollingPolicy.start();
        fileAppender.start();
        return fileAppender;
	}
	
	public static Logger createLoggerFor(String string){
		ch.qos.logback.classic.Logger logger = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(string);
        logger.addAppender(luciLogFileAppender);
        logger.setLevel(DEBUG ? Level.TRACE : Level.INFO);
        logger.setAdditive(false); /* set to true if root should log too */
        return logger;
	}
	
	public static Logger createLoggerFor(String string, File file) {
        ch.qos.logback.classic.Logger logger = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(string);
        logger.addAppender(createFileAppender(file));
        logger.setLevel(DEBUG ? Level.TRACE : Level.INFO);
        logger.setAdditive(false); /* set to true if root should log too */

        return logger;
	}
	
	
	public static int getSRID(){
		return SRID;
	}
	
	private static void loadConfig(){
		Properties properties = new Properties();
		FileInputStream inputStream;
		try {
			inputStream = new FileInputStream(configFile.getAbsoluteFile());
			properties.load(inputStream);
			inputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
			// set defaults for some property values

			// the portNumber of the Middleware
			properties.setProperty("portNo", "7654");

			// the default credentials of the database
			properties.setProperty("dbHost", "localhost");
			properties.setProperty("dbPort", "5432");
			properties.setProperty("dbUser", "luci");
			properties.setProperty("dbPass", "1234");
			properties.setProperty("dbName", "luci");
			
			// mqtt
			properties.setProperty("mqtt_pre", "/Luci/");
			properties.setProperty("mqtt_broker", "EXTERNAL");
			properties.setProperty("mqtt_broker_address", "tcp://localhost:1883");
			properties.setProperty("mqtt_max_timeout", "5");
			
			// coordinate system for the geometry table (EPSG:4326 being WGS84 = lat/lon)
			properties.setProperty("srid", "NONE");
			
			// data access
			properties.setProperty("dbType", "PostGIS");

			// save properties to config file
			FileOutputStream outputStream;
			try {
				configFile.createNewFile();
				outputStream = new FileOutputStream(configFile);
				properties.store(outputStream, "Automatically generated Luci conf file; srid: EPSG:4326 = WGS84 = lat/lon");
			} catch (FileNotFoundException e1) {
				logger.error("Could not write properties file to '"
						+ configFile.getAbsolutePath() + "'");
				logger.debug(e.toString());
				
			} catch (IOException e1) {
				logger.error("Could not create file '" + configFile.getAbsolutePath() + "'");
				logger.debug(e.toString());
				
			}
		}
		String srid = properties.getProperty("srid");
		try {
			if (!srid.equals("NONE")) SRID = Integer.parseInt(srid);
		} catch (Exception e){
			System.err.println("Could not parse SRID from the config file. Must be an integer denoting the EPSG code.");
			// SRID remains 0
		}
		config = properties;
	}
	
	public static String resolveUserHome(String in){
		return in.replaceAll("\\{user\\.home\\}", System.getProperty("user.home"));
	}
	
	public static String getMosquittoInfo(File versionOutputFile) throws IOException{
		String info;
		InputStream fis = new FileInputStream(versionOutputFile);
	    InputStreamReader isr = new InputStreamReader(fis, Charset.forName("UTF-8"));
	    BufferedReader br = new BufferedReader(isr);
	    String line = br.readLine();
	    if (line != null) info = "Mosquitto: "+line;
	    else info = "Mosquitto: no information available!";
		br.close();
		return info;
	}
	
	public static int newClientId(){
		return ++clientID;
		
	}
	
	
	/** LUCI OBJECT */
	
	private Image logoNeutral;
	private Image logoConnected;
	private TrayIcon icon;
	private Process mosquitto;
	private Listener listener;
	private FactoryObserver observer;
	private Icon alertLogo;
	private Icon luciLogo;
	private String info = "";
	private int trayIconWidth = 0;
	private String osname = "";
	private MqttClient mqtt;
	private MqttAsyncClient asyncMqtt;
//	private Jetty jetty;
	protected boolean isMqttConnected;
	private ClientHandlerObserver clientHandlerObserver;
	private BrokerService broker;
	private Cache<Integer, ClientProxy> clientProxies;
	final public boolean headless;
	
	private MenuItem specification;
	private MenuItem resetTables;
	
	public Luci() {
		headless = (System.getProperty("headless", "false").equals("true"))  ;
		
		if (headless){
			System.out.println("running headless");
			return;
		} else {
			LuciFrame = new JFrame();
		}
		// check SystemTray
		if (!SystemTray.isSupported()) {
			JOptionPane.showMessageDialog(null, "System tray not supported!");
            return;
        } else {
        	this.logoNeutral = getIcon("logo/logo128x128_g.png");
    		this.alertLogo = new ImageIcon(getIcon("logo/alert64x64.png"));
    			osname = System.getProperty("os.name").toLowerCase();
    		if (osname.contains("unix")){
    			this.logoConnected = getIcon("logo/logo128x128_w.png");
    		} else {
    			this.logoConnected = getIcon("logo/logo128x128_b.png");
    		}
        	if (!osname.contains("mac")){
        		trayIconWidth = new TrayIcon(this.logoNeutral).getSize().width;
        		this.icon = new TrayIcon(this.logoNeutral.getScaledInstance(trayIconWidth, -1, Image.SCALE_SMOOTH));
        	} else {
        		this.icon = new TrayIcon(this.logoNeutral);
        		this.icon.setImageAutoSize(true);
        	}
        }
//		jetty = new Jetty(8080, webroot);
		specification = new MenuItem("Open Specification");
		specification.setEnabled(false);
		resetTables = new MenuItem("Reset Tables (except user table)");
		resetTables.setEnabled(false);
	}
	
	public TrayIcon getIcon(){
		return this.icon;
	}
	
	public String getInfo(){
		String i;
		if (this.observer != null){
			i = this.observer.getInfo()+"\r\n";
		} else {
			i = "Luci "+Luci.getVersion()+"\r\n\r\nNot running.\r\n";
		}
		
		if (this.listener != null){
			i += this.listener.getInfo()+"\r\n";
		} else if (i.length() == 0){
			i += "Not running\r\n";
		}
		
		i += this.info+"\r\n";
		
		return i;
	}
	
	public ClientProxy getClientProxy(int ID){
		ClientProxy cp = clientProxies.getIfPresent(ID);
		if (cp == null){
			cp = lu.getClientHandlerObserver().getClientProxy(ID);
		}
		return cp;
	}
	
	public FactoryObserver getObserver(){
		return observer;
	}
	
	public ClientHandlerObserver getClientHandlerObserver(){
		return clientHandlerObserver;
	}
	
	public void logAlert(String s) {
		if(headless)
			System.err.println(s);
		else
			JOptionPane.showMessageDialog(null, s, "Error Message", LuciFrame.NORMAL, alertLogo);
		
	}
	
	public void logMessage(String s) {
		if(headless)
			System.err.println(s);
		else
			JOptionPane.showMessageDialog(null, s, "Luci Info", LuciFrame.NORMAL, luciLogo);	
	}
	
	public void start(){
		// start Luci
		info("****************", true);
		info("** LUCI START **", true);
		info("****************", true);
		
		loadConfig();
		Luci.clientID = 0;
		if (this.observer == null){
			clientProxies = CacheBuilder.newBuilder()
				.maximumSize(10000)
				.expireAfterAccess(WSockTimeoutInMillis, TimeUnit.MILLISECONDS)
				.build();
			
			
			// make sure the mqtt broker is started up first
			String who_brokers = config.getProperty("mqtt_broker", "INTERNAL");
			
			// mosquitto broker deprecated:
			
//			// if we need to start up mosquitto:
//			if (who_brokers.equals("INTERNAL")){
//				File wd = mosquittoBin.getParentFile();
//				ProcessBuilder versionOutput;
//				ProcessBuilder mosquitto;
//				if (OSTYPE == MAC_OS_X || OSTYPE == UNIX) {
//					versionOutput = new ProcessBuilder("./"+mosquittoBin.getName(), "-h");
//					versionOutput.directory(wd);
//					mosquitto = new ProcessBuilder("./"+mosquittoBin.getName());
//					mosquitto.directory(wd);
//				} else {
//					versionOutput = new ProcessBuilder(mosquittoBin.getAbsolutePath(), "-h");
//					mosquitto = new ProcessBuilder(mosquittoBin.getAbsolutePath());
//				}
//				try {
//					File versionOutputFile = new File(wd, "versionOutput.txt");
//					versionOutput.redirectErrorStream(true);
//					versionOutput.redirectOutput(versionOutputFile);
//					Process vo = versionOutput.start();
//					vo.waitFor();
//					mosquitto.redirectErrorStream(true);
//					mosquitto.redirectOutput(new File(logDir, "mosquitto.log"));
//					this.mosquitto = mosquitto.start();
//					info(getMosquittoInfo(versionOutputFile));
//					Runtime.getRuntime().addShutdownHook(new Thread(new BrokerShutdown(this.mosquitto),"Mosquitto Killer"));
//					
//					
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			} else 
			if (!who_brokers.equals("EXTERNAL")){
				broker = new BrokerService();
				try {
					System.setProperty("activemq.conf", "config/activemq");
					System.setProperty("activemq.data", "data");
					//System.setProperty("activemq.home", "/Users/treyerl/Downloads/apache-activemq-5.11.1");
					System.setProperty("luci.webroot", webroot.getAbsolutePath());
					System.setProperty("luci.webport", webport);
					
					try {
						broker = BrokerFactory.createBroker("xbean:config/activemq.xml");
						broker.start();
					} catch (Exception e1) {
						e1.printStackTrace();
					}
				} catch (Exception e) {
					logAlert("ActiveMQ/MQTT Broker did not start: "+e.toString());
					e.printStackTrace();
				}
			}
			
			String broker_address = config.getProperty("mqtt_broker_address", "tcp://localhost:1883");
			if (broker_address.contains("localhost")) MQTT_CONNECTION_TIMEOUT = 1;
			else MQTT_CONNECTION_TIMEOUT = 5;
			
			if (!createMqttAndConnect(4000) || !createAsyncMqttAndConnect(4000)) {
				logAlert("MQTT Broker not reacheable. Luci will not start.");
				return;
			}
			
			int no = new Integer(config.getProperty("portNo"));
			try {
				this.observer = new FactoryObserver(config, mqtt, asyncMqtt);
			} catch (JSONException | NoSuchMethodException
					| SecurityException | IllegalAccessException
					| IllegalArgumentException | InvocationTargetException
					| ClassNotFoundException | InstantiationException
					| IOException e){
				logAlert(e.getMessage());
				e.printStackTrace();
				return;
			} catch (SQLException eSQL){
				logAlert("Database: "+eSQL.getMessage());
				logger.error(eSQL.getMessage());
				eSQL.printStackTrace();
				try {
					publishStopMqtt();
				} catch (Exception e) {
					e.printStackTrace();
				}
				mosquitto.destroy();
				return;
			} catch (RuntimeException noDatabasePlugins){
				logAlert("Runtime Error: "+noDatabasePlugins.getMessage()+ "\n Check your plugins, please!");
				noDatabasePlugins.printStackTrace();
				try {
					publishStopMqtt();
				} catch (Exception e) {
					e.printStackTrace();
				}
				mosquitto.destroy();
				return;
			}
			List<String> errors = observer.getErrors();
			if (errors.size() > 0){
				logAlert(errorListToString(errors));
			}
			clientHandlerObserver = new ClientHandlerObserver();
			this.listener = new Listener(no, observer, clientHandlerObserver);
			Thread l = new Thread(this.listener);
			l.start();
			
			if(headless)
				return;
			// kick off webserver
//			try {
//				jetty.start();
//				info("webserver url: "+jetty.getServerURI() + ", webroot: "+webroot);
//			} catch (Exception e1) {
//				JOptionPane.showMessageDialog(null, e1.toString(), "Error Message", NORMAL, alertLogo);
//			}
			
			specification.setEnabled(true);
			resetTables.setEnabled(true);
			
			if (!osname.contains("mac"))
				this.icon.setImage(logoConnected.getScaledInstance(trayIconWidth, -1, Image.SCALE_SMOOTH));
			else
				this.icon.setImage(logoConnected);
			
			// /Luci/admin startup
			String mqtt_pre = Luci.getConfig().getProperty("mqtt_pre", "/Luci/");
			try {
				mqtt.publish(mqtt_pre + "admin", new MqttMessage("startup".getBytes("UTF8")));
			} catch (UnsupportedEncodingException | MqttException e) {
				logAlert("Publishing event to mqtt failed! "+e.getMessage());
				e.printStackTrace();
			}
		}
	}
	
	private String errorListToString(List<String> errors){
		StringBuilder msg = new StringBuilder();
		int i = 0;
		for (String error: errors){
			msg.append(error+"\r\n");
			if (++i > 15){
				msg.append("Only the first 15 error messages are displayed. For more messages consult the log file.\r\n");
				break;
			}
		}
		return msg.toString();
	}
	
	public void stop() throws Exception{
		specification.setEnabled(false);
		resetTables.setEnabled(false);
		
		if (observer != null){
			observer.shutDownServiceFactories();
			ExecutorService pool = observer.getThreadPool();
			List<Runnable> unfinished = pool.shutdownNow();
			System.out.println("is terminated: " + pool.isTerminated());
			System.out.println("unfinished threads in thread pool: "+unfinished.size());
			observer.getThreadPool().awaitTermination(MQTT_CONNECTION_TIMEOUT, TimeUnit.SECONDS);
			observer.closeDBConnectionPool();
		}
		
		if (broker != null){
			broker.stop();
			broker = null;
		}
		
		clientProxies = null;
		
		if (clientHandlerObserver != null){
			clientHandlerObserver.stop();
			clientHandlerObserver.getThreadPool().awaitTermination(SOCKET_TIMEOUT, TimeUnit.SECONDS);
		}
		
		// /Luci/admin shutdown
		publishStopMqtt();
		CountDownLatch cdl = new CountDownLatch(1);
		mqttDisconnectAndRelease(cdl);
		cdl.await();
		if (mosquitto != null) {
			//mosquitto.destroy();
//			mosquitto.destroyForcibly(); // Java 1.8
			mosquitto.destroy();
			mosquitto = null;
		}
			
		if (listener != null) {
			listener.stop();
			listener = null;
		}
		
//		jetty.stop();
		
		observer = null;
		clientHandlerObserver = null;
		info = "";
		if (!osname.contains("mac"))
			this.icon.setImage(logoNeutral.getScaledInstance(trayIconWidth, -1, Image.SCALE_SMOOTH));
		else 
			this.icon.setImage(logoNeutral);
		System.out.println("Luci shut down");
	}

	public File generateSpec() throws Exception{
		File specPDF_file = new File(Luci.getWorkingDir() + File.separator + "spec" + File.separator +Luci.getVersion()+".pdf");
		if (specPDF_file.exists()){
			specPDF_file.delete();
		}
		if (this.observer == null)
			throw new Exception("Luci must be running for generating a spec.");
		SpecPDF pdf = new SpecPDF(this.observer);
		pdf.save(specPDF_file);
		pdf.close();
		return specPDF_file;
	}
	
	public void edit(File f) throws IOException{
		try {
			Desktop.getDesktop().open(f);
		} catch (Exception ex){
			Runtime run = Runtime.getRuntime();
			System.out.println(f.getAbsolutePath());
		    switch(OSTYPE){
		    case MAC_OS_X: run.exec("open -a TextEdit " + f.getAbsolutePath()); break;
		    case WINDOWS: run.exec("rundll32 url.dll, FileProtocolHandler " + f.getAbsolutePath()); break;
		    case UNIX: run.exec("xterm -e nano " + f.getAbsolutePath()); break;
		    default: 
		    }
		}
	}
	
	public void openSpec() {
		try {
			File specFile = generateSpec();
			edit(specFile);
		} catch (Exception e) {
			logAlert(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void printSpec(){
		try {
			File specFile = this.generateSpec();
	    	PrinterJob job = PrinterJob.getPrinterJob();
	    	PageFormat pf = job.defaultPage();
	    	pf.setOrientation(PageFormat.LANDSCAPE);
	    	PDDocument spec = PDDocument.load(specFile);
			job.setPageable(new PDPageable(spec, job));
			job.setJobName("Luci Specification");
			if (job.printDialog()){
				job.print();
			}
			spec.close();
		} catch (Exception e) {
			logAlert(e.getMessage());
			e.printStackTrace();
		}
	}
	
//	private void info(String s){
//		info(s, false);
//	}
	
	private void info(String s, boolean logonly){
		logger.info(s);
		if (!logonly){
			this.info += s;
			System.out.println(s);
		}
	}
	
	
	private void load(){
		final SystemTray tray = SystemTray.getSystemTray();
		final PopupMenu popup = new PopupMenu();
		final Luci lu = this;
		this.luciLogo = new ImageIcon(getIcon("logo/logo64x64.png"));
		final Icon alertLogo = this.alertLogo;
				
        MenuItem info = new MenuItem("Luci Info");
        MenuItem start = new MenuItem("Start Luci");
        MenuItem stop = new MenuItem("Stop Luci");
        MenuItem print = new MenuItem("Print Specification");
        MenuItem editConfig = new MenuItem("Edit Config File");
        MenuItem openPlugins= new MenuItem("Open Plugins Folder");
        MenuItem openLog= new MenuItem("Open Log File");
        MenuItem exit = new MenuItem("Exit");
        MenuItem clientConsole = new MenuItem("Interactive Console");
        MenuItem testConsole = new MenuItem("Test Run");
        MenuItem openExamples = new MenuItem("Open Examples");
        MenuItem web = new MenuItem("Web Interface");
        
        info.addActionListener(new ActionListener(){
        	@Override
			public void actionPerformed(ActionEvent e){
        		logMessage(lu.getInfo()); 
        	}
        });
        
        start.addActionListener(new ActionListener(){
        	@Override
			public void actionPerformed(ActionEvent e){lu.start();}
        });
	     
        
        stop.addActionListener(new ActionListener(){
        	@Override
			public void actionPerformed(ActionEvent e){
        		try {
					lu.stop();
				} catch (Exception e1) {
					logAlert(e1.getMessage());
					e1.printStackTrace();
				}
        	}
        });
        
        print.addActionListener(new ActionListener(){
        	@Override
			public void actionPerformed(ActionEvent e){lu.printSpec();}
       	});
        
        
        specification.addActionListener(new ActionListener(){
        	@Override
			public void actionPerformed(ActionEvent e){lu.openSpec();}
        });
        
        editConfig.addActionListener(new ActionListener(){
        	@Override
			public void actionPerformed(ActionEvent e){
        		try {
        			File config = new File(Luci.getWorkingDir() + File.separator +"config"+ File.separator + LUCICONF);
        			if (!config.exists()) config.createNewFile();
        			edit(config);
				} catch (IOException e1) {
					logAlert(e1.getMessage());
					e1.printStackTrace();
				}
        	}
        });
        
        openPlugins.addActionListener(new ActionListener(){
        	@Override
			public void actionPerformed(ActionEvent e){
        		try {
					Desktop.getDesktop().open(new File(Luci.getWorkingDir() + File.separator + "plugins"));
				} catch (IOException e1) {
					logAlert(e1.getMessage());
					e1.printStackTrace();
				}
        	}
        });
        
        openLog.addActionListener(new ActionListener(){ 
        	@Override
			public void actionPerformed(ActionEvent e){
        		try {
					Desktop.getDesktop().open(new File(Luci.getLogDir() + File.separator + LUCILOG));
				} catch (IOException e1) {
					logAlert(e1.getMessage());
					e1.printStackTrace();
				}
        	}
        });
        
        exit.addActionListener(new ActionListener(){ 
        	@Override
			public void actionPerformed(ActionEvent e){
                if (tray != null) {                    
                    tray.remove(icon);
                    System.exit(0);
                }
            }
        });
        
        clientConsole.addActionListener(new ActionListener(){
        	@Override
			public void actionPerformed(ActionEvent e){runConsole(false);}
        });
        
        testConsole.addActionListener(new ActionListener(){
        	@Override
			public void actionPerformed(ActionEvent e){runConsole(true);}
        });
        
        openExamples.addActionListener(new ActionListener(){
        	@Override
			public void actionPerformed(ActionEvent e){
        		try {
					Desktop.getDesktop().open(new File(Luci.getWorkingDir() + File.separator + "spec" + File.separator + "json_snippets.txt"));
				} catch (IOException e1) {
					logAlert(e1.getMessage());
					e1.printStackTrace();
				}
        	}
        });
        
        web.addActionListener(new ActionListener(){
        	@Override
			public void actionPerformed(ActionEvent e){
		        try {
//					Desktop.getDesktop().browse(jetty.getServerURI());
		        	Desktop.getDesktop().browse(new URI("http://localhost:"+webport));
		        } catch (NullPointerException n){
		        	logAlert("Webserver not running. Start Luci first!");
				} catch (Exception e1) {
					logAlert(e1.getMessage());
					e1.printStackTrace();
				}
        	}
        });
        
        resetTables.addActionListener(new ActionListener(){
        	@Override
			public void actionPerformed(ActionEvent e){
	        	if (observer != null){
	        		try {
						String msg = observer.resetTables();
						logMessage(msg);
					} catch (Exception e1) {
						logAlert(e1.toString());
						e1.printStackTrace();
					}
	        	} else logAlert("Luci not running. Start it first.");
        	}
        });
        
        
        popup.add(info);
        popup.addSeparator();
        popup.add(start);
        popup.add(stop);
        popup.addSeparator();
        popup.add(editConfig);
        popup.add(openPlugins);
        popup.add(openLog);
        popup.add(resetTables);
        popup.addSeparator();
        popup.add(specification);
        popup.add(openExamples);
        popup.addSeparator();
        popup.add(web);
        popup.add(clientConsole);
        popup.add(testConsole);
        popup.addSeparator();
        popup.add(exit);
        
        icon.setPopupMenu(popup);
        
        if(!headless) try {
            tray.add(icon);
            LuciFrame.setVisible(false);
        } catch (AWTException e) {
            System.out.println("TrayIcon could not be added.");
            return;
        }
	}
	
	public void runConsole(boolean test){
		try{
			run(test);
		} catch (IllegalStateException e){
			logAlert("Cannot run console. "
					+ "Export Luci project to a runnable jar \"luci-project-folder\\native\\win\\luci.jar\"\r\n"
					+ "(preferably with the required libs in a subfolder next to the generated jar file).");
		}
	}
	
	@SuppressWarnings("unused")
	private void run(boolean test){
		Runtime run = Runtime.getRuntime();
		String command = "";
		List<String> unixCommand = new ArrayList<String>();
		byte[] bash = new byte[0];
		File bashFile = null;
		String className = test ? "org.luci.console.TestConsole": "org.luci.console.ClientConsole";
		if (jar != null){
			switch (OSTYPE){
    		case MAC_OS_X:
    			String libs = jar.getParentFile().getAbsolutePath()+"Luci_lib";
    			String classPath = workingDir+"/"+libs+"/org.json.jar:"+workingDir+"/"+libs+"/jline-2.13-SNAPSHOT.jar";
    			String javaCommand = "java  -classpath '"+jar.getAbsolutePath()+":"+classPath+"' "+className;
    			bash = ("#!/bin/bash\nDIR=$( cd \"$( dirname \"${BASH_SOURCE[0]}\" )\" && pwd )\ncd $DIR\n"
    					+ javaCommand).getBytes();
    			bashFile = new File(jar.getParentFile(), "console.sh");
    			command = "open -a Terminal "+jar.getParentFile().getAbsolutePath()+"/console.sh";
    			break;
    		case WINDOWS:
    			command = "cmd.exe /c start cmd.exe /k \"" + new File(jar.getParentFile(), test ? "testconsole.exe" : "console.exe").getAbsolutePath() + "\"";
    			break;
    		case UNIX:
    			String unixLibs = "luci_lib/*";
    			unixCommand = new ArrayList<String>(Arrays.asList("java", "-classpath", jar.getAbsolutePath()+":luci_lib/*",  className));
    			break;
    		}
		} else {
			switch(OSTYPE){
			case MAC_OS_X: 
				bash = ("#!/bin/bash\nDIR=$( cd \"$( dirname \"${BASH_SOURCE[0]}\" )\" && pwd )\ncd $DIR\n"
						+ "java -classpath 'bin/:lib/org.json.jar:lib/jline-2.13-SNAPSHOT.jar' "+className).getBytes();
				bashFile = new File(workingDir, "console.sh");
				command = "open -a Terminal "+workingDir+"/console.sh";
				break;
			case WINDOWS:
				File console = new File(workingDir+"\\native\\win\\luci.jar");
				if (!console.exists()) {
					throw new IllegalStateException();
				}
				command = "cmd.exe /c start cmd.exe /k \"" + workingDir+"\\native\\win\\" + (test ? "testconsole.exe" : "console.exe" )+ "\"";
    			break;
    		case UNIX:
    			unixCommand = new ArrayList<String>(Arrays.asList("java", "-classpath", "bin/:lib/*",  className));
    			System.out.println(command);
    			break;
			}
		}
		
		switch(OSTYPE){
		case MAC_OS_X: 
			try {
    			bashFile.delete();
    			bashFile.createNewFile();
    			bashFile.setExecutable(true);
    			FileOutputStream fos = new FileOutputStream(bashFile);
				fos.write(bash, 0, bash.length);
				fos.close();
//				bashFile.deleteOnExit();
				run.exec(command);
				Thread.sleep(500);
			} catch (IOException e1) {
				e1.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			break;
		case WINDOWS:
			try {
				run.exec(command);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			break;
		case UNIX:
			String javaCall = unixCommand.toString().replace(",", "").replace("[", "").replace("]", "");
			String konsoleCommand = "konsole -e '"+javaCall+"'";
			String gnomeCommand = "gnome-terminal -e '"+javaCall+"'";
			try {
				run.exec(new String[]{"gnome-terminal", "-e", javaCall});
			} catch (Exception e1) {
				try {
					unixCommand.add(0, "-e");
					unixCommand.add(0, "konsole");
    				run.exec(unixCommand.toArray(new String[unixCommand.size()]));
				} catch (Exception e2) {
					unixCommand.set(0, "xterm");
					try {
						run.exec(unixCommand.toArray(new String[unixCommand.size()]));
					} catch (Exception e3) {
						e3.printStackTrace();
					}
				}
			}
			break;
		}
	}
	
	private Image getIcon(String name) {
        URL _url = this.getClass().getResource(name);
        return new ImageIcon(_url).getImage();
    }
	
	private void publishStopMqtt() throws MqttException, UnsupportedEncodingException{
		String mqtt_pre = Luci.getConfig().getProperty("mqtt_pre", "/Luci/");
		if (mqtt != null && mqtt.isConnected()) {
			mqtt.publish(mqtt_pre + "admin", new MqttMessage("shutdown".getBytes("UTF8")));
		}	
	}
	
	protected boolean mqttDisconnectAndRelease(CountDownLatch cdl){
		isMqttConnected = false;
		try {
			mqtt.disconnect(MQTT_CONNECTION_TIMEOUT);
			asyncMqtt.disconnect(MQTT_CONNECTION_TIMEOUT, null, new CountsOnMqttActionSuccess(cdl));
			mqtt = null;
			asyncMqtt = null;
		} catch (MqttException e) {
			e.printStackTrace();
			return false;
		}
		return true;		
	}
	
	private boolean createMqttAndConnect(long period) {
		try {
			MemoryPersistence persistence = new MemoryPersistence();
			String serverURI = Luci.getConfig().getProperty("mqtt_broker_address", "tcp://localhost:1883");
			mqtt = new MqttClient(serverURI, "LuciMain", persistence);
		} catch (MqttException e) {
			e.printStackTrace();
		}
		
		MqttConnectOptions o = new MqttConnectOptions();
		o.setConnectionTimeout(MQTT_CONNECTION_TIMEOUT);
		o.setCleanSession(true);
		long time = System.currentTimeMillis();
		long until = time + period;
		while((time = System.currentTimeMillis()) < until){
			try {
				mqtt.connect(o);
				isMqttConnected = true;
				return true;
			} catch (MqttException e){
				isMqttConnected = false;
				try {
					Thread.sleep(25);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
			}
		}
		return false;
	}
	
	private boolean createAsyncMqttAndConnect(long period){
		String serverURI = Luci.getConfig().getProperty("mqtt_broker_address", "tcp://localhost:1883");
		try {
			asyncMqtt = new MqttAsyncClient(serverURI, "LuciConnectionSupervisor", null);
			asyncMqtt.setCallback(new MqttConnectionSupervisor());
			MqttConnectOptions o = new MqttConnectOptions();
			o.setConnectionTimeout(MQTT_CONNECTION_TIMEOUT);
			asyncMqtt.connect(o);
		} catch (MqttException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	class MqttConnectionSupervisor implements MqttCallback{
		@Override
		public void connectionLost(Throwable cause) {
			logAlert("Connection to MQTT Broker lost.");
			cause.printStackTrace();
		}

		@Override
		public void deliveryComplete(IMqttDeliveryToken token) {}

		@Override
		public void messageArrived(String topic, MqttMessage msg) throws Exception {}
		
	}

}



