package org.luci;

import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.luci.connect.LcMetaJSONObject;
import org.luci.connect.LcString;
import org.luci.factory.ActionFactory;
import org.luci.factory.observer.FactoryObserver;

public class SpecPDF extends PDDocument{
	protected class Word{
		protected Color c;
		protected int length;
		protected Word(Color c, int len){
			this.c = c;
			length = len;
		}
		
		protected int getLen(){
			return length;
		}
		
		protected Color getColor(){
			return c;
		}
	}
	protected class Type extends Word{
		protected Type(int len) {
			super(blue, len);
		}
	}
	protected class Option extends Word{
		protected Option(int len) {
			super(pink, len);
		}
	}
	protected class Modifier extends Word{
		protected Modifier(int len) {
			super(grey, len);
		}
	}
	protected class PlaceHolder extends Word{
		protected PlaceHolder(int len) {
			super(green, len);
		}
	}
	private FactoryObserver obiwan;
	private PDRectangle a4 = PDPage.PAGE_SIZE_A4; // 595x842 pts
	private PDFont font = PDType1Font.COURIER;
	private PDFont bold = PDType1Font.COURIER_BOLD;
	private PDPageContentStream c;
	private int current_x;
	private int current_y;
	
	private int leftBorder = 28; //40 = 2 columns, 28 = 3 columns
	private int linewidth = 80; //120 = 2 columns, 80 = 3 columns
	private int columnWidth = 242; //361 = 2 columns, 242 = 3 columns
	private int columns = 2;
	private int columnHeight = 545;
	private final int fontSize = 5;
	private final int lineheight = 7;
	private final int lowerBorder = 25;
	private int topStart = columnHeight +lowerBorder;
	private final Color pink = new Color(230, 0, 180);
	private final Color blue = new Color(10, 0, 240);
	private final Color grey = new Color(150, 150, 150);
	private final Color black = new Color(0,0,0);
	private final Color green = new Color(50,140,50);
	private final Color darkblue = new Color(50,10,90);
	
	private List<String> modifiers = new ArrayList<String>();
	private List<int[]> underlines = new ArrayList<int[]>();
	
	public SpecPDF(FactoryObserver obiwan) throws IOException, COSVisitorException, URISyntaxException{
		super();
		
		//     A4 LANDSCAPE IN POINTS:
		//
		//     <-------------- 842 --------------->
		//     ____________________________________
		//     |<40>|<- 361 ->|<40>|<- 361 ->|<40>|  25  ^
		//     |    |         |    |         |    |      |
		//     |    |  ^      |    |         |    |      |
		//     |    |  |      |    |         |    |      |
		//     |    | 545     |    |         |    |      |
		//     |    |  |      |    |         |    |     595
		//	   |    |  |      |    |         |    |      |
		//	   |    |  v      |    |         |    |      |
		//     |____|_________|____|_________|____|      |
		//     |    |         |    |         |    |  25  v
		// ORIGIN
		//
		//     --> 361/120 = 3 
		//     --> linewidth: 120 characters (monospace)
		//     --> font size: 5
		//     --> lineheight: 7

		this.obiwan = obiwan;
		
		modifiers.add("OPT");
		modifiers.add("XOR");
		generate();
	}
	
	private void initPage() throws IOException{
		initPage(0);
	}
	
	private void initPage(int x) throws IOException{
		PDPage page = new PDPage(new PDRectangle(a4.getHeight(), a4.getWidth()));
		this.addPage( page );
		c = new PDPageContentStream(this, page);
		c.beginText();
		c.setFont( font, this.fontSize );
		
		c.moveTextPositionByAmount( leftBorder + x, topStart );
		this.current_x = leftBorder + x;
		this.current_y = topStart;
	}
	
	private void setColumnNumber(int c){
		if (c == 3){
			leftBorder = 27; //40 = 2 columns, 28 = 3 columns
			linewidth = 80; //120 = 2 columns, 80 = 3 columns
			columnWidth = 242; //361 = 2 columns, 242 = 3 columns
			columns = 3;
		} else if (c == 2){
			leftBorder = 40; //40 = 2 columns, 28 = 3 columns
			linewidth = 120; //120 = 2 columns, 80 = 3 columns
			columnWidth = 361; //361 = 2 columns, 242 = 3 columns
			columns = 2;
		} else if (c == 1){
			leftBorder = 40;
			linewidth = 254;
			columnWidth = 762;
			columns = 1;
		}
	}
	
	private void newLine() throws IOException{
		newLine(0);
	}
	
	private void newLine(int x) throws IOException{
		// if next line would be below the lower border
		if (current_y - lineheight < lowerBorder ){
//			int mostRightStart = 1*columnWidth + 2*leftBorder; // 2 columns
			int middleRightStart = 1*columnWidth + 2*leftBorder;
			int mostRightStart = columns == 3 ? 2*columnWidth + 3*leftBorder: middleRightStart;
			if (current_x >= mostRightStart ){
				c.endText();
				c.close();
				initPage(x);
			} else if (current_x < middleRightStart && columns == 3){
				c.moveTextPositionByAmount(-current_x + middleRightStart + x, -current_y + topStart);
				current_x = middleRightStart + x;
				current_y = topStart;
			} else {
				c.moveTextPositionByAmount(-current_x + mostRightStart + x, -current_y + topStart);
				current_x = mostRightStart + x;
				current_y = topStart;
			}
		} else {
			c.moveTextPositionByAmount(x, -lineheight);
			current_x = current_x + x;
			current_y = current_y - lineheight;
		}
	}
	
	private void generate() throws IOException, COSVisitorException, URISyntaxException{
		initPage();
		setColumnNumber(2);
		generateDocTitle();
		newLine();
		addText("spec/short_desc.txt", "");
		newLine();
		addText("spec/luci-json.txt", "");
		newLine();
		addSyntaxNotes();
		addTypeDefs("JSON Types");
//		addTimestamp();
//		pageBreak();
		
		addSubtitle(new String[]{"Action Index"});
		List<ActionFactory> af = addActionIndex();
		newLine();
		
		addTableSpec("spec/data_tables.html");
		
		
		c.endText();
		drawUnderlines();
		c.close();
		
		initPage();
		setColumnNumber(2);
		addSubtitle(new String[]{"Examples"});
		addText("spec/json_snippets.txt", LcString.fill(10, " "));
		newLine();
		c.endText();
		c.close();
		
		initPage();
		setColumnNumber(1);
		
//		af.add(2, af.remove(5)); // manual override to save space
		addSubtitle(new String[]{"Action Parameters"});
		setColumnNumber(3);
		columnHeight -= lineheight;
		topStart -= lineheight;
		addActionSpecs(af);
		c.moveTextPositionByAmount(0, (lowerBorder + 3*lineheight) - current_y);
		System.out.println((lowerBorder + 3*lineheight) - current_y);
		addTimestamp();
		c.endText();
		c.close();
	}
	
	@SuppressWarnings("unused")
	private void pageBreak() throws IOException{
		int linesLeft = (current_y - lowerBorder) / lineheight;
		for (int i = 0; i < linesLeft + 1; i++)
			newLine();
	}
	
	private void generateDocTitle() throws IOException{
		String servername = "Luci Server";
		if (Luci.getConfig().containsKey("serverName")){
			servername = Luci.getConfig().getProperty("serverName");
		} else {
			servername = InetAddress.getLocalHost().getHostName();
		}
		if (servername.length() > 60){
			addTitle(new String[]{"Luci Action Descriptions of host '", servername, "'"});
		} else {
			addTitle(new String[]{"Luci Action Descriptions of '"+servername+"'"});
		}
	}
	
	private void addSyntaxNotes() throws IOException {
		String notes0 = "SYNTAX NOTES for reading this document: ";
		String notes1 = "- WORDS in ALL CAPITAL LETTERS denote placeholders. ";
		String notes11 = "One exception: XOR = exclusive 'O R', OPT = optional";
		String notes2 = "  are MODIFIERS, which mark a key as optional or a group of keys of which only one key is allowed at a time.";
		c.drawString(notes0);
		newLine();
		c.setNonStrokingColor(green);
		c.drawString(notes1);
		c.setNonStrokingColor(0,0,0);
		colorize(notes11, "");
//		newLine();
//		c.setNonStrokingColor(grey);
		c.drawString(notes2);
		newLine();
		c.setNonStrokingColor(blue);
		c.drawString("- TYPES are in blue color; refer to the DATA TYPES section for available types");
		newLine();
		c.setNonStrokingColor(pink);
		c.drawString("- PREDEFINED OPTIONS are in pink color defining a set of valid strings");
		c.setNonStrokingColor(0,0,0);
		newLine();
		newLine();
	}
	
	private void addTitle(String[] titlelines) throws IOException{
		String line = "";
		
		for (int i = 0; i < linewidth; i++){
			line += "*";
		}
		
		c.drawString(line);
		newLine();
		addSubtitle(titlelines);
		c.drawString(line);
		newLine();
	}

	private void addSubtitle(String[] titlelines) throws IOException{
		for (String title: titlelines){
			String preline = "";
			String postline = "";
			for (int i = 0; i < Math.round((linewidth - title.length() - 1) / 2); i++) {
				preline += "*";
			}
			c.drawString(preline);
			int pl = (preline.length() + 1) * 3;
			c.setFont(bold, fontSize);
			c.moveTextPositionByAmount(pl, 0);
			c.drawString(title.toUpperCase());
			int tl = (title.length() + 1) *3;
			c.setFont(font, fontSize);
			c.moveTextPositionByAmount(tl, 0);			
			for (int i = preline.length() + title.length() + 2; i < linewidth; i++){
				postline += "*";
			}
			c.drawString(postline);
			c.moveTextPositionByAmount(-(pl + tl), 0);
			newLine();
		}
	}
	
	private void addHorizontalLine() throws IOException{
		String line = "";
		for (int i = 0; i < linewidth ; i++){
			line += "_";
		}
		c.drawString(line);
		newLine();
	}
	
	private void addHorizontalDottedLine(String prefix) throws IOException{
		StringBuilder sb = new StringBuilder();
		sb.append(prefix);
		String ch = "";
		switch(Luci.OSTYPE){
		case Luci.MAC_OS_X: ch = "¨"; break;
		case Luci.WINDOWS: ch = "�"; break;
		case Luci.UNIX: ch = "¨"; break;
		}
		for (int i = 0; i < linewidth - prefix.length() ; i++){
			sb.append(ch);
		}
		c.drawString(sb.toString());
		newLine();
	}
	
	private void addText(String loc, String indent) throws IOException, URISyntaxException{
		File specFile = new File(Luci.getWorkingDir() + File.separator + loc);
		String[] desc = new String[]{};
		desc = Files.readAllLines(Paths.get(specFile.toURI()), StandardCharsets.UTF_8).toArray(desc);
		for (String s: desc){
			s = s.replace("“", "\"").replace("‘", "'").replace("’", "'").replace("—", "-");
			if (s.length() > (linewidth - indent.length())) {
				int len = 0;
				StringBuilder sb = new StringBuilder();
				String[] words = s.split("\\s");
				for (String word: words){
					len += word.length() + 1;
					if (len < (linewidth - indent.length()))
						sb.append(word + " ");
					else {
						c.drawString(sb.toString());
						newLine();
						sb.delete(0, sb.length());
						sb.append(indent + word + " ");
						len = word.length() + 1;
					}
				}
				c.drawString(sb.toString());
				newLine();
			} else {
				c.drawString(s);
				newLine();
			}
		}
	}
	
	private void addTableSpec(String loc) throws IOException, URISyntaxException{
		File specFile = new File(Luci.getWorkingDir() + File.separator + loc);
		String[] lines = new String[]{};
		lines = Files.readAllLines(Paths.get(specFile.toURI()), StandardCharsets.UTF_8).toArray(lines);
		for (String line: lines){
			if (line.startsWith("<h1>")) {
				newLine();
				addSubtitle(new String[]{line.replace("<h1>", "").replace("</h1>", "")});
				continue;
			}
			if (line.startsWith("<h2>")) {
				newLine();
				addBoldTitle("", line.replace("<h2>", "").replace("</h2>", ""));
				continue;
			}
			line = line.trim().replace("</br>", "");
			if (line.length() > 0){
				StringBuilder sb = new StringBuilder();
				String[] halfs = line.split("\\(");
				String tablename = halfs[0];
				String[] columns = halfs[1].substring(0, halfs[1].trim().length()-1).split(",");
				sb.append(tablename+"(");
				int indent = (sb.length() + 1) * 3;
				int remaining = linewidth - sb.length();
				int len = 0;
				int i = 0;
				for (String column: columns){
					i += 1;
					column = column.trim();
					boolean underline = column.startsWith("<u>"); 
					column = column.replaceAll("\\<u\\>(.+)\\<\\/u\\>", "$1");
					int columnlen = column.length() + 2;
					if (underline) {
						int cx = current_x + indent;
						underlines.add(new int[]{cx+(len)*3, current_y-2, cx+(len+columnlen-2)*3, current_y-2});
					}
					len += columnlen;
					if (len < remaining){
						sb.append(" "+column+addComa(i,columns.length));
					} else {
						c.drawString(sb.toString());
						newLine();
						sb.delete(0, sb.length());
						len = columnlen;
						sb.append(LcString.fill(linewidth - remaining - 1, " "));
						sb.append(" "+column+addComa(i,columns.length));
					}
				}
				c.drawString(sb.toString()+" )");
				newLine();
			}
		}
	}
	
	private void drawUnderlines() throws IOException{
		c.setLineWidth(0.5F);
		for (int[] u: underlines){
			c.drawLine(u[0], u[1], u[2], u[3]);
		}
	}
	
	private void addBoldTitle(String prefix, String title) throws IOException {
		// BOLD title
		int prefixLength = 0;
		if (!prefix.equals("")) {
			c.drawString(prefix);
			prefixLength = prefix.length() * 3;
			c.moveTextPositionByAmount(prefixLength, 0);
		}
		c.setFont(bold, fontSize);
		c.drawString(title.toUpperCase() + " ");
		int rightShift = (title.length() + 1) * 3;
		c.moveTextPositionByAmount(rightShift, 0);
		
		// ::::::::::::::::
		String colons = "";
		for (int i = title.length() + prefix.length() + 1; i < linewidth; i++){
			colons += ":";
		}
		c.drawString(colons);
		c.setFont(font, fontSize);
		c.moveTextPositionByAmount(-(rightShift+prefixLength), 0);
		newLine();
	}
	
	private List<ActionFactory> addActionIndex() throws IOException{
		TreeMap<String, String> subjects = new TreeMap<String, String>();
		Set<String> actions = this.obiwan.getActionSet();
		List<ActionFactory> actionFactories= new ArrayList<ActionFactory>();
		int maxKeyLen = 0;
		for (String s: actions){
			int len = s.length();
			if (len > maxKeyLen) maxKeyLen = len;
			ActionFactory a = this.obiwan.getActionFactory(s);
			subjects.put(s, a.getIndexDescription());
			actionFactories.add(a);
		}
		maxKeyLen += 3;
		
		// HEADER
		c.drawString(pad("ACTION: ", maxKeyLen + 3));
		c.moveTextPositionByAmount((maxKeyLen + 3) * 3, 0);
		c.drawString("SUBJECT");
		c.moveTextPositionByAmount(-(maxKeyLen + 3) * 3, 0);
		newLine();
		
		Iterator<Entry<String, String>> it = subjects.entrySet().iterator();
		int i = 1;
		int j = 0;
		while(it.hasNext()){
			@SuppressWarnings("rawtypes")
			Map.Entry pair = it.next();
			c.setNonStrokingColor(black);
			if (subjects.size() > 9){
				c.drawString(pad(i++ + ":", 3));
				j = 9;
			} else {
				
				c.drawString(i++ + ":");
				j = 6;
			}
			c.moveTextPositionByAmount(j, 0);
			
			
			c.drawString(pad(pair.getKey() + ": ", maxKeyLen));
			c.moveTextPositionByAmount((maxKeyLen * 3), 0);
			c.setNonStrokingColor(grey);
			
			String value = (String) pair.getValue();
			int currentPos = j + (maxKeyLen * 3);
			int leftOver = columnWidth - currentPos;
			if ((value.length() * 3 )> leftOver){
				String[] v = value.split("\\s");
				int l = 0;
				String line = "";
				for (String word: v){
					if ((word.length() * 3) + l > leftOver){
						c.drawString(line);
	
						newLine();
						line = word + " ";
						l = (word.length() + 1) * 3;
					} else {
						line += word + " ";
						l += (word.length() + 1) * 3;
					}
				}
				c.drawString(line);

			} else {
				c.drawString(pair.getValue() +"");

			}
			c.moveTextPositionByAmount(-currentPos, 0);
			newLine();
			c.setNonStrokingColor(black);
		}
		return actionFactories;
	}
	
	private void addActionSpecs(List<ActionFactory> afl) throws IOException{
		for (ActionFactory af: afl){
			String[] iLines = index2String(af.getInputIndex(), 0, "").split("\\n");
			String[] oLines = index2String(af.getOutputIndex(), 0, "").split("\\n");
			int linesLeft = (current_y - lowerBorder) / lineheight;
			int printedElseWhere = 5;
			if (linesLeft < iLines.length + oLines.length + printedElseWhere)
				for (int i = 0; i < linesLeft + 1; i++)
					newLine();
			for (int j = 0; j < 2; j++){
				String[] jLines = j == 1 ? oLines : iLines;
				
				if (j == 0) addHorizontalLine();
				if (j == 1){
					//c.setNonStrokingColor(cyan);
					c.setFont(bold, fontSize);
					c.drawString("--> returns: {");
					c.setFont(font, fontSize);
					newLine();
					//c.setNonStrokingColor(0,0,0);
				} else
					addBoldTitle("<-- { ", "\"action\" : "+af.getName());
				
				for (int i = 0; i < jLines.length; i++){
					colorize(jLines[i], "    ");
//						newLine();
				}
				
				addHorizontalDottedLine("    } ");
			}
		}
	}
	
	private void colorize(String line, String indent) throws IOException{
		int lastColonIndex = line.lastIndexOf(":");

		SortedMap<Integer, Word> occurence = new TreeMap<Integer, Word>();
		
		// types
		for (int j = 0; j < LcMetaJSONObject.jsontypes.size(); j++){
			String type = LcMetaJSONObject.jsontypes.get(j);
			int index = 0;
			while((index = line.indexOf("\""+type+"\"", index+1)) > 0 ){
				occurence.put(index, new Type(type.length()+2)); 
			}
		}
		
		// options (= no types, but also in an array)
		if (occurence.size() == 0){
			int x1 = line.indexOf("[");
			int x2 = line.indexOf("]");
			if (x1 != -1 && x2 != -1){
				int index = x1;
				int j = 0;
				int lastIndex = 0;
				while((index = line.indexOf("\"", index+1)) > 0){
					String word = line.substring(lastIndex+1, index);
					if (++j%2 == 0 && !LcString.isAllUpperCase(word)) 
						occurence.put(lastIndex, new Option(index-lastIndex+1));
					lastIndex = index;
				}
			}
		}
		
		// modifiers
		for (String modifier: modifiers){
			int index = 0;
			while((index = line.indexOf(modifier, index+1)) > 0 ){
				occurence.put(index, new Modifier(3)); 
			}
		}
		
		//PLACEHOLDERS
		int index = 0;
		int j = 0;
		int lastIndex = 0;
		while((index = line.indexOf("\"", index+1)) > 0){
			if (++j%2 == 0) {
				String word = line.substring(lastIndex+1, index);
				if (LcString.isAllUpperCase(word)){
					if (word.startsWith("OPT") || word.startsWith("XOR")){
						occurence.put(lastIndex + 4, new PlaceHolder(index-lastIndex-4));
					} else occurence.put(lastIndex+1, new PlaceHolder(index-lastIndex-1));
				}
					
			}
			lastIndex = index;
		}
		int end = 0;
		int lineBreakAt = 0;
		c.drawString(indent);
		for (Entry<Integer, Word> occ: occurence.entrySet()){
			int offset = occ.getKey();
			Word w = occ.getValue();
			c.setNonStrokingColor(0, 0, 0);
			c.drawString(line.substring(end, offset));
			c.setNonStrokingColor(w.getColor());
			end = offset + w.getLen();
			if (end - lineBreakAt > linewidth - indent.length()) {
				lineBreakAt = end;
				newLine((indent.length()+lastColonIndex+3)*3);
			}
			c.drawString(line.substring(offset, end));
			
		}
		c.setNonStrokingColor(0, 0, 0);
		c.drawString(line.substring(end, line.length()));
		if (lineBreakAt > 0) newLine((-indent.length()-lastColonIndex-3)*3);
		else newLine();
	}
	
	private void addTypeDefs(String subtitle) throws IOException{
		
		String[][] typeDefs = new String[4][];
		typeDefs[0] = new String[]{"json types", "{'json':"+LcMetaJSONObject.jsontypes.toString()+"}"};
		typeDefs[1] = new String[]{"timerange", LcMetaJSONObject.timerange};
		typeDefs[2] = new String[]{"streaminfo", LcMetaJSONObject.streaminfo};
		typeDefs[3] = new String[]{"geometry", LcMetaJSONObject.geometry};
		
		addSubtitle(new String[]{"Data Types"});
		
		for (String[] def: typeDefs){
			addHorizontalLine();
			addBoldTitle("{ ", "\"type\" : "+def[0]);
//			System.out.println(def[0]);
			String[] jLines = this.index2String(new JSONObject(def[1]), 0, "").split("\\n");
			for (int i = 0; i < jLines.length; i++){
				colorize(jLines[i], "");
//				newLine();
			}
			addHorizontalDottedLine("} ");
		}
	}
	
	private String pad(String s, int len){
		if (s.length() < len){
			String white = "";
			for (int i = 0; i < len - s.length(); i++){
				white += " ";
			}
			s = white + s;
		}
		return s;
	}
	
	private void addTimestamp() throws IOException{
		Timestamp t = new Timestamp(new Date().getTime());
		
		addHorizontalLine();
		String time = "Created on "+t.toString() + " ";
		c.setNonStrokingColor(darkblue);
		c.drawString(time);
		
		int len = time.length();
		time = "";
		for (int i = len + 1; i < linewidth; i++){
			time += ":";
		}
		c.setNonStrokingColor(0,0,0);
		c.drawString(time);
		newLine();
		addHorizontalDottedLine("");
	}
	
	private String index2String(Object index, int recursion, String end){
		StringBuilder sb = new StringBuilder();
		String indent = LcString.fill((recursion+1) * 4, " ");
		if (recursion > 0) sb.append("{ \n");
		if (index == null) {
			@SuppressWarnings("unused")
			boolean t = true;
		}
		if (index instanceof JSONArray){
			handleJSONArray(recursion, sb, 0, 0, index, "");
		}
		if (index instanceof JSONObject){
			ArrayList<String> keys = new ArrayList<String>(Arrays.asList(JSONObject.getNames((JSONObject) index)));
			Collections.sort(keys, new KeySorter());
			int i = 0;
			int l = keys.size();
			for (String key: keys){
				i += 1;
				if (key.equalsIgnoreCase("action")) continue;
				sb.append(indent + "\""+key+"\" : ");
				Object value = ((JSONObject)index).get(key);
				if (value instanceof JSONObject){
					handleJSONObject(recursion, sb, i, l, key, value, "");
				} else if (value instanceof JSONArray){
					handleJSONArray(recursion, sb, i, l, value, key);
				}
				else sb.append("\""+value.toString()+"\""+addComa(i,l)+"\n");
				//sb.append("\n");
			}
		}
		if (recursion > 0) sb.append(indent+"}"+end+"\n");
		return sb.toString();
	}

	private void handleJSONArray(int recursion, StringBuilder sb, int i, int l, Object value, String key) throws JSONException {
		if (((JSONArray) value).length() > 0){
			Object first = ((JSONArray) value).get(0);
			if (((JSONArray) value).length() == 1 && first instanceof JSONObject ){
				sb.append("[");
				handleJSONObject(recursion, sb, 0, 0, key, first, "]");
			}
			else sb.append(value.toString()+""+addComa(i,l)+"\n"); //--> lists are printed to one line as well. : make sure they get abbreviated if they are longer as one line...
		}
	}

	private void handleJSONObject(int recursion, StringBuilder sb, int i, int l, String key, Object value, String end) {
		if (((JSONObject) value).length() > 0){
			key = key.replace("OPT ", "");
//			if (repeatingKeys.contains(key)) sb.append("{Ref #"+getRepeatIndex(key, (JSONObject) value)+"}"+addComa(i,l)+end+"\n");
			sb.append(index2String(value, recursion + 1, end));
		} else sb.append("{}"+addComa(i,l)+end+"\n");
	}
	
	private String addComa(int a, int b){
		if (a < b) return ",";
		else return "";
	}
	
//	private int getRepeatIndex(String keyname, JSONObject index){
//		int j = repeatingKeys.indexOf(sameRepeatingKeys.get(keyname));
//		int maxI = repeatingKeys.size() - sameRepeatingKeys.size() / 2 ;
//		int i = Math.min(repeatingKeys.indexOf(keyname), (j < 0) ? maxI - 1 : j);
//		if (i >= 0 && i < repeatingDefs.length){
//			JSONObject storedDisplay = repeatingDefs[i];
//			if (storedDisplay == null && index != null)
//				repeatingDefs[i] = index;
//		}
//		return i+1;
//	}
	
	private class KeySorter implements Comparator<String>{
		private int getIndex(String s){
			if (s.equalsIgnoreCase("action")) return 1;
			if (!s.startsWith("XOR ") && !s.startsWith("OPT ")) return 2;
			if (s.startsWith("XOR ")) return 3;
			if (s.startsWith("OPT ")) return 4;
			return 0;
		}
		@Override
		public int compare(String o1, String o2) {
			int i1 = getIndex(o1);
			int i2 = getIndex(o2);
			if (i1 < i2) return -1;
			else if (i1 > i2) return 1;
			else return 0;
		}
		
	}
}
