package org.luci;

/**
 * Very limited class for now. To be extended in future versions. Should
 * implement the authentication process.
 * 
 * @author Lukas Treyer <treyer@arch.ethz.ch>
 * 
 */
public class User {
	private long uid;
	private String username;
	private String email;
	
	public User(long uid, String username, String email){
		this.uid = uid;
		this.username = username;
		this.email = email;
	}
	
	public String getUsername(){
		return this.username;
	}
	
	public long getUid(){
		return this.uid;
	}
	public String getEmail(){
		return this.email;
	}

}
