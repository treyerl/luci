package org.luci.comm.action;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;
import org.json.JSONObject;
import org.luci.Luci;
import org.luci.comm.clienthandlers.ClientHandler;
import org.luci.connect.DirectStreamWriter;
import org.luci.connect.LcInputStream;
import org.luci.connect.LcJSONObject;
import org.luci.connect.LcOutputStream;
import org.luci.converters.IStreamReader;
import org.luci.factory.ITask;
import org.luci.factory.observer.FactoryObserver;
import org.slf4j.Logger;

public abstract class Action implements ITask {
	protected FactoryObserver o;
	protected Logger log;
	protected LcJSONObject request;
	protected ClientHandler h;
	protected LcInputStream lastInput;
	private int i = 0;
	
	public Action(FactoryObserver o, ClientHandler h, LcJSONObject json) {
		this.h = h;
		this.o = o;
		this.request = json;
		this.log = Luci.createLoggerFor(getClass().getSimpleName());
//		this.inputStreams = new ArrayList<LcInputStream>();
	}
	
	public abstract JSONObject perform(List<LcInputStream> inputStreams, 
			List<LcOutputStream> outputStreams, Set<String> unknownKeys) throws Exception;
	
	public boolean publish(Map<String, String> topics) throws MqttPersistenceException, MqttException{
		return this.publish(topics, Luci.qos_0_at_most_once, false);
	}
	
	public boolean publish(Map<String, String> topics, int qos, boolean retain) 
			throws MqttPersistenceException, MqttException {
		MqttClient con = o.getMqttClient();
		synchronized(con) {
			Iterator<Entry<String, String>> it = topics.entrySet().iterator();
		    while (it.hasNext()) {
		        Map.Entry<String, String> pair = it.next();
		        String topic = pair.getKey();
		        String message = pair.getValue();
		        con.publish(topic, message.getBytes(), qos, retain);
		    }
			return true;
		}
		
	}
	
	public void publish(String topic, String message) throws MqttPersistenceException, MqttException {
		publish(topic, message, Luci.qos_0_at_most_once, false);
	}
	
	public void publish(String topic, String message, int qos, boolean retain) 
			throws MqttPersistenceException, MqttException{
		MqttClient con = o.getMqttClient();
		synchronized(con){ 
			con.publish(topic, message.getBytes(), qos, retain);
		}	
	}
	
	/**
	 * @param inputStreams
	 * @throws FileNotFoundException 
	 * @throws ExecutionException 
	 * @throws InterruptedException 
	 */
	protected void readAllInputStreamsToFile(List<LcInputStream> inputStreams) 
			throws FileNotFoundException, InterruptedException, ExecutionException {
		for (LcInputStream lis: inputStreams){
			readInputStreamToFile(lis);
		}
	}
	
	/**
	 * @param lis LcInputStream
	 * @throws FileNotFoundException 
	 * @throws InterruptedException 
	 * @throws ExecutionException 
	 */
	protected void readInputStreamToFile(LcInputStream lis) 
			throws FileNotFoundException, InterruptedException, ExecutionException {
		ExecutorService pool = o.getThreadPool();
		boolean hasChecksum = lis.getChecksum() != null;
		String chck = hasChecksum ? lis.getChecksum() : "file"+h.getID()+i++;
		File f = new File(Luci.getDataRoot(), chck+"."+lis.format);
		FileOutputStream fos = new FileOutputStream(f);
		if (!hasChecksum) lis.waitUntilReady(20, TimeUnit.SECONDS); // if is url download wait for ready so that at least content length is set
		
		LcOutputStream los = new LcOutputStream(lis, fos);
		los.setReady();
//		this.inputStreams.add(lis);
		DirectStreamWriter dsw = new DirectStreamWriter(lis, los, false, true, true, hasChecksum);
		if (hasChecksum) pool.submit(dsw);
		else {
			pool.submit(dsw).get();
			f.renameTo(new File(Luci.getDataRoot(), lis.getChecksum()+"."+lis.format));
		}
	}
	
	protected void readInputStream(final IStreamReader reader, final LcInputStream inputStream){
		ExecutorService pool = o.getThreadPool();
//		inputStreams.add(inputStream);
		pool.submit(new Callable<Void>(){
			@Override
			public Void call() throws Exception {
				inputStream.waitUntilReady(60, TimeUnit.SECONDS);
				try{
					reader.readStream(inputStream);
				} catch(RuntimeException e){
					inputStream.setDone(e);
					throw e;
				}
				inputStream.setDone();
				return null;
			}	
		});
	}
	
	protected void waitForAllInputStreamsToBeRead() throws InterruptedException {
		if (lastInput != null) lastInput.waitUntilDone(100, TimeUnit.SECONDS);
	}
}
