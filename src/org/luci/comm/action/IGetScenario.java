package org.luci.comm.action;

import java.sql.SQLException;

public interface IGetScenario extends IServiceAction{
	public String getScenarioName() throws SQLException;
}
