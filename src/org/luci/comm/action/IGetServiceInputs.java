package org.luci.comm.action;

import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.luci.connect.LcInputStream;
import org.luci.connect.LcJSONObject;
import org.luci.connect.LcOutputStream;

/**methods that need to be available to Luci's service factories
 * @author treyerl
 *
 */
public interface IGetServiceInputs extends IServiceAction {
	public List<Long> getServiceInstanceList(String ServiceName, String status) throws SQLException;
	public List<Long> getServiceInstanceList(String ServiceName) throws SQLException;
	public Map<Long, Long> getServiceInstanceMap(String ServiceName) throws SQLException;
	public LcJSONObject getServiceInstanceInfo() throws SQLException;
	public LcJSONObject getServiceInputs(boolean getSubscription,  List<LcInputStream> inputStreams, 
			List<LcOutputStream> outputStreams) throws SQLException, FileNotFoundException, InstantiationException;
}
