package org.luci.comm.action;

import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;
import org.json.JSONArray;
import org.json.JSONException;
import org.luci.connect.LcInputStream;
import org.luci.services.ServiceCall;

public interface ISetServiceOutputs extends IServiceAction {
	public IServiceAction setInputHashcode(Timestamp inputHash);
	public void setServiceStatus(ServiceCall call, String status) throws SQLException;
	public JSONArray updateServiceOutputs(List<LcInputStream> binaryOutputs) throws SQLException, 
		FileNotFoundException, JSONException, InstantiationException, ExecutionException, 
		InterruptedException, MqttPersistenceException, MqttException;
}

