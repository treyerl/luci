package org.luci.comm.action;

import org.luci.comm.clienthandlers.ClientHandler;

public interface ISwitchClientHandler {
	public boolean shouldSwitch();
	public ClientHandler switchClientHandler();
}
