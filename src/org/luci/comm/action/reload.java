package org.luci.comm.action;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;
import org.luci.comm.clienthandlers.ClientHandler;
import org.luci.connect.LcInputStream;
import org.luci.connect.LcJSONObject;
import org.luci.connect.LcMetaJSONObject;
import org.luci.connect.LcOutputStream;
import org.luci.factory.observer.FactoryObserver;

public class reload extends Action {

	public reload(FactoryObserver o, ClientHandler h, LcJSONObject json) {
		super(o, h, json);
	}
	
	@Override
	public LcMetaJSONObject getInputIndex() {
		String inputjson = ""
				+ "{	'action':'"+this.getClass().getSimpleName()+"',"
				+ "		'OPT plugins':['actions', 'services', 'geometry converters', 'file converters', 'unit converters'],"
				+ "		'subject':'Converters AND Actions AND Services (in that order)'"
				+ "}";
		return new LcMetaJSONObject(inputjson);
	}
	
	@Override
	public JSONObject perform(List<LcInputStream> inputStreams, List<LcOutputStream> outputStreams, 
			Set<String> unknownKeys) throws Exception{
		JSONObject jsonout = new JSONObject();
		if (request.has("plugins")){
			String selector = "";
			JSONArray plugins = request.getJSONArray("plugins");
			List<String> buildList = new ArrayList<String>();
			for (int i = 0; i < plugins.length(); i++){
				buildList.add(plugins.getString(i));
			}
			if (buildList.contains("actions")) selector += "1";
			else selector += "0";
			if (buildList.contains("services")) selector += "1";
			else selector += "0";
			if (buildList.contains("geometry converters")) selector += "1";
			else selector += "0";
			if (buildList.contains("file converters")) selector += "1";
			else selector += "0";
			if (buildList.contains("unit converters")) selector += "1";
			else selector += "0";

			jsonout.put("result", o.build(Integer.parseInt(selector, 2)));
		} else {
			jsonout.put("result", o.build(Integer.parseInt("11111", 2)));
		}
		return jsonout;
	}

	@Override
	public LcMetaJSONObject getOutputIndex() {
		String outputjson = ""
				+ "	{'XOR result': {"
				+ "		'plugins':{"
				+ "			'file converters':{'FORMAT':'number'},"
				+ "			'services':'number',"
				+ "			'actions':'number',"
				+ "			'geometry converters':'number',"
				+ "			'unit converters':'number'"
				+ "		}"
				+ "	},"
				+ "	'XOR error': ['json','list','string']"
				+ "}";
		return new LcMetaJSONObject(outputjson);
	}


}
