package org.luci.comm.clienthandlers;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;
import org.luci.User;
import org.luci.connect.LcJSONObject;
import org.luci.factory.observer.FactoryObserver;

public class AuthenticatedClientHandler extends ClientHandler {
	protected boolean authenticated = true;
	
	public AuthenticatedClientHandler(ClientHandler predecessor, FactoryObserver factories, User u) {
		super(predecessor.getSocket(), factories, u, predecessor.getClientHandlerObserver(), predecessor.getID());
	}
	
	@Override
	protected void restrictActions()  {
		availableActions = new ArrayList<String>(o.getActionSet());
		List<String> allowed = new ArrayList<String>(availableActions);
		allowed.remove("authenticate");
		allowed.remove("update_outputs_of");
		allowed.remove("remote_cancel");
		allowed.remove("remote_unregister");
		allowedActions = allowed;
	}
	
	@Override
	protected String getAction(LcJSONObject joi) throws Exception{
		if (joi.has("action")) {
			String action = joi.getString("action");
			if (action.equals("exit")){
				print(new JSONObject().put("result", "closing connection!"));
				throw new BreakException();
			}if (!availableActions.contains(action)){
				print(new JSONObject().put("error","Action \\'" + action+ "\\' unknown!"));
			} else if (allowedActions.contains(action)) {
				return action;
			} else {
				print(new JSONObject().put("error","Action \\'" + action+  "\\' not allowed!'}"));
			}
		} else {
			print(new JSONObject().put("error","Message contains no status keys 'action', 'result', 'warning','error'"));
		}
		return null;
	}

	@Override
	protected void handleBreakException() {
		// nothing to do
		
	}
	
	@Override
	protected boolean confirmation(JSONObject json){
		return true;
	}

	@Override
	protected void handleIOException() {		
	}
	
	@Override
	public String getStatus(){
		return "authenticated";
	}
}