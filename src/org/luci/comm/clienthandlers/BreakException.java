package org.luci.comm.clienthandlers;

import org.luci.comm.action.ISwitchClientHandler;

@SuppressWarnings("serial")
public class BreakException extends Exception implements ISwitchClientHandler{
	private ISwitchClientHandler switcher;
	public BreakException(String s){
		super(s);
	}
	public BreakException(){
		super();
	}
	public BreakException(ISwitchClientHandler switcher){
		this.switcher = switcher;
	}
	/* (non-Javadoc)
	 * @see org.luci.comm.action.ISwitchClientHandler#shouldSwitch()
	 */
	@Override
	public boolean shouldSwitch() {
		if (switcher != null)
			return switcher.shouldSwitch();
		return false;
	}
	/* (non-Javadoc)
	 * @see org.luci.comm.action.ISwitchClientHandler#switchClientHandler()
	 */
	@Override
	public ClientHandler switchClientHandler() {
		if (switcher != null)
			return switcher.switchClientHandler();
		return null;
	}
}
