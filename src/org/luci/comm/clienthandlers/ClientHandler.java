package org.luci.comm.clienthandlers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.luci.Luci;
import org.luci.User;
import org.luci.comm.action.Action;
import org.luci.comm.action.ISwitchClientHandler;
import org.luci.connect.ILcSocket;
import org.luci.connect.LcInputStream;
import org.luci.connect.LcJSONObject;
import org.luci.connect.LcOutputStream;
import org.luci.factory.ActionFactory;
import org.luci.factory.observer.FactoryObserver;
import org.slf4j.Logger;

public abstract class ClientHandler{
	protected Logger log;
	protected final ILcSocket socket;
	protected FactoryObserver o;
	protected ClientHandlerObserver co;
	protected int id;
	protected User user = null;
	protected Object actionResponse;
	protected List<String> allowedActions;
	protected List<String> availableActions;
	protected boolean handover = false;
	protected List<LcInputStream> inputStreams;
	private boolean authenticated = false;

	/**
	 * @param s: Socket object
	 * @param o: FactoryObserver object  
	 * @param u: User object
	 * @param l: listener object (that holds a hashmap with all clienthandler threads that needs to be updated when changing the clienthandler type
	 * @param id: (int) counter assigned by Listener (also needed by Listener internally to interrupt the clienthandler threads)
	 */
	public ClientHandler(ILcSocket s, FactoryObserver o, User u, ClientHandlerObserver co, int id) {
		log = Luci.createLoggerFor(getClass().getSimpleName());
		socket = s;
		user = u;
		if (user != null){
			authenticated = true;
		}
		this.o = o;
		this.co = co;
		this.id = id;
//		if (socket.isConnected()){
//			try {
//				input = new LcInputStream(socket.getInputStream());
//				output = new PrintStream(this.socket.getOutputStream(), true, "UTF8");
//			} catch (IOException e) {
//				this.log.debug(e.toString());
//			}
//		}
		this.restrictActions();
	}
	
	public int getID(){
		return id;
	}
	
	public ClientHandlerObserver getClientHandlerObserver(){
		return co;
	}
	
	public List<String> getAllowedActions(){
		return this.allowedActions;
	}
	
	public User getUser(){
		return this.user;
	}
	
	public ILcSocket getSocket(){
		return this.socket;
	}
	
	public boolean isAuthenticated(){
		return this.authenticated;
	}
	
	public void reloadAvailableActions(){
		if (this.authenticated){
			this.availableActions = new ArrayList<String>(this.o.getActionSet());
			this.allowedActions = this.availableActions;
		}
	}
		
	public void close() throws IOException{
		this.socket.close();
	}
	
	public void close(Throwable t) throws Throwable{
		socket.close();
		throw t;
	}
	
	public void print(JSONObject jo) throws IOException{
		socket.send(jo.toString());
	}
	
	public void print(JSONObject jo, List<LcOutputStream> outputStreams, Set<String> unknownKeys) 
			throws IOException, InterruptedException{
		if (unknownKeys.size() > 0){
			String unknown = "Unknown keys '"+unknownKeys.toString()+"'.";
			if (jo.has("warnings")){
				jo.getJSONArray("warnings").put(unknown);
			} else {
				jo.put("warnings", new JSONArray().put(unknown));
			}
		}
		socket.send(jo, outputStreams);
	}
	
	protected void runAction() throws Exception {
		boolean printed = false;
//		int je = 0;
		while (true) {
			LcJSONObject request = null;
			printed = false;
			Set<String> unknownKeys = new HashSet<String>();
			inputStreams = new ArrayList<LcInputStream>();
			try {
				request = socket.getMessage(inputStreams);
//				System.err.println(pool);
				if (request == null) throw new BreakException();
				log.trace("receive: "+request);
				
				if (!request.isValid()){
					LcJSONObject error = new LcJSONObject();
					error.put("error", 
							"JSON-Luci message must contain ONLY ONE of the four status keys action,"
							+ " result, error, warning");
					socket.send(error);
					continue;
				}
				String action = this.getAction(request);
				if (action != null){
					ActionFactory af = o.getActionFactory(action);
					unknownKeys = new HashSet<String>();
					final List<LcInputStream> urlStreams = new ArrayList<LcInputStream>();
					int order = inputStreams.size();
					for (LcInputStream lis: LcURLInputStream.recursivelyFindURLS(request)){
						lis.setOrder(order++);
						inputStreams.add(lis);
						urlStreams.add(lis);
						lis.setReady();
					}
					af.validateInput(request, unknownKeys);
					Action a = af.getAction(this, request);
					if (a != null){
						List<LcOutputStream> outputStreams = new ArrayList<LcOutputStream>();
						JSONObject response = a.perform(inputStreams, outputStreams, unknownKeys);
						print(response, outputStreams, unknownKeys);
						printed = true;
						if (outputStreams.size() > 0) outputStreams.get(outputStreams.size() - 1)
							.waitUntilDone(60, TimeUnit.SECONDS);
						// a hook to run something after the action has completed
						if (this.confirmation(request)){

							if (a instanceof ISwitchClientHandler){
								throw new BreakException((ISwitchClientHandler)a);
							}
						}
					}
				}
//				je = 0;
				
			} catch(IOException e){
				this.log.debug("Closing socket upon IOException: "+this.socket.getRemoteAddress()+".");
				if (Luci.DEBUG) e.printStackTrace();
				this.handleIOException();
				this.close();
//				System.err.println(e);
				throw new BreakException();
			} catch (BreakException e){
//				System.err.println(e);
				this.handleBreakException();
				throw e;
			} catch (JSONException e) {
//				System.err.println(e);
				/* should not be necessary anymore
				 if (request != null){
					LcJSONObject stream = null;
					if (request.has("geometry")) stream = request.getLcJSONObject("geometry");
					if (request.has("inputs")) stream = request.getLcJSONObject("inputs");
					if (stream != null){
						LcInputStream input = socket.getLcInputStream();
						for (@SuppressWarnings("unused") String info[]: stream.getStreamInfos()){
							input.readAttachment();
						}
					}
				}
				
				if (e.getMessage().startsWith("A JSONObject text must begin with '{'")) { //) && je++ > 0) {
					continue;
				}*/
				socket.send(new JSONObject().put("error", e.getMessage()));
				printed = true;
//				if (this.log.isDebugEnabled()){
//					e.printStackTrace();
//				}
			}  catch (Exception e) {
//				System.err.println(e);
				if (e.getMessage() != null && !e.getMessage().startsWith("A JSONObject text must begin with")){
					if (!printed) {
						LcJSONObject error = new LcJSONObject();
						error.put("error", e.getMessage());
						socket.send(error);
					}
					if (Luci.DEBUG) e.printStackTrace();
				
				} else {
					if (!printed) {
						LcJSONObject error = new LcJSONObject();
						error.put("error", e.toString());
						socket.send(error);
					}
					if (Luci.DEBUG) e.printStackTrace();
				}
			} 
		}
	}
	
	protected abstract String getAction(LcJSONObject joi) throws Exception;
	protected abstract void restrictActions();
	protected abstract void handleBreakException();
	protected abstract boolean confirmation(JSONObject json);
	protected abstract void handleIOException();
	public abstract String getStatus();
}
