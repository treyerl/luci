/**
 * The MIT License (MIT)
 *
 * Copyright (c) <year> <copyright holders>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package org.luci.comm.clienthandlers;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * 
 * @author Lukas Treyer
 *
 */
public class ClientHandlerObserver {
	
	private Map<ClientProxy, Future<Void>> clients;
	private ExecutorService clientPool;
	
	public ClientHandlerObserver() {
		clients = new HashMap<ClientProxy, Future<Void>>();
		clientPool = Executors.newCachedThreadPool();
	}
	
	public ClientProxy run(ClientProxy cp){
		Future<Void> f = clientPool.submit(cp);
		clients.put(cp, f);
		return cp;
	}
	
	public ClientHandler getClientHandler(int ID){
		ClientProxy cp = getClientProxy(ID);
		if (cp != null) return cp.ch;
		else return null;
	}
	
	public ClientProxy getClientProxy(int ID){
		for (ClientProxy cp: clients.keySet()){
			if (cp.getID() == ID) return cp;
		}
		return null;
	}

	public ExecutorService getThreadPool() {
		return clientPool;
	}
	
	
	public void stop() throws IOException{
		Iterator<ClientProxy> it = clients.keySet().iterator();
		while(it.hasNext()){
			ClientProxy c = it.next();
			Future<Void> f = clients.remove(c);
			c.stop();
			f.cancel(true);
		}
	}
	
	public void remove(ClientProxy cp) throws IOException{
		clients.remove(cp);
	}

}
