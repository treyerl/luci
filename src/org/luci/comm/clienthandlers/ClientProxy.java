/**
 * The MIT License (MIT)
 *
 * Copyright (c) <year> <copyright holders>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package org.luci.comm.clienthandlers;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import org.luci.Luci;
import org.luci.comm.action.ISwitchClientHandler;
import org.luci.connect.ILcSocket;
import org.luci.factory.observer.FactoryObserver;
import org.slf4j.Logger;

public class ClientProxy implements Callable<Void>{
	ClientHandler ch;
	ILcSocket socket;
	Logger log;

	public ClientProxy(ILcSocket socket, FactoryObserver o, ClientHandlerObserver co){
		ch = new PublicClientHandler(socket, o, co, Luci.newClientId());
		log = Luci.createLoggerFor(getClass().getSimpleName());
		this.socket = socket;
	}
	public void stop() throws IOException{
		ch.close();
	}
	
	public String getIP(){
		SocketAddress remote = ch.getSocket().getRemoteAddress();
		if (remote instanceof InetSocketAddress){
			return ((InetSocketAddress)remote).getAddress().getHostAddress();
		}
		return null;
	}
	
	public int getID(){
		return ch.getID();
	}
	public void update(ClientHandler ch, Future<Object> f) throws IOException{
		this.ch = ch;
	}

	public ClientHandler getClientHandler() {
		return ch;
	}
	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public Void call() throws Exception {
		boolean run = true;
		while(run){
			try {
				ch.runAction();
			} catch (BreakException e) {
				this.log.trace("BreakException from "+this.socket.getRemoteAddress()+" - "+e.getMessage());
				if (e instanceof ISwitchClientHandler){
					ISwitchClientHandler switcher = (ISwitchClientHandler) e;
					if (switcher.shouldSwitch()){
						ch = switcher.switchClientHandler();
					}
				} else run = false;
			}
		}
		System.out.println(ch.getID()+" should stop now");
		stop();
		return null;
	}
	
}