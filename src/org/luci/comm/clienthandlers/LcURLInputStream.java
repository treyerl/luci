package org.luci.comm.clienthandlers;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.eclipse.jetty.client.HttpClient;
import org.eclipse.jetty.client.api.Request;
import org.eclipse.jetty.client.api.Response;
import org.eclipse.jetty.client.util.InputStreamResponseListener;
import org.json.JSONArray;
import org.json.JSONObject;
import org.luci.connect.LcInputStream;
import org.luci.connect.StreamInfo;

/**wrapper around InputStream to provide some functionality specific to Luci
 * @author Lukas Treyer <treyer@arch.ethz.ch>
 *
 */
public class LcURLInputStream extends LcInputStream {
	private String url;
	private HttpClient httpClient;
	private InputStreamResponseListener listener;
	private boolean sentRequest;
	
	public LcURLInputStream(String url, String format, 
			String name, String crs, JSONObject attributeMap ) 
			throws Exception{
		super(new StreamInfo(null, format, name, 0, 0, crs, attributeMap));
		this.url = url;
	}
	
	
	@Override
	public void setReady() {
		if (url != null && ! sentRequest){
			sentRequest = true;
			try{
				listener = new InputStreamResponseListener();
				httpClient = new HttpClient();
				httpClient.start();
				Request r = httpClient.newRequest(url);
				r.send(listener);
				// blocking method; waiting max 5 seconds
				Response response = listener.get(5, TimeUnit.SECONDS);
				if (response.getStatus() == 200){
					InputStream in = listener.getInputStream();
					setLength(Integer.parseInt(response.getHeaders().get("Content-Length")));
			    	setInputStream(in);
				} else {
					httpClient.stop();
					throw new TimeoutException("No response at "+url);
				}
			} catch (Exception e){
				throw new RuntimeException(e);
			}
			
		} 
		super.setReady();	
	}
	
	public void setDone(){
		if (httpClient != null){
			try {
				httpClient.stop();
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
		super.setDone();
	}
	
	public static List<LcURLInputStream> recursivelyFindURLS(JSONObject json) 
			throws Exception{
		List<LcURLInputStream> streams = new ArrayList<LcURLInputStream>();
		@SuppressWarnings("unchecked")
		Set<String> keys = json.keySet();
		for (String key: keys){
			Object val = json.get(key);
			if (val instanceof JSONObject){
				LcURLInputStream luis = _recursivelyFindURLS(streams, key, (JSONObject) val);
				if (luis != null) json.put(key, luis.getJSON());
			} else if (val instanceof JSONArray){
				JSONArray aval = (JSONArray) val;
				for (int i = 0; i < aval.length(); i++){
					Object oval = aval.get(i);
					if (oval instanceof JSONObject){
						LcURLInputStream luis = _recursivelyFindURLS(streams, key, (JSONObject) oval);
						if (luis != null) aval.put(i, luis.getJSON());
					}
				}
			}
		}
		return streams;
	}

	private static LcURLInputStream _recursivelyFindURLS(List<LcURLInputStream> streams, String key, 
			JSONObject jval) throws Exception {
		if (jval.has("format") && jval.has("url")){
			String url = jval.optString("url", null);
			if (url != null){
				LcURLInputStream lis = new LcURLInputStream(url, jval.getString("format"), key,
						jval.optString("crs", null), jval.optJSONObject("attributeMap"));
				streams.add(lis);
				return lis;
			}
		} else if (!(jval.has("format") && jval.has("streaminfo")) ){
			streams.addAll(recursivelyFindURLS(jval));
		}
		return null;
	}
	
}
