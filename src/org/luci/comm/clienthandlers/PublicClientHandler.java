package org.luci.comm.clienthandlers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.json.JSONObject;
import org.luci.connect.ILcSocket;
import org.luci.connect.LcJSONObject;
import org.luci.factory.observer.FactoryObserver;



public class PublicClientHandler extends ClientHandler {
	protected boolean authenticated = false;
	
	public PublicClientHandler(ILcSocket socket, FactoryObserver factories, ClientHandlerObserver co, int id) {
		super(socket, factories, null, co, id);
	}
	
	@Override
	protected void restrictActions()  {
		availableActions = new ArrayList<String>(o.getActionSet());
		List<String> allowed = new ArrayList<String>(Arrays.asList(
				"authenticate", 
				"get_list",
				"create_user",
				"exists",
				"exit"));
		allowedActions = allowed;
	}

	@Override
	protected String getAction(LcJSONObject joi) throws Exception {
		if (joi.has("action")) {
			String action = joi.getString("action");
			if (action.equals("exit")){
				print(new JSONObject().put("result","closing connection."));
				throw new BreakException("Client exit.");
			}
			if (!availableActions.contains(action)){
				print(new JSONObject().put("error","No action '" + action+ "'!"));
			} else if (allowedActions.contains(action)) {
				return action;
			} else {
				print(new JSONObject().put("error","Action '" + action+ "' not allowed!"));
			}
		} else if (joi.has("result")){
			print(new JSONObject().put("error", "Authentication required"));
		} else if (joi.has("warning")){
			print(new JSONObject().put("error", "Authentication required"));
		} else if (joi.has("error")){
			print(new JSONObject().put("error", "Authentication required"));
		} else {
			print(new JSONObject().put("error","Message contains no status keys 'action', 'result', 'warning','error'"));
		}
		return null;
	}

	@Override
	protected void handleBreakException() {
		// nothing to do
		
	}
	
	@Override
	protected boolean confirmation(JSONObject json){
		return true;
	}

	@Override
	protected void handleIOException() {
	}
	
	@Override
	public String getStatus(){
		return "public";
	}
}
