package org.luci.comm.clienthandlers;

import java.sql.Connection;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;

import org.eclipse.paho.client.mqttv3.MqttException;
import org.json.JSONObject;
import org.luci.comm.action.IGetServiceInputs;
import org.luci.comm.action.ISetServiceOutputs;
import org.luci.connect.DirectStreamWriter;
import org.luci.connect.LcInputStream;
import org.luci.connect.LcJSONObject;
import org.luci.connect.LcOutputStream;
import org.luci.factory.RemoteServiceFactoryThread;
import org.luci.factory.observer.FactoryObserver;
import org.luci.services.RemoteService;
import org.luci.services.ServiceCall;
import org.luci.utilities.TimeRangeSelection;

public class RemoteServiceClientHandler extends ClientHandler{
	private long SObjID = 0;
	private RemoteServiceFactoryThread rsf;
	private RemoteService remoteServiceInstance;
	private String serviceName;
	private String machineName;
	private String version;
	private CountDownLatch timer;
	private long reservedFor;
	private int iteration;
	private boolean hasResult;
	
	public RemoteServiceClientHandler(ClientHandler ch, FactoryObserver factories, 
			RemoteServiceFactoryThread rsf, String version, String machinename, long reservedFor) {
		super(ch.getSocket(), factories, ch.getUser(), ch.getClientHandlerObserver(), ch.getID());
		this.serviceName = rsf.getName();
		this.machineName = machinename; 
		this.version = version;
		this.rsf = rsf;
		this.reservedFor = reservedFor;
		iteration = 1;
		hasResult = false;
	}
	
	public long SObjIDreservation(){
		return reservedFor;
	}
	
	@Override
	protected void restrictActions() {
		this.availableActions = new ArrayList<String>(this.o.getActionSet());
		this.allowedActions = new ArrayList<String>(Arrays.asList(
				"remote_unregister", 
				"remote_cancel", 
				"update_service_outputs",
				"get_scenario"));
	}
		
	public ServiceCall getServiceCall(){
		if (remoteServiceInstance != null) return remoteServiceInstance.getCall();
		return null;
	}

	public CountDownLatch getAnswerLatch(int i){
		return timer = new CountDownLatch(i);
	}
	
	public void setRemoteService(RemoteService rs){
		this.remoteServiceInstance = rs;
		if (rs != null) SObjID = rs.getSObjID();
		else SObjID = 0;
	}
	
	public void notifyRemoteService(){
		if (timer != null ){
			timer.countDown();
		}
	}

	public RemoteServiceFactoryThread getFactory(){
		return this.rsf;
	}
	
	public void sendRun() throws Exception{
		ExecutorService pool = o.getThreadPool();
		
		IGetServiceInputs gsi = (IGetServiceInputs) o.getAction("get_service_inputs", this, null);
		ISetServiceOutputs sso = (ISetServiceOutputs) o.getAction("update_service_outputs", this, null);
		ServiceCall call = remoteServiceInstance.getCall();
		LcJSONObject run = new LcJSONObject();
		run.put("action", "run");
		run.put("SObjID", SObjID);
		run.put("ScID", call.getScID());
		run.put("timestamp_inputs", call.getTimestamp().getTime());
		if (call.hasMessage()) run.put("message", call.getMessage());
		
		iteration = 1;
		hasResult = false;
		
		JSONObject inputs;
		List<LcOutputStream> outputStreams = new ArrayList<LcOutputStream>();
		if (call.isSync()){
			inputs = call.getInputs();
			for (LcInputStream lis: call.getInputStreams()){
				LcOutputStream los = new LcOutputStream(lis, null);
				pool.submit(new DirectStreamWriter(lis, los));
			}
		} else {
			Connection con = o.getDatabaseConnection();
			gsi.setCon(con);
			boolean getSubscriptions = false;
			gsi.setSObjID(call.getID());
			gsi.setScID(call.getScID());
			inputs = gsi.getServiceInputs(getSubscriptions, null, outputStreams);
			sso.setCon(con);
			sso.setServiceStatus(remoteServiceInstance.getCall(), "SEND");
			con.close();
		}
		run.put("inputs", inputs); 
		this.log.debug(this.SObjID+" sending 'RUN' to "+this.machineName+". ");
		socket.send(run, outputStreams);
		
		this.log.debug(this.SObjID+" sent 'RUN' and all parameters to "+this.machineName+". ");
		/* if the remote serivce is REALLY fast the remoteServiceInstance might be null again already
		 * therefore we ask for != null here */
		if (remoteServiceInstance != null && !call.isSync()) 
			sso.setServiceStatus(remoteServiceInstance.getCall(), "PEND");
	}
	
	public void sendCancel(){
		try{
			iteration = 1;
			hasResult = false;
			print(new JSONObject("{'action':'cancel'}"));
		} catch (Exception e){
			// cancel might be called because of a connection failure FIXME: necessary?
		}
	}
	
	public String getServiceName() {
		return this.serviceName;
	}
	
	public String getVersion() {
		return this.version;
	}
	
	public String getMachineName(){
		return this.machineName;
	}
	
	@Override
	protected String getAction(LcJSONObject joi) throws Exception{
		if (joi.has("action")) {
			String action = joi.getString("action");
			if (action.equalsIgnoreCase("remote_unregister")) {
				joi.put("servicename", serviceName);
				joi.put("SObjID", this.SObjID);
				// this allows the unregister-action to test whether the remote service provider
				// is in the middle of a task; if a remote service provider is processing a task
				// its client handler knows the SObjID of the service instance (of ID of the task)
				// when a task is fulfilled the SObjID is set to 0;
			}
			if (this.availableActions.contains(action)){
				if (this.allowedActions.contains(action)){
					return action;
				} else {
					print(new JSONObject().put("error","Action '"+action+"' not allowed!"));
				}
			} else {
				print(new JSONObject().put("error","Action '"+action+"' unknown!"));
			}
		} else if (joi.has("error")){
			this.remoteServiceInstance.throwException(new Exception(joi.getString("error")));
			this.rsf.putBack(this);
		} else return receive(joi);
		return null;
	}
	
	private String receive(JSONObject json) throws Exception {
		Timestamp rt = new Timestamp(json.getJSONObject("result").getLong("timestamp_inputs"));
		if (!rt.equals(remoteServiceInstance.getCall().getTimestamp())) {
			print(new JSONObject().put("error", "wrong input_hashcode"));
		}
		json.put("iteration", iteration++);
		try{
			if ((hasResult = json.has("result"))){
				remoteServiceInstance.setOutputs(
						json.getJSONObject("result").getJSONObject("outputs"), inputStreams);
				if (timer != null ) timer.countDown();
				this.rsf.putBack(this);
			}
			if (json.has("progress")){
				remoteServiceInstance.setProgress(
						json.getJSONObject("progress").getJSONObject("outputs"), inputStreams);
			}
			print(new JSONObject().put("result", new JSONObject().put("confirmation", true)));
		} catch (Exception e){
			print(new JSONObject().put("error", e.toString()));
			throw e;
		}
		
		return null;
	}
	
	@Override
	protected boolean confirmation(JSONObject json){
		return true;
	}
	
	@Override
	protected void handleIOException(){
		handleException();
	}
	
	@Override
	protected void handleBreakException() {
		handleException();
	}
	
	private void handleException(){
		
		//if (SObjID != 0) rsf.cancelExecution(remoteServiceInstance.getCall());
		try {
			rsf.unregister(this);
		} catch (MqttException e) {
			e.printStackTrace();
		}
		if (reservedFor != 0) o.unregisterRemoteService(reservedFor);
	}
	
 	@Override
	public String getStatus(){
		return "remote registered";
	}
}
