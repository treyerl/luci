/**
 * The MIT License (MIT)
 *
 * Copyright (c) <year> <copyright holders>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package org.luci.comm.clienthandlers;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.luci.connect.ILcSocket;
import org.luci.connect.LcInputStream;
import org.luci.connect.LcOutputStream;
import org.luci.factory.observer.FactoryObserver;

/**
 * 
 * @author treyerl
 *
 */
public class WebClientProxy extends ClientProxy {
	private Map<String, LcInputStream> uploads;
	private Map<String, CountDownLatch> upWait;
	private Map<String, LcOutputStream> downloads;
	private Map<String, CountDownLatch> downWait;
	/**
	 * @param ch
	 * @param f
	 */
	public WebClientProxy(ILcSocket socket, FactoryObserver o, ClientHandlerObserver co) {
		super(socket, o, co);
		uploads = new HashMap<String, LcInputStream>();
		upWait = new HashMap<String, CountDownLatch>();
		downloads = new HashMap<String, LcOutputStream>();
		downWait = new HashMap<String, CountDownLatch>();
	}
	
	public void stop() throws IOException{
		uploads.clear();
		downloads.clear();
		ch.close();
	}
	public LcOutputStream getDownloadStream(String checksum, int waitFor, TimeUnit tu) throws InterruptedException{
//		System.out.println("ClientProxy: get DownloadStream: "+checksum);
		boolean contains = false;
		synchronized(downloads){
			contains = downloads.containsKey(checksum);
			if (contains) return downloads.remove(checksum);
		}
		if (!contains) {
			CountDownLatch c = new CountDownLatch(1);
			downWait.put(checksum, c);
			if(c.await(waitFor, tu)){
				downWait.remove(checksum);
				return downloads.remove(checksum);
			}
		}
		throw new NoSuchElementException();
	}
	
	public void putDownloadStream(LcOutputStream stream){
//		System.out.println("ClientProxy: put DownloadStream: "+stream.getChecksum());
		String checksum = stream.getChecksum();
		synchronized(downloads){
			downloads.put(checksum, stream);
			if (downWait.containsKey(checksum)) downWait.get(checksum).countDown();
		}
	}
	
	public LcInputStream getUploadStream(String chck, long millisToWait) throws InterruptedException{
		if (uploads.containsKey(chck)) return uploads.remove(chck);
		else {
			CountDownLatch c = new CountDownLatch(1);
			upWait.put(chck, c);
			c.await(millisToWait, TimeUnit.MILLISECONDS);
			upWait.remove(chck);
			return uploads.remove(chck);
		}
	}
	
	public void putUploadStream(LcInputStream stream){
		String checksum = stream.getChecksum();
		uploads.put(checksum, stream);
		if (upWait.containsKey(checksum)) upWait.get(checksum).countDown();
	}
}
