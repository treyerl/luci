package org.luci.connect;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import org.luci.Luci;
import org.luci.connect.LcInputStream;
import org.luci.connect.LcString;

public class BufferedStreamWriter extends StreamWriter{
	private class StreamReader implements Runnable{
		@Override
		public void run() {
			try {
				MessageDigest d = MessageDigest.getInstance("MD5");
				in.waitUntilReady(20, TimeUnit.SECONDS);
				int buffersize = Luci.BUFFERSIZE;
				int nRead, len = in.getLength(), pos = 0;
				byte[] data = new byte[buffersize];
				while (pos < len ) {
					nRead = in.read(data, 0, Math.min(buffersize, len - pos));
					buffer.position(pos).limit(pos+nRead);
					ByteBuffer slice = buffer.slice();
					buffer.put(data, 0, nRead);
					if (chck) d.update(data, 0, nRead);
					pos += nRead;
					bufferQueue.add(slice);
				}
				if (chck){
					String checksum = LcString.getHexFromBytes(d.digest());
					if (!in.getChecksum().equalsIgnoreCase(checksum)){
						throw new RuntimeException("BufferedStreamWriter: Checksums differ!");
					}
				}
				in.setDone();
			} catch (InterruptedException | IOException | NoSuchAlgorithmException e) {
				throw new RuntimeException(e);
			}
		}
	}
	private final LinkedBlockingQueue<ByteBuffer> bufferQueue;
	private final ByteBuffer buffer;
	
	public BufferedStreamWriter(LcInputStream in, LcOutputStream out, 
			boolean closeIn, boolean closeOut, boolean checksumCheck) {
		super(in, out, closeIn, closeOut, checksumCheck, checksumCheck);
		bufferQueue = new LinkedBlockingQueue<ByteBuffer>();
		buffer = ByteBuffer.allocateDirect(in.getLength());
		new Thread(new StreamReader()).start();
	}
	
	public BufferedStreamWriter(LcInputStream in, LcOutputStream out) {
		this(in, out, false, false, false);
	}

	@Override
	public Void call() throws Exception {
		try {
			out.waitUntilReady(20, TimeUnit.SECONDS);
			int buffersize = Luci.BUFFERSIZE;
			int nRead, len = in.getLength();
			byte[] data = new byte[buffersize];
			while (len > 0 ) {
				ByteBuffer bb = bufferQueue.take();
				len -= nRead = bb.limit();
				bb.get(data, 0, nRead);
				out.write(data, 0, nRead);
			}
			out.flush();
			out.setDone();
		} catch (InterruptedException e) {
			in.setDone(new RuntimeException(e));
			out.setDone(new RuntimeException(e));
			throw e;
		}
		if (shouldCloseIn) in.close();
		if (shouldCloseOut) out.close();
		return null;
	}
		
	public static BufferedStreamWriter create(StreamInfo si, ByteBuffer bb){
		LcInputStream in = new LcInputStream(si, new ByteArrayInputStream(bb.array()));
		LcOutputStream out = new LcOutputStream(si);
		in.setReady();
		return new BufferedStreamWriter(in, out, true, false, false);
	}
	
	public static BufferedStreamWriter create(StreamInfo si, File file) throws FileNotFoundException {
		FileInputStream fis = new FileInputStream(file);
		LcInputStream in = new LcInputStream(si, fis);
		LcOutputStream out = new LcOutputStream(si);
		in.setReady();
		return new BufferedStreamWriter(in, out, true, false, false);
	}
	
}
