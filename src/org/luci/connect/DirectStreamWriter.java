package org.luci.connect;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.util.concurrent.TimeUnit;

import org.luci.Luci;
import org.luci.connect.LcInputStream;
import org.luci.connect.LcString;

public class DirectStreamWriter extends StreamWriter{

	/**
	 * @param in
	 * @param out
	 * @param closeIn
	 * @param closeOut
	 * @param calculateChecksum
	 * @param verifyChecksum
	 */
	public DirectStreamWriter(LcInputStream in, LcOutputStream out,
			boolean closeIn, boolean closeOut, boolean calculateChecksum, boolean verifyChecksum) {
		super(in, out, closeIn, closeOut, calculateChecksum, verifyChecksum);
	}
	

	/**
	 * @param in
	 * @param out
	 */
	public DirectStreamWriter(LcInputStream in, LcOutputStream out) {
		super(in, out);
	}

	@Override
	public Void call() throws Exception {
		try {
			MessageDigest d = MessageDigest.getInstance("MD5");
			boolean outime = out.waitUntilReady(20, TimeUnit.SECONDS);
			boolean intime = in.waitUntilReady(20, TimeUnit.SECONDS);
			if (intime && outime){
//				System.out.println("DirectStreamWriter: done waiting!");
				int buffersize = Luci.BUFFERSIZE;
				int nRead, len = in.getLength();
				byte[] data = new byte[buffersize];
				while (len > 0 ) {
					len -= (nRead = in.read(data, 0, Math.min(buffersize, len)));
					out.write(data, 0, nRead);
					if (clck) d.update(data, 0, nRead);
				}
				out.flush();
				if (clck){
					String checksum = LcString.getHexFromBytes(d.digest());
					if (chck && !in.getChecksum().equalsIgnoreCase(checksum)){
						throw new RuntimeException("DirectStreamWriter: Checksums differ!");
					}
					in.setChecksum(checksum);
				}
				in.setDone();
				out.setDone();
			} else System.out.println(String.format("WARN: timeout on inputStream (%B), "
					+ "outputStream (%b)!", !intime, !outime));
		} catch (InterruptedException e) {
			in.setDone(new RuntimeException(e));
			out.setDone(new RuntimeException(e));
			throw e;
		}
		if (shouldCloseIn) in.close();
		if (shouldCloseOut) out.close();
		return null;
	}
	
	public static DirectStreamWriter create(StreamInfo si, ByteBuffer bb){
		LcInputStream in = new LcInputStream(si, new ByteArrayInputStream(bb.array()));
		LcOutputStream out = new LcOutputStream(si);
//		System.out.println("DirectStreamWriter: "+out);
		in.setReady();
		return new DirectStreamWriter(in, out, true, false, false, false);
	}
	
	public static DirectStreamWriter create(StreamInfo si, File file) 
			throws FileNotFoundException {
		FileInputStream fis = new FileInputStream(file);
		LcInputStream in = new LcInputStream(si, fis);
		LcOutputStream out = new LcOutputStream(si);
		in.setReady();
		return new DirectStreamWriter(in, out, true, false, false, false);
	}
	
}
