/**
 * The MIT License (MIT)
 *
 * Copyright (c) <year> <copyright holders>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package org.luci.connect;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.json.JSONObject;

/**
 * 
 * @author treyerl
 *
 */
public class GeometryInfo {
	public final String format;
	public final String crs;
	protected String name;
	protected String representation;
	protected final JSONObject json;
	public final Map<String, String> attributeMap;

	public GeometryInfo(String name, JSONObject j) {
		this(name, j.getString("format"), j.optString("crs", null), 
				j.optJSONObject("geometry"),
				j.optJSONObject("attributeMap"));
	}
	
	public GeometryInfo(String name, String format, String crs, JSONObject geometry, 
			JSONObject aMap){
		this(name, format, crs, geometry, null, 0);
		if (geometry != null) json.put("geometry", geometry);
		if (crs != null) json.put("crs", crs);
		initAttributeMap(aMap);
	}
	
	public GeometryInfo(String name, String format, String crs, JSONObject geometry, 
			Map<String, String> aMap){
		this(name, format, crs, geometry, aMap, 0);
		if (geometry != null) json.put("geometry", geometry);
		if (crs != null) json.put("crs", crs);
	}
	
	private GeometryInfo(String name, String format, String crs, JSONObject geometry,
			Map<String, String> map, int privateDistinguishingMethodSignatures){
		this.name = name;
		this.format = format;
		this.crs = crs;
		this.json = new JSONObject()
			.put("format", format);
		attributeMap = (map == null) ? new HashMap<String, String>(): map;
	}
	
	private void initAttributeMap(JSONObject map){
 		if (map != null){
			@SuppressWarnings("unchecked")
			Set<String> keys = map.keySet();
			for (String key: keys){
				String mappedTo = map.optString(key);
				if (mappedTo != null) attributeMap.put(key, mappedTo);
			}
		}
 	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		System.out.println("set name");
		this.name = name;
	}
	
	public void setRepresentation(String r){
		representation = r;
	}
	
	@Override
	public String toString(){
		if (representation != null) return representation;
		return getJSON().toString();
	}
	
	public JSONObject getJSON(){
 		if (attributeMap != null && !attributeMap.isEmpty()) 
 			json.put("attributeMap", new JSONObject(attributeMap));
 		return json;
 	}

}
