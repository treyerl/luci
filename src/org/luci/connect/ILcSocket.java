/**
 * The MIT License (MIT)
 *
 * Copyright (c) <year> <copyright holders>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package org.luci.connect;

import java.io.IOException;
import java.net.SocketAddress;
import java.util.List;

import org.json.JSONObject;
import org.luci.connect.LcInputStream;

/**
 * 
 * @author Lukas Treyer
 *
 */
public interface ILcSocket {

	/**sends a string in UTF8
	 * @param msg
	 * @throws IOException
	 */
	public void send(String msg) throws IOException;
	
	/**
	 * @param o
	 * @return
	 * @throws IOException 
	 * @throws Exception 
	 */
	public String send(JSONObject o) throws InterruptedException, IOException;
	public String send(JSONObject o, List<LcOutputStream> outputs) throws InterruptedException, IOException;
	
	public SocketAddress getRemoteAddress();
	public SocketAddress getLocalAddress();
	public void close() throws IOException;
	
	/**Blocks until newline char has arrived (which has to appear after the json string) and before
	 * any binary attachments. Any streaminfo json objects are transformed into LcInputStream objects.
	 * @param inputstreams
	 * @return JSONObject representing the parsed header string
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public LcJSONObject getMessage(List<LcInputStream> inputstreams) 
			throws IOException, InterruptedException;
}
