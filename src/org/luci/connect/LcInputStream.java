package org.luci.connect;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.luci.connect.LcInputStream;
import org.luci.connect.LcString;

/**wrapper around InputStream to provide some functionality specific to Luci
 * @author Lukas Treyer <treyer@arch.ethz.ch>
 *
 */
public class LcInputStream extends SynchronizedStream {
	private int index;
	private InputStream in;
	
	public LcInputStream(String keyname, JSONObject j, InputStream i){
		super(keyname, j);
		in = i;
	}
	
	public LcInputStream(StreamInfo si, InputStream i){
		super(si);
		in = i;
	}
	
	public LcInputStream(StreamInfo si){
		super(si);
	}
	
	public LcInputStream(String checksum, String format, String keyname, int order, int len, 
			String crs, JSONObject attributeMap, InputStream i){
		super(checksum, format, keyname, order, len, crs, attributeMap);
		in = i;
	}
		
	public void setInputStream(InputStream i){
		in = i;
	}
	
	public InputStream getInputStream(){
		return in;
	}
	
	/**reads exactly one line from the input stream: no data is lost in a left over buffer or alike.
	 * http://en.wikipedia.org/wiki/UTF-8 as a reference for UTF8 decoding
	 * @return String: line
	 * @throws IOException
	 */
	@Deprecated
	public static String readExactlyOneLineUTF8(InputStream in) throws IOException{
		
		StringBuilder sb = new StringBuilder();
		byte b1[] = new byte[1];
		byte b2[] = new byte[2];
		byte b3[] = new byte[3];
		byte b4[] = new byte[4];

		
		int i = 0;
		int j = 0;
		while(i >= 0){
			// in.read() returns an integer between 0 and 255
			i = in.read();
//			System.out.print(i+"-");
			
			if (i >= 240){ // 1111 0000 = 4 byte character
				b4[0] = (byte) i;
				for (j = 1; j < 4; j++)
					b4[j] = (byte) in.read();
				sb.append(new String(b4, "UTF-8"));
				
			} else if (i >= 224) { // 1110 0000 = 3 byte character
				b3[0] = (byte) i;
				for (j = 1; j < 3; j++)
					b3[j] = (byte) in.read();
				sb.append(new String(b3, "UTF-8"));
				
			} else if (i >= 192) { // 1100 0000 = 2 byte character
				b2[0] = (byte) i;
				for (j = 1; j < 2; j++)
					b2[j] = (byte) in.read();
				sb.append(new String(b2, "UTF-8"));
				
			} else if (i >= 128) { // 1000 0000 = continuation of multi-byte character
				// now this should not happen, but apparently we are receiving 5 and/or 6 bytes characters
				// --> do nothing, ignore this byte
				
			} else if (i >= 0) { // ASCII case
				// Microsoft, most textual internet protocols: CR + LF, \r + \n
				// UNIX, C based programs: LF, \n
				// old Macs (<=OS9): CR, \r --- not supported
				
				if (i == 13) // \r = 13, \r\n = CR + LF = Microsoft
					i = in.read();
				if (i == 10) {// \n = 10
//					System.out.println("");
					return sb.toString();
				}
				b1[0] = (byte) i;
				sb.append(new String(b1, "UTF-8"));
				
			} else { // EOF
				if (sb.length() > 0){
					return sb.toString();
				}
			}
		}
//		System.err.println("it");
		throw new IOException();
//		return null;
	}
	
	public int read(byte[] buf) throws IOException{
		int nRead = read(buf, 0, buf.length);
		index += nRead;
		return nRead;
	}
	
	public int read(byte[] buf, int offset, int length) throws IOException{
		if (isReady()) {
			int nRead = in.read(buf, offset, length);
			index += nRead;
			return nRead;
		}
		throw new IllegalStateException("InputStream not ready!");
	}

	public int read() throws IOException {
		if (isReady()) return (getLength() - index++ > 0) ? in.read() : -1;
		throw new IllegalStateException("InputStream not ready!");
	}
	
	public void close() throws IOException{
		in.close();
	}
	
	/**Calculates the MD5 checksum of a ByteBuffer and creates a new Attachment with the 
	 * parameters passed. The ByteBuffer's position will be set to 0; checksum is being calculated 
	 * from position().
	 * @param bb ByteBuffer
	 * through limit()
	 * @param format String
	 * @param name String
	 * @param attributeMap  optional JSONObject describing the mapping between attribute names
	 * @return
	 */
	public static LcInputStream load(ByteBuffer bb, String format, String name, String crs, JSONObject attributeMap) {
		String checksum = LcString.calcChecksum(bb);
		LcInputStream in = new LcInputStream(checksum, format, name, 0, bb.capacity(), crs, attributeMap, 
				new ByteArrayInputStream(bb.array()));
		in.setReady();
		return in;

	}
	
	public static LcInputStream load(File file) throws IOException{
		return load(file, null, null, null);
	}
	
	/**loads a file into a ByteBuffer which is used to build an Attachment object
	 * the order of the attachment is set to 0
	 * the name is set to the filename (excl. format suffix)
	 * the format is set to filename suffix
	 * @param file File to load
	 * @param attributeMap optional JSONObject describing the mapping between attribute names
	 * @return Attachment
	 * @throws IOException
	 */
	public static LcInputStream load(File file, String checksum, String crs, JSONObject attributeMap) 
			throws IOException{
		if (file.exists()){
			String filename = file.getName();
			String format = filename.substring(filename.lastIndexOf(".")+1);
			filename = filename.substring(0,filename.lastIndexOf('.'));
			int len = (int) file.length();
			if (checksum == null)  {
				try{
					FileInputStream fis = new FileInputStream(file);
					checksum = getChecksum(len, fis);
					fis.close();
				} catch(NoSuchAlgorithmException e){
					// WHAT?
				}
			}
//			JSONObject containterToMakeItWork = new JSONObject().put("attributeMap", attributeMap);
			LcInputStream in = new LcInputStream(checksum, format, filename, 0, len , crs, attributeMap, 
					new FileInputStream(file));
			in.setReady();
			return in;
		}
		return null;
	}
	
	public static Comparator<LcInputStream> getOrderComparator(){
		return new Comparator<LcInputStream>(){
			@Override
			public int compare(LcInputStream o1, LcInputStream o2) {
				return ((Integer) o1.getOrder()).compareTo(o2.getOrder());
			}	
		};
	}
	
	public static String getChecksum(int len, InputStream in) 
			throws NoSuchAlgorithmException, IOException{
		int i = 0, nRead = 0, size = 8196;
		byte[] data = new byte[size];
		MessageDigest d = MessageDigest.getInstance("MD5");
		while (i < len){
			i += (nRead = in.read(data, 0, size));
			d.update(data, 0, nRead);
		}
		
		return LcString.getHexFromBytes(d.digest());
	}
	
	public static Map<Integer, LcInputStream> recursivelyFindIn(JSONObject json){
		Map<Integer, LcInputStream> streams = new TreeMap<Integer, LcInputStream>();
		@SuppressWarnings("unchecked")
		Set<String> keys = json.keySet();
		for (String key: keys){
			Object val = json.get(key);
			if (val instanceof JSONObject){
				_recursivelyFindIn(streams, key, (JSONObject)val);
			} else if (val instanceof JSONArray){
				JSONArray aval = (JSONArray) val;
				for (int i = 0; i < aval.length(); i++){
					Object oval = aval.get(i);
					if (oval instanceof JSONObject)
						_recursivelyFindIn(streams, null, (JSONObject)oval);
				}
			}
		}
		return streams;
	}

	private static void _recursivelyFindIn(Map<Integer, LcInputStream> streams, String key, 
			JSONObject jval) throws JSONException {
		if (jval.has("format") && jval.has("streaminfo")){
			JSONObject streaminfo = jval.getJSONObject("streaminfo");
			String checksum = streaminfo.getString("checksum");
			// check for LcMetaJSON
			if (!checksum.equals("string")){
				int order = streaminfo.getInt("order");
				String name = (key != null) ? key : jval.getJSONObject("streaminfo").getString("checksum");
				streams.put(order, new LcInputStream(name, jval, null));
			}
			
		} else {
			for(Entry<Integer, LcInputStream> en: recursivelyFindIn(jval).entrySet()){
				LcInputStream kid = en.getValue();
				LcInputStream old = streams.putIfAbsent(en.getKey(), kid);
				if (old != null) {
					if (!old.getChecksum().equalsIgnoreCase(kid.getChecksum())){
						String msg = String.format(chck, old.getName(), kid.getName());
						throw new JSONException(msg);
					}
				}
			}
		}
	}
	
	public static Map<String, LcInputStream> getStreamsByChck(JSONObject json){
		return getStreamsByChck(recursivelyFindIn(json).values());
	}
	
	public static Map<String, LcInputStream> getStreamsByChck(Collection<LcInputStream> streams){
		Map<String, LcInputStream> streamsByChck = new HashMap<String, LcInputStream>();
		for (LcInputStream lis: streams) streamsByChck.put(lis.getChecksum(), lis);
		return streamsByChck;
	}
	
	public static void recursivelyPutInto(JSONObject json, Map<String, LcInputStream> streamsByChck){
		@SuppressWarnings("unchecked")
		Set<String> keys = json.keySet();
		for (String key: keys){
			Object val = json.get(key);
			if (val instanceof JSONObject){
				JSONObject jval = (JSONObject) val;
				if (jval.has("format") && jval.has("streaminfo")){
					String chck = jval.getJSONObject("streaminfo").getString("checksum");
					if (!chck.equals("string")) json.put(key, streamsByChck.get(chck));
				} else {
					recursivelyPutInto(jval, streamsByChck);
				}
			} else if (val instanceof JSONArray){
				JSONArray aval = (JSONArray) val;
				for (int i = 0; i < aval.length(); i++){
					Object oval = aval.get(i);
					if (oval instanceof JSONObject){
						JSONObject jval = (JSONObject) oval;
						if (jval.has("format") && jval.has("streaminfo")){
							String chck = jval.getJSONObject("streaminfo").getString("checksum");
							// check for LcMetaJSON
							if (!chck.equals("string")) aval.put(i, streamsByChck.get(chck));
						}
					}
				}
			}
		}
	}
}
