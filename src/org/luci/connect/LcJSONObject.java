package org.luci.connect;

import java.util.Arrays;
import java.util.Collections;
import java.util.Set;

import org.json.JSONObject;

/**JSON Luci Object: extending a normal JSONObject with some methods
 * useful for the JSON Luci convention.
 * @author Lukas Treyer <treyer@arch.ethz.ch>
 *
 */
public class LcJSONObject extends JSONObject {
	public interface ValueFilter{
		public boolean filter(Object value);
	}
	
// constructors
	
	public LcJSONObject() {
		super();
	}
	
	public LcJSONObject(String s){
		super(s);
	}
	
	public LcJSONObject(JSONObject jo){
		super(jo, JSONObject.getNames(jo));
	}
	
// basic
	
	@Override
	@SuppressWarnings("unchecked")
	public Set<String> keySet(){
		return super.keySet();
	}
	
	public boolean isValid(){
		return !Collections.disjoint(keySet(), Arrays.asList("action", "error", "result", "progress"));
	}
	
//	
//	public static JSONObject keepOnlyStrings(JSONObject json){
//		@SuppressWarnings("unchecked")
//		Set<String> keySet = json.keySet();
//		for (String key: keySet){
//			Object value = json.get(key);
//			if (!(value instanceof String)) json.remove(key);
//		}
//		return json;
//	}
//	
//// filter
//
//	public Map<String, ?> typedFilter(JSONObject json, ValueFilter vf, Class<?> cls, boolean recursive){
//		Map<String, Object> result = new HashMap<String, Object>();
//		@SuppressWarnings("unchecked")
//		Set<String> keySet = json.keySet();
//		for (String key: keySet){
//			Object value = json.get(key);
//			if (cls.isAssignableFrom(value.getClass()) && vf.filter(value)) result.put(key, cls.cast(value));
//			if (value instanceof JSONObject && recursive) 
//				result.putAll(typedFilter((JSONObject) value, vf, cls, recursive));
//		}
//		return result;
//	}
//	
//	public Map<String, String> getURLs(){
//		@SuppressWarnings("unchecked")
//		Map<String, String> result = (Map<String, String>) typedFilter(this, new ValueFilter(){
//			@Override
//			public boolean filter(Object value) {
//				return ((String) value).equals("http://");
//			}
//		}, String.class, true);
//		return result;
//	}
//	
//	public Map<String, String> getMqttTopicStrings(){
//		@SuppressWarnings("unchecked")
//		Map<String, String> result = (Map<String, String>) typedFilter(this, new ValueFilter(){
//			@Override
//			public boolean filter(Object value) {
//				return ((String) value).equals(Luci.getConfig().getProperty("mqtt_pre", "/Luci/"));
//			}
//		}, String.class, true);
//		return result;
//	}
//	
//	public Map<String, JSONObject> getChildrenJSONObjects(){
//		@SuppressWarnings("unchecked")
//		Map<String, JSONObject> result = (Map<String, JSONObject>) typedFilter(this, new ValueFilter(){
//			@Override
//			public boolean filter(Object value) {
//				return value instanceof JSONObject;
//			}
//		}, JSONObject.class, true);
//		return result;
//	}
//	
//	public boolean hasObjects(){
//		return getChildrenJSONObjects().size() > 0;
//	}
//	
}
