/**
 * The MIT License (MIT)
 *
 * Copyright (c) <year> <copyright holders>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package org.luci.connect;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.luci.Luci;
import org.luci.connect.LcString;

/**Class that handles the Luci specific syntax checking for JSON keys when describing the 
 * I/O capabilities of a services or an action.
 * 
 * @author treyerl
 *
 */
public class LcMetaJSONObject extends JSONObject{
	
	public static final List<String> jsontypes = 
			Arrays.asList("json", "list", "string", "number", "boolean", "streaminfo", "geometry", "range", "timerange");
		
	public static final List<String> untypedKeys = Arrays.asList("action", "error", "subject", "comment", "attributemap");
	
	public final static String streaminfo = "{'format':'string', "
										  + " 'streaminfo':{"
										  + "		'checksum':'string',"
										  + "		'order':'number',"
										  + "		'length':'number',"
										  + "		'OPT encoding':'string'"
										  + " },"
										  + " 'OPT attributeMap':{'luci attribute name':'foreign attribute name'},"
										  + " 'OPT crs':'string'"
										  + "}";
	public final static String geometry = "{	'format':'string', "
											+ "	'geometry':'json',"
											+ "	'OPT attributeMap':{'luci attribute name':'foreign attribute name'},"
											+ "}";
	
	public final static String timerange = "{"
										+ "	'XOR until':number,"
										+ "	'XOR from':number,"
										+ "	'XOR exactly':number,"
										+ "	'XOR between':[number,number],"
										+ "	'OPT all':'boolean'"
										+ "}";
	
	private String wildcardKeyname = null;
	
// constructors
	
	public LcMetaJSONObject() {
		super();
	}
	
	public LcMetaJSONObject(String s){
		super(s);
	}
	
	public LcMetaJSONObject(JSONObject jo){
		super(jo, getNames(jo));
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public Set<String> keySet(){
		return super.keySet();
	}
	
	public List<String> getXorKeys(){
		List<String> keys = new ArrayList<String>();
		if (this.length() > 0){
			for (String key: JSONObject.getNames(this)){
				if (key.startsWith("XOR ")) keys.add(key.replace("XOR ", ""));
			}
		}
		return keys;
	}
	
	public int countXorKeysFrom(JSONObject json){
		List<String> xors = this.getXorKeys();
		int count = 0;
		if (json != null && json.length() > 0){
			for (String key: JSONObject.getNames(json)){
				if (xors.contains(key)) count++;
			}
		}
		return count;
	}
	
	public List<String> getOptKeys(){
		List<String> keys = new ArrayList<String>();
		if (this.length() > 0){
			for (String key: JSONObject.getNames(this)){
				if (key.startsWith("OPT ")) keys.add(key.replace("OPT ", ""));
			}
		}
		return keys;
	}
	
	public Map<String, String> getCleanedMap(){
		Map<String, String> cleanedKeys = new HashMap<String, String>();
		if (this.length() > 0){
			for (String key: keySet()){
				if (key.startsWith("OPT ")) cleanedKeys.put(key.replace("OPT ", ""), key);
				else if (key.startsWith("XOR ")) cleanedKeys.put(key.replace("XOR ", ""),key);
				else if (LcString.isAllUpperCase(key)) wildcardKeyname = key;
				else cleanedKeys.put(key, key);
			}
		}
		return cleanedKeys;
	}
	
	public Set<String> getCleanedKeys(){
		return getCleanedMap().keySet();
	}
	
	public Set<String> getReqKeys(){
		Set<String> keys = new HashSet<String>();
		if (this.length() > 0){
			for (String key: keySet()){
				if (key.startsWith("OPT ")) continue;
				if (key.equals("subject")) continue;
				if (key.equals("comment")) continue;
				if (key.startsWith("XOR ")) continue;
				if (LcString.isAllUpperCase(key)) {
					this.wildcardKeyname = key;
					continue;
				}
				keys.add(key);
			}
		}
		return keys;
	}
	
	public boolean hasWildcardKeyname(){
		return this.wildcardKeyname != null;
	}
	
	public String getWildcardKeyname(){
		if (this.wildcardKeyname == null){
			for (String key: JSONObject.getNames(this)){
				if (LcString.isAllUpperCase(key))
					this.wildcardKeyname = key;
			}
		}
		return wildcardKeyname;
	}
	
	public LcMetaJSONObject isValidRequirement(String classname){
		throwErrorIfHasMoreThanOneWildcard(this);
		if (classname == null) classname = "unknown class";
		// TODO: check for valid modifiers in keynames (OPT/XOR)
		for (String key: keySet()){
			if (untypedKeys.contains(key.toLowerCase())) continue;
			Object reqValue = get(key);
			if (reqValue instanceof JSONObject) {
				LcMetaJSONObject reqJSON = new LcMetaJSONObject((JSONObject) reqValue);
				if (reqJSON.has("streaminfo")){
					if (!reqJSON.has("format")) throw new JSONException("streaminfo: missing 'format' key");
					reqJSON.getString("format"); // throws JSONException 'format' is not string
					JSONObject streaminfo = reqJSON.getJSONObject("streaminfo");
					if (!streaminfo.has("checksum")) 
						throw new JSONException("streaminfo: missing 'checksum' key in "+classname);
					if (!streaminfo.has("order")) 
						throw new JSONException("streaminfo: missing 'order' key in "+classname);
					if (!streaminfo.getString("checksum").equalsIgnoreCase("string")) 
						throw new JSONException("streaminfo: checksum must be string typenot "+streaminfo.getString("order")+"' in "+classname);
					if (!streaminfo.getString("order").equalsIgnoreCase("number")) 
						throw new JSONException("streaminfo: order must be number type not "+streaminfo.getString("order")+" in "+classname);
				} else put(key,reqJSON.isValidRequirement(classname));
			}
			if (reqValue instanceof String) {
				if (LcString.isAllUpperCase((String) reqValue)) continue;
				String reqType = reqValue.toString();
				if (!jsontypes.contains(reqType)) throw new JSONException("Unknown type '"+reqType+"' in "+classname);
				//put(key, reqType);
			}
			if (reqValue instanceof JSONArray){
				JSONArray reqArray = (JSONArray) reqValue;
				for (int i = 0; i < reqArray.length(); i++){
					Object iObj = reqArray.get(i);
					if (iObj instanceof JSONObject) {
						new LcMetaJSONObject((JSONObject)iObj).isValidRequirement(classname);
					} else if (iObj instanceof Number){
						throw new JSONException("A JSONArray in a requirement definition cannot contain a number; "+classname);
					} else if (iObj instanceof Boolean){
						throw new JSONException("A JSONArray in a requirement definition cannot contain a boolean; "+classname);
					}
				}
			}
		}
		return this;
	}
	
	public static JSONObject throwErrorIfHasMoreThanOneWildcard(JSONObject json) throws JSONException{
		boolean hasWildcard = false;
		// JSONObject.getNames(json) return null if json.lenght() == 0
		if (json.length() > 0){
			@SuppressWarnings("unchecked")
			Set<String> keySet = json.keySet();
			for (String key: keySet){
				boolean isWildcard = LcString.isAllUpperCase(key);
				if (hasWildcard && isWildcard)
					throw new JSONException("Only one wildcard-keyname per JSON-Object allowed.");
				Object value = json.get(key);
				if (value instanceof JSONObject)
					throwErrorIfHasMoreThanOneWildcard((JSONObject) value);
			}
		}
		return json;
	}
	
	public boolean isValidInput(JSONObject testee, Set<String> unknowns, String classname) throws JSONException{
//		LcMetaJSONObject requirements = this;
		if (this.length() == 0) return true;

		// XOR check
		int n = this.countXorKeysFrom(testee);
		if (n > 1){
			throw new JSONException("Multiple exclusive keys detected: Only one of the keys '"
					+ this.getXorKeys().toString()+"' allowed at a time.");
		}
		if ((n == 0) && (this.getXorKeys().size() > 0) ){
			throw new JSONException("At least one of the keys '"
					+ this.getXorKeys().toString()+"' is required.");
		}
		
		// required keys check
		Set<String> reqKeys = this.getReqKeys();
		for (String r: reqKeys){
			if (!has(r)) throw new JSONException("Key '"+r+"' required in "+classname);
		}
		
		if (testee.length() > 0){
			Map<String, String> knownCleanKeys = this.getCleanedMap();
			@SuppressWarnings("unchecked")
			Set<String> keys = testee.keySet();
			for (String key: keys){
				// if key is action or error or alike do not check type
				if (untypedKeys.contains(key.toLowerCase())) continue;
				// inputs and outputs are keys appearing in actions calling services the in and outputs of which gets checked separately
				if (key.toLowerCase().equals("inputs") || key.toLowerCase().equals("outputs")) continue;
				
				// now do the check
				if (knownCleanKeys.keySet().contains(key) || this.hasWildcardKeyname()){
					
					Object subValueRequired = null;
					Set<String> requiredTypes = new HashSet<String>();
					if (knownCleanKeys.keySet().contains(key)) subValueRequired = this.get(knownCleanKeys.get(key));
					else if (this.hasWildcardKeyname()) subValueRequired = this.get(this.getWildcardKeyname());
					if (subValueRequired instanceof String) requiredTypes.add((String) subValueRequired);
					else if (subValueRequired instanceof JSONArray) {
						JSONArray subValueRequiredTypes = (JSONArray) subValueRequired;
						try{
							/* A list holding options / strings other than type names makes this 
							 * validation method check for options instead of types.
							 * Abstract options must be written in CAPITAL letters; e.g. [ZMIN, ZMAX]
							 * Lists with dimension higher than 1 don't get checked: [[],[]] results 
							 * in requiredTypes = ["list"] since the getString() method will raise 
							 * JSONException. 
							 */
							for (int i = 0; i < subValueRequiredTypes.length(); i++){
								requiredTypes.add(subValueRequiredTypes.getString(i));
							}
						} catch (JSONException e){
							requiredTypes.add("list");
						}
					} else if (subValueRequired instanceof JSONObject){
						JSONObject subJSON = (JSONObject) subValueRequired;
						if (subJSON.has("streaminfo")) requiredTypes.add("streaminfo");
						if (subJSON.has("geometry")) requiredTypes.add("geometry");
						if (!subJSON.has("streaminfo") && !subJSON.has("geometry")) requiredTypes.add("json");
					}

					Object subValue = testee.get(key);
					/* json (and alike) types */
					if (subValue instanceof JSONObject){
						String error = "'"+key+"' is required to be of type(s) "+requiredTypes+" not 'json'";
						JSONObject subJSON = (JSONObject) subValue;
						if (requiredTypes.contains("streaminfo")){
							/* the value of format can be anything; doesn't get tested here */
							if (!subJSON.has("format") || !subJSON.has("streaminfo"))
								error = "'"+key+"' is required to be a json object holding 'format' and 'stream' keys";
							else if (!subJSON.getJSONObject("streaminfo").has("checksum") &&
									!subJSON.getJSONObject("streaminfo").has("order"))
								error = "streaminfo is missing checksum or order";
							else error = null;
						} 
						if (requiredTypes.contains("geometry") && error != null){
							if (!subJSON.has("format") || !subJSON.has("geometry"))
								error = "'"+key+"' is required to be a json object holding 'format' and 'geometry' keys";
							else error = null;
						}
						if (key.equalsIgnoreCase("timerange") && requiredTypes.contains("timerange") && error != null){
							new LcMetaJSONObject((JSONObject) subValueRequired)
									.isValidInput(new LcMetaJSONObject(timerange), unknowns, classname);
						}
						
						if (requiredTypes.contains("json") && error != null){
							error = null;
							/* recursive check for required types defined as a json object */
							if (subValueRequired instanceof JSONObject)
								new LcMetaJSONObject((JSONObject) subValueRequired)
									.isValidInput(new LcMetaJSONObject(subJSON), 
											unknowns, classname);
						}
						if (error != null) throw new JSONException(error);
					
					/* list (=jsonarray; and alike) types */
					} else if (subValue instanceof JSONArray){
						JSONArray subJSONArray = (JSONArray) subValue;
						if (requiredTypes.contains("range")){
							if (subJSONArray.length() > 3)
								throw new JSONException("'"+key+"' is required to be of type 'range', a list of a maximum length of 3");
						}
						
						if (!requiredTypes.contains("list")) {
							/* upper case means PLACEHOLDER / wildcard. can be anything */
							boolean valid = false;
							for (String type: requiredTypes) {
								if (LcString.isAllUpperCase(type)){ 
									valid = true;
									break;
								}
							}
							if (valid) continue;
							
							try{
								// json array may be an option list. if so it must not only hold type names, e.g. ["actions", "services", "converters"]
								List<String> subArray = new ArrayList<String>();
								for (int i = 0; i < subJSONArray.length(); i++) subArray.add(subJSONArray.getString(i));
								if (!requiredTypes.containsAll(subArray)){
									subArray.removeAll(requiredTypes);
									throw new IllegalArgumentException("Unknown properties "+subArray+". "+classname+" expects "+requiredTypes);
								}
							} catch (JSONException e){
								throw new JSONException("'"+key+"' is an option list. "+subJSONArray+" does not meet the provided options "+requiredTypes+". ");
							}
							
						}
						
					// string / mqtt
					} else if (subValue instanceof String){
						String error = "'"+key+"' is required to be of type(s) "+requiredTypes+" not 'string' in "+classname;
						String mqtt_pre = Luci.getConfig().getProperty("mqtt_pre", "/Luci/");
						if (!requiredTypes.contains("string") && !((String) subValue).startsWith(mqtt_pre)) 
							throw new JSONException(error);
					
					// number
					} else if (subValue instanceof Number){
						String error = "'"+key+"' is required to be of type(s) "+requiredTypes+" not 'number' in "+classname;
						if (!requiredTypes.contains("number")) throw new JSONException(error);
					
					// bool
					} else if (subValue instanceof Boolean){
						String error = "'"+key+"' is required to be of type(s) "+requiredTypes+" not 'bool' in "+classname;
						if (!requiredTypes.contains("boolean")) throw new JSONException(error);
					}
				
				// unknown keys 
				} else if (!this.hasWildcardKeyname()) 
					unknowns.add(key);
			}
		}
		return true;
	}
	
	
	public boolean fullyEquals(JSONObject jo){
		@SuppressWarnings("unchecked")
		Set<String> keys = jo.keySet();
		for (String key: keys){
			if (jo.has(key)){
				Object o = this.get(key);
				if (o.getClass().equals(jo.get(key).getClass())){
					if (o instanceof JSONObject){
						if (((JSONObject) o).length() > 0){
							LcJSONObject jlo = new LcJSONObject((JSONObject) o);
							if (!jlo.equals(jo.getJSONObject(key))){
								return false;
							}
						}
					} else if(o instanceof JSONArray){
						if (!isSameArray(this.getJSONArray(key), jo.getJSONArray(key))){
							return false;
						}
					} else {
						if (!o.equals(jo.getString(key))){
							return false;
						}
					}
				} else {
					return false;
				}
			} else {
				return false;
			}
		}
		return true;
	}
	
	public static boolean isSameArray(JSONArray a1, JSONArray a2){
		if (a1.length() == a2.length()){
			List<Object> s1 = new ArrayList<Object>();
			List<Object> s2 = new ArrayList<Object>();
			for (int i = 0; i < a1.length(); i++){
				s1.add(a1.get(i));
				s2.add(a2.get(i));
			}
			s1.removeAll(s2);
			if (s1.size() > 0){
				return false;
			}
			return true;
		}
		return false;
	}
}
