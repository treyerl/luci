/**
 * The MIT License (MIT)
 *
 * Copyright (c) <year> <copyright holders>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package org.luci.connect;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CountDownLatch;

import org.json.JSONException;
import org.json.JSONObject;
import org.luci.connect.LcInputStream;

/**
 * 
 * @author Lukas Treyer
 *
 */
public class LcOutputStream extends SynchronizedStream{
	
//	private static String streaminfoWarning = "Don't put any streaminfos into the json, this is "
//			+ "done by LuciConnect for you. Instead put Attachment objects into the "
//			+ "JSONObject. Those are being transmitted for by LuciConnect.";
	private static int order;
	
	/**
	 * @param checksum
	 * @param format
	 * @param name
	 * @param order
	 * @param len
	 * @param crs
	 * @param attributeMap
	 * @param out
	 */
	public LcOutputStream(String checksum, String format, String name, int order,
			int len, String crs, JSONObject attributeMap, OutputStream out) {
		super(checksum, format, name, order, len, crs, attributeMap);
		this.out = out;
	}
	
	public LcOutputStream(LcInputStream i, OutputStream o){
		super(i.getChecksum(), i.format, i.getName(), i.getOrder(), i.getLength(), i.crs, 
				i.getJSON().optJSONObject("attributeMap"));
		this.out = o;
	}
	public LcOutputStream(StreamInfo si){
		super(si);
	}

	public void reset(){
		this.ready = new CountDownLatch(1);
		this.done = new CountDownLatch(1);
	}
	
	private OutputStream out;
	
	public OutputStream getOutputStream(){
		return out;
	}
	
	public void setOutputStream(OutputStream o){
		out = o;
	}

	/* (non-Javadoc)
	 * @see java.io.OutputStream#write(int)
	 */
	public void write(int b) throws IOException {
		if (isReady() && out != null) out.write(b);
		else throw new IllegalStateException("OutputStream not ready! "+((out == null) ? "No OutputStream set!": ""));
	}
	
	 public void write(byte b[]) throws IOException {
	        write(b, 0, b.length);
	    }

    public void write(byte b[], int off, int len) throws IOException {
        if (isReady() && out != null) out.write(b, off, len);
        else throw new IllegalStateException("OutputStream not ready!"+((out == null) ? "No OutputStream set!": ""));
    }

    public void flush() throws IOException {
    	out.flush();
    }

    public void close() throws IOException {
    	out.close();
    }
    
    public void writeAttachmentSize() throws IOException{
    	ByteBuffer bufl = ByteBuffer.allocate(8);
		bufl.putLong(getLength());
		out.write(bufl.array());
	}
    
    public static List<LcOutputStream> prepareForTransmission(JSONObject json){
    	List<LcOutputStream> streams = new ArrayList<LcOutputStream>();
    	order = 1;
    	for(LcOutputStream stream: prepare(json).values()){
    		stream.setOrder(order++);
    		streams.add(stream);
    	}
    	return streams;
    }
    
    private static Map<String, LcOutputStream> prepare(JSONObject json){
    	Map<String, LcOutputStream> streams = new HashMap<String, LcOutputStream>();
    	Map<Integer, String> check = new HashMap<Integer, String>();
    	@SuppressWarnings("unchecked")
		Set<String> keys = json.keySet();
		for (String key: keys){
			Object val = json.get(key);
			if (val instanceof JSONObject){
				JSONObject jval = (JSONObject) val;
				// if the json has stream infos already, check them for consistency
				if (jval.has("format") && jval.has("streaminfo")){
					JSONObject s = jval.getJSONObject("streaminfo");
					String New = s.getString("checksum");
					String old = check.putIfAbsent(s.getInt("order"), New);
					if (!old.equalsIgnoreCase(New)){
						String error = String.format(chck, old, New);
						throw new JSONException(error);
					}
					//throw new RuntimeException(streaminfoWarning);
				} else {
					streams.putAll(prepare(jval));
				}
			} else if (val instanceof LcOutputStream){
				LcOutputStream lval = (LcOutputStream) val;
				json.put(key, lval.getJSON());
				streams.put(lval.getChecksum(), lval);
			} else if (val instanceof LcInputStream){
				throw new RuntimeException("A service's output must not contain LcInputStreams!");
			}
		}
		int length = check.size();
		if (length > 0){
			if (Collections.max(check.keySet()) != length) {
				throw new JSONException("Inconsistent order sequence "+check.keySet());
			}
		}
		
		order = check.size() + 1;
		return streams;
    }	
}
