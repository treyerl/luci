/**
{'action':'run', 'service':{'classname':'Util_Files', 'inputs':{'n2':'resources/testfiles/phj_schneider.png'}}} * The MIT License (MIT)
 *
 * Copyright (c) <year> <copyright holders>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package org.luci.connect;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.Socket;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.json.JSONException;
import org.json.JSONObject;
import org.luci.connect.LcInputStream;

/**
 * 
 * @author Lukas Treyer
 *
 */
public class LcSocket implements ILcSocket {
	Socket s;
	InputStream input;
	PrintStream output;
	LcJSONObject header;
	private long headerLength;
	private long attachmentsLength;

//	private CountDownLatch attachmentsRead;
	/**
	 * @throws IOException 
	 * 
	 */
	public LcSocket(Socket socket) {
		s = socket;
		if (socket != null){
			try {
				input = socket.getInputStream();
				output = new PrintStream(socket.getOutputStream());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	/* (non-Javadoc)
	 * @see org.luci.comm.ILcSocket#send(org.luci.connect.LcJSONObject)
	 */
	@Override
	public String send(JSONObject o) throws InterruptedException, IOException {
		return send(o, LcOutputStream.prepareForTransmission(o));
		
	}
	
	/* (non-Javadoc)
	 * @see org.luci.connect.ILcSocket#send(org.json.JSONObject, java.util.List)
	 */
	@Override
	public String send(JSONObject json, List<LcOutputStream> outputStreams) 
			throws InterruptedException, IOException {
		header = null;
		String string;
		if (json.length() > 0) string = json.toString();
		else string = "{'error':'Empty result string'}";
		byte[] utf8string = string.getBytes("UTF-8");
		long sentHeaderLength = utf8string.length;
//		System.out.println("sending"+ json);
		long attachmentsLength = 0;
		for (LcOutputStream out: outputStreams) attachmentsLength += out.getLength();
		
//		System.out.println("  -  "+sentHeaderLength);
//		System.out.println("  -  "+attachmentsLength);
		
		OutputStream o = s.getOutputStream();
		o.write(ByteBuffer.allocate(8).putLong(sentHeaderLength).array());
		o.write(ByteBuffer.allocate(8).putLong(attachmentsLength).array());
		o.write(utf8string);
//		output.println(string);
		o.flush();
		for (LcOutputStream out: outputStreams){
			out.setOutputStream(o);
//			out.writeAttachmentSize();
			out.setReady();
//			System.out.println("*");
			out.waitUntilDone(10, TimeUnit.SECONDS);
		}
		o.flush();
		return string;
	}
	
	/* (non-Javadoc)
	 * @see org.luci.comm.ILcSocket#println(java.lang.String)
	 */
	@Override
	public void send(String msg) throws IOException {
		byte[] utf8string = msg.getBytes("UTF-8");
		long sentHeaderLength = utf8string.length;
		OutputStream o = s.getOutputStream();
		o.write(ByteBuffer.allocate(8).putLong(sentHeaderLength).array());
		o.write(ByteBuffer.allocate(8).putLong(0).array());
		o.write(utf8string);
		o.flush();
	}
	

	/* (non-Javadoc)
	 * @see org.luci.comm.ILcSocket#getRemoteAddress()
	 */
	@Override
	public SocketAddress getRemoteAddress() {
		return s.getRemoteSocketAddress();
	}

	/* (non-Javadoc)
	 * @see org.luci.comm.ILcSocket#close()
	 */
	@Override
	public void close() throws IOException {
		s.close();
	}

	/* (non-Javadoc)
	 * @see org.luci.comm.ILcSocket#getLocalAddress()
	 */
	@Override
	public SocketAddress getLocalAddress() {
		return s.getLocalSocketAddress();
	}

	/* (non-Javadoc)
	 * @see org.luci.comm.ILcSocket#getMessage()
	 */
	@Override
	public LcJSONObject getMessage(final List<LcInputStream> inputStreams) throws IOException, InterruptedException {
//		String headerString = LcInputStream.readExactlyOneLineUTF8(input);
		try{
			byte[] headerLengthB = new byte[8];
			byte[] attachmentsLengthB = new byte[8];
			for (int i = 0; i < 8; i++) input.read(headerLengthB, i, 1);
			for (int i = 0; i < 8; i++) input.read(attachmentsLengthB, i, 1);
//			System.out.println("header: "+java.util.Arrays.toString(headerLengthB));
//			System.out.println("attachments: "+java.util.Arrays.toString(attachmentsLengthB));
			headerLength = ByteBuffer.wrap(headerLengthB).getLong();
			attachmentsLength = ByteBuffer.wrap(attachmentsLengthB).getLong();
//			System.out.println("header: "+headerLength);
//			System.out.println("attachments: "+attachmentsLength);
			byte[] headerB = new byte[(int) headerLength];
			int len = 0, nRead = 0;
			while(len < headerLength - 1){
				nRead = input.read(headerB, len, (int) (headerLength - len));
				len += nRead;
			}
			String msg = new String(headerB, "UTF-8");
//			System.out.println(msg);
			header = new LcJSONObject(msg);
			for (LcInputStream lis: LcInputStream.recursivelyFindIn(header).values()){
				lis.setInputStream(input);
				inputStreams.add(lis);
			}
//			System.out.println(inputStreams.size());
			// force the input streams to be read sequentially
			new Thread(new Runnable(){
				@Override
				public void run() {
					for (LcInputStream lis: inputStreams){
//						int len = lis.length;
						//long byteLength = readAttachmentSize();
//						System.out.println("LcSocket: "+lis.getName()+" size("+byteLength+") / "+lis.length);
//						if (len != byteLength){
//							throw new RuntimeException(String.format("length does not match: %d(json), "
//									+ "%d(big endian bytes)", len, byteLength));
//						}
						lis.setReady();
						try {
							lis.waitUntilDone(2000, TimeUnit.SECONDS);
						} catch (Exception e) {
							// printed elsewhere
							// maybe add the error also to a final error list handed over to getMessage();
							System.err.println(e);
							e.printStackTrace();
						}
					}
				}}).start();
			return header;
		} catch (JSONException e){
			byte[] headerB = new byte[8196];
			int len = 0, nRead = 0;
			while(len < attachmentsLength){
				nRead = input.read(headerB, len, (int) (attachmentsLength - len));
				len += nRead;
			}
			throw e;
		}
	}
	
	public long readAttachmentSize() {
		try{
			ByteBuffer bufl = ByteBuffer.allocate(8);
			input.read(bufl.array(), 0 , 8);
			return bufl.getLong(0);
		} catch (IOException e){
			throw new RuntimeException(e);
		}
		
	}
}
