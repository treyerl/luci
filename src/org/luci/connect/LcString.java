package org.luci.connect;

import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

public class LcString {
	public static String fill(int length, String with) {
	    StringBuilder sb = new StringBuilder(length);
	    while (sb.length() < length) {
	        sb.append(with);
	    }
	    return sb.toString();
	}
	
	public static String fillQuantity(int quantity, String with) {
	    StringBuilder sb = new StringBuilder();
		for(int i = 0; i < quantity; i++){
	        sb.append(with);
		}
	    return sb.toString();
	}
	
	public static String multiply(int times, String what){
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < times; i++) sb.append(what);
		return sb.toString();
	}
	
	public static String trim(String str, int i){
		return str.substring(i, str.length() - i);
	}
	
	public static boolean isAllUpperCase(String r) {
		boolean is = true;
		for (int i = 0; i < r.length(); i++){
			char ch = r.charAt(i);
			if (Character.isWhitespace(ch))
				continue;
			if (ch == '_') 
				continue;
			if (!Character.isUpperCase(ch))
				is = false;
		}
		return is;
	}
	
	public static String join(List<?> strings, String sep){
		if (strings.size() > 0){
			StringBuilder sb = new StringBuilder(strings.get(0).toString());
			for (int i = 1; i < strings.size(); i++){
				sb.append(sep+strings.get(i).toString());
			}
			return sb.toString();
		}
		return ""; 
	}
	
	public static String getHexFromBytes(byte[] bytes){
		StringBuilder sb = new StringBuilder();
		for (byte b: bytes){
			sb.append(String.format("%X", b));
		}
		return sb.toString();
	}
	
	public static byte[] getByteFromHex(String s) {
		if (s == null) return null;
//		s = s.replaceAll("\\s+","");
		int len = s.length();
		byte[] data = new byte[len / 2];
		for (int i = 0; i < len; i += 2) {
		    data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
		                         + Character.digit(s.charAt(i+1), 16));
		}
		return data;
	}
	
	public static String calcChecksum(ByteBuffer bb) {
		try {
			MessageDigest digest = MessageDigest.getInstance("MD5");
			int buffersize = 8196;
			int capacity = bb.capacity();
			byte[] b = bb.array();
			for(int i = 0; i < capacity; i+=buffersize ){
				digest.update(b, i, Math.min(buffersize, capacity - i));
			}
			return getHexFromBytes(digest.digest());
		} catch (NoSuchAlgorithmException e) {
			// never happens and if so we need to debug anyway
			e.printStackTrace();
			return null;
		}
	}
}
