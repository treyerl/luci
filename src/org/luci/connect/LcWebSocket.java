/**
 * The MIT License (MIT)
 *
 * Copyright (c) <year> <copyright holders>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package org.luci.connect;

import java.io.IOException;
import java.net.SocketAddress;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.CountDownLatch;

import org.eclipse.jetty.websocket.api.RemoteEndpoint;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketClose;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketConnect;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketError;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;
import org.json.JSONObject;
import org.luci.Luci;
import org.luci.comm.clienthandlers.BreakException;
import org.luci.comm.clienthandlers.ClientHandlerObserver;
import org.luci.comm.clienthandlers.WebClientProxy;
import org.luci.factory.observer.FactoryObserver;

/**
 * 
 * @author Lukas Treyer
 * http://jansipke.nl/websocket-tutorial-with-java-server-jetty-and-javascript-client/
 */
//@WebSocket(maxTextMessageSize = 64 * 1024, maxBinaryMessageSize = 64 * 1024, inputBufferSize = 31 * 1024)
@WebSocket
public class LcWebSocket implements ILcSocket{
	public final byte CONTINUATION = 0x00;
	public final byte TEXT = 0x01;
	public final byte BINARY = 0x02;
	public final byte CLOSE = 0x08;
	public final byte PING = 0x09;
	public final byte PONG = 0x0A;

	LcInputStream in = null;
	Session session;
	CountDownLatch headerLatch;
	CountDownLatch messageLatch;
	StringBuilder textBuffer;
	LcJSONObject header;
	WebClientProxy cp;
	
	class HttpHeaderComparator implements Comparator<Map.Entry<String, List<String>>>{

		/* (non-Javadoc)
		 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
		 */
		@Override
		public int compare(Entry<String, List<String>> o1, Entry<String, List<String>> o2) {
			return o1.getKey().compareTo(o2.getKey());
		}
		
	}

	@OnWebSocketError
	public void onError(Session s, Throwable t) throws Throwable {
		cp.getClientHandler().close(t);
		s.close();
	}
	@OnWebSocketConnect
	public void onConnect(Session session) throws IOException{
		this.session = session;
		SortedSet<Map.Entry<String, List<String>>> request = 
				new TreeSet<Map.Entry<String, List<String>>>(new HttpHeaderComparator());
		request.addAll(session.getUpgradeRequest().getHeaders().entrySet());
//		System.out.println(Arrays.toString(request.toArray()));
		SortedSet<Map.Entry<String, List<String>>> response = 
				new TreeSet<Map.Entry<String, List<String>>>(new HttpHeaderComparator());
		response.addAll(session.getUpgradeResponse().getHeaders().entrySet());
//		System.out.println(Arrays.toString(response.toArray()));
		
		session.getPolicy().setMaxTextMessageSize(256*1024);
		
		FactoryObserver o = Luci.getLuciInstance().getObserver();
		ClientHandlerObserver co = Luci.getLuciInstance().getClientHandlerObserver();
		cp = (WebClientProxy) co.run(new WebClientProxy(this, o, co));
//		System.err.println(co.getThreadPool());
		session.setIdleTimeout(Luci.WSockTimeoutInMillis);
		send(cp.getID()+"");
		if (headerLatch == null) headerLatch = new CountDownLatch(1);
	}
	
	@OnWebSocketMessage
	public void onTextMessage(String s) {
		header = new LcJSONObject(s);
		headerLatch.countDown();
//		System.out.println("onTextMessage(String s): s="+s);
	}

	@OnWebSocketClose
	public void onClose(Session session, int statusCode, String reason) throws BreakException {
		System.out.printf("onClose(%d, %s)%n",statusCode,reason);
		session.close();
		headerLatch.countDown();
	}

	/* (non-Javadoc)
	 * @see org.luci.comm.ILcSocket#println()
	 */
	@Override
	public void send(String msg) throws IOException {
		header = null;
		RemoteEndpoint remote = session.getRemote();
		remote.sendString(msg);
	}
	

	/* (non-Javadoc)
	 * @see org.luci.comm.ILcSocket#getRemoteAddress()
	 */
	@Override
	public SocketAddress getRemoteAddress() {
		return session.getRemoteAddress();
	}
	/* (non-Javadoc)
	 * @see org.luci.comm.ILcSocket#close()
	 */
	@Override
	public void close() {
		session.close();
	}
	/* (non-Javadoc)
	 * @see org.luci.comm.ILcSocket#getLocalAddress()
	 */
	@Override
	public SocketAddress getLocalAddress() {
		return session.getLocalAddress();
	}

	/* (non-Javadoc)
	 * @see org.luci.comm.ILcSocket#getMessage()
	 */
	@Override
	public LcJSONObject getMessage(List<LcInputStream> attachments) 
			throws IOException, InterruptedException {
//		System.out.println("getMessage()");
		if (headerLatch == null) headerLatch = new CountDownLatch(1);
		headerLatch.await();
		for (LcInputStream lis : LcInputStream.recursivelyFindIn(header).values()){
			attachments.add(lis);
			cp.putUploadStream(lis);
		}
		headerLatch = new CountDownLatch(1);
		return header;
	}
	
	/* (non-Javadoc)
	 * @see org.luci.connect.ILcSocket#send(org.luci.connect.LcJSONObject)
	 */
	@Override
	public String send(JSONObject o) throws InterruptedException, IOException {
		return send(o, LcOutputStream.prepareForTransmission(o));
	}
	
	/* (non-Javadoc)
	 * @see org.luci.connect.ILcSocket#send(org.luci.connect.LcJSONObject)
	 */
	@Override
	public String send(JSONObject o, List<LcOutputStream> outputStreams) 
			throws  InterruptedException, IOException {
//		System.out.println("send");
		header = null;
		String string;
		if (o.length() > 0) string = o.toString();
		else string = "{'error':'Empty result string'}";
		for (LcOutputStream out: outputStreams){
			cp.putDownloadStream(out);
		}
		RemoteEndpoint remote = session.getRemote();
		remote.sendString(string);
		return string;
		
	}
}