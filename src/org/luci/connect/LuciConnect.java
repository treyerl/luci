/**
 * The MIT License (MIT)
 *
 * Copyright (c) <year> <copyright holders>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

package org.luci.connect;
import java.io.IOException;
import java.io.PrintStream;
import java.math.BigDecimal;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * This class is intended as a utility to facilitate development of clients that need to connect 
 * to Luci. It handles 
 * - connect/disconnect
 * - assembly of blocking and unblocking action calls incl. translation of binary files
 *   into streaminfo json objects and subsequent transmission of the bytearray. Subsequent = as 
 *   specified in the Luci JSON Protocol Specification {link} the JSON string can be followed by
 *   binary streams the length of which is denoted by a 8bytes long BIG endian number.
 * - processing of incoming action answers i.e. reading binary streams and making them available
 *   in the member Map<String, List<byte[]>> inputStreams. Binary streams are stored as a list of 
 *   byte arrays which corresponds to the buffer size. The reason for this is that the stream
 *   reader code of Luci server is used, which allows for overriding the stream reading method
 *   by converter plugins. On server side this allows enables converters to store binary data in 
 *   customized chunks (e.g. dbf records, geometry of a shapefile etc.)
 * 
 * @author Lukas Treyer
 *
 */
public class LuciConnect {
	class Receiver implements Runnable{
		List<IReceive> listeners;
		List<IReceive> permanentListeners;
		Receiver(){ reset(); }
		
		private void reset(){
			listeners = new ArrayList<IReceive>();
			permanentListeners = new ArrayList<IReceive>();
		}
		
		@Override
		public void run() {
			while(true){
				try {
					List<LcInputStream> attachments = new ArrayList<LcInputStream>(); 
					LcJSONObject lcj = lcsocket.getMessage(attachments);
					duration = System.nanoTime() - start;
					LuciResponse response = new LuciResponse(lcj, duration, attachments);
					boolean shouldBreak = false;
					if (response != null){
						synchronized(listeners){
							for (IReceive r: listeners) 
								if (!r.receive(response)) shouldBreak = true;
							listeners = new ArrayList<IReceive>();
						}
						synchronized(permanentListeners){
							for (IReceive r: permanentListeners) 
								if (!r.receive(response)) shouldBreak = true;
						}
					}
					if (shouldBreak || lcj == null){
						if (!disconnectOnPurpose) print(connectionFailure());
						for (IReceive r: onDisconnect) r.receive(response);
						break;
					}					
				} catch (IOException e) {
					if (!disconnectOnPurpose) print(connectionFailure());
					for (IReceive r: onDisconnect) r.receive(null);
//					e.printStackTrace();
					break;
				} catch (JSONException e) {
					e.printStackTrace();
					break;
				} catch (InterruptedException e) {
					e.printStackTrace();
				} 
			}
		}
	}
	public final static String authenticateACTION = "{'action':'authenticate', 'username':'%s', 'userpasswd':'%s'";
	public final static String getParametersACTION = "{'action':'get_infos_about', 'actionname':'%s'}";

	protected String sent;
	protected LcSocket lcsocket;
	private PrintStream print;
	private boolean connected;
	long start;
	long duration;
	private Receiver recv;
	List<IReceive> onDisconnect;
	boolean disconnectOnPurpose = false;
	
	
	// Java 1.8 (comment if exporting for Java 1.6): 
	//@FunctionalInterface
	public interface IReceive {
		public boolean receive(LuciResponse r);
	}
	
	public LuciConnect(){
		onDisconnect = new ArrayList<IReceive>();
	}
	
	public String getSentMessage(){
		return sent;
	}
	
	protected void print(LuciResponse r){
		System.out.println(r.print());
		System.out.println();
	}
	
	/**
	 * @param address
	 * 		A string holding the IP address or URL
	 * @param port
	 * 		Integer
	 * @param millisecondsTimeout
	 * 		Integer
	 * @return
	 * 		A message string that could be used as a confirmation display. 
	 * @throws IOException
	 */
	public LuciResponse connect(String address, int port, int millisecondsTimeout) throws IOException {		
        Socket socket = new Socket();
        long start = System.currentTimeMillis();
        socket.connect(new InetSocketAddress(address,port), millisecondsTimeout);
        if (!socket.isConnected()) {
        	LuciResponse r = LuciResponse.Error("Server connection timeout ("+millisecondsTimeout+")", 
        			millisecondsTimeout);
        	socket.close();
        	return r;
        }
        lcsocket = new LcSocket(socket);
		connected = true;
		disconnectOnPurpose = false;
		recv = new Receiver();
		new Thread(recv, Receiver.class.getName()).start();
		return new LuciResponse(new JSONObject().put("result","connected to "+address+":"+port), 
				System.currentTimeMillis() - start);
	}
	
	
	public LuciResponse disconnect() {
		connected = false;
		disconnectOnPurpose = true;
		try {
			print.close();
			lcsocket.close();
		} catch (Exception e) {
			return LuciResponse.Error(e.toString(), 0);
		}
	    return new LuciResponse(new JSONObject().put("result", "disconnected"), 0);
	}
	
	public LuciResponse connectionFailure(){
		disconnect();
		return LuciResponse.Error("Connection lost", 0);
	}
	
	
	/**
	 * @return
	 * 		boolean indicating the state of LuciConnect (not the socket).
	 */
	public boolean isConnected(){
		return connected;
	}
	
	
	/**
	 * @return
	 * 		Nanoseconds that denote the time for sending a request and receiving an answer
	 * 		excluding pre- and postprocessing.
	 */
	public long getCallDuration(){
		return duration;
	}
	
	/**
	 * @return
	 * 		Time needed for sending a request and receiving an answer excl. pre- and postprocessing.
	 */
	public BigDecimal getCallDurationInSeconds(){
		return BigDecimal.valueOf((double)duration/1000000000).setScale(3, BigDecimal.ROUND_HALF_UP);
	}
	
	/**
	 * @return
	 * 		Time needed for sending a request and receiving an answer excl. pre- and postprocessing.
	 */
	public BigDecimal getCallDurationInMilliSeconds(){
		return BigDecimal.valueOf((double)duration/1000000).setScale(3, BigDecimal.ROUND_HALF_UP);
	}
	
	
	public void onReceive(IReceive r){
		if (connected){
			synchronized(recv.listeners){
				recv.listeners.add(r);
			}
		} else throw new IllegalStateException("Not connected! ResponseListeners / "
				+ "Receivers can only be registered when connected.");
		
	}
	
	public void onAllReceive(IReceive r){
		if (connected){
			synchronized(recv.permanentListeners){
				recv.permanentListeners.add(r);
			}
		} else throw new IllegalStateException("Not connected! ResponseListeners / "
				+ "Receivers can only be registered when connected.");
	}
	
	public void onDisconnect(IReceive r){
		synchronized(onDisconnect){
			onDisconnect.add(r);
		}
	}
	
	public boolean removeReceiver(IReceive r){
		if (connected){
			synchronized(recv.listeners){
				return recv.listeners.remove(r);
			}
		} else throw new IllegalStateException("Not connected! No registered receivers");
	}
	
	public boolean removePermanentReceiver(IReceive r){
		if (connected){
			synchronized(recv.permanentListeners){
				return recv.permanentListeners.remove(r);
			}
		} else throw new IllegalStateException("Not connected! No registered receivers");
	}
	
	public boolean removeConnectionFailureReceiver(IReceive r){
		synchronized(onDisconnect){
			return onDisconnect.remove(r);
		}
	}
	
	/**
	 * Prints a string to the socket.output using a PrintStream writer. Any output streams detected
	 * during preprocessing will be sent as a byte array. 
	 * @param s
	 * 		String to be sent.
	 * @throws IOException
	 */
	public void send(String s) throws IOException{
		if (!connected) throw new IllegalStateException("Not connected!");
		start = System.nanoTime();
		sent = s;
		lcsocket.send(s);
	}
	
	protected LuciResponse sendAndReceive(String send) throws IOException, InterruptedException{
		final CountDownLatch cdl = new CountDownLatch(1);
		final LuciResponse[] container = new LuciResponse[1];
		onReceive(new IReceive(){
			@Override
			public boolean receive(LuciResponse r) {
				container[0] = r;
				cdl.countDown();
				return true;
			}});
		send(send);
		cdl.await();
		return container[0];
	}
	
	public void send(LcJSONObject send) throws IOException, InterruptedException{
		if (!connected) throw new IllegalStateException("Not connected!");
		start = System.nanoTime();
		sent = lcsocket.send(send);
	}
	
	protected LuciResponse sendAndReceive(LcJSONObject send) throws IOException, InterruptedException{
		final CountDownLatch cdl = new CountDownLatch(1);
		final LuciResponse[] container = new LuciResponse[1];
		onReceive(new IReceive(){
			@Override
			public boolean receive(LuciResponse r) {
				container[0] = r;
				cdl.countDown();
				return true;
			}});
		send(send);
		cdl.await();
		return container[0];
	}
	
	public List<String> availableActions() throws IOException, InterruptedException{
		sent = "{'action':'get_list','of':['actions']}";
		LuciResponse result = sendAndReceive(sent);
		if (result.isOk){
			JSONArray actions = result.json.getJSONArray("actions");
			List<String> a = new ArrayList<String>();
			for (int i = 0; i < actions.length(); i++) a.add(actions.getString(i));
			return a;
		}
		throw new RuntimeException(result.print());
	}
	
	public Map<String, List<String>> getPluginsList() throws IOException, InterruptedException{
		sent = "{'action':'get_list','of':['actions','services','converters']}";
		LuciResponse result = sendAndReceive(sent);
		if (result.isOk){
			Map<String, List<String>> plugins = new HashMap<String, List<String>>();
			JSONArray actions = result.json.getJSONArray("actions");
			List<String> a = new ArrayList<String>();
			for (int i = 0; i < actions.length(); i++) a.add(actions.getString(i));
			plugins.put("actions", a);
			JSONArray services = result.json.getJSONArray("services");
			List<String> s = new ArrayList<String>();
			for (int i = 0; i < services.length(); i++) s.add(services.getString(i));
			plugins.put("services", s);
			JSONArray converters = result.json.getJSONArray("converters");
			List<String> c = new ArrayList<String>();
			for (int i = 0; i < converters.length(); i++) c.add(converters.getString(i));
			plugins.put("converters", c);
			return plugins;
		}
		throw new RuntimeException(result.print());
	}
	
}
