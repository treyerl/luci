package org.luci.connect;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;

import org.json.JSONObject;

public class LuciRemoteService extends LuciConnect{
	private String servicename;
	private String machinename;
	private String description;
	private String version;
	private JSONObject tellInputs;
	private JSONObject tellOutputs;
	private CountDownLatch wait;
	
	// valid for one task
	protected long input_hashcode;
	protected long SObjID;
	protected long ScID;
	protected JSONObject inputs;
	
	public LuciRemoteService(String servicename, JSONObject inputs, JSONObject outputs, 
			String machinename, String description, String version) {
		this.servicename = servicename;
		this.machinename = machinename;
		this.description = description;
		this.version = version;
		this.tellInputs = inputs;
		this.tellOutputs = outputs;
		input_hashcode = 0;
		SObjID = 0;
		ScID = 0;
	}
	
	public JSONObject getInputs(){
		return inputs;
	}
	
	public long getInput_hashcode() {
		return input_hashcode;
	}

	public long getSObjID() {
		return SObjID;
	}

	public long getScID() {
		return ScID;
	}
	
	public boolean register() throws IOException, InterruptedException{
		JSONObject service = new JSONObject();
		service.put("classname", servicename);
		service.put("description", description);
		service.put("version", version);
		service.put("inputs", tellInputs);
		service.put("outputs", tellOutputs);
		LcJSONObject request = new LcJSONObject();
		request.put("action", "remote_register");
		request.put("machinename", machinename);
		request.put("service", service);
		LuciResponse response = sendAndReceive(request);
		if (response.isOk) onAllReceive(new IReceive(){
			@Override
			public boolean receive(LuciResponse r) {
				if (r.json.getString("action").equalsIgnoreCase("run")){
					input_hashcode = r.json.getLong("input_hash");
					inputs = r.json.optJSONObject("inputs");
					SObjID = r.json.getLong("SObjID");
					ScID = r.json.getLong("ScID");
					if (wait != null) wait.countDown();
					doTask();
					return true;
				} else return false;
			}});
		return response.isOk;
	}
	
	/** override this method if you wish to create a remote service by inheritance
	 */
	protected void doTask(){}
	
	public void waitForTask() throws InterruptedException{
		wait = new CountDownLatch(1);
		wait.await();
	}
}
