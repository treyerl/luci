/**
 * The MIT License (MIT)
 *
 * Copyright (c) <year> <copyright holders>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package org.luci.connect;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.json.JSONObject;
import org.luci.connect.LcInputStream;

public class LuciResponse{
	protected static String STD_OK = "";
	protected static String STD_OK_WITH_ERRORS = "";
	protected static String STD_ERROR = "";
	protected static String STD_NO_ERRORS = "";
	protected static File home = new File(System.getProperty("user.home"), "LuciClientFiles");
	
	public static void configurePrefixes(String[] defaults){
		STD_OK = defaults[0];
		STD_OK_WITH_ERRORS = defaults[1];
		STD_ERROR = defaults[2];
		STD_NO_ERRORS = defaults[3];
	}
	
	public static void configureHome(File file) {
		home = file;
	}
	
	public final JSONObject json;
	public final String message;
	public final boolean isError;
	public final boolean hasErrors;
	public final boolean isOk;
	public final long duration;
	public final List<LcInputStream> inputStreams;
	private ExecutorService pool = Executors.newCachedThreadPool();
	
	public static LuciResponse Error(String error, long dur){
		return new LuciResponse(error, dur);
	}
	
	public LuciResponse(JSONObject r, long dur){
		this(r, dur, null);
	}
	
	public LuciResponse(JSONObject r, long dur, List<LcInputStream> attachments){
		json = r;
		duration = dur;
		if (r.has("result")){
			Object result = r.get("result");
			if (result instanceof JSONObject && ((JSONObject) result).has("errors")){
				message = ((JSONObject) result).get("errors").toString();
				hasErrors = true;
				isError = false;
				isOk = false;
			} else {
				message = "";
				hasErrors = false;
				isError = false;
				isOk = true;
			}
		} else if (r.has("error")){
			hasErrors = false;
			isError = true;
			message = r.getString("error");
			isOk = false;
		} else {
			hasErrors = false;
			isError = true;
			isOk = false;
			message = "No valid json fields!";
		}
		this.inputStreams = attachments;
	}
	private LuciResponse(String error, long dur){
		duration = dur;
		message = error;
		isError = true;
		hasErrors = false;
		isOk = false;
		json = null;
		inputStreams = null;
		home.mkdir();
	}
	
	public void receiveAttachments(){
		for (LcInputStream lis: inputStreams){
			File f = new File(home, lis.getName()+"."+lis.format);
			try	 {
				f.createNewFile();
				FileOutputStream fos = new FileOutputStream(f);
				LcOutputStream los = new LcOutputStream(lis, fos);
				los.setReady();
				try {
					pool.submit(new DirectStreamWriter(lis, los, false, true, true, true)).get();
				} catch (InterruptedException | ExecutionException e) {
					e.printStackTrace();
				}
				lis.getJSON().put("filename", f.getAbsolutePath());
				lis.setRepresentation(f.getAbsolutePath());
			} catch (IOException e1){
				e1.printStackTrace();
				lis.setRepresentation(String.format("storing to %s failed", f.getAbsolutePath()));
			}
		}
		LcInputStream.recursivelyPutInto(json, LcInputStream.getStreamsByChck(inputStreams));
	}
	
	public List<LcInputStream> getInputStreams(){
		return inputStreams;
	}
	
	private String getStandardString(){
		if (isError) return STD_ERROR;
		if (!isError && hasErrors) return STD_OK_WITH_ERRORS;
		return STD_OK;
	}
	
	@Override
	public String toString(){
		return print(0);
	}
	
	public String print(){
		return print(0);
	}
	
	public String print(int i){
		if (json != null && !isError) {
			if (json.get("result") instanceof String) return getStandardString()+json.getString("result");
			else return getStandardString()+json.toString(i);
		} else if (hasErrors()) return getStandardString()+message;
		else return getStandardString()+message;
	}
	
	public String print(String s){
		if (isError) return getStandardString()+s+" "+message;
		return getStandardString()+s;
	}
	
	public String getErrors(){
		if (message == null) return STD_NO_ERRORS;
		else return message;
	}
	
	public boolean hasErrors(){
		return hasErrors;
	}
	
	public boolean isError(){
		return isError;
	}
	public boolean isOK(){
		return !isError && !hasErrors;
	}

	
}