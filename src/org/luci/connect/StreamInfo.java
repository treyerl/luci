/**
 * The MIT License (MIT)
 *
 * Copyright (c) <year> <copyright holders>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package org.luci.connect;

import org.json.JSONObject;

public class StreamInfo extends GeometryInfo{
	private String checksum;
	private int length;
	private int order;
	
	
	
 	/**
 	 * @param checksum
 	 * @param format
 	 * @param name
 	 * @param order
 	 * @param len
 	 * @param crs
 	 * @param attributeMap
 	 */
 	public StreamInfo(String checksum, String format, String name, int order, int len, String crs,
			JSONObject attributeMap) {
 		super(name, format, crs, null, attributeMap);
		this.checksum = checksum;
		this.name = name;
		this.order = order;
		length = len;
		json.put("streaminfo", new JSONObject()
			.put("checksum", checksum)
			.put("order", order)
			.put("length", len));
	}
 	
 	public StreamInfo(String checksum, String format, String name, int order, int len){
 		this(checksum, format, name, order, len, null, null);
 	}
	
 	public StreamInfo(String name, JSONObject j){
 		super(name, j.getString("format"), j.optString("crs", null), null, j.optJSONObject("attributeMap"));
		JSONObject s = j.getJSONObject("streaminfo");
		this.checksum = s.getString("checksum");
		this.order = s.getInt("order");
		this.length = s.getInt("length");
		this.json.put("streaminfo", s);
	}
 	
 	public StreamInfo(StreamInfo si){
 		super(si.getName(), si.format, si.crs, null, si.attributeMap);
 		this.length = si.getLength();
 		this.checksum = si.getChecksum();
 		this.order = si.getOrder();
 		json.put("streaminfo", si.getJSON().getJSONObject("streaminfo"));
 	}
 	
 	public int getOrder(){
 		return order;
 	}
 	
 	public StreamInfo setOrder(int o){
 		order = o;
 		json.getJSONObject("streaminfo").put("order", o);
 		return this;
 	}
 	
 	public int getLength(){
 		return length;
 	}

 	protected void setLength(int l){
 		length = l;
 		json.getJSONObject("streaminfo").put("length", l);
 	}
 	
 	public String getChecksum(){
 		return checksum;
 	}
 	
 	void setChecksum(String checksum){
 		this.checksum = checksum;
 		json.getJSONObject("streaminfo").put("checksum", checksum);
 	}
 	
}