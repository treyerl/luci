/**
 * The MIT License (MIT)
 *
 * Copyright (c) <year> <copyright holders>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package org.luci.connect;

import java.util.concurrent.Callable;

import org.luci.connect.LcInputStream;

/**
 * 
 * @author Lukas Treyer
 *
 */
public abstract class StreamWriter implements Callable<Void>{
	protected final LcInputStream in;
	protected final LcOutputStream out;
	protected boolean shouldCloseIn;
	protected boolean shouldCloseOut;
	protected boolean clck;
	protected boolean chck;
	
	public StreamWriter(LcInputStream in, LcOutputStream out, boolean closeIn, boolean closeOut,
			boolean calculateChecksum, boolean verifyChecksum) {
		this.in = in;
		this.out = out;
		shouldCloseIn = closeIn;
		shouldCloseOut = closeOut;
		clck = calculateChecksum;
		chck = verifyChecksum;
		out.setStreamWriter(this);
		in.setStreamWriter(this);
	}
	
	public StreamWriter(LcInputStream in, LcOutputStream out) {
		this(in, out, false, false, false, false);
	}
	
	public LcInputStream getLcInputStream(){
		return in;
	}
	
	public LcOutputStream getLcOutputStream(){
		return out;
	}
}
