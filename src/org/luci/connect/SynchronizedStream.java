/**
 * The MIT License (MIT)
 *
 * Copyright (c) <year> <copyright holders>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package org.luci.connect;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.json.JSONObject;

/**
 * 
 * @author Lukas Treyer
 *
 */
public class SynchronizedStream extends StreamInfo {
	protected static String chck = "'%s' and '%s' share the same order but differ in checksums!";
	protected CountDownLatch ready;
	protected CountDownLatch done;
	private RuntimeException e;
	private StreamWriter streamWriter;
	
	
	/**
	 * @param checksum String
	 * @param format String
	 * @param name String
	 * @param order int
	 * @param len int
	 * @param crs String
	 * @param attributeMap JSONObject
	 */
	public SynchronizedStream(String checksum, String format, String name,
			int order, int len, String crs, JSONObject attributeMap) {
		super(checksum, format, name, order, len, crs, attributeMap);
		this.ready = new CountDownLatch(1);
		this.done = new CountDownLatch(1);
	}
	
	public SynchronizedStream(StreamInfo si){
		super(si);
		this.ready = new CountDownLatch(1);
		this.done = new CountDownLatch(1);
	}

	/**
	 * @param name
	 * @param j
	 */
	public SynchronizedStream(String name, JSONObject j) {
		super(name, j);
		this.ready = new CountDownLatch(1);
		this.done = new CountDownLatch(1);
	}
	
	public void setStreamWriter(StreamWriter w){
		streamWriter = w;
	}
	
	public boolean hasWriter(){
		return streamWriter != null;
	}
	
	public StreamWriter getWriter(){
		return streamWriter;
	}
	
	public boolean waitUntilReady(long t, TimeUnit unit) throws InterruptedException{
		return ready.await(t, unit);
	}
	
	public boolean waitUntilDone(long t, TimeUnit unit) throws InterruptedException{
		boolean timeout = done.await(t, unit);
		if (e != null) throw e;
		return timeout;
	}
	
	public boolean isReady(){
		return ready.getCount() == 0;
	}
	
	public boolean isDone(){
		return done.getCount() == 0;
	}
	
	
	public void setReady() {
//		StackTraceElement[] stack = Thread.currentThread().getStackTrace();
//		StackTraceElement[] stack2  = new StackTraceElement[]{stack[2], stack[3]};
//		System.out.println(getClass().getSimpleName()+this.hashCode()+" setReady() "+java.util.Arrays.toString(stack2));
		ready.countDown();
	}
	
	public void setDone(){
//		StackTraceElement[] stack = Thread.currentThread().getStackTrace();
//		StackTraceElement[] stack2  = new StackTraceElement[]{stack[2], stack[3]};
//		System.out.println(getClass().getSimpleName()+this.hashCode()+" setDone() from "+java.util.Arrays.toString(stack2));
		done.countDown();
	}
	
	public void setDone(RuntimeException e){
		this.e = e;
		done.countDown();
	}

}
