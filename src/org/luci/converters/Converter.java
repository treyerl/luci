package org.luci.converters;

import org.luci.Luci;
import org.luci.factory.observer.FactoryObserver;
import org.slf4j.Logger;

public class Converter{
	protected FactoryObserver o;
	protected Logger log;
	
	public Converter(FactoryObserver o) {
		this.o = o;
		log = Luci.createLoggerFor(getClass().getSimpleName());
	}
}
