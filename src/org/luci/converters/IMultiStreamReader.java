package org.luci.converters;

import java.util.Set;

public interface IMultiStreamReader extends IStreamReader{
	public Set<String> fileFormatSuffixes();
	public String getMainFormatSuffix();
}
