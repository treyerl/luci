package org.luci.converters;

public interface IVariableFormatSuffix {
	public abstract String[] getSuffixVariants();
}
