/**
 * The MIT License (MIT)
 *
 * Copyright (c) <year> <copyright holders>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package org.luci.converters;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.luci.Luci;
import org.luci.converters.geometry.IGeoJSONReader;

/**
 * 
 * @author treyerl
 *
 */
public class JSON2SQL {

	/**Follows the convention to set Format, Text, Number, Geometry, Output Subscription, Checksum
	 * in this very order.
	 * So the prepared statement should name those columns in the here mention order as well.
	 * @param ps
	 * @param value
	 * @param i
	 * @param g
	 * @return
	 * @throws SQLException
	 * @throws JSONException
	 */
	public static int ServiceInput(PreparedStatement ps, String keyname, Object value, int i, 
			IGeoJSONReader g) throws SQLException {
		if (value instanceof String){
			String mqtt_pre = Luci.getConfig().getProperty("mqtt_pre", "/Luci/");
			if (((String) value).startsWith(mqtt_pre)){
				ps.setString(i++, keyname);
				ps.setString(i++, value.toString());
				ps.setNull(i++, Types.NUMERIC);
				ps.setNull(i++, Types.OTHER);
				ps.setString(i++, value.toString());
				ps.setNull(i++, Types.VARCHAR);
			} else {
				ps.setString(i++, "string");
				ps.setString(i++, value.toString());
				ps.setNull(i++, Types.NUMERIC);
				ps.setNull(i++, Types.OTHER);
				ps.setNull(i++, Types.VARCHAR);
				ps.setNull(i++, Types.VARCHAR);
			}
		} else if (value instanceof JSONObject){
			JSONObject json = (JSONObject) value;
			if (json.has("geometry") && g != null){
				ps.setString(i++, "geometry");
				ps.setNull(i++, Types.VARCHAR);
				ps.setNull(i++, Types.NUMERIC);
				ps.setObject(i++, g.geometryToDb(json.getJSONObject("geometry")));
				ps.setNull(i++, Types.VARCHAR);
				ps.setNull(i++, Types.VARCHAR);
			} else if (json.has("streaminfo")) {
				JSONObject streaminfo = json.getJSONObject("streaminfo");
				ps.setString(i++, json.getString("format"));
				ps.setNull(i++, Types.VARCHAR);
				ps.setNull(i++, Types.NUMERIC);
				ps.setNull(i++, Types.OTHER);
				ps.setNull(i++, Types.VARCHAR);
				ps.setString(i++, streaminfo.getString("checksum"));
			} else {
				ps.setString(i++, "json");
				ps.setString(i++, json.toString());
				ps.setNull(i++, Types.NUMERIC);
				ps.setNull(i++, Types.OTHER);
				ps.setNull(i++, Types.VARCHAR);
				ps.setNull(i++, Types.VARCHAR);
			}
		} else if (value instanceof JSONArray){
			ps.setString(i++, "list");
			ps.setString(i++, value.toString());
			ps.setNull(i++, Types.NUMERIC);
			ps.setNull(i++, Types.OTHER);
			ps.setNull(i++, Types.VARCHAR);
			ps.setNull(i++, Types.VARCHAR);
		} else if (value instanceof Number){
			ps.setString(i++, "number");
			ps.setNull(i++, Types.VARCHAR);
			ps.setBigDecimal(i++, new BigDecimal(value.toString()));
			ps.setNull(i++, Types.OTHER);
			ps.setNull(i++, Types.VARCHAR);
			ps.setNull(i++, Types.VARCHAR);
		} else if (value instanceof Boolean){
			ps.setString(i++, "boolean");
			ps.setNull(i++, Types.VARCHAR);
			ps.setBigDecimal(i++, new BigDecimal(boolean2Int((boolean) value)));
			ps.setNull(i++, Types.OTHER);
			ps.setNull(i++, Types.VARCHAR);
			ps.setNull(i++, Types.VARCHAR);
		}
		return i;
	}
	
	/**sets  {@link format,text,number,checksum}  based on passed Object value.
	 * @param ps PreparedStatement
	 * @param value Object to store in SQL
	 * @param i int; the current ps-index after the SQL values have been set
	 * @return
	 * @throws SQLException
	 * @throws JSONException
	 */
	public static int ServiceOutputOrGeometryAttribute(PreparedStatement ps, Object value, int i) 
			throws SQLException, JSONException {
		if (value instanceof String){
			ps.setString(i++, "string");
			ps.setString(i++, value.toString());
			ps.setNull(i++, Types.NUMERIC);
			ps.setNull(i++, Types.VARCHAR);
		} else if (value instanceof JSONObject){
			JSONObject json = (JSONObject) value;
			if (json.has("streaminfo")){
				ps.setString(i++, json.getString("format"));
				ps.setNull(i++, Types.VARCHAR);
				ps.setNull(i++, Types.NUMERIC);
				ps.setString(i++, json.getJSONObject("streaminfo").getString("checksum"));
			} else {
				ps.setString(i++, "json");
				ps.setString(i++, json.toString());
				ps.setNull(i++, Types.NUMERIC);
				ps.setNull(i++, Types.VARCHAR);
			}
		} else if (value instanceof JSONArray){
			ps.setString(i++, "list");
			ps.setString(i++, value.toString());
			ps.setNull(i++, Types.NUMERIC);
			ps.setNull(i++, Types.VARCHAR);
		} else if (value instanceof Number){
			ps.setString(i++, "number");
			ps.setNull(i++, Types.VARCHAR);
			ps.setBigDecimal(i++, new BigDecimal(value.toString()));
			ps.setNull(i++, Types.VARCHAR);
		} else if (value instanceof Boolean){
			ps.setString(i++, "boolean");
			ps.setNull(i++, Types.VARCHAR);
			ps.setBigDecimal(i++, new BigDecimal(boolean2Int((boolean) value)));
			ps.setNull(i++, Types.VARCHAR);
		}
		return i;
	}
	
	public static int updateSubscribedField(PreparedStatement ps, Object value, String format, int i)
			throws SQLException, JSONException {
		switch(format){
		case "number":
			if (value instanceof String || value instanceof Number){
				ps.setNull(i++, Types.VARCHAR);
				ps.setBigDecimal(i++, new BigDecimal(value.toString()));
			} else {
				throw new IllegalArgumentException(value.getClass().getName()+" cannot be converted to number");
			}
			break;
		default:
			ps.setString(i++, value.toString());
			ps.setNull(i++, Types.NUMERIC);
		}
		return i;
	}
	
	private static int boolean2Int(boolean b){
		if (b) return 1;
		return 0;
	}
	

}
