/**
 * The MIT License (MIT)
 *
 * Copyright (c) <year> <copyright holders>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package org.luci.converters;

import java.math.BigDecimal;

import org.json.JSONArray;
import org.json.JSONObject;
import org.luci.connect.LcInputStream;
import org.luci.connect.LcMetaJSONObject;
import org.luci.connect.StreamInfo;

/**
 * 
 * @author treyerl
 *
 */
public class SQL2JSON {

	public static Object toJSONValue(String format, String text, BigDecimal number, StreamInfo a){
		return toJSONValue(format, text, number, a, null);
	}
	
	public static Object toJSONValue(String format, String text, BigDecimal number, StreamInfo si, 
			JSONObject geometry){
		if (si != null){
			if (si instanceof LcInputStream) return si;
			return si.getJSON();
		} else if (geometry != null){
			return new JSONObject().put("format", "GeoJSON").put("geometry", geometry);
		} else if (text != null){
			if (format.equals("list")) return new JSONArray(text);
			else if (format.equals("json")) return new JSONObject(text);
			return text;
		} else {
			if (format.equals("boolean")) return int2Boolean(number.intValue());
			return number;
		}
	}
	
	public static Object convert(Object value, String to){
		if (LcMetaJSONObject.jsontypes.contains(to)){
			if (to.equals("string")) return value.toString();
			if (to.equals("number")) {
				try{
					return new BigDecimal((String) value);
				} catch (NumberFormatException e){
					return value;
				}
			}
			if (to.equals("list")){
				JSONArray list = new JSONArray();
				list.put(value);
			}
			if (to.equals("json")){
				// TODO: LcJSONValidator: refine the to JSON object conversion 
				return new JSONObject().put("value", value);
			}
			if (to.equals("boolean")){
				if (value instanceof String){
					if (((String) value).equalsIgnoreCase("true")) return true;
					if (((String) value).equalsIgnoreCase("1")) return true;
					if (((String) value).equalsIgnoreCase("false")) return false;
					if (((String) value).equalsIgnoreCase("0")) return false;
					return false;
				}
				if (value instanceof Number) {
					if (value.equals(1)) return true;
					if (value.equals(0)) return false;
				}
			}
		}
		return value;
	}
	
	private static boolean int2Boolean(int i){
		if (i == 0) return false;
		return true;
	}
}
