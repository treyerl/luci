package org.luci.converters;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.luci.factory.observer.FactoryObserver;

public abstract class UnitConverter extends Converter {
	protected double amount;
	public static final Pattern pattern = Pattern.compile("^([0-9\\.]+)\\s?([\\D]+)$");
	
	public UnitConverter(FactoryObserver o, String literal) {
		super(o);
		Matcher matcher = pattern.matcher(literal);
		if (matcher.find()) amount = getCommonAmount(Double.parseDouble(matcher.group(1)), matcher.group(2));
		else amount = Double.parseDouble(literal);
	}
	
	public UnitConverter(FactoryObserver o, double amount, String unitSign){
		super(o);
		this.amount = getCommonAmount(amount, unitSign);
	}
	
	public abstract String getUnitName(String unitSign);
	public abstract double getInUnit(String unitSign);
	protected abstract double getCommonAmount(double amount, String unitSign);
}
