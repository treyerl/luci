package org.luci.converters.geometry;

import java.sql.Timestamp;
import java.util.List;
import java.util.concurrent.Callable;

import org.json.JSONArray;
import org.luci.converters.Converter;
import org.luci.factory.observer.FactoryObserver;
import org.luci.factory.observer.IDatabase;
import org.luci.utilities.Projection;
import org.luci.utilities.TAB;

public abstract class GeometryReader extends Converter implements Callable<List<Long>>{
	protected Projection projection;
	protected int ScID;
	protected long uid;
	protected Timestamp st;
	protected JSONArray errors;
	protected IDatabaseGeometryObjectReader dbG;
	protected IDatabase db;
	protected String data;
	protected String history;
	
	/**
	 * @param o FactoryObserver
	 * @param ScID long
	 * @param uid long
	 * @param t Timestamp
	 * @param pc Projections
	 * @param errors JSONArray
	 * @throws InstantiationException
	 */
	public GeometryReader(FactoryObserver o, int ScID, long uid, Timestamp t, Projection pc, 
			JSONArray errors) throws InstantiationException {
		super(o);
		this.st = t;
		this.projection = (pc != null) ? pc: new Projection();
		this.ScID = ScID;
		this.uid = uid;
		this.errors = errors;
		dbG = o.getDatabaseGeometryObjectReader();
		db = o.dataModelImplementation();
		data = TAB.getScenarioTable(ScID, TAB.SCN);
		history = TAB.getScenarioTable(ScID, TAB.SCN_HISTORY);
	}
	
	protected String getInsertString(){
		return "INSERT INTO "+data+"(geomID, timestamp, batchID, layer, nurb, transform, uid, flag, geom";
	}
	
	protected String getInsertAutoIDString(){
		return "INSERT INTO "+data+"(timestamp, batchID, layer, nurb, transform, uid, flag, geom ";
	}
}
