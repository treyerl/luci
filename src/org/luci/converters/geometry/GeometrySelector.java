package org.luci.converters.geometry;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.luci.connect.LcString;
import org.luci.factory.observer.FactoryObserver;
import org.luci.factory.observer.IDatabase;
import org.luci.utilities.TAB;
import org.luci.utilities.TimeRangeSelection;

public class GeometrySelector {
	private FactoryObserver o;
	private int ScID;
	private int batchID;
	private String layer;
	private long[] geomIDs;
	private TimeRangeSelection trs;
	private boolean inclHistory;
	private String version;
	private Map<String, String> attributes;
	private String[] orderBy;
	private int offset;
	private int limit;
	private IDatabase db;
	
	public GeometrySelector(FactoryObserver o, int ScID) {
		this(o, ScID, 0, null, new long[0], null, null, 0, 0, TimeRangeSelection.NULL(), false, null);
	}
	
	public GeometrySelector(FactoryObserver o, int ScID, TimeRangeSelection trs) {
		this(o, ScID, 0, null, new long[0], null, null, 0, 0, trs, false, null);
	}
	
	/**
	 * @param o FactoryObserver
	 * @param ScID int
	 * @param batchID int
	 * @param layer String
	 * @param geomIDs long
	 * @param trs TimeRangeSelection
	 * @param inclHistory boolean
	 * @param version String
	 * @param pc Projection
	 * @param outputStreams List< LcOutputStream >
	 * @param errors JSONArray
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	public GeometrySelector(FactoryObserver o, int ScID, int batchID,  String layer, long[] geomIDs,
			Map<String, String> attributes, String[] orderBy, int offset, int limit, 
			TimeRangeSelection trs, boolean inclHistory, String version){
		this.o = o;
		this.ScID = ScID;
		this.batchID = batchID;
		this.layer = layer;
		this.geomIDs = geomIDs;
		this.attributes = attributes;
		this.orderBy = orderBy;
		this.offset = offset;
		this.limit = limit;
		this.trs = trs;
		this.inclHistory = inclHistory;
		this.version = version;
		
		try {
			db = o.dataModelImplementation();
		} catch (InstantiationException e) {
			throw new RuntimeException(e);
		}
	}
	
	private String getFieldSelectionString(Set<String> fields){
		StringBuilder s = new StringBuilder();
		for (String field: fields) s.append("\""+field+"\", ");
		if (fields.size() > 0) s.delete(s.length()-2, s.length());
		return s.toString();
	}
	public String getSelectionSQL() throws SQLException {
		return getSelectionSQL(null);
	}
	
	public String getSelectionSQL(LinkedHashMap<String, Short> columns) throws SQLException{
		Connection con = o.getDatabaseConnection();
		try {
			LinkedHashMap<String, Short> types = db.getColumnNamesAndTypes(con, ScID);
			if (columns != null) columns.putAll(types);
			Set<String> fields = types.keySet();
			String sel = getFieldSelectionString(fields);
			String tab = TAB.getScenarioTable(ScID, TAB.SCN);
			String hst = TAB.getScenarioTable(ScID, TAB.SCN_HISTORY);
			
			String b = (batchID > 0) ? " AND batchID = "+batchID+" " : "";
			String l = (layer != null) ? "AND layer = ? " : "";
			String g = (geomIDs.length >  0) ? " AND geomID IN ( "+LcString.trim(Arrays.toString(geomIDs), 1)+" ) " : "";
			String a = "";
			if (attributes != null && attributes.size() > 0){
				StringBuilder aBuilder = new StringBuilder();
				Iterator<Entry<String, String>> it = attributes.entrySet().iterator();
				while(it.hasNext()){
					Entry<String, String> e = it.next();
					if (fields.contains((e.getKey()))) aBuilder.append(" AND ? = ?");
					else it.remove();
				}
				if (attributes.size() > 0) a = aBuilder.toString();
			}
			String o = "";
			if (orderBy != null){
				StringBuilder oBuilder = new StringBuilder(" ORDER BY ");
				for (int i = 0; i < orderBy.length; i++){
					String by = orderBy[i];
					String bySafe = by.split(";")[0];
					String byClean = bySafe.substring(1);
					if (i > 0) oBuilder.append(", ");
					if (by.startsWith("+")) {
						if (fields.contains(byClean)) oBuilder.append(" \""+byClean+"\" ASC");
					} else if (by.startsWith("-")) {
						if (fields.contains(byClean)) oBuilder.append(" \""+byClean+"\" DESC");
					} else {
						if (fields.contains(bySafe)) oBuilder.append(" \""+bySafe+"\" ASC");
					}
				}
				if (oBuilder.length() > 10) o = oBuilder.toString();
			} else if (inclHistory){
				o = " ORDER BY timestamp DESC ";
			}
			String limit = (this.limit > 0) ? " LIMIT "+this.limit+" " : "";
			String offset = (this.offset > 0) ? " OFFSET "+this.offset+" " : "";
			@SuppressWarnings("unused")
			String v = "something with RIGHT JOIN the boolean mask ";
			
			String sql = "SELECT "+sel+" FROM "+tab+" WHERE 1=1 "+b+l+g+a+trs.getSQL()+o+limit+offset;
			if (inclHistory) 
				sql = 	"( SELECT "+sel+" FROM "+tab+" WHERE 1=1 "+b+l+g+a+trs.getSQL()
						+ " UNION SELECT "+sel+" FROM "+hst+" WHERE 1=1 "+b+l+g+a+trs.getSQL()+") "+o+limit+offset;
			con.close();
			return sql;
		} catch (SQLException e) {
			con.close();
			throw e;
		}
		
	}
	
	public ResultSet select(Connection con, LinkedHashMap<String, Short> types) throws SQLException{
		String sql = getSelectionSQL(types);
		PreparedStatement ps = con.prepareStatement(sql);
		int i = 1;
		if (layer != null) ps.setString(i++, layer);
		if (attributes != null && attributes.size() > 0) {
			for (Entry<String, String> e : attributes.entrySet()){
				ps.setString(i++, e.getKey());
				ps.setString(i++, e.getValue());
			}
		}
		i = trs.setTimestampSQL(ps, i);
		try {
			return ps.executeQuery();
		} catch (SQLException e){
			con.close();
			
			throw e;
		}
	}
	
	public TimeRangeSelection getTimeRangeSelection(){
		return trs;
	}
	
	public int getScID(){
		return ScID;
	}
}
