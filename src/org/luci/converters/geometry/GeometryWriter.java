/**
 * The MIT License (MIT)
 *
 * Copyright (c) <year> <copyright holders>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package org.luci.converters.geometry;

import java.util.List;
import java.util.concurrent.Callable;

import org.json.JSONArray;
import org.json.JSONObject;
import org.luci.connect.LcOutputStream;
import org.luci.converters.Converter;
import org.luci.factory.observer.FactoryObserver;
import org.luci.utilities.Projection;

/**
 * 
 * @author treyerl
 *
 */
public abstract class GeometryWriter extends Converter implements Callable<JSONObject>{
	protected Projection projection;
	protected List<LcOutputStream> outputStreams;
	protected JSONArray errors;
	protected GeometrySelector selector;
	protected IDatabaseGeometryObjectWriter dbG;
	
	/**
	 * @param o FactoryObserver
	 * @param ScID Scenario ID int
	 * @param batchID Batch ID int
	 * @param layer Layer name String
	 * @param geomIDs Geometry ID long
	 * @param trs TimeRangeSelection
	 * @param inclHistory boolean
	 * @param version String (like 1.1.2.3.4)
	 * @param pc Projection
	 * @param outputStreams List< LcOutputStream >
	 * @param errors JSONArray
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 */
	public GeometryWriter(FactoryObserver o, GeometrySelector gs, Projection pc,
			List<LcOutputStream> outputStreams, JSONArray errors)
			throws InstantiationException, IllegalAccessException {
		super(o);
		this.selector = gs;
		this.outputStreams = outputStreams;
		this.errors = errors;
		
		if (pc == null) pc = new Projection();
		projection = pc;
		dbG = o.getDatabaseGeometryObjectWriter();
	}
	
	public Projection getProjection(){
		return projection;
	}

}
