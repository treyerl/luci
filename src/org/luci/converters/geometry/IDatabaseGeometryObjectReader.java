/**
 * The MIT License (MIT)
 *
 * Copyright (c) <year> <copyright holders>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package org.luci.converters.geometry;

import java.util.Iterator;

import org.luci.converters.OGISGeometry;
import org.luci.utilities.Projection;


/**
 * 
 * @author Lukas Treyer
 *
 */
public interface IDatabaseGeometryObjectReader extends OGISGeometry{
	
	public Object toPointObject(Iterator<Double> c, Projection proj);
	
	public byte[] toPointBytes(Iterator<Double> c, Projection proj);
		
	public Object toLinearRingObject(Iterator<Iterator<Double>> points, Projection proj);
	
	public byte[] toLinearRingBytes(Iterator<Iterator<Double>> points, Projection proj);
	
	public Object toLineStringObject(Iterator<Iterator<Double>> points, Projection proj);
	
	public byte[] toLineStringBytes(Iterator<Iterator<Double>> points, Projection proj);
	
	public Object toPolygonObject(Iterator<Iterator<Iterator<Double>>> rings, Projection proj);
	
	public byte[] toPolygonBytes(Iterator<Iterator<Iterator<Double>>> rings, Projection proj);
	
	public Object toMultiPointObject(Iterator<Iterator<Double>> points, Projection proj);
	
	public byte[] toMultiPointBytes(Iterator<Iterator<Double>> points, Projection proj);
	
	public Object toMultiLineStringObject(Iterator<Iterator<Iterator<Double>>> lineCollection, Projection proj);
	
	public byte[] toMultiLineStringBytes(Iterator<Iterator<Iterator<Double>>> lineCollection, Projection proj);
	
	public Object toMultiPolygonObject(Iterator<Iterator<Iterator<Iterator<Double>>>> polyCollection, Projection proj);
	
	public byte[] toMultiPolygonBytes(Iterator<Iterator<Iterator<Iterator<Double>>>> polyCollection, Projection proj);
	
	@SuppressWarnings("rawtypes")
	public Object toGeometryCollectionObject(Iterator collection, Projection proj);
	
	@SuppressWarnings("rawtypes")
	public byte[] toGeometryCollectionBytes(Iterator collection, Projection proj);

}
