/**
 * The MIT License (MIT)
 *
 * Copyright (c) <year> <copyright holders>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package org.luci.converters.geometry;

import org.json.JSONObject;

/**
 * 
 * @author Lukas Treyer
 *
 */
public interface IGeoJSONWriter {
	/**Abstract to retrieve geometry from the db. The specific implementation of a converter
	 * lets other plugins profit from the conversion method. For instance a tile service could
	 * run specific table queries and use the ordinary converter to export the geometry from
	 * the db to SVG-tiles for instance. 
	 * @param dbGeometry: java wrapper object for the db geometry object (as provided by the jdbc
	 * driver for the specific db)
	 * @param pc: Projection
	 * @return Object in the requested format. (GeoJSON, shp, svg, dxf, etc.)
	 */
	public abstract JSONObject geometryFromDb(Object dbGeometry);
}
