/**
 * The MIT License (MIT)
 *
 * Copyright (c) <year> <copyright holders>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package org.luci.converters.geometry;

import java.io.InputStream;
import java.security.MessageDigest;
import java.sql.Timestamp;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.luci.connect.GeometryInfo;
import org.luci.connect.LcInputStream;
import org.luci.connect.LcJSONObject;
import org.luci.converters.IStreamReader;
import org.luci.factory.observer.FactoryObserver;
import org.luci.utilities.Projection;

/**
 * 
 * @author Lukas Treyer
 *
 */
public abstract class JSONGeometryReader extends GeometryReader implements IStreamReader {
	protected JSONObject geometry;
	protected Map<String, String> attributeMap;
	/**
	 * @param o FactoryObserver
	 * @param geoInfo JSONObject
	 * @param ScID long
	 * @param uid long
	 * @param t Timestamp (SQL)
	 * @param pc Projection
	 * @param errors JSONArray
	 * @throws InstantiationException 
	 */
	public JSONGeometryReader(FactoryObserver o, GeometryInfo geoInfo, int ScID, long uid, Timestamp t,
			Projection pc, JSONArray errors) throws InstantiationException {
		super(o, ScID, uid, t, pc, errors);
		if (geoInfo != null) {
			this.geometry = geoInfo.getJSON().getJSONObject("geometry");
			attributeMap = geoInfo.attributeMap;
		} // else will it be read in readStream() in case the json file is being sent binary
		
	}
	
	/* (non-Javadoc)
	 * @see org.luci.converters.IStreamReader#readStream(org.luci.connect.LcInputStream)
	 */
	@Override
	public void readStream(LcInputStream lis) throws Exception {
		int length = lis.getLength();
		String checksum = lis.getChecksum();
		InputStream in = lis.getInputStream();
		MessageDigest d = MessageDigest.getInstance("MD5");
		byte[] headerB = new byte[(int) length];
		int len = 0, nRead = 0;
		while(len < length - 1){
			nRead = in.read(headerB, len, (int) (length - len));
			len += nRead;
		}
		d.update(headerB);
		String checksumRcv = org.luci.connect.LcString.getHexFromBytes(d.digest());
		// "checksum == null" if the file was downloaded from url
		if (checksum != null && !checksumRcv.equalsIgnoreCase(checksum)) 
			throw new IllegalArgumentException();
		String msg = new String(headerB, "UTF-8");
		geometry = new LcJSONObject(msg);
		attributeMap = lis.attributeMap;
	}
}
