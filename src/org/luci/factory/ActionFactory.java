package org.luci.factory;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import org.json.JSONException;
import org.json.JSONObject;
import org.luci.comm.action.Action;
import org.luci.comm.clienthandlers.ClientHandler;
import org.luci.connect.LcJSONObject;
import org.luci.factory.observer.FactoryObserver;

public class ActionFactory extends TaskFactory {

	public ActionFactory(Class<?> cls, FactoryObserver o) 
			throws InstantiationException, IllegalAccessException, IllegalArgumentException, 
			InvocationTargetException, SecurityException {
		super(cls, o);
	}

	public Action getAction(ClientHandler h, LcJSONObject json) throws InstantiationException {
		Constructor<?> ct;
		try {
			ct = productClass.getDeclaredConstructor(FactoryObserver.class, ClientHandler.class , LcJSONObject.class);
			return (Action) ct.newInstance(o, h, json);
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | 
				IllegalArgumentException | InvocationTargetException e) {
			throw new InstantiationException(e.toString());
		}
		
	}
	
	public String getName() {
		return productClass.getSimpleName();
	}
	
	public String getIndexDescription(){
		String comment = "";
		JSONObject inputs = this.ioFormat.getJSONObject("inputs");
		if (inputs.has("comment")){
			comment = " [" + inputs.getString("comment") + "]";
		}
		try {
			return inputs.getString("subject")+comment;
		} catch (JSONException e){
			throw new JSONException(this.getName()+": "+e.getMessage());
		}
	}
}
