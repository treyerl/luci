package org.luci.factory;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

//import org.luci.converters.FileReader;


import org.luci.connect.DirectStreamWriter;
import org.luci.factory.observer.FactoryObserver;

public class ConverterFactory extends Factory{
	protected Class<?> converterClass;

	public ConverterFactory(FactoryObserver o, Class<?> readerClass) {
		super(o);
		this.converterClass = readerClass;
	}
//
//	public FileReader getReaderDummy() throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
//		Constructor<?> ct = getFirstPublic(readerClass.getDeclaredConstructors());
//		Object[] args = createParameters(ct, o, this);
//		return (FileReader) ct.newInstance(args);
//	}
	
	public DirectStreamWriter getDummy(Class<?> converterClass) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		Constructor<?> ct = getFirstPublic(converterClass.getDeclaredConstructors());
		Object[] args = createEmptyParameters(ct, o, this);
		return (DirectStreamWriter) ct.newInstance(args);
	}

}
