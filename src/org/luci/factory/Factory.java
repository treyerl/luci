package org.luci.factory;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.sql.Timestamp;
import java.util.Date;

import org.luci.Luci;
import org.luci.comm.clienthandlers.ClientHandler;
import org.luci.comm.clienthandlers.PublicClientHandler;
import org.luci.connect.LcSocket;
import org.luci.factory.observer.FactoryObserver;
import org.luci.services.ServiceCall;
import org.luci.utilities.Projection;
import org.slf4j.Logger;

public abstract class Factory{
	protected FactoryObserver o;
	protected Logger log;

	public Factory(FactoryObserver o) {
		this.o = o;
		this.log = Luci.createLoggerFor(getClass().getSimpleName());
	}
	
	/**
	 * Returns an observer object that holds the services hash map.
	 * 
	 * @return Observer object that holds the services hash map.
	 */
	public FactoryObserver getObserver() {
		return o;
	}
	
	protected Constructor<?> getFirstPublic(Constructor<?>[] cts){
		for (Constructor<?> ct: cts){
			if (Modifier.isPublic(ct.getModifiers())) return ct;
		}
		return null;
	}
	
	protected Object[] createEmptyParameters(Constructor<?> ct, FactoryObserver o, Factory f) {
		Object[] args = new Object[ct.getParameterTypes().length];
		int i = 0;
		for (Class<?> pc: ct.getParameterTypes()){
			if (pc.isPrimitive()){
				if (boolean.class.isAssignableFrom(pc)) {
					args[i++] = false;
				} else {
					args[i++] = 0;
				}
			} else if (ServiceCall.class.isAssignableFrom(pc)){
				args[i++] = new ServiceCall(null, 0,0,0, null);
			} else if (FactoryObserver.class.isAssignableFrom(pc)){
				args[i++] = o;
			} else if (Factory.class.isAssignableFrom(pc)){
				args[i++] = f;
			} else if(ClientHandler.class.isAssignableFrom(pc)) {
				args[i++] = new PublicClientHandler(new LcSocket(null), o, null, 0); 
			} else if(Projection.class.isAssignableFrom(pc)){
				args[i++] = new Projection();
			} else if(Timestamp.class.isAssignableFrom(pc)){
				args[i++] = new Timestamp(new Date().getTime());
			} else {
//				Constructor<?> ctWithLeastParams = null;
//				int minParamLength = 1000;
//				for (Constructor<?> c: pc.getDeclaredConstructors()){
//					if (c.getParameterCount() <= minParamLength 
//							&& Modifier.isPublic(c.getModifiers())
//							&& c.getAnnotation(Deprecated.class) == null
//							&& !contains(c.getParameterTypes(), pc)) 
//						ctWithLeastParams = c;
//				}
//				if (ctWithLeastParams != null){
//					Object[] subArgs = createParameters(ctWithLeastParams, o, f);
//					args[i++] = ctWithLeastParams.newInstance(subArgs);
//				}
				args[i++] = null;
			}
		}
		return args;
	}
//	
//	protected boolean contains(Class<?>[] list, Class<?> cls){
//		for (Class<?> c: list) if (c.equals(cls)) return true;
//		return false;
//	}
	
	
}