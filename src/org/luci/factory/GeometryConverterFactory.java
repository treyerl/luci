package org.luci.factory;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.sql.Timestamp;
import java.util.List;

import org.json.JSONArray;
import org.luci.connect.GeometryInfo;
import org.luci.connect.LcOutputStream;
import org.luci.converters.geometry.GeometryReader;
import org.luci.converters.geometry.GeometrySelector;
import org.luci.converters.geometry.GeometryWriter;
import org.luci.converters.geometry.JSONGeometryReader;
import org.luci.factory.observer.FactoryObserver;
import org.luci.utilities.Projection;

public class GeometryConverterFactory extends Factory{
	protected Class<?> readerClass;
	protected Class<?> writerClass;

	public GeometryConverterFactory(FactoryObserver o, Class<?> readerClass, Class<?> writerClass) {
		super(o);
		this.readerClass = readerClass;
		this.writerClass = writerClass;
	}
	
	public Class<?> getReaderClass(){
		return readerClass;
	}
	
	public Class<?> getWriterClass(){
		return writerClass;
	}
	
	public JSONGeometryReader getJSONGeometryReader(FactoryObserver o, GeometryInfo geometry, 
			int ScID, long uid, Timestamp t, Projection pc, JSONArray errors) 
			throws InstantiationException{
		
		if (!JSONGeometryReader.class.isAssignableFrom(readerClass)) 
			throw new InstantiationException(readerClass+" cannot handle JSONGeometry!");
		
		try {
//			Class<?> c = readerClass;
//			// if DEBUG get class from classpath not the loaded plugin class; better for stepping ?
//			if (Luci.DEBUG) c = Class.forName(readerClass.getName());
			
			Constructor<?> ct = readerClass.getDeclaredConstructor(FactoryObserver.class, 
					GeometryInfo.class, int.class, long.class, Timestamp.class, Projection.class, JSONArray.class);
			return (JSONGeometryReader) ct.newInstance(o, geometry, ScID, uid, t, pc, errors);
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | 
				IllegalArgumentException | InvocationTargetException  e) {
			throw new InstantiationException(e.toString());
		}
	}
	
	public GeometryReader getGeometryReader(FactoryObserver o, int ScID, long uid, 
			Timestamp t, Projection pc, JSONArray errors) throws InstantiationException{
		
		try {
			Class<?> c = readerClass;
			// if DEBUG get class from classpath not the loaded plugin class; better for stepping ?
//			if (Luci.DEBUG) c = Class.forName(readerClass.getName());
			if (!JSONGeometryReader.class.isAssignableFrom(c)){
				Constructor<?> ct = c.getDeclaredConstructor(FactoryObserver.class,
						int.class, long.class, Timestamp.class, Projection.class, JSONArray.class);
				return (GeometryReader) ct.newInstance(o, ScID, uid, t, pc, errors);
			} else {
				Constructor<?> ct = c.getDeclaredConstructor(FactoryObserver.class, 
						GeometryInfo.class, int.class, long.class, Timestamp.class, Projection.class, JSONArray.class);
				return (JSONGeometryReader) ct.newInstance(o, null, ScID, uid, t, pc, errors);
			}
			
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | 
				IllegalArgumentException | InvocationTargetException e) {
//			e.printStackTrace();
			throw new InstantiationException(e.toString());
		}
	}
	
	public GeometryWriter getGeometryWriter(FactoryObserver o, GeometrySelector gs, Projection pc, 
			List<LcOutputStream> outputStreams, JSONArray errors) 
			throws InstantiationException {
		try {
			Constructor<?> ct = writerClass.getDeclaredConstructor(FactoryObserver.class, 
					GeometrySelector.class, Projection.class, List.class, JSONArray.class);
			return (GeometryWriter) ct.newInstance(o, gs, pc, outputStreams, errors);
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | 
				IllegalArgumentException | InvocationTargetException e) {
			throw new InstantiationException(e.toString());
		}
		
	}

}
