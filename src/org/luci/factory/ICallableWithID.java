package org.luci.factory;

import java.util.concurrent.Callable;

import org.luci.factory.call.Call;

public interface ICallableWithID<V> extends Callable<V> {
	public long getID();
	public Call getCall();
}
