package org.luci.factory;

import org.luci.connect.LcMetaJSONObject;

public interface ITask {
	
	/**
	 * @return  inputs of a task encoded in a LcMetaJSONObject();
	 */
	public LcMetaJSONObject getInputIndex();
	
	/**
	 * @return outputs of a task encoded in a LcMetaJSONObject();
	 */
	public LcMetaJSONObject getOutputIndex();
}
