package org.luci.factory;

import java.util.Set;

import org.json.JSONObject;

public interface ITaskFactory extends ITask {
	public Class<?> getProductClass();
	public boolean validateInput(JSONObject json, Set<String> unknowns);
}
