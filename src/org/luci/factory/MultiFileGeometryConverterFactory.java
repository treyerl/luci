package org.luci.factory;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Set;

import org.luci.converters.IMultiStreamReader;
import org.luci.factory.observer.FactoryObserver;

public class MultiFileGeometryConverterFactory extends GeometryConverterFactory {
	private String format;
	private Set<String> req;
	
	public MultiFileGeometryConverterFactory(FactoryObserver o, Class<?> readerClass, Class<?> writerClass) 
			throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		super(o, readerClass, writerClass);
		Constructor<?> ctR = getFirstPublic(readerClass.getDeclaredConstructors());
		Constructor<?> ctW = getFirstPublic(writerClass.getDeclaredConstructors());
		if (ctR == null) throw new InstantiationException("Task '"+readerClass.getSimpleName()+"' has no public constuctor!");
		if (ctW == null) throw new InstantiationException("Task '"+writerClass.getSimpleName()+"' has no public constuctor!");
		IMultiStreamReader r = (IMultiStreamReader) ctR.newInstance(createEmptyParameters(ctR, o, this));
		req = r.fileFormatSuffixes();
		format = r.getMainFormatSuffix();
		
	}

	public Set<String> fileFormatSuffixes() {
		return req;
	}

	public String getMainFormatSuffix() {
		return format;
	}
}
