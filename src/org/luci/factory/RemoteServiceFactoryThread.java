package org.luci.factory;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;

import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;
import org.json.JSONObject;
import org.luci.comm.clienthandlers.AuthenticatedClientHandler;
import org.luci.comm.clienthandlers.ClientHandler;
import org.luci.comm.clienthandlers.RemoteServiceClientHandler;
import org.luci.connect.LcMetaJSONObject;
import org.luci.factory.observer.FactoryObserver;
import org.luci.services.RemoteService;
import org.luci.services.ServiceCall;

/**
 * Service Factory for connected services. For all connected services with the
 * same name there is one factory.
 * 
 * @author Lukas Treyer <treyer@arch.ethz.ch>
 * 
 */
public class RemoteServiceFactoryThread extends ServiceFactoryThread {
	private LinkedBlockingQueue<RemoteServiceClientHandler> availableNodes;
	private String version;

	public RemoteServiceFactoryThread(FactoryObserver o, JSONObject description, long reservedSObjID) throws Exception {
		super(RemoteService.class, o, 
				new LcMetaJSONObject(description.optJSONObject("inputs")), 
				new LcMetaJSONObject(description.optJSONObject("outputs")), 
				description.getString("classname"));
		this.availableNodes = new LinkedBlockingQueue<RemoteServiceClientHandler>();
		this.version = description.getString("version");
		Connection con = o.getDatabaseConnection();
		get.setCon(con);
		for (Long SObjID: get.getServiceInstanceList(this.name, "INIT")){
			run(SObjID);
		}
		con.close();
	}
	
	public String getVersion(){
		return this.version;
	}
	
	private synchronized void availableNodesAdd(RemoteServiceClientHandler rsch){
		this.availableNodes.add(rsch);
	}
	
	private synchronized void availableNodesRemove(RemoteServiceClientHandler rsch){
		this.availableNodes.remove(rsch);
	}
	
	/**back-end implementation of 'remote_service_register':
	 * put the service into the db and store the retrieved id in RsID
	 * 
	 * @param s
	 * @param request
	 * @param u
	 * @throws Exception 
	 */
	public RemoteServiceClientHandler register(ClientHandler pre, String machinename, long SObjIDreservation) throws Exception{
		if (pre instanceof AuthenticatedClientHandler){
			RemoteServiceClientHandler rsch = new RemoteServiceClientHandler(pre, o, this, version, machinename, SObjIDreservation);
			availableNodesAdd(rsch);
			return rsch;
		} else {
			return null;
		}
		
	}
	
	public void unregister(RemoteServiceClientHandler rsch) throws MqttPersistenceException, MqttException {
		ServiceCall call = rsch.getServiceCall();
		if (call != null){
			this.cancelExecution(call);
		}
		this.availableNodesRemove(rsch);
		if (this.availableNodes.size() == 0){
			this.o.unregisterRemoteService(this.name);
		}
	}
	
	public void putBack(RemoteServiceClientHandler rsch){
		rsch.setRemoteService(null);
		this.availableNodesAdd(rsch);
	}
	
	public List<String> getProviders(){
		List<String> providers = new ArrayList<String>();
		for (RemoteServiceClientHandler rsch: this.availableNodes){
			providers.add(rsch.getMachineName()+":"+rsch.getUser().getUsername());
		}
		return providers;
	}
	
	public int[] getNodeInfo(){
		int[] i = {availableNodes.size(), runningCalls.size()};
		return i;
	}
	
	@Override
	public RemoteService getService(ServiceCall call) {
		try {
			RemoteServiceClientHandler rsch;
			rsch = availableNodes.take();
			return new RemoteService(call, this, rsch, version);
		} catch (Exception e) {
			this.log.debug(e.toString());
		}
		return null;
	}
}
