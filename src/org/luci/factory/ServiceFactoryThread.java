package org.luci.factory;

import java.io.FileNotFoundException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;

import org.json.JSONArray;
import org.json.JSONObject;
import org.luci.Luci;
import org.luci.comm.action.IGetServiceInputs;
import org.luci.comm.action.ISetServiceOutputs;
import org.luci.connect.LcJSONObject;
import org.luci.connect.LcMetaJSONObject;
import org.luci.factory.call.Call;
import org.luci.factory.call.CallSubscriptions;
import org.luci.factory.call.CallSupervisor;
import org.luci.factory.observer.FactoryObserver;
import org.luci.factory.observer.IDatabase;
import org.luci.services.Service;
import org.luci.services.ServiceCall;

public class ServiceFactoryThread extends CallSubscriptions implements ITaskFactory{
	protected TaskFactory tf;
	protected IGetServiceInputs get;
	private ISetServiceOutputs set;
	
	public ServiceFactoryThread(Class<?> cls, FactoryObserver o) 
			throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, 
			SecurityException, NoSuchMethodException, FileNotFoundException {
		super(o, cls.getSimpleName());
		tf = new TaskFactory(cls, o, this);
		get = o.getActionGetServiceInputs();
		set = o.getActionSetServiceOutputs(null);
		loadInstances();
	}
	
	/**Constructor to be used by RemoteServiceFactory (child) in order to load 
	 * in & output indices and the service name from the json string.
	 * @param cls
	 * @param o
	 * @param inDex
	 * @param outDex
	 * @param n
	 * @throws FileNotFoundException 
	 * @throws SecurityException 
	 * @throws NoSuchMethodException 
	 * @throws InvocationTargetException 
	 * @throws IllegalArgumentException 
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 */
	protected ServiceFactoryThread(Class<?> cls, FactoryObserver o, LcMetaJSONObject inDex, LcMetaJSONObject outDex, String n) 
			throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, 
			NoSuchMethodException, SecurityException, FileNotFoundException {
		super(o, n);
		tf = new TaskFactory(cls, o, inDex, outDex);
		get = o.getActionGetServiceInputs();
		set = o.getActionSetServiceOutputs(null);
		loadInstances();
	}
	
	@Override
	public LcMetaJSONObject getInputIndex() {
		return tf.getInputIndex();
	}

	@Override
	public LcMetaJSONObject getOutputIndex() {
		return tf.getOutputIndex();
	}


	@Override
	public Class<?> getProductClass() {
		return tf.getProductClass();
	}

	@Override
	public boolean validateInput(JSONObject input, Set<String> unknowns) {
		return tf.validateInput(input, unknowns);
	}
	
	/**
	 * Upon factory creation: lookup any stored service instances in the DB and
	 * create their mqtt-subscriptions
	 * @throws UnknownHostException 
	 * 
	 */
	@Override
	protected boolean loadInstances() {
		try {
			if (mqttConnect()){
				Connection con = o.getDatabaseConnection();
				get.setCon(con);

				Map<Long, Long> instances = get.getServiceInstanceMap(name);
				String mqtt_pre = Luci.getConfig().getProperty("mqtt_pre", "/Luci/");
				Set<String> new_topics = new HashSet<String>();
				
				if (instances.size() > 0){
					for (long SObjID: instances.keySet()){
						get.setSObjID(SObjID);
						JSONObject instance = get.getServiceInstanceInfo();
						boolean getSubscriptions = true;
						LcJSONObject inputs = get.getServiceInputs(getSubscriptions, null, null);
						if (inputs != null){
							for (String key: inputs.keySet()){
								String value = inputs.optString(key);
								if (value != null && value.startsWith(mqtt_pre)){
									String topic = value.replaceFirst("\\/$", "");
									subscribeBlocking(topic, SObjID, key);
									new_topics.add(topic);
								}
							}
						}
						List<String> nonBlockingTopics = new ArrayList<String>();
						long ScID = instance.getLong("ScID");
						nonBlockingTopics.add(mqtt_pre+ScID+"/"+SObjID);
						if (instance.has("subscriptions")){
							JSONArray subscriptions = instance.getJSONArray("subscriptions");
							for (int i = 0; i < subscriptions.length(); i++) {
								nonBlockingTopics.add(subscriptions.getString(i));
							}
									
						}
						updateNonBlockingSubscriptions(nonBlockingTopics, SObjID);
						new_topics.addAll(nonBlockingTopics);
						
					}
					subscribeMQTT(new_topics);
					/* no unsubscribeMQTT(del_topics) needed since we are setting up existing instances */
				}
				con.close();
				return true;
			}
		} catch (Exception e) {
			this.log.error(e.toString());
			e.printStackTrace();
		}
		return false;
	}
	
	/**
	 * Creates a new instance of the service class for the given User u.
	 * 
	 * @return Services s
	 * @throws SecurityException 
	 * @throws NoSuchMethodException 
	 * @throws InvocationTargetException 
	 * @throws IllegalArgumentException 
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 */
	public Service getService(ServiceCall call) throws NoSuchMethodException, SecurityException, InstantiationException, 
			IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		Constructor<?> ct = tf.getProductClass().getDeclaredConstructor(ServiceCall.class, ServiceFactoryThread.class, String.class);
		return (Service) ct.newInstance(call, this, Luci.getConfig().getProperty("projectname", "LuciServer"));
	}
	
	
	public Service getService(long SObjID, long uID) throws Exception{
		ServiceCall call = new ServiceCall(SObjID, uID, o);
		Constructor<?> ct = tf.getClass().getDeclaredConstructor(ServiceCall.class, ServiceFactoryThread.class, String.class);
		return (Service) ct.newInstance(call, this, Luci.getConfig().getProperty("projectname", "LuciServer "+InetAddress.getLocalHost().getHostAddress()));
	}
	
	@Override
	protected void setStatus(Call call, String status){
		try {
			if (call.getID() != 0) set.setServiceStatus((ServiceCall) call, status);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public Object call() throws Exception{
		ExecutorService pool = o.getThreadPool();
		while(true){
			// this.calls is blocking queue; take() blocks until an item gets available in the queue
			ServiceCall call = calls.take();
			long SObjID = call.getSObjID(); 
			long ScID = call.getScID();
			String topic = mqtt_pre+ScID+"/"+SObjID;
			Service s;
			try {
				s = getService(call);
			} catch (Exception e){
				e.printStackTrace();
				setStatus(call, "FAIL");
				mqtt.publish(topic, "FAIL".getBytes(), Luci.qos_0_at_most_once, false);
				continue;
			}
			try { 
				
				CallSupervisor e = new CallSupervisor(pool, s, topic, this);
				runningCalls.put(call, e);
				new Thread(e,"ServiceSupervisor ."+SObjID).start();
			} catch (Exception e) {
				this.setStatus(call, "FAIL");
				log.debug(e.toString());
			} 
		}
	}
}
