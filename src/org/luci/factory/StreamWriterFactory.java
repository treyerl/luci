package org.luci.factory;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;

import org.luci.connect.DirectStreamWriter;
import org.luci.factory.observer.FactoryObserver;

public class StreamWriterFactory extends Factory{
	protected HashMap<String, Class<?>> writerClasses;

	public StreamWriterFactory(FactoryObserver o, HashMap<String, Class<?>> writerClasses) {
		super(o);
		this.writerClasses = writerClasses;
	}
	
	public DirectStreamWriter getFileWriter(FactoryObserver o, String fromChecksum, String toFormat) 
			throws InstantiationException {
		try {
			Constructor<?> ct = writerClasses.get(toFormat).getDeclaredConstructor(FactoryObserver.class, String.class);
			return (DirectStreamWriter) ct.newInstance(o, fromChecksum);
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			throw new InstantiationException(e.toString());
		}
	}
	
	public DirectStreamWriter getWriterDummy(String writerName) throws InstantiationException {
		Constructor<?> ct = getFirstPublic(writerClasses.get(writerName).getDeclaredConstructors());
		Object[] args = createEmptyParameters(ct, o, this);
		try {
			return (DirectStreamWriter) ct.newInstance(args);
		} catch (IllegalAccessException | IllegalArgumentException
				| InvocationTargetException e) {
			throw new InstantiationException(e.toString());
		}
	}

}
