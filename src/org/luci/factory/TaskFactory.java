package org.luci.factory;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Set;

import org.json.JSONObject;
import org.luci.connect.LcJSONObject;
import org.luci.connect.LcMetaJSONObject;
import org.luci.factory.observer.FactoryObserver;

/**Implementation of ITaskFactory (to be used as a delegate of classes implementing ITaskFactory; 
 * workaround for multiple inheritance).
 * 
 * @author Lukas Treyer <treyer@arch.ethz.ch>
 * 
 */
public class TaskFactory extends Factory implements ITaskFactory{
	protected Class<?> productClass;
	protected LcJSONObject ioFormat;
	protected LcJSONObject debug;
	
	public TaskFactory(Class<?> cls, FactoryObserver o) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, SecurityException{
		this(cls, o, null);
	}
	
	public TaskFactory(Class<?> cls, FactoryObserver o, Factory f) throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, SecurityException {
		super(o);
		if (!ITask.class.isAssignableFrom(cls)) throw new IllegalArgumentException("Class of type ITask required!");
		Constructor<?> ct = getFirstPublic(cls.getDeclaredConstructors());
		if (ct == null) throw new InstantiationException("Task '"+cls.getSimpleName()+"' has no public constuctor!");
		if (f == null) f = this;
		ITask t = (ITask) ct.newInstance(createEmptyParameters(ct, o, f));
		ioFormat = new LcJSONObject();
//		if (cls.getSimpleName().equals("create_scenario")){
//			debug = t.getInputIndex();
//			debug = new LcJSONObject(t.getInputIndex());
//			debug = new LcJSONObject(t.getInputIndex()).isValidRequirement(cls.getSimpleName());
//		}
		LcMetaJSONObject inputs = t.getInputIndex();
		LcMetaJSONObject outputs = t.getOutputIndex();
		if (inputs == null) throw new IllegalArgumentException(t.getClass().getSimpleName()+" inputs");
		if (outputs == null) throw new IllegalArgumentException(t.getClass().getSimpleName()+" outputs");
		ioFormat.put("inputs",  inputs.isValidRequirement(cls.getSimpleName()));
		ioFormat.put("outputs", outputs.isValidRequirement(cls.getSimpleName()));
		productClass = cls;
		
	}

	public TaskFactory(Class<?> cls, FactoryObserver o, LcMetaJSONObject inDex, LcMetaJSONObject outDex) {
		super(o);
		if (!ITask.class.isAssignableFrom(cls)) throw new IllegalArgumentException("Class of type ITask required!");
		ioFormat = new LcJSONObject();
		ioFormat.put("inputs", inDex.isValidRequirement(cls.getSimpleName()));
		ioFormat.put("outputs", outDex.isValidRequirement(cls.getSimpleName()));
		productClass = cls;
	}
	
	@Override
	public Class<?> getProductClass() {
		return productClass;
	}
	
	@Override
	public LcMetaJSONObject getInputIndex() {
		JSONObject inputs = ioFormat.getJSONObject("inputs");
		if (inputs.has("subject") || inputs.has("comment")) {
			LcMetaJSONObject inputsCopy = new LcMetaJSONObject(inputs);
			inputsCopy.remove("subject");
			inputsCopy.remove("comment");
			return inputsCopy;
		}
		return new LcMetaJSONObject(inputs);
	}
	
	@Override
	public LcMetaJSONObject getOutputIndex() {
		return new LcMetaJSONObject(ioFormat.getJSONObject("outputs"));
	}	
	
	@Override
	public boolean validateInput(JSONObject input, Set<String> unknowns){
		return getInputIndex().isValidInput(input, unknowns, productClass.getSimpleName());
	}
}
