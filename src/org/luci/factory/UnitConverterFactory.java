package org.luci.factory;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import org.luci.converters.UnitConverter;
import org.luci.factory.observer.FactoryObserver;

public class UnitConverterFactory extends Factory {
	protected Class<?> unit;
	
	public UnitConverterFactory(FactoryObserver o, Class<?> unit) {
		super(o);
		this.unit = unit;
	}
	
	public UnitConverter getUnitConverter(String literal) 
			throws NoSuchMethodException, SecurityException, InstantiationException, 
			IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		Constructor<?> ct = unit.getDeclaredConstructor(FactoryObserver.class, String.class);
		return (UnitConverter) ct.newInstance(o, literal);
	}
	
	public UnitConverter getUnitConverter(double amount, String unitSign) 
			throws NoSuchMethodException, SecurityException, InstantiationException, 
			IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		Constructor<?> ct = unit.getDeclaredConstructor(FactoryObserver.class, double.class, String.class);
		return (UnitConverter) ct.newInstance(o, amount, unitSign);
	}

}
