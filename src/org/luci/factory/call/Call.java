package org.luci.factory.call;

import java.sql.Timestamp;

import org.luci.Luci;
import org.luci.services.ServiceCall;
import org.slf4j.Logger;

public class Call {
	protected Timestamp newest;
	protected long ID;
	protected int ScID;
	protected long uID;
	protected Logger log;
	protected String message;
	
	/**Creates a Call object holding ID, ScID and the newest timestamp of inputs.
	 * @param newest: newest Timestamp
	 * @param SObjID: (long) service instance ID
	 * @param ScID: (long) scenario ID
	 * @param uID: (long) user ID
	 */
	public Call(Timestamp newest, long ID, int ScID, long uID, String message) {
		this.newest = newest;
		this.ID = ID;
		this.ScID = ScID;
		this.uID = uID;
		this.message = message;
		log = Luci.createLoggerFor(getClass().getSimpleName());
	}
	
	public Timestamp getTimestamp(){
		return newest;
	}
	
	public long getID(){
		return ID;
	}
	
	public int getScID(){
		return ScID;
	}
	
	public long getUID(){
		return uID;
	}
	
	public String getMessage(){
		return message;
	}
	
	public boolean hasMessage(){
		return message != null;
	}
	
	public boolean equals(ServiceCall call){
		return (ID == call.getID() && ScID == call.getScID() && newest.equals(call.getTimestamp()));
	}
}
