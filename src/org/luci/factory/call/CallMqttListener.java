package org.luci.factory.call;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.luci.Luci;
import org.luci.services.ServiceCall;

/**Define the default reaction when a service gets fired by a MQTT event:
	 * if (this.queue.contains(the same event already)){
	 *  		don't add it. the event being in queue will 
	 *   		take the new results stored in the database
	 *    } else {
	 *     		this.queue.add(SObjID)
	 *    }
	 * }   
	 */
	public class CallMqttListener implements MqttCallback{
		
		private CallSubscriptions cs;
		public CallMqttListener(CallSubscriptions cs){
			this.cs = cs;
		}
		
		@SuppressWarnings("unused")
		private void setWait(long SObjID, String topic) throws Exception{
			// no way around, we need to get the newest call
			ServiceCall call = new ServiceCall(SObjID, 0, cs.getObserver());
			cs.setStatus(call, "WAIT");
			cs.mqtt.publish(topic, "WAIT".getBytes(), Luci.qos_0_at_most_once, false);
		}
		
		@Override
		public void messageArrived(String topic, MqttMessage msg) {
 			String m = msg.toString(); //.replace("ascii: ", "").trim();
// 			System.out.println(java.util.Arrays.toString(m.getBytes()));
// 			System.out.println(java.util.Arrays.toString("RUN".getBytes()));
 			if (Luci.DEBUG) System.out.println(" "+topic+" "+msg);
			try {
				String t = topic.toString().trim();
				/* matching /mqtt_pre/ScID/SObjID like /Luci/0/23 */
				Matcher scnAndServ = cs.scenarioAndServiceTopicPattern.matcher(t);
				/* matching /mqtt_pre/ScID/SObjID/outputs/KEYNAME like /Luci/0/23/outputs/inhabitants
				 * don't mind the comments in the block below, just trying to summarize a somewhat complex task
				 * (aka 3 maps used in parallel...) */ 
				Matcher servOut = cs.serviceOutputPattern.matcher(t);
				Matcher any = cs.anyTopicPattern.matcher(t);
				if (scnAndServ.matches()){
					//String scn_topic = match.group(2);
					long SObjID = 0;
					if (scnAndServ.groupCount() > 2)
						SObjID = Long.parseLong(scnAndServ.group(4));
					
//					System.err.println(cs.nonBlockingTopics.keySet());
//					System.err.println(cs.nonBlockingTopics.values());
					
					if (cs.nonBlockingTopics.containsKey(t)){
						if (cs.ignoreOnMQTT.containsKey(SObjID)) {
							cs.ignoreOnMQTT.remove(SObjID);
							return;
						}
						HashSet<Long> SObjIDs = cs.nonBlockingTopics.get(t);
						if (SObjIDs != null){
							if (m.equals("RUN")){
								if (SObjID == 0){ // = RUN on scenario, run all services immediately hooked to it
									for (long id: SObjIDs){
//										if (Luci.DEBUG) System.out.println("RUN on mqtt: "+SObjID+"/"+id);
										cs.run(id);
									}
								} else { // = RUN on service, run current service and set all hooked services to WAIT
									for (long id: SObjIDs){
										if (id == SObjID){
//											if (Luci.DEBUG)  System.out.println("RUN on mqtt: "+SObjID+"/"+id);
											cs.run(SObjID);
										} else {
											//setWait(id, scn_topic + "/" + id);
										}
									}
								}
							} else if (m.equals("DONE")){ // RUN all services hooked to this one
								for (long id: SObjIDs){
									if (id != SObjID){
										cs.run(id);
									}
								}
							} else if (m.equals("WAIT")){ // set the whole chain to WAIT
								for (long id: SObjIDs){
									if (id != SObjID){
										//setWait(id, scn_topic + "/" + id);
									}
								}
							}
						}
					}
				} else if (servOut.matches() && cs.blockingTopics.containsKey(t) && m.equals("DONE")){ // FIXME: publish DONE on outputs instead of value
					// get all SObjIDs listening to t
					HashSet<Long> SObjIDs = cs.blockingTopics.get(t);
					for (long id: SObjIDs){
						// get all keyname <-> DONE pairs per service object id
						Map<String, Boolean> keys = cs.blockingArrival.get(id);
						
						// iterating over topicTracker map which holds keyname <-> subscription (topic) pairs
						// giving us all the keynames listening to this topic
						List<String> keynames = new ArrayList<String>();
						for (Entry<String, String> keyMap: cs.topicTracker.get(id).entrySet())
							if (keyMap.getValue().equals(t)) keynames.add(keyMap.getKey());
						
						// update the blockingTopics map
						for (String keyname: keynames) keys.put(keyname, true);
						
						// if all those keyname switches are set to true we can run the service and reset 
						boolean let_it_run = true;
						for (Boolean go: keys.values()) if (!go) let_it_run = false;
						if (let_it_run) cs.run(id);
						
						// reset the blocking arrival map for id
						for (String keyname: keys.keySet()) keys.put(keyname, false);
					}
				} else if (any.matches()){
					HashSet<Long> SObjIDs = cs.nonBlockingTopics.get(t);
					if (SObjIDs != null){
						for (long id: SObjIDs)
							cs.run(id, m);
					}
				}
				
				// TODO: process the service messages
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		@Override
		public void connectionLost(Throwable e) {
			if (cs.isMqttConnected){
				if (cs.loadInstances()) System.err.println("reconnect " + cs.name);
			}
		}

		@Override
		public void deliveryComplete(IMqttDeliveryToken arg0) {}
		
	}
