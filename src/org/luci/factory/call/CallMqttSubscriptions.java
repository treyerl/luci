package org.luci.factory.call;

import java.io.IOException;
import java.net.InetAddress;
import java.util.Collection;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import org.eclipse.paho.client.mqttv3.MqttAsyncClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.luci.CountsOnMqttActionSuccess;
import org.luci.Luci;
import org.luci.factory.observer.FactoryObserver;

public abstract class CallMqttSubscriptions extends CallQueue{	
	
	public final Pattern scenarioAndServiceTopicPattern;
	public final Pattern converterPattern;
	public final Pattern serviceOutputPattern;
	public final Pattern anyTopicPattern;
	
	protected String mqtt_pre;
	protected MqttAsyncClient mqtt;
	protected InetAddress host;
	protected boolean isMqttConnected;
	protected CallMqttSubscriptions(FactoryObserver o, String name) {
		super(o, name);
		mqtt_pre = Luci.getConfig().getProperty("mqtt_pre", "/Luci/").replaceAll("\\/", "\\/");
		scenarioAndServiceTopicPattern = Pattern.compile("^(("+mqtt_pre+"(\\d+))\\/([\\d]*))$");
		converterPattern = Pattern.compile("^("+mqtt_pre+"convert\\/([\\d]+)).*");
		serviceOutputPattern = Pattern.compile("^"+mqtt_pre+"(\\d+)\\/([\\d]+)\\/outputs\\/(.+).*");
		anyTopicPattern = Pattern.compile("^"+mqtt_pre+"(.+)");
		
		String serverURI = Luci.getConfig().getProperty("mqtt_broker_address", "tcp://localhost:1883");
		try {
			mqtt = new MqttAsyncClient(serverURI, name, new MemoryPersistence());
		} catch (Exception e){
			if (log.isDebugEnabled())
				e.printStackTrace();
			System.err.println(e.getMessage());
		}
	}

	
	/** MQTT **/
	
	protected boolean mqttConnect(){
		MqttConnectOptions o = new MqttConnectOptions();
		o.setConnectionTimeout(Luci.getMQTTConnectionTimeout());
		o.setCleanSession(true);
		mqtt.setCallback(new CallMqttListener((CallSubscriptions)this));
		try {
			mqtt.connect(o);
		} catch (MqttException e){
			return false;
		}
		return true;
		
	}
	
	public MqttAsyncClient getMqttClient(){
		return mqtt;
	}
	
	public void mqttDisconnect(CountDownLatch cdl) throws IOException, MqttException, InterruptedException {
		if (mqtt.isConnected()){
			CountDownLatch cdl2 = new CountDownLatch(1);
			mqtt.disconnect(Luci.getMQTTConnectionTimeout(), new CountsOnMqttActionSuccess(cdl2));
			cdl2.await();
			mqtt.close();
		}
		cdl.countDown();
		
	}
	
	
	public boolean subscribeMQTT(Collection<String> topics){
//		if (Luci.DEBUG) System.out.println(getName() + " subscribe to "+topics);
		return subscribeMQTT(topics.toArray(new String[topics.size()]));
	}
	
	public boolean unsubscribeMQTT(Collection<String> topics){
//		if (Luci.DEBUG) System.out.println(getName() + " unsubscribe from "+topics);
		return unsubscribeMQTT(topics.toArray(new String[topics.size()]));
	}
	
	public boolean subscribeMQTT(String[] topics){
//		if (Luci.DEBUG) System.out.println(getName() + " subscribe to "+topics);
		return unAndSubscribeMQTT(topics, false);
	}
	
	public boolean unsubscribeMQTT(String[] topics){
//		if (Luci.DEBUG) System.out.println(getName() + " unsubscribe from "+topics);
		return unAndSubscribeMQTT(topics, true);
	}
	
	/**
	 * @param SObjID
	 * @return List of parameters of this service instance that are hooked to other service instances or to the scenario
	 * @throws MqttException 
	 */
	private boolean unAndSubscribeMQTT(String[] topics, boolean un) {
		if (topics.length > 0){
			int[] qos = new int[topics.length];
			if (!un){
				for (int i = 0; i < topics.length; i++){
					qos[i] = Luci.qos_0_at_most_once;
				}
			}
			CountDownLatch cdl = new CountDownLatch(1);
			try {
				if (un) mqtt.unsubscribe(topics, null, new CountsOnMqttActionSuccess(cdl));
				else mqtt.subscribe(topics, qos, null, new CountsOnMqttActionSuccess(cdl));
				cdl.await(Luci.getMQTTConnectionTimeout(), TimeUnit.SECONDS);
			} catch (MqttException e){
				if (log.isDebugEnabled()) e.printStackTrace();
				return false;
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			return true;
		}
		return false;
	}
	
	protected abstract boolean loadInstances();

}
