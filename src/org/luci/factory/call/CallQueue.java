package org.luci.factory.call;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.LinkedBlockingQueue;

import org.luci.factory.Factory;
import org.luci.factory.observer.FactoryObserver;
import org.luci.services.ServiceCall;

public abstract class CallQueue extends Factory implements Callable<Object>{
	
	protected LinkedBlockingQueue<ServiceCall> calls;
	protected Map<ServiceCall, CallSupervisor> runningCalls;
	protected Map<ServiceCall, CountDownLatch> wait;
	protected Map<Long, ServiceCall> ignoreOnMQTT;
	protected Map<ServiceCall, Object> results;
	protected String name;
	
	public CallQueue(FactoryObserver o, String name) {
		super(o);
		this.name = name;
		calls = new LinkedBlockingQueue<ServiceCall>();
		runningCalls = new HashMap<ServiceCall, CallSupervisor>();
		wait = new HashMap<ServiceCall, CountDownLatch>();
		ignoreOnMQTT = new HashMap<Long, ServiceCall>();
		results = new HashMap<ServiceCall, Object>();
	}
	
	public String getName() {
		return name;
	}
	
	public LinkedBlockingQueue<ServiceCall> getTaskQueue(){
		return calls;
	}

	public List<ServiceCall> getTasksList(){
		return new ArrayList<ServiceCall>(this.calls);
	}
	
	public List<ServiceCall> getRunningCalls(){
		return new ArrayList<ServiceCall>(this.runningCalls.keySet());
	}
	
	
	public boolean cancelExecution(long SObjID, long input_hash){
		ServiceCall cancel = null;
		for (ServiceCall call: runningCalls.keySet()){
			if (call.getTimestamp().equals(new Timestamp(input_hash)) && call.getSObjID() == SObjID){
				cancel = call;
				break;
			}
		}
		if (cancel != null) return cancelExecution(cancel);
		return false;
	}
	
	public boolean cancelExecution(ServiceCall call){
		CallSupervisor es = runningCalls.remove(call);
		CountDownLatch cdl = wait.remove(call);
		this.setStatus(call, "CNCL");
		if (es != null) es.cancel();
		if (cdl != null) cdl.countDown();
		
		return true;
	}

	public void unreference(CallSupervisor s, Object result){
		List<ServiceCall> keys = new ArrayList<ServiceCall>(runningCalls.keySet());
		for (ServiceCall key: keys){
			if (this.runningCalls.get(key) == s){
				this.runningCalls.remove(key);
				if (wait.containsKey(key)){
					CountDownLatch done = wait.get(key);
					if (done != null){
						results.put(key, result);
						done.countDown();
					}
				}
			}
		}
	}
	
	public void run(ServiceCall call){
		if (!calls.contains(call)){
			calls.add(call);
		}
	}
	
	
	/** adds the SObjID to the task queue if it is not already in there
	 * @param SObjID
	 * @throws SQLException 
	 * @throws UnsupportedEncodingException 
	 * @throws NoSuchAlgorithmException 
	 */
	
	public void run(long SObjID) throws SQLException {
		run(SObjID, null);
	}
	
	public void run(long SObjID, String message) throws SQLException {
		run(SObjID, 0, message);
	}
	
	public void run(long SObjID, long uID, String message) throws SQLException{
		run(new ServiceCall(SObjID, uID,  message, o));
	}
	
	/** adds the SObjID to the task queue if it is not already in there, publishes a RUN message
	 * and waits for a) receiving the RUN message again or that the publishing of RUN failed and
	 * b) that the task finishes; to wait for = blocks the thread. The ServiceCallback object 
	 * that listens to MQTT will count down the CountDownLatch created in this method in order to
	 * make a) work. 
	 * The RUN message is published just in case somebody is listening for it. 
	 * The intention of runAndWaitFor is to make any service invocation as easy as possible for 
	 * all sorts of plugins (actions, services, converters); it takes off the load to implement 
	 * mqtt listeners on their own.
	 * @param SObjID
	 * @param topic
	 * @throws SQLException 
	 * @throws InterruptedException 
	 * @throws ExecutionException 
	 */
	public Object runAndWaitFor(ServiceCall call) throws SQLException, InterruptedException, ExecutionException{
		CountDownLatch cdl = new CountDownLatch(1);
		if (!calls.contains(call)){
			calls.add(call);
			wait.put(call, cdl);
			ignoreOnMQTT.put(call.ID, call);
		}
		cdl.await();
		wait.remove(call);
		Object result = results.get(call);
		if (result instanceof ExecutionException) throw (ExecutionException) result;
		else return result;
	}
	
	public void runAndWaitFor(long SObjID) throws SQLException, InterruptedException, ExecutionException {
		runAndWaitFor(SObjID, 0);
	}
	
	public void runAndWaitFor(long SObjID, long uID) throws SQLException, InterruptedException, ExecutionException{
		Connection con = o.getDatabaseConnection();
		ServiceCall call = new ServiceCall(SObjID, uID, null, o);
		con.close();
		runAndWaitFor(call);
	}
	
	protected abstract void setStatus(Call call, String status);
}
	
	

