package org.luci.factory.call;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;

import org.json.JSONObject;
import org.luci.factory.observer.FactoryObserver;

public abstract class CallSubscriptions extends CallMqttSubscriptions {
	protected Map<String, HashSet<Long>> nonBlockingTopics;
	protected Map<String, HashSet<Long>> blockingTopics;
	protected Map<Long, Map<String, Boolean>> blockingArrival;
	protected Map<Long, Map<String, String>> topicTracker;

	public CallSubscriptions(FactoryObserver o, String name) {
		super(o, name);
		nonBlockingTopics = new ConcurrentHashMap<String, HashSet<Long>>();
		blockingTopics    = new ConcurrentHashMap<String, HashSet<Long>>();
		blockingArrival   = new ConcurrentHashMap<Long, Map<String, Boolean>>();
		topicTracker      = new ConcurrentHashMap<Long, Map<String, String>>();
	}
	
	public String outputKeynameOfTopic(String topic){
		Matcher match = serviceOutputPattern.matcher(topic);
		if (match.matches()) return match.group(3);
		return null;
	}
	
	/** JSON */
	
	public void subscribe(long SObjID, JSONObject json){
		if (json != null){
			List<String> new_topics = new ArrayList<String>();
			List<String> del_topics = new ArrayList<String>();
			@SuppressWarnings("unchecked")
			Set<String> keySet = json.keySet();
			for (String key: keySet){
				String value = json.optString(key);
				if (value != null && value.startsWith(mqtt_pre)){
					String new_topic = subscribeBlocking(value, SObjID, key);
					if (new_topic != null) new_topics.add(new_topic);
				} else {
					String del_topic = unsubscribeBlocking(SObjID, key);
					if (del_topic != null ) del_topics.add(del_topic);
				}
			}
			subscribeMQTT(new_topics);
			unsubscribeMQTT(del_topics);
		}
	}
	
	/** NON BLOCKING */
	
	private void subscribeNonBlocking(Collection<String> topics, long SObjID){
		for (String topic: topics){
			if (!nonBlockingTopics.containsKey(topic)) 
				nonBlockingTopics.put(topic, new HashSet<Long>());
			nonBlockingTopics.get(topic).add(SObjID);
		}
	}
	
	public Collection<String> updateNonBlockingSubscriptions(Collection<String> newTopics, long SObjID) {
		Collection<String> delTopics = new ArrayList<String>();
		if (newTopics.size() > 0) {
			for (Entry<String, HashSet<Long>> entry: nonBlockingTopics.entrySet()){
				HashSet<Long> SObjIDs = entry.getValue();
				if (SObjIDs.contains(SObjID)){
					String topic = entry.getKey();
					if (!newTopics.contains(topic)){
						SObjIDs.remove(SObjID);
						if (SObjIDs.size() == 0){
							nonBlockingTopics.remove(topic);
							delTopics.add(topic);
						}
					} else {
						newTopics.remove(topic);
					}
				}
			}
		}
		subscribeNonBlocking(newTopics, SObjID);
		return delTopics;
	}
	
	public boolean updateNonBlockingAndMQTT(Collection<String> newTopics, long SObjID){
		Collection<String> delTopics = updateNonBlockingSubscriptions(newTopics, SObjID);
		return subscribeMQTT(newTopics) && unsubscribeMQTT(delTopics);
	}
	
	public boolean unsubscribeAllNonBlocking(){
		boolean success = unsubscribeMQTT(nonBlockingTopics.keySet());
		nonBlockingTopics = new HashMap<String, HashSet<Long>>();
		return success;
	}
	
	/** BLOCKING */
	
	public String subscribeBlocking(String topic, long SObjID, String keyname){
		String new_topic = null;
		if (!blockingTopics.containsKey(topic)) {
			blockingTopics.put(topic, new HashSet<Long>());
			new_topic = topic;
		}
		Set<Long> ids = blockingTopics.get(topic);
		ids.add(SObjID);
		if (!blockingArrival.containsKey(SObjID)) blockingArrival.put(SObjID, new ConcurrentHashMap<String, Boolean>());
		Map<String, Boolean> keys = blockingArrival.get(SObjID);
		keys.put(keyname, false);
		if (!topicTracker.containsKey(SObjID)) topicTracker.put(SObjID, new ConcurrentHashMap<String, String>());
		Map<String, String> topics = topicTracker.get(SObjID);
		topics.put(keyname, topic);
		return new_topic;
	}
	
	public String unsubscribeBlocking(long SObjID, String keyname){
		if (blockingArrival.containsKey(SObjID)){
			Map<String, Boolean> keys = blockingArrival.get(SObjID);
			if (keys.containsKey(keyname)) keys.remove(keyname);
			if (keys.size() == 0) blockingArrival.remove(SObjID);
			if (topicTracker.containsKey(SObjID)){
				Map<String, String> topics = topicTracker.get(SObjID);
				String topic = topics.remove(keyname);
				if (topics.size() == 0) topicTracker.remove(SObjID);
				if (blockingTopics.containsKey(topic)){
					Set<Long> ids = blockingTopics.get(topic);
					ids.remove(SObjID);
					if (ids.size() == 0){
						blockingTopics.remove(topic);
						return topic;
					}
				} return null;
			} else throw new IllegalStateException("blockingArrival and topicTracker hash maps got out of sync!");
		}else return null;
		
		
	}
}

