package org.luci.factory.call;

import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.luci.Luci;
import org.luci.comm.clienthandlers.RemoteServiceClientHandler;
import org.luci.factory.ICallableWithID;
import org.luci.services.RemoteService;
import org.luci.services.ServiceCall;

public class CallSupervisor implements Runnable {
	private CallQueue factory;
	private ICallableWithID<?> callable;
	private Future<?> future;
	private String topic;
	private ExecutorService pool;
	

	public CallSupervisor(ExecutorService pool, ICallableWithID<?> s, String topic, CallQueue cp) {
		factory = cp;
		callable = s;
		this.pool = pool;
		this.topic = topic;
	}
	
	public void cancel(){
		if (RemoteService.class.isAssignableFrom(callable.getClass())){
			RemoteService rs = (RemoteService) callable;
			RemoteServiceClientHandler c = rs.getClientHandler();
			c.sendCancel();
		}
		future.cancel(true);
	}

	@Override
	public void run() {
		MqttClient con = factory.getObserver().getMqttClient();
		Object result = null;
		future = pool.submit(callable);
		try {
			result = future.get();
			Call call = callable.getCall();
			if (call.getID() != 0) factory.setStatus(call, "DONE");
			if (con == null) System.out.println("mqtt con is null");
			con.publish(topic, "DONE".getBytes(), Luci.qos_0_at_most_once, false);
			
		} catch (InterruptedException | CancellationException e) {
			factory.setStatus(callable.getCall(), "CNCL");
			if (con != null){
				try {
					con.publish(this.topic, "CNCL".getBytes(), Luci.qos_0_at_most_once, false);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
			e.printStackTrace();
			
		} catch (ExecutionException e) {
			Call call = callable.getCall();
			factory.setStatus(call, "FAIL");
			try {
				con.publish(this.topic, "FAIL".getBytes(), Luci.qos_0_at_most_once, false);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			if (call instanceof ServiceCall){
				if (!(((ServiceCall) call).isSync())) e.printStackTrace();
			} else e.printStackTrace();
			result = e;
		} catch (Exception e) {
			e.printStackTrace();
		}
		factory.unreference(this,result);
	}

}
