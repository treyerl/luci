package org.luci.factory.observer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.TreeMap;

import org.json.JSONException;
import org.luci.comm.action.Action;
import org.luci.comm.action.IGetScenario;
import org.luci.comm.action.IGetServiceInputs;
import org.luci.comm.action.ISetServiceOutputs;
import org.luci.comm.clienthandlers.ClientHandler;
import org.luci.connect.LcJSONObject;
import org.luci.factory.ActionFactory;

public class ActionLoader extends ConverterLoader{
	protected TreeMap<String, ActionFactory> actions;
	protected String[] builtInActions = new String[]{"reload"};
	protected ActionFactory getScenarioAction;
	protected ActionFactory getServiceInputsAction;
	protected ActionFactory setServiceOutputsAction;
	
	protected ActionLoader(Properties p) throws JSONException, NoSuchMethodException, 
			SecurityException, MalformedURLException, IllegalAccessException, 
			IllegalArgumentException, InvocationTargetException{
		super(p);
		this.actions = new TreeMap<String, ActionFactory>(String.CASE_INSENSITIVE_ORDER);
	}
	
	protected int buildActions() {
		this.actions.clear();
		File srcFolder = new File(getSrcPath(),"actions");
		File binFolder = new File(getBinPath(),"actions");
		binFolder.mkdir();

		List<Class<?>> classes = new ArrayList<Class<?>>();
		try {
		for (String builtIn: this.builtInActions){
			classes.add(Class.forName("org.luci.comm.action."+builtIn));
		}
		loader.recompileAllRecursively(srcFolder);
		classes.addAll(loader.loadAll(srcFolder));
		} catch (ClassNotFoundException | IOException e){
			errors.add(e.getMessage());
		}

		for (Class<?> cls : classes) {
			try {
				ActionFactory af = this.createActionFactory(cls);
				actions.put(cls.getSimpleName(), af);
				if (IGetScenario.class.isAssignableFrom(cls)) getScenarioAction = af;
				if (IGetServiceInputs.class.isAssignableFrom(cls)) getServiceInputsAction = af;
				if (ISetServiceOutputs.class.isAssignableFrom(cls)) setServiceOutputsAction = af;
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | 
					InvocationTargetException| SecurityException e) {
				errors.add(e.getMessage());
			}
		}
		
		if (getScenarioAction == null || getServiceInputsAction == null || setServiceOutputsAction == null)
			throw new IllegalStateException("Your set of actions must implement 'IGetScenario', 'IGetServiceInputs', 'ISetServiceOutputs'");

		return classes.size();
	}
	
	protected ActionFactory createActionFactory(Class<?> product) 
			throws InstantiationException, IllegalAccessException, IllegalArgumentException, 
			InvocationTargetException, SecurityException{
		if (Action.class.isAssignableFrom(product))
			return new ActionFactory(product, (FactoryObserver) this);
		else
			throw new IllegalArgumentException(product.getSimpleName()+" is not an action!");
	}
	
	/**
	 * @param a: name of the action
	 * @param h: ClientHandler (for socket and user information)
	 * @param json: the JSONObject holding the full request
	 * @return An instance of the action.
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 * @throws IllegalArgumentException 
	 * @throws NoSuchMethodException 
	 * @throws SecurityException 
	 * @throws FileNotFoundException 
	 * @throws Exception
	 */
	public Action getAction(String a, ClientHandler h, LcJSONObject json) 
			throws SecurityException, NoSuchMethodException, IllegalArgumentException, InstantiationException, 
			IllegalAccessException, InvocationTargetException, FileNotFoundException{
		ActionFactory factory = actions.get(a);
		if (factory != null) {
			return factory.getAction(h, json);
		} else {
			throw new FileNotFoundException("Action '" + a + "' not found!");
		}
	}
	
	public IGetScenario actionGetScenario() throws InstantiationException{
		return (IGetScenario) getScenarioAction.getAction(null, null);
	}
	
	public ISetServiceOutputs getActionSetServiceOutputs(LcJSONObject outputs) 
			throws InstantiationException{
		return (ISetServiceOutputs) setServiceOutputsAction.getAction(null, outputs);
	}
	
	public IGetServiceInputs getActionGetServiceInputs() throws InstantiationException{
		return (IGetServiceInputs) getServiceInputsAction.getAction(null, null);
	}
}
