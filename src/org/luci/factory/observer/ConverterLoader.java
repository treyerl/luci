package org.luci.factory.observer;

import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.TreeMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.luci.connect.DirectStreamWriter;
import org.luci.connect.GeometryInfo;
import org.luci.connect.LcOutputStream;
import org.luci.converters.Converter;
import org.luci.converters.IMultiStreamReader;
import org.luci.converters.UnitConverter;
import org.luci.converters.geometry.GeometryReader;
import org.luci.converters.geometry.GeometrySelector;
import org.luci.converters.geometry.GeometryWriter;
import org.luci.converters.geometry.IDatabaseGeometryObjectReader;
import org.luci.converters.geometry.IDatabaseGeometryObjectWriter;
import org.luci.converters.geometry.IGeoJSONReader;
import org.luci.converters.geometry.IGeoJSONWriter;
import org.luci.converters.geometry.JSONGeometryReader;
import org.luci.factory.GeometryConverterFactory;
import org.luci.factory.MultiFileGeometryConverterFactory;
import org.luci.factory.StreamWriterFactory;
import org.luci.factory.UnitConverterFactory;
import org.luci.utilities.Projection;

public class ConverterLoader extends DatabaseLoader{
	protected TreeMap<String, GeometryConverterFactory> geometryConverters;
	protected TreeMap<String, StreamWriterFactory> fileConverters;
	protected TreeMap<String, UnitConverterFactory> unitConverters;
	
	
	protected ConverterLoader(Properties p) throws JSONException, NoSuchMethodException, SecurityException, 
	MalformedURLException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		super(p);
		geometryConverters = new TreeMap<String, GeometryConverterFactory>(String.CASE_INSENSITIVE_ORDER);
		fileConverters = new TreeMap<String, StreamWriterFactory>(String.CASE_INSENSITIVE_ORDER);
		unitConverters = new TreeMap<String, UnitConverterFactory>(String.CASE_INSENSITIVE_ORDER);
	}

	protected int buildGeometryConverters(){
		geometryConverters.clear();
		File srcFolder = new File(new File(getSrcPath(),"converters"), "geometry");
		File binFolder = new File(new File(getBinPath(),"converters"), "geometry");
		binFolder.mkdir();
		int i = 0;
		for (String conv: srcFolder.list(new FilenameFilter(){
					@Override
					public boolean accept(File dir, String s){
						return !s.startsWith(".");
					}
				})
			){
			File src = new File(srcFolder, conv);
			File[] jars = src.listFiles(new FileFilter(){
					@Override
					public boolean accept(File o){
						return o.isFile() && o.getName().endsWith(".jar");
					}
				});
			try {
				loader.recompileAll(src, jars, true /*recursively*/);
				Class<?> reader = loader.load(new File(src, "Reader.java"));
				Class<?> writer = loader.load(new File(src, "Writer.java"));
				
				if (Converter.class.isAssignableFrom(reader) && Converter.class.isAssignableFrom(writer)){
					if (IMultiStreamReader.class.isAssignableFrom(reader))
						 geometryConverters.put(conv, createMultiFileGeometryConverterFactory(reader, writer));
					else geometryConverters.put(conv, createGeometryConverterFactory(reader, writer));
					i++;
				}
			} catch (IOException | InstantiationException | IllegalAccessException | 
					IllegalArgumentException | InvocationTargetException | ClassNotFoundException e) {
				e.printStackTrace();
				errors.add(e.getMessage());
			}
		}
		return i;
	}
	
	protected JSONObject buildFileConverters(){
		fileConverters.clear();
		JSONObject result = new JSONObject();
		File path = new File(new File(getSrcPath(),"converters"), "file");
		File bins = new File(new File(getBinPath(),"converters"), "file");
		bins.mkdir();
		for (String conv: path.list()){
			File srcFolder = new File(path, conv);

			File binFolder = new File(bins, conv);
			binFolder.mkdirs();
			try{

				HashMap<String, Class<?>> writerClasses = loader.recompileAndLoadAll(srcFolder, passFilter());
				fileConverters.put(conv, createFileConverterFactory(writerClasses));
				result.put(conv, +writerClasses.size());
			} catch(IOException | ClassNotFoundException e){
				e.printStackTrace();
				errors.add(e.getMessage());
			}
		}
		return result;
	}
	
	protected FilenameFilter exclude(final String start){
		return new FilenameFilter(){
			@Override
			public boolean accept(File dir, String current){
				return !current.startsWith(start);
			}
		};
	}
	
	protected FilenameFilter passFilter(){
		return new FilenameFilter(){
			@Override
			public boolean accept(File dir, String name) {
				return true;
			}
		};
	}
	
	protected int buildUnitConverters() throws IOException, ClassNotFoundException {
		unitConverters.clear();
		File srcFolder = new File(new File(getSrcPath(),"converters"), "unit");
		for (Class<?> unit: loader.recompileAndLoadAll(srcFolder)){
			unitConverters.put(unit.getSimpleName(), createUnitConverterFactory(unit));
		}
		return unitConverters.size();
	}
	
	public String getMultiFileMainFormat(Set<String> formats){
		if (formats.size() == 1){
			String format = new ArrayList<String>(formats).get(0);
			if (geometryConverters.containsKey(format)) return format;
		} else {
			for (GeometryConverterFactory cf: geometryConverters.values()){
				if (MultiFileGeometryConverterFactory.class.isAssignableFrom(cf.getClass())){
					MultiFileGeometryConverterFactory mcf = (MultiFileGeometryConverterFactory) cf;
					if (mcf.fileFormatSuffixes().containsAll(formats))
						return mcf.getMainFormatSuffix();
				}
			}
		}
		return null;
	}
	
	protected GeometryConverterFactory createGeometryConverterFactory(Class<?> readerClass, Class<?> writerClass) {
		return new GeometryConverterFactory((FactoryObserver) this, readerClass, writerClass);
	}
	
	protected StreamWriterFactory createFileConverterFactory(HashMap<String, Class<?>> writerClasses) {
		return new StreamWriterFactory((FactoryObserver) this, writerClasses);
	}
	
	protected MultiFileGeometryConverterFactory createMultiFileGeometryConverterFactory(Class<?> readerClass, Class<?> writerClass) 
			throws InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		return new MultiFileGeometryConverterFactory((FactoryObserver) this, readerClass, writerClass);
	}
	
	protected UnitConverterFactory createUnitConverterFactory(Class<?> unit){
		return new UnitConverterFactory((FactoryObserver) this, unit);
	}
		
	/**Creates a converter that reads geometry (and attributes associated with the geometry) into the geometry and attributes databases. 
	 * @param name: (String) name of the format
	 * @param h: ClientHandler (for streaming if necessary).
	 * @param id: service instance id (SObjID)
	 * @param t: Timestamp to be used as the timestamp in the timestamp field of the geometry data base.
	 * @param info: header information for binary / stream formats; the information to be stored for json formats.
	 * @param pc: Projection object holding the global projection information (e.g. lat/lon to local coordinate system). 
	 * @return
	 * @throws FileNotFoundException
	 * @throws SecurityException
	 * @throws NoSuchMethodException
	 * @throws IllegalArgumentException
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 */
	public GeometryReader getGeometryReader(String name, int ScID, long uid, Timestamp t, Projection pc, 
			JSONArray errors) throws FileNotFoundException, InstantiationException{
		GeometryConverterFactory factory = geometryConverters.get(name);
		if (factory != null) {
			return factory.getGeometryReader((FactoryObserver) this, ScID, uid, t, pc, errors);
		} else {
			throw new FileNotFoundException("GeometryReader '" + name + "' not found!");
		}
	}
	
	public JSONGeometryReader getJSONGeometryReader(String name, int ScID, long uid, Timestamp t, 
			Projection pc, JSONArray errors, GeometryInfo geometry) 
			throws InstantiationException, FileNotFoundException{
		GeometryConverterFactory factory = geometryConverters.get(name);
		if (factory != null){
			return factory.getJSONGeometryReader((FactoryObserver) this, geometry, ScID, uid, t, pc, errors);
		} else throw new FileNotFoundException("GeometryReader '" + name + "' not found!");
	}
	
	public Class<?> getGeometryReaderClass(String name){
		return geometryConverters.get(name).getReaderClass();
	}
	
	/**Creates a stub version of the geojson converter, i.e. initalizes a new geojson converter 
	 * with null for all initialization parameters except for the projection. This is intended for using the geojson conversion
	 * for other tables than the geometry table (e.g. geo reference of a scenario, geometry being used as a service input parameter, location of a user etc).
	 * @param pc: Projection object holding the global projection information (e.g. lat/lon to local coordinate system). 
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws InvocationTargetException
	 * @throws NoSuchMethodException
	 * @throws SecurityException
	 * @throws FileNotFoundException
	 */
	public IGeoJSONReader getGeoJSON_ToDb(Projection pc) 
			throws InstantiationException, FileNotFoundException{
		GeometryConverterFactory factory = geometryConverters.get("GeoJSON");
		if (factory != null) {
			Class<?> readerClass = factory.getReaderClass();
			try {
				Constructor<?> ct = readerClass.getDeclaredConstructor(FactoryObserver.class, 
						GeometryInfo.class, int.class, long.class, Timestamp.class, Projection.class, 
						JSONArray.class);
				return (IGeoJSONReader) ct.newInstance(this, null, 0, 0, null, pc, null);
			} catch (NoSuchMethodException | SecurityException | IllegalAccessException | 
					IllegalArgumentException | InvocationTargetException e) {
				throw new InstantiationException(e.toString());
			}
		} else {
			throw new FileNotFoundException("No GeoJSONReader found!");
		}
	}
	
	/**Creates a converter that reads information from the service output table and encodes it in the requested format.
	 * @param name: (String) name of the existing format
	 * @param format (String) name of the requested format
	 * @param h: ClientHandler (for streaming if necessary).
	 * @param id: service instance id (SObjID)
	 * @param input_hash: (String) used to select the output that was generated with the input represented by this very hash.
	 * @param trs: TimeRangeSelection object  
	 * @return
	 * @throws FileNotFoundException
	 * @throws SecurityException
	 * @throws NoSuchMethodException
	 * @throws IllegalArgumentException
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 */
	public DirectStreamWriter getStreamWriter(String fromFormat, String toFormat, String fromChecksum) 
			throws FileNotFoundException, InstantiationException {
		StreamWriterFactory factory = fileConverters.get(fromFormat);
		if (factory != null) {
			return factory.getFileWriter((FactoryObserver) this, toFormat, fromChecksum);
		} else {
			throw new FileNotFoundException("FileWriter '"+fromFormat+" to "+toFormat+"' not found!");
		}
	}
	
	/**Creates a converter that reads information from the geometry and attributes tables and encodes it in 
	 * the requested format and stores it to a file named with its checksum.
	 * @param name: (String) name of the format
	 * @param gh: GeometryHash object
	 * @param at: AttributesHash object
	 * @param trs: TimeRangeSelection object
	 * @param pc: Projection
	 * @return GeometryWriter
	 * @throws FileNotFoundException
	 * @throws SecurityException
	 * @throws NoSuchMethodException
	 * @throws IllegalArgumentException
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws InvocationTargetException
	 * @throws SQLException
	 */
	public GeometryWriter getGeometryWriter(String name, GeometrySelector gs, Projection pc, 
			List<LcOutputStream> outputStreams, JSONArray errors) 
			throws FileNotFoundException, InstantiationException {
		GeometryConverterFactory factory = this.geometryConverters.get(name);
		if (factory != null) {
			return factory.getGeometryWriter((FactoryObserver) this, gs, pc, outputStreams, errors);
		} else {
			throw new FileNotFoundException("GeometryWriter '" + name + "' not found!");
		}
	}
	
	/**Creates a stub version of the geojson converter, i.e. initalizes a new geojson converter 
	 * with null for all initialization parameters except for the projection. This is intended for using the geojson conversion
	 * for other tables than the geometry table (e.g. geo reference of a scenario, geometry being used as a service input parameter, location of a user etc).
	 * @param pc: Projection object holding the global projection information (e.g. lat/lon to local coordinate system). 
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws InvocationTargetException
	 * @throws NoSuchMethodException
	 * @throws SecurityException
	 * @throws FileNotFoundException
	 * @throws SQLException
	 */
	public IGeoJSONWriter getGeoJSON_FromDb(Projection pc) throws InstantiationException, FileNotFoundException{
		GeometryConverterFactory factory = geometryConverters.get("GeoJSON");
		if (factory != null) {
			Class<?> writerClass = factory.getWriterClass();
			try {
				Constructor<?> ct = writerClass.getDeclaredConstructor(FactoryObserver.class, 
						GeometrySelector.class, Projection.class, List.class, JSONArray.class);
				return (IGeoJSONWriter) ct.newInstance(this, null, pc, null, null);
			} catch (NoSuchMethodException | SecurityException | IllegalAccessException | 
					IllegalArgumentException | InvocationTargetException e) {
				throw new InstantiationException(e.toString());
			}
		} else {
			throw new FileNotFoundException("No GeoJSONReader found!");
		}
	}
	
	public IDatabaseGeometryObjectReader getDatabaseGeometryObjectReader() 
			throws InstantiationException{
		try {
			return geometryObjectReader.newInstance();
		} catch (IllegalAccessException e) {
			throw new InstantiationException(e.toString());
		}
	}
	
	public IDatabaseGeometryObjectWriter getDatabaseGeometryObjectWriter() 
			throws InstantiationException, IllegalAccessException{
		return geometryObjectWriter.newInstance();
	}
	
	public UnitConverter getUnitConverter(String converterName, String literal) 
			throws NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, 
			IllegalArgumentException, InvocationTargetException, FileNotFoundException{
		UnitConverterFactory factory = unitConverters.get(converterName);
		if (factory != null) return factory.getUnitConverter(literal);
		else throw new FileNotFoundException("Unit converter '"+converterName+"' not found!");
	}
	
	public UnitConverter getUnitConverter(String converterName, double amount, String unitSign) 
			throws NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, 
			IllegalArgumentException, InvocationTargetException, FileNotFoundException{
		UnitConverterFactory factory = unitConverters.get(converterName);
		if (factory != null) return factory.getUnitConverter(amount, unitSign);
		else throw new FileNotFoundException("Unit converter '"+converterName+"' not found!");
	}
}
