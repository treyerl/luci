package org.luci.factory.observer;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.json.JSONObject;
import org.luci.Luci;
import org.luci.converters.geometry.IDatabaseGeometryObjectReader;
import org.luci.converters.geometry.IDatabaseGeometryObjectWriter;
import org.slf4j.Logger;

import com.jolbox.bonecp.BoneCP;
import com.jolbox.bonecp.BoneCPConfig;

public class DatabaseLoader {
	protected FileClassLoader loader;
	protected Class<IDatabase> dataModelImplementation;
	protected Class<IDatabaseGeometryObjectReader> geometryObjectReader;
	protected Class<IDatabaseGeometryObjectWriter> geometryObjectWriter;
	protected BoneCP connectionPool = null;
	protected Properties properties;
	protected Logger log;
	protected String info = "";
	protected final File src;
	protected final File bin;
	protected List<String> errors;
	private List<String> lastFrom = new ArrayList<String>(); 
	
	protected DatabaseLoader(Properties p) 
			throws NoSuchMethodException, SecurityException, MalformedURLException, IllegalAccessException, 
			IllegalArgumentException, InvocationTargetException {
		log = Luci.createLoggerFor("Observer");
		this.properties = p;
		File plugins = new File(Luci.getWorkingDir(), "plugins");
		src = new File(plugins, "src");
		bin = new File(plugins, "bin");
		errors = new ArrayList<String>();
	}
	
	public List<String> getErrors(){
		return errors;
	}
	
	public BoneCP getDbConnectionPool(){
		return connectionPool;
	}
	
	protected void info(String s){
		this.log.info(s);
		System.out.println(s);
	}
	
	protected void infoGUI(String s){
		info += s + "\r\n";
	}
	
	protected void info(JSONObject json){
		String line = json.toString();
		this.log.info(line);
		System.out.println(line);
	}
	
	
	public String getInfo(){
		return info;
	}
	
	public IDatabase dataModelImplementation() throws InstantiationException{
		try {
			return dataModelImplementation.newInstance();
		} catch (IllegalAccessException e) {
			throw new InstantiationException(e.toString());
		}
	}
	
	protected File getSrcPath(){
		return src;
	}
	
	protected File getBinPath(){
		return bin;
	}
	
	public Connection getDatabaseConnection() throws SQLException{
		if (connectionPool.getTotalFree() == 0) {
			this.log.warn("no free DB connections");
			if (this.log.isDebugEnabled()){
				System.err.println("no free DB connections; call before this one:"+lastFrom+", #used:"+connectionPool.getTotalLeased());
			}
		}
		StackTraceElement[] stack = Thread.currentThread().getStackTrace();
		StackTraceElement[] stack2  = new StackTraceElement[]{stack[2], stack[3]};
		lastFrom.add(java.util.Arrays.toString(stack2));
		if (lastFrom.size() > 10) lastFrom.remove(0);
//		System.err.println("DB con requested from: "+lastFrom);
		/* 
		StackTraceElement st = Thread.currentThread().getStackTrace()[2];
		System.out.println("Total Free: "+connectionPool.getTotalFree() 
				+" called from: "+st.getMethodName()
				+", "+st.getClassName()); */ 
		return connectionPool.getConnection();
	}
	
	public String getDbType(){
		return dataModelImplementation.getSimpleName();
	}
	
	public JSONObject getDbInfo() throws SQLException, InstantiationException, IllegalAccessException{
		IDatabase db = dataModelImplementation.newInstance();
		Connection con = getDatabaseConnection();
		JSONObject info = null;
		try {
			info = db.getDBInfo(con);
		} catch (SQLException e){
			con.close();
			throw e;
		}
		con.close();
		return info;
	}
	
	@SuppressWarnings("unchecked")
	protected void loadDataModelImplementation() 
			throws SQLException, IOException, InstantiationException, IllegalAccessException, ClassNotFoundException {		
		dataModelImplementation = loader.loadDataModelImplementation();
		geometryObjectReader = (Class<IDatabaseGeometryObjectReader>) loader.getDatabaseGeometryObjectReaderClass();
		geometryObjectWriter = (Class<IDatabaseGeometryObjectWriter>) loader.getDatabaseGeometryObjectWriterClass();
		setupDBConnectionPool();
		IDatabase data = dataModelImplementation.newInstance();
		Connection con = getDatabaseConnection();
		data.setupTables(con, log);
		info("==========");

	}
	
	public String resetTables() throws InstantiationException, IllegalAccessException, SQLException{
		IDatabase db = dataModelImplementation.newInstance();
		Connection con = getDatabaseConnection();
		String msg = db.resetTables(con, log);
		con.close();
		return msg;
	}

	protected void setupDBConnectionPool() throws SQLException, InstantiationException , IllegalAccessException , ClassNotFoundException{
		if (dataModelImplementation == null)
			throw new SQLException("Cannot setup connection pool without a valid Data Model Implementation");
		
		Properties lconf = Luci.getConfig();
		BoneCPConfig config = new BoneCPConfig();
		
		IDatabase data;
		String url = "jdbc://unknown";
		try {
			data = dataModelImplementation.newInstance();
			// load the JDBC driver (make sure it is in your class path!)
			url = data.getJdbcUrl(lconf.getProperty("dbHost", "localhost"), 
								  lconf.getProperty("dbPort", "7654"), 
								  lconf.getProperty("dbName", "luci"));
			Class.forName(data.getJdbcClassname());
			config.setJdbcUrl(url);
			Properties t = new Properties();
			t.put("user", lconf.getProperty("dbUser", "luci"));
			t.put("password", lconf.getProperty("dbPass", "1234"));
			@SuppressWarnings("unused")
			Connection test = DriverManager.getConnection(url, t);
		} catch (SQLException e) {
			log.error(e.getMessage());
			throw new SQLException("Unable to establish database connection using "+url+"\r\nEither install a database or edit the config file.\r\n"+e.getMessage());
		}
		
		config.setUsername(lconf.getProperty("dbUser"));
		config.setPassword(lconf.getProperty("dbPass"));
		Runtime runtime = Runtime.getRuntime();
		int cpus = runtime.availableProcessors();
		cpus *= 2;
		cpus = Math.min(cpus, 8);
		int poolMax = cpus + 5;
		config.setMinConnectionsPerPartition(Integer.parseInt(lconf.getProperty("poolMin", ""+cpus)));
		config.setMaxConnectionsPerPartition(Integer.parseInt(lconf.getProperty("poolMax", ""+poolMax)));
		config.setPartitionCount(Integer.parseInt(lconf.getProperty("poolPartitionCount", "1")));
		info(config.toString());
		infoGUI(config.toString().split(",")[0]);
		connectionPool = new BoneCP(config); 
	}
	
	public void closeDBConnectionPool(){
		connectionPool.close();
		connectionPool.shutdown();
	}
}
