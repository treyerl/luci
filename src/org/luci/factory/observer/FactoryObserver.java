package org.luci.factory.observer;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

import org.eclipse.paho.client.mqttv3.MqttAsyncClient;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.json.JSONException;
import org.json.JSONObject;
import org.luci.Luci;
import org.luci.converters.UnitConverter;
import org.luci.factory.ActionFactory;
import org.luci.factory.GeometryConverterFactory;
import org.luci.factory.ServiceFactoryThread;

/**
 * The factory-observer will scan the directories "plugins/actions", "plugins/services", 
 * "plugins/converters" for java classes, compile them and hold hash tables with the 
 * action, service and converter factories accessible by the simple class name of the found classes. 
 * Service registered remotely (from a client) are being recorded in the services hash table as well.
 * In order to group similar code the above described functionality is implemented in different 
 * loader classes from which the FactoryObserver inherits in the following order:
 * DatabaseLoader: 	loads the data model implementation specified in Luci's config file (e.g. PostGIS or SpatiaLite etc.)
 * ConverterLoader: loads all converters according to the data model implementation 
 * 					(e.g. "plugins/converters/GeoJSON/PostGIS/Reader.java")
 * ServiceLoader: 	loads all services (= compiles and load them, then instantiate one class each 
 * 				  	to tell its factory what in and outputs it requires)
 * ActionLoader: 	loads all actions similar to services except that also "built-in" actions that cannot be recompiled 
 * 					but are part of Luci's build path are being also instantiated and added to the action list 
 * 					(necessary for actions like reload that recompiles and reloads all plugins)
 * HashLoader:		holds HashMaps of all hashes that have been requested so far and a cleanup function to remove the oldest hashes.
 * 					Includes GetHash and UpdateHash methods.
 * 
 * @author Lukas Treyer
 * 
 */
public class FactoryObserver extends RemoteRegistrar{
	private MqttAsyncClient asyncMqtt;
	
	/**
	 * @param Properties
	 *            p, given by the MiddleWare-Class. Contains all the config
	 *            properties from the config file.
	 * @throws InvocationTargetException 
	 * @throws IllegalArgumentException 
	 * @throws IllegalAccessException 
	 * @throws SecurityException 
	 * @throws NoSuchMethodException 
	 * @throws Exception 
	 * @throws JSONException 
	 * @throws IOException 
	 * @throws SQLException 
	 * @throws InstantiationException 
	 * @throws ClassNotFoundException 
	 */
	public FactoryObserver(Properties p, MqttClient m, MqttAsyncClient ac) throws JSONException, NoSuchMethodException, 
	SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, ClassNotFoundException, 
	InstantiationException, SQLException, IOException {
		super(p, m);
		this.asyncMqtt = ac;
		this.build(Integer.parseInt("11111", 2));
		UnitConverter byteUnit = getUnitConverter("ByteUnit", p.getProperty("cache_size", "100MB"));
		long size = (long) byteUnit.getInUnit("B");
	}

	/** 
	 * @return properties given from Luci's config file.
	 */

	public Properties getProperties() {
		return properties;
	}
	
	/**Builds all plugins based on the binary selector parameter.
	 * @param binary_selector: everthing from 000 to 111 in order to select "actions", "services", "converters"
	 * @return JSONObject holding compile and load counts.
	 * @throws JSONException
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 * @throws IOException 
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 * @throws SecurityException 
	 * @throws InvocationTargetException 
	 * @throws IllegalArgumentException 
	 * @throws NoSuchMethodException 
	 */
	public synchronized JSONObject build(int binary_selector) 
			throws JSONException, SQLException, ClassNotFoundException, InstantiationException, 
			IllegalAccessException, IOException, IllegalArgumentException, InvocationTargetException, SecurityException, NoSuchMethodException{
		JSONObject j = new JSONObject();
		int UnitConverters = 0b00001;
		int FileConverters = 0b00010;
		int GeomConverters = 0b00100;
		int Services = 0b01000;
		int Actions  = 0b10000;
		loader =  new FileClassLoader(getSrcPath(), getBinPath());
		loadDataModelImplementation();
		/* this order is crucial*/
		if ((binary_selector & UnitConverters) == 1) j.put("unit converters", buildUnitConverters());
		if ((binary_selector & FileConverters) == 2) j.put("file converters", buildFileConverters());
		if ((binary_selector & GeomConverters) == 4) j.put("geometry converters", buildGeometryConverters());
		if ((binary_selector & Actions) == 16) 		 j.put("actions", buildActions());
		if ((binary_selector & Services) == 8) 		 j.put("services", buildServices());
		JSONObject plugins = new JSONObject().put("plugins", j);
		info(plugins);
		return plugins;
	}
	
	/**
	 * @param a: (String) name of the action factory
	 * @return ActionFactory
	 */
	public ActionFactory getActionFactory(String a){
		return actions.get(a);
	}
	
	/**
	 * @param s: (String) name of the service factory
	 * @return ServiceFactory
	 */
	public ServiceFactoryThread getServiceFactory(String s){
		return this.services.get(s);
	}
	
	/**
	 * @param c: (String) name of the converter factory
	 * @return ConverterFactory
	 */
	public GeometryConverterFactory getConverterFactory(String c){
		return this.geometryConverters.get(c);
	}
	
	/**
	 * @return a set of all action names
	 */
	public Set<String> getActionSet(){
		return this.actions.keySet();
	}
	
	/**
	 * @return a set of all service names
	 */
	public Set<String> getServiceSet(){
		return this.services.keySet();
	}
	
	/**
	 * @return a set of all converter names
	 */
	public Set<String> getConverterSet(){
		return this.geometryConverters.keySet();
	}
	
	
	/**
	 * @return Luci's main thread pool
	 */
	public ExecutorService getThreadPool() {
//		StackTraceElement[] stack = Thread.currentThread().getStackTrace();
//		System.out.println("getThreadPool() from "+Arrays.toString(stack)+" - "+threadPool.getPoolSize());
		return threadPool;
	}
	
	/**
	 * @return the paho mqtt client
	 */
	public MqttClient getMqttClient(){
		return mqtt;
	}
	
	/**
	 * @return the paho asynchronous mqtt client
	 */
	public MqttAsyncClient getAsyncMqttClient(){
		return asyncMqtt;
	}
	
	/**Iterates over all service factories and waits until all of their mqtt clients are disconnected.
	 * @throws InterruptedException 
	 * @throws MqttException 
	 * @throws IOException 
	 * @throws Exception
	 */
	public void closeMqttConnections() throws IOException, MqttException, InterruptedException {
		CountDownLatch cdl = new CountDownLatch(services.size());
		for (ServiceFactoryThread service: services.values()){
			service.mqttDisconnect(cdl);
		}
		cdl.await(Luci.getMQTTConnectionTimeout(), TimeUnit.SECONDS);
	}
}
