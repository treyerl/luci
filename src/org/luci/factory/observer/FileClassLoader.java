package org.luci.factory.observer;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.AccessController;
import java.security.PrivilegedExceptionAction;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.jdt.internal.compiler.batch.Main;
import org.eclipse.jdt.internal.compiler.impl.CompilerOptions;
import org.luci.Luci;
import org.luci.converters.geometry.IDatabaseGeometryObjectReader;
import org.luci.converters.geometry.IDatabaseGeometryObjectWriter;

/**
 * FileClassLoader deals with all the file system specific tasks before loading
 * a class.
 * 
 * @author Lukas Treyer <treyer@arch.ethz.ch>
 * 
 */
public class FileClassLoader extends ClassLoader {
	private File bin;
	private File src;
	private Path srcRoot;
	private Path binRoot;
	private File dbSrcPath;
	private String classPath;
	private Main j;
	private String dbType;
	private Class<?> r;
	private Class<?> w;
	
	public FileClassLoader(File src, File bin) throws NoSuchMethodException, SecurityException, MalformedURLException, 
			IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		super(getSystemClassLoader());
		this.bin = bin;
		this.src = src;
		srcRoot = Paths.get(src.getAbsolutePath());
		binRoot = Paths.get(bin.getAbsolutePath());
		classPath = System.getProperty("java.class.path");
		classPath += File.pathSeparator + bin.getAbsolutePath();
		
		// list jdbc jars
		dbType = Luci.getConfig().getProperty("dbType", "PostGIS");
		dbSrcPath = new File(new File(src, "database"), dbType);
		File[] jars = dbSrcPath.listFiles(filterJarFiles());
		if (jars.length == 0) throw new RuntimeException("No JDBC-Drivers found for '"+dbType+"'!");
		// add them to the classpath and load them
		classPath = loadJars(jars);
		Map<String, String> defaultCompileOptions = new HashMap<String, String>();
		defaultCompileOptions.put(CompilerOptions.OPTION_Compliance, CompilerOptions.VERSION_1_7);
		j = new Main(
				new PrintWriter(System.out),
				new PrintWriter(System.err),
				false /*systemexit*/, 
				defaultCompileOptions, 
				null /*progress*/);
		j.destinationPath = bin.getAbsolutePath();

	}
	
	public String loadJars(File[] jars) 
			throws MalformedURLException, NoSuchMethodException, SecurityException, IllegalAccessException, 
			IllegalArgumentException, InvocationTargetException{
		
		String tempClassPath = classPath;
		if (jars.length > 0){
			URL[] jarURLs = new URL[jars.length];
			int i = 0;
			for (File jar: jars) {
				tempClassPath += File.pathSeparator + jar.getAbsolutePath();
				jarURLs[i++] = jar.toURI().toURL();
			}
			
			// load jars
			URLClassLoader sysLoader = (URLClassLoader)ClassLoader.getSystemClassLoader();
			Class<URLClassLoader> sysClass = URLClassLoader.class;
			Method sysMethod = sysClass.getDeclaredMethod("addURL",new Class[] {URL.class});
			sysMethod.setAccessible(true);
			for (URL jarURL: jarURLs) sysMethod.invoke(sysLoader, jarURL);
		}
		return tempClassPath;
	}
	
	public File getClassFile(File srcFile){
		Path srcPath = Paths.get(srcFile.getAbsolutePath());
		return new File(binRoot.toFile(), srcRoot.relativize(srcPath).toString().replace(".java", ".class"));
	}
	
	public File getClassFile(String binaryName){
		File classFile = new File(bin, binaryName.replace('.', File.separatorChar).concat(".class"));
		if (classFile.exists()) return classFile;
		return null;
	}
	
	public File getJavaFile(String binaryName) {
		File javaFile = new File(src, binaryName.replace('.', File.separatorChar).concat(".java"));
		if (javaFile.exists()) return javaFile;
		return null;
	}
	
	public File getJavaFile(File classFile){
		Path srcPath = Paths.get(classFile.getAbsolutePath());
		return new File(srcRoot.toFile(), binRoot.relativize(srcPath).toString().replace(".class", ".java"));
	}
	
	public File getPackageFolder(String binaryName) {
		File pkg = new File(src, binaryName.substring(0, binaryName.lastIndexOf(".")).replace('.', File.separatorChar));
		if (pkg.exists()) return pkg;
		return null;
	}
	
	public String getBinaryName(File src) throws MalformedURLException{
		Path srcPath = Paths.get(src.getAbsolutePath());
		Path relPath = srcRoot.relativize(srcPath);
		return relPath.toString().replace(File.separator, ".").replace(".java", "").replace(".class", "");
	}
	
	@SuppressWarnings({ "unchecked" })
	public Class<IDatabase> loadDataModelImplementation() throws IOException, ClassNotFoundException{
		recompileAll(dbSrcPath);
		r = load(new File(dbSrcPath, "GeometryObjectReader"));
		w = load(new File(dbSrcPath, "GeometryObjectWriter"));
		if (! IDatabaseGeometryObjectReader.class.isAssignableFrom(r) ) 
			throw new RuntimeException(r.getName()+" must extend "+IDatabaseGeometryObjectReader.class.getName()+"!");
		if (! IDatabaseGeometryObjectWriter.class.isAssignableFrom(w) ) 
			throw new RuntimeException(w.getName()+" must extend "+IDatabaseGeometryObjectWriter.class.getName()+"!");
		return (Class<IDatabase>) load(new File(dbSrcPath, dbType));
	}
	
	public Class<?> getDatabaseGeometryObjectReaderClass(){
		return r;
	}
	
	public Class<?> getDatabaseGeometryObjectWriterClass(){
		return w;
	}
	
	public void deleteClassFile(File srcFile) throws MalformedURLException{
		File classFile = getClassFile(srcFile);
		File classFileParent = classFile.getParentFile();
		classFileParent.mkdirs();
		// deleting all class files (incl. myClass$8.class and myClass$myInnerClass.class files)
		for (File f: classFileParent.listFiles(filterStartingWith(srcFile.getName().replace(".java", "")))){
			f.delete();
		}
	}
	
	public void deleteAllClassFiles(File srcFolder, boolean recursive) throws MalformedURLException{
		//System.out.println(srcFolder);
		for (File f: srcFolder.listFiles(new FileFilter(){@Override
		public boolean accept(File o){return o.isFile();}} )) deleteClassFile(f);
		if (recursive) for (File f: srcFolder.listFiles(new FileFilter(){@Override
		public boolean accept(File o){return o.isDirectory();}})) deleteAllClassFiles(f, recursive);
	}
	
	public void recompileAll(File srcFolder) throws IOException {
		recompileAll(srcFolder, null, false);
	}
	
	public void recompileAllRecursively(File srcFolder) throws IOException{
		recompileAll(srcFolder, null, true);
	}
	
	void recompileAll(File srcFolder, File[] jars, boolean recursive) throws IOException{
		deleteAllClassFiles(srcFolder, recursive);
		compileAll(srcFolder, jars, recursive);
	}
	
	public void recompileAll(File srcFolder, FilenameFilter filter) throws IOException{
		deleteAllClassFiles(srcFolder, false);
		compileAll(srcFolder, filter);
	}
	
	public void recompile(File srcFile) throws MalformedURLException{
		deleteClassFile(srcFile);
		compile(srcFile);
	}
	
	public void compile(File srcFile) {
		File classFile = getClassFile(srcFile);
		if (!classFile.exists()){
			j.compile(new String[]{"-classpath", classPath, "-1.7", "-nowarn", srcFile.getAbsolutePath()});
		}
	}
	
	public File compile(String binaryName) {
		File javaFile = getJavaFile(binaryName);
		if (javaFile != null) compile(javaFile);
		return getClassFile(binaryName);
	}
	
	private void recursivelyAddJavaFiles(List<File> files, File directory){
		File[] fileArray = directory.listFiles(new FileFilter(){@Override
		public boolean accept(File o){return o.isFile() && o.getName().endsWith(".java");}});
		List<File> javaFiles = new ArrayList<File>(Arrays.asList(fileArray));
		files.addAll(javaFiles);
		for (File d: directory.listFiles(new FileFilter(){@Override
		public boolean accept(File o){return o.isDirectory();}})) 
			recursivelyAddJavaFiles(files, d);
	}
	
	public void compileAll(File srcFolder, File[] jars, boolean recursive) throws IllegalArgumentException, MalformedURLException{
		if (getPackage(getBinaryName(srcFolder)) == null){
			definePackage(getBinaryName(srcFolder), null, null, null, null, null, null, null);
			List<File> sources = new ArrayList<File>();
			if (recursive) recursivelyAddJavaFiles(sources, srcFolder);
			else {
				File[] fileArray = srcFolder.listFiles(new FileFilter(){@Override
				public boolean accept(File o){return o.isFile() && o.getName().endsWith(".java");}});
				sources = Arrays.asList(fileArray);
			}
			
			try {
				String classPath = this.classPath;
				if (jars!= null) classPath = loadJars(jars);
				List<String> args = new ArrayList<String>(Arrays.asList(new String[]{"-classpath", classPath, "-1.7", "-nowarn"}));
				for (File f: sources) args.add(f.getAbsolutePath());
				j.compile(args.toArray(new String[args.size()]));
			} catch (NoSuchMethodException | SecurityException | IllegalAccessException | InvocationTargetException e) {
				throw new IllegalArgumentException(e);
			}
		}
	}
	
	public void compileAll(File srcFolder, FilenameFilter filter) throws IllegalArgumentException, MalformedURLException{
		if (getPackage(getBinaryName(srcFolder)) == null){
			definePackage(getBinaryName(srcFolder), null, null, null, null, null, null, null);
			for (File srcFile: srcFolder.listFiles(filter)) if (srcFile.isFile()) compile(srcFile);
		}
	}
	
	public List<Class<?>> loadAll(File srcFolder) throws IOException, ClassNotFoundException {
		List<Class<?>> classList = new ArrayList<Class<?>>();
		for (File file : srcFolder.listFiles(filterJavaFiles())) 
			classList.add(loadClass(getBinaryName(file), false));
		return classList;
	}
	
	public List<Class<?>> recompileAndLoadAll(File srcFolder) throws IOException, ClassNotFoundException {
		recompileAll(srcFolder);
		return loadAll(srcFolder);
	}
	
	public HashMap<String, Class<?>> recompileAndLoadAll(File srcFolder, FilenameFilter filter) throws IOException, ClassNotFoundException {
		recompileAll(srcFolder, filter);
		HashMap<String, Class<?>> classMap = new HashMap<String, Class<?>>();
		for (File file : srcFolder.listFiles(filter)){
			Class<?> cls = loadClass(getBinaryName(file), false);
			classMap.put(cls.getSimpleName(), cls);
		}
		return classMap;
	}
	
	public Class<?> load(File srcFile) throws IOException, ClassNotFoundException{
		return loadClass(getBinaryName(srcFile), false);
	}
	
	public Class<?> recompileAndLoad(File srcFile) throws IOException, ClassNotFoundException{
		recompile(srcFile);
		return loadClass(getBinaryName(srcFile), false);
	}
	
	@Override
	protected Class<?> loadClass(String name, boolean resolveIt) throws ClassNotFoundException {
		Class<?> cls = null;
		/* getClassFile returns null if name is not a plugin
		 * = if name is not a plugin use the parent's / system's loadClass method */
		if (name.endsWith("sub"))
			System.out.println(name);
		File classFile = getClassFile(name);
//		if (classFile == null) classFile = compile(name);
		if (classFile == null){
			cls = getSystemClassLoader().loadClass(name);
			return cls;
		} else {
			try {
				byte[] classBytes = loadClassBytes(getClassFile(name));
				cls = defineClass(name, classBytes, 0, classBytes.length);
				if (resolveIt) resolveClass(cls);
				return cls;
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
		return cls;
	}
	
	/* (non-Javadoc) 
	 * method copied and adapted from URLClassLoader
	 * @see java.lang.ClassLoader#findClass(java.lang.String)
	 */
	@Override
	protected Class<?> findClass(final String binaryName) throws ClassNotFoundException {
		try {
            return AccessController.doPrivileged(
                new PrivilegedExceptionAction<Class<?>>() {
                    @Override
					public Class<?> run() throws ClassNotFoundException {
                        File classFile = getClassFile(binaryName);
//                        File pkg = getClassFile(binaryName.substring(binaryName.lastIndexOf(".")));
                        if (classFile != null){
//                        	if (!classFile.exists()){
//                        		try {
//									compileAll(pkg);
//								} catch (IllegalArgumentException | MalformedURLException e) {
//									throw new ClassNotFoundException(e.toString());
//								}
//                        	}
                        	if (classFile.exists()) {
                                try {
                                	byte[] classBytes = loadClassBytes(classFile);
                                    return defineClass(binaryName, classBytes, 0, classBytes.length);
                                } catch (IOException e) {
                                	
                                    throw new ClassNotFoundException(binaryName, e);
                                }
                            } else {
                                throw new ClassNotFoundException(binaryName);
                            }
                        }
                        return getSystemClassLoader().loadClass(binaryName);
                    }
                }, AccessController.getContext());
        } catch (java.security.PrivilegedActionException pae) {
            throw (ClassNotFoundException) pae.getException();
        }
		
	}
	
	/**
     * Reads the class bytes from different local and remote resources using
     * ClasspathResources
     * 
     * @param className
     * @return byte[]
	 * @throws IOException 
     */
    private byte[] loadClassBytes(File classFile) throws IOException {
        byte[] classBytes = new byte[(int) classFile.length()];
        FileInputStream fis = new FileInputStream(classFile);
        fis.read(classBytes);
        fis.close();
        return classBytes;
    }
	
	public FilenameFilter filterClassFiles() {
		return new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.toLowerCase().endsWith(".class");
			}
		};
	}
	
	public FilenameFilter filterJavaFiles() {
		return new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.toLowerCase().endsWith(".java");
			}
		};
	}
	
	public FilenameFilter filterJarFiles() {
		return new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.toLowerCase().endsWith(".jar");
			}
		};
	}
	
	public FilenameFilter filterStartingWith(final String start){
		return new FilenameFilter(){
			@Override
			public boolean accept(File dir, String name){
				return name.startsWith(start);
			}
		};
	}
}
