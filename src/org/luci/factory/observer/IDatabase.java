package org.luci.factory.observer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import org.json.JSONObject;
import org.slf4j.Logger;

public interface IDatabase {

	/**Generates the URL used for establishing database connection
	 * @param dbHost: (String) hostname or IP number
	 * @param dbPort: (String) portnumber
	 * @param dbName: (String) name of the database
	 * @return Url used to connect to the database
	 */
	public String getJdbcUrl(String dbHost, String dbPort, String dbName);
	
	/**return e.g. "org.postgresql.Driver"
	 * @return the JDBC class name 
	 */
	public String getJdbcClassname();
	
	/**Run a query to the DB to retrieve the database version
	 * @param con: SQL Connection that has been established using getJdbcUrl()
	 * @return (String) database version
	 * @throws SQLException
	 */
	public JSONObject getDBInfo(Connection con) throws SQLException;
	
	/**Run all the SQL scripts to setup the tables specified in the spec/data_tables.html
	 * @param con: SQL Connection that has been established using getJdbcUrl()
	 * @param log: logger instance used for logging
	 * @throws SQLException
	 */
	public void setupTables(Connection con, Logger log) throws SQLException;
	
	/**Delete all tables specified in the spec/data_tables.html
	 * @param con: SQL Connection that has been established using getJdbcUrl()
	 * @param log: logger instance used for logging
	 * @throws SQLException
	 */
	public String resetTables(Connection con, Logger log) throws SQLException;
	
	/**Indicates which version of the data model is being implemented by the SQL scripts run by setupTables()
	 * @return (String) data model version
	 */
	public String getDataModelVersion();
	
	/**
	 * @param con
	 * @param log
	 * @param ScID
	 * @param srid
	 * @throws SQLException
	 */
	void createScenarioTables(Connection con, Logger log, int ScID, int srid) throws SQLException;
	
	/**
	 * @param con SQL Connection
	 * @param ScID long
	 * @return
	 * @throws SQLException
	 */
	public LinkedHashMap<String, Short> getColumnNamesAndTypes(Connection con, int ScID) throws SQLException;
	
	public int addColumns(Connection con, int ScID, Map<String, Short> attributes) 
			throws SQLException;
	
	public int dropColumns(Connection con, int ScID, String[] columnNames) throws SQLException;
	
	public void dropScenario(Connection con, int ScID) throws SQLException;

	public long[] getNewestTimestampForService(Connection con, long SObjID) throws SQLException;
	
	public long updateAutoIDSequence(Connection con, String tableName, String idName) throws SQLException;
	
	public void updateAutoIDSequence(Connection con, String tableName, String idName, long id) throws SQLException;

	public String getInsertSQL(long geomID, Set<String> fields, String tableName, String historyTable, Connection con) throws SQLException ;
	
	public int setChecksum(PreparedStatement ps, int i, String format, String checksum, Connection con) throws SQLException;
	
}

	