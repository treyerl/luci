package org.luci.factory.observer;

import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Future;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;
import org.json.JSONException;
import org.json.JSONObject;
import org.luci.Luci;
import org.luci.comm.clienthandlers.ClientHandler;
import org.luci.comm.clienthandlers.RemoteServiceClientHandler;
import org.luci.connect.LcJSONObject;
import org.luci.connect.LcMetaJSONObject;
import org.luci.factory.RemoteServiceFactoryThread;

public class RemoteRegistrar extends ServiceLoader{
	protected MqttClient mqtt;
	private Map<Long, Future<Object>> reservedServices;
	private Map<String, Future<Object>> runningRSF;
	
	protected RemoteRegistrar(Properties p, MqttClient m) 
			throws JSONException, NoSuchMethodException, SecurityException, MalformedURLException, 
			IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		super(p);
		mqtt = m;
		reservedServices = new HashMap<Long, Future<Object>>();
		runningRSF = new HashMap<String, Future<Object>>();
	}
	
	/**
	 * Registers a service. Creates a RemoteServiceFactoryThread for every new
	 * service name. All connected services with the same name are controlled in
	 * one RemoteServiceFactoryThread object.
	 * 
	 * @param so: Socket; to be handed over to the returned new RemoteServiceClientHandler           
	 * @param u: User: to be handed over to the returned new RemoteServiceClientHandler
	 * @param json: JSONObject containing the registration information
	 * 
	 * @throws Exception 
	 * @throws ClientAlreadyRegisteredException
	 *             - distinguishable exception type
	 * @return RemoteServiceClientHandler
	 */
	public synchronized RemoteServiceClientHandler registerRemoteService(ClientHandler pre, JSONObject json) throws Exception {
		JSONObject service = json.getJSONObject("service");
		String serviceName = service.getString("classname");
		String version = service.getString("version");
		JSONObject inputs = service.optJSONObject("inputs");
		JSONObject outputs = service.optJSONObject("outputs");
		long SObjID = json.optLong("SObjID", 0);
		if (inputs == null) inputs = new JSONObject();
		if (outputs == null) outputs = new JSONObject(); 

		if (!service.has("inputs")) service.put("inputs", new JSONObject());
		if (!service.has("outputs")) service.put("outputs", new JSONObject());
		
		if (SObjID != 0){
			RemoteServiceFactoryThread rsf = new RemoteServiceFactoryThread((FactoryObserver) this, service, SObjID);
			reservedServices.put(SObjID, threadPool.submit(rsf));
			return rsf.register(pre, json.getString("machinename"), SObjID);
		} else {
			RemoteServiceFactoryThread rsf = (RemoteServiceFactoryThread) services.get(serviceName);
			boolean New = false;
			if (rsf == null) {
				rsf = new RemoteServiceFactoryThread((FactoryObserver) this, service, SObjID);
				services.put(serviceName, rsf);
				// start the remote service factory thread
				Future<Object> f  = threadPool.submit(rsf);
				runningRSF.put(serviceName, f);
				New = true;
			} else {
				// validate integrity
				LcMetaJSONObject input = rsf.getInputIndex();
				if (input == null) input = new LcMetaJSONObject();
				if (!rsf.getVersion().equalsIgnoreCase(version)){
					throw new Exception("Service \\'"+serviceName+"\\' already registered with different version!");
				} else if (!input.fullyEquals(inputs)){
					throw new Exception("Service \\'"+serviceName+"\\' already registered with different inputs!");
				} else if (!rsf.getOutputIndex().fullyEquals(outputs)){
					throw new Exception("Service \\'"+serviceName+"\\' already registered with different outputs!");
				}
			}

			// register the client as a connected service
			RemoteServiceClientHandler rsch = rsf.register(pre, json.getString("machinename"), SObjID);
			
			// populates a newly available remote service
			if (New) {
				String mqtt_pre = Luci.getConfig().getProperty("mqtt_pre", "/Luci/");
				mqtt.publish(mqtt_pre + "/admin/registration/service", serviceName.getBytes("UTF8"), Luci.qos_1_at_least_once, true);
			}
			return rsch;
		}
	}
	
	public void unregisterRemoteService(long SObjID){
		Future<Object> f = reservedServices.remove(SObjID);
		f.cancel(true);
	}
	
	public void unregisterRemoteService(String name) throws MqttPersistenceException, MqttException {
		services.remove(name);
		Future<Object> f = runningRSF.remove(name);
		f.cancel(true);
		String mqtt_pre = Luci.getConfig().getProperty("mqtt_pre", "/Luci/");
		mqtt.publish(mqtt_pre + "/admin/unregistration/service", name.getBytes(), Luci.qos_1_at_least_once, true);
	}
}
