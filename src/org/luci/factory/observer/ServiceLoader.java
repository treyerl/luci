package org.luci.factory.observer;

import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;

import org.eclipse.paho.client.mqttv3.MqttException;
import org.json.JSONException;
import org.luci.factory.ServiceFactoryThread;
import org.luci.services.RemoteService;
import org.luci.services.Service;
import org.luci.services.ServiceCall;

public class ServiceLoader extends ActionLoader {
	protected TreeMap<String, ServiceFactoryThread> services;
	protected ThreadPoolExecutor threadPool;
	private Map<String, Future<Object>> runningFactories;
	
	protected ServiceLoader(Properties p) throws JSONException, NoSuchMethodException, SecurityException, 
	MalformedURLException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		super(p);
		services = new TreeMap<String, ServiceFactoryThread>(String.CASE_INSENSITIVE_ORDER);
		threadPool = (ThreadPoolExecutor) Executors.newFixedThreadPool(50);//Executors.newCachedThreadPool();
		runningFactories = new HashMap<String, Future<Object>>();
	}
	
	protected int buildServices() {

		this.unregisterLocalServices();
		File srcFolder = new File(getSrcPath(),"services");
		File binFolder = new File(getBinPath(),"services");
		binFolder.mkdir();
		
		int numReloaded = 0;
		try {
			// recompile all java source files; also in subfolders
			loader.recompileAllRecursively(srcFolder);
			/* reload all classes in service folder (top level only) */
			List<Class<?>> serviceClasses = loader.loadAll(srcFolder);
			/* reload all classes in subfolders that have the same name as the subfolder 
			 * (inheriting from or delegating to many other classes in that subfolder, 
			 * which got compiled with the recursive compilation just before) */
			for (File d: srcFolder.listFiles(new FileFilter(){@Override
			public boolean accept(File dir){return dir.isDirectory();}})) {
				File sub = new File(d, d.getName()+".java");
				serviceClasses.add(loader.load(sub));
			}
			for (Class<?> cls : serviceClasses) {
				ServiceFactoryThread sf = createServiceFactory(cls);
				services.put(cls.getSimpleName(), sf);
				runningFactories.put(cls.getSimpleName(), threadPool.submit(sf));
			}
			numReloaded = serviceClasses.size();
		} catch (ClassNotFoundException | IOException | InstantiationException | IllegalAccessException | 
				IllegalArgumentException | InvocationTargetException | SecurityException | NoSuchMethodException e) {
			errors.add("buildServices: "+e.toString());
			if (log.isDebugEnabled()) e.printStackTrace();
		}
		
		return numReloaded;
	}
	
	/**
	 * clean up method to unregister all local services
	 */
	private void unregisterLocalServices() {
		for (Iterator<?> it = this.services.keySet().iterator(); it.hasNext();) {
			String key = (String) it.next();
			ServiceFactoryThread s = services.get(key);
			if (!RemoteService.class.isAssignableFrom(s.getProductClass())) {
				CountDownLatch cdl = new CountDownLatch(1);
				try {
					s.mqttDisconnect(cdl);
					cdl.await();
				} catch (Exception e) {
					e.printStackTrace();
				}
				it.remove();
			}
			runningFactories.get(key).cancel(true);
		}
	}
	
	public void shutDownServiceFactories() throws IOException, MqttException, InterruptedException {
		for (String key: runningFactories.keySet()){
			ServiceFactoryThread s = services.get(key);
			CountDownLatch cdl = new CountDownLatch(1);
			s.mqttDisconnect(cdl);
			cdl.await();
			runningFactories.get(key).cancel(true);
		}
	}
	
	protected ServiceFactoryThread createServiceFactory(Class<?> product) 
			throws InstantiationException, IllegalAccessException, IllegalArgumentException, 
			InvocationTargetException, SecurityException, NoSuchMethodException, FileNotFoundException {
		return new ServiceFactoryThread(product, (FactoryObserver) this);
	}
	
	/**Calls the service factory's getService method, which will resolve the id to a ServiceInputHash 
	 * (see {@link org.luci.factory.ServiceFactoryThread#getInputHash private getInputHash} method).
	 * @param s: (String) name of the service
	 * @param id: The SObjID (service object/instance ID) the service should be initialized with. 
	 * @return An instance of the service.
	 * @throws Exception
	 */
	
	public Service getService(String s, int id) throws Exception {
		return getService(s, id, 0);
	}
	
	public Service getService(String s, int id, long uid) throws Exception {
		ServiceFactoryThread factory = this.services.get(s);
		if (factory != null) {
			return factory.getService(id, uid);
		} else {
			throw new Exception("Service '" + s + "' not found!");
		}
	}
	
	/**
	 * @param s: (String) name of the service
	 * @param sih: ServiceInputHash object
	 * @return An instance of the service.
	 * @throws InvocationTargetException 
	 * @throws IllegalArgumentException 
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 * @throws SecurityException 
	 * @throws NoSuchMethodException 
	 * @throws FileNotFoundException 
	 * @throws Exception
	 */
	public Service getService(String s, ServiceCall sih) 
			throws NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, 
			IllegalArgumentException, InvocationTargetException, FileNotFoundException {
		ServiceFactoryThread factory = this.services.get(s);
		if (factory != null) {
			return factory.getService(sih);
		} else {
			throw new FileNotFoundException("Service '" + s + "' not found!");
		}
	}
}
