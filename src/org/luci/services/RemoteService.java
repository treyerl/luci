package org.luci.services;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import org.json.JSONObject;
import org.luci.User;
import org.luci.comm.clienthandlers.RemoteServiceClientHandler;
import org.luci.connect.LcInputStream;
import org.luci.connect.LcMetaJSONObject;
import org.luci.factory.RemoteServiceFactoryThread;


/**
 * Created upon service registration. When a client registers as a service the
 * service factory 'SFConnected' will create "connected service"
 * ServiceConnected that translates the middleware's service calls to a
 * text-based server-client communication and reroutes the communication between
 * connected service and client.
 * 
 * @author Lukas Treyer <treyer@arch.ethz.ch>
 * 
 */

public class RemoteService extends Service {
	private RemoteServiceClientHandler comm;
	private String version;

	public RemoteService(ServiceCall call, RemoteServiceFactoryThread rf,
			 RemoteServiceClientHandler rc, String version) throws Exception{
		super(call, rf, rc.getMachineName());
		this.comm = rc;
		this.version = version;
	}
	
	@Override
	public String getVersion(){
		return version;
	}

	/**
	 * Returns a User object containing all the details about the user who
	 * registered the service.
	 * 
	 * @return User owner
	 */
	public User getOwner() {
		return this.comm.getUser();
	}

	@Override
	public Object call() throws Exception {
		comm.setRemoteService(this);
		comm.sendRun();
		CountDownLatch wait = comm.getAnswerLatch(1);
		wait.await();
		comm.setRemoteService(null);
		return call.getOutputs();
	}
	
	@Override
	public LcMetaJSONObject getInputIndex() {
		RemoteServiceFactoryThread rsf = this.comm.getFactory();
		return rsf.getInputIndex();
	}

	@Override
	public LcMetaJSONObject getOutputIndex() {
		RemoteServiceFactoryThread rsf = this.comm.getFactory();
		return rsf.getOutputIndex();
	}

	public void receive(JSONObject result, List<LcInputStream> binaryOutputs){
		//TODO: receive the result from within RemoteService so we can re-use the setOutputs method 
		//      to re-use the isSync-check; remember: clienthandler blocks only until the message 
		//      header has arrived - binary attachments must be processed still!!
		//
	}
	

	public RemoteServiceClientHandler getClientHandler() {
		return this.comm;
	}
	
	public void throwException(Exception e) throws Exception{
		throw e;
	}

}
