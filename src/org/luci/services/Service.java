package org.luci.services;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;
import org.json.JSONException;
import org.json.JSONObject;
import org.luci.Luci;
import org.luci.comm.action.IGetServiceInputs;
import org.luci.comm.action.ISetServiceOutputs;
import org.luci.connect.BufferedStreamWriter;
import org.luci.connect.LcInputStream;
import org.luci.connect.LcJSONObject;
import org.luci.connect.LcMetaJSONObject;
import org.luci.connect.LcOutputStream;
import org.luci.connect.StreamWriter;
import org.luci.factory.ICallableWithID;
import org.luci.factory.ITask;
import org.luci.factory.ServiceFactoryThread;
import org.luci.factory.observer.FactoryObserver;
import org.slf4j.Logger;

/**
 * Following the idea of polymorphism it is the key concept of this software
 * design to have services that are known for certain capabilities: - run(call),
 * set and get parameters, getServiceName, edit the services that are controlled
 * by this very service (for piping and parallel jobs). Basically the abstract
 * behavior could have been expressed also with interfaces, but would have
 * leaded to a bad coding style, imo: http://stackoverflow.com/a/7256729 The
 * service defined in this abstract class is really the core for all inheriting
 * classes and not just an additional role/behavior a service must implement.
 * 
 * @author Lukas Treyer
 * 
 */
abstract public class Service implements ICallableWithID<Object>, ITask {
	protected long SObjID;
	protected long ScID;
	protected Logger log;
	protected String machinename;
	protected ServiceCall call;
	protected ServiceFactoryThread factory;
	protected FactoryObserver o;
	protected IGetServiceInputs get;
	private int iteration; 
	
	public Service(ServiceCall call, ServiceFactoryThread factory, String machinename) 
			throws SQLException, InstantiationException, IOException, InterruptedException {
		this.call = call;
		this.SObjID = call.getSObjID();
		this.ScID = call.getScID();
		this.log = Luci.createLoggerFor(getClass().getSimpleName());
		this.factory = factory;
		this.o = factory.getObserver();
		this.machinename = machinename;
		get = o.getActionGetServiceInputs();
		iteration = 1;
	}

	/**
	 * Returns the simple name of the class of this service.
	 * ServiceConnected.class overrides this.
	 * 
	 * @return String - return this.getClass().getSimpleName();
	 */
	public String getServiceName() {
		return this.getClass().getSimpleName();
	}
	
	public long getSObjID(){
		return SObjID;
	}
	
	@Override
	public long getID(){
		return SObjID;
	}
	
	public long getScID(){
		return ScID;
	}
	
	@Override
	public ServiceCall getCall(){
		return call;
	}
	
	protected JSONObject getInputs() throws Exception{
		return getInputs(true, null);
	}
	
	protected JSONObject getInputs(boolean getSubscriptions, List<LcInputStream> inputStreams) 
			throws Exception{
		if (!call.isSync()) {
			Connection con = o.getDatabaseConnection();
			get.setCon(con);
			get.setSObjID(call.getID());
			get.setScID(call.getScID());
			
			LcJSONObject inputs = get.getServiceInputs(getSubscriptions, inputStreams, null);
			con.close();
			return inputs;
		} else {
			// TODO: handle a service's output in the snyc run case 
			// --> return the value also from sf.runAndAwait();
			for (LcInputStream input: call.getInputStreams()) inputStreams.add(input);
			return call.getInputs();
		}
	}
	
	public void setOutputs(JSONObject outputs) throws Exception{
		setOutputs(outputs, null, false);
	}
	
	public void setOutputs(JSONObject outputs, List<LcInputStream> binaryOutputs) throws Exception{
		setOutputs(outputs, binaryOutputs, false);
	}
	
	private void setOutputs(JSONObject outputs, List<LcInputStream> binaryOutputs, boolean progress) 
			throws SQLException, InstantiationException, FileNotFoundException, JSONException, 
			MqttPersistenceException, ExecutionException, InterruptedException, MqttException, IOException {
		
		LcJSONObject msg = prepareOutputRequest(outputs);
		msg.put("iteration", iteration++);
		if (!call.isSync()){
			Connection con = o.getDatabaseConnection();
			ISetServiceOutputs set = o.getActionSetServiceOutputs(msg);
			set.setInputHashcode(getCall().getTimestamp());
			set.setCon(con);
			set.setFlag(0);
			set.setSObjID(SObjID);
			set.updateServiceOutputs(binaryOutputs);
			con.close();
		} else {
			List<LcOutputStream> outputStreams = new ArrayList<LcOutputStream>();
			if (binaryOutputs != null){
				for(LcInputStream i: binaryOutputs){
					StreamWriter sw = new BufferedStreamWriter(i, new LcOutputStream(i));
					outputStreams.add(sw.getLcOutputStream());
					o.getThreadPool().submit(sw);
				}
			}
			if (progress){
				JSONObject progressMsg = new JSONObject().put("progress", msg);
				call.writeProgress(progressMsg, outputStreams);
			} else {
				call.setOutputs(outputs, outputStreams);
			}
		}
	}
	
	public void setProgress(JSONObject outputs, List<LcInputStream> binaryOutputs) throws Exception{
		setOutputs(outputs, binaryOutputs, true);
	}
	
	public void setProgress(JSONObject outputs) throws Exception {
		setOutputs(outputs, null, true);
	}
	
	protected LcJSONObject prepareOutputRequest(JSONObject outputs) {
		LcJSONObject outputRequest= new LcJSONObject();
		outputRequest.put("outputs", outputs);
		outputRequest.put("machinename", machinename);
		outputRequest.put("serviceversion", getVersion());
		outputRequest.put("input_hashcode", call.getTimestamp().getTime());
		outputRequest.put("SObjID", call.getSObjID());
		return outputRequest;
	}

	/**
	 * 
	 * @return JSONOject
	 */
	@Override
	public abstract LcMetaJSONObject getInputIndex() ;
	
	@Override
	public abstract LcMetaJSONObject getOutputIndex();

	/**
	 * Execute the service
	 * 
	 * @return classes implementing this method can specify the return type, but
	 *         it must be of type Object or descendant of Object.
	 */
	@Override
	public abstract Object call() throws Exception;
	public abstract String getVersion();
}
