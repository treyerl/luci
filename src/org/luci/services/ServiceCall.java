package org.luci.services;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.luci.comm.clienthandlers.ClientHandler;
import org.luci.connect.ILcSocket;
import org.luci.connect.LcInputStream;
import org.luci.connect.LcOutputStream;
import org.luci.factory.call.Call;
import org.luci.factory.observer.FactoryObserver;
import org.luci.factory.observer.IDatabase;

public class ServiceCall extends Call{
	boolean isSync;
	private JSONObject inputs;
	private JSONObject outputs;
	private List<LcInputStream> inputStreams;
	private List<LcOutputStream> outputStreams;
	private ILcSocket socket;
	
	public ServiceCall(Timestamp newest, long SObjID, int ScID, long uID, String message) {
		this(newest, SObjID, ScID, uID, message, null);
	}
	
	/**Creates a ServiceCall object holding SOjbID, ScID and the newest timestamp of inputs.
	 * @param newest: newest Timestamp
	 * @param SObjID: (long) service instance ID
	 * @param ScID: (long) scenario ID
	 * @param uID: (long) user ID
	 * @throws SQLException 
	 */
	public ServiceCall(Timestamp newest, long SObjID, int ScID, long uID, String message, Connection con){
		super(newest, SObjID, ScID, uID, message);
//		if (con != null) {
//			try {
//				createRecordInServiceCallTable(SObjID, newest, con);
//			} catch (SQLException e) {
//				e.printStackTrace();
//			}
//		}
	}
	
	public ServiceCall(JSONObject inputs, List<LcInputStream> inputStreams, int scId, ClientHandler h){
		super(new Timestamp(new Date().getTime()), 0, scId, h.getUser().getUid(), null);
		this.inputs = inputs;
		this.inputStreams = inputStreams;
		isSync = true;
		socket = h.getSocket();
	}
	
	public ServiceCall(JSONObject inputs, List<LcInputStream> inputStreams, ClientHandler h){
		super(new Timestamp(new Date().getTime()), 0, 0, h.getUser().getUid(), null);
		this.inputs = inputs;
		this.inputStreams = inputStreams;
		isSync = true;
		socket = h.getSocket();
	}
	
//	public ServiceCall(JSONObject json, ClientHandler h) throws JSONException {
//		this(json, h, null, null);
//	}
//	
//	public ServiceCall(JSONObject json, ClientHandler h, Connection con) throws JSONException {
//		this(json, h, null, con);
//	}
//	
//	public ServiceCall(JSONObject json, ClientHandler h, String msg, Connection con) throws JSONException {
//		super(new Timestamp(json.getLong("input_hashcode")), json.getLong("SObjID"), json.optLong("ScID", 0), h.getUser().getUid(), msg);
//		isSync = json.has("isSync");
//		socket = h.getSocket();
//		if (con != null)
//			try {
//				createRecordInServiceCallTable(json.getLong("SObjID"), new Timestamp(json.getLong("input_hashcode")), con);
//			} catch (SQLException e) {
//				e.printStackTrace();
//			}
//	}

//	/**Creates a ServiceCall object holding SOjbID, ScID and the lastest timestamp of inputs. 
//	 * It does so by looking up the current max timestamps of service inputs, 
//	 * geometry and geometry attributes in the database. 
//	 * @param SObjID: (long) service instance ID
//	 * @param con: SQL Connection
//	 * @throws SQLException
//	 */
//	
	public ServiceCall(long SObjID, long uID, FactoryObserver o) throws SQLException {
		this(create(SObjID, uID, null, o));
	}
	
	public ServiceCall(long SObjID, long uID, String message, FactoryObserver o) throws SQLException {
		this(create(SObjID, uID, message, o));
	}
	
	protected ServiceCall(Call call) {
		super(call.getTimestamp(), call.getID(), call.getScID(), call.getUID(), call.getMessage());
	}
	
//	private static void createRecordInServiceCallTable(long SObjID, Timestamp input_hash, Connection con) throws SQLException{
//		String sql = "INSERT INTO "+TAB.getTableName(TAB.SERVICECALLS)+"(sobjid, input_hashcode, flag) VALUES(?,?,?);";
//		PreparedStatement ps = con.prepareStatement(sql);
//		ps = con.prepareStatement(sql);
//		ps.setLong(1, SObjID);
//		ps.setTimestamp(2, input_hash);
//		ps.setLong(3, 0);
//		ps.execute();
//	}
	
	private static Call create(long SObjID, long uID, String message, FactoryObserver o) 
			throws SQLException {
		if (SObjID > 0){
			IDatabase db;
			Connection con = null;
			try {
				db = o.dataModelImplementation();
				con = o.getDatabaseConnection();
				long[] newest = db.getNewestTimestampForService(con, SObjID);
				Timestamp st = new Timestamp(newest[0]);
				long ScID = newest[1];
				con.close();
				return new Call(st, SObjID, (int) ScID, uID, message);
			} catch (SQLException s){
				if (con != null) con.close();
				throw s;
			} catch (InstantiationException e) {
				throw new SQLException(e);
			}
		} else {
			return new Call(new Timestamp(new Date().getTime()), SObjID, 0, uID, message);
		}
		
		
	}
	
	public long getSObjID(){
		return ID;
	}
	
	public boolean isSync(){
		return isSync;
	}
	
	public JSONObject getInputs(){
		return inputs;
	}
	
	public List<LcInputStream> getInputStreams(){
		return inputStreams;
	}
	
	public void setOutputs(JSONObject outputs, List<LcOutputStream> outputStreams){
		this.outputs = outputs;
		this.outputStreams = outputStreams;
	}
	
	public JSONObject getOutputs(){
		return outputs;
	}
	
	public List<LcOutputStream> getOutputStreams(){
		return outputStreams;
	}

	/**
	 * @param outputs
	 * @param outputStreams
	 * @throws IOException 
	 * @throws InterruptedException 
	 */
	public void writeProgress(JSONObject outputs, List<LcOutputStream> outputStreams) 
			throws InterruptedException, IOException {
		if (!outputs.has("progress")) throw new JSONException("No progress key found!");
		socket.send(outputs, outputStreams);
	}
}
