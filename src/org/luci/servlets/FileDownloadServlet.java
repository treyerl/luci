/**
 * The MIT License (MIT)
 *
 * Copyright (c) <year> <copyright holders>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package org.luci.servlets;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.luci.Luci;
import org.luci.comm.clienthandlers.WebClientProxy;
import org.luci.connect.LcOutputStream;
import org.slf4j.Logger;

/**
 * 
 * @author treyerl
 *
 */
public class FileDownloadServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2484754348235234949L;

	private final static Logger LOGGER = Luci.createLoggerFor("fileDownloads");

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws InterruptedException 
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    	response.addHeader("Access-Control-Allow-Origin", "*");
        // Create path components to save the file
        final String id = request.getParameter("clientID");
        final String pathInfo = request.getPathInfo();
        File f = new File(Luci.getDataRoot(), pathInfo);
        if (id != null){
        	
        	try {
        		int ID = Integer.parseInt(id);
            	WebClientProxy cp = (WebClientProxy) Luci.getLuciInstance().getClientProxy(ID);
                if (cp != null){
                	LcOutputStream los = cp.getDownloadStream(pathInfo.substring(1), 5, TimeUnit.SECONDS);
                	if (los != null){
//                		response.setContentType("application/octet-stream");
//                	    response.setHeader("Content-Disposition", "attachment; filename=\"" + los.getName() + "\"");
                	    response.setContentLength(los.getLength());
                	    los.setOutputStream(response.getOutputStream());
                	    los.setReady();
            			los.waitUntilDone(Luci.WSockTimeoutInMillis, TimeUnit.MILLISECONDS);
                	} else {
                		PrintWriter writer = response.getWriter();
                		response.setContentType("text/html;charset=UTF-8");
                		response.setStatus(HttpServletResponse.SC_NOT_FOUND);
                		writer.printf("File '%s' not found!%n", pathInfo);
                	}
                } else {
                	PrintWriter writer = response.getWriter();
                	response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                	response.setContentType("text/html;charset=UTF-8");
                	writer.printf("invalid clientID '%d'%n", ID);
                }
        	} catch (NumberFormatException | InterruptedException e){
        		PrintWriter writer = response.getWriter();
        		e.printStackTrace(writer);
        	}
        } else if(f.exists()){
        	int len = (int)f.length(), nRead = 0;
//        	response.setContentType("application/octet-stream");
        	String mimeType = java.nio.file.Files.probeContentType(f.toPath());
//        	System.out.println(f.toPath());
//        	System.out.println("mimeType: "+mimeType);
        	response.setContentType(mimeType);
//    	    response.setHeader("Content-Disposition", "filename=\"" + f.getName() + "\"");
    	    response.setContentLength(len);
    	    FileInputStream fis = new FileInputStream(f);
    	    int buffersize = Luci.BUFFERSIZE;
    	    byte[] data = new byte[buffersize];
    	    OutputStream out = response.getOutputStream();
    	    while (len > 0 ) {
				len -= (nRead = fis.read(data, 0, Math.min(buffersize, len)));
				out.write(data, 0, nRead);
			}
			out.flush();
    	    fis.close();
        } else {
        	PrintWriter writer = response.getWriter();
        	writer.println("'clientID' missing!");
        }
        
//        response.setHeader("Connection", "close");
//        request.getSession().invalidate();
//        System.out.println(request.getPathInfo());
        
    }

    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Servlet that serves files from the websocketBinaries cache";
    }

}
