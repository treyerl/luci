package org.luci.servlets;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.luci.Luci;
import org.luci.comm.clienthandlers.WebClientProxy;
import org.luci.connect.LcInputStream;
import org.slf4j.Logger;

/**
 * File upload servlet example
 */
@WebServlet(name = "FileUploadServlet", urlPatterns = {"/upload"})
public class FileUploadServlet extends HttpServlet {

    /**
	 * 
	 */
	private static final long serialVersionUID = 898522442105608288L;
	private final static Logger LOGGER = Luci.createLoggerFor("fileUploads");

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    	response.addHeader("Access-Control-Allow-Origin", "*");
        response.setContentType("text/html;charset=UTF-8");
//        System.err.println("/upload");
        // Create path components to save the file
        final PrintWriter writer = response.getWriter();
        InputStream is = null;
//        System.out.println(request);
        String id = request.getParameter("clientID");
        String checksum = request.getParameter("checksum");
        if (id != null){
	        final int ID = Integer.parseInt(id);
	        final WebClientProxy cp = (WebClientProxy) Luci.getLuciInstance().getClientProxy(ID);
	        
	        if (cp != null){
	        	try {
	        		// blocks for max 5000 milliseconds / until the LcInputStream becomes available
	                LcInputStream lis = cp.getUploadStream(checksum, 5000);
	                if (lis != null) {
	                	try {
	                		is = request.getInputStream();
	    	                lis.setInputStream(is);
	    	                lis.setReady();
	    	                lis.waitUntilDone(Luci.WSockTimeoutInMillis, TimeUnit.MILLISECONDS);
	    	                writer.println("New file " + lis.getName() + " created at " + lis.getChecksum() + "</br>");
	    	                LOGGER.trace("File %s being uploaded to %s%n", lis.getName(), ID);
	                	} catch (RuntimeException e){ 
    	                	writer.println(e);
    	                	LOGGER.error(e.toString());
	                	}
	                } else {
	                	String warn = "No slot ready for '"+checksum+"'!";
	                	writer.println(warn);
	                	LOGGER.warn(warn);
	                }
	                
				} catch (InterruptedException e) {
					LOGGER.error(e.toString());
					e.printStackTrace(writer);
				}
	        } else {
	        	writer.println("invalid clientID '"+ID+"'");
	        }
        } else {
        	writer.println("'clientID' missing!");
        }
        
        if (is != null) is.close();
        writer.close();
    }

    @SuppressWarnings("unused")
	private String getFileName(final Part part) {
        final String partHeader = part.getHeader("content-disposition");
        LOGGER.debug("Part Header = {0}", partHeader);
        for (String content : part.getHeader("content-disposition").split(";")) {
            if (content.trim().startsWith("filename")) {
                return content.substring(
                        content.indexOf('=') + 1).trim().replace("\"", "");
            }
        }
        return null;
    }

    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Servlet that uploads files to a user-defined destination";
    }
}
