package org.luci.servlets;

import java.io.File;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.luci.Luci;

import com.asual.lesscss.LessEngine;
import com.asual.lesscss.LessException;

public class LessCompileServlet extends HttpServlet {

	private static final long serialVersionUID = 95869269364922697L;

	@Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) 
    		throws ServletException, IOException {
		String lessUri = req.getRequestURI().substring(req.getContextPath().length());
		String cssUri = lessUri.replace(".less", ".css");
		File less = new File(Luci.webroot.getAbsolutePath(), new File(lessUri).getPath());
		File css = new File(Luci.webroot.getAbsolutePath(), new File(cssUri).getPath());
		if (css.lastModified() < less.lastModified()){
			LessEngine engine = new LessEngine();
			try {
				engine.compile(less, css);
			} catch (LessException e) {
				e.printStackTrace();
			}
//			System.out.println(String.format("compiled %s to %s", less.getName(), css.getName()));
		}

        req.getRequestDispatcher(cssUri).forward(req,resp);
    }

}
