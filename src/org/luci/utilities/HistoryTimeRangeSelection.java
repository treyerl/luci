/**
 * The MIT License (MIT)
 *
 * Copyright (c) <year> <copyright holders>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package org.luci.utilities;

import org.json.JSONObject;
import org.luci.utilities.TimeRangeSelection;

/**
 * 
 * @author treyerl
 *
 */
public class HistoryTimeRangeSelection extends TimeRangeSelection{
	public static int ALL = 1;
	public static int OLDEST = 2;
	public static int NEWEST = 3;
	public static String[] types = new String[]{"all","oldest","newest"};
	
	private int type;

	public HistoryTimeRangeSelection(JSONObject timerange) {
		super(timerange);
		if (timerange.has("type")) {
			String t = timerange.getString("type");
			switch(t){
			case "all": type = ALL; break;
			case "oldest": type = OLDEST; break;
			case "newest": type = NEWEST; break;
			}
		} else type = ALL;
	}
	
	public HistoryTimeRangeSelection setType(int t){
		type = t;
		return this;
	}
	
	public String getInputsHistoryTimestampSQL(String o){
		if (type == ALL){
			return super.getSQL();
		} else {
			String operator = " >= ";
			if (type == OLDEST) operator = " <= ";
			// TODO getInputsHistoryTimestampSQL
			return null;
		}
	}
	
	public String getOutputsHistoryTimestampSQL(String o){
		if (type == ALL){
			return super.getSQL();
		} else {
			String operator = " >= ";
			if (type == OLDEST) operator = " <= ";
			// TODO getOutputsHistoryTimestampSQL
			return null;
		}
	}
	
	public String getScenarioHistoryTimestampSQL(String o){
		if (type == ALL){
			return super.getSQL();
		} else {
			String operator = " >= ";
			if (type == OLDEST) operator = " <= ";
			// TODO getScenarioHistoryTimestampSQL
			return null;
		}
	}
	
	public JSONObject getTimerangeJSON(){
		JSONObject timerange = super.getTimerangeJSON();
		timerange.put("type", types[type]);
		return timerange;
	}
	
}
