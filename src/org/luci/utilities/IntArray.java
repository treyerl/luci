/**
 * The MIT License (MIT)
 *
 * Copyright (c) <year> <copyright holders>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package org.luci.utilities;

import java.util.Iterator;
//import java.util.PrimitiveIterator; // JAVA 1.8

/**
 * 
 * @author Lukas Treyer
 *
 */
public class IntArray implements Iterable<Integer>{
	protected int[] base;
	protected int i = 0;
	private IntArray(int[] base) {
		this.base = base;
	}
	
	public static IntArray wrap(int[] base){
		return new IntArray(base);
	}
	
	public int count(int numToFind){
		int occurence=0;
		for (int i = 0; i < base.length; i++) { 
			if (base[i] == numToFind) occurence++;
		}
		return occurence;
	}
	
	public int findNextByIndex(int index){
		return findNextByIndex(index, false);
	}
	
	public int findNextByIndex(int index, boolean length){
		for (; i < base.length; i++){
			if (base[i] == base[index]) return i++ - 1;
		}
		if (length)
			return base.length;
		else
			return -1;
	}
	
	public int findNextIndexOf(int numToFind){
		return findNextIndexOf(numToFind, false);
	}
	
	public int findNextIndexOf(int numToFind, boolean length){
		for (; i < base.length; i++){
			if (base[i] == numToFind) return i++ - 1;
		}
		if (length)
			return base.length;
		else
			return -1;
	}
	
	public int[] findAllByIndex(int index){
		int[] matches = new int[count(base[index])];
		for (int i = 0, j = 0; i < base.length; i++){
			if (base[i] == base[index]) matches[j++] = i;
		}
		return matches;
	}
	
	public int[] findAll(int numToFind){
		int[] matches = new int[count(numToFind)];
		for (int i = 0, j = 0; i < base.length; i++){
			if (base[i] == numToFind) matches[j++] = i;
		}
		return matches;
	}
	
	public IntArray rewind(){
		i = 0;
		return this;
	}
	
	public int position(){
		return i;
	}
	
	public IntArray position(int i){
		this.i = i;
		return this;
	}
	
	public int get(int i){
		return base[i];
	}
	
	public int get(){
		return base[i];
	}
	
	public void set(int i){
		base[this.i] = i;
	}
	
	public void set(int idx, int i){
		base[idx] = i;
	}
	
	public int length(){
		return base.length;
	}

	@Override
	public Iterator<Integer> iterator() {
		return new Iterator<Integer>() {
			int j = 0;
			@Override
			public boolean hasNext() {
				return j < base.length;
			}

			@Override
			public Integer next() {
				return base[j++];
			}

			@Override
			public void remove() {
				int[] newbase = new int[base.length - 1];
				for (int k = 0; k < newbase.length; k++){
					if (k != j) newbase[k] = base[k];
				}
				base = newbase;
			}
		};
	}
	
	@Override
	public String toString(){
		return java.util.Arrays.toString(base);
	}

}
