package org.luci.utilities;

import java.util.Iterator;
import java.util.List;

@SuppressWarnings("rawtypes")
public class Iter implements Iterator {
	
	private Object o;
	private int i;
	private int size;
	
	public Iter(Object o) {
		this.o = o;
		if (o instanceof List) size = ((List) o).size();
		else size = 1;
		i = 0;
	}

	@Override
	public boolean hasNext() {
		return i < size;
	}

	@Override
	public Object next() {
		if (o instanceof List) return ((List) o).get(i++);
		else return o;
	}

	@Override
	public void remove(){
		throw new UnsupportedOperationException();
	}

}
