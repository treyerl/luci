package org.luci.utilities;

import org.json.JSONArray;
import org.osgeo.proj4j.CRSFactory;
import org.osgeo.proj4j.CoordinateReferenceSystem;
import org.osgeo.proj4j.CoordinateTransform;
import org.osgeo.proj4j.CoordinateTransformFactory;
import org.osgeo.proj4j.ProjCoordinate;

/**
 * @author Lukas Treyer <treyer@arch.ethz.ch>
 * @param center: scenario geo reference; used to calculate a dynamic mercator transformation 
 * in case a transformation to WGS84 (and from there to other CRS's) is needed
 */
public class Projection {
	public final static int earth_radius = 6378137;
	
	private int srid = 0;
	private double[] center = null;
	private CoordinateTransform transform = null;
	private CoordinateReferenceSystem fromCRS;
	private CoordinateReferenceSystem toCRS;
	private int dim = 4;
	private boolean switchLatLon;
	
	public static Projection NULL(){
		return new Projection();
	}
	
	/** no transformation; 
	 *  SRID is NOT stored when saving to DB 
	 */
	public Projection(){
	}
	
	/** no transformation; 
	 *  SRID is NOT stored when saving to DB
	 *  @param center: double[], 
	 */
	public Projection(double[] center){
		this.center = center;
	}
	
	/**no transformation; 
	 * SRID is stored when saving to DB
	 * @param fromCrs: String
	 */
	public Projection(String fromCrs){
		this.srid = parseEPSG(fromCrs);
	}
	
	/**transformation: crs --> dynamic mercator,
	 * SRID is NOT stored, 
	 * do NOT call from_scenario() of this instance
	 * @param fromCrs
	 * @param center
	 */
	public Projection(String fromCrs, double[] center){
		CRSFactory crsFactory = new CRSFactory();
		CoordinateTransformFactory transFactory = new CoordinateTransformFactory();
		
		this.center = validateCenter(center);
		this.srid = parseEPSG(fromCrs);
		
		
		this.fromCRS = crsFactory.createFromName(fromCrs);
		this.toCRS = crsFactory.createFromName("EPSG:4326");
		
		this.transform = transFactory.createTransform(fromCRS, toCRS);
	}
	
	/**transformation: dynamic mercator --> crs;
	 * SRID would be stored but this Projection is intended for transforming scenario geometry to another CRS.
	 * Hence, do NOT call to_scenario of this instance.
	 * @param center
	 * @param toCrs
	 */
	public Projection(double[] center, String toCrs){
		CRSFactory crsFactory = new CRSFactory();
		CoordinateTransformFactory transFactory = new CoordinateTransformFactory();
		
		this.center = validateCenter(center);
		this.srid = parseEPSG(toCrs);
		
		this.toCRS = crsFactory.createFromName(toCrs);
		this.fromCRS = crsFactory.createFromName("EPSG:4326");
		
		this.transform = transFactory.createTransform(fromCRS, toCRS);
	}
	
	/**transformation: crs --> crs, 
	 * SRID is stored when saving to DB
	 * @param fromCrs
	 * @param toCrs
	 */
	public Projection(String fromCrs, String toCrs){
		CRSFactory crsFactory = new CRSFactory();
		CoordinateTransformFactory transFactory = new CoordinateTransformFactory();
		
		this.srid = parseEPSG(toCrs);
		
		this.fromCRS = crsFactory.createFromName(fromCrs);
		this.toCRS = crsFactory.createFromName(toCrs);			

		this.transform = transFactory.createTransform(fromCRS, toCRS);
	}
	
	public void switchLatLon(){
		switchLatLon = true;
	}
	
	public double[] to_scenario(JSONArray c){
		double[] temp = to_scenario(c.getDouble(0), c.getDouble(1), c.optDouble(2,0));
		return temp;
	}
	
	public double[] to_scenario(double[] co){
		return to_scenario(co[0], co[1], (co.length > 2) ? co[2]: 0);
	}
	
	public double[] to_scenario(double x, double y, double z){
		if (transform != null){
			ProjCoordinate p1 = new ProjCoordinate(x,y,z);
			ProjCoordinate p2 = new ProjCoordinate();
			transform.transform(p1, p2);
			double p2z = (Double.isNaN(p2.z)) ? 0: p2.z;
			if (this.center != null){
				return wgs84_to_mercator(new double[]{p2.x, p2.y, p2z});
			}
			// PostGIS is still lon/lat
			if (switchLatLon) return new double[]{p2.y,p2.x,p2z};
			return new double[]{p2.x, p2.y, p2z};
		}
		// PostGIS is still lon/lat
		if (switchLatLon)return new double[]{y,x,z};
		return new double[]{x,y,z};
	}
	
	public double[] from_scenario(double[] co){
		if (transform != null){
			if (this.center != null){
				co = mercator_to_wgs84(co);
			}
			ProjCoordinate p1 = new ProjCoordinate(co[0], co[1]);
			ProjCoordinate p2 = new ProjCoordinate();
			transform.transform(p1, p2);
			co[0] = p2.x;
			co[1] = p2.y;
		}
		
		double x = co[0];
		double y = co[1];
		if (switchLatLon){
			x = co[1];
			y = co[0];
		}
		
		if (dim == 2)
			return new double[]{x,y};
		else if (dim == 3)
			return new double[]{x, y, co[2]};
		else if (dim == 4)
			return new double[]{x, y, co[2], co[3]};
		else return co;
	}
	
	public int getDim(){
		return dim;
	}
	
	public int setDim(int dim){
		if (dim > 0 && dim < 5)
			this.dim = dim;
		return this.dim;
	}
	
	public int getSRID(){
		return this.srid;
	}
	
	public boolean shouldStoreSRID(){
		return this.srid != 0;
	}
	
	public double[] getCenter(){
		return this.center;
	}
	
	// TRANSVERSE MERCATOR PROJECTION
	// ref: https://github.com/vvoovv/blender-geo/blob/master/transverse_mercator.py
	
	private double[] wgs84_to_mercator(double[] co){
		float scale = 1.0F;
		double[] merc = new double[3];
		// FIXME: seems to work with EPSG:21781 only if lat/lon are flipped (co[0 <-> 1], co[1 <-> 0])
		double lat = Math.toRadians(co[1]);
		double lon = Math.toRadians(co[0] - center[1]);
		double B = Math.sin(lon) * Math.cos(lat);
		merc[0] = 0.5 * scale * earth_radius * Math.log((1+B)/(1-B));
		merc[1] = scale * earth_radius * ( Math.atan(Math.tan(lat)/Math.cos(lon)) - Math.toRadians(center[0]) );
		merc[2] = (co.length > 2) ? co[2] : 0;
		return merc;
	}
	
	private double[] mercator_to_wgs84(double[] co){
		float scale = 1.0F;
		double[] wgs84 = new double[3];
		double x = co[0]/(scale * earth_radius);
		double y = co[1]/(scale * earth_radius);
		double D = y + Math.toRadians(center[0]);
		double lon = Math.atan(Math.sinh(x)/Math.cos(D));
		double lat = Math.asin(Math.sin(D)/Math.cosh(x));

		wgs84[0] = center[1] + Math.toDegrees(lon);
		wgs84[1] = Math.toDegrees(lat);
		wgs84[2] = (co.length > 2) ? co[2] : 0;
		return wgs84;
	}
	
	private double[] validateCenter(double[] center){
		if (center != null) return center;
		else return new double[]{0,0,0};
	}
	
	private int parseEPSG(String epsg){
		return Integer.parseInt(epsg.split(":")[1]);
	}

	/**
	 * @return
	 */
	public String getEPSG() {
		if (srid != 0) return "EPSG:"+srid;
		else return null;
	}
}
