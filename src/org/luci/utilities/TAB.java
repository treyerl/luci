package org.luci.utilities;

import java.sql.Array;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.luci.Luci;

public class TAB {
	public static class Checksum {
		public final String md5;
		public final String format;
		public Checksum(Array pointer) throws SQLException{
			Object[] c = (Object[]) pointer.getArray();
			if (c.length > 0) format = (String) c[0];
			else format = null;
			if (c.length > 1) md5 = (String) c[1];
			else md5 = null;
		}
		public Checksum(String format, String md5){
			this.format = format;
			this.md5 = md5;
		}
		public Array getSQLArray(java.sql.Connection con) throws SQLException{
			return con.createArrayOf("varchar", new String[]{format, md5});
		}
		public String toString(){
			return md5+"."+format;
		}
	}
	
	public static class Transform {
		public final double[][] t;
		public Transform(Array pointer){
			if (pointer != null) t = new double[4][];
			else t = null;
		}
		public Transform(double[][] t){
			this.t = t;
		}
		public Array getSQLArray(java.sql.Connection con) throws SQLException {
			// con.createArrayOf("float8", transform));
			return null;
		}
	}
	
	/* STATIC */
	
	// Table types & names:
	public final static short USERS = 0;
	public final static short USERDETAILS = 1;
	public final static short SCENARIOS = 2;
	public final static short SCN = 3;
	public final static short SCN_HISTORY = 4;
	public final static short SCN_VARIANT_MASK = 5;
	public final static short SCN_VARIANTS = 6;
	public final static short SERVICES = 7;
	public final static short SERVICEINPUTS = 8;
	public final static short SRV_IN_HISTORY = 9;
	public final static short SERVICEOUTPUTS = 10;
	public final static short SRV_OUT_HISTORY = 11;
	public final static short SERVICECALLS = 12;
	public final static short VIEW = 13;
	public final static short SCN_STR_TYPES = 14;
	
	public final static short INPUT = 101;
	public final static short OUTPUT = 102;
	public final static short flagDELETED  = 0b00000001;
	public final static short flagEXPORTED = 0b00000010;
	
	public final static int NURB_CIRCULARSTRING = 1;
	public final static int NURB_CIRCLE = 2;
	public final static int NURB_2BEZIER = 3;
	public final static int NURB_3BEZIER = 4;
	
	public final static short NUMERIC = 1;
	public final static short INTEGER = 2;
	public final static short STRING = 3; 
	public final static short BOOLEAN = 4;
	public final static short CHECKSUM = 5;
	public final static short TIMESTAMP = 6;
	public final static short GEOMETRY = 7;
	public final static short NUMERICARRAY = 8;
	public final static short JSON = 10;
	public final static short LIST = 11;
	
	public final static String DEFAULTLAYER = "default";
	
	public final static Map<Short, String> TYPE_NAMES = new HashMap<Short, String>();
	static {
		TYPE_NAMES.put(NUMERIC, "numeric");
		TYPE_NAMES.put(STRING, "string");
		TYPE_NAMES.put(BOOLEAN, "boolean");
		TYPE_NAMES.put(CHECKSUM, "checksum");
		TYPE_NAMES.put(TIMESTAMP, "timestamp");
		TYPE_NAMES.put(GEOMETRY, "geometry");
		TYPE_NAMES.put(NUMERICARRAY, "numeric_array");
		TYPE_NAMES.put(JSON, "json");
		TYPE_NAMES.put(LIST, "list");
	}
	
	public final static List<Short> UIDs = Arrays.asList(USERDETAILS);
	public final static List<Short> SOBJIDs = Arrays.asList(SERVICEINPUTS,SRV_IN_HISTORY,SERVICECALLS,SERVICEOUTPUTS,SRV_OUT_HISTORY);
	public final static List<Short> GEOMIDs = Arrays.asList(SCN, SCN_HISTORY, SCN_VARIANT_MASK);
	
	public final static int srid = Luci.getSRID();
	
	public static String scn_base = "lc_sc_";
	
	public static Map<String, Short> merge(Map<String, Short> att_base, Map<String, Short> att_add){
		Map<String, Short> added = new LinkedHashMap<String, Short>();
		for (String att: att_add.keySet()){
			short t = att_add.get(att);
			if (att_base.containsKey(att)){
				if (t != att_base.get(att)) {
					String key = att+"_"+TYPE_NAMES.get(t);
					att_base.put(key, t);
					added.put(key, t);
				}
			} else {
				att_base.put(att, t);
				added.put(att, t);
			}
		}
		return added;
	}
	
	public static short JSONType2SQL(Object o){
		if (o instanceof JSONArray) return LIST;
		else if (o instanceof JSONObject) {
			if (((JSONObject) o).has("streaminfo")) return CHECKSUM;
			return JSON;
		}
		else if (o instanceof String) return STRING;
		else if (o instanceof Number) return NUMERIC;
		else if (o instanceof Boolean) return BOOLEAN;
		return 0;
	}
	
	public static String getTableName(short tableType){
		switch(tableType){
		case(USERS): return "LC_USERS";
		case(USERDETAILS): return "LC_USER_DETAILS";
		case(SCENARIOS): return "lc_scenarios";
		case(SCN_STR_TYPES): return "lc_scn_str_types";
		case(SCN): return scn_base;
		case(SERVICES): return "lc_service_instances";
		case(SERVICEINPUTS): return "lc_service_inputs";
		case(SRV_IN_HISTORY): return "lc_srv_in_history";
		case(SERVICECALLS): return "lc_service_calls";
		case(SERVICEOUTPUTS): return "lc_service_outputs";
		case(SRV_OUT_HISTORY): return "lc_srv_out_history";
		case(VIEW): return "all_lc_geometry";
		default: return null;
		}
	}
	
	public static String getScenarioTable(long ScID, short tableType){
		switch(tableType){
		case SCN: return scn_base+ScID;
		case SCN_HISTORY: return scn_base+ScID+"_h";
		case SCN_VARIANT_MASK: return scn_base+ScID+"_variant_mask";
		case SCN_VARIANTS: return scn_base+ScID+"_variants";
		default: return null;
		}
	}
	
	public static String getIdName(short tabletype){
		if (SOBJIDs.contains(tabletype)) return "SObjID";
		if (UIDs.contains(tabletype)) return "uID";
		if (GEOMIDs.contains(tabletype)) return "geomID";
		return "ID";
	}
	
	public static String getName(short tableType){
		switch(tableType){
		case(USERS): return "user";
		case(USERDETAILS): return "user details";
		case(SCENARIOS): return "Scenario";
		case(SCN_STR_TYPES): return "Scenario Attribute String Types";
		case(SCN): return "scenario repository";
		case(SCN_HISTORY): return "scenario history";
		case(SCN_VARIANT_MASK): return "scenario variant mask";
		case(SCN_VARIANTS): return "scenario variant names";
		case(SERVICES): return "service instance";
		case(SERVICEINPUTS): return "service instance input parameter";
		case(SRV_IN_HISTORY): return "service input history";
		case(SERVICECALLS): return "service calls (input hashes)";
		case(SERVICEOUTPUTS): return "service instance outputs";
		case(SRV_OUT_HISTORY): return "service output history";
		case(VIEW): return "2d view of all geometry in the geometry table (to view with QGIS)";
		default: return null;
		}
	}
	
	public static List<String> listAllTables(){
		List<String> tables = new ArrayList<String>();
		for (short i = 0; i <= 11; i++){
			tables.add(getTableName(i));
		}
		return tables;
	}
}
