package org.luci.utilities;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**Used to select data based on timestamps. Basic selectors: at (exactly), from, until, before, after, between.
 *
 */
public class TimeRangeSelection {
	private Timestamp at;
	private Timestamp from;
	private Timestamp until;
	private Timestamp before;
	private Timestamp after;
	private JSONObject j;
	
	public static TimeRangeSelection NULL(){
		return new TimeRangeSelection();
	}
	
	public TimeRangeSelection() {}
	
	public TimeRangeSelection(JSONObject json){
		j = json;
		if (j.has("at"))  at  = new Timestamp(j.getLong("at"));
		else if (j.has("from"))  from  = new Timestamp(j.getLong("from"));
		else if (j.has("until")) until = new Timestamp(j.getLong("until"));
		else if (j.has("before"))  before  = new Timestamp(j.getLong("before"));
		else if (j.has("after"))  after  = new Timestamp(j.getLong("after"));
		else if (j.has("between")){
			from   = new Timestamp(j.getJSONArray("between").getLong(0));
			until  = new Timestamp(j.getJSONArray("between").getLong(1));
		} else {
			throw new JSONException("No valid key (at, from, until, before, after, between) found in timerange!");
		}
	}
	
	public HistoryTimeRangeSelection getHistoryTimeRangeSelection(){
		return new HistoryTimeRangeSelection(j);
	}
	
	private String whichNotNull(){
		if (from != null && until != null) return "between";
		else if (at != null) return "at";
		else if (from != null) return "from";
		else if (until != null) return "until";
		else if (before != null) return "before";
		else if (after != null) return "after";
		else return "undefined";
	}
	
	public boolean isAllNull(){
		return at == null && from == null && until == null && before == null && after == null;
	}
	
	private void assertNull(){
		if (!isAllNull()) 
			throw new RuntimeException("TimeRangeSelection is set already to "+whichNotNull());
	}
	
	public TimeRangeSelection setAt(Timestamp at){
		assertNull();
		this.at = at;
		return this;
	}
	
	public TimeRangeSelection setFrom(Timestamp from){
		assertNull();
		this.from = from;
		return this;
	}
	
	public TimeRangeSelection setUntil(Timestamp until){
		assertNull();
		this.until = until;
		return this;
	}
	
	public TimeRangeSelection setBefore(Timestamp before){
		assertNull();
		this.before = before;
		return this;
	}
	
	public TimeRangeSelection setAfter(Timestamp after){
		assertNull();
		this.after = after;
		return this;
	}
	
	public TimeRangeSelection setBetween(Timestamp from, Timestamp until){
		assertNull();
		this.from = from;
		this.until = until;
		return this;
	}
	
	public Timestamp getAt(){
		return at;
	}
	
	public Timestamp getFrom(){
		return from;
	}
	
	public Timestamp getUntil(){
		return until;
	}
	
	public Timestamp getBefore(){
		return before;
	}
	
	public Timestamp getAfter(){
		return after;
	}
	
	public boolean isAt(){
		return at != null;
	}
	
	public boolean isFrom(){
		return from != null;
	}
	
	public boolean isUntil(){
		return until != null;
	}
	
	public boolean isBefore(){
		return before != null;
	}
	
	public boolean isAfter(){
		return after != null;
	}
	
	public boolean isBetween(){
		return from != null && until != null;
	}
	
	public boolean validate(Timestamp st){
		if (isBetween()) return ( (st.after(until) || st.equals(until)) 
				&& (st.before(from) || st.equals(from)) );
		else if (isAt()) return st.equals(at);
		else if (isFrom()) return st.after(from) || st.equals(from);
		else if (isUntil()) return st.before(until) || st.equals(until);
		else if (isBefore()) return st.before(before);
		else if (isAfter()) return st.after(after);
		return false;
	}
	
	public int setTimestampSQL(PreparedStatement ps, int index) throws SQLException{
		if (isAllNull()) return index;
		if (isBetween()){
			ps.setTimestamp(index, from);
			ps.setTimestamp(index + 1, until);
			return index + 2;
		} else if (isBefore()){
			ps.setTimestamp(index, before);
		} else if (isAfter()){
			ps.setTimestamp(index, after);
		} else if (isFrom()){
			ps.setTimestamp(index, from);
		} else if (isUntil()){
			ps.setTimestamp(index, until);
		} else if (isAt()){
			ps.setTimestamp(index, at);
		}
		return index + 1;
	}
	
	public boolean equals(TimeRangeSelection trs){
		if (isBetween()) return (from.equals(trs.getFrom()) && until.equals(trs.getUntil()));
		else if (isBefore()) return (before.equals(trs.getBefore()));
		else if (isAfter()) return (after.equals(trs.getAfter()));
		else if (isUntil()) return (until.equals(trs.getUntil()));
		else if (isFrom()) return (from.equals(trs.getFrom()));
		else if (isAt()) return (at.equals(trs.getAt()));
		return false;
	}
	
	public String getSQL(){
		String t = "timestamp";
		if (isAllNull()) return "";
		String timestamp = t+" = ?;"; // = isAt()
		if (isFrom())    timestamp = t+" >= ?";
		else if (isUntil())   timestamp = t+" <= ?";
		else if (isBefore())  timestamp = t+" < ?";
		else if (isAfter())   timestamp = t+" > ?";
		else if (isBetween()) timestamp = t+" >= ? AND "+t+" <= ?";
		return " AND "+timestamp;
	}
	
	public JSONObject getTimerangeJSON(){
		JSONObject timerange = new JSONObject();
		if (isBetween()) timerange.put("between", new JSONArray().put(from.getTime()).put(until.getTime()));
		if (isAt()) timerange.put("at", at.getTime());
		if (isFrom()) timerange.put("from", from.getTime());
		if (isUntil()) timerange.put("until", until.getTime());
		if (isBefore()) timerange.put("before", before.getTime());
		if (isAfter()) timerange.put("after", after.getTime());
		return timerange;
	}
	
}
