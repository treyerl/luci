﻿using CollabSample.ViewModels;
using collact.go.Communication;
//using LineService.Communication;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace CollabSample.Communication
{
    
    public class Communication2Lucy 
    {
        public StreamReader OutputStream { get; private set; }
        //private StreamWriter InputStream;
        private BinaryWriter InputStream;
    //    private LineReader OutputStream;

        public Communication2Lucy() {
            //connect ("localhost", 7654);
        }
    
        
    public bool connect(String host, int port) {
        TcpClient socketForServer;
        try
        {
            socketForServer = new TcpClient(host, port);
        }
        catch
        {
            Console.WriteLine(
            "Failed to connect to server at {0}:{1}", host, port);
            return false;
        }
       
        NetworkStream networkStream = socketForServer.GetStream();
        System.IO.StreamReader streamReader =
        new System.IO.StreamReader(networkStream);
        System.IO.StreamWriter streamWriter =
        new System.IO.StreamWriter(networkStream);
        Console.WriteLine("******* Trying to communicate with lucy on host " + host + " and port " + port + " *****");

      //  streamWriter.WriteLine("{'action':'authenticate','username':'lukas','userpasswd':'1234'}");
      //  streamWriter.Flush();

        OutputStream = new StreamReader(networkStream);
        //InputStream = new StreamWriter(networkStream);
        InputStream = new BinaryWriter(networkStream, System.Text.Encoding.UTF8);
     //   OutputStream = new LineReader(networkStream, System.Text.Encoding.UTF8);

        return true;
        try
        {
            string outputString;
            // read the data from the host and display it
            {
                //outputString = streamReader.ReadLine();
                //Console.WriteLine("Message Recieved by server:" + outputString);

                //Console.WriteLine("Type your message to be recieved by server:");

                streamWriter.WriteLine("{'action':'authenticate','username':'lukas','userpasswd':'1234'}");
                streamWriter.Flush();

                //outputString = streamReader.ReadLine();


                Console.WriteLine("type:");
                string str = Console.ReadLine();
                str = "exit";
                while (str != "exit")
                {
                    streamWriter.WriteLine(str);
                    streamWriter.Flush();
                    Console.WriteLine("type:");
                    str = Console.ReadLine();
                }
                if (str == "exit")
                {
                    streamWriter.WriteLine(str);
                    streamWriter.Flush();
                   
                }
                
            }
        }
        catch
        {
            Console.WriteLine("Exception reading from Server");
        }
        // tidy up
        networkStream.Close();
        //Console.WriteLine("Press any key to exit from client program");
        //Console.ReadKey();
    }

    public JObject sendAction2Lucy(string action)
    {
        //InputStream.WriteLine(action);
        InputStream.Write(Encoding.UTF8.GetBytes(action + "\n"));
        InputStream.Flush();

        string json = OutputStream.ReadLine();
        var jObj2 = (JObject)JsonConvert.DeserializeObject(json);

        return jObj2;
    }

    public JObject sendAction2Lucy(string action, byte[] byteStream)
    {
        if (byteStream == null)
        {
            //InputStream.WriteLine(action);
            InputStream.Write(Encoding.UTF8.GetBytes(action + "\n"));
        }
        else
        {
            //InputStream.WriteLine(action);
            InputStream.Write(Encoding.UTF8.GetBytes(action + "\n"));
            InputStream.Flush();

            //   byte len = (byte)byteStream.Length;
            // InputStream.Write(len);            

            if (byteStream != null)
                 InputStream.Write(byteStream);
    //           InputStream.WriteLine(byteStream);
        }

        InputStream.Flush();

        string json = OutputStream.ReadLine();
        var jObj2 = (JObject)JsonConvert.DeserializeObject(json);

        return jObj2;
    }

    public JObject sendAction2Lucy(string action, byte[] byteStream, byte[] byteStream2)
    {
        //InputStream.WriteLine(action);
        InputStream.Write(Encoding.UTF8.GetBytes(action + "\n"));
        InputStream.Flush();

        long len1 = byteStream.Length;
        byte[] arr1 = BitConverter.GetBytes(len1).Reverse().ToArray();;
        InputStream.Write(arr1);
        InputStream.Flush();
        InputStream.Write(byteStream);
        InputStream.Flush();

        long len2 = byteStream2.Length;
        byte[] arr2 = BitConverter.GetBytes(len2).Reverse().ToArray();;
        InputStream.Write(arr2);
        InputStream.Write(byteStream2);
        InputStream.Flush();

        string json = OutputStream.ReadLine();
        var jObj2 = (JObject)JsonConvert.DeserializeObject(json);

        return jObj2;
    }


    public ConfigSettings sendCreateService2Lucy(string command, int scenarioID)
    {
        ConfigSettings result = null;

        //InputStream.WriteLine(command);
        InputStream.Write(Encoding.UTF8.GetBytes(command + "\n")); 
        InputStream.Flush();

        string json = OutputStream.ReadLine();

        try
        {
            var jObj2 = (JObject)JsonConvert.DeserializeObject(json);

            //int serviceID = ((Newtonsoft.Json.Linq.JProperty)(jObj2.First)).Value.Value<int>("ID");
            //string mqtt_topic = "/KicT/Models/ETH/" + scenarioID + "/" + serviceID;

            int serviceID = jObj2.Value<JObject>("result").Value<int>("SObjID");
            string mqtt_topic = jObj2.Value<JObject>("result").Value<String>("mqtt_topic");
            mqtt_topic = mqtt_topic.Substring(0, mqtt_topic.Length - 1);

            result = new ConfigSettings() { scenarioID = scenarioID, mqtt_topic = mqtt_topic, objID = serviceID };

            //       MqttManager.Subscribe(mqtt_topic);
            //       MqttManager.publish(mqtt_topic, "RUN");
        }
        catch (Exception ex)
        {
            Console.Out.WriteLine(ex.Message);
        }

        return result;
    }

    public JObject sendService2Lucy(string command)
    {
        //InputStream.WriteLine(command);
        InputStream.Write(Encoding.UTF8.GetBytes(command + "\n")); 
        InputStream.Flush();

        string json = OutputStream.ReadLine();
        var jObj2 = (JObject)JsonConvert.DeserializeObject(json);

        if (jObj2.Value<JToken>("result") != null)
        {
            if (((Newtonsoft.Json.Linq.JProperty)(jObj2.Value<JToken>("result").First())).Name.Equals("mqtt_topic"))
            {
                json = OutputStream.ReadLine();
                jObj2 = (JObject)JsonConvert.DeserializeObject(json);
            }

            if (jObj2.Value<JToken>("result") != null) // && ((Newtonsoft.Json.Linq.JProperty)(jObj2.Value<JToken>("result").First())).Name.Equals("mqtt"))
                return ((Newtonsoft.Json.Linq.JObject)(jObj2.Value<JToken>("result")));
        }
        return null;
    }

    public JProperty authenticate(string user, string password)
    {   
        return (JProperty)sendAction2Lucy("{'action':'authenticate','username':'" + user + "','userpasswd':'" + password + "'}").First;

        //InputStream.WriteLine("{'action':'authenticate','username':'" + user+ "','userpasswd':'" + password + "'}");
        InputStream.Write("{'action':'authenticate','username':'" + user + "','userpasswd':'" + password + "'}");
        InputStream.Flush();

        string json = OutputStream.ReadLine();
        var jObj2 = (JObject)JsonConvert.DeserializeObject(json);

        return ((Newtonsoft.Json.Linq.JProperty)(jObj2.First));
    }

    public JProperty getActionsList()
    {
        return (JProperty) sendAction2Lucy("{'action':'get_list','of':['actions']}").First;

        //InputStream.WriteLine("{'action':'get_list','of':['actions']}");
        InputStream.Write("{'action':'get_list','of':['actions']}");
        InputStream.Flush();

        string json = OutputStream.ReadLine();
        var jObj2 = (JObject)JsonConvert.DeserializeObject(json);

        return ((Newtonsoft.Json.Linq.JProperty)(jObj2.First));
    }

    public JProperty getActionParameters(string action)
    {
        return (JProperty) sendAction2Lucy("{'action':'get_infos_about','actionname':'" + action + "'}").First;
    }

    public JProperty getServiceParameters(string service)
    {
        return (JProperty)sendAction2Lucy("{'action':'get_infos_about','servicename':'" + service + "'}").First;
    }

    public JProperty getServicesList()
    {
        return (JProperty)sendAction2Lucy("{'action':'get_list','of':['services']}").First;

        //InputStream.WriteLine("{'action':'get_list','of':['services']}");
        InputStream.Write("{'action':'get_list','of':['services']}");
        InputStream.Flush();

        string json = OutputStream.ReadLine();
        var jObj2 = (JObject)JsonConvert.DeserializeObject(json);

        return ((Newtonsoft.Json.Linq.JProperty)(jObj2.First));
    }

    private static string GetData()
    {
        //Ack from sql server
        return "ack";
    }

        /*
        public class Connection extends Thread{	
	Socket socket;
	ClientConsole console;
	BufferedReader input;
	DataOutputStream output;
	PrintStream print;
	boolean stopTalking = false;
		
	public Connection() throws IOException, ConnectException{		
		this.connect();
	}	   	
	
	private void connect() throws IOException{		

		System.out.println("connect");
		InetAddress host = InetAddress.getLocalHost();
        this.socket = new Socket(host.getHostName(), 7654);
		this.console = new ClientConsole(this);
        this.input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        this.output = new DataOutputStream(socket.getOutputStream());
		this.print = new PrintStream(socket.getOutputStream()); 
	}		*/
    }
}
