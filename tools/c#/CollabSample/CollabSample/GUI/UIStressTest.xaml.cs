﻿using CollabSample.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CollabSample.GUI
{
    /// <summary>
    /// Interaction logic for UIStressTest.xaml
    /// </summary>
    public partial class UIStressTest : UserControl
    {
        #region ViewModel dependency property

        public static DependencyProperty ViewModelProperty =
            DependencyProperty.Register(
                "ViewModel",
                typeof(GeoVM),
                typeof(UIStressTest),
                null
            );

        public GeoVM ViewModel
        {
            get { return (GeoVM)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }
        #endregion

        #region DataVM dependency property
        public static DependencyProperty DataVMProperty =
            DependencyProperty.Register(
                "DataVM",
                typeof(StressTestVM),
                typeof(UIStressTest),
                null
            );

        public StressTestVM DataVM
        {
            get { return (StressTestVM)GetValue(DataVMProperty); }
            set { SetValue(DataVMProperty, value); }
        }
        #endregion

        public UIStressTest()
        {
            InitializeComponent();
/*
            DataContext = new NestedItemsViewModel()
            {
                Level1 = new System.Collections.ObjectModel.ObservableCollection<Level1Item>(Enumerable.Range(0, 10)
                                   .Select(l1 => new Level1Item()
                                   {
                                       Level2 = new System.Collections.ObjectModel.ObservableCollection<Level2Item>(Enumerable.Range(0, 10)
                                                          .Select(l2 => new Level2Item { Value = l1.ToString() + "-" + l2.ToString() })
                                                          .ToList())
                                   })
                                    .ToList())
            };
*/

//            Loaded += UIStressTest_Loaded;
            IsVisibleChanged += UIStressTest_IsVisibleChanged;
        }

        void UIStressTest_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if ((bool)e.NewValue == true)
            {
                if (DataContext != null)
                {
                    ViewModel = DataContext as GeoVM;
                    DataVM = new StressTestVM(ViewModel);
                    ViewModel.StressTestVM = DataVM;
                }
            }
        }

        void UIStressTest_Loaded(object sender, RoutedEventArgs e)
        {
            
            ViewModel = DataContext as GeoVM;
            DataVM = new StressTestVM(ViewModel);
            ViewModel.StressTestVM = DataVM;
        }
    }
}
