﻿using CollabSample.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CollabSample.GUI
{
    /// <summary>
    /// Interaction logic for VisualizationGrid.xaml
    /// </summary>
    public partial class VisualizationGrid : UserControl
    {
        #region ViewModel dependency property

        public static DependencyProperty ViewModelProperty =
            DependencyProperty.Register(
                "ViewModel",
                typeof(GeoVM),
                typeof(VisualizationGrid),
                null
            );

        public GeoVM ViewModel
        {
            get { return (GeoVM)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }
        #endregion

        #region VM dependency property
        public static DependencyProperty VMProperty =
            DependencyProperty.Register(
                "VM",
                typeof(VisualizationGridVM),
                typeof(VisualizationGrid),
                null
            );

        public VisualizationGridVM VM
        {
            get { return (VisualizationGridVM)GetValue(VMProperty); }
            set { SetValue(VMProperty, value); }
        }
        #endregion


//        public ObservableCollection<System.Windows.Rect> Buildings { get; set; }

 //       public int Index { get; set; }

        public VisualizationGrid()
        {
            InitializeComponent();

            VM = new VisualizationGridVM();

            Loaded += VisualizationGrid_Loaded;
            SizeChanged += VisualizationGrid_SizeChanged;
        }


        void VisualizationGrid_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            initCanvas();
        }

        void VisualizationGrid_Loaded(object sender, RoutedEventArgs e)
        {
            //ViewModel = new GeoVM();
            ViewModel = DataContext as GeoVM;

//            Buildings = ViewModel.AllBuildings[0];
           // ViewModel.Submissions = new System.Collections.ObjectModel.ObservableCollection<Data.SubmissionResult>();
            initCanvas();
        }

        private void initCanvas()
        {
            //Buildings = ViewModel.AllBuildings[0];
            gridCanvas.Children.Clear();

            int deltaX = 10;
            int deltaY = 10;

            int numCols = (int)Math.Ceiling(gridCanvas.ActualWidth / deltaX);
            int numRows = (int)Math.Ceiling(gridCanvas.ActualHeight / deltaY);

            for (int c=0; c < numCols; c++)
            {
                Rectangle rect = new Rectangle()
                {
                    Width = 1,
                    Height = gridCanvas.ActualHeight,
                    Stroke = new SolidColorBrush(Colors.DarkGray),
                    StrokeThickness = 1
                };

                Canvas.SetTop(rect, 0);
                Canvas.SetLeft(rect, c * deltaX);

                gridCanvas.Children.Add(rect);
            }

            for (int r = 0; r < numRows; r++)
            {
                Rectangle rect = new Rectangle()
                {
                    Width = gridCanvas.ActualWidth,
                    Height = 1,
                    Stroke = new SolidColorBrush(Colors.DarkGray),
                    StrokeThickness = 1
                };

                Canvas.SetTop(rect, r * deltaY);
                Canvas.SetLeft(rect, 0);

                gridCanvas.Children.Add(rect);
            }
        }
    }
}
