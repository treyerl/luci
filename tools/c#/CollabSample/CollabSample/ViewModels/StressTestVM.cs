﻿using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.ViewModel;
using Microsoft.Win32;
using ReadWriteCsv;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Shapes;
using uPLibrary.Networking.M2Mqtt;

namespace CollabSample.ViewModels
{
    [Serializable]
    public class StressTestSettings : NotificationObject
    {
        public ObservableCollection<UILine> lines { get; set; }
        public int delay { get; set; }
        public int scenarioID { get; set; }
        public int serviceID { get; set; }
        public string mqtt_topic { get; set; }
        public DateTime startScenario { get; set; }
        public DateTime finishedScenario { get; set; }
        public DateTime startService { get; set; }
        public DateTime finishedService { get; set; }

        private int _state;
        public int state
        {
            get { return _state; }
            set
            {
                _state = value;
                RaisePropertyChanged("state");
            }
        }
    }

    [Serializable]
    public class UILine
    {
        public double X1 { get; set; }
        public double X2 { get; set; }
        public double Y1 { get; set; }
        public double Y2 { get; set; }
    }

    public class StressTestVM : NotificationObject
    {
        public ObservableCollection<StressTestSettings> Settings { get; set; }

        private ObservableCollection<StressTestSettings> _settings;
        public ObservableCollection<StressTestSettings> settings
        {
            get { return _settings; }
            set
            {
                _settings = value;
                RaisePropertyChanged("settings");
            }
        }


        private GeoVM ViewModel;
        private Random r = new Random();

        private MqttClient mqttClient;


        public int numScenarios { get; set; }
        public int numLinesMin { get; set; }
        public int numLinesMax { get; set; }
        public int delayMin { get; set; }
        public int delayMax { get; set; }

        // Commands
        public ICommand StartStressTestCommand { get; private set; }
        public ICommand StopStressTestCommand { get; private set; }
        public ICommand ExportFileCommand { get; private set; }
        public ICommand CreateSettingsCommand { get; private set; }
        public ICommand LoadSettingsCommand { get; private set; }
        public ICommand SaveSettingsCommand { get; private set; }

        public StressTestVM(GeoVM VM)
        {
            ViewModel = VM;
            settings = new ObservableCollection<StressTestSettings>();
            Settings = new ObservableCollection<StressTestSettings>();

            numLinesMin = 1;
            numLinesMax = 10;
            numScenarios = 10;
            delayMin = 0;
            delayMax = 5;

            string host = VM.getMqttHost();
            if (host != null)
            {
                mqttClient = new MqttClient(host);
                //            mqttClient = new MqttClient(System.Net.Dns.GetHostName());
                //          mqttClient = new MqttClient(System.Net.IPAddress.Parse("129.132.6.35"));
                int r = new Random(DateTime.Now.Millisecond).Next();
                mqttClient.Connect("StressTest_" + r.ToString());
                mqttClient.MqttMsgPublishReceived += mqttClient_MqttMsgPublishReceived;
            }

            StartStressTestCommand = new DelegateCommand(OnStartStressTestExecuted);
            StopStressTestCommand = new DelegateCommand(OnStopStressTestExecuted);
            ExportFileCommand = new DelegateCommand(OnExportFileExecuted);

            CreateSettingsCommand = new DelegateCommand(OnCreateSettingsExecuted);
            LoadSettingsCommand = new DelegateCommand(OnLoadSettingsExecuted);
            SaveSettingsCommand = new DelegateCommand(OnSaveSettingsExecuted);
        }

        void mqttClient_MqttMsgPublishReceived(object sender, uPLibrary.Networking.M2Mqtt.Messages.MqttMsgPublishEventArgs e)
        {
            if (Settings != null)
            {
                StressTestSettings foundSettings = null;

                try
                {
                    foundSettings = Settings.Where(p => p.mqtt_topic == e.Topic).First();
                }
                catch (Exception ex)
                {
                    // no matching key found
                    Console.Out.WriteLine("not found: " + e.Topic);
                }

                if (foundSettings != null)
                {
                    string message = Encoding.UTF8.GetString(e.Message);
                    if (message == "DONE")
                    {
                        foundSettings.state = 2;
                        foundSettings.finishedService = DateTime.Now;

                        // read results for given key
                        System.Windows.Application.Current.Dispatcher.Invoke((Action)(delegate
                        {
                            readResultsOfService(foundSettings);
                        }));
                    }
                    else
                    {
                        if (message == "RUN")
                        {
                            foundSettings.state = 1;
                            foundSettings.startService = DateTime.Now;
                        }
                        else
                        {
                            foundSettings.state = 10;
                        }
                    }
                }
            }
        }

        private Line[] CreateSetup(int numLines)
        {
            Line[] lines = new Line[numLines];

            for (int i = 0; i < numLines; i++)
            {
                lines[i] = new Line() { X1 = r.Next(100), X2 = r.Next(100), Y1 = r.Next(50), Y2 = r.Next(50) };
            }

            return lines;
        }

        private UILine[] CreateSetup2(int numLines)
        {
            UILine[] lines = new UILine[numLines];

            for (int i = 0; i < numLines; i++)
            {
                lines[i] = new UILine() { X1 = r.Next(100), X2 = r.Next(100), Y1 = r.Next(50), Y2 = r.Next(50) };
            }

            return lines;
        }

        private void OnCreateSettingsExecuted()
        {
            if (Settings == null)
                Settings = new ObservableCollection<StressTestSettings>();

            Settings.Clear();

            int minLines = numLinesMin > 0 ? numLinesMin : 1;
            int maxLines = numLinesMax > 0 && numLinesMax >= minLines ? numLinesMax : 1;

            for (int s = 0; s < numScenarios; s++)
            {
                int numLines = r.Next(minLines, maxLines + 1);
                int delay = r.Next(delayMin, delayMax + 1);
                UILine[] lines = CreateSetup2(numLines);

                Settings.Add(new StressTestSettings() { lines = new ObservableCollection<UILine>(lines), delay = delay, state = 0 });
            }

            RaisePropertyChanged("Settings");
        }


        private void OnLoadSettingsExecuted()
        {
            OpenFileDialog loadFileDialog1 = new OpenFileDialog();
            loadFileDialog1.Filter = "Serialized Text files (*.txt)|*.txt|All files (*.*)|*.*";

            if (loadFileDialog1.ShowDialog() == true)
            {
                string filePath = loadFileDialog1.FileName;

                Settings.Clear();

                using (Stream stream = File.Open(filePath, FileMode.Open))
                {
                    var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                    Settings = (ObservableCollection<StressTestSettings>)binaryFormatter.Deserialize(stream);
                    RaisePropertyChanged("Settings");
                }
            }
        }

        private void OnSaveSettingsExecuted()
        {
            if (Settings == null || Settings.Any() == false)
            {
                MessageBox.Show("No Settings found. Please create!");
                return;
            }

            if (Settings != null && Settings.Any() == true)
            {
                SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                saveFileDialog1.Filter = "Serialized Text files (*.txt)|*.txt|All files (*.*)|*.*";

                if (saveFileDialog1.ShowDialog() == true)
                {
                    string filePath = saveFileDialog1.FileName;
                    using (Stream stream = File.Open(filePath, FileMode.Create))
                    {
                        var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                        binaryFormatter.Serialize(stream, Settings);
                    }
                }
            }
        }

        private void OnStartStressTestExecuted()
        {
            if (Settings == null || Settings.Any() == false)
            {
                MessageBox.Show("No Settings found. Please create or load settings first of all!");
                return;
            }

            System.Threading.Tasks.Task.Factory.StartNew(() =>
            {
                foreach (StressTestSettings entry in Settings)
                {
                    entry.startScenario = DateTime.Now;
                    entry.scenarioID = ViewModel.createScenario4StressTest(entry.lines.ToArray<UILine>());
                    entry.finishedScenario = DateTime.Now;
                    ConfigSettings configInfo = startService(entry.scenarioID, entry.delay);

                 
                    System.Windows.Application.Current.Dispatcher.BeginInvoke((Action)(() =>
                    {
                        entry.serviceID = configInfo.objID;
                        entry.state = 0;
                        entry.mqtt_topic = configInfo.mqtt_topic;
                        mqttClient.Publish(configInfo.mqtt_topic, Encoding.UTF8.GetBytes("RUN"));
                    }),
                        System.Windows.Threading.DispatcherPriority.Normal, null);
                }
            }, System.Threading.Tasks.TaskCreationOptions.LongRunning);



            /*
            int minLines = numLinesMin > 0 ? numLinesMin : 1;
            int maxLines = numLinesMax > 0 && numLinesMax >= minLines ? numLinesMax : 1;


            Settings.Clear();
            RaisePropertyChanged("Settings");
            
            System.Threading.Tasks.Task.Factory.StartNew(() =>
            {
                for (int s = 0; s < numScenarios; s++)
                {
                    int numLines = r.Next(minLines, maxLines + 1);
                    int delay = r.Next(delayMin, delayMax + 1);
                    UILine[] lines = CreateSetup2(numLines);
                    DateTime startScenario = DateTime.Now;
                    int scID = ViewModel.createScenario4StressTest(lines);
                    DateTime endScenario = DateTime.Now;
                    ConfigSettings configInfo = startService(scID, delay);

                    System.Windows.Application.Current.Dispatcher.BeginInvoke((Action)(() =>
                    {
                        Settings.Add(new StressTestSettings() { scenarioID = scID, serviceID = configInfo.objID, lines = new ObservableCollection<UILine>(lines), delay = delay, state = 0, mqtt_topic = configInfo.mqtt_topic, startScenario = startScenario, finishedScenario = endScenario });
                        mqttClient.Publish(configInfo.mqtt_topic + "/", Encoding.UTF8.GetBytes("RUN"));
                    }),
                        System.Windows.Threading.DispatcherPriority.Normal, null);
                }
            }, System.Threading.Tasks.TaskCreationOptions.LongRunning);*/
        }

        private ConfigSettings startService(int scenarioID, int delay)
        {
            string mqtt_topic = null;

            string msg = "{'action':'create_service','inputs':{'delay':" + delay
                + "}, 'classname':'LineService','ScID':" + scenarioID + "}";

            ConfigSettings configInfo = ViewModel.startService(msg, scenarioID);
            if (configInfo != null && configInfo.mqtt_topic != null)
            {
                if (mqttClient.IsConnected)
                {
                    mqttClient.Subscribe(new string[] { configInfo.mqtt_topic }, new byte[] { 1 });
                    //  mqttClient.Publish(configInfo.mqtt_topic + "/", Encoding.UTF8.GetBytes("RUN"));
                    mqtt_topic = configInfo.mqtt_topic;
                }
            }

            return configInfo;
        }

        private void readResultsOfService(StressTestSettings settings)
        {
            /*
            // {'action':'get_outputs_of','SObjID':115}
            string msg = "{'action':'get_service_outputs','SObjID':" + instanceID + "}";

            JObject result = cl.sendAction2Lucy(msg);
            if (result != null)
            {
                JToken outputs = result.Value<JToken>("result").Value<JToken>("outputs");
                if (outputs != null)
                {
                    string output = outputs.Value<JObject>("outputVals").Value<String>("value");
                    // für ecoPot
                    // outputs.Value<JObject>("potential").Value<String>("value")
                    AddText2InfoMW("result: " + outputs + " - output rows: " + output + "\n");
                }
            }*/
        }

        private void OnStopStressTestExecuted()
        {
        }


        private void OnExportFileExecuted()
        {
            if (Settings != null)
            {
                SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                saveFileDialog1.Filter = "Comma-separated Text files (*.csv, *.txt)|*.csv;*.txt|All files (*.*)|*.*";

                if (saveFileDialog1.ShowDialog() == true)
                {
                    string csvFilename = saveFileDialog1.FileName;

                    using (CsvFileWriter writer = new CsvFileWriter(csvFilename))
                    {
                        CsvRow title = new CsvRow();
                        title.Add("scenario ID");
                        title.Add("service ID");
                        title.Add("lines count");
                        title.Add("delay [s]");
                        title.Add("start time scenario");
                        title.Add("finish time scenario");
                        title.Add("start time service");
                        title.Add("finish time service");
                        title.Add("state");

                        writer.WriteRow(title);

                        foreach (StressTestSettings entry in Settings)
                        {
                            CsvRow row = new CsvRow();

                            row.Add(entry.scenarioID.ToString());
                            row.Add(entry.serviceID.ToString());
                            row.Add(entry.lines.Count().ToString());
                            row.Add(entry.delay.ToString());
                            row.Add(entry.startScenario.ToLongTimeString());
                            row.Add(entry.finishedScenario.ToLongTimeString());
                            row.Add(entry.startService.ToLongTimeString());
                            row.Add(entry.finishedService.ToLongTimeString());
                            row.Add(entry.state.ToString());

                            writer.WriteRow(row);
                        }
                    }
                }
            }
        }
    }
}

