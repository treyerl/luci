﻿
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace CollabSample.Communication
{

    public class Communication2Lucy
    {
        public StreamReader OutputStream { get; private set; }
        private BinaryWriter InputStream;
     //   private LineReader OutputStream;

        public Communication2Lucy()
        {
        }


        public bool connect(String host, int port)
        {
            TcpClient socketForServer;
            try
            {
                socketForServer = new TcpClient(host, port);
            }
            catch
            {
                Console.WriteLine(
                "Failed to connect to server at {0}:{1}", host, port);
                return false;
            }

            NetworkStream networkStream = socketForServer.GetStream();
//            System.IO.StreamReader streamReader =
//            new System.IO.StreamReader(networkStream);
//            System.IO.StreamWriter streamWriter =
//            new System.IO.StreamWriter(networkStream);
            Console.WriteLine("******* Trying to communicate with lucy on host " + host + " and port " + port + " *****");

            OutputStream = new StreamReader(networkStream);
            InputStream = new BinaryWriter(networkStream, System.Text.Encoding.UTF8);
        //    OutputStream = new LineReader(networkStream, System.Text.Encoding.UTF8);

            return true;
            try
            {
                string outputString;
                // read the data from the host and display it
                {
//                    streamWriter.WriteLine("{'action':'authenticate','username':'lukas','userpasswd':'1234'}");
//                    streamWriter.Flush();

                    InputStream.Write("{'action':'authenticate','username':'lukas','userpasswd':'1234'}\n");
                    InputStream.Flush();


                    Console.WriteLine("type:");
                    string str = Console.ReadLine();
                    str = "exit";
                    while (str != "exit")
                    {
//                        streamWriter.WriteLine(str);
//                        streamWriter.Flush();
                        InputStream.Write(str + "\n");
                        InputStream.Flush();
                        
                        Console.WriteLine("type:");
                        str = Console.ReadLine();
                    }
                    if (str == "exit")
                    {
//                        streamWriter.WriteLine(str);
//                        streamWriter.Flush();
                        InputStream.Write(str + "\n");
                        InputStream.Flush();

                    }

                }
            }
            catch
            {
                Console.WriteLine("Exception reading from Server");
            }
            // tidy up
            networkStream.Close();
        }

        public JObject sendAction2Lucy(string action)
        {
            InputStream.Write(Encoding.UTF8.GetBytes(action + "\n"));
            InputStream.Flush();

            string json = OutputStream.ReadLine();
            var jObj2 = (JObject)JsonConvert.DeserializeObject(json);

            return jObj2;
        }

        public JObject sendAction2Lucy(string action, byte[] byteStream)
        {
            if (byteStream == null)
            {
                InputStream.Write(Encoding.UTF8.GetBytes(action + "\n"));
            }
            else
            {
                InputStream.Write(Encoding.UTF8.GetBytes(action + "\n"));
                InputStream.Flush();

                if (byteStream != null)
                    InputStream.Write(byteStream);
            }

            InputStream.Flush();

            string json = OutputStream.ReadLine();
            var jObj2 = (JObject)JsonConvert.DeserializeObject(json);

            return jObj2;
        }

        public JObject sendAction2Lucy(string action, byte[] byteStream, byte[] byteStream2)
        {
            InputStream.Write(Encoding.UTF8.GetBytes(action + "\n"));
            InputStream.Flush();

            long len1 = byteStream.Length;
            byte[] arr1 = BitConverter.GetBytes(len1).Reverse().ToArray(); ;
            InputStream.Write(arr1);
            InputStream.Flush();
            InputStream.Write(byteStream);
            InputStream.Flush();

            long len2 = byteStream2.Length;
            byte[] arr2 = BitConverter.GetBytes(len2).Reverse().ToArray(); ;
            InputStream.Write(arr2);
            InputStream.Write(byteStream2);
            InputStream.Flush();

            string json = OutputStream.ReadLine();
            var jObj2 = (JObject)JsonConvert.DeserializeObject(json);

            return jObj2;
        }


        public string sendCreateService2Lucy(string command, int scenarioID, out int sobjID)
        {
            InputStream.Write(Encoding.UTF8.GetBytes(command + "\n"));
            InputStream.Flush();

            string json = OutputStream.ReadLine();
            var jObj2 = (JObject)JsonConvert.DeserializeObject(json);

            sobjID = jObj2.Value<JObject>("result").Value<int>("SObjID");
            string mqtt_topic = jObj2.Value<JObject>("result").Value<String>("mqtt_topic");
            mqtt_topic = mqtt_topic.Substring(0, mqtt_topic.Length - 1);

            return mqtt_topic;
        }

        public JObject sendService2Lucy(string command)
        {
            InputStream.Write(Encoding.UTF8.GetBytes(command + "\n"));
            InputStream.Flush();

            string json = OutputStream.ReadLine();
            var jObj2 = (JObject)JsonConvert.DeserializeObject(json);

            if (jObj2.Value<JToken>("result") != null)
            {
                if (((Newtonsoft.Json.Linq.JProperty)(jObj2.Value<JToken>("result").First())).Name.Equals("mqtt_topic"))
                {
                    json = OutputStream.ReadLine();
                    jObj2 = (JObject)JsonConvert.DeserializeObject(json);
                }

                if (jObj2.Value<JToken>("result") != null) 
                    return ((Newtonsoft.Json.Linq.JObject)(jObj2.Value<JToken>("result")));
            }
            return null;
        }

        public JProperty authenticate(string user, string password)
        {
            return (JProperty)sendAction2Lucy("{'action':'authenticate','username':'" + user + "','userpasswd':'" + password + "'}").First;
        }

        public JProperty getActionsList()
        {
            return (JProperty)sendAction2Lucy("{'action':'get_list','of':['actions']}").First;
        }

        public JProperty getActionParameters(string action)
        {
            return (JProperty)sendAction2Lucy("{'action':'get_infos_about','actionname':'" + action + "'}").First;
        }

        public JProperty getServiceParameters(string service)
        {
            return (JProperty)sendAction2Lucy("{'action':'get_infos_about','servicename':'" + service + "'}").First;
        }

        public JProperty getServicesList()
        {
            return (JProperty)sendAction2Lucy("{'action':'get_list','of':['services']}").First;
        }

        public JObject receiveMsg()
        {
            string json = OutputStream.ReadLine();
            var jObj2 = (JObject)JsonConvert.DeserializeObject(json);

            return ((Newtonsoft.Json.Linq.JObject)(jObj2));
        }


        private static string GetData()
        {
            //Ack from sql server
            return "ack";
        }
    }
}
