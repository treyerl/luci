﻿using CollabSample.Communication;
using NetTopologySuite.Features;
using NetTopologySuite.Geometries;
using NetTopologySuite.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HeatEmissionService
{
    class Program
    {
        static void Main(string[] args)
        {
            Communication2Lucy cl = new Communication2Lucy();

            string result = "Connection to Lucy was ";
            string host = "localhost";
            if (args.Count() > 0)
                host = args[0];
//            host = "129.132.6.35";

//          host = "129.132.6.36";

        //     if (!cl.connect("localhost", 7654))
           if (!cl.connect(host, 7654))
                result += "not ";

            result += "successful! \n";

            Console.Out.WriteLine(result);

            Console.Out.WriteLine("-> authenticate\n");
            JProperty result2 = cl.authenticate("lukas", "1234");
            result = result2.Name + ": " + result2.Value + "\n";
            Console.Out.WriteLine(result);

            Console.Out.WriteLine("-> registering service\n");
            string actionRegister = "{'action':'remote_register', 'service':{ 'classname':'HeatEmission', 'version':'0.1', 'description':'Simple Demo for Calculation of Heat Emisson of Buildings', 'inputs':{ 'inputNumber':'number'}, 'outputs':{'outputVals':'string'}}, 'machinename':'Rennsemmel' }";
            JProperty result3 = (JProperty) cl.sendAction2Lucy(actionRegister).First;
            //"{'action':'remote_register', 'service':{ 'classname':'timesTwo', 'version':'0.1', 'description':'Multiplies a number by two', 'inputs':{ 'inputNumber':'number'}, 'outputs':{'outputNumber':'number' }, }, 'machinename':'danisLaptop' }\n"
            result = result3.Name + ": " + result3.Value + "\n";
            Console.Out.WriteLine(result);

            /* 
            bool test = true;

            while (test)
            {
                int testID = 3;
                    // scenario abfragen
                    // {'action':'get_inputs_of','ScID':'36'}
                   // JObject scenario = cl.sendAction2Lucy("{'action':'get_inputs_of','ScID':'" + scID + "'}");
                    JObject scenario = cl.sendAction2Lucy("{'action':'get_inputs_of','ScID':'" + testID + "'}");

                    if (scenario != null)
                    {
                        JObject geom = (JObject)scenario.Value<JToken>("inputs").Value<JObject>("geometry");

                        JObject scInput = (JObject)scenario.Value<JToken>("inputs").Value<JObject>("GeoJSON");
                        var sjson = JsonConvert.DeserializeObject(scInput.ToString());
                        //string strjson = "{'type': 'FeatureCollection'," + sjson.ToString() + "}";

                        var strjson = ((JToken)(scenario.Value<JToken>("inputs").Value<JObject>("GeoJSON").Value<JToken>("features")).First).Value<JObject>("geometry");

                        string sss = "{" + strjson.Value<JToken>("type").Parent.ToString() + "," + strjson.Value<JToken>("coordinates").Parent.ToString() + "}";
                        
                     //   string sss = strjson.ToString();
                        sss = sss.Replace("\n", "");
                        sss = sss.Replace("\r", "");
                        sss = sss.Replace(" ", "");
                        sss = sss.Replace("MULTIPOLYGON", "MultiPolygon");

                        string completeString = "";

                        int endS = 0;
                        int index = sss.IndexOfAny("0123456789".ToCharArray());
                        while (index > 0)
                        {
                            completeString += sss.Substring(endS, index - endS);
                            endS = sss.IndexOfAny("[],".ToCharArray(), index);
                            if (endS > 0)
                            {
                                string part = sss.Substring(index, endS - index);
                                if (!part.Contains("."))
                                {
                                    int val = 0;
                                    if (Int32.TryParse(part, out val))
                                    {
                                        part = val + ".0";
                                    }
                                }
                                completeString += part;

                                index = sss.IndexOfAny("0123456789".ToCharArray(), endS);
                            }
                        }
                        if (endS < sss.Length)
                        {
                            completeString += sss.Substring(endS, sss.Length - endS);
                        }

                        
            
                        GeoJsonReader reader = new GeoJsonReader();
                        MultiPolygon ccc = reader.Read<MultiPolygon>(completeString);
                        int numPoints = ccc.NumPoints;
     //                   Feature fc = reader.Read<Feature>(sjson.ToString());
       //                 Feature fc2 = reader.Read<Feature>(scInput.ToString());

                       // JArray array = (JArray)(((JProperty)(geom.First.First.First.Last)).Value.Last.First);
                    }

            }
            */
            while (true)
            {
                JObject resultRecv = cl.receiveMsg();

//                JToken action;
//                JToken inputs;
//                JToken timestamp;
//                JToken SObjID;

//                width = jToken.Value<double?>("width") ?? 100;

                string action = resultRecv.Value<string>("action");
                string timestamp = resultRecv.Value<string>("timestamp");
                string SObjID = resultRecv.Value<string>("SObjID");
                string hashCodeOfInputs = resultRecv.Value<string>("input_hashcode");
                int scID = resultRecv.Value<int>("ScID");
                JToken inputs = resultRecv.Value<JToken>("inputs");

                /*
                foreach (KeyValuePair<string, JToken> prop in resultRecv) {
                    if (prop.Key.Equals("action"))
                        action = prop.Value;

                    if (prop.Key.Equals("inputs"))
                        inputs = prop.Value;

                    if (prop.Key.Equals("timestamp"))
                        timestamp = prop.Value;

                    if (prop.Key.Equals("SObjID"))
                        SObjID = prop.Value;
                }*/

                if (action != null && (action as string).Equals("run") && scID >= 0)
                {
                    double input = 0;

                    // prepare input values
                    if (inputs.HasValues)
                    {
                        input = inputs.Value<double>("inputNumber");
                    }

                    int[][] calcData = new int[85][];
                    int[][] buildingData = new int[56][];

                    // scenario abfragen
                    // {'action':'get_inputs_of','ScID':'36'}
                   // JObject scenario = cl.sendAction2Lucy("{'action':'get_inputs_of','ScID':'" + scID + "'}");
                    JObject scenario = cl.sendAction2Lucy("{'action':'get_scenario','ScID':" + scID + "}");

                    if (scenario != null)
                    {
                        /* alte version
                        JObject geom = (JObject)scenario.Value<JToken>("inputs").Value<JObject>("geometry");

                        JObject scInput = (JObject)scenario.Value<JToken>("inputs").Value<JObject>("GeoJSON");
                        var sjson = JsonConvert.DeserializeObject(scInput.ToString());
                        GeoJsonReader reader = new GeoJsonReader();
                        Feature fc = reader.Read<Feature>(sjson.ToString());
                        Feature fc2 = reader.Read<Feature>(scInput.ToString());

                        JArray array = (JArray)(((JProperty)(geom.First.First.First.Last)).Value.Last.First);
                        */


                        //JObject scInput = (JObject)scenario.Value<JToken>("result").Value<JToken>("GeoJSON");
//                        JObject scInput = (JObject)scenario.Value<JToken>("result").Value<JToken>("geometry").Value<JToken>("GeoJSON").Value<JToken>("value");
//                        var sjson = JsonConvert.DeserializeObject(scInput.ToString());
                        //var strjson = ((JToken)(scenario.Value<JToken>("result").Value<JToken>("GeoJSON").Value<JToken>("features")).First).Value<JObject>("geometry");
                        var strjson = ((JToken)(scenario.Value<JToken>("result").Value<JToken>("geometry").Value<JToken>("GeoJSON").Value<JToken>("geometry").Value<JToken>("features")).First).Value<JObject>("geometry");

//                        var strjson = scenario.Value<JToken>("result").Value<JToken>("geometry").Value<JToken>("test.001").Value<JToken>("value").Value<JToken>("features").First().Value<JObject>("geometry");

                        //string sss = strjson.ToString();
                        string sss = "{" + strjson.Value<JToken>("type").Parent.ToString() + "," + strjson.Value<JToken>("coordinates").Parent.ToString() + "}";                        
                        sss = sss.Replace("\n", "");
                        sss = sss.Replace("\r", "");
                        sss = sss.Replace(" ", "");
                        sss = sss.Replace("MULTIPOLYGON", "MultiPolygon");

                        string completeString = "";

                        int endS = 0;
                        int index = sss.IndexOfAny("0123456789".ToCharArray());
                        while (index > 0)
                        {
                            completeString += sss.Substring(endS, index - endS);
                            endS = sss.IndexOfAny("[],".ToCharArray(), index);
                            if (endS > 0)
                            {
                                string part = sss.Substring(index, endS - index);
                                if (!part.Contains("."))
                                {
                                    int val = 0;
                                    if (Int32.TryParse(part, out val))
                                    {
                                        part = val + ".0";
                                    }
                                }
                                completeString += part;

                                index = sss.IndexOfAny("0123456789".ToCharArray(), endS);
                            }
                        }
                        if (endS < sss.Length)
                        {
                            completeString += sss.Substring(endS, sss.Length - endS);
                        }

            
                        GeoJsonReader reader = new GeoJsonReader();
                        MultiPolygon ccc = reader.Read<MultiPolygon>(completeString);



                        // 500 * 800                        
                        for (int bd = 0; bd< 56; bd++)
                            buildingData[bd] = new int[4];

                        for (int c = 0; c < 85; c++)
                        {
                            calcData[c] = new int[55];

                            for (int r = 0; r < 55; r++)
                            {
                                calcData[c][r] = 0;
                            }
                        }

                        /*alte Version
                                                int numPoly = array.Count / 5;
                        for (int i = 0; i < numPoly; i++)
                        {
                            int y1 = (int)array[i * 5][0] / 10;
                            int x1 = (int)array[i * 5][1] / 10;
                            int y2 = (int)array[i * 5 + 1][0] / 10;
                            int x2 = (int)array[i * 5 + 1][1] / 10;
                            int y3 = (int)array[i * 5 + 2][0] / 10;
                            int x3 = (int)array[i * 5 + 2][1] / 10;
                            int y4 = (int)array[i * 5 + 3][0] / 10;
                            int x4 = (int)array[i * 5 + 3][1] / 10;
                            int y5 = (int)array[i * 5 + 4][0] / 10;
                            int x5 = (int)array[i * 5 + 4][1] / 10;
*/
                        int numPoly = ccc.Count;
                        for (int i = 0; i < numPoly; i++)
                        {
                            int x1 = (int)ccc[i].Coordinates[0].Y / 10;
                            int y1 = (int)ccc[i].Coordinates[0].X / 10;
                            int x2 = (int)ccc[i].Coordinates[1].Y / 10;
                            int y2 = (int)ccc[i].Coordinates[1].X / 10;
                            int x3 = (int)ccc[i].Coordinates[2].Y / 10;
                            int y3 = (int)ccc[i].Coordinates[2].X / 10;
                            int x4 = (int)ccc[i].Coordinates[3].Y / 10;
                            int y4 = (int)ccc[i].Coordinates[3].X / 10;
                            int x5 = (int)ccc[i].Coordinates[4].Y / 10;
                            int y5 = (int)ccc[i].Coordinates[4].X / 10;

                            buildingData[i][0] = x1;
                            buildingData[i][1] = x3;
                            buildingData[i][2] = y1;
                            buildingData[i][3] = y3;

                            if (x1 == x5 && y1 == y5)
                            {
                                for (int a = x1; a < x2; a++)
                                    for (int z = y3; z < y1; z++)
                                        calcData[a][z] = -1;

                                for (int ic = x1 - 1; ic < x2 + 1; ic++)
                                {
                                    if (ic >= 0)
                                    {
                                        if (y1 > 0)
                                            calcData[ic][y1] += 6;

                                        if (y3 > 0)
                                            calcData[ic][y3 - 1] += 6;
                                    }
                                }

                                for (int ir = y3; ir < y1; ir++)
                                {
                                    if (x1 > 0)
                                        calcData[x1 - 1][ir] += 6;

                                    if (x2 > 0)
                                        calcData[x2][ir] += 6;
                                }
                            }
                        }

                        for (int b = 0; b < 56; b++)
                        {
                            int left = buildingData[b][0];
                            int right = buildingData[b][1];
                            int bottom = buildingData[b][2];
                            int top = buildingData[b][3];

                            for (int bc = left - 2; bc < right + 2; bc++)
                            {
                                if (bc >= 0)
                                {
                                    if (bottom >= 0)
                                    {
                                        if (calcData[bc][bottom + 1] == -1)
                                        {
                                            if (calcData[bc][bottom] != -1)
                                            {
                                                calcData[bc][bottom] += 3;
                                            }
                                        }
                                        else
                                        {
                                            calcData[bc][bottom + 1] += 3;
                                        }
                                    }

                                    if (top >= 2)
                                    {
                                        if (calcData[bc][top - 2] == -1)
                                        {
                                            if (calcData[bc][top - 1] != -1)
                                                calcData[bc][top - 1] += 3;
                                        }
                                        else
                                        {
                                            calcData[bc][top - 2] += 3;
                                        }
                                    }
                                }
                            }
                            
                            for (int br = top-1; br <= bottom; br++)
                            {
                                if (left > 1 && br >= 0)
                                {
                                    if (calcData[left - 2][br] == -1)
                                    {
                                        if (calcData[left - 1][br] != -1)
                                            calcData[left - 1][br] += 3;
                                    }
                                    else
                                    {
                                        calcData[left - 2][br] += 3;
                                    }
                                }


                                if (right > 0 && br >= 0)
                                {
                                    if (calcData[right + 1][br] == -1)
                                    {
                                        if (calcData[right][br] != -1)
                                            calcData[right][br] += 3;
                                    }
                                    else
                                    {
                                        calcData[right + 1][br] += 3;
                                    }
                                }
                            }
                        }
                    }

                    /*
                    if (scenario != null)
                    {
                        GeoJsonReader GeoJSONReader = new GeoJsonReader();

                        JToken scInputs = scenario.Value<JToken>("inputs");
                        string geoJSON = scenario.Value<string>("GeoJSON");
                        string geometry = scenario.Value<string>("geometry");

                        JObject geom = (JObject) scenario.Value<JToken>("inputs").Value<JObject>("geometry");
                        var jObj2 = (JObject)JsonConvert.DeserializeObject(scenario.Value<JToken>("inputs").ToString());
                       // string geometry2 = jObj2.Value<string>("geometry");

                        //string geo = geom.Value<string>("GeoJSON");

                       
                        FeatureCollection fc = GeoJSONReader.Read<FeatureCollection>(geom.ToString());
                        //MultiPolygon geoJSONPoly = GeoJSONReader.Read<MultiPolygon>(geo);


                        //string geoJSONPoly = GeoJSONReader.Read(mPoly);
                        //MultiPolygon mPoly = new MultiPolygon(polyArray);
                    }*/

                    // calculate results
                    //double output = input * 3;

                    double[][] outputData = new double[4][];
                    for (int i = 0; i < 4; i++)
                    {
                        outputData[i] = new double[5];
                        for (int j = 0; j < 5; j++)
                        {
                            outputData[i][j] = i * j;
                        }
                    }
                    string json1 = JsonConvert.SerializeObject(outputData);
                    
                    string json = JsonConvert.SerializeObject(calcData);

                    byte[] outputArr = new byte[] {13,14,15,16,17,18,19,20,21,22,23,24};
                    byte[] buffer = new byte[4];
                    buffer = Encoding.ASCII.GetBytes(outputArr.Length.ToString());
                    string outputLen = System.Text.Encoding.UTF8.GetString(buffer);
                    string output = System.Text.Encoding.UTF8.GetString(outputArr);

                    // send output
                    string outputMessage = "{'result':'outputs', 'timestamp':'" + timestamp + "', 'SObjID':" + SObjID + ", 'serviceVersion':'0.1', 'machinename':'Rennsemmel', 'input_hashcode':" + hashCodeOfInputs + ", 'outputs':{'outputVals':{'format':'text','value':'" + json + "'}}}\n";
                    
                    //{'result':'outputs', 'timestamp':'1393861447786', 'SObjID':42, 'version':'0.1', 'machinename':'roemroem','outputs':{'outputVals':[[0.0,0.0,0.0,0.0,0.0],[0.0,1.0,2.0,3.0,4.0],[0.0,2.0,4.0,6.0,8.0],[0.0,3.0,6.0,9.0,12.0]]}}
                    JProperty confirmationInfo = (JProperty) cl.sendAction2Lucy(outputMessage).First;


                    // read confirmation and write it to console
                    Console.Out.WriteLine("confirmation: " + confirmationInfo.Name + ": " + confirmationInfo.Value);

//                    Console.Out.WriteLine(action + " - " + inputs + " - " + SObjID + " - " + timestamp);

                    //Console.Out.WriteLine(resultRecv.Name + ": " + resultRecv.Value + "\n");
                }
/*                string str = Console.ReadLine();

                if (str == "stop")
                    break;*/
            }
        }
    }
}
