using CollabSample.Communication;
using CPlan.Isovist2D;
using GeoAPI.Geometries;
using NetTopologySuite.Features;
using NetTopologySuite.Geometries;
using NetTopologySuite.IO;
using NetTopologySuite.IO.Converters;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IsovistService
{
    class Program
    {
        static void Main(string[] args)
        {
            GeoJsonWriter GeoJSONWriter = new GeoJsonWriter();
            Communication2Lucy cl = new Communication2Lucy();

            string result = "Connection to Lucy was ";
            string host = "localhost";

            if (args.Count() > 0)
                host = args[0];

//            host = "129.132.6.36";
            //if (!cl.connect("localhost", 7654))
            if (!cl.connect(host, 7654))
                result += "not ";

            result += "successful! \n";

            Console.Out.WriteLine(result);

            Console.Out.WriteLine("-> authenticate\n");
            JProperty result2 = cl.authenticate("lukas", "1234");
            result = result2.Name + ": " + result2.Value + "\n";
            Console.Out.WriteLine(result);

            Console.Out.WriteLine("-> registering service\n");
            string actionRegister = "{'action':'remote_register', 'service':{ 'classname':'IsovistDemo', 'version':'0.1', 'description':'Simple Demo for Isovist', 'inputs':{ 'cols':'number', 'rows':'number', 'inputVals':'list'}, 'outputs':{'outputVals':'string'}}, 'machinename':'Rennsemmel' }";
            JProperty result3 = (JProperty)cl.sendAction2Lucy(actionRegister).First;
            //"{'action':'remote_register', 'service':{ 'classname':'timesTwo', 'version':'0.1', 'description':'Multiplies a number by two', 'inputs':{ 'inputNumber':'number'}, 'outputs':{'outputNumber':'number' }, }, 'machinename':'danisLaptop' }\n"
            result = result3.Name + ": " + result3.Value + "\n";
            Console.Out.WriteLine(result);

            while (true)
            {
                JObject resultRecv = cl.receiveMsg();

                string action = resultRecv.Value<string>("action");
                string timestamp = resultRecv.Value<string>("timestamp");
                string SObjID = resultRecv.Value<string>("SObjID");
                string hashCodeOfInputs = resultRecv.Value<string>("input_hashcode");
                int scID = resultRecv.Value<int>("ScID");
                JToken inputs = resultRecv.Value<JToken>("inputs");

                if (action != null && (action as string).Equals("run") && scID >= 0)
                {
                    int numRows = 0, numCols = 0;
                    //double[][] calcPoints = new double[2][];
                    JArray calcPoints;
                    List<CPlan.Geometry.Vector2D> points = new List<CPlan.Geometry.Vector2D>();

                    // prepare input values
                    if (inputs != null && inputs.HasValues)
                    {
                        numRows = inputs.Value<int>("rows");
                        numCols = inputs.Value<int>("cols");
                        //string inputVals = inputs.Value<string>("inputVals");
                        calcPoints = inputs.Value<JArray>("inputVals");
//                        string input = (string)((Newtonsoft.Json.Linq.JProperty)(inputs.First())).Value;
                      //  calcPoints = JsonConvert.DeserializeObject<double[][]>(inputVals);

                        int numP = calcPoints[0].Count();

                        for (int j = 0; j < numP; j++)
                        {
                            points.Add(new CPlan.Geometry.Vector2D((double)calcPoints[0][j], (double)calcPoints[1][j]));
                        }
                    }

                    List<CPlan.Geometry.Line2D> lines = new List<CPlan.Geometry.Line2D>();
                    /*                    int[][] calcData = new int[85][];
                                        int[][] buildingData = new int[56][];
                    */
                    // scenario abfragen
                    // {'action':'get_inputs_of','ScID':'36'}
                    JObject scenario = cl.sendAction2Lucy("{'action':'get_scenario','ScID':" + scID + "}");

                    if (scenario != null)
                    {
                        //JObject scInput = (JObject)scenario.Value<JToken>("result").Value<JToken>("GeoJSON");
//                        JObject scInput = (JObject)scenario.Value<JToken>("result").Value<JToken>("geometry").Value<JToken>("GeoJSON").Value<JToken>("value");
                        //var sjson = JsonConvert.DeserializeObject(scInput.ToString());
//                        var strjson = ((JToken)(scenario.Value<JToken>("result").Value<JToken>("geometry").Value<JToken>("GeoJSON").Value<JToken>("value").Value<JToken>("features")).First).Value<JObject>("geometry");
                        //var strjson = ((JToken)(scInput.Value<JToken>("features")).First).Value<JObject>("geometry");
                       
                        JObject scInput = (JObject)scenario.Value<JToken>("result").Value<JToken>("geometry").Value<JToken>("GeoJSON").Value<JToken>("geometry");
                        var sjson = JsonConvert.DeserializeObject(scInput.ToString());
                        //int numPoly = scInput.Value<JToken>("features").Count();
                        var strjson = ((JToken)(scInput.Value<JToken>("features"))[0]).Value<JObject>("geometry");


                        string sss = "{" + strjson.Value<JToken>("type").Parent.ToString() + "," + strjson.Value<JToken>("coordinates").Parent.ToString() + "}";
                        sss = sss.Replace("\n", "");
                        sss = sss.Replace("\r", "");
                        sss = sss.Replace(" ", "");
                        sss = sss.Replace("MULTIPOLYGON", "MultiPolygon");

                        string completeString = "";

                        int endS = 0;
                        int index = sss.IndexOfAny("0123456789".ToCharArray());
                        while (index > 0)
                        {
                            completeString += sss.Substring(endS, index - endS);
                            endS = sss.IndexOfAny("[],".ToCharArray(), index);
                            if (endS > 0)
                            {
                                string part = sss.Substring(index, endS - index);
                                if (!part.Contains("."))
                                {
                                    int val = 0;
                                    if (Int32.TryParse(part, out val))
                                    {
                                        part = val + ".0";
                                    }
                                }
                                completeString += part;

                                index = sss.IndexOfAny("0123456789".ToCharArray(), endS);
                            }
                        }
                        if (endS < sss.Length)
                        {
                            completeString += sss.Substring(endS, sss.Length - endS);
                        }


                        GeoJsonReader reader = new GeoJsonReader();
                        MultiPolygon ccc = reader.Read<MultiPolygon>(completeString);


                        
                        lines.Add(new CPlan.Geometry.Line2D(new CPlan.Geometry.Vector2D(-1, -1), new CPlan.Geometry.Vector2D(-1, numRows + 1)));
                        lines.Add(new CPlan.Geometry.Line2D(new CPlan.Geometry.Vector2D(-1, numRows + 1), new CPlan.Geometry.Vector2D(numCols + 1, numRows + 1)));
                        lines.Add(new CPlan.Geometry.Line2D(new CPlan.Geometry.Vector2D(-1, -1), new CPlan.Geometry.Vector2D(numCols + 1, -1)));
                        lines.Add(new CPlan.Geometry.Line2D(new CPlan.Geometry.Vector2D(numCols + 1, -1), new CPlan.Geometry.Vector2D(numCols + 1, numRows + 1)));



                        // 500 * 800      
                        /*
                                                for (int bd = 0; bd< 56; bd++)
                                                    buildingData[bd] = new int[4];

                                                for (int c = 0; c < 85; c++)
                                                {
                                                    calcData[c] = new int[55];

                                                    for (int r = 0; r < 55; r++)
                                                    {
                                                        calcData[c][r] = 0;
                                                    }
                                                }
                        */
                        int numPoly = ccc.Count;
                        for (int i = 0; i < numPoly; i++)
                        {
                            //lines.Add(new CPlan.Geometry.Line2D(new CPlan.Geometry.Vector2D(2, 5), new CPlan.Geometry.Vector2D(8, 5)));

                            int numCoordinates = ccc[i].Coordinates.Count();

                            for (int c = 0; c < numCoordinates - 1; c++)
                            {
                                Coordinate coord = ccc[i].Coordinates[c];
                                Coordinate coordNext = ccc[i].Coordinates[c + 1];

                                lines.Add(new CPlan.Geometry.Line2D(new CPlan.Geometry.Vector2D(coord.Y / 10, coord.X / 10),
                                          new CPlan.Geometry.Vector2D(coordNext.Y / 10, coordNext.X / 10)));
//xx                                lines.Add(new CPlan.Geometry.Line2D(new CPlan.Geometry.Vector2D(coord.Y, coord.X),
//xx                                          new CPlan.Geometry.Vector2D(coordNext.Y, coordNext.X)));
                            }

                            Coordinate coordLast = ccc[i].Coordinates[numCoordinates - 1];
                            Coordinate coordFirst = ccc[i].Coordinates[0];

                           lines.Add(new CPlan.Geometry.Line2D(new CPlan.Geometry.Vector2D(coordLast.Y / 10, coordLast.X / 10),
                                      new CPlan.Geometry.Vector2D(coordFirst.Y / 10, coordFirst.X / 10)));
//xx                             lines.Add(new CPlan.Geometry.Line2D(new CPlan.Geometry.Vector2D(coordLast.Y, coordLast.X),
//xx                                      new CPlan.Geometry.Vector2D(coordFirst.Y, coordFirst.X)));

                            /*
                            int x1 = (int)ccc[i].Coordinates[0].Y / 10;
                            int y1 = (int)ccc[i].Coordinates[0].X / 10;
                            int x2 = (int)ccc[i].Coordinates[1].Y / 10;
                            int y2 = (int)ccc[i].Coordinates[1].X / 10;
                            int x3 = (int)ccc[i].Coordinates[2].Y / 10;
                            int y3 = (int)ccc[i].Coordinates[2].X / 10;
                            int x4 = (int)ccc[i].Coordinates[3].Y / 10;
                            int y4 = (int)ccc[i].Coordinates[3].X / 10;
                            int x5 = (int)ccc[i].Coordinates[4].Y / 10;
                            int y5 = (int)ccc[i].Coordinates[4].X / 10;

                            lines.Add(new CPlan.Geometry.Line2D(new CPlan.Geometry.Vector2D(x1, y1), new CPlan.Geometry.Vector2D(x2, y2)));
                            lines.Add(new CPlan.Geometry.Line2D(new CPlan.Geometry.Vector2D(x2, y2), new CPlan.Geometry.Vector2D(x3, y3)));
                            lines.Add(new CPlan.Geometry.Line2D(new CPlan.Geometry.Vector2D(x3, y3), new CPlan.Geometry.Vector2D(x4, y4)));
                            lines.Add(new CPlan.Geometry.Line2D(new CPlan.Geometry.Vector2D(x4, y4), new CPlan.Geometry.Vector2D(x1, y1)));*/
                        }

   

                        /*
                                                                    double[][] outputData = new double[4][];
                                                                    for (int i = 0; i < 4; i++)
                                                                    {
                                                                        outputData[i] = new double[5];
                                                                        for (int j = 0; j < 5; j++)
                                                                        {
                                                                            outputData[i][j] = i * j;
                                                                        }
                                                                    }
                                                                    string json1 = JsonConvert.SerializeObject(outputData);
                    
                                                                    string json = JsonConvert.SerializeObject(calcData);

                                                                    byte[] outputArr = new byte[] {13,14,15,16,17,18,19,20,21,22,23,24};
                                                                    byte[] buffer = new byte[4];
                                                                    buffer = Encoding.ASCII.GetBytes(outputArr.Length.ToString());
                                                                    string outputLen = System.Text.Encoding.UTF8.GetString(buffer);
                                                                    string output = System.Text.Encoding.UTF8.GetString(outputArr);
                          */
                    }

                    //create sample iso object on client
                    IsovistField2D iso_client_orig = new IsovistField2D(points, lines);

                    //pass it to the wrapper and serialize to JSON
                    IsovistField2DWrapper wrapper = new IsovistField2DWrapper(iso_client_orig);

                    //perform actual calculations, the precision need to be set explicitly here since we can't do it using isovist constructor
                    iso_client_orig.Calculate(wrapper.Precision, true);

                    //wrap the results
                    IsovistField2DWrapper iso_server_wrapper_result = new IsovistField2DWrapper(iso_client_orig);
                    string iso_server_send = GeoJSONWriter.Write(iso_server_wrapper_result);

                   // string json = JsonConvert.SerializeObject(calcPoints);

                    // send output
                    string outputMessage = "{'result':'outputs', 'timestamp':'" + timestamp + "', 'SObjID':" + SObjID + ", 'serviceVersion':'0.1', 'machinename':'Rennsemmel', 'input_hashcode':" + hashCodeOfInputs + ", 'outputs':{'outputVals':'" + iso_server_send + "'}}\n";

                    //{'result':'outputs', 'timestamp':'1393861447786', 'SObjID':42, 'version':'0.1', 'machinename':'roemroem','outputs':{'outputVals':[[0.0,0.0,0.0,0.0,0.0],[0.0,1.0,2.0,3.0,4.0],[0.0,2.0,4.0,6.0,8.0],[0.0,3.0,6.0,9.0,12.0]]}}
                    JProperty confirmationInfo = (JProperty)cl.sendAction2Lucy(outputMessage).First;


                    // read confirmation and write it to console
                    Console.Out.WriteLine("confirmation: " + confirmationInfo.Name + ": " + confirmationInfo.Value);

                }
            }
        }
    }

    public class GeometryConverter
    {
        public static Point ToPoint(CPlan.Geometry.Vector2D point)
        {
            return new Point(point.X, point.Y);
        }

        public static LineString ToLineString(CPlan.Geometry.Line2D line)
        {
            return new LineString(new Coordinate[] { new Coordinate(line.Start.X, line.Start.Y), new Coordinate(line.End.X, line.End.Y) });
        }

        public static CPlan.Geometry.Vector2D ToVector2D(IPoint point)
        {
            return new CPlan.Geometry.Vector2D(point.X, point.Y);
        }

        public static CPlan.Geometry.Line2D ToLine2D(ILineString line)
        {
            return new CPlan.Geometry.Line2D(line.StartPoint.X, line.StartPoint.Y, line.EndPoint.X, line.EndPoint.Y);
        }

        public static MultiPoint ToMultiPoint(List<CPlan.Geometry.Vector2D> points)
        {
            Point[] tmp = new Point[points.Count];

            for (int i = 0; i < points.Count; i++)
            {
                tmp[i] = ToPoint(points[i]);
            }

            return new MultiPoint(tmp);
        }

        public static List<CPlan.Geometry.Vector2D> ToVector2DList(MultiPoint points)
        {
            List<CPlan.Geometry.Vector2D> result = new List<CPlan.Geometry.Vector2D>();

            foreach (IPoint point in points.Geometries)
            {
                result.Add(ToVector2D(point));
            }

            return result;
        }

        public static MultiLineString ToMultiLineString(List<CPlan.Geometry.Line2D> lines)
        {
            LineString[] tmp = new LineString[lines.Count];

            for (int i = 0; i < lines.Count; i++)
            {
                tmp[i] = ToLineString(lines[i]);
            }

            return new MultiLineString(tmp);
        }

        public static List<CPlan.Geometry.Line2D> ToLine2DList(MultiLineString lines)
        {
            List<CPlan.Geometry.Line2D> result = new List<CPlan.Geometry.Line2D>();

            foreach (ILineString line in lines.Geometries)
            {
                result.Add(ToLine2D(line));
            }

            return result;
        }
    }

    public class IsovistField2DWrapper
    {
        private MultiPoint m_points;
        private MultiLineString m_obstacles;
        private float m_precision;
        private List<Dictionary<ResultsIsovist, double>> m_results;

        public List<Dictionary<ResultsIsovist, double>> Results
        {
            get { return m_results; }
            set { m_results = value; }
        }

        public MultiLineString Obstacles
        {
            get { return m_obstacles; }
            set { m_obstacles = value; }
        }

        public float Precision
        {
            get { return m_precision; }
            set { m_precision = value; }
        }

        //by default all Properties are serialized but can be exluded using following annotation        
        //[Newtonsoft.Json.JsonIgnore]
        public MultiPoint Points
        {
            get { return m_points; }
            set { m_points = value; }
        }

        [Newtonsoft.Json.JsonConstructor]
        public IsovistField2DWrapper(MultiPoint points, MultiLineString obstacles, List<Dictionary<ResultsIsovist, double>> results, float precision)
        {
            m_points = points;
            m_precision = precision;
            m_obstacles = obstacles;
            m_results = results;
        }

        public IsovistField2DWrapper(IsovistField2D isovist) : this(GeometryConverter.ToMultiPoint(isovist.Points), GeometryConverter.ToMultiLineString(isovist.ObstacleLines), isovist.Results, isovist.Precision) { }

        public IsovistField2D ToIsovistField2D()
        {
            return new IsovistField2D(GeometryConverter.ToVector2DList(Points), GeometryConverter.ToLine2DList(Obstacles));
        }
    }
}
