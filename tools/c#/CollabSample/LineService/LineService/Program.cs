﻿using CollabSample.Communication;
using NetTopologySuite.Geometries;
using NetTopologySuite.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LineService
{
    class Program
    {
        static void Main(string[] args)
        {
            GeoJsonWriter GeoJSONWriter = new GeoJsonWriter();
            GeoJsonReader GeoJSONReader = new GeoJsonReader();
            Communication2Lucy cl = new Communication2Lucy();

            string result = "Connection to Lucy was ";
            string host = "localhost";

            if (args.Count() > 0)
                host = args[0];

//            host = "129.132.6.36";

            if (!cl.connect(host, 7654))
                result += "not ";

            result += "successful! \n";

            Console.Out.WriteLine(result);

            Console.Out.WriteLine("-> authenticate\n");
            JProperty result2 = cl.authenticate("lukas", "1234");
            result = result2.Name + ": " + result2.Value + "\n";
            Console.Out.WriteLine(result);

            Console.Out.WriteLine("-> registering service\n");
            string actionRegister = "{'action':'remote_register', 'service':{ 'classname':'LineService', 'version':'0.1', 'description':'Simple Demo for suming up line lengths', 'inputs':{ 'delay':'number'}, 'outputs':{'lengthSum':'number'}}, 'machinename':'Rennsemmel' }";
            JProperty result3 = (JProperty)cl.sendAction2Lucy(actionRegister).First;
            result = result3.Name + ": " + result3.Value + "\n";
            Console.Out.WriteLine(result);

            while (true)
            {
                JObject resultRecv = cl.receiveMsg();

                string action = resultRecv.Value<string>("action");
                string timestamp = resultRecv.Value<string>("timestamp");
                string SObjID = resultRecv.Value<string>("SObjID");
                string hashCodeOfInputs = resultRecv.Value<string>("input_hashcode");
                int scID = resultRecv.Value<int>("ScID");
                JToken inputs = resultRecv.Value<JToken>("inputs");

                int delay = 0;

                if (action != null && (action as string).Equals("run") && scID >= 0)
                {
                
                    // prepare input values
                    if (inputs != null && inputs.HasValues)
                    {
                        delay = inputs.Value<int>("delay") * 1000;
                    }

                    // scenario abfragen
                    JObject scenario = cl.sendAction2Lucy("{'action':'get_scenario','ScID':" + scID + "}");

                    if (scenario != null)
                    {
                        //JObject scInput = (JObject)scenario.Value<JObject>("result").Value<JToken>("GeoJSON");
                        JObject scInput = (JObject)scenario.Value<JToken>("result").Value<JToken>("geometry").Value<JToken>("GeoJSON").Value<JToken>("geometry");
                        var sjson = JsonConvert.DeserializeObject(scInput.ToString());
                        //var strjson = ((JToken)(scenario.Value<JToken>("result").Value<JToken>("geometry").Value<JToken>("value").Value<JToken>("features")).First).Value<JObject>("geometry");
                        var strjson = ((JToken)(scenario.Value<JToken>("result").Value<JToken>("geometry").Value<JToken>("GeoJSON").Value<JToken>("geometry").Value<JToken>("features")).First).Value<JObject>("geometry");
                        //var strjson = ((JToken)(scenario.Value<JObject>("result").Value<JToken>("GeoJSON").Value<JToken>("features")).First).Value<JObject>("geometry");

                        string sss = "{" + strjson.Value<JToken>("type").Parent.ToString() + "," + strjson.Value<JToken>("coordinates").Parent.ToString() + "}";
                        sss = sss.Replace("\n", "");
                        sss = sss.Replace("\r", "");
                        sss = sss.Replace(" ", "");
                        sss = sss.Replace("MULTIPOLYGON", "MultiPolygon");

                        string completeString = "";

                        int endS = 0;
                        int index = sss.IndexOfAny("0123456789".ToCharArray());
                        while (index > 0)
                        {
                            completeString += sss.Substring(endS, index - endS);
                            endS = sss.IndexOfAny("[],".ToCharArray(), index);
                            if (endS > 0)
                            {
                                string part = sss.Substring(index, endS - index);
                                if (!part.Contains("."))
                                {
                                    int val = 0;
                                    if (Int32.TryParse(part, out val))
                                    {
                                        part = val + ".0";
                                    }
                                }
                                completeString += part;

                                index = sss.IndexOfAny("0123456789".ToCharArray(), endS);
                            }
                        }
                        if (endS < sss.Length)
                        {
                            completeString += sss.Substring(endS, sss.Length - endS);
                        }

                        GeoJsonReader reader = new GeoJsonReader();
                        MultiLineString multiLines = reader.Read<MultiLineString>(completeString);

                        int numLines = multiLines.Count;
                        for (int i = 0; i < numLines; i++)
                        {
                            int numCoordinates = multiLines[i].Coordinates.Count();
                            /*
                            for (int c = 0; c < numCoordinates - 1; c++)
                            {
                                Coordinate coord = multiPolys[i].Coordinates[c];
                                Coordinate coordNext = multiPolys[i].Coordinates[c + 1];

                                lines.Add(new CPlan.Geometry.Line2D(new CPlan.Geometry.Vector2D(coord.X, coord.Y),
                                          new CPlan.Geometry.Vector2D(coordNext.X, coordNext.Y)));
                            }*/
                        }
                    }

                    //perform actual calculations, the precision need to be set explicitly here since we can't do it using isovist constructor

                    //wrap the results

                    System.Threading.Thread.Sleep(delay);

                    double lengthSum = 1234.045;

                    // send output
                    string outputMessage = "{'result':'outputs', 'timestamp':'" + timestamp + "', 'SObjID':" + SObjID + ", 'serviceVersion':'0.1', 'machinename':'Rennsemmel', 'input_hashcode':" + hashCodeOfInputs + ", 'outputs':{'lengthSum':" + lengthSum + "}}\n";

                    //{'result':'outputs', 'timestamp':'1393861447786', 'SObjID':42, 'version':'0.1', 'machinename':'roemroem','outputs':{'outputVals':[[0.0,0.0,0.0,0.0,0.0],[0.0,1.0,2.0,3.0,4.0],[0.0,2.0,4.0,6.0,8.0],[0.0,3.0,6.0,9.0,12.0]]}}
                    JProperty confirmationInfo = (JProperty)cl.sendAction2Lucy(outputMessage).First;

                    // read confirmation and write it to console
                    Console.Out.WriteLine("confirmation: " + confirmationInfo.Name + ": " + confirmationInfo.Value);

                }
            }
        }
    }
}
