﻿using CPlan.Isovist2D;
using CPlan.VisualObjects;
using GeoAPI.Geometries;
//using LucyCommunication;
using LucySerializers;
using NetTopologySuite.Geometries;
using Newtonsoft.Json.Linq;
using OpenTK;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using uPLibrary.Networking.M2Mqtt;

namespace CPlanVisualizer.ViewModels
{
    public static class Settings
    {
        public static string LucyHost { get; set; }

        public static int SObjID { get; set; }

        public static int ScenarioID { get; set; }

        public static int Parameter { get; set; }

        public static bool SyncCamera { get; set; }

        public static bool SyncSelection { get; set; }
    }

    public class IsovistVM
    {
        public List<GeoObject> sceneDrawingObjects = new List<GeoObject>();


        public List<Dictionary<CPlan.Isovist2D.ResultsIsovist, double>> Results { get; protected set; }
        public List<double> AreaCleaned { get; set; }
        public List<double> PerimeterCleaned { get; set; }
        public List<double> CompactnessCleaned { get; set; }
        public List<double> OcclusivityCleaned { get; set; }
        public List<double> MinRadialCleaned { get; set; }
        public List<double> MaxRadialCleaned { get; set; }

        // --- Arrays ---
        [Category("Measures"), Description("Area")]
        public double[] Area { get; set; }
        [Category("Measures"), Description("Perimeter")]
        public double[] Perimeter { get; set; }
        [Category("Measures"), Description("Compactness")]
        public double[] Compactness { get; set; }
        [Category("Measures"), Description("Occlusivity")]
        public double[] Occlusivity { get; set; }
        [Category("Measures"), Description("MinRadial")]
        public double[] MinRadial { get; set; }
        [Category("Measures"), Description("MaxRadial")]
        public double[] MaxRadial { get; set; }
        [Category("Measures"), Description("White : no measure")]
        public double[] White { get; set; }

        private List<Vector2d> analysisPoints;
        private List<Polygon> buildings;
        private List<double> buildingHeights;

        private Communication2Lucy cl = new Communication2Lucy();

        private bool _connected2Luci = false;
        private MqttClient mqttClient;

        private string service_topic;
        private string scenario_topic;
        private string service_scenario_topic;

        private GLCameraControl _glCamera;

        //  private int SObjID = 134;
        private int ScID;

        private int _sObjID;
        public int SObjID
        {
            get { return _sObjID; }
            set
            {
                if (value != _sObjID)
                {
                    if (value >= 0)
                    {
//                        hook2SObjID(value);
                    }

                    _sObjID = value;
                }
            }
        }

        public IsovistVM(GLCameraControl glCamera) {
            _glCamera = glCamera;

            //Settings.LucyHost = "localhost";
           // Settings.SObjID = 64; // 47; // 15;
            init();

            double[] par;

            switch (Settings.Parameter)
            {
                case 0: par = Area;
                    break;
                case 1: par = Compactness;
                    break;
                case 2: par = Perimeter;
                    break;
                case 3: par = Occlusivity;
                    break;
                case 4: par = MinRadial;
                    break;
                case 5: par = MaxRadial;
                    break;
                default:
                    par = Area;
                    break;
            }
        }

        private void drawResults() {
            //GGrid _smartGrid = new GGrid(analysisPoints, Area, 10);
            double[] par;

            switch (Settings.Parameter)
            {
                case 0: par = Area;
                    break;
                case 1: par = Compactness;
                    break;
                case 2: par = Perimeter;
                    break;
                case 3: par = Occlusivity;
                    break;
                case 4: par = MinRadial;
                    break;
                case 5: par = MaxRadial;
                    break;
                default:
                    par = Area;
                    break;
            }

            int cellsize = 4;

            if (analysisPoints != null && analysisPoints.Count() > 1)
            {
                int cellsizeX = (int)Math.Abs(analysisPoints[1].X - analysisPoints[0].X);
                int cellsizeY = (int)Math.Abs(analysisPoints[1].Y - analysisPoints[0].Y);
                cellsize = Math.Max(cellsizeX, cellsizeY);
                cellsize = Math.Max(cellsize, 2);
            }

//            GGrid _smartGrid = new GGrid(analysisPoints, par, 10);
            GGrid _smartGrid = new GGrid(analysisPoints, par, cellsize);


            _glCamera.AllObjectsList.ObjectsToDraw.Clear();
            _glCamera.AllObjectsList.ObjectsToDraw = new ObjectsDrawList();
            _glCamera.AllObjectsList.ObjectsToDraw.AddRange(sceneDrawingObjects);
            _glCamera.AllObjectsList.ObjectsToDraw.Add(_smartGrid);


            // scene start

            /*
            glCamera.OpenObjFile("C:\\home\\dev\\eth\\cplan\\Various\\InputDataRochor\\Environment3DBuildings2.obj");

            List<GeoObject> _remainDrawingObjects = new List<GeoObject>();

            CPlan.Geometry.Poly2D Border = new CPlan.Geometry.Poly2D(DxfConversions.LinesFromDxf("C:\\home\\dev\\eth\\cplan\\Various\\InputDataRochor\\Border2.dxf"));

            List<CPlan.Geometry.Line2D> EnvironmentCloseNetwork = DxfConversions.LinesFromDxf("C:\\home\\dev\\eth\\cplan\\Various\\InputDataRochor\\EnvironmentClose2.dxf");

            List<CPlan.Geometry.Line2D> InitialLines = DxfConversions.LinesFromDxf("C:\\home\\dev\\eth\\cplan\\Various\\InputDataRochor\\InitialLines2.dxf");

     //       System.IO.File.cl

            _remainDrawingObjects.Add(new GPolygon(Border));
            foreach (CPlan.Geometry.Line2D line in EnvironmentCloseNetwork)
            {
                _remainDrawingObjects.Add(new GLine(line));
            }
            foreach (CPlan.Geometry.Line2D line in InitialLines)
            {
                GLine iniLine = new GLine(line);
                iniLine.Width = 5;
                iniLine.Color = ConvertColor.CreateColor4(System.Drawing.Color.Red); //RoyalBlue);
                _remainDrawingObjects.Add(iniLine);
            }
            glCamera.AllObjectsList.ObjectsToDraw.AddRange(_remainDrawingObjects);
             */
            // scene end

            if (buildings != null)
            {
                int b = 0;

                foreach (Polygon p in buildings)
                {
                    List<CPlan.Geometry.Line2D> lines = new List<CPlan.Geometry.Line2D>();
                    int numCoordinates = p.NumPoints;

                    Vector2d[] v = new Vector2d[numCoordinates - 1];

                    for (int i = 0; i < numCoordinates - 1; i++)
                    {
                        Coordinate coord = p.Coordinates[i];
                        Coordinate coordNext = p.Coordinates[i + 1];

                        lines.Add(new CPlan.Geometry.Line2D(new Vector2d(coord.X, coord.Y),
                                  new Vector2d(coordNext.X, coordNext.Y)));

                        v[i] = new Vector2d(coord.X, coord.Y);
                    }

                    CPlan.Geometry.Poly2D poly = new CPlan.Geometry.Poly2D(v);
                    var bPoly = new GPrism(v);
                    bPoly.Identity = 32;
                    
                    if (buildingHeights != null && buildingHeights.Count > b)
                        bPoly.Height = buildingHeights[b];
                    else
                        bPoly.Height = 20;

                    bPoly.DeltaZ = 0.1f;
                    bPoly.FillColor = ConvertColor.CreateColor4(System.Drawing.Color.White);
                    bPoly.Filled = true;
                    bPoly.BorderWidth = 5;
                    Vector4 col = ConvertColor.CreateColor4(System.Drawing.Color.Black);
                    col.W = 1f; //0.1f; // alpha value
                    bPoly.BorderColor = col;
                    b++;


                    //       glCamera.AllObjectsList.Add(bPoly);
                    _glCamera.AllObjectsList.ObjectsToDraw.Add(bPoly);
                }
            }

//            glCamera.Refresh();

            _glCamera.ActivateInteraction();
            _glCamera.Invalidate();
  //          glCamera.MakeCurrent();
  //          _glCamera.ZoomAll();
    //        glCamera.Show();


        }


        private void init()
        {
//            GridVM = new RasterGridVM();

            SObjID = Settings.SObjID;

            string result = "Connection to Lucy was ";

            if (!cl.connect(Settings.LucyHost, 7654))
            {
                result += "not ";
                _connected2Luci = false;
            }
            else
                _connected2Luci = true;

            result += "successful! \n";

            Console.Out.WriteLine(result);

            if (_connected2Luci)
            {

                Console.Out.WriteLine("-> authenticate\n");
                JProperty result2 = cl.authenticate("lukas", "1234");
                result = result2.Name + ": " + result2.Value + "\n";
                Console.Out.WriteLine(result);

                Guid newguid = Guid.NewGuid();
                string identifier = newguid.ToString("N");

 //               mqttClient = new MqttClient(Settings.LucyHost);
   //             mqttClient.Connect("CPlanVisualizer_" + identifier);
     //           mqttClient.Subscribe(new string[] { "CameraPos" }, new byte[] { 1 });
 //               mqttClient.MqttMsgPublishReceived += mqttClient_MqttMsgPublishReceived;

//                hook2SObjID(SObjID);

                

//                readIsovistInputs(Settings.SObjID);
//                readLucyResults(Settings.SObjID);
            }

        }

        public void reload(int sobjId, int scenarioID)
        {
            Settings.SObjID = sobjId;
            Settings.ScenarioID = scenarioID;

            readIsovistInputs(Settings.SObjID);
            readLucyResults(Settings.SObjID);

            drawResults();
        }

        void mqttClient_MqttMsgPublishReceived(object sender, uPLibrary.Networking.M2Mqtt.Messages.MqttMsgPublishEventArgs e)
        {
            if (e.Topic == "CameraPos")
            {
                string message = Encoding.UTF8.GetString(e.Message);

                System.IO.MemoryStream ms = new System.IO.MemoryStream(e.Message);
                System.Runtime.Serialization.Formatters.Binary.BinaryFormatter bf = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                object result = bf.Deserialize(ms);
                if (result != null && result is double[])
                {
                    double[] pos = (double[])result;

                    _glCamera.ActiveCamera.Position.x = pos[0];
                    _glCamera.ActiveCamera.Position.y = pos[1];
                    _glCamera.ActiveCamera.Position.z = pos[2];
                    _glCamera.Invalidate();

                }
                    /*
                if (message == "DONE")
                {
                    Console.Out.WriteLine("DOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOONE");

                    System.Windows.Application.Current.Dispatcher.Invoke((Action)(delegate
                    {
                        showResults();
                    }));

                }*/
            }
        }

        private void readIsovistInputs(int instanceID) {
            string msg = "{'action':'get_service_inputs','SObjID':" + instanceID + "}";
            JObject result = cl.sendAction2Lucy(msg);


            if (result != null)
            {
                JToken inputs = result.Value<JToken>("inputs").Value<JToken>("inputVals");

                List<Vector2d> points = new List<Vector2d>();

                // prepare input values
                if (inputs != null && inputs.HasValues)
                {
                    JArray pts = (JArray)result.Value<JToken>("inputs").Value<JToken>("inputVals").Value<JArray>("coordinates");
                    //inputs.Value<JObject>("inputVals").Value<JArray>("coordinates");
                    int numP = pts.Count();
                    for (int j = 0; j < numP; j++)
                    {
                        points.Add(new Vector2d((double)pts[j][0], (double)pts[j][1]));
                    }
                }

                analysisPoints = points;
                
                
                List<CPlan.Geometry.Line2D> lines = new List<CPlan.Geometry.Line2D>();

               // int scID = 71; // 53; // 20;
                // fetch scenario 
                JObject scenario = cl.sendAction2Lucy("{'action':'get_scenario','ScID':" + Settings.ScenarioID + "}");

                
                if (scenario != null)
                {
                    buildings = new List<Polygon>();
                    buildingHeights = new List<double>();

                    LucyGeoJsonSuite.LucyGeoJsonReader reader = new LucyGeoJsonSuite.LucyGeoJsonReader();
                    
                    JObject scInput = (JObject)scenario.Value<JToken>("result").Value<JToken>("geometry").Value<JToken>("GeoJSON").Value<JToken>("geometry");

                    int delta = 1;

                    try
                    {
                        var strjsonHeights = scInput.Value<JToken>("features").Last().Value<JObject>("geometry");
                        MultiPoint heights = reader.Read<MultiPoint>(strjsonHeights.ToString());

                        int numHeights = heights.Count;
                        for (int h = 0; h < numHeights; h++)
                        {
                            buildingHeights.Add(heights[h].Coordinate.Y);
                        }
                        delta = 2;
                    }
                    catch (Exception ex)
                    {
                    }

                    int numPoly = scInput.Value<JToken>("features").Count()-delta;

                    for (int f = 0; f < numPoly; f++)
                    {
                        var strjson = ((JToken)(scInput.Value<JToken>("features"))[f]).Value<JObject>("geometry");

                        //LucyGeoJsonSuite.LucyGeoJsonReader reader = new LucyGeoJsonSuite.LucyGeoJsonReader();
                        Polygon poly = reader.Read<Polygon>(strjson.ToString());                        
                        buildings.Add(poly);
                        

                        /*
                        int numCoordinates = poly.NumPoints;
                        for (int i = 0; i < numCoordinates; i++)
                        {
                            for (int c = 0; c < numCoordinates - 1; c++)
                            {
                                Coordinate coord = poly.Coordinates[c];
                                Coordinate coordNext = poly.Coordinates[c + 1];

                                lines.Add(new CPlan.Geometry.Line2D(new Vector2d(coord.X, coord.Y),
                                          new Vector2d(coordNext.X, coordNext.Y)));
                            }
                        }*/
                    }
                }

                //create sample iso object on client
    //            CPlan.Isovist2D.IsovistField2D iso_client_orig = new CPlan.Isovist2D.IsovistField2D(points, lines);


/*                if (outputs != null)
                {
                    string output = (string)(outputs.Value<JObject>("outputVals").Value<String>("value"));

                    AnalyseLucyResults(output);
                }*/
            }
        }


        private void readLucyResults(int instanceID)
        {
            string msg = "{'action':'get_service_outputs','SObjID':" + instanceID + "}";
            JObject result = cl.sendAction2Lucy(msg);

            if (result != null)
            {
                JToken outputs = result.Value<JToken>("result").Value<JToken>("outputs");
                if (outputs != null)
                {
                    string output = (string)(outputs.Value<JObject>("outputVals").Value<String>("value"));

                    AnalyseLucyResults(output);
                }
            }
        }

        public void AnalyseLucyResults(string output)
        {
            NetTopologySuite.IO.GeoJsonReader GeoJSONReader = new NetTopologySuite.IO.GeoJsonReader();
            LucySerializers.IsovistResultsWrapper iso_results_wrapper = GeoJSONReader.Read<LucySerializers.IsovistResultsWrapper>(output);
            Results = iso_results_wrapper.Results;
            FillArrays();
        }

        /// <summary>
        /// Copy the results into individual arrays.
        /// </summary>
        private void FillArrays()
        {
            AreaCleaned = new List<double>();
            PerimeterCleaned = new List<double>();
            CompactnessCleaned = new List<double>();
            OcclusivityCleaned = new List<double>();
            MinRadialCleaned = new List<double>();
            MaxRadialCleaned = new List<double>();

            Area = new double[Results.Count];
            Compactness = new double[Results.Count];
            Perimeter = new double[Results.Count];
            Occlusivity = new double[Results.Count];
            MinRadial = new double[Results.Count];
            MaxRadial = new double[Results.Count];

            for (int i = 0; i < Results.Count; i++)
            {
                Area[i] = Results[i][ResultsIsovist.Area];
                if (!double.IsNaN(Area[i]) && Area[i] != null && Area[i] > 0) AreaCleaned.Add(Area[i]);
               Compactness[i] = Results[i][ResultsIsovist.Compactness];
                if (!double.IsNaN(Compactness[i]) && Compactness[i] != null && Compactness[i] > 0) CompactnessCleaned.Add(Compactness[i]);
                Perimeter[i] = Results[i][ResultsIsovist.Perimeter];
                if (!double.IsNaN(Perimeter[i]) && Perimeter[i] != null && Perimeter[i] > 0) PerimeterCleaned.Add(Perimeter[i]);
                 Occlusivity[i] = Results[i][ResultsIsovist.Occlusivity];
                if (!double.IsNaN(Occlusivity[i]) && Occlusivity[i] != null && Occlusivity[i] > 0) OcclusivityCleaned.Add(Occlusivity[i]);
                MinRadial[i] = Results[i][ResultsIsovist.MinRadial];
                if (!double.IsNaN(MinRadial[i]) && MinRadial[i] != null && MinRadial[i] > 0) MinRadialCleaned.Add(MinRadial[i]);
                MaxRadial[i] = Results[i][ResultsIsovist.MaxRadial];
                if (!double.IsNaN(MaxRadial[i]) && MaxRadial[i] != null && MaxRadial[i] > 0) MaxRadialCleaned.Add(MaxRadial[i]);
            }

        }
    }
}
