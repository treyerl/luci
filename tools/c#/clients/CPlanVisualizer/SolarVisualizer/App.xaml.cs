﻿using SolarVisualizer.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace SolarVisualizer
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            Settings.LucyHost = "localhost";

            if (e.Args.Count() > 0)
                Settings.LucyHost = e.Args[0];
            else
                Settings.LucyHost = "localhost"; // 

            if (e.Args.Count() > 1)
                Settings.Parameter = Int16.Parse(e.Args[1]);
            else
                Settings.Parameter = 0;

            base.OnStartup(e);
        }
    }
}
