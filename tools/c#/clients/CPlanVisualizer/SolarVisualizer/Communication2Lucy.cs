﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace SolarVisualizer
{
    public class SocketConnection
    {
//        public StreamReader OutputStream { get; private set; }
//        public BinaryWriter InputStream { get; private set; }
        public MixedStream inOutStream;
        public bool isBusy;

        public SocketConnection(TcpClient tcpClient, bool auth = false)
        {
            if (tcpClient != null)
            {
                NetworkStream networkStream = tcpClient.GetStream();

                //OutputStream = new StreamReader(networkStream, System.Text.Encoding.UTF8);
               //InputStream = new BinaryWriter(networkStream, System.Text.Encoding.UTF8);

                inOutStream = new MixedStream(networkStream, Encoding.UTF8, 1024 * 1024 * 2);


                if (auth)
                {
                    string message = "{'action':'authenticate','username':'lukas','userpasswd':'1234'}";
//                    InputStream.Write(Encoding.UTF8.GetBytes(message + "\n"));
//                    InputStream.Flush();
                    inOutStream.Write(Encoding.UTF8.GetBytes(message + "\n"));
                    inOutStream.Flush();

                    string json = inOutStream.ReadLine();
                }

                isBusy = false;
            }
        }
    }

    public class ConfigSettings
    {
        public int scenarioID;
        public string mqtt_topic;
        public int objID;
    }

    public class Communication2Lucy
    {
        public List<SocketConnection> socketPool = new List<SocketConnection>();

        private string _host;
        private int _port;

        public Communication2Lucy()
        {
        }


        public bool connect(String host, int port)
        {
            _host = host;
            _port = port;

            TcpClient socketForServer;
            try
            {
                socketForServer = new TcpClient(host, port);
                SocketConnection sc = new SocketConnection(socketForServer);
                socketPool.Add(sc);
            }
            catch
            {
                Console.WriteLine(
                "Failed to connect to server at {0}:{1}", host, port);
                return false;
            }

            Console.WriteLine("******* Trying to communicate with lucy on host " + host + " and port " + port + " *****");

            return true;
        }


        private SocketConnection getFreeSocketConnection()
        {
            SocketConnection result = null;

            var query = socketPool.Where(s => s.isBusy == false);
            if (query != null && query.Any())
            {
                result = query.First();
            }
            else
            {
                TcpClient socketForServer = new TcpClient(_host, _port);
                result = new SocketConnection(socketForServer, true);

                socketPool.Add(result);
            }

            return result;
        }



        public byte[] GetBinaryAttachmentMurx(JObject response)
        {
            SocketConnection sc = getFreeSocketConnection();

            sc.isBusy = true;
            {

                byte[] bArr;

                using (var streamReader = new MemoryStream())
                {
                    sc.inOutStream.CopyTo(streamReader);
                    bArr = streamReader.ToArray();
                }
                byte[] result = new byte[bArr.Length - 8];
                Buffer.BlockCopy(bArr, 8, result, 0, bArr.Length - 8);

                return result;
            }
        }

        /// <summary>
        ///     Get binary attachments followed after response to SendAction2Lucy.
        ///     Works only after calling SendAction2Lucy
        /// </summary>
        /// <param name="response">response from Lucy after calling SendAction2Lucy</param>
        /// <returns>array of attachments, preserving their order</returns>
        public byte[][] GetBinaryAttachments(JObject response)
        {
            SocketConnection sc = getFreeSocketConnection();

            sc.isBusy = true;
            {
                lock (sc.inOutStream)
                {
                    var count = response.SelectTokens("..streaminfo").Count();
                    var attachments = new byte[count][];
                    for (var i = 0; i < count; i++)
                    {
                        var lengthBytes = new byte[sizeof(long)];
                        sc.inOutStream.Read(lengthBytes, 0, lengthBytes.Length);
                        if (BitConverter.IsLittleEndian)
                            Array.Reverse(lengthBytes);
                        long length = BitConverter.ToInt64(lengthBytes, 0), haveRead = 0;
                        attachments[i] = new byte[length];
                        int readPrev;
                        while (haveRead < length &&
                               (readPrev = sc.inOutStream.Read(attachments[i], (int)haveRead, (int)(length - haveRead))) >
                               0)
                            haveRead += readPrev;
                    }
                    return attachments;
                }
                sc.isBusy = false;
            }
        }



        private string sendMessage(string message)
        {
            SocketConnection sc = getFreeSocketConnection();

            sc.isBusy = true;
//            sc.InputStream.Write(Encoding.UTF8.GetBytes(message + "\n"));
//            sc.InputStream.Flush();

//            string json = sc.OutputStream.ReadLine();

            sc.inOutStream.Write(Encoding.UTF8.GetBytes(message + "\n"));
            sc.inOutStream.Flush();

            string json = sc.inOutStream.ReadLine();

            if (json == null || json == "")
                json = sc.inOutStream.ReadLine();
//            json = sc.OutputStream.ReadLine();

            sc.isBusy = false;

            return json;
        }

        public JObject sendAction2Lucy(string action)
        {
            string json = sendMessage(action);

            try
            {
                var jObj2 = (JObject)JsonConvert.DeserializeObject(json);

                return jObj2;
            }
            catch (Exception ex)
            {
                var jObj2 = (JObject)JsonConvert.DeserializeObject("{\"" + json);

                return jObj2;
            }
        }



        public ConfigSettings sendCreateService2Lucy(string command, int scenarioID)
        {
            ConfigSettings result = null;

            string json = sendMessage(command);

            try
            {
                var jObj2 = (JObject)JsonConvert.DeserializeObject(json);

                int serviceID = jObj2.Value<JObject>("result").Value<int>("SObjID");
                string mqtt_topic = jObj2.Value<JObject>("result").Value<String>("mqtt_topic");

                if (mqtt_topic.EndsWith("/"))
                    mqtt_topic = mqtt_topic.Substring(0, mqtt_topic.Length - 1);

                result = new ConfigSettings() { scenarioID = scenarioID, mqtt_topic = mqtt_topic, objID = serviceID };
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine(ex.Message);
            }

            return result;
        }

        public JObject sendService2Lucy(string command)
        {
            SocketConnection sc = getFreeSocketConnection();

            sc.isBusy = true;
//            sc.InputStream.Write(Encoding.UTF8.GetBytes(command + "\n"));
//            sc.InputStream.Flush();

//            string json = sc.OutputStream.ReadLine();

            sc.inOutStream.Write(Encoding.UTF8.GetBytes(command + "\n"));
            sc.inOutStream.Flush();

            string json = sc.inOutStream.ReadLine();

            if (json == null || json == "")
                json = sc.inOutStream.ReadLine();
//            json = sc.OutputStream.ReadLine();

            var jObj2 = (JObject)JsonConvert.DeserializeObject(json);

            if (jObj2.Value<JToken>("result") != null)
            {
                if (((Newtonsoft.Json.Linq.JProperty)(jObj2.Value<JToken>("result").First())).Name.Equals("mqtt_topic"))
                {
//                    json = sc.OutputStream.ReadLine();
                    json = sc.inOutStream.ReadLine();
                    jObj2 = (JObject)JsonConvert.DeserializeObject(json);
                }

                sc.isBusy = false;

                if (jObj2.Value<JToken>("result") != null) // && ((Newtonsoft.Json.Linq.JProperty)(jObj2.Value<JToken>("result").First())).Name.Equals("mqtt"))
                    return ((Newtonsoft.Json.Linq.JObject)(jObj2.Value<JToken>("result")));
            }

            sc.isBusy = false;
            return null;
        }

        public JProperty authenticate(string user, string password)
        {
            return (JProperty)sendAction2Lucy("{'action':'authenticate','username':'" + user + "','userpasswd':'" + password + "'}").First;
        }

        public JProperty getActionsList()
        {
            return (JProperty)sendAction2Lucy("{'action':'get_list','of':['actions']}").First;
        }

        public JProperty getActionParameters(string action)
        {
            return (JProperty)sendAction2Lucy("{'action':'get_infos_about','actionname':'" + action + "'}").First;
        }

        public JProperty getServiceParameters(string service)
        {
            return (JProperty)sendAction2Lucy("{'action':'get_infos_about','servicename':'" + service + "'}").First;
        }

        public JProperty getServicesList()
        {
            return (JProperty)sendAction2Lucy("{'action':'get_list','of':['services']}").First;
        }

        private static string GetData()
        {
            //Ack from sql server
            return "ack";
        }
    }

    public class MixedStream : Stream
    {
        private readonly byte[] _buffer;
        private readonly Encoding _encoding = Encoding.UTF8;
        private readonly Stream _ns;
        private readonly byte[] _newLine;
        private int _bPos, _bSize;

        public MixedStream(Stream ns, Encoding enc = null, int bufferSize = 1024*1024)
        {
            if (enc != null)
                _encoding = enc;
            _ns = ns;
            _buffer = new byte[bufferSize];
            _newLine = _encoding.GetBytes("\n");
        }

        public int BufferSize
        {
            get { return _buffer.Length; }
        }

        public override bool CanRead
        {
            get { return _ns.CanRead; }
        }

        public override bool CanSeek
        {
            get { return _ns.CanSeek; }
        }

        public override bool CanWrite
        {
            get { return _ns.CanWrite; }
        }

        public override long Length
        {
            get { return _ns.Length; }
        }

        public override long Position
        {
            get { return _ns.Position; }
            set { _ns.Position = value; }
        }

        /// <summary>
        ///     Resets buffer - loses all unread data
        /// </summary>
        public void ResetBuffer()
        {
            _bPos = 0;
            _bSize = 0;
        }

        public override void Flush()
        {
            _ns.Flush();
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            return _ns.Seek(offset, origin);
        }

        public override void SetLength(long value)
        {
            _ns.SetLength(value);
        }

        /// <summary>
        ///     write line into buffer
        /// </summary>
        /// <param name="s"></param>
        public void Write(string s)
        {
            var b = _encoding.GetBytes(s);
            _ns.Write(b, 0, b.Length);
        }

        public void WriteLine(string s)
        {
            var b = _encoding.GetBytes(s + '\n');
            _ns.Write(b, 0, b.Length);
        }

        public void Write(byte[] bytes)
        {
            _ns.Write(bytes, 0, bytes.Length);
        }

        /// <summary>
        ///     Reads the line from buffer, preserving remaining bytes
        /// </summary>
        /// <returns></returns>
        public string ReadLine()
        {
            var sb = new StringBuilder("");
            if (_bPos < _bSize)
            {
                string s;
                var idx = IndexOf(_buffer, _newLine, _bPos, _bSize);
                if (idx >= 0)
                {
                    s = _encoding.GetString(_buffer, _bPos, idx - _bPos);
                    _bPos = idx + _newLine.Length;
                    return s.TrimEnd('\r');
                }
                s = _encoding.GetString(_buffer, _bPos, _bSize - _bPos);
                _bPos += _encoding.GetByteCount(s);
                sb.Append(s);
                if (_bPos < _bSize)
                    Buffer.BlockCopy(_buffer, _bPos, _buffer, 0, _bSize - _bPos);
                _bSize -= _bPos;
                _bPos = 0;
            }
            // now buffer is almost empty
            int curRead;
            while ((curRead = _ns.Read(_buffer, _bSize, _buffer.Length - _bSize)) > 0)
            {
                _bSize += curRead;
                string s;
                var idx = IndexOf(_buffer, _newLine, _bPos, _bSize);
                if (idx >= 0)
                {
                    s = _encoding.GetString(_buffer, _bPos, idx - _bPos);
                    _bPos = idx + _newLine.Length;
                    return sb.Append(s.TrimEnd('\r')).ToString();
                }
                s = _encoding.GetString(_buffer, _bPos, _bSize - _bPos);
                _bPos += _encoding.GetByteCount(s);
                sb.Append(s);
                if (_bPos < _bSize)
                    Buffer.BlockCopy(_buffer, _bPos, _buffer, 0, _bSize - _bPos);
                _bSize -= _bPos;
                _bPos = 0;
            }
            return sb.ToString();
        }

        /// <summary>
        ///     Naively search for a pattern
        /// </summary>
        /// <param name="src"></param>
        /// <param name="pattern"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        private int IndexOf(byte[] src, byte[] pattern, int start, int end)
        {
            bool found;
            for (var i = start; i <= end - pattern.Length; i++)
            {
                found = true;
                for (var j = 0; j < pattern.Length; j++)
                    if (pattern[j] != src[i + j])
                    {
                        found = false;
                        break;
                    }
                if (found)
                    return i;
            }
            return -1;
        }


        public int Read(byte[] buffer)
        {
            return Read(buffer, 0, buffer.Length);
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            int haveRead = 0, curRead;
            // there are unread bytes in buffer, send first them
            if (_bPos < _bSize)
            {
                haveRead = Math.Min(_bSize - _bPos, count);
                Buffer.BlockCopy(_buffer, _bPos, buffer, offset, haveRead);
                _bPos += haveRead;
                if (haveRead == count)
                    return count;
            }
            // now _bPos == _bSize for sure (otherwise it would return to caller), so we can reset _buffer safely
            ResetBuffer();
            // now _buffer is empty
            while ((curRead = _ns.Read(_buffer, _bSize, _buffer.Length - _bSize)) > 0)
            {
                _bSize += curRead;
                // if we have read enough
                if (_bSize + haveRead >= count)
                {
                    var toRead = count - haveRead;
                    Buffer.BlockCopy(_buffer, 0, buffer, offset + haveRead, toRead);
                    _bPos += toRead;
                    return count;
                }
                // if _buffer is full
                if (_bSize == _buffer.Length)
                {
                    Buffer.BlockCopy(_buffer, 0, buffer, offset + haveRead, _bSize);
                    haveRead += _bSize;
                    ResetBuffer();
                }
            }
            // if we could not read anything
            if (_bSize == 0)
                return haveRead;
            // if we have read something, but not enough
            Buffer.BlockCopy(_buffer, 0, buffer, offset + haveRead, _bSize);
            haveRead += _bSize;
            ResetBuffer();
            return haveRead;
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            _ns.Write(buffer, offset, count);
        }
    }
}


