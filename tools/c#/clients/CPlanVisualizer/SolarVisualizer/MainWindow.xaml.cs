﻿using System;
using System.Windows;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

using OpenTK.Graphics.OpenGL;

using Size = System.Drawing.Size;
using SolarVisualizer.ViewModels;
using uPLibrary.Networking.M2Mqtt;
using System.IO;
using CPlan.VisualObjects;
using System.Collections.Generic;
using System.Text;

namespace SolarVisualizer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>

    public partial class MainWindow : Window
    {
        #region Fields

        private WriteableBitmap backbuffer;

        private FrameBufferHandler framebufferHandler;

        private int frames;

        private DateTime lastMeasureTime;

        private Renderer renderer;

        private List<GeoObject> _remainDrawingObjects = new List<GeoObject>();

        private SolarVM ViewModel;

        DispatcherTimer timer = new DispatcherTimer();

        #endregion

        #region Constructors and Destructors

        public MainWindow()
        {
            this.InitializeComponent();

            this.renderer = new Renderer(new Size(400, 400));
            this.framebufferHandler = new FrameBufferHandler();

            timer.Interval = TimeSpan.FromMilliseconds(1);
            timer.Tick += this.TimerOnTick;
            timer.Start();

            ViewModel = new SolarVM(framebufferHandler.glControl);

            Settings.SyncCamera = true;
            Settings.SyncSelection = true;
            syncCamera.Checked += syncCamera_Checked;
            syncSelection.Checked += syncSelection_Checked;
            syncCamera.Unchecked += syncCamera_Unchecked;
            syncSelection.Unchecked += syncSelection_Unchecked;

            this.SizeChanged += MainWindow_SizeChanged;
            this.MouseWheel += MainWindow_MouseWheel;
            this.MouseDown += MainWindow_MouseDown;
            this.MouseUp += MainWindow_MouseUp;
            this.MouseMove += MainWindow_MouseMove;

            initMQTT();
        }

        void syncSelection_Unchecked(object sender, RoutedEventArgs e)
        {
            Settings.SyncSelection = false;
        }

        void syncCamera_Unchecked(object sender, RoutedEventArgs e)
        {
            Settings.SyncCamera = false;
        }

        void syncSelection_Checked(object sender, RoutedEventArgs e)
        {
            Settings.SyncSelection = true;
        }

        void syncCamera_Checked(object sender, RoutedEventArgs e)
        {
            Settings.SyncCamera = true;
        }


        private void initMQTT()
        {
            Guid newguid = Guid.NewGuid();
            string identifier = newguid.ToString("N");

            MqttClient mqttClient = new MqttClient(Settings.LucyHost);
            mqttClient.Connect("SolarVisualizer_" + identifier);
            mqttClient.Subscribe(new string[] { "CameraPos" }, new byte[] { 1 });
            mqttClient.Subscribe(new string[] { "SelectionChanged" }, new byte[] { 1 });
            mqttClient.Subscribe(new string[] { "LoadScene_Border" }, new byte[] { 1 });
            mqttClient.Subscribe(new string[] { "LoadScene_InitialLines" }, new byte[] { 1 });
            mqttClient.Subscribe(new string[] { "LoadScene_EnvironmentClose" }, new byte[] { 1 });
            mqttClient.Subscribe(new string[] { "LoadScene_Environment3DBuildings" }, new byte[] { 1 });
            mqttClient.Subscribe(new string[] { "+" }, new byte[] { 1 });

            mqttClient.MqttMsgPublishReceived += mqttClient_MqttMsgPublishReceived;
        }

        void mqttClient_MqttMsgPublishReceived(object sender, uPLibrary.Networking.M2Mqtt.Messages.MqttMsgPublishEventArgs e)
        {
            if (e.Topic == "CameraPos" && Settings.SyncCamera)
            {
                System.IO.MemoryStream ms = new System.IO.MemoryStream(e.Message);
                System.Runtime.Serialization.Formatters.Binary.BinaryFormatter bf = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                object result = bf.Deserialize(ms);
                if (result != null && result is double[])
                {
                    double[] pos = (double[])result;

                    framebufferHandler.glControl.ActiveCamera.Position.x = pos[0];
                    framebufferHandler.glControl.ActiveCamera.Position.y = pos[1];
                    framebufferHandler.glControl.ActiveCamera.Position.z = pos[2];
                    framebufferHandler.glControl.ActiveCamera.Alpha = float.Parse(pos[3].ToString());
                    framebufferHandler.glControl.ActiveCamera.Beta = float.Parse(pos[4].ToString());
                    framebufferHandler.glControl.ActiveCamera.Gamma = float.Parse(pos[5].ToString());
                }
            }
            else if (e.Topic == "Camera2D" && Settings.SyncCamera)
            {
                if (framebufferHandler.glControl.ActiveCamera != framebufferHandler.glControl.Camera_2D)
                    framebufferHandler.glControl.CameraChange();
            }
            else if (e.Topic == "Camera3D" && Settings.SyncCamera)
            {
                if (framebufferHandler.glControl.ActiveCamera != framebufferHandler.glControl.Camera_3D)
                    framebufferHandler.glControl.CameraChange();
            }
            else if (e.Topic == "SelectionChangedSolar" && Settings.SyncSelection)
            {
                System.IO.MemoryStream ms = new System.IO.MemoryStream(e.Message);
                System.Runtime.Serialization.Formatters.Binary.BinaryFormatter bf = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                var result = bf.Deserialize(ms);
                if (result != null && result is int[])
                {
                    int[] config = (int[])result;

                    if (config != null)
                    {
                        ViewModel.reload(config[0], config[1]);
                    }
                }
            }
            else if (e.Topic == "LoadScene_Border")
            {
                loadScene_Border(new System.IO.MemoryStream(e.Message));
            }
            else if (e.Topic == "LoadScene_InitialLines")
            {
                loadScene_InitialLines(new System.IO.MemoryStream(e.Message));
            }
            else if (e.Topic == "LoadScene_EnvironmentClose")
            {
                loadScene_EnvironmentClose(new System.IO.MemoryStream(e.Message));
            }
            else if (e.Topic == "LoadScene_Environment3DBuildings")
            {
                loadScene_Environment3DBuildings(new System.IO.MemoryStream(e.Message));
                framebufferHandler.glControl.ZoomAll();
            }
            else if (e.Topic == "SmartGrid")
            {
                string message = Encoding.UTF8.GetString(e.Message);
                System.IO.MemoryStream ms = new System.IO.MemoryStream(e.Message);
                System.Runtime.Serialization.Formatters.Binary.BinaryFormatter bf = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                var result = bf.Deserialize(ms);

                if (result != null && result is GGrid)
                {
                  //  ScalarField sf = (ScalarField)result;
                    ViewModel.smartGrid = (GGrid)result;
                }
                else if (result != null && result is OpenTK.Vector3d[])
                {
                   
                    List<OpenTK.Vector3d> pp = new List<OpenTK.Vector3d>();
                    OpenTK.Vector3d[] vecs = (OpenTK.Vector3d[])result;
                    double[] rr = new double[vecs.Length];
                    int i=0;
                    foreach (var v in vecs) {
                 //       if (!pp.Contains(v))
                            pp.Add(v);

                        rr[i] = i;
                        i++;
                    }
     //               pp.Add(vecs[0]);

                    //    OpenTK.Vector2d[] pts = (OpenTK.Vector2d[])result;
                    
                    var sf = new ScalarField((OpenTK.Vector3d[])pp.ToArray(),
                       4, new OpenTK.Vector3d(0, 0, 1));
                ViewModel.smartGrid = new GGrid(sf, "solarAnalysis");


           //         ViewModel.analysisPoints = pp;
          //            ViewModel.smartGrid = new GGrid(pp, rr, 4); 
                    ViewModel.reload(Settings.SObjID, Settings.ScenarioID);
                    //ViewModel.reload(3120, 2801);
                }
                else if (result != null && result is List<OpenTK.Vector2d>)
                {
                    List<OpenTK.Vector2d> pp = (List<OpenTK.Vector2d>)result;
                    double[] rr = new double[pp.Count];
             
                    for (int i=0; i<pp.Count; i++)
                    {
                       rr[i] = i;
                    }

                    //    OpenTK.Vector2d[] pts = (OpenTK.Vector2d[])result;

                    //             var sf = new ScalarField(pts,
                    //              4, new OpenTK.Vector3d(0, 0, 1));
                    //         ViewModel.smartGrid = new GGrid(sf, "solarAnalysis");


                    ViewModel.analysisPoints = pp;
     //               ViewModel.smartGrid = new GGrid(pp, rr, 4);
                    ViewModel.reload(Settings.SObjID, Settings.ScenarioID);
                    //ViewModel.reload(3120, 2801);
                }
            }

            timer.Start();
        }

        private void loadScene_Border(Stream stream)
        {
            List<CPlan.Geometry.Line2D> BorderLines = DxfConversions.LinesFromDxf(stream);
            if (BorderLines != null)
            {
                CPlan.Geometry.Poly2D Border = new CPlan.Geometry.Poly2D(BorderLines);

                if (Border != null)
                {
                    ViewModel.sceneDrawingObjects.Add(new GPolygon(Border));
                    _remainDrawingObjects.Add(new GPolygon(Border));

                    framebufferHandler.glControl.AllObjectsList.ObjectsToDraw.Clear();
                    framebufferHandler.glControl.AllObjectsList.ObjectsToDraw.AddRange(_remainDrawingObjects);
                }
            }
        }


        private void loadScene_InitialLines(Stream stream)
        {
            List<CPlan.Geometry.Line2D> InitialLines = DxfConversions.LinesFromDxf(stream);

            if (InitialLines != null)
            {
                foreach (CPlan.Geometry.Line2D line in InitialLines)
                {
                    GLine iniLine = new GLine(line);
                    iniLine.Width = 5;
                    iniLine.Color = ConvertColor.CreateColor4(System.Drawing.Color.Red); //RoyalBlue);
                    _remainDrawingObjects.Add(iniLine);
                    ViewModel.sceneDrawingObjects.Add(iniLine);
                }

                framebufferHandler.glControl.AllObjectsList.ObjectsToDraw.Clear();
                framebufferHandler.glControl.AllObjectsList.ObjectsToDraw.AddRange(_remainDrawingObjects);
            }
        }

        private void loadScene_EnvironmentClose(Stream stream)
        {
            List<CPlan.Geometry.Line2D> EnvironmentCloseNetwork = DxfConversions.LinesFromDxf(stream);

            if (EnvironmentCloseNetwork != null)
            {
                foreach (CPlan.Geometry.Line2D line in EnvironmentCloseNetwork)
                {
                    _remainDrawingObjects.Add(new GLine(line));
                    ViewModel.sceneDrawingObjects.Add(new GLine(line));
                }

                framebufferHandler.glControl.AllObjectsList.ObjectsToDraw.Clear();
                framebufferHandler.glControl.AllObjectsList.ObjectsToDraw.AddRange(_remainDrawingObjects);
            }
        }

        private void loadScene_Environment3DBuildings(Stream stream)
        {
            framebufferHandler.glControl.OpenObjFile(stream);

            framebufferHandler.glControl.AllObjectsList.ObjectsToDraw.Clear();
            framebufferHandler.glControl.AllObjectsList.ObjectsToDraw.AddRange(_remainDrawingObjects);
        }



        void MainWindow_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            System.Reflection.MethodInfo onMouseMove =
       framebufferHandler.glControl.GetType().GetMethod("OnMouseMove",
                                   System.Reflection.BindingFlags.NonPublic |
                                   System.Reflection.BindingFlags.Instance);

            // Call the panel2 mousehwweel event with the same parameters
            if (onMouseMove != null)
            {
                System.Windows.Forms.MouseButtons b = System.Windows.Forms.MouseButtons.None;
                if (e.MouseDevice.LeftButton == System.Windows.Input.MouseButtonState.Pressed)
                    b = System.Windows.Forms.MouseButtons.Left;
                if (e.MouseDevice.RightButton == System.Windows.Input.MouseButtonState.Pressed)
                    b = System.Windows.Forms.MouseButtons.Right;

                System.Windows.Forms.MouseEventArgs args = new System.Windows.Forms.MouseEventArgs(b, 0, 0, 0, 0);
                onMouseMove.Invoke(framebufferHandler.glControl, new object[] { args });
            }

            timer.Start();
        }

        void MainWindow_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            System.Reflection.MethodInfo onMouseUp =
               framebufferHandler.glControl.GetType().GetMethod("OnMouseUp",
                                   System.Reflection.BindingFlags.NonPublic |
                                   System.Reflection.BindingFlags.Instance);

            // Call the panel2 mousehwweel event with the same parameters
            if (onMouseUp != null)
            {
                System.Windows.Forms.MouseButtons b = System.Windows.Forms.MouseButtons.None;
                if (e.MouseDevice.LeftButton == System.Windows.Input.MouseButtonState.Pressed)
                    b = System.Windows.Forms.MouseButtons.Left;
                if (e.MouseDevice.RightButton == System.Windows.Input.MouseButtonState.Pressed)
                    b = System.Windows.Forms.MouseButtons.Right;

                System.Windows.Forms.MouseEventArgs args = new System.Windows.Forms.MouseEventArgs(b, e.ClickCount, (int)e.GetPosition(image).X, (int)(-e.GetPosition(image).Y), 0);
                onMouseUp.Invoke(framebufferHandler.glControl, new object[] { args });
            }

            timer.Start();
        }

        void MainWindow_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            System.Reflection.MethodInfo onMouseDown =
                framebufferHandler.glControl.GetType().GetMethod("OnMouseDown",
                                   System.Reflection.BindingFlags.NonPublic |
                                   System.Reflection.BindingFlags.Instance);

            // Call the panel2 mousehwweel event with the same parameters
            if (onMouseDown != null)
            {
                System.Windows.Forms.MouseButtons b = System.Windows.Forms.MouseButtons.None;
                if (e.MouseDevice.LeftButton == System.Windows.Input.MouseButtonState.Pressed)
                    b = System.Windows.Forms.MouseButtons.Left;
                if (e.MouseDevice.RightButton == System.Windows.Input.MouseButtonState.Pressed)
                    b = System.Windows.Forms.MouseButtons.Right;

                System.Windows.Forms.MouseEventArgs args = new System.Windows.Forms.MouseEventArgs(b, e.ClickCount, (int)e.GetPosition(image).X, (int)(-e.GetPosition(image).Y), 0);
                onMouseDown.Invoke(framebufferHandler.glControl, new object[] { args });
            }

            timer.Start();
        }

        void MainWindow_MouseRightButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            System.Reflection.MethodInfo onMouseRightButtonUp =
       framebufferHandler.glControl.GetType().GetMethod("OnMouseRightButtonUp",
                                   System.Reflection.BindingFlags.NonPublic |
                                   System.Reflection.BindingFlags.Instance);

            // Call the panel2 mousehwweel event with the same parameters
            if (onMouseRightButtonUp != null)
            {
                System.Windows.Forms.MouseEventArgs args = new System.Windows.Forms.MouseEventArgs(System.Windows.Forms.MouseButtons.Right, e.ClickCount, 0, 0, 0);
                onMouseRightButtonUp.Invoke(framebufferHandler.glControl, new object[] { args });
            }

            timer.Start();
        }

        void MainWindow_MouseRightButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            System.Reflection.MethodInfo onMouseRightButtonDown =
       framebufferHandler.glControl.GetType().GetMethod("OnMouseRightButtonDown",
                                   System.Reflection.BindingFlags.NonPublic |
                                   System.Reflection.BindingFlags.Instance);

            // Call the panel2 mousehwweel event with the same parameters
            if (onMouseRightButtonDown != null)
            {
                System.Windows.Forms.MouseEventArgs args = new System.Windows.Forms.MouseEventArgs(System.Windows.Forms.MouseButtons.Right, e.ClickCount, 0, 0, 0);
                onMouseRightButtonDown.Invoke(framebufferHandler.glControl, new object[] { args });
            }

            timer.Start();
        }


        void MainWindow_MouseWheel(object sender, System.Windows.Input.MouseWheelEventArgs e)
        {
            System.Windows.Forms.ScrollableControl bla = framebufferHandler.glControl as System.Windows.Forms.ScrollableControl;

            System.Reflection.MethodInfo onMouseWheel =
       framebufferHandler.glControl.GetType().GetMethod("OnMouseWheel",
                                   System.Reflection.BindingFlags.NonPublic |
                                   System.Reflection.BindingFlags.Instance);

            // Call the panel2 mousehwweel event with the same parameters
            if (onMouseWheel != null)
            {
                System.Windows.Forms.MouseEventArgs args = new System.Windows.Forms.MouseEventArgs(System.Windows.Forms.MouseButtons.None, 0, 0, 0, e.Delta);
                onMouseWheel.Invoke(framebufferHandler.glControl, new object[] { args });
            }

            timer.Start();
        }

        void MainWindow_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            timer.Start();
        }

        #endregion

        #region Methods

        private void Render()
        {
            if (this.image.ActualWidth <= 0 || this.image.ActualHeight <= 0)
            {
                return;
            }

            this.framebufferHandler.Prepare(new Size((int)this.ActualWidth, (int)this.ActualHeight));

            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadIdentity();
            float halfWidth = (float)(this.ActualWidth / 2);
            float halfHeight = (float)(this.ActualHeight / 2);
            GL.Ortho(-halfWidth, halfWidth, halfHeight, -halfHeight, 1000, -1000);
            GL.Viewport(0, 0, (int)this.ActualWidth, (int)this.ActualHeight);

            framebufferHandler.Render2();

            GL.Finish();

            this.framebufferHandler.Cleanup(ref this.backbuffer);

            if (this.backbuffer != null)
            {
                this.image.Source = this.backbuffer;
            }

            this.frames++;

            timer.Stop();
        }

        private void TimerOnTick(object sender, EventArgs eventArgs)
        {
            if (DateTime.Now.Subtract(this.lastMeasureTime) > TimeSpan.FromSeconds(100))
            {
                this.frames = 0;
                this.lastMeasureTime = DateTime.Now;
            }

            this.Render();
        }

        #endregion

        private void SwitchCamera_Click(object sender, RoutedEventArgs e)
        {
            framebufferHandler.glControl.CameraChange();
            timer.Start();

        }

        private void Window_Closed_1(object sender, EventArgs e)
        {
            System.Environment.Exit(0);
        }
    }
}