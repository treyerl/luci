﻿//using CPlan.Geometry;
using CPlan.Isovist2D;
using CPlan.VisualObjects;
using GeoAPI.Geometries;
//using LucyCommunication;
using LucySerializers;
using NetTopologySuite.Geometries;
using Newtonsoft.Json.Linq;
using OpenTK;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using uPLibrary.Networking.M2Mqtt;

namespace SolarVisualizer.ViewModels
{
    public static class Settings
    {
        public static string LucyHost { get; set; }

        public static int SObjID { get; set; }

        public static int ScenarioID { get; set; }

        public static int Parameter { get; set; }

        public static bool SyncCamera { get; set; }

        public static bool SyncSelection { get; set; }
    }

    public class SolarVM
    {
        public List<GeoObject> sceneDrawingObjects = new List<GeoObject>();

        public List<double> solarResults { get; protected set; }

        // --- Arrays ---
        [Category("Measures"), Description("Area")]
        public double[] Area { get; set; }

        public List<Vector2d> analysisPoints;
        private List<Polygon> buildings;
        private List<double> buildingHeights;

        private Communication2Lucy cl = new Communication2Lucy();

        private bool _connected2Luci = false;
        private MqttClient mqttClient;

        private string service_topic;
        private string scenario_topic;
        private string service_scenario_topic;

        private GLCameraControl _glCamera;

        public GGrid smartGrid;

        private double[] points;

        //  private int SObjID = 134;
        private int ScID;

        private int _sObjID;
        public int SObjID
        {
            get { return _sObjID; }
            set
            {
                if (value != _sObjID)
                {
                    if (value >= 0)
                    {
                        //                        hook2SObjID(value);
                    }

                    _sObjID = value;
                }
            }
        }

        public SolarVM(GLCameraControl glCamera)
        {
            _glCamera = glCamera;

            solarResults = new List<double>();
            //Settings.LucyHost = "localhost";
            // Settings.SObjID = 64; // 47; // 15;
            init();

 //           reload(3120, 2801);
        }

        private void drawResults()
        {
            //GGrid _smartGrid = new GGrid(analysisPoints, Area, 10);
            double[] par;

            int cellsize = 4;

            if (analysisPoints != null && analysisPoints.Count() > 1)
            {
                int cellsizeX = (int)Math.Abs(analysisPoints[1].X - analysisPoints[0].X);
                int cellsizeY = (int)Math.Abs(analysisPoints[1].Y - analysisPoints[0].Y);
                cellsize = Math.Max(cellsizeX, cellsizeY);
                cellsize = Math.Max(cellsize, 2);
            }

            /* */
            analysisPoints = new List<Vector2d>();
            int num = (int) Math.Floor((decimal)(points.Count() / 3));
            for (int i = 0; i < num; i++)
            {
                analysisPoints.Add(new Vector2d(points[3 * i], points[3 * i + 1]));
            }
            GGrid _smartGrid = new GGrid(analysisPoints, solarResults.ToArray(), 4);
            _smartGrid.GenerateTexture();

            smartGrid.GenerateTexture();
            smartGrid.ScalarField.GenerateTexture(solarResults.Take(smartGrid.ScalarField.GridSize), "solarAnalysis");
            smartGrid.GenerateTexture();



/*
            List<Vector2d>  ap = new List<Vector2d>();

            int num2 = smartGrid.ScalarField.GridSize;
            double[] r = new double[num2];
            for (int i = 0; i < num2; i++)
            {
                ap.Add(new Vector2d(smartGrid.ScalarField.Grid[i].X, smartGrid.ScalarField.Grid[i].Y));
                r[i] = i;
            }
            GGrid __smartGrid = new GGrid(ap, r, 4);
            _smartGrid.GenerateTexture();*/
//            GGrid _smartGrid = new GGrid(analysisPoints, Results.ToArray(), cellsize);


            _glCamera.AllObjectsList.ObjectsToDraw.Clear();
            _glCamera.AllObjectsList.ObjectsToDraw = new ObjectsDrawList();
            _glCamera.AllObjectsList.ObjectsToDraw.AddRange(sceneDrawingObjects);
            _glCamera.AllObjectsList.ObjectsToDraw.Add(smartGrid);
            //_glCamera.AllObjectsList.ObjectsToDraw.Add(__smartGrid);

            // scene start

            /*
            glCamera.OpenObjFile("C:\\home\\dev\\eth\\cplan\\Various\\InputDataRochor\\Environment3DBuildings2.obj");

            List<GeoObject> _remainDrawingObjects = new List<GeoObject>();

            CPlan.Geometry.Poly2D Border = new CPlan.Geometry.Poly2D(DxfConversions.LinesFromDxf("C:\\home\\dev\\eth\\cplan\\Various\\InputDataRochor\\Border2.dxf"));

            List<CPlan.Geometry.Line2D> EnvironmentCloseNetwork = DxfConversions.LinesFromDxf("C:\\home\\dev\\eth\\cplan\\Various\\InputDataRochor\\EnvironmentClose2.dxf");

            List<CPlan.Geometry.Line2D> InitialLines = DxfConversions.LinesFromDxf("C:\\home\\dev\\eth\\cplan\\Various\\InputDataRochor\\InitialLines2.dxf");

     //       System.IO.File.cl

            _remainDrawingObjects.Add(new GPolygon(Border));
            foreach (CPlan.Geometry.Line2D line in EnvironmentCloseNetwork)
            {
                _remainDrawingObjects.Add(new GLine(line));
            }
            foreach (CPlan.Geometry.Line2D line in InitialLines)
            {
                GLine iniLine = new GLine(line);
                iniLine.Width = 5;
                iniLine.Color = ConvertColor.CreateColor4(System.Drawing.Color.Red); //RoyalBlue);
                _remainDrawingObjects.Add(iniLine);
            }
            glCamera.AllObjectsList.ObjectsToDraw.AddRange(_remainDrawingObjects);
             */
            // scene end

            buildings = null;
            if (buildings != null)
            {
                int b = 0;

                foreach (Polygon p in buildings)
                {
                    List<CPlan.Geometry.Line2D> lines = new List<CPlan.Geometry.Line2D>();
                    int numCoordinates = p.NumPoints;

                    Vector2d[] v = new Vector2d[numCoordinates - 1];

                    for (int i = 0; i < numCoordinates - 1; i++)
                    {
                        Coordinate coord = p.Coordinates[i];
                        Coordinate coordNext = p.Coordinates[i + 1];

                        lines.Add(new CPlan.Geometry.Line2D(new Vector2d(coord.X, coord.Y),
                                  new Vector2d(coordNext.X, coordNext.Y)));

                        v[i] = new Vector2d(coord.X, coord.Y);
                    }

                    CPlan.Geometry.Poly2D poly = new CPlan.Geometry.Poly2D(v);
                    var bPoly = new GPrism(v);
                    bPoly.Identity = 32;

                    if (buildingHeights != null && buildingHeights.Count > b)
                        bPoly.Height = buildingHeights[b];
                    else
                        bPoly.Height = 20;

                    bPoly.DeltaZ = 0.1f;
                    bPoly.FillColor = ConvertColor.CreateColor4(System.Drawing.Color.White);
                    bPoly.Filled = true;
                    bPoly.BorderWidth = 5;
                    Vector4 col = ConvertColor.CreateColor4(System.Drawing.Color.Black);
                    col.W = 1f; //0.1f; // alpha value
                    bPoly.BorderColor = col;
                    b++;


                    //       glCamera.AllObjectsList.Add(bPoly);
                    _glCamera.AllObjectsList.ObjectsToDraw.Add(bPoly);
                }
            }

            //            glCamera.Refresh();

            _glCamera.ActivateInteraction();
            _glCamera.Invalidate();
            //          glCamera.MakeCurrent();
            //          _glCamera.ZoomAll();
            //        glCamera.Show();


        }

        private void showResultsSolar()
        {
            /*
 *                             string msg = "{'action':'create_service','inputs':{'northOrientationAngle':" + angle +
                                ",'Points':{'format':'doublesXYZ','streaminfo':{'order':1, 'checksum':'" + checksum1 +
                                "'}},'Normales':{'format':'doublesXYZ','streaminfo':{'order':2, 'checksum':'" + checksum2 +
                                "'}},'HdriImage':{'format':'hdrImage','streaminfo':{'order':3, 'checksum':'" + checksum3 +
                                "'}},'resolution':" + resolution + ", 'calculationMode':'" + calcMode + "'},'classname':'SolarAnalysis','ScID':" + scenarioID + "}";
*/
            try
            {
                /*
                var sf = new ScalarField(border.PointsFirstLoop
                               .Select(v => new Vector3d(v.X, v.Y, 0)).ToArray(),
                               cellSize, new Vector3d(0, 0, 1));

                smartGrid = new GGrid(sf, "solarAnalysis");
                */
                if (solarResults == null || smartGrid == null)
                {
                    System.Windows.Forms.MessageBox.Show("no solar data to show .. :(");
                    return;
                }


                analysisPoints = new List<Vector2d>();
                int num = (int)Math.Floor((decimal)(points.Count() / 3));
                for (int i = 0; i < num; i++)
                {
                   // analysisPoints.Add(new Vector2d(points[3 * i], points[3 * i + 1]));
                    analysisPoints.Add(new Vector2d(points[3 * i], points[3 * i + 1]));
                }
              //  GGrid _smartGrid = new GGrid(analysisPoints, solarResults.ToArray(), 4); 
//                smartGrid = new GGrid(analysisPoints, solarResults.ToArray(), 4);

                //            GenBuildingLayout[] buildings = new GenBuildingLayout[0];


 //               if (solarResults.Count < smartGrid.ScalarField.GridSize)
   //                 Console.Out.WriteLine("zu kleiiiiiiiiiiiiiiiin " + smartGrid.ScalarField.GridSize + " - " + solarResults.Count);
     //           else
                {
//                    List<Vector2d> pts = new List<Vector2d>();
  //                  foreach (var pc in smartGrid.ScalarField.Grid)
    //                    pts.Add(new Vector2d(pc.X, pc.Y));

 //                    smartGrid = new GGrid(analysisPoints, solarResults.ToArray(), 4);
 ////                   smartGrid.ScalarField.GenerateTexture(solarResults.Take(smartGrid.ScalarField.GridSize), "solarAnalysis");
 ////                  smartGrid.GenerateTexture();
                }
                    
                    /*
                    _activeMultiLayout.SolarAnalysisResults = saResults.Take(_smartGrid.ScalarField.GridSize);
                    if (_activeLayout != null)
                        _activeLayout.SolarAnalysisResults = saResults.Take(_smartGrid.ScalarField.GridSize);*/

/*                IEnumerable<double> saResults = solarResults.Skip(smartGrid.ScalarField.GridSize);

                var pointsV = new Vector3d[points.Length / 3];
                //            var normalesV = new CGeom3d.Vec_3d[normales.Length / 3];
                for (var i = 0; i < pointsV.Length; i++)
                {
                    pointsV[i] = new Vector3d(points[3 * i], points[3 * i + 1], points[3 * i + 2]);
                    //                normalesV[i] = new CGeom3d.Vec_3d(normales[3 * i], normales[3 * i + 1], normales[3 * i + 2]);
                }

                int num = (int)Math.Floor((decimal)pointsV.Count() / 4);
                num = 2;
                for (int i = 0; i < num; i++)
                {
                    Vector3d[] side = new Vector3d[] { pointsV[i * 4 + 0], pointsV[i * 4 + 1], pointsV[i * 4 + 2], pointsV[i * 4 + 3] };
                    Vector3d bcenter = new Vector3d(); // center of building

                    ScalarField sideGrid = new ScalarField(side, cellSize, bcenter); // side.Mean() - bcenter);
                    sideGrid.GenerateTexture(saResults.Take(sideGrid.GridSize), "solarAnalysis");
                    saResults = saResults.Skip(sideGrid.GridSize);
                }
*/
                //    _smartGrid = _solarAnalysisLayout.SmartGrid;
                //    DrawLayout();

            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message);
            }

        }


        private void init()
        {
            //            GridVM = new RasterGridVM();

            SObjID = Settings.SObjID;

            string result = "Connection to Lucy was ";

            if (!cl.connect(Settings.LucyHost, 7654))
            {
                result += "not ";
                _connected2Luci = false;
            }
            else
                _connected2Luci = true;

            result += "successful! \n";

            Console.Out.WriteLine(result);

            if (_connected2Luci)
            {

                Console.Out.WriteLine("-> authenticate\n");
                JProperty result2 = cl.authenticate("lukas", "1234");
                result = result2.Name + ": " + result2.Value + "\n";
                Console.Out.WriteLine(result);

                Guid newguid = Guid.NewGuid();
                string identifier = newguid.ToString("N");

                //               mqttClient = new MqttClient(Settings.LucyHost);
                //             mqttClient.Connect("CPlanVisualizer_" + identifier);
                //           mqttClient.Subscribe(new string[] { "CameraPos" }, new byte[] { 1 });
                //               mqttClient.MqttMsgPublishReceived += mqttClient_MqttMsgPublishReceived;

                //                hook2SObjID(SObjID);



                //                readIsovistInputs(Settings.SObjID);
                //                readLucyResults(Settings.SObjID);
            }

        }

        public void reload(int sobjId, int scenarioID)
        {
            Settings.SObjID = sobjId;
            Settings.ScenarioID = scenarioID;

            readSolarInputs(Settings.SObjID);
            readLucyResults(Settings.SObjID);

            showResultsSolar();

            drawResults();
        }

        void mqttClient_MqttMsgPublishReceived(object sender, uPLibrary.Networking.M2Mqtt.Messages.MqttMsgPublishEventArgs e)
        {
            if (e.Topic == "CameraPos")
            {
                string message = Encoding.UTF8.GetString(e.Message);

                System.IO.MemoryStream ms = new System.IO.MemoryStream(e.Message);
                System.Runtime.Serialization.Formatters.Binary.BinaryFormatter bf = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                object result = bf.Deserialize(ms);
                if (result != null && result is double[])
                {
                    double[] pos = (double[])result;

                    _glCamera.ActiveCamera.Position.x = pos[0];
                    _glCamera.ActiveCamera.Position.y = pos[1];
                    _glCamera.ActiveCamera.Position.z = pos[2];
                    _glCamera.Invalidate();

                }
                /*
            if (message == "DONE")
            {
                Console.Out.WriteLine("DOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOONE");

                System.Windows.Application.Current.Dispatcher.Invoke((Action)(delegate
                {
                    showResults();
                }));

            }*/
            }
        }

        private void readSolarInputs(int instanceID)
        {
            string msg = "{'action':'get_service_inputs','SObjID':" + instanceID + "}";
            JObject result = cl.sendAction2Lucy(msg);

            if (result != null)
            {
                JToken inputs = result.Value<JToken>("inputs").Value<JToken>("Normales");

                byte[][] attachments = cl.GetBinaryAttachments(result);
                byte[] arr0 = attachments[2];
                byte[] arr1 = attachments[1];
                byte[] arr2 = attachments[0];

                            

//                List<Vector3d> 
points = new double[arr0.Length / 8];
                Buffer.BlockCopy(arr0, 0, points, 0, arr0.Length / 8);

                double[] normals = new double[arr1.Length / 8];
                Buffer.BlockCopy(arr1, 0, normals, 0, arr1.Length / 8);

//for (int i = 0; i < arr0.Length; i++)
  //  points[i] = BitConverter.ToDouble(arr0, i * 8);
                /*
                // prepare input values
                if (inputs != null && inputs.HasValues)
                {
                    JArray pts = (JArray)result.Value<JToken>("inputs").Value<JToken>("inputVals").Value<JArray>("coordinates");
                    //inputs.Value<JObject>("inputVals").Value<JArray>("coordinates");
                    int numP = pts.Count();
                    for (int j = 0; j < numP; j++)
                    {
                        points.Add(new Vector2d((double)pts[j][0], (double)pts[j][1]));
                    }
                }

                analysisPoints = points;


                List<CPlan.Geometry.Line2D> lines = new List<CPlan.Geometry.Line2D>();
*/
                // int scID = 71; // 53; // 20;
                // fetch scenario 

                    /*                JObject scenario = cl.sendAction2Lucy("{'action':'get_scenario','ScID':" + Settings.ScenarioID + "}");


                if (scenario != null)
                {
                    buildings = new List<Polygon>();
                    buildingHeights = new List<double>();

                    LucyGeoJsonSuite.LucyGeoJsonReader reader = new LucyGeoJsonSuite.LucyGeoJsonReader();

                    JObject scInput = (JObject)scenario.Value<JToken>("result").Value<JToken>("geometry").Value<JToken>("GeoJSON").Value<JToken>("geometry");

                    int delta = 1;

                    try
                    {
                        var strjsonHeights = scInput.Value<JToken>("features").Last().Value<JObject>("geometry");
                        MultiPoint heights = reader.Read<MultiPoint>(strjsonHeights.ToString());

                        int numHeights = heights.Count;
                        for (int h = 0; h < numHeights; h++)
                        {
                            buildingHeights.Add(heights[h].Coordinate.Y);
                        }
                        delta = 2;
                    }
                    catch (Exception ex)
                    {
                    }

                    int numPoly = scInput.Value<JToken>("features").Count() - delta;

                    for (int f = 0; f < numPoly; f++)
                    {
                        var strjson = ((JToken)(scInput.Value<JToken>("features"))[f]).Value<JObject>("geometry");

                        Polygon poly = reader.Read<Polygon>(strjson.ToString());
                        buildings.Add(poly);

                    }

                    
                } */
            }
        }


        private void readLucyResults(int instanceID)
        {
            string msg = "{'action':'get_service_outputs','SObjID':" + Settings.SObjID + "}";
            JObject result = cl.sendAction2Lucy(msg);

            if (result != null)
            {
                JToken outputs = result.Value<JToken>("result").Value<JToken>("outputs");
                if (outputs != null)
                {
                    JArray radiationList = outputs.Value<JArray>("RadiationList");
                    //solarResults = new double[radiationList.Count];
                    solarResults.Clear();
                    for (int r = 0; r < radiationList.Count; r++)
                    {
                      //  solarResults.Add(r);
                        solarResults.Add((double)radiationList[r] * 1000);
                        //solarResults.Add((double)radiationList[r]+100);
                    }
      /*              
                    List<double> bla = new List<double>();
                    for (int r = 0; r < radiationList.Count; r++)
                    {
                        bla.Add(r);
                    }
                    solarResults = bla;*/
                    
                }
            }
        }

        public void AnalyseLucyResults(string output)
        {
            NetTopologySuite.IO.GeoJsonReader GeoJSONReader = new NetTopologySuite.IO.GeoJsonReader();
            LucySerializers.IsovistResultsWrapper iso_results_wrapper = GeoJSONReader.Read<LucySerializers.IsovistResultsWrapper>(output);
//            Results = iso_results_wrapper.Results;
//            FillArrays();
        }

    }
}
