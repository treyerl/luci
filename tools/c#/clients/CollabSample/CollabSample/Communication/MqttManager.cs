﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading;
using System.Windows;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;

namespace collact.go.Communication
{
    public class MqttManager
    {
        public static string mqtt_host = "localhost";
        private static MqttManager instance = new MqttManager();

        public static MqttManager getInstance()
        {
            if (instance == null)
                instance = new MqttManager();

            return instance;
        }

        private MqttClient AppMQTTClient;
        private string clientName;


        private MqttManager()
        {

            initMqtt();

            //AppMQTTClient.MqttMsgPublishReceived += mqttClient_MqttMsgPublishReceived;

        }

        private PhysicalAddress GetMacAddress() {
            String sMacAddress = string.Empty;
            PhysicalAddress adr = null;

            foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
            {
                if (sMacAddress == String.Empty)
                {
                    IPInterfaceProperties properties = nic.GetIPProperties();
                    adr = nic.GetPhysicalAddress();
                    sMacAddress = nic.GetPhysicalAddress().ToString();
                }
            }

            return adr;
        }

        private void initMqtt()
        {
            clientName = GetMacAddress().ToString();
 /*
            AppMQTTClient = new MqttClient(System.Net.IPAddress.Parse(mqtt_host));
            AppMQTTClient.Connect("collabSample." + clientName);
            AppMQTTClient.Subscribe(new string[] { "upload2public", "deleteFromPublic", "alarmSMS" }, new byte[] { 1, 1, 1 });
   */         
        }

        public static void publish(string topic, MemoryStream message)
        {
            if (instance.AppMQTTClient.IsConnected)
            {
                instance.AppMQTTClient.Publish(topic, message.ToArray());
            }
        }


        public static void publish(string topic, byte[] message)
        {
            if (instance.AppMQTTClient.IsConnected)
            {
                instance.AppMQTTClient.Publish(topic, message);
            }
        }

        public static void publish(string topic, string message)
        {
            publish(topic, GetBytes(message));
        }

        public static void Subscribe(string topic)
        {
            if (instance.AppMQTTClient.IsConnected)
            {
                instance.AppMQTTClient.Subscribe(new string[] { topic }, new byte[] { 1 });
            }
        }

        public MqttClient GetMQTTClient()
        {
            AppMQTTClient = new MqttClient(System.Net.IPAddress.Parse(mqtt_host));
            AppMQTTClient.Connect("collabSample." + clientName);
            //AppMQTTClient.Subscribe(new string[] { "upload2public", "deleteFromPublic", "alarmSMS" }, new byte[] { 1, 1, 1 });

            return instance.AppMQTTClient;
        }

        public static byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

        public static string GetString(byte[] bytes)
        {
            char[] chars = new char[bytes.Length / sizeof(char)];
            System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
            return new string(chars);
        }

        void mqttClient_MqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e)
        {
            if (e.Topic == "deleteFromPublic")
            {
                // delete data from database
                string msg = Encoding.UTF8.GetString(e.Message);

/*                DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(CommonExchangeData));

                CommonExchangeData cEx = (CommonExchangeData)ser.ReadObject(new MemoryStream(e.Message));

                if (cEx.owner != myDevice)
                {
                    bool isPrivate = false;
                    collact.go.Model.DataAccessor.DefaultRepository.Remove_Exchange_Data(cEx, ref isPrivate);

                    // remove all openApps containing data as context
                    collact.go.Apps.AppManager.removeOpenedApp(cEx);


                    Dictionary<string, object> objInfo = new Dictionary<string, object>();
                    objInfo.Add("context", cEx);
                    toolkit.Notification.NotificationBroker.Instance.PostNotification("DeleteObject", this, objInfo);
                }
            }
            else if (e.Topic == "upload2public")
            {
                // write data into database
                string msg = Encoding.UTF8.GetString(e.Message);

                DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(FileExchange));

                FileExchange fEx = (FileExchange)ser.ReadObject(new MemoryStream(e.Message));

                if (fEx.sender == myDevice)
                {
                    collact.go.Model.DataAccessor.DefaultRepository.Update_Exchange_Data(fEx.id, fEx.id);
                }
                else
                {
                    collact.go.Model.DataAccessor.DefaultRepository.Write_File_Data(fEx, false);
                }
            }
            else if (e.Topic == "alarmSMS")
            {
                string msg = Encoding.UTF8.GetString(e.Message);

                DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(AlarmExchangeFormat));

                AlarmExchangeFormat alarm = (AlarmExchangeFormat)ser.ReadObject(new MemoryStream(e.Message));

                Dictionary<String, Object> alarmInfo = new Dictionary<string, object>();
                alarmInfo.Add("alarmData", alarm);

                NotificationBroker.Instance.PostNotification("newAlarmTicket", this, alarmInfo);
*/            }
        }

    }
}
