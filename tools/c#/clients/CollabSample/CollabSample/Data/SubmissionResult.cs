﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;

namespace CollabSample.Data
{
    public class SubmissionResult
    {
        public Rect rect { get; set; }
        public SolidColorBrush brush { get; set; }
    }
}
