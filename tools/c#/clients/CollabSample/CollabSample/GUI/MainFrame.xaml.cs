﻿using CollabSample.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CollabSample.GUI
{
    /// <summary>
    /// Interaction logic for MainFrame.xaml
    /// </summary>
    public partial class MainFrame : UserControl
    {
        #region ViewModel dependency property

        public static DependencyProperty ViewModelProperty =
            DependencyProperty.Register(
                "ViewModel",
                typeof(GeoVM),
                typeof(MainFrame),
                null
            );

        public GeoVM ViewModel
        {
            get { return (GeoVM)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }
        #endregion


        public MainFrame()
        {
            InitializeComponent();

            Loaded += MainFrame_Loaded;
        }

        void MainFrame_Loaded(object sender, RoutedEventArgs e)
        {
            ViewModel = new GeoVM();
            DataContext = ViewModel;

            ViewModel.configSettings.Add(0, new CollabConfigSettings() { gridVM = grid0.VM, scenarioID = -1, objID = -1 });
            ViewModel.configSettings.Add(1, new CollabConfigSettings() { gridVM = grid1.VM, scenarioID = -1, objID = -1 });
            ViewModel.configSettings.Add(2, new CollabConfigSettings() { gridVM = grid2.VM, scenarioID = -1, objID = -1 });
            ViewModel.configSettings.Add(3, new CollabConfigSettings() { gridVM = grid3.VM, scenarioID = -1, objID = -1 });

            ViewModel.configSettings.Add(13, new CollabConfigSettings() { gridVM = null, scenarioID = -1, objID = -1 });
        }

        private void chk0_Checked(object sender, RoutedEventArgs e)
        {
            grid0.VM.IsSelected = true;
        }

        private void chk0_Unchecked(object sender, RoutedEventArgs e)
        {
            grid0.VM.IsSelected = false;
        }

        private void chk1_Checked(object sender, RoutedEventArgs e)
        {
            grid1.VM.IsSelected = true;
        }

        private void chk1_Unchecked(object sender, RoutedEventArgs e)
        {
            grid1.VM.IsSelected = false;
        }

        private void chk2_Checked(object sender, RoutedEventArgs e)
        {
            grid2.VM.IsSelected = true;
        }

        private void chk2_Unchecked(object sender, RoutedEventArgs e)
        {
            grid2.VM.IsSelected = false;
        }

        private void chk3_Checked(object sender, RoutedEventArgs e)
        {
            grid3.VM.IsSelected = true;
        }

        private void chk3_Unchecked(object sender, RoutedEventArgs e)
        {
            grid3.VM.IsSelected = false;
        }
    }
}
