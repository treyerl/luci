﻿using CollabSample.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CollabSample.GUI
{
    /// <summary>
    /// Interaction logic for UIFileServices.xaml
    /// </summary>
    public partial class UIFileServices : UserControl
    {
        #region ViewModel dependency property

        public static DependencyProperty ViewModelProperty =
            DependencyProperty.Register(
                "ViewModel",
                typeof(GeoVM),
                typeof(UIFileServices),
                null
            );

        public GeoVM ViewModel
        {
            get { return (GeoVM)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }
        #endregion

        #region DataVM dependency property
        public static DependencyProperty DataVMProperty =
            DependencyProperty.Register(
                "DataVM",
                typeof(FileServiceVM),
                typeof(UIFileServices),
                null
            );

        public FileServiceVM DataVM
        {
            get { return (FileServiceVM)GetValue(DataVMProperty); }
            set { SetValue(DataVMProperty, value); }
        }
        #endregion


        public UIFileServices()
        {
            InitializeComponent();

            Loaded += UIFileServices_Loaded;
        }

        void UIFileServices_Loaded(object sender, RoutedEventArgs e)
        {
            ViewModel = DataContext as GeoVM;
            DataVM = new FileServiceVM();
            ViewModel.EcopotVM = DataVM;
        }
    }
}
