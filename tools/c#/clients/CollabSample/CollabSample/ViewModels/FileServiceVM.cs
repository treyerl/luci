﻿using CollabSample.Data;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.ViewModel;
using Microsoft.Win32;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace CollabSample.ViewModels
{
    public class FileServiceVM : NotificationObject
    {
        public double potentialStrength { get; set; }
        public double resolution { get; set; }

        private String _filenameResidents;
        public String filenameResidents
        {
            get { return _filenameResidents; }
            set
            {
                _filenameResidents = value;
                RaisePropertyChanged("filenameResidents");
            }
        }
 
        private String _filenameWorkplaces;
        public String filenameWorkplaces
        {
            get { return _filenameWorkplaces; }
            set
            {
                _filenameWorkplaces = value;
                RaisePropertyChanged("filenameWorkplaces");
            }
        }

        private ObservableCollection<SubmissionResult> _residentsData;
        public ObservableCollection<SubmissionResult> ResidentsData
        {
            get { return _residentsData; }
            set
            {
                _residentsData = value;
                RaisePropertyChanged("ResidentsData");
            }
        }

        private ObservableCollection<SubmissionResult> _workplacesData;
        public ObservableCollection<SubmissionResult> WorkplacesData
        {
            get { return _workplacesData; }
            set
            {
                _workplacesData = value;
                RaisePropertyChanged("WorkplacesData");
            }
        }

        private ObservableCollection<SubmissionResult> _resultData;
        public ObservableCollection<SubmissionResult> ResultData
        {
            get { return _resultData; }
            set
            {
                _resultData = value;
                RaisePropertyChanged("ResultData");
            }
        }

        // Commands
        public ICommand OpenResidentsFileCommand { get; private set; }
        public ICommand OpenWorkplacesFileCommand { get; private set; }

        public FileServiceVM()
        {
            potentialStrength = 1.0;
            resolution = 50;

            ResidentsData = new ObservableCollection<SubmissionResult>();
            WorkplacesData = new ObservableCollection<SubmissionResult>();
            ResultData = new ObservableCollection<SubmissionResult>();
            
            OpenResidentsFileCommand = new DelegateCommand(OnOpenResidentsFileExecuted);
            OpenWorkplacesFileCommand = new DelegateCommand(OnOpenWorkplacesFileExecuted);
/*
            ResidentsData.Add(new SubmissionResult() { rect = new System.Windows.Rect(0, 0, 20, 30), brush = new System.Windows.Media.SolidColorBrush(Colors.Yellow) });
            ResidentsData.Add(new SubmissionResult() { rect = new System.Windows.Rect(40,40, 50, 10), brush = new System.Windows.Media.SolidColorBrush(Colors.Green) });
            ResidentsData.Add(new SubmissionResult() { rect = new System.Windows.Rect(100, 100, 30, 30), brush = new System.Windows.Media.SolidColorBrush(Colors.Blue) });
  
 */
        }

        private void OnOpenResidentsFileExecuted()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                try
                {
                    filenameResidents = openFileDialog.FileName;
                    OnOpenFile(filenameResidents, ResidentsData);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error at opening File '" + filenameResidents + "': \r\n" + ex.Message);
                }
            }
        }

        private void OnOpenWorkplacesFileExecuted()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                try
                {
                    filenameWorkplaces = openFileDialog.FileName;
                    OnOpenFile(filenameWorkplaces, WorkplacesData);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error at opening File '" + filenameWorkplaces + "': \r\n" + ex.Message);
                }
            }
        }

        public void OnRunEcopot()
        {

        }


        public void ReadEcopotResults(int resolution, JArray data, double maxi)
        {
            double min = 0;
           // double max = data.Max();

            int pixel = 10;
            int pixel2 = 4;

            int c = 0;

            double max = 0;
            for (int i = 0; i < data.Count(); i++)
                for (int j=0; j< data[i].Count(); j++)
                    max = Math.Max(max, (double)data[i][j]);

            for (int a = 0; a < resolution; a++)
            {
                for (int r = 0; r < resolution; r++)
                {
                    double val = ((double)data[a][r] < 0) ? 0 : (double)data[a][r];

                    //int pos = c * resolution + r;
                    //double val = ((double)data[pos] < 0) ? 0 : (double)data[pos];
                    Color col = toGradient(val, 0, max, 100, 255, 0, 255, 100, 255);
                    ResultData.Add(new SubmissionResult() { rect = new System.Windows.Rect(pixel * c, pixel2 * (resolution - r - 1), pixel, pixel2), brush = new System.Windows.Media.SolidColorBrush(col) });
                }
                c++;
            }

        }

        private void oldfunc() {
            List<double>[] residentsList = ReadCSVFile("C:\\tmp\\lucy\\homesAgg.csv", 3);

            double xMin = residentsList[0].Min();
            double xMax = residentsList[0].Max();

            double yMin = residentsList[1].Min();
            double yMax = residentsList[1].Max();

            double vMin = residentsList[2].Min();
            double vMax = residentsList[2].Max();

            int cols = 100;
            int rows = 100;
            int count = Math.Min(residentsList[0].Count(), residentsList[1].Count());

            double [][] rasteredData = new double[rows][];
            for (int i = 0; i < rows; i++)
            {
                rasteredData[i] = new double[cols];
            }

            double deltaX = (xMax - xMin) / (cols-1);
            double deltaY = (yMax - yMin) / (rows-1);

            for (int n = 0; n < count; n++)
            {
                int x = (int)Math.Ceiling((residentsList[0][n] - xMin) / deltaX);
                int y = rows - 1 - (int)Math.Ceiling((residentsList[1][n] - yMin) / deltaY);

                rasteredData[x][y] += residentsList[2][n];
            }

            double rMin = rasteredData[0].Min();
            double rMax = rasteredData[0].Max();

            for (int r = 1; r < rows; r++)
            {
                rMin = Math.Min(rMin, rasteredData[r].Min());
                rMax = Math.Max(rMax, rasteredData[r].Max());
            }

            int pixel = 5;
            int pixel2 = 2;
            rMin = 0;

            for (int r = 0; r < rows; r++)
            {
                for (int c = 0; c < cols; c++)
                {
                    Color col = toGradient(rasteredData[c][r], rMin, rMax, 100, 255, 0, 255, 100, 255);
                    ResidentsData.Add(new SubmissionResult() { rect = new System.Windows.Rect(pixel * c, pixel2 * r, pixel, pixel2), brush = new System.Windows.Media.SolidColorBrush(col) });
                }
            }
        }

        private void OnOpenFile (String filename, ObservableCollection<SubmissionResult> resultColl) {
            List<double>[] residentsList = ReadCSVFile(filename, 3);

            double xMin = residentsList[0].Min();
            double xMax = residentsList[0].Max();

            double yMin = residentsList[1].Min();
            double yMax = residentsList[1].Max();

            double vMin = residentsList[2].Min();
            double vMax = residentsList[2].Max();

            int cols = 100;
            int rows = 40;
            int count = Math.Min(residentsList[0].Count(), residentsList[1].Count());

            double[][] rasteredData = new double[cols][];
            for (int i = 0; i < cols; i++)
            {
                rasteredData[i] = new double[rows];
            }

            double deltaX = (xMax - xMin) / (cols);
            double deltaY = (yMax - yMin) / (rows);

            for (int n = 0; n < count; n++)
            {
                int x = (int)Math.Ceiling((residentsList[0][n] - xMin) / deltaX) - 1;
                int y = rows - 1 - (int)Math.Ceiling((residentsList[1][n] - yMin) / deltaY);

                x = (x < 0) ? 0 : x;
                y = (y < 0) ? 0 : y;

                rasteredData[x][y] += residentsList[2][n];
            }

            double rMin = rasteredData[0].Min();
            double rMax = rasteredData[0].Max();

            for (int r = 1; r < rows; r++)
            {
                rMin = Math.Min(rMin, rasteredData[r].Min());
                rMax = Math.Max(rMax, rasteredData[r].Max());
            }

            int pixel = 4; //10
            int pixel2 = 4;
            rMin = 0;

            for (int r = 0; r < rows; r++)
            {
                for (int c = 0; c < cols; c++)
                {
                    Color col = toGradient(rasteredData[c][r], rMin, rMax, 100, 255, 0, 255, 100, 255);
//                    ResidentsData.Add(new SubmissionResult() { rect = new System.Windows.Rect(pixel * c, pixel2 * r, pixel, pixel2), brush = new System.Windows.Media.SolidColorBrush(col) });
                    resultColl.Add(new SubmissionResult() { rect = new System.Windows.Rect(pixel * c, pixel2 * r, pixel, pixel2), brush = new System.Windows.Media.SolidColorBrush(col) });
                }
            }
        }

        private Color toGradient(double value, double minValue, double maxValue, double rMin, double rMax, double gMin, double gMax, double bMin, double bMax)
        {
            double meanValue = Math.Log10( 0.5 * (2 +maxValue - minValue));

            value = Math.Log10(value+ 1);
            maxValue = Math.Log10(maxValue + 1);
            minValue = Math.Log10(minValue + 1);



//            double meanValue = 0.5 * (maxValue - minValue);

            double r = (value < meanValue ? 0 : ((value - minValue) / (maxValue - minValue)) * 255);
            double g = (value < meanValue ? ((value - minValue) / (maxValue - minValue)) * 255 : ((minValue - value) / (maxValue - minValue)) * 255);
            double b = (value < meanValue ? ((minValue - value) / (maxValue - minValue)) * 255 : 0);
            

            Color color = Color.FromArgb(255, (byte)r, (byte)g, (byte)b);
            return color;
        }

        private List<double>[] ReadCSVFile(String filename, int numCols)
        {
            List<double>[] result = new List<double>[numCols];
            
            for (int c=0; c<numCols; c++)
                result[c] = new List<double>();

            using (var rd = new StreamReader(filename))
            {
                while (!rd.EndOfStream)
                {
                    var splits = rd.ReadLine().Split(';');

                    for (int i = 0; i < numCols; i++)
                    {
                        double buffer = double.NaN;
                        if (splits.Count() > i)
                            double.TryParse(splits[i], out buffer);

                        result[i].Add(buffer);
                    }
                }
            }

            return result;
        }
            
    }
}
