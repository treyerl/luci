﻿using CollabSample.Data;
using collact.go.Communication;
using CPlan.Isovist2D;
using GeoAPI.Geometries;
using LuciCommunication;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.ViewModel;
using NetTopologySuite.CoordinateSystems;
using NetTopologySuite.Features;
using NetTopologySuite.Geometries;
using NetTopologySuite.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OpenTK;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
//using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows.Threading;
using uPLibrary.Networking.M2Mqtt;

namespace CollabSample.ViewModels
{
    public enum ServiceType
    {
        HeatEmission,
        Isovist,
        EcoPot,
        StressTest
    }

    public class CollabConfigSettings
    {
        public int scenarioID;
        public string mqtt_topic;
        public int objID;
        public VisualizationGridVM gridVM;
        public ServiceType service_type;
    }


    public class GeoVM : NotificationObject
    {

//x        public ObservableCollection<System.Windows.Rect> Buildings { get; set; }

//x        private ObservableCollection<SubmissionResult> _submissions;
   //     public ObservableCollection<SubmissionResult> Submissions { get; set; }
 /*       public ObservableCollection<SubmissionResult> Submissions//{ get; set; }
        {
            get { return _submissions; }
            set
            {
                _submissions = value;
                RaisePropertyChanged("Submissions");
            }
        }
        */
        private int curScenarioID = -1;

        private bool waiting4Results = false;
        private bool receivedResult = false;

        public FileServiceVM EcopotVM;
        public StressTestVM StressTestVM;

        public String InfoMW { get; set; }

        public String ServerIP { get; set; }
        public String User { get; set; }
        public String PW { get; set; }

        public IEnumerable<JToken> ActionsListContent { get; set; }
        public IEnumerable<JToken> ServicesListContent { get; set; }

        public String ActionDetailsContent { get; set; }
        public String ServiceDetailsContent { get; set; }

        private  StreamReader OutputFromLucy;
        private DispatcherTimer timer;

        private LuciCommunication.Communication2Luci cl = null;

        public Dictionary<int, CollabConfigSettings> configSettings = new Dictionary<int, CollabConfigSettings>();
        public Dictionary<int, CollabConfigSettings> stresstestSettings = new Dictionary<int, CollabConfigSettings>();
        private MqttClient mqttClient;

        private string _host = null;

        public String SelectedAction
        {
            get { return _selectedAction; }
            set
            {
                if (value == _selectedAction)
                    return;

                _selectedAction = value;

                RaisePropertyChanged("SelectedAction");

                // selection changed - do something special
                ShowDetails4SelectedAction(_selectedAction);
            }
        }
        private String _selectedAction;

        public Communication2Luci getLuciHub()
        {
            return cl;
        }

        public String SelectedService
        {
            get { return _selectedService; }
            set
            {
                if (value == _selectedService)
                    return;

                _selectedService = value;

                RaisePropertyChanged("SelectedService");

                // selection changed - do something special
                ShowDetails4SelectedService(_selectedService);
            }
        }
        private String _selectedService;

        // commands
        public ICommand CreateNewGeoCommand { get; private set; }
        public ICommand CreateScenarioCommand { get; private set; }
        public ICommand UpdateScenarioCommand { get; private set; }
        public DelegateCommand<String> Connect2LucyCommand { get; private set; }
        public DelegateCommand Connect2LucyServerCommand { get; private set; }
        public ICommand Login2LucyCommand { get; private set; }
        public ICommand GetActionsListCommand { get; private set; }
        public ICommand GetServicesListCommand { get; private set; }
        public DelegateCommand<String> RunServiceCommand { get; private set; }
        public ICommand RunIsovistServiceCommand { get; private set; }
        public ICommand RunImageServiceCommand { get; private set; }
        public ICommand RunEcoPotServiceCommand { get; private set; }
        public DelegateCommand<String> SendMessageToMWCommand { get; private set; }


        public GeoVM() {
//            ObservableCollection<System.Windows.Rect> newB = new ObservableCollection<System.Windows.Rect>();
  //          AllBuildings.Add(0, newB);
            
//            Buildings = new ObservableCollection<System.Windows.Rect>();
//x            Submissions = new ObservableCollection<SubmissionResult>();
    //        Submissions = new ObservableCollection<SubmissionResult>();
  //          initBuildings(35, 20, 4, 4);


            InfoMW = "Started\n";
            ServerIP = "129.132.6.60";
           
            CreateNewGeoCommand = new DelegateCommand(OnCreateNewGeoExecuted);
            CreateScenarioCommand = new DelegateCommand(OnCreateScenarioExecuted);
            UpdateScenarioCommand = new DelegateCommand(OnUpdateScenarioExecuted);
            Connect2LucyCommand = new DelegateCommand<String>(OnConnect2LucyExecuted);
            Connect2LucyServerCommand = new DelegateCommand(OnConnect2LucyServerExecuted);
            Login2LucyCommand = new DelegateCommand<object>(OnLogin2LucyExecuted);
            GetActionsListCommand = new DelegateCommand(OnGetActionsListExecuted);
            GetServicesListCommand = new DelegateCommand(OnGetServicesListExecuted);
            RunServiceCommand = new DelegateCommand<String>(OnRunServiceExecuted);
            RunIsovistServiceCommand = new DelegateCommand(OnRunIsovistServiceExecuted);
            RunImageServiceCommand = new DelegateCommand(OnRunImageServiceExecuted);
            RunEcoPotServiceCommand = new DelegateCommand(OnRunEcoPotServiceExecuted);
            SendMessageToMWCommand = new DelegateCommand<String>(OnSendMessageToMWCommandExecuted);

        }

        public void init()
        {
            OnCreateScenarioExecuted();
        }

        /*
        private void initBuildings(int numCols, int numRows, int numX, int numY)
        {
            Random num = new Random();

            for (int c = 0; c < numY; c++)
            {
                for (int r = 0; r < numX; r++)
                {
                    int valX = 10 * (num.Next(0, numCols) / 10);
                    int valY = 10 * (num.Next(0, numRows) / 10);
                    int width = 10 * (num.Next(20, 100 - valX) / 10);
                    int height = 10 * (num.Next(20, 50 - valY) / 10);

                    System.Windows.Rect Rect = new System.Windows.Rect(100 * c + valX, 50 * r + valY, width, height);

                    //xx AllBuildings[0].Add(Rect);
                    Buildings.Add(Rect);
                }
            }
        }


        private void initBuildings()
        {
            
            System.Windows.Rect r1 = new System.Windows.Rect(10, 10, 25, 80);
            System.Windows.Rect r2 = new System.Windows.Rect(50, 50, 100, 20);
          
            
            //Buildings.Add(r1);
            //Buildings.Add(r2);

            Random num = new Random();

            for (int c = 0; c < 8; c++)
            {
                for (int r = 0; r < 7; r++)
                {
                    int valX = 10 * (num.Next(0, 80)/10);
                    int valY = 10 * (num.Next(0, 50)/10);
                    int width = 10*(num.Next(20, 100 - valX)/10);
                    int height = 10*(num.Next(20, 70 - valY)/10);

                    System.Windows.Rect Rect = new System.Windows.Rect(100*c + valX, 70*r + valY, width, height);

                    Buildings.Add(Rect);
                }
            }
        }
        */
        private void OnCreateNewGeoExecuted()
        {
            //xx AllBuildings[0].Clear();
//            Buildings.Clear();
//            initBuildings(35, 20, 4, 4);

            foreach (CollabConfigSettings config in configSettings.Values)
            {
                if (config.gridVM != null && config.gridVM.IsSelected)
                {
                    config.gridVM.createNewGeo();
                    //config.scenarioID = -1;
                }
            }

//*            Submissions.Clear();
//            curScenarioID = -1;

            InfoMW += "new Geo created\n";
            RaisePropertyChanged("InfoMW");
        }

        private void OnCreateScenarioExecuted() {
            if (cl == null)
            {
                System.Windows.MessageBox.Show("Please log-in to lucy first");
                return;
            }

            for ( int i=0; i<configSettings.Count(); i++)
            {
                if (configSettings.ElementAt(i).Value.gridVM != null && configSettings.ElementAt(i).Value.gridVM.IsSelected)
                {
                    int scid = configSettings.ElementAt(i).Value.scenarioID;
                        
                    OnCreateScenario4Index(configSettings.ElementAt(i).Value.gridVM.BBB);
                    configSettings.ElementAt(i).Value.scenarioID = curScenarioID;

                    if (scid < 0)
                    {
                        OnCreateHeatEmissionService4Index(i, curScenarioID, null);
                    }
                    else
                    {
                        // update service input
                        
                        OnUpdateScenarioID4Index(i, configSettings[i].gridVM.SObjID, curScenarioID);
                        
                        //OnUpdateScenario4Index(scid, configSettings.ElementAt(i).Value.gridVM.BBB);
                    }
                }
            }
        }

        private void OnUpdateScenarioExecuted()
        {
            if (cl == null)
            {
                System.Windows.MessageBox.Show("Please log-in to lucy first");
                return;
            }

            for (int i = 0; i < configSettings.Count(); i++)
            {
                if (configSettings.ElementAt(i).Value.gridVM != null && configSettings.ElementAt(i).Value.gridVM.IsSelected)
                {
                    int scid = configSettings.ElementAt(i).Value.scenarioID;
                    if (scid < 0)
                    {
                        OnCreateScenario4Index(configSettings.ElementAt(i).Value.gridVM.BBB);
                        configSettings.ElementAt(i).Value.scenarioID = curScenarioID;

                        OnCreateHeatEmissionService4Index(i, curScenarioID, null);
                    }
                    else
                    {
                        OnUpdateScenario4Index(scid, configSettings.ElementAt(i).Value.gridVM.BBB);
                    }
                }
            }
        }


        private void OnCreateScenario4Index(ObservableCollection<System.Windows.Rect> buildings)
        {
            GeoJsonWriter GeoJSONWriter = new GeoJsonWriter();
            int index = 0;

            NetTopologySuite.Geometries.Polygon[] polyArray = new NetTopologySuite.Geometries.Polygon[buildings.Count];
            double OffsetX = 0;
            double OffsetY = 0;
            foreach (System.Windows.Rect r in buildings)
            {
                LinearRing r1 = new NetTopologySuite.Geometries.LinearRing(new Coordinate[]
                    {
                    new Coordinate(OffsetX + r.Bottom, OffsetY + r.Left),
                    new Coordinate(OffsetX + r.Bottom, OffsetY + r.Right),
                    new Coordinate(OffsetX + r.Top, OffsetY + r.Right),
                    new Coordinate(OffsetX + r.Top, OffsetY + r.Left),
                    new Coordinate(OffsetX + r.Bottom, OffsetY + r.Left)
                    }
                );
                NetTopologySuite.Geometries.Polygon p = new NetTopologySuite.Geometries.Polygon(r1);
                polyArray[index] = p;
                index++;
            }
            MultiPolygon mPoly = new MultiPolygon(polyArray);
            string geoJSONPoly = GeoJSONWriter.Write(mPoly);
            geoJSONPoly = geoJSONPoly.Substring(0, geoJSONPoly.Length - 1);
//            string createScenario4Geo = "{'action':'create_scenario','name':'test','geometry':{'GeoJSON':{'format':'GeoJSON','value':" + geoJSONPoly + "}}}}}";
            string createScenario4Geo = "{'action':'create_scenario','name':'test','geometry':{'GeoJSON':{'format':'GeoJSON','geometry':" + geoJSONPoly + "}}}}}";


//            string createScenario4Geo = "{'action':'create_scenario','name':'test','geometry':{'format':'GeoJSON','value':" + geoJSONPoly + "}}}}";
            AddText2InfoMW("sent to lucy: " + createScenario4Geo + "\n");

            /* TODO_NewLuci
            JObject scenarioObj = cl.sendAction2Lucy(createScenario4Geo);

            if (scenarioObj != null)
            {
                JProperty result = ((JProperty)(scenarioObj.First));
                if (result != null)
                {
                    AddText2InfoMW(result.Name + ": " + result.Value + "\n");

                    if (result.Value["ScID"] != null)
                        curScenarioID = (int)result.Value["ScID"];
                }
            }*/
        }


        private void OnCreateHeatEmissionService4Index(int gridID, int scenarioID, String input)
        {
            double userInput;
            if (input != null && input.Length > 0)
            {
                Double.TryParse(input, out userInput);
            }
            else
            {
                userInput = 1.23456;
            }

            string msg = "{'action':'create_service','inputs':{'inputNumber':" + userInput + "},'classname':'HeatEmission','ScID':" + scenarioID + "}";

            if (cl == null)
            {
                InfoNoConnection();
            }
            else
            {
                /* TODO_NewLuci
                ConfigSettings lucyResult = cl.sendCreateService2Lucy(msg, scenarioID);
                CollabConfigSettings configInfo = new CollabConfigSettings() { mqtt_topic = lucyResult.mqtt_topic, objID = lucyResult.objID, scenarioID = lucyResult.scenarioID };
                if (configInfo.mqtt_topic != null)
                {
                    configSettings[gridID].gridVM.SObjID = configInfo.objID;
                    configInfo.gridVM = configSettings[gridID].gridVM;
                   // configSettings[gridID].service_type = ServiceType.HeatEmission;
                    configSettings[gridID] = configInfo;

                    if (mqttClient.IsConnected)
                    {
                        string wildcardTopic = configInfo.mqtt_topic.Replace("/" + scenarioID + "/", "/+/");
                        mqttClient.Subscribe(new string[] { wildcardTopic }, new byte[] { 1 });
                        //mqttClient.Subscribe(new string[] { configInfo.mqtt_topic }, new byte[] { 1 });
                    }
                }
                else
                    AddText2InfoMW("something went wrong ... \n");
                 * */
            }

        }

        private void OnUpdateScenarioID4Index(int gridID, int sObjID, int scid)
        {
            string updateScenarioID4Geo = "{'action':'update_service_scenario', 'SObjID':" + sObjID + ", 'ScID':" + scid + "}";


            AddText2InfoMW("sent to lucy: " + updateScenarioID4Geo + "\n");

            /* TODO_NewLuci            
                        JObject scenarioObj = cl.sendAction2Lucy(updateScenarioID4Geo);

                        if (scenarioObj != null)
                        {
                            JProperty result = ((JProperty)(scenarioObj.First));
                            if (result != null)
                            {
                                AddText2InfoMW(result.Name + ": " + result.Value + "\n");

                                var newScID = result.Value["newScID"];
                                var oldScID = result.Value["oldScID"];

                                if (oldScID != null && newScID != null)
                                {
                                    configSettings[gridID].mqtt_topic = configSettings[gridID].mqtt_topic.Replace("/" + oldScID.ToString() + "/", "/" + newScID.ToString() + "/");
                                }


                                //if (result.Value["ScID"] != null)
                                  //  curScenarioID = (int)result.Value["ScID"];
                            }
                        }
             */
                    }

                    private void OnUpdateScenario4Index(int scid, ObservableCollection<System.Windows.Rect> buildings)
                    {
                        string geoJSONPoly = buildGeoJsonScenario(buildings);
                        string updateScenario4Geo = "{'action':'update_scenario','ScID':" + scid + ",'geometry':{'GeoJSON':{'format':'GeoJSON','geometry':" + geoJSONPoly + "}}}}}";


                        //            string createScenario4Geo = "{'action':'create_scenario','name':'test','geometry':{'format':'GeoJSON','value':" + geoJSONPoly + "}}}}";
                        AddText2InfoMW("sent to lucy: " + updateScenario4Geo + "\n");

                        /* TODO_NewLuci
                        JObject scenarioObj = cl.sendAction2Lucy(updateScenario4Geo);

                        if (scenarioObj != null)
                        {
                            JProperty result = ((JProperty)(scenarioObj.First));
                            if (result != null)
                            {
                                AddText2InfoMW(result.Name + ": " + result.Value + "\n");

                                if (result.Value["ScID"] != null)
                                    curScenarioID = (int)result.Value["ScID"];
                            }
                        }
                        */
                    }

                    private string buildGeoJsonScenario(ObservableCollection<System.Windows.Rect> buildings)
                    {
                        GeoJsonWriter GeoJSONWriter = new GeoJsonWriter();
                        int index = 0;

                        NetTopologySuite.Geometries.Polygon[] polyArray = new NetTopologySuite.Geometries.Polygon[buildings.Count];
                        double OffsetX = 0;
                        double OffsetY = 0;
                        foreach (System.Windows.Rect r in buildings)
                        {
                            LinearRing r1 = new NetTopologySuite.Geometries.LinearRing(new Coordinate[]
                                {
                                new Coordinate(OffsetX + r.Bottom, OffsetY + r.Left),
                                new Coordinate(OffsetX + r.Bottom, OffsetY + r.Right),
                                new Coordinate(OffsetX + r.Top, OffsetY + r.Right),
                                new Coordinate(OffsetX + r.Top, OffsetY + r.Left),
                                new Coordinate(OffsetX + r.Bottom, OffsetY + r.Left)
                                }
                            );
                            NetTopologySuite.Geometries.Polygon p = new NetTopologySuite.Geometries.Polygon(r1);
                            polyArray[index] = p;
                            index++;
                        }
                        MultiPolygon mPoly = new MultiPolygon(polyArray);
                        string geoJSONPoly = GeoJSONWriter.Write(mPoly);
                        geoJSONPoly = geoJSONPoly.Substring(0, geoJSONPoly.Length - 1);

                        return geoJSONPoly;
                    }


                    public int createScenario4StressTest(UILine[] allLines)
                    {
                        if (allLines == null)
                            return -1;

                        GeoJsonWriter GeoJSONWriter = new GeoJsonWriter();
                        int scenarioID = -1;
                        int numLines = allLines.Count();

                        Random r = new Random();

                        ILineString[] lines = new ILineString[numLines];

                        for (int i=0; i< numLines; i++) {
                            Coordinate[] coords = new Coordinate[2];
                            coords[0] = new Coordinate(allLines[i].X1, allLines[i].Y1);
                            coords[1] = new Coordinate(allLines[i].X2, allLines[i].Y2);

                            lines[i] = new LineString(coords);
                        }

                        NetTopologySuite.Geometries.MultiLineString multiLines = new NetTopologySuite.Geometries.MultiLineString(lines);

                        string geoJSONLines = GeoJSONWriter.Write(multiLines);
                        string createScenario4Geo = "{'action':'create_scenario','name':'stresstest','geometry':{'GeoJSON':{'format':'GeoJSON','geometry':" + geoJSONLines + "}}}}}";
                        AddText2InfoMW("sent to lucy: " + createScenario4Geo + "\n");

                        LuciCommunication.Communication2Luci.LuciAnswer answer = cl.createScenario("test", multiLines, null);

                        if (answer.Outputs != null)
                        {
                            List<KeyValuePair<string, object>> Outputs = ((LuciCommunication.Communication2Luci.LuciAnswer)answer).Outputs;
                            var f = Outputs.Where(q => q.Key == "ScID");
                            long sL;
                            if (f != null && f.Any())
                            {
                                sL = (long)f.First().Value;
                                scenarioID = (int)sL;
                            }
                            
                        }
                        /*
                        JObject scenarioObj = cl.sendAction2Lucy(createScenario4Geo);

                        if (scenarioObj != null)
                        {
                            JProperty result = ((JProperty)(scenarioObj.First));
                            if (result != null)
                            {
                                AddText2InfoMW(result.Name + ": " + result.Value + "\n");

                                if (result.Value["ScID"] != null)
                                    scenarioID = (int)result.Value["ScID"];
                            }
                        }*/

                        return scenarioID;
                    }


                    public int getInstanceID(int sobjID)
                    {
                        int id = -1;

                        LuciCommunication.Communication2Luci.LuciAnswer result = cl.getServiceOutputs(sobjID);

                        var s = result.Outputs.Where(q => q.Key == "instance_id");
                        if (s != null && s.Any())
                            id = (int)((long)s.First().Value);

                        return id;
                    }


                    public LuciCommunication.Communication2Luci.LuciAnswer runService(string serviceName, List<KeyValuePair<string, object>> inputs, long scId = -1)
                    {
                        return cl.runService(serviceName, inputs, scId);
                    }

        public async Task<Communication2Luci.LuciAnswer> runAsyncService(string serviceName, List<KeyValuePair<string, object>> inputs, long scId = -1, params LuciCommunication.Communication2Luci.LuciStreamData[] attachments)
        {
            Communication2Luci.LuciAnswer result = await cl.runAsyncService(serviceName, inputs, scId, attachments);
            return result;
        }

        public object getOutput (LuciCommunication.Communication2Luci.LuciAnswer luciAnswer, string outputParameter)
        {
            object result = null;

            try
            {
                if (luciAnswer.State == LuciCommunication.Communication2Luci.LuciState.Result)
                    result = cl.GetOutputValue(luciAnswer, outputParameter);
            } catch (Exception ex)
            {
                result = null;
            }

            return result;
        }

        public ConfigSettings createService(string serviceName, List<KeyValuePair<string, object>> inputs, long scId = -1)
                    {
                        ConfigSettings configInfo = null;
                        LuciCommunication.Communication2Luci.LuciAnswer? answer = cl.createService(serviceName, inputs, scId);

                        if (answer != null && ((LuciCommunication.Communication2Luci.LuciAnswer)answer).Outputs != null)
                        {
                            configInfo = new ConfigSettings();
                            List<KeyValuePair<string, object>> Outputs = ((LuciCommunication.Communication2Luci.LuciAnswer)answer).Outputs;
                            var f = Outputs.Where(q => q.Key == "mqtt_topic");
                            if (f != null && f.Any())
                                configInfo.mqtt_topic = (string)f.First().Value;

                            var s = Outputs.Where(q => q.Key == "SObjID");
                            if (s != null && s.Any())
                                configInfo.objID = (int)((long)s.First().Value);

                            
                            configInfo.scenarioID = (int)scId;
                           
                        }

                        return configInfo;
                    }

                    public ConfigSettings startService(string msg, int scenarioID)
                    {
                        ConfigSettings configInfo = null;

                        if (cl == null)
                        {
                            InfoNoConnection();
                        }
                        else
                        {
                            // TODO_NewLuci                            configInfo = cl.sendCreateService2Lucy(msg, scenarioID);
                            if (configInfo != null) 
                                AddText2InfoMW("service created with ID: " + configInfo.objID + "\n");
                        }

                        return configInfo;
                    }

                    private void OnRunServiceExecuted(String input)
                    {
                        for (int i = 0; i < configSettings.Count(); i++)
                        {
                            if (configSettings.ElementAt(i).Value.gridVM != null && configSettings.ElementAt(i).Value.gridVM.IsSelected)
                            {
            //                    OnRunServiceExecuted(i, input);
                                OnRunHeatEmissionServiceExecuted(i, input);
                            }
                        }
                    }

                    private void OnRunServiceExecuted(int gridID, String input) {
                        if (gridID < 0)
                        {
                            System.Windows.MessageBox.Show("Please select a geometry first!");
                            return;
                        }

                        int scenarioID = configSettings[gridID].scenarioID;
                        if (scenarioID == -1)
                        {
                            OnCreateScenarioExecuted();
                            scenarioID = configSettings[gridID].scenarioID;
                        }

                        double userInput;
                        if (input != null && input.Length > 0)
                        {
                            Double.TryParse(input, out userInput);
                        }
                        else
                        {
                            userInput = 1.23456;
                        }

                        string msg = "{'action':'run','service':{'inputs':{'inputNumber':" + userInput + "},'classname':'HeatEmission','ScID':'" + scenarioID + "'}}";
                        //,'ScID':'1'
                        //OnSendMessageToMWCommandExecuted(msg);

                        if (cl == null)
                        {
                            InfoNoConnection();
                        }
                        else
                        {
                            if (configSettings[gridID].gridVM.Submissions == null)
                                configSettings[gridID].gridVM.Submissions = new ObservableCollection<SubmissionResult>();
                            else
                                configSettings[gridID].gridVM.Submissions.Clear();

                            // TODO_NewLuci                            JObject result = cl.sendService2Lucy(msg);
                            JObject result = null;
                            if (result != null)
                            {
                                JToken outputs = result.Value<JToken>("outputs");
                                if (outputs != null)
                                {
                                    //JArray buffer = outputs.Value<JObject>("outputVals").Value<JArray>("value");
                                    JArray buffer = (JArray)JsonConvert.DeserializeObject(outputs.Value<JObject>("outputVals").Value<String>("value"));
                                    //int[][] buffer = JsonConvert.DeserializeObject<int[][]>(output);
                                    AddText2InfoMW("result: " + outputs + " - output rows: " + buffer + "\n");

                                    for (int c=0; c<85; c++) {
                                        for (int r=0; r<55; r++) {
                                            if ((int)buffer[c][r] > 0)
                                            {
                                                int valX = 10 * c;
                                                int valY = 10 * r;
                                                int width = 10;
                                                int height = 10;

                                                System.Windows.Rect Rect = new System.Windows.Rect(valX, valY, width, height);
                                                SolidColorBrush br = new SolidColorBrush();

                                    
                                                if ((int)buffer[c][r] >=18)
                                                    br.Color = Colors.DarkRed;
                                                else if ((int)buffer[c][r] >= 15)
                                                    br.Color = Colors.Red;
                                                else if ((int)buffer[c][r] >= 12)
                                                    br.Color = Colors.DarkOrange;
                                                else if ((int)buffer[c][r] >= 9)
                                                    br.Color = Colors.OrangeRed;
                                                else if ((int)buffer[c][r] >= 6)
                                                    br.Color = Colors.Orange;
                                                else
                                                    br.Color = Colors.Yellow;

                                                configSettings[gridID].gridVM.Submissions.Add(new SubmissionResult() { rect =Rect, brush = br });
                                            }
                                        }
                                    }
                                }
                                else 
                                    AddText2InfoMW("result: null \n");
                            }
                            else
                                AddText2InfoMW("something went wrong ... \n");
                        }

                    }

                    private void OnRunHeatEmissionServiceExecuted(int gridID, String input) {
                        if (gridID < 0)
                        {
                            System.Windows.MessageBox.Show("Please select a geometry first!");
                            return;
                        }

                        int scenarioID = configSettings[gridID].scenarioID;
                        if (scenarioID == -1)
                        {
                            OnCreateScenarioExecuted();
                            scenarioID = configSettings[gridID].scenarioID;
                        }

                        if (configSettings[gridID].mqtt_topic != null)
                        {
                            mqttClient.Publish(configSettings[gridID].mqtt_topic, Encoding.UTF8.GetBytes("RUN"));
                        }
                        else
                        {
                            double userInput;
                            if (input != null && input.Length > 0)
                            {
                                Double.TryParse(input, out userInput);
                            }
                            else
                            {
                                userInput = 1.23456;
                            }

                            string msg = "{'action':'create_service','inputs':{'inputNumber':" + userInput + "},'classname':'HeatEmission','ScID':" + scenarioID + "}";

                            if (cl == null)
                            {
                                InfoNoConnection();
                            }
                            else
                            {
                                // TODO_NewLuci                                ConfigSettings lucyResult = cl.sendCreateService2Lucy(msg, scenarioID);
                                ConfigSettings lucyResult = null;
                                CollabConfigSettings configInfo = new CollabConfigSettings() { mqtt_topic = lucyResult.mqtt_topic, objID = lucyResult.objID, scenarioID = lucyResult.scenarioID };
                                if (configInfo.mqtt_topic != null)
                                {
                                    configSettings[gridID].gridVM.SObjID = configInfo.objID;
                                    configInfo.gridVM = configSettings[gridID].gridVM;
                                    configSettings[gridID].service_type = ServiceType.HeatEmission;
                                    configSettings[gridID] = configInfo;

                                    if (mqttClient.IsConnected)
                                    {
                                        string wildcardTopic = configInfo.mqtt_topic.Replace("/" + scenarioID + "/", "/+/");
                                        mqttClient.Subscribe(new string[] { wildcardTopic }, new byte[] { 1 });
                                       // mqttClient.Subscribe(new string[] { configInfo.mqtt_topic }, new byte[] { 1 });
                                        mqttClient.Publish(configInfo.mqtt_topic, Encoding.UTF8.GetBytes("RUN"));
                                        mqttClient.Publish("bla", Encoding.UTF8.GetBytes("RUN"));

                                    }
                                }
                                else
                                    AddText2InfoMW("something went wrong ... \n");
                            }
                        }
                    }

                    private void OnRunIsovistServiceExecuted()
                    {
                        for (int i=0; i<configSettings.Count(); i++) 
                        {
                            if (configSettings.ElementAt(i).Value.gridVM != null && configSettings.ElementAt(i).Value.gridVM.IsSelected)
                            {
                                OnRunIsovistServiceExecuted(i);
                            }
                        }
                    }


                    private void OnRunIsovistServiceExecuted(int gridID)
                    {
                        int scenarioID = configSettings[gridID].scenarioID;
                        if (scenarioID == -1)
                        {
                            OnCreateScenarioExecuted();
                            scenarioID = configSettings[gridID].scenarioID;
                        }
            /*
                        if (curScenarioID == -1)
                            OnCreateScenarioExecuted();
                        */
            int numCols = 40;
            int numRows = 25;


 //          string msg = "{'action':'run','service':{'inputs':{'inputNumber':" + userInput + "},'classname':'HeatEmission','ScID':'" + curScenarioID + "'}}";


            PointCollection ptColl = new PointCollection();
            foreach (System.Windows.Rect rr in configSettings[gridID].gridVM.BBB)
            {
                for (int l = (int)rr.Left / 10; l < (int)(rr.Left + rr.Width) / 10; l++)
                {
                    for (int h = (int)rr.Top / 10; h < (int)(rr.Top + rr.Height) / 10; h++)
                    {
                        ptColl.Add(new System.Windows.Point(l, h));
                    }
                }
            }

            int num = numCols * numRows - ptColl.Count();
            Point[] points = new Point[num];

            double[][] inputData = new double[2][];
            inputData[0] = new double[num];
            inputData[1] = new double[num];
            int counter = 0;
            for (int c = 0; c < numCols; c++)
                {
                    for (int r=0; r< numRows; r++) {
                        if (!ptColl.Contains(new System.Windows.Point(c, r)))
                        {
                            inputData[0][counter] = c + 0.5;
                            inputData[1][counter] = r + 0.5;
                            points[counter] = new Point(c + 0.5, r + 0.5);
                            counter++;
                        }
                }
            }

            string json = JsonConvert.SerializeObject(inputData);

            MultiPoint gridPointsJ = new MultiPoint(points);

            GeoJsonWriter GeoJSONWriter = new GeoJsonWriter();
            string geoJSONPoints = GeoJSONWriter.Write(gridPointsJ);


            string msg = "{'action':'create_service','inputs':{'cols':" + numCols
                + ", 'rows':" + numRows + ", 'inputVals':" + json + "},'classname':'IsovistSimple','ScID':" + scenarioID + "}";

//            string msg = "{'action':'create_service','inputs':{'inputVals':" + geoJSONPoints + "},'classname':'Isovist','ScID':" + scenarioID + "}";

            if (cl == null)
            {
                InfoNoConnection();
            }
            else
            {
                // TODO_NewLuci                ConfigSettings lucyResult = cl.sendCreateService2Lucy(msg, scenarioID);
                ConfigSettings lucyResult = null;
                CollabConfigSettings configInfo = new CollabConfigSettings() { mqtt_topic = lucyResult.mqtt_topic, objID = lucyResult.objID, scenarioID = lucyResult.scenarioID, service_type = ServiceType.Isovist };
                if (configInfo.mqtt_topic != null)
                {
                    configInfo.gridVM = configSettings[gridID].gridVM;
                    //configSettings[gridID].service_type = ServiceType.Isovist;
                    configSettings[gridID] = configInfo;
/*                    configSettings[gridID].mqtt_topic = configInfo.mqtt_topic;
                    config.objID = configInfo.objID;
                    config.scenarioID = configInfo.scenarioID;
  */                  

                    if (mqttClient.IsConnected)
                    {
                        mqttClient.Subscribe(new string[] { configInfo.mqtt_topic }, new byte[] { 1 });
                        mqttClient.Publish(configInfo.mqtt_topic, Encoding.UTF8.GetBytes("RUN"));
                    }

                    //MqttManager.Subscribe(mqtt_topic);
                    //MqttManager.publish(mqtt_topic, "RUN");

                    /*
                    JToken outputs = result.Value<JToken>("outputs");
                    if (outputs != null)
                    {
                        string output = (string)((Newtonsoft.Json.Linq.JProperty)(outputs.First())).Value;
                        AddText2InfoMW("result: " + outputs + " - output rows: " + output + "\n");

                        GeoJsonReader GeoJSONReader = new GeoJsonReader();
                        IsovistField2DWrapper iso_client_wrapper_result = GeoJSONReader.Read<IsovistField2DWrapper>(output);

                        Coordinate[] coords = iso_client_wrapper_result.Points.Coordinates;
                        for (int c = 0; c < coords.Count(); c++)
                        {
                            // Rect = Pixel showing a color representing the value of visible area (= result isovist service)
                            System.Windows.Rect Rect = new System.Windows.Rect(10 * coords[c].X - 5, 10 * coords[c].Y -5, 10, 10);
                            SolidColorBrush br = new SolidColorBrush();

                            double value;
                            iso_client_wrapper_result.Results[c].TryGetValue(ResultsIsovist.Area, out value);

                            byte b = 255;
                            if (value < 1020) 
                                b = (byte)(value / 4);

                            br.Color = Color.FromArgb(180, 0, 0, b);

                            Submissions.Add(new SubmissionResult() { rect = Rect, brush = br });
                        }
                    }
                    else 
                        AddText2InfoMW("result: null \n");*/
                }
                else
                    AddText2InfoMW("something went wrong ... \n");
            }

        }

        private void OnRunImageServiceExecuted()
        {
            string filename = "giraffe.jpg";
            string msg = "{'action':'run','service':{'inputs':{'inputVals':{'format':'text','value':'" + filename + "'}},'classname':'ImageService'}}";

            if (cl == null)
            {
                InfoNoConnection();
            }
            else
            {
                // TODO_NewLuci                JObject result = cl.sendService2Lucy(msg);
                JObject result = null;
                if (result != null)
                {
                    
                    JToken outputs = result.Value<JToken>("outputs");
                    if (outputs != null)
                    {
         //               JArray buffer = outputs.Value<JObject>("outputVals").Value<JArray>("value");
                        //int[][] buffer = JsonConvert.DeserializeObject<int[][]>(output);
            //            AddText2InfoMW("result: " + outputs + " - output rows: " + buffer + "\n");
                    }
                }
            }

        }

        private void OnRunEcoPotServiceExecuted()
        {
            if (cl == null)
            {
                InfoNoConnection();
                System.Windows.MessageBox.Show("Please login to lucy first!");
            }
            else
            {
                string filepath = "c:\\tmp\\lucy\\zueri\\";
                //string filepath = "c:\\tmp\\lucy\\";
                string filename = "populationZH.csv"; // "giraffe.jpg";
                string checksum = GetMD5HashFromFile(EcopotVM.filenameResidents);
                byte[] bytestream = ReadFile(EcopotVM.filenameResidents);

                string filename2 = "workersZH.csv"; // "giraffe.jpg";
//                string filename2 = "workplacesAgg.csv"; // "giraffe.jpg";
                string checksum2 = GetMD5HashFromFile(EcopotVM.filenameWorkplaces);
                byte[] bytestream2 = ReadFile(EcopotVM.filenameWorkplaces);

                //            string outputMessage = "{'result':'outputs', 'SObjID':" + SObjID + ", 'serviceVersion':'0.1', 'machinename':'Rennsemmel', 'hashcode_inputs':" + hashCodeOfInputs
                //    + ", 'outputs':{'filestream':{'format':'jpg','streaminfo':{'order':1, 'checksum':'" + checksum + "'}}}}";


                //            JProperty confirmationInfo = (JProperty)cl.sendAction2Lucy(outputMessage, bytestream).First;




                //            string msg = "{'action':'create_service','inputs':{'inputVals':{'format':'text','value':" + filename + "}},'classname':'EcoPot'}";
                string msg = "{'action':'create_service','inputs':{'potentialStrength':" + EcopotVM.potentialStrength + 
                    ",'residents':{'format':'csv','streaminfo':{'order':1, 'checksum':'" + checksum +
                    "'}},'workers':{'format':'csv','streaminfo':{'order':2, 'checksum':'" + checksum2 + 
                    "'}},'resolution':" + EcopotVM.resolution + "},'classname':'ecoPot'}";
                // TODO_NewLuci                JProperty confirmationInfo = (JProperty)cl.sendAction2Lucy(msg, bytestream, bytestream2).First;

                JProperty confirmationInfo = null;
                int serviceID = confirmationInfo.Value.Value<int>("SObjID");
                string mqtt_topic = confirmationInfo.Value.Value<String>("mqtt_topic");
                mqtt_topic = mqtt_topic.Substring(0, mqtt_topic.Length - 1);

                CollabConfigSettings configEcopot = new CollabConfigSettings() { scenarioID = 0, mqtt_topic = mqtt_topic, objID = serviceID, service_type = ServiceType.EcoPot };
                configSettings[13] = configEcopot;

                if (mqttClient.IsConnected)
                {
                    mqttClient.Subscribe(new string[] { mqtt_topic }, new byte[] { 1 });
                    mqttClient.Publish(mqtt_topic, Encoding.UTF8.GetBytes("RUN"));
                }
            }

        }

        private void OnRunStressTestExecuted()
        {
            if (cl == null)
            {
                InfoNoConnection();
                System.Windows.MessageBox.Show("Please login to lucy first!");
            }
            else
            {
                // start num stress tests
            }
        }

        private void StartSingleStressTest() {

            /*
                string msg = "{'action':'create_service','inputs':{'delay':{'format':'number','value':" + StressTestVM.delay +
                    "},'classname':'stressTest'}";
                JProperty confirmationInfo = (JProperty)cl.sendAction2Lucy(msg).First;

                int serviceID = confirmationInfo.Value.Value<int>("SObjID");
                string mqtt_topic = confirmationInfo.Value.Value<String>("mqtt_topic");
                mqtt_topic = mqtt_topic.Substring(0, mqtt_topic.Length - 1);

                ConfigSettings configStresstest = new ConfigSettings() { scenarioID = 0, mqtt_topic = mqtt_topic, objID = serviceID };
                configSettings[13] = configEcopot;

                if (mqttClient.IsConnected)
                {
                    mqttClient.Subscribe(new string[] { mqtt_topic }, new byte[] { 1 });
                    mqttClient.Publish(mqtt_topic + "/", Encoding.UTF8.GetBytes("RUN"));
                }
            */
        }


        /*
        private void OnRunIsovistServiceExecuted_Direct()
        {
            if (curScenarioID == -1)
                OnCreateScenarioExecuted();

            PointCollection ptColl = new PointCollection();
            foreach (System.Windows.Rect rr in Buildings)
            {
                for (int l = (int)rr.Left / 10; l < (int)(rr.Left + rr.Width) / 10; l++)
                {
                    for (int h = (int)rr.Top / 10; h < (int)(rr.Top + rr.Height) / 10; h++)
                    {
                        ptColl.Add(new System.Windows.Point(l, h));
                    }
                }
            }

            int num = 85 * 51 - ptColl.Count();

            double[][] inputData = new double[2][];
            inputData[0] = new double[num];
            inputData[1] = new double[num];
            int counter = 0;
            for (int c = 0; c < 85; c++)
            {
                for (int r = 0; r < 51; r++)
                {
                    if (!ptColl.Contains(new System.Windows.Point(c, r)))
                    {
                        inputData[0][counter] = c + 0.5;
                        inputData[1][counter] = r + 0.5;
                        counter++;
                    }
                }
            }

            string json = JsonConvert.SerializeObject(inputData);

            string msg = "{'action':'run','service':{'inputs':{'inputVals':'" + json + "'},'classname':'Isovist','ScID':'" + curScenarioID + "'}}";

            if (cl == null)
            {
                InfoNoConnection();
            }
            else
            {
                JObject result = cl.sendService2Lucy(msg);
                if (result != null)
                {
                    JToken outputs = result.Value<JToken>("outputs");
                    if (outputs != null)
                    {
                        string output = (string)((Newtonsoft.Json.Linq.JProperty)(outputs.First())).Value;
                        AddText2InfoMW("result: " + outputs + " - output rows: " + output + "\n");

                        GeoJsonReader GeoJSONReader = new GeoJsonReader();
                        IsovistField2DWrapper iso_client_wrapper_result = GeoJSONReader.Read<IsovistField2DWrapper>(output);

                        Coordinate[] coords = iso_client_wrapper_result.Points.Coordinates;
                        for (int c = 0; c < coords.Count(); c++)
                        {
                            // Rect = Pixel showing a color representing the value of visible area (= result isovist service)
                            System.Windows.Rect Rect = new System.Windows.Rect(10 * coords[c].X - 5, 10 * coords[c].Y - 5, 10, 10);
                            SolidColorBrush br = new SolidColorBrush();

                            double value;
                            iso_client_wrapper_result.Results[c].TryGetValue(ResultsIsovist.Area, out value);

                            byte b = 255;
                            if (value < 1020)
                                b = (byte)(value / 4);

                            br.Color = Color.FromArgb(180, 0, 0, b);
                            Submissions.Add(new SubmissionResult() { rect = Rect, brush = br });
                        }
                    }
                    else
                        AddText2InfoMW("result: null \n");
                }
                else
                    AddText2InfoMW("something went wrong ... \n");
            }

        }
        */

        private void readResultsOfHeatEmissionService(int gridID, int instanceID)
        {
            if (configSettings[gridID].gridVM.Submissions == null)
                configSettings[gridID].gridVM.Submissions = new ObservableCollection<SubmissionResult>();
            else
                configSettings[gridID].gridVM.Submissions.Clear();

            string msg = "{'action':'get_service_outputs','SObjID':" + instanceID + "}";

            /* TODO_NewLuci
            JObject result = cl.sendAction2Lucy(msg);
            if (result != null)
            {
                JToken outputs = result.Value<JToken>("result").Value<JToken>("outputs");
                if (outputs != null)
                {
                    JArray buffer = (JArray)JsonConvert.DeserializeObject(outputs.Value<JObject>("outputVals").Value<String>("value"));
                    //int[][] buffer = JsonConvert.DeserializeObject<int[][]>(output);
                    AddText2InfoMW("result: " + outputs + " - output rows: " + buffer + "\n");

                    for (int c = 0; c < 85; c++)
                    {
                        for (int r = 0; r < 55; r++)
                        {
                            if ((int)buffer[c][r] > 0)
                            {
                                int valX = 10 * c;
                                int valY = 10 * r;
                                int width = 10;
                                int height = 10;

                                System.Windows.Rect Rect = new System.Windows.Rect(valX, valY, width, height);
                                SolidColorBrush br = new SolidColorBrush();


                                if ((int)buffer[c][r] >= 18)
                                    br.Color = Colors.DarkRed;
                                else if ((int)buffer[c][r] >= 15)
                                    br.Color = Colors.Red;
                                else if ((int)buffer[c][r] >= 12)
                                    br.Color = Colors.DarkOrange;
                                else if ((int)buffer[c][r] >= 9)
                                    br.Color = Colors.OrangeRed;
                                else if ((int)buffer[c][r] >= 6)
                                    br.Color = Colors.Orange;
                                else
                                    br.Color = Colors.Yellow;

                                configSettings[gridID].gridVM.Submissions.Add(new SubmissionResult() { rect = Rect, brush = br });
                            }
                        }
                    }
                }
            }*/
        }


        private void readResultsOfService(int gridID, int instanceID)
        {
            // {'action':'get_outputs_of','SObjID':115}
            string msg = "{'action':'get_service_outputs','SObjID':" + instanceID + "}";

            // TODO_NewLuci            JObject result = cl.sendAction2Lucy(msg);
            JObject result = null;
            if (result != null)
            {
                JToken outputs = result.Value<JToken>("result").Value<JToken>("outputs");
                if (outputs != null)
                {
                    string output = outputs.Value<String>("outputVals");
                    // für ecoPot
                    // outputs.Value<JObject>("potential").Value<String>("value")
                    AddText2InfoMW("result: " + outputs + " - output rows: " + output + "\n");

                    GeoJsonReader GeoJSONReader = new GeoJsonReader();
                    IsovistField2DWrapper iso_client_wrapper_result = GeoJSONReader.Read<IsovistField2DWrapper>(output);
                    /*
                                            BackgroundWorker bgworker = new BackgroundWorker();
                        bgworker.WorkerReportsProgress = true;
                        bgworker.DoWork += (s, e) =>
                        {
                    */

                    List<SubmissionResult> results = new List<SubmissionResult>();

                    Coordinate[] coords = iso_client_wrapper_result.Points.Coordinates;
                    for (int c = 0; c < coords.Count(); c++)
                    {
                        // Rect = Pixel showing a color representing the value of visible area (= result isovist service)
                        System.Windows.Rect Rect = new System.Windows.Rect(10 * coords[c].X - 5, 10 * coords[c].Y - 5, 10, 10);
                        SolidColorBrush br = new SolidColorBrush();

                        double value;
                        iso_client_wrapper_result.Results[c].TryGetValue(ResultsIsovist.Area, out value);

                        byte b = 255;
                        if (value < 1020)
                            b = (byte)(value / 4);

                           br.Color = Color.FromArgb(180, 0, 0, b);
                           results.Add(new SubmissionResult() { rect = Rect, brush = br });
                        
      //                  System.Windows.Application.Current.Dispatcher.BeginInvoke((Action)(() => 
        //    {
//                Submissions.Add(new SubmissionResult() { rect = Rect, brush = br });
//                RaisePropertyChanged("Submissions");
         //   }));


                        /*
                                                bgworker.ReportProgress(c, new SubmissionResult() { rect = Rect, brush = br });
                                            }
                                            e.Result = null;

                                                };
    
                                                Task.Factory.StartNew(() => 
                                                {
                                                Dispatcher.CurrentDispatcher.Invoke(new Action(() =>
                                                {
                                                    Submissions.Add(new SubmissionResult() { rect = Rect, brush = br });
                                                }));
                                                });

                                                BackgroundWorker bgworker = new BackgroundWorker();
                                                bgworker.WorkerReportsProgress = true;
                                                bgworker.DoWork += (s, e) =>
                                                {
                                                    for (int i = 0; i < 10; i++)
                                                    {
                                                        System.Threading.Thread.Sleep(1000);
                                                        bgworker.ReportProgress(i, new List<ViewModel>());
                                                    }
                                                    e.Result = null;
                                                };
                                                bgworker.ProgressChanged += (s, e) =>
                                                {
                                                    SubmissionResult partialresult = (SubmissionResult)e.UserState;
                                                    Submissions.Add(partialresult);
                                                };
                                                bgworker.RunWorkerCompleted += (s, e) =>
                                                {
                                                    //do anything here
                                                   //Submissions.Add(new SubmissionResult() { rect = Rect, brush = br });
                                                };


                                            }*/
                    }

                    //x Submissions = new ObservableCollection<SubmissionResult>( results);
                    configSettings[gridID].gridVM.Submissions = new ObservableCollection<SubmissionResult>(results);
//                    RaisePropertyChanged("Submissions");
                }
            }
        }
        //}

        private void readResultsOfEcopotService(int instanceID)
        {
            // {'action':'get_outputs_of','SObjID':115}
            string msg = "{'action':'get_service_outputs','SObjID':" + instanceID + "}";

            // TODO_NewLuci            JObject result = cl.sendAction2Lucy(msg);
            JObject result = null;
            if (result != null)
            {
                JToken outputs = result.Value<JToken>("result").Value<JToken>("outputs");
                if (outputs != null)
                {
                    int resolution = outputs.Value<int>("resolution");
                    JArray output = outputs.Value<JArray>("potential");
                    // für ecoPot
                    // outputs.Value<JObject>("potential").Value<String>("value")
                    AddText2InfoMW("result: " + outputs + " - output rows: " + output + "\n");

                    int max = 1000;
                    if (EcopotVM != null)
                    {
                        EcopotVM.ReadEcopotResults(resolution, output,max);
                    }

                }
            }
        }


        private void OnConnect2LucyServerExecuted()
        {
            if (ServerIP != null && ServerIP.Length < 7)
            {
                System.Windows.MessageBox.Show("Please enter a valid Server IP address!");
                return;
            }

            OnConnect2LucyExecuted(ServerIP);
        }



        //129.132.6.4:7654
        private void OnConnect2LucyExecuted(String host)
        {
            cl = new Communication2Luci();

            string result = "Connection to Luci was ";

            //if (!cl.connect("localhost", 7654))
            if (!cl.connect(host, 7654))
                result += "not ";

            result += "successful! \n";

            AddText2InfoMW(result);
            //string mqtt_host = "129.132.6.88";
            string mqtt_host = host;

            if (host == "localhost")
                _host = System.Net.Dns.GetHostName();
            else 
                _host = mqtt_host;

            mqttClient = new MqttClient(_host);


            /*
            if (host == "localhost")
                mqttClient = new MqttClient(System.Net.Dns.GetHostName());
            else
                mqttClient = new MqttClient(System.Net.IPAddress.Parse(mqtt_host));
            */

            mqttClient.Connect("collabSampleUI");
            mqttClient.MqttMsgPublishReceived += mqttClient_MqttMsgPublishReceived;


            return;
        }

        public string getMqttHost()
        {
            return _host;
        }

        void mqttClient_MqttMsgPublishReceived(object sender, uPLibrary.Networking.M2Mqtt.Messages.MqttMsgPublishEventArgs e)
        {
            if (configSettings != null)
            {
                int index = -1;

                try
                {
                    var keyWithMatchingValue = configSettings.Where(p => p.Value.mqtt_topic == e.Topic).Select(p => p.Key).First();
                    index = (int)keyWithMatchingValue;
                }
                catch (Exception ex)
                {
                    // no matching key found
                }

                if (index >= 0)
                {
                    CollabConfigSettings foundSettings;
                    configSettings.TryGetValue(index, out foundSettings);
                    string message = Encoding.UTF8.GetString(e.Message);
                    if (message == "DONE")
                    {
                        // read results for given key
                        receivedResult = true;
                        //{'action':'get_outputs_of','SObjID':115}
                        int sobjID = foundSettings.objID;

                        if (index == 13)
                        {
                            System.Windows.Application.Current.Dispatcher.Invoke((Action)(delegate
                            {
                                readResultsOfEcopotService(sobjID);
                            }));                
                        }
                        else
                        {
                            if (foundSettings.service_type == ServiceType.HeatEmission)
                            {
                                System.Windows.Application.Current.Dispatcher.Invoke((Action)(delegate
                                {
                                    readResultsOfHeatEmissionService(index, sobjID);
                                }));
                            }
                            else
                            {
                                System.Windows.Application.Current.Dispatcher.Invoke((Action)(delegate
                                {
                                    readResultsOfService(index, sobjID);
                                }));
                            }
                        }
                    }
                }
            }
        }

        private void OnGetActionsListExecuted()
        {
            if (cl == null)
            {
                InfoNoConnection();
            }
            else
            {
                /* TODO_NewLuci
                JProperty result = cl.getActionsList();
                AddText2InfoMW(result.Name + ": " + result.Value + "\n");

                IEnumerable<JToken> arrActions =  result.Value["actions"];
                ActionsListContent = arrActions;
                SelectedAction = null;

                RaisePropertyChanged("ActionsListContent");
                 */
            }
        }

        private void OnGetServicesListExecuted()
        {
            if (cl == null)
            {
                InfoNoConnection();
            }
            else
            {
                /* TODO_NewLuci
                JProperty result = cl.getServicesList();
                AddText2InfoMW(result.Name + ": " + result.Value + "\n");

                ServicesListContent = result.Value["services"]; 
                SelectedService = null;

                RaisePropertyChanged("ServicesListContent");
                 */
            }
        }

        private void OnSendMessageToMWCommandExecuted(string msg) 
        {
            if (cl == null)
            {
                InfoNoConnection();
            }
            else
            {
                //JProperty result = cl.sendAction2Lucy(msg);
                //AddText2InfoMW(result.Name + ": " + result.Value + "\n");

                // TODO_NewLuci                JObject result = cl.sendAction2Lucy(msg);
                // TODO_NewLuci                AddText2InfoMW(result.ToString());
                
            }
        }


        private void ShowDetails4SelectedAction(string action)
        {
            if (cl == null)
            {
                InfoNoConnection();
            }
            else
            {
                if (action == null || action.Length == 0)
                {
                    ActionDetailsContent = "no action selected";
                }
                else
                {
                    // TODO_NewLuci                    JProperty result = cl.getActionParameters(action);
                    //AddText2InfoMW(result.Name + ": " + result.Value + "\n");

                    // TODO_NewLuci                    ActionDetailsContent = result.Name + ": " + result.Value;
                }
                RaisePropertyChanged("ActionDetailsContent");
            }
        }

        private void ShowDetails4SelectedService(string service)
        {
            if (cl == null)
            {
                InfoNoConnection();
            }
            else
            {
                if (service == null || service.Length == 0)
                {
                    ActionDetailsContent = "no service selected";
                }
                else
                {
                    // TODO_NewLuci                    JProperty result = cl.getServiceParameters(service);

                    // TODO_NewLuci                    ServiceDetailsContent = result.Name + ": " + result.Value;
                }
                RaisePropertyChanged("ServiceDetailsContent");
            }
        }


        private void ReadOutputFromServer(object sender, EventArgs e)
        {
            // Timer callback code here...
           // while (!OutputFromLucy.EndOfStream) 
            {
                string json = OutputFromLucy.ReadLine();
                dynamic jObj = JsonConvert.DeserializeObject(json);

                var jObj2 = (JObject)JsonConvert.DeserializeObject(json);

                string name = ((Newtonsoft.Json.Linq.JProperty)(jObj2.First)).Name;
                JToken value = ((Newtonsoft.Json.Linq.JProperty)(jObj2.First)).Value;
                object obj = value.ToObject(value.GetType());

                var val2 = ((Newtonsoft.Json.Linq.JProperty)(jObj2.First)).Value;

                InfoMW += name + ": " + obj.ToString() + "\n";
                RaisePropertyChanged("InfoMW");
            }

            timer.Stop();
        }

        private void OnLogin2LucyExecuted(object pwBox)
        {
            if (cl == null)
            {
                InfoNoConnection();
            }
            else
            {
                if (pwBox != null && pwBox is PasswordBox)
                {
                    //var passwordBox = (PasswordBox)param;
                    // string result = cl.authenticate("lukas", "1234");
                    LuciCommunication.Communication2Luci.LuciAnswer answer = cl.authenticate(User, (pwBox as PasswordBox).Password);
                    AddText2InfoMW(answer.Message);

                    // TODO_NewLuci                    JProperty result = cl.authenticate(User, (pwBox as PasswordBox).Password);
                    // TODO_NewLuci                    AddText2InfoMW(result.Name + ": " + result.Value + "\n");
                }
            }
        }

        private void InfoNoConnection()
        {
            AddText2InfoMW("No Connection to Lucy established yet! Please connect to Lucy first of all!\r\n");
        }

        private void AddText2InfoMW(string text)
        {
            InfoMW += text;
            RaisePropertyChanged("InfoMW");
        }

        public static byte[] ReadFile(string filePath)
        {
            byte[] buffer;
            FileStream fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
            try
            {
                int length = (int)fileStream.Length;  // get file length
                buffer = new byte[length];            // create buffer
                int count;                            // actual number of bytes read
                int sum = 0;                          // total number of bytes read

                // read until Read method returns 0 (end of the stream has been reached)
                while ((count = fileStream.Read(buffer, sum, length - sum)) > 0)
                    sum += count;  // sum is a buffer offset for next reading
            }
            finally
            {
                fileStream.Close();
            }
            return buffer;
        }


        public static string GetMD5HashFromFile(string filename)
        {
            using (var md5 = MD5.Create())
            {
                using (var stream = File.OpenRead(filename))
                {
                    return BitConverter.ToString(md5.ComputeHash(stream)).Replace("-", string.Empty);
                }
            }
        }

    }

/*
    public class GeometryConverter
    {
        public static Point ToPoint(CPlan.Geometry.Vector2D point)
        {
            return new Point(point.X, point.Y);
        }

        public static LineString ToLineString(CPlan.Geometry.Line2D line)
        {
            return new LineString(new Coordinate[] { new Coordinate(line.Start.X, line.Start.Y), new Coordinate(line.End.X, line.End.Y) });
        }

        public static CPlan.Geometry.Vector2D ToVector2D(IPoint point)
        {
            return new CPlan.Geometry.Vector2D(point.X, point.Y);
        }

        public static CPlan.Geometry.Line2D ToLine2D(ILineString line)
        {
            return new CPlan.Geometry.Line2D(line.StartPoint.X, line.StartPoint.Y, line.EndPoint.X, line.EndPoint.Y);
        }

        public static MultiPoint ToMultiPoint(List<CPlan.Geometry.Vector2D> points)
        {
            Point[] tmp = new Point[points.Count];

            for (int i = 0; i < points.Count; i++)
            {
                tmp[i] = ToPoint(points[i]);
            }

            return new MultiPoint(tmp);
        }

        public static List<Vector2d> ToVector2DList(MultiPoint points)
        {
            List<Vector2d> result = new List<CPlan.Geometry.Vector2D>();

            foreach (IPoint point in points.Geometries)
            {
                result.Add(ToVector2D(point));
            }

            return result;
        }

        public static MultiLineString ToMultiLineString(List<CPlan.Geometry.Line2D> lines)
        {
            LineString[] tmp = new LineString[lines.Count];

            for (int i = 0; i < lines.Count; i++)
            {
                tmp[i] = ToLineString(lines[i]);
            }

            return new MultiLineString(tmp);
        }

        public static List<CPlan.Geometry.Line2D> ToLine2DList(MultiLineString lines)
        {
            List<CPlan.Geometry.Line2D> result = new List<CPlan.Geometry.Line2D>();

            foreach (ILineString line in lines.Geometries)
            {
                result.Add(ToLine2D(line));
            }

            return result;
        }
    }

    public class IsovistField2DWrapper
    {
        private MultiPoint m_points;
        private MultiLineString m_obstacles;
        private float m_precision;
        private List<Dictionary<ResultsIsovist, double>> m_results;

        public List<Dictionary<ResultsIsovist, double>> Results
        {
            get { return m_results; }
            set { m_results = value; }
        }

        public MultiLineString Obstacles
        {
            get { return m_obstacles; }
            set { m_obstacles = value; }
        }

        public float Precision
        {
            get { return m_precision; }
            set { m_precision = value; }
        }

        //by default all Properties are serialized but can be exluded using following annotation        
        //[Newtonsoft.Json.JsonIgnore]
        public MultiPoint Points
        {
            get { return m_points; }
            set { m_points = value; }
        }

        [Newtonsoft.Json.JsonConstructor]
        public IsovistField2DWrapper(MultiPoint points, MultiLineString obstacles, List<Dictionary<ResultsIsovist, double>> results, float precision)
        {
            m_points = points;
            m_precision = precision;
            m_obstacles = obstacles;
            m_results = results;
        }

        public IsovistField2DWrapper(IsovistField2D isovist) : this(GeometryConverter.ToMultiPoint(isovist.Points), GeometryConverter.ToMultiLineString(isovist.ObstacleLines), isovist.Results, isovist.Precision) { }

        public IsovistField2D ToIsovistField2D()
        {
            return new IsovistField2D(GeometryConverter.ToVector2DList(Points), GeometryConverter.ToLine2DList(Obstacles));
        }


    }
 */
}
