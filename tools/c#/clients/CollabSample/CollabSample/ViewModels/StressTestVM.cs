﻿using LuciCommunication;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.ViewModel;
using Microsoft.Win32;
using ReadWriteCsv;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Shapes;
using uPLibrary.Networking.M2Mqtt;

namespace CollabSample.ViewModels
{
    [Serializable]
    public class StressTestSettings : NotificationObject
    {
        public ObservableCollection<UILine> lines { get; set; }
        public int delay { get; set; }
        public int scenarioID { get; set; }
        public int serviceID { get; set; }
        public string mqtt_topic { get; set; }
        public DateTime startScenario { get; set; }
        public DateTime finishedScenario { get; set; }
        public DateTime startService { get; set; }
        public DateTime finishedService { get; set; }
        public DateTime startServiceIntern { get; set; }
        public DateTime finishedServiceIntern { get; set; }
        public int instance_id { get; set; }

        private int _state;
        public int state
        {
            get { return _state; }
            set
            {
                _state = value;
                RaisePropertyChanged("state");
            }
        }
    }

    [Serializable]
    public class UILine
    {
        public double X1 { get; set; }
        public double X2 { get; set; }
        public double Y1 { get; set; }
        public double Y2 { get; set; }
    }

    public class StressTestVM : NotificationObject
    {
        public ObservableCollection<StressTestSettings> Settings { get; set; }

        private ObservableCollection<StressTestSettings> _settings;
        public ObservableCollection<StressTestSettings> settings
        {
            get { return _settings; }
            set
            {
                _settings = value;
                RaisePropertyChanged("settings");
            }
        }


        private GeoVM ViewModel;
        private Random r = new Random();

        private MqttClient mqttClient;
        private static Communication2Luci cl;

        public int numScenarios { get; set; }
        public int numLinesMin { get; set; }
        public int numLinesMax { get; set; }
        public int delayMin { get; set; }
        public int delayMax { get; set; }

        // Commands
        public ICommand StartStressTestCommand { get; private set; }
        public ICommand StopStressTestCommand { get; private set; }
        public ICommand ExportFileCommand { get; private set; }
        public ICommand CreateSettingsCommand { get; private set; }
        public ICommand LoadSettingsCommand { get; private set; }
        public ICommand SaveSettingsCommand { get; private set; }

        public StressTestVM(GeoVM VM)
        {
            ViewModel = VM;
            settings = new ObservableCollection<StressTestSettings>();
            Settings = new ObservableCollection<StressTestSettings>();

            numLinesMin = 1;
            numLinesMax = 10;
            numScenarios = 10;
            delayMin = 0;
            delayMax = 5;

            string host = VM.getMqttHost();
            if (host != null)
            {
                mqttClient = new MqttClient(host);
                //            mqttClient = new MqttClient(System.Net.Dns.GetHostName());
                //          mqttClient = new MqttClient(System.Net.IPAddress.Parse("129.132.6.35"));
                int r = new Random(DateTime.Now.Millisecond).Next();
                mqttClient.Connect("StressTest_" + r.ToString());
                mqttClient.MqttMsgPublishReceived += mqttClient_MqttMsgPublishReceived;
            }

            StartStressTestCommand = new DelegateCommand(OnStartStressTestExecuted);
            StopStressTestCommand = new DelegateCommand(OnStopStressTestExecuted);
            ExportFileCommand = new DelegateCommand(OnExportFileExecuted);

            CreateSettingsCommand = new DelegateCommand(OnCreateSettingsExecuted);
            LoadSettingsCommand = new DelegateCommand(OnLoadSettingsExecuted);
            SaveSettingsCommand = new DelegateCommand(OnSaveSettingsExecuted);
        }

        void mqttClient_MqttMsgPublishReceived(object sender, uPLibrary.Networking.M2Mqtt.Messages.MqttMsgPublishEventArgs e)
        {
            if (Settings != null)
            {
                StressTestSettings foundSettings = null;

                try
                {
                    foundSettings = Settings.Where(p => p.mqtt_topic == e.Topic).First();
                }
                catch (Exception ex)
                {
                    // no matching key found
                    Console.Out.WriteLine("not found: " + e.Topic);
                }

                if (foundSettings != null)
                {
                    string message = Encoding.UTF8.GetString(e.Message);
                    if (message == "DONE")
                    {
                        foundSettings.state = 2;
                        foundSettings.finishedService = DateTime.Now;

                        // read results for given key
                        System.Windows.Application.Current.Dispatcher.Invoke((Action)(delegate
                        {
                            readResultsOfService(foundSettings);
                        }));
                    }
                    else
                    {
                        if (message == "RUN")
                        {
                            foundSettings.state = 1;
                            foundSettings.startService = DateTime.Now;
                        }
                        else
                        {
                            foundSettings.state = 10;
                        }
                    }
                }
            }
        }

        private Line[] CreateSetup(int numLines)
        {
            Line[] lines = new Line[numLines];

            for (int i = 0; i < numLines; i++)
            {
                lines[i] = new Line() { X1 = r.Next(100), X2 = r.Next(100), Y1 = r.Next(50), Y2 = r.Next(50) };
            }

            return lines;
        }

        private UILine[] CreateSetup2(int numLines)
        {
            UILine[] lines = new UILine[numLines];

            for (int i = 0; i < numLines; i++)
            {
                lines[i] = new UILine() { X1 = r.Next(100), X2 = r.Next(100), Y1 = r.Next(50), Y2 = r.Next(50) };
            }

            return lines;
        }

        private void OnCreateSettingsExecuted()
        {
            if (Settings == null)
                Settings = new ObservableCollection<StressTestSettings>();

            Settings.Clear();

            int minLines = numLinesMin > 0 ? numLinesMin : 1;
            int maxLines = numLinesMax > 0 && numLinesMax >= minLines ? numLinesMax : 1;

            for (int s = 0; s < numScenarios; s++)
            {
                int numLines = r.Next(minLines, maxLines + 1);
                int delay = r.Next(delayMin, delayMax + 1);
                UILine[] lines = CreateSetup2(numLines);

                Settings.Add(new StressTestSettings() { lines = new ObservableCollection<UILine>(lines), delay = delay, state = 0 });
            }

            cl = ViewModel.getLuciHub();
            RaisePropertyChanged("Settings");
        }


        private void OnLoadSettingsExecuted()
        {
            OpenFileDialog loadFileDialog1 = new OpenFileDialog();
            loadFileDialog1.Filter = "Serialized Text files (*.txt)|*.txt|All files (*.*)|*.*";

            if (loadFileDialog1.ShowDialog() == true)
            {
                string filePath = loadFileDialog1.FileName;

                Settings.Clear();

                using (Stream stream = File.Open(filePath, FileMode.Open))
                {
                    var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                    Settings = (ObservableCollection<StressTestSettings>)binaryFormatter.Deserialize(stream);
                    RaisePropertyChanged("Settings");
                }
            }
        }

        private void OnSaveSettingsExecuted()
        {
            if (Settings == null || Settings.Any() == false)
            {
                MessageBox.Show("No Settings found. Please create!");
                return;
            }

            if (Settings != null && Settings.Any() == true)
            {
                SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                saveFileDialog1.Filter = "Serialized Text files (*.txt)|*.txt|All files (*.*)|*.*";

                if (saveFileDialog1.ShowDialog() == true)
                {
                    string filePath = saveFileDialog1.FileName;
                    using (Stream stream = File.Open(filePath, FileMode.Create))
                    {
                        var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                        binaryFormatter.Serialize(stream, Settings);
                    }
                }
            }
        }


        private async Task runST(List<KeyValuePair<string, object>> kvp)
        {
            // Communication2Luci cl = ViewModel.getLuciHub();
            Console.Out.WriteLine("run");
            LuciCommunication.Communication2Luci.LuciAnswer? asyncAnswer = await cl.runAsyncService("LineService", kvp, 513);
            Console.Out.WriteLine("done");
            //            entry.finishedService = DateTime.Now;

            //           await ReadLuciResults(asyncAnswer, entry);
            //            entry.state = 2;
        }
        private async Task runST(List<KeyValuePair<string, object>> kvp, StressTestSettings entry)
        {
           // Communication2Luci cl = ViewModel.getLuciHub();
//            Console.Out.WriteLine("run");
            LuciCommunication.Communication2Luci.LuciAnswer? asyncAnswer = await cl.runAsyncService("LineService", kvp, (long)entry.scenarioID);
 //           Console.Out.WriteLine("done");
            entry.finishedService = DateTime.Now;

           await ReadLuciResults(asyncAnswer, entry);
            entry.state = 2;
        }

        private async void OnStartStressTestExecuted()
        {
            if (Settings == null || Settings.Any() == false)
            {
                MessageBox.Show("No Settings found. Please create or load settings first of all!");
                return;
            }

 /*           var kvp = new List<KeyValuePair<string, object>>
                    {
                        new KeyValuePair<string, object>("delay", 10),
                    };
            for (int i = 0; i < 100; i++)
                runST(kvp);
*/
            
            foreach (StressTestSettings entry in Settings)
            {
                entry.startScenario = DateTime.Now;
                entry.scenarioID = ViewModel.createScenario4StressTest(entry.lines.ToArray<UILine>());
                RaisePropertyChanged("Settings");
                entry.finishedScenario = DateTime.Now;

                var kvp = new List<KeyValuePair<string, object>>
                    {
                        new KeyValuePair<string, object>("delay", entry.delay * 1000),
                    };

                entry.state = 1;
                entry.startService = DateTime.Now;

                //entry.startService = DateTime.Now;

                runST(kvp, entry);
                await Task.Delay(1);

                
            }

                //LuciCommunication.Communication2Luci.LuciAnswer luciResult = ViewModel.runService("LineService", kvp, entry.scenarioID);


                /*                

                               BackgroundWorker bg = new BackgroundWorker();
                               bg.DoWork += bg_DoWork;
                               bg.RunWorkerCompleted += bg_RunWorkerCompleted;
                               bg.RunWorkerAsync(entry);
                               */

                /*
                CollabConfigSettings configInfo = startService(entry.scenarioID, entry.delay);

                 
                    System.Windows.Application.Current.Dispatcher.BeginInvoke((Action)(() =>
                    {
                        entry.serviceID = configInfo.objID;
                        entry.state = 0;
                        entry.mqtt_topic = configInfo.mqtt_topic;
                        mqttClient.Publish(configInfo.mqtt_topic, Encoding.UTF8.GetBytes("RUN"));
                    }),
                        System.Windows.Threading.DispatcherPriority.Normal, null);
                }
            }, System.Threading.Tasks.TaskCreationOptions.LongRunning);
            */


            /*
            int minLines = numLinesMin > 0 ? numLinesMin : 1;
            int maxLines = numLinesMax > 0 && numLinesMax >= minLines ? numLinesMax : 1;


            Settings.Clear();
            RaisePropertyChanged("Settings");
            
            System.Threading.Tasks.Task.Factory.StartNew(() =>
            {
                for (int s = 0; s < numScenarios; s++)
                {
                    int numLines = r.Next(minLines, maxLines + 1);
                    int delay = r.Next(delayMin, delayMax + 1);
                    UILine[] lines = CreateSetup2(numLines);
                    DateTime startScenario = DateTime.Now;
                    int scID = ViewModel.createScenario4StressTest(lines);
                    DateTime endScenario = DateTime.Now;
                    ConfigSettings configInfo = startService(scID, delay);

                    System.Windows.Application.Current.Dispatcher.BeginInvoke((Action)(() =>
                    {
                        Settings.Add(new StressTestSettings() { scenarioID = scID, serviceID = configInfo.objID, lines = new ObservableCollection<UILine>(lines), delay = delay, state = 0, mqtt_topic = configInfo.mqtt_topic, startScenario = startScenario, finishedScenario = endScenario });
                        mqttClient.Publish(configInfo.mqtt_topic + "/", Encoding.UTF8.GetBytes("RUN"));
                    }),
                        System.Windows.Threading.DispatcherPriority.Normal, null);
                }
            }, System.Threading.Tasks.TaskCreationOptions.LongRunning);*/
        }

        public async Task ReadLuciResults(LuciCommunication.Communication2Luci.LuciAnswer? luciAnswer, StressTestSettings entry)
        {
            if (luciAnswer == null)
                return;

            object output = ViewModel.getOutput((LuciCommunication.Communication2Luci.LuciAnswer)luciAnswer, "lengthSum");
            object serviceId = ViewModel.getOutput((LuciCommunication.Communication2Luci.LuciAnswer)luciAnswer, "instance_id");
            object startTime = ViewModel.getOutput((LuciCommunication.Communication2Luci.LuciAnswer)luciAnswer, "start_time");
            object endTime = ViewModel.getOutput((LuciCommunication.Communication2Luci.LuciAnswer)luciAnswer, "end_time");

            int id = -1;
            int.TryParse(serviceId.ToString(), out id);
            entry.instance_id = id;
            entry.startServiceIntern = (DateTime)startTime;
            entry.finishedServiceIntern = (DateTime)endTime;
            
        }

        void bg_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
        }

        void bg_DoWork(object sender, DoWorkEventArgs e)
        {
            if (e != null && e.Argument != null && e.Argument is StressTestSettings)
            {
                StressTestSettings entry = (StressTestSettings)e.Argument;

                var kvp = new List<KeyValuePair<string, object>>
                    {
                        new KeyValuePair<string, object>("delay", entry.delay),                       
                    };

                entry.state = 1;
                entry.startService = DateTime.Now;
                LuciCommunication.Communication2Luci.LuciAnswer luciResult = ViewModel.runService("LineService", kvp, entry.scenarioID);
                entry.finishedService = DateTime.Now;

                if (luciResult.Outputs != null)
                {
                    var s = luciResult.Outputs.Where(q => q.Key == "instance_id");
                    if (s != null && s.Any())
                        entry.serviceID = (int)((long)s.First().Value);
                }
//                Console.Out.Write("instance: " + entry.serviceID + ", scenario: " + entry.scenarioID);
                entry.state = 2;
            }
        }

        private CollabConfigSettings startService(int scenarioID, int delay)
        {
            /*
            string mqtt_topic = null;

            string msg = "{'action':'create_service','inputs':{'delay':" + delay
                + "}, 'classname':'LineService','ScID':" + scenarioID + "}";
            */

            var kvp = new List<KeyValuePair<string, object>>
                    {
                        new KeyValuePair<string, object>("delay", delay),                       
                    };
            ConfigSettings lucyResult = ViewModel.createService("LineService", kvp, scenarioID);

            //ConfigSettings lucyResult = ViewModel.startService(msg, scenarioID);
            CollabConfigSettings configInfo = new CollabConfigSettings() { mqtt_topic = lucyResult.mqtt_topic, objID = lucyResult.objID, scenarioID = lucyResult.scenarioID };
            if (configInfo != null && configInfo.mqtt_topic != null)
            {
                configInfo.service_type = ServiceType.StressTest;
                if (mqttClient.IsConnected)
                {
                    mqttClient.Subscribe(new string[] { configInfo.mqtt_topic }, new byte[] { 1 });
                    //  mqttClient.Publish(configInfo.mqtt_topic + "/", Encoding.UTF8.GetBytes("RUN"));
                    //mqtt_topic = configInfo.mqtt_topic;
                }
            }

            return configInfo;
        }

        private void readResultsOfService(StressTestSettings settings)
        {
            if (Settings != null)
            {
                settings.instance_id = ViewModel.getInstanceID(settings.serviceID);
            }
                
            /*
            // {'action':'get_outputs_of','SObjID':115}
            string msg = "{'action':'get_service_outputs','SObjID':" + instanceID + "}";

            JObject result = cl.sendAction2Lucy(msg);
            if (result != null)
            {
                JToken outputs = result.Value<JToken>("result").Value<JToken>("outputs");
                if (outputs != null)
                {
                    string output = outputs.Value<JObject>("outputVals").Value<String>("value");
                    // für ecoPot
                    // outputs.Value<JObject>("potential").Value<String>("value")
                    AddText2InfoMW("result: " + outputs + " - output rows: " + output + "\n");
                }
            }*/
        }

        private void OnStopStressTestExecuted()
        {
        }


        private void OnExportFileExecuted()
        {
            if (Settings != null)
            {
                SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                saveFileDialog1.Filter = "Comma-separated Text files (*.csv, *.txt)|*.csv;*.txt|All files (*.*)|*.*";

                string fullPattern = DateTimeFormatInfo.CurrentInfo.LongTimePattern;
                fullPattern = Regex.Replace(fullPattern, "(:ss|:s)", "$1.fff");

                if (saveFileDialog1.ShowDialog() == true)
                {
                    string csvFilename = saveFileDialog1.FileName;

                    using (CsvFileWriter writer = new CsvFileWriter(csvFilename))
                    {
                        CsvRow title = new CsvRow();
                        title.Add("scenario ID");
                        title.Add("service ID");
                        title.Add("instance ID");
                        title.Add("lines count");
                        title.Add("delay [s]");
                        title.Add("start time scenario");
                        title.Add("finish time scenario");
                        title.Add("call time service");
                        title.Add("start time service");
                        title.Add("end time service");
                        title.Add("return time service");
                        title.Add("state");

                        writer.WriteRow(title);

                        foreach (StressTestSettings entry in Settings)
                        {
                            CsvRow row = new CsvRow();

                            row.Add(entry.scenarioID.ToString());
                            row.Add(entry.serviceID.ToString());
                            row.Add(entry.instance_id.ToString());
                            row.Add(entry.lines.Count().ToString());
                            row.Add(entry.delay.ToString());
                            row.Add(entry.startScenario.ToString(fullPattern));
                            row.Add(entry.finishedScenario.ToString(fullPattern));
                            row.Add(entry.startService.ToString(fullPattern));
                            row.Add(entry.startServiceIntern.ToString(fullPattern));
                            row.Add(entry.finishedServiceIntern.ToString(fullPattern));
                            row.Add(entry.finishedService.ToString(fullPattern));
                            row.Add(entry.state.ToString());

                            writer.WriteRow(row);
                        }
                    }
                }
            }
        }
    }
}

