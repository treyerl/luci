﻿using CollabSample.Data;
using Microsoft.Practices.Prism.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollabSample.ViewModels
{
    public class VisualizationGridVM : NotificationObject
    {
        private int _sObjID;
        public int SObjID 
        {
            get { return _sObjID; }
            set {
                _sObjID = value;
                RaisePropertyChanged("SObjID");
            }
        }

        public ObservableCollection<System.Windows.Rect> BBB { get; set; }

        private ObservableCollection<SubmissionResult> _submissions;
        public ObservableCollection<SubmissionResult> Submissions//{ get; set; }
        {
            get { return _submissions; }
            set
            {
                _submissions = value;
                RaisePropertyChanged("Submissions");
            }
        }

//        public ObservableCollection<SubmissionResult> Submissions = new ObservableCollection<SubmissionResult>();

        public bool IsSelected { get; set; }

        public VisualizationGridVM() {
            BBB = new ObservableCollection<System.Windows.Rect>();
            Submissions = new ObservableCollection<SubmissionResult>();
            initBuildings(35, 20, 4, 4);
            //BBB.Add(new System.Windows.Rect(20, 20, 150, 100));
        }

        private void initBuildings(int numCols, int numRows, int numX, int numY)
        {
            Random num = new Random(DateTime.Now.Millisecond);

            for (int c = 0; c < numY; c++)
            {
                for (int r = 0; r < numX; r++)
                {
                    int valX = 10 * (num.Next(0, numCols) / 10);
                    int valY = 10 * (num.Next(0, numRows) / 10);
                    int width = 10 * (num.Next(20, 100 - valX) / 10);
                    int height = 10 * (num.Next(20, 50 - valY) / 10);

                    System.Windows.Rect Rect = new System.Windows.Rect(100 * c + valX, 50 * r + valY, width, height);

                    BBB.Add(Rect);
                }
            }
        }

        public void createNewGeo()
        {
            Submissions.Clear();
            BBB.Clear();
            initBuildings(35, 20, 4, 4);
            
        }
    }
}
