﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace HeatEmissionVisualizer.Converters
{
    class RectToPointsConverter : IValueConverter
    {
        // Define the Convert method to compare a selected string to 
        // a given string.
        public object Convert(object value, Type targetType,
            object parameter, System.Globalization.CultureInfo culture)
        {
            PointCollection coll = new PointCollection();

            if (value != null && value is Rect)
            {
                Rect input = (Rect)value;

                coll.Add(input.BottomLeft);
                coll.Add(input.BottomRight);
                coll.Add(input.TopRight);
                coll.Add(input.TopLeft);
            }

            // Return the value to pass to the target.
            return coll;
        }

        // ConvertBack is not implemented for a OneWay binding.
        public object ConvertBack(object value, Type targetType,
            object parameter, System.Globalization.CultureInfo culture)
        {
            return parameter;
        }
    }
}
