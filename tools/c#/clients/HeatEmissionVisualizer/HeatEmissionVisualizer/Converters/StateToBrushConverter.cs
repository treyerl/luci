﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;

namespace HeatEmissionVisualizer.Converters
{
    class StateToBrushConverter : IValueConverter
    {

        #region IValueConverter Members

        // Define the Convert method to compare a selected string to 
        // a given string.
        public object Convert(object value, Type targetType,
            object parameter, System.Globalization.CultureInfo culture)
        {
            SolidColorBrush brush = new SolidColorBrush();

            int state = 0;

            if (value != null && value is int)
            {
                state = (int)value;
            }

            switch (state)
            {
                case 0:
                    brush.Color = Colors.Black;
                    break;
                case 1:
                    brush.Color = Colors.Yellow;
                    break;
                case 2:
                    brush.Color = Colors.Green;
                    break;
                case 10:
                    brush.Color = Colors.Red;
                    break;
                default:
                    brush.Color = Colors.White;
                    break;
            }

            // Return the value to pass to the target.
            return brush;
        }

        // ConvertBack is not implemented for a OneWay binding.
        public object ConvertBack(object value, Type targetType,
            object parameter, System.Globalization.CultureInfo culture)
        {
            return parameter;
        }

        #endregion
    }
}
