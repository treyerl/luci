﻿using HeatEmissionVisualizer.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HeatEmissionVisualizer.GUI
{
    /// <summary>
    /// Interaction logic for RasterGrid.xaml
    /// </summary>
    public partial class RasterGrid : UserControl
    {
        public static DependencyProperty ViewModelProperty =
            DependencyProperty.Register(
                "ViewModel",
                typeof(RasterGridVM),
                typeof(RasterGrid),
                null
            );

        public RasterGridVM ViewModel
        {
            get { return (RasterGridVM)GetValue(ViewModelProperty); }
            set { SetValue(ViewModelProperty, value); }
        }

        public RasterGrid()
        {
            InitializeComponent();

            Loaded += RasterGrid_Loaded;
        }

        void RasterGrid_Loaded(object sender, RoutedEventArgs e)
        {
            if (DataContext != null && DataContext is RasterGridVM)
            {
                ViewModel = (RasterGridVM)DataContext;
            }

            initCanvas();
        }

        private void initCanvas()
        {
            //Buildings = ViewModel.AllBuildings[0];
            gridCanvas.Children.Clear();

            int deltaX = 10;
            int deltaY = 10;

//            int numCols = (int)Math.Ceiling(gridCanvas.ActualWidth / deltaX);
//            int numRows = (int)Math.Ceiling(gridCanvas.ActualHeight / deltaY);

            int numCols = (int)Math.Ceiling(LayoutGrid.ActualWidth / deltaX);
            int numRows = (int)Math.Ceiling(LayoutGrid.ActualHeight / deltaY);

            double scaleX = (double)numCols / 40.0;
            double scaleY = (double)numRows / 25.0;
            double scale = Math.Min(scaleX, scaleY);

            scaleTransform.ScaleX = scale;
            scaleTransform.ScaleY = scale;

            numCols =  (int)Math.Ceiling(numCols / scale);
            numRows = (int)Math.Ceiling(numRows / scale);

            for (int c = 0; c < numCols; c++)
            {
                Rectangle rect = new Rectangle()
                {
                    Width = 1,
                    Height = LayoutGrid.ActualHeight,
                    Stroke = new SolidColorBrush(Colors.DarkGray),
                    StrokeThickness = 1
                };

                Canvas.SetTop(rect, 0);
                Canvas.SetLeft(rect, c * deltaX);

                gridCanvas.Children.Add(rect);
            }

            for (int r = 0; r < numRows; r++)
            {
                Rectangle rect = new Rectangle()
                {
                    Width = LayoutGrid.ActualWidth,
                    Height = 1,
                    Stroke = new SolidColorBrush(Colors.DarkGray),
                    StrokeThickness = 1
                };

                Canvas.SetTop(rect, r * deltaY);
                Canvas.SetLeft(rect, 0);

                gridCanvas.Children.Add(rect);
            }
        }

        private void LayoutGrid_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            initCanvas();
        }

    }
}
