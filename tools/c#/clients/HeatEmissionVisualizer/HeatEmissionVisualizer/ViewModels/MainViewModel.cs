﻿using LucyCommunication;
using LucyGeoJsonSuite;
using Microsoft.Practices.Prism.ViewModel;
using NetTopologySuite.Geometries;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using uPLibrary.Networking.M2Mqtt;

namespace HeatEmissionVisualizer.ViewModels
{
    public class MainViewModel : NotificationObject
    {
        private Communication2Lucy cl = new Communication2Lucy();

        private bool _connected2Luci = false;
        private MqttClient mqttClient;        

        private string service_topic;
        private string scenario_topic;
        private string service_scenario_topic;

      //  private int SObjID = 134;
        private int ScID;

        private int _sObjID;
        public int SObjID
        {
            get { return _sObjID; }
            set
            {
                if (value != _sObjID)
                {
                    if (value >= 0)
                    {
                        hook2SObjID(value);
                    }

                    _sObjID = value;
                }
            }
        }

        public string LuciInfo { get; set; }

        public RasterGridVM GridVM { get; set; }

        public MainViewModel() {
            init();
        }

        private void init()
        {
            GridVM = new RasterGridVM();

            SObjID = Settings.SObjID;

            string result = "Connection to Lucy was ";
//            string host = "localhost";

            if (!cl.connect(Settings.LucyHost, 7654))
            {
                result += "not ";
                _connected2Luci = false;
            }
            else
                _connected2Luci = true;

            result += "successful! \n";

            Console.Out.WriteLine(result);

            if (_connected2Luci)
            {

                Console.Out.WriteLine("-> authenticate\n");
                JProperty result2 = cl.authenticate("lukas", "1234");
                result = result2.Name + ": " + result2.Value + "\n";
                Console.Out.WriteLine(result);

                Guid newguid = Guid.NewGuid();
                string identifier = newguid.ToString("N");

                mqttClient = new MqttClient(Settings.LucyHost);
                mqttClient.Connect("HeatEmissionVisualizer_" + identifier);
                mqttClient.MqttMsgPublishReceived += mqttClient_MqttMsgPublishReceived;

               // int SObjID = 109;

                /*
                string actionInfo = "{'action':'get_infos_about', 'SObjID':" + SObjID + "}";
                JObject info = cl.sendAction2Lucy(actionInfo);

                ScID = info.Value<JToken>("result").Value<int>("ScID");

                string mqtt_topic = info.Value<JToken>("result").Value<string>("mqtt_topic");

                service_topic = mqtt_topic.Substring(0, mqtt_topic.Length - 1);
                scenario_topic = "/Lucy/" + ScID + "/new_hash";

                if (mqttClient.IsConnected)
                {
                    mqttClient.Subscribe(new string[] { service_topic }, new byte[] { 1 });
                    mqttClient.Subscribe(new string[] { scenario_topic }, new byte[] { 1 });
                }



                Console.Out.WriteLine(info);
*/
                hook2SObjID(SObjID);
            }

            LuciInfo = result;
            RaisePropertyChanged("LuciInfo");
        }

        private void hook2SObjID(int newSObjID)
        {
            if (!_connected2Luci || newSObjID < 0)
                return;

            try
            {
                if (SObjID >= 0 && service_topic != null && service_topic.Length > 0 && scenario_topic != null && scenario_topic.Length > 0)
                {
                    mqttClient.Unsubscribe(new string[] { service_topic });
                    mqttClient.Unsubscribe(new string[] { scenario_topic });
                }

                string actionInfo = "{'action':'get_infos_about', 'SObjID':" + newSObjID + "}";
                JObject info = cl.sendAction2Lucy(actionInfo);

                ScID = info.Value<JToken>("result").Value<int>("ScID");

                string mqtt_topic = info.Value<JToken>("result").Value<string>("mqtt_topic");

                service_topic = mqtt_topic.Substring(0, mqtt_topic.Length - 1);
                scenario_topic = "/Lucy/" + ScID + "/new_hash";
                service_scenario_topic = "/Lucy/+/" + newSObjID + "/new_hash";

                if (mqttClient.IsConnected)
                {
                    mqttClient.Subscribe(new string[] { service_topic }, new byte[] { 1 });
                    mqttClient.Subscribe(new string[] { scenario_topic }, new byte[] { 1 });
                    mqttClient.Subscribe(new string[] { service_scenario_topic }, new byte[] { 1 });
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void clearResults()
        {
            if (GridVM.Submissions != null)
            {
                GridVM.Submissions.Clear();
                RaisePropertyChanged("Submissions");
            }
        }

        private void showResults()
        {
            if (GridVM.Submissions == null)
                GridVM.Submissions = new ObservableCollection<SubmissionResult>();
            else
                GridVM.Submissions.Clear();

            string msg = "{'action':'get_service_outputs','SObjID':" + SObjID + "}";

            JObject result = cl.sendAction2Lucy(msg);
            if (result != null)
            {
                JToken outputs = result.Value<JToken>("result").Value<JToken>("outputs");
                if (outputs != null)
                {
                    JArray buffer = (JArray)JsonConvert.DeserializeObject(outputs.Value<JObject>("outputVals").Value<String>("value"));

                    for (int c = 0; c < 85; c++)
                    {
                        for (int r = 0; r < 55; r++)
                        {
                            if ((int)buffer[c][r] > 0)
                            {
                                int valX = 10 * c;
                                int valY = 10 * r;
                                int width = 10;
                                int height = 10;

                                System.Windows.Rect Rect = new System.Windows.Rect(valX, valY, width, height);
                                SolidColorBrush br = new SolidColorBrush();


                                if ((int)buffer[c][r] >= 18)
                                    br.Color = Colors.DarkRed;
                                else if ((int)buffer[c][r] >= 15)
                                    br.Color = Colors.Red;
                                else if ((int)buffer[c][r] >= 12)
                                    br.Color = Colors.DarkOrange;
                                else if ((int)buffer[c][r] >= 9)
                                    br.Color = Colors.OrangeRed;
                                else if ((int)buffer[c][r] >= 6)
                                    br.Color = Colors.Orange;
                                else
                                    br.Color = Colors.Yellow;

                                GridVM.Submissions.Add(new SubmissionResult() { rect = Rect, brush = br });
                            }
                        }
                    }
                }
            }
            RaisePropertyChanged("Submissions");
        }

        private void showBuildings()
        {
            clearResults();

            if (GridVM.Buildings == null)
                GridVM.Buildings = new ObservableCollection<Rect>();
            else
                GridVM.Buildings.Clear();

            string actionScenario = "{'action':'get_scenario', 'ScID':" + ScID + "}";
            JObject scenario = cl.sendAction2Lucy(actionScenario);

            var strjson = ((JToken)(scenario.Value<JToken>("result").Value<JToken>("geometry").Value<JToken>("GeoJSON").Value<JToken>("geometry").Value<JToken>("features")).Last).Value<JObject>("geometry");
            /*
            string sss = "{" + strjson.Value<JToken>("type").Parent.ToString() + "," + strjson.Value<JToken>("coordinates").Parent.ToString() + "}";
            sss = sss.Replace("\n", "");
            sss = sss.Replace("\r", "");
            sss = sss.Replace(" ", "");
            sss = sss.Replace("MULTIPOLYGON", "MultiPolygon");

            string completeString = "";

            int endS = 0;
            int index = sss.IndexOfAny("0123456789".ToCharArray());
            while (index > 0)
            {
                completeString += sss.Substring(endS, index - endS);
                endS = sss.IndexOfAny("[],".ToCharArray(), index);
                if (endS > 0)
                {
                    string part = sss.Substring(index, endS - index);
                    if (!part.Contains("."))
                    {
                        int val = 0;
                        if (Int32.TryParse(part, out val))
                        {
                            part = val + ".0";
                        }
                    }
                    completeString += part;

                    index = sss.IndexOfAny("0123456789".ToCharArray(), endS);
                }
            }
            if (endS < sss.Length)
            {
                completeString += sss.Substring(endS, sss.Length - endS);
            }
            */

            LucyGeoJsonReader reader = new LucyGeoJsonReader();
            MultiPolygon ccc = reader.Read<MultiPolygon>(strjson.ToString(Formatting.None));

            //            int[][] buildingData = new int[56][];

            // 500 * 800                        
            //            for (int bd = 0; bd < 56; bd++)
            //                buildingData[bd] = new int[4];

            int numPoly = ccc.Count;
            for (int i = 0; i < numPoly; i++)
            {
                int x1 = (int)ccc[i].Coordinates[0].Y;
                int y1 = (int)ccc[i].Coordinates[0].X;
                int x3 = (int)ccc[i].Coordinates[2].Y;
                int y3 = (int)ccc[i].Coordinates[2].X;

                /*
                buildingData[i][0] = x1;
                buildingData[i][1] = x3;
                buildingData[i][2] = y1;
                buildingData[i][3] = y3;
                */

                Rect r = new Rect(new System.Windows.Point(x1, y1), new System.Windows.Point(x3, y3));
                GridVM.Buildings.Add(r);
            }

            RaisePropertyChanged("Buildings");
        }

        void mqttClient_MqttMsgPublishReceived(object sender, uPLibrary.Networking.M2Mqtt.Messages.MqttMsgPublishEventArgs e)
        {
            if (e.Topic == service_topic)
            {
                string message = Encoding.UTF8.GetString(e.Message);
                if (message == "DONE")
                {
                    Console.Out.WriteLine("DOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOONE");

                    System.Windows.Application.Current.Dispatcher.Invoke((Action)(delegate
                    {
                        showResults();
                    }));

                }
            }
            else if (e.Topic == scenario_topic)
            {
                Console.Out.WriteLine("update scenario");

                System.Windows.Application.Current.Dispatcher.Invoke((Action)(delegate
                {
                    showBuildings();
                }));
            }
            else if (e.Topic.StartsWith("/Lucy/")) {
                int pos = e.Topic.IndexOf("/", 6);
                if (pos > 6)
                {
                    ScID = Int16.Parse(e.Topic.Substring(6, pos-6));

                    scenario_topic = "/Lucy/" + ScID + "/new_hash";
                    mqttClient.Subscribe(new string[] { scenario_topic }, new byte[] { 1 });

                    System.Windows.Application.Current.Dispatcher.Invoke((Action)(delegate
                    {
                        showBuildings();
                    }));
                }
            }
        }
    }
}
