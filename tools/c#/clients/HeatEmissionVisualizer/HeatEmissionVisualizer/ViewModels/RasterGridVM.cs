﻿using Microsoft.Practices.Prism.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace HeatEmissionVisualizer.ViewModels
{
    public class SubmissionResult
    {
        public Rect rect { get; set; }
        public SolidColorBrush brush { get; set; }
    }

    public class RasterGridVM : NotificationObject
    {
        public ObservableCollection<Rect> Buildings { get; set; }

        private ObservableCollection<SubmissionResult> _submissions;
        public ObservableCollection<SubmissionResult> Submissions//{ get; set; }
        {
            get { return _submissions; }
            set
            {
                _submissions = value;
                RaisePropertyChanged("Submissions");
            }
        }

        public RasterGridVM() {
            Buildings = new ObservableCollection<System.Windows.Rect>();
            Submissions = new ObservableCollection<SubmissionResult>();
        }
    }
}
