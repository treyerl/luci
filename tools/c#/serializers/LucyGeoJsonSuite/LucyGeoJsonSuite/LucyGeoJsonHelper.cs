﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LucyGeoJsonSuite
{
    static class LucyGeoJsonHelper
    {

        public static string makeMeFloat(string input)
        {
            // maybe this is a simpler solution :)
            return Regex.Replace(input, @"(?<=[^\d\.])(\d+)(?=[^\d\.])", "$1.0");

            string result = "";

            int endS = 0;
            int index = input.IndexOfAny("0123456789".ToCharArray());
            while (index > 0)
            {
                result += input.Substring(endS, index - endS);
                endS = input.IndexOfAny("[],".ToCharArray(), index);
                if (endS > 0)
                {
                    string part = input.Substring(index, endS - index);
                    if (!part.Contains("."))
                    {
                        int val = 0;
                        if (Int32.TryParse(part, out val))
                        {
                            part = val + ".0";
                        }
                    }
                    result += part;

                    index = input.IndexOfAny("0123456789".ToCharArray(), endS);
                }
            }
            if (endS < input.Length)
            {
                result += input.Substring(endS, input.Length - endS);
            }

            return result;
        }
    }
}
