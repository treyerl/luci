﻿using NetTopologySuite.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LucyGeoJsonSuite
{
    public class LucyGeoJsonReader : GeoJsonReader
    {
        public LucyGeoJsonReader()
        {
        }

        public new TObject Read<TObject>(string input) where TObject : class
        {
            input = LucyGeoJsonHelper.makeMeFloat(input);
            return base.Read<TObject>(input);
        }
    }
}

