﻿using GeoAPI.Geometries;
using NetTopologySuite.Features;
using NetTopologySuite.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LucyGeoJsonSuite
{
    public class LucyGeoJsonWriter : GeoJsonWriter
    {
        public LucyGeoJsonWriter()
        {
        }

        public new string Write(FeatureCollection featureCollection)
        {
            return base.Write(featureCollection);
        }

        public new string Write(IFeature feature)
        {
            return base.Write(feature);
        }

        public new string Write(IGeometry geometry)
        {
            return base.Write(geometry);
        }

        public new string Write(object value)
        {
            return base.Write(value);
        }
    }
}
