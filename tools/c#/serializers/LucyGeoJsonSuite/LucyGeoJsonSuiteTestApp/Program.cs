﻿using System;
using System.Text.RegularExpressions;
using LucyGeoJsonSuite;
using NetTopologySuite.Geometries;
using NetTopologySuite.IO;

namespace LucyGeoJsonSuiteTestApp
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var input =
                "{'coordinates':[[[1.0,1.0,0.0],[500.0,1.0,0.0],[500.0,400.0,0.0],[1.0,400.0,0.0],[1.0,1.0,0.0]]],'type':'Polygon'}";
            var input1 =
                "{  'coordinates': [    [      [        49.569874039804851,        126.11922412189128,        0      ],      [        43.528067610317606,        165.66029865292302,        0      ],      [        142.38075393789694,        180.76481472664111,        0      ],      [        148.42256036738419,        141.22374019560937,        0      ],      [        49.569874039804851,        126.11922412189128,        0      ]    ]  ],  'type': 'Polygon'}";
            var input2 = "{'coordinates':[[[1,1,0],[500,1,0],[500,400,0],[1,400,0],[1,1,0]]],'type':'Polygon'}";
            var input3 = "{'type':'Polygon','coordinates':[[[1,1,0],[500,1,0],[500,400,0],[1,400,0],[1,1,0]]]}";

            LucyGeoJsonReader readerNew = new LucyGeoJsonReader();
            Polygon result = readerNew.Read<Polygon>(input);
            Polygon result2 = new GeoJsonReader().Read<Polygon>(fixJson(input));
            Console.Out.WriteLine(input);
            Console.Out.WriteLine(fixJson(input));
            Console.Out.WriteLine(result.ToString());
            Console.Out.WriteLine(result2.ToString());
            Console.Out.WriteLine(new LucyGeoJsonWriter().Write(result));
            Console.Out.WriteLine(new GeoJsonWriter().Write(result2));

            result = readerNew.Read<Polygon>(input1);
            result2 = new GeoJsonReader().Read<Polygon>(fixJson(input1));
            Console.Out.WriteLine(input1);
            Console.Out.WriteLine(fixJson(input1));
            Console.Out.WriteLine(result.ToString());
            Console.Out.WriteLine(result2.ToString());
            Console.Out.WriteLine(new LucyGeoJsonWriter().Write(result));
            Console.Out.WriteLine(new GeoJsonWriter().Write(result2));

            result = readerNew.Read<Polygon>(input2);
            result2 = new GeoJsonReader().Read<Polygon>(fixJson(input2));
            Console.Out.WriteLine(input2);
            Console.Out.WriteLine(fixJson(input2));
            Console.Out.WriteLine(result.ToString());
            Console.Out.WriteLine(result2.ToString());
            Console.Out.WriteLine(new LucyGeoJsonWriter().Write(result));
            Console.Out.WriteLine(new GeoJsonWriter().Write(result2));

            result = readerNew.Read<Polygon>(input3);
            result2 = new GeoJsonReader().Read<Polygon>(fixJson(input3));
            Console.Out.WriteLine(input3);
            Console.Out.WriteLine(fixJson(input3));
            Console.Out.WriteLine(result.ToString());
            Console.Out.WriteLine(result2.ToString());
            Console.Out.WriteLine(new LucyGeoJsonWriter().Write(result));
            Console.Out.WriteLine(new GeoJsonWriter().Write(result2));


        }

        private static string fixJson(string input)
        {
            return Regex.Replace(Regex.Replace(input, @"(?<=[^\d\.])(\d+)(?=[^\d\.])", "$1.0"), @"\s", "");
        }
    }
}