﻿using CPlan.Geometry;
using CPlan.Isovist2D;
using NetTopologySuite.Geometries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LucySerializers
{
    public class IsovistField2DWrapper
    {
        private MultiPoint m_points;
        private MultiLineString m_obstacles;
        private float m_precision;
        private List<Dictionary<ResultsIsovist, double>> m_results;

        public List<Dictionary<ResultsIsovist, double>> Results
        {
            get { return m_results; }
            set { m_results = value; }
        }

        public MultiLineString Obstacles
        {
            get { return m_obstacles; }
            set { m_obstacles = value; }
        }

        public float Precision
        {
            get { return m_precision; }
            set { m_precision = value; }
        }

        //by default all Properties are serialized but can be exluded using following annotation        
        //[Newtonsoft.Json.JsonIgnore]
        public MultiPoint Points
        {
            get { return m_points; }
            set { m_points = value; }
        }

        [Newtonsoft.Json.JsonConstructor]
        public IsovistField2DWrapper(MultiPoint points, MultiLineString obstacles, List<Dictionary<ResultsIsovist, double>> results, float precision)
        {
            m_points = points;
            m_precision = precision;
            m_obstacles = obstacles;
            m_results = results;
        }

        public IsovistField2DWrapper(IsovistField2D isovist) : this(GeometryConverter.ToMultiPoint(isovist.Points), GeometryConverter.ToMultiLineString(isovist.ObstacleLines), isovist.Results, isovist.Precision) { }
    }

}
