﻿using CPlan.Isovist2D;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LucySerializers
{
    public class IsovistResultsWrapper
    {
        private List<Dictionary<ResultsIsovist, double>> _results;

        [Newtonsoft.Json.JsonConstructor]
        public IsovistResultsWrapper(List<Dictionary<ResultsIsovist, double>> results)
        {
            _results = results;
        }

        public List<Dictionary<ResultsIsovist, double>> Results
        {
            get { return _results; }
            set { _results = value; }
        }
    }
}
