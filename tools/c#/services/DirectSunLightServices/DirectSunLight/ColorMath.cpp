#include "stdafx.h"
#include <math.h>
#include "MyMath.h"
#include "ColorMath.h"

/////////////////////////////////////////////////////////////////////////////

//Farbfunktionen von http://semmix.pl/color/extrans

/*
double CColorMath::minimum(double r, double g, double b)
{
	double m;

	if (r<g) m = r; else m = g;
	if (b<m) m = b;

	return m;
}

double CColorMath::maximum(double r, double g, double b)
{
	double m;

	if (r>g) m = r; else m = g;
	if (b>m) m = b;

	return m;
}

double CColorMath::centre(double r, double g, double b)
{
	double m;
	while (true)
	{
		if ((r<g) && (r<b)) { if (g<b) m = g; else m = b; break; }
		if ((g<r) && (g<b)) { if (r<b) m = r; else m = b; break; }
		if (r<g) m = r; else m = g; break;
	}
	return m;
}

bool CColorMath::verify(double *v)
{
	if (*v<-eps) return false;
	if (*v>1+eps) return false;
	if (*v<=eps) *v = 0; else
	if (*v>=1-eps) *v = 1;
	return true;
}
*/
/*
void CColorMath::HueToRGB(double m0, double m2, double H,
									  double *R, double *G, double *B)
{
	double m1, F;
	int n;

   if ((H<-360) || (H>720)) H=0;
   while (H<0)    H += 360;
	while (H>=360) H -= 360;
	n = floor(H / 60);
	if (H<60)  F = H; else
	if (H<180) F = H - 120; else
	if (H<300) F = H - 240; else F = H - 360;
	m1 = m0 + fabs(F/60) * (m2-m0);
	switch (n)
	{
	case 0: *R = m2; *G = m1; *B = m0; break;
	case 1: *R = m1; *G = m2; *B = m0; break;
	case 2: *R = m0; *G = m2; *B = m1; break;
	case 3: *R = m0; *G = m1; *B = m2; break;
	case 4: *R = m1; *G = m0; *B = m2; break;
	case 5: *R = m2; *G = m0; *B = m1; break;
	}
}
*/
/*
double CColorMath::RGBtoHue(double R, double G, double B)
{
	double F, H, m0, m1, m2;

	m2 = maximum(R,G,B);
	m1 = centre(R,G,B);
	m0 = minimum(R,G,B);
   if (fabs(m2-m0)<=eps)
      return 0;
   H=0;
	while (true)
	{
		if (fabs(m2 - m1) <= eps) //exist two max: m1=m2
		{
			if (fabs(R - G) <= eps) { H = 60; break; }
			if (fabs(G - B) <= eps) { H = 180; break; }
			H = 300; break;
		}

		//exist single man only
		F = 60 * (m1 - m0) / (m2 - m0);
		if (fabs(R - m2) <= eps) { if (G < B) H = -F; else H = F; break; }
		if (fabs(G - m2) <= eps) { if (B < R) H = -F; else H = F; H += 120; break; }
		if (R < G) H = -F; else H = F; H += 240; break; 
	}
	if (H<0) H += 360;

	return H;
}
*/
/*
void CColorMath::HueToRGB(double m0, double m2, double H,
									  double *R, double *G, double *B)
{
	double m1, F;
	int n;

   if ((H<-360) || (H>720)) H=0;
   while (H<0)    H += 360;
	while (H>=360) H -= 360;
	n = floor(H / 60);
	if (H<60)  F = H; else
	if (H<180) F = H - 120; else
	if (H<300) F = H - 240; else F = H - 360;
	m1 = m0 + (m2 - m0) * sqrt(fabs(F / 60));
	switch (n)
	{
	case 0: *R = m2; *G = m1; *B = m0; break;
	case 1: *R = m1; *G = m2; *B = m0; break;
	case 2: *R = m0; *G = m2; *B = m1; break;
	case 3: *R = m0; *G = m1; *B = m2; break;
	case 4: *R = m1; *G = m0; *B = m2; break;
	case 5: *R = m2; *G = m0; *B = m1; break;
	}
}
*/
/*
double CColorMath::RGBtoHue(double R, double G, double B)
{
	double F, H, m0, m1, m2;

	m2 = maximum(R,G,B);
	m1 = centre(R,G,B);
	m0 = minimum(R,G,B);
	while (true)
	{
		if (fabs(m2 - m1) <= eps) //exist two max: m1=m2
		{
			if (fabs(R - G) <= eps) { H = 60; break; }
			if (fabs(G - B) <= eps) { H = 180; break; }
			H = 300; break;
		}

		//exist single man only
		F = 60 * (m1 - m0) / (m2 - m0);
		if (fabs(R - m2) <= eps) {	H = 0   + F * (G - B); break; }
		if (fabs(G - m2) <= eps) {	H = 120 + F * (B - R); break; }
		H = 240 + F * (R - G); break; 
	}
	if (H<0) H += 360;

	return H;
}
*/
/*
void CColorMath::HSBtoRGB(double H, double S, double V,
									  double *R, double *G, double *B)
{
	double m0;
	*R = -1; *G = -1; *B = -1; 
	if (!verify(&V)) return;
	if (!verify(&S)) return;
	if (V <= eps2) { *R = 0; *G = 0; *B = 0; return; }
	m0 = (1 - S) * V;
	if (fabs(V - m0) <= eps2) { *R = V; *G = V; *B = V; return; }
	HueToRGB(m0,V,H,R,G,B);
}
*/
/*
void CColorMath::RGBtoHSB(double R, double G, double B,
									  double *H, double *S, double *V)
{
	double m0, m2;

	*H = 0; *S = -1; *V = -1;
	if (!verify(&R)) return;
	if (!verify(&G)) return;
	if (!verify(&B)) return;
	m0 = minimum(R,G,B);
	m2 = maximum(R,G,B);
	if (m2 <= eps2) { *S = 0; *V = 0; return; }
	*V = m2;
   //new
   *S = 1.0 - m0 / m2;
	if (*S <= eps2) { *S = 0; return; }
	*H = RGBtoHue(R,G,B);
}
*/
/*
void CColorMath::GetIttenColors(double angle,double *r, double *g, double *b)
{
   angle=angle/DSL_PI*180;
   while (angle<0) angle+=360;
   while (angle>360) angle-=360;

   if (angle==0)
   {
      //gelb
      (*r)=1; (*g)=1; (*b)=0;
      return;
   }
   if ((angle>0) && (angle<120))
   {
      //gelb-orange-rot
      (*r)=1; (*g)=(120.0-angle)/120.0; (*b)=0;
      return;
   }
   if (angle==120)
   {
      //rot
      (*r)=1; (*g)=0; (*b)=0;
      return;
   }
   if ((angle>120) && (angle<240))
   {
      //rot-violett-blau
      (*r)=(240.0-angle)/120.0; (*g)=0; (*b)=(angle-120.0)/120.0;
      return;
   }
   if (angle==240)
   {
      //blau
      (*r)=0; (*g)=0; (*b)=1;
      return;
   }
   if ((angle>240) && (angle<300))
   {
      //blau-gr�n
      (*r)=0; (*g)=(angle-240.0)/60.0; (*b)=(300.0-angle)/60.0;
      return;
   }
   if (angle==300)
   {
      //gr�n
      (*r)=0; (*g)=1; (*b)=0;
      return;
   }
   if ((angle>300) && (angle<360))
   {
      //gr�n-gelb
      (*r)=(angle-300.0)/60.0; (*g)=1.0; (*b)=0;
      return;
   }
}
*/
/*
void CColorMath::RGBToIttenAngle(double r,double g, double b, double * angle)
{
	double m0, m1, m2;

	m2 = maximum(r,g,b);
	m1 = centre(r,g,b);
	m0 = minimum(r,g,b);

   //wei� grau scharz
   if ((m0==m1) && (m1==m2))
   {
      (*angle)=0;
      return;
   }

   //Festfarben
   if (m1-m0<=eps)
   {
      if (m2-r<=eps) { (*angle)=120.0 /180.0*DSL_PI; return; } //rot
      if (m2-g<=eps) { (*angle)=300.0 /180.0*DSL_PI; return; } //gr�n
      (*angle)=240.0 /180.0*DSL_PI; //blau
      return;
   }
   if (m2-m1<=eps)
   {
      if (b-m0<=eps) { (*angle)=0.0 /180.0*DSL_PI; return; } //gelb
   }

   //gelb bis rot
   if ((m2-r<=eps) && (m1-g<=eps))
   {
      (*angle)=(1.0-(m1-m0)/(m2-m0))*120.0 /180.0*DSL_PI;
      return;
   }
   //rot bis blau
   if (g-m0<=eps)
   {
      //(*r)=(240.0-angle)/120.0; (*g)=0; (*b)=(angle-120.0)/120.0;
      r-=m0;
      b-=m0;
      //r+b=1
      if (r+b<=eps) 
      {
         (*angle)=0;
         return;
      }
      r/=(r+b);
      //b/=(r+b);

      (*angle)=(240.0-r*120.0) /180*DSL_PI;

      return;
   }
   //blau bis gr�n
   if (r-m0<=eps)
   {
      //(*r)=0; (*g)=(angle-240.0)/60.0; (*b)=(300.0-angle)/60.0;
      g-=m0;
      b-=m0;
      //g+b=1
      if (g+b<=eps) 
      {
         (*angle)=0;
         return;
      }
      g/=(g+b);
      //b/=(g+b);

      (*angle)=(g*60.0+240.0) /180*DSL_PI;

      return;
   }
   //gr�n bis gelb
   if ((m2-g<=eps) && (m1-r<=eps))
   {
      (*angle)=(300.0 + (m1-m0)/(m2-m0)*60.0) /180.0*DSL_PI;
      return;
   }

   (*angle)=0.0;
   return;
}
*/
/*
double CColorMath::GetAngleAxisDiff(double * angle,double * axis)
{
   double d;

   d=(*angle)-(*axis);
   if (d<-DSL_PI) d+=2*DSL_PI;
   if (d>DSL_PI)  d-=2*DSL_PI;

   return d;
}
*/
void CColorMath::ApplyColorSymetry(DoubleVector * angle,IntVector constant_angle)
{
   DoubleVector delta,saved_delta;
   DoubleVector sort_angle;
   IntVector sort_nr;
   double min_delta,d,d2,old_delta;
   double axis,zw,min_axis;
   int anz,z,z2,z3,z4,z5,z6,z7,min_z;
   BoolVector done;
   bool set_axis,next_z;

   //delta & done initialisieren
   for (z=0; z<(int)(*angle).size(); z++)
   {
      delta.push_back(0.0);
      saved_delta.push_back(0.0);
      done.push_back(false);
      sort_nr.push_back(z);
   }
   min_delta=-1;
   old_delta=-1;

   //Winkel sortieren
   sort_angle=*angle;
   for (z=0; z<(int)sort_angle.size()-1; z++)
      for (z2=z+1; z2<(int)sort_angle.size(); z2++)
         if (sort_angle[z]>sort_angle[z2])
         {
            zw=sort_angle[z];
            sort_angle[z]=sort_angle[z2];
            sort_angle[z2]=zw;

            z3=sort_nr[z];
            sort_nr[z]=sort_nr[z2];
            sort_nr[z2]=z3;
         }
   //alle Achsen durchgehen
   z=0;
   set_axis=true;
   while (z<(int)sort_angle.size()*2)
   {
      next_z=false;
      z2=(int)floor(z/2.0);
      z3=z2+ (z % 2);
      if (z3>=(int)sort_angle.size())
         z3-=(int)sort_angle.size();
      if (set_axis)
      {
         if (z2==z3)
         {
            axis=sort_angle[z2];
         }
         else
         {
            if (sort_angle.size()<=2)
            {
               //Keine dazwischenliegenden Achsen bei nur 2 Farben
               z++;
               continue;
            }
            if (sort_angle[z2]<sort_angle[z3])
               axis=(sort_angle[z2]+sort_angle[z3])/2;
            else
               axis=(sort_angle[z2]+sort_angle[z3]+2*DSL_PI)/2;
         }
      }
      else set_axis=true;
      //Achse ermittelt
      
      //Startwerte
      for (z4=0; z4<(int)sort_angle.size(); z4++)
      {
         delta[z4]=0.0;
         done[z4]=false;
      }

      //alle Farb-angles auf Symetrie pr�fen - Symetrieachsen Farben finden
      if (z2==z3)
      {
         d=GetAngleAxisDiff(&sort_angle[z2],&axis);
         delta[z2]=-d;
         done[z2]=true;

         for (z7=0; z7<(int)constant_angle.size(); z7++)
            if ((sort_nr[z2]==constant_angle[z7]) && (fabs(d)>eps))
            {
               next_z=true;
               break;
            }
         if (next_z)
         {
            z++;
            continue;
         }
      }
      if (((z2!=z3) && (sort_angle.size()%2==1)) || ((z2==z3) && (sort_angle.size()%2==0)))
      {
         z4=z3+(int)floor(sort_angle.size()/2.0);
         if (z4>=(int)sort_angle.size())
            z4-=(int)sort_angle.size();
         d=GetAngleAxisDiff(&sort_angle[z4],&axis);
         delta[z4]=DSL_PI-d;
         if (delta[z4]<-DSL_PI) delta[z4]+=2*DSL_PI;
         if (delta[z4]>DSL_PI) delta[z4]-=2*DSL_PI;
         done[z4]=true;

         for (z7=0; z7<(int)constant_angle.size(); z7++)
            if ((sort_nr[z4]==constant_angle[z7]) && (fabs(d-DSL_PI)>eps))
            {
               next_z=true;
               break;
            }
         if (next_z)
         {
            z++;
            continue;
         }
      }
      if (z2==z3)
         anz=(int)ceil((double)sort_angle.size()/2.0);
      else
         anz=(int)floor((double)sort_angle.size()/2.0);
      for (z4=0; z4<anz;z4++)
      {
         z5=z3+z4;
         if (z5>=(int)sort_angle.size())
            z5-=(int)sort_angle.size();
         z6=z2-z4;
         if (z6<0)
            z6+=(int)sort_angle.size();

         if ((!done[z5]) && (!done[z6]))
         {
            d=GetAngleAxisDiff(&sort_angle[z5],&axis);
            d2=GetAngleAxisDiff(&sort_angle[z6],&axis);

            for (z7=0; z7<(int)constant_angle.size(); z7++)
               if (sort_nr[z5]==constant_angle[z7])
               {
                  done[z5]=true;
                  done[z6]=true;
                  delta[z5]=0.0;
                  delta[z6]=-(d+d2);
                  break;
               }
            for (z7=0; z7<(int)constant_angle.size(); z7++)
               if (sort_nr[z6]==constant_angle[z7])
               {
                  if ((!done[z5]) && (!done[z6]))
                  {
                     done[z5]=true;
                     done[z6]=true;
                     delta[z6]=0.0;
                     delta[z5]=-(d+d2);
                  }
                  else
                  {
                     if (fabs(delta[z6])>eps)
                        next_z=true;
                     delta[z6]=0.0;
                  }
                  break;
               }
            if (next_z)
               break;
            if ((!done[z5]) && (!done[z6]))
            {
               delta[z5]=-(d+d2)/2;
               delta[z6]=-(d+d2)/2;
            }
            if (delta[z5]<-DSL_PI) delta[z5]+=2*DSL_PI;
            if (delta[z5]>DSL_PI) delta[z5]-=2*DSL_PI;
            if (delta[z6]<-DSL_PI) delta[z6]+=2*DSL_PI;
            if (delta[z6]>DSL_PI) delta[z6]-=2*DSL_PI;
         }
      }
      if (next_z)
      {
         z++;
         continue;
      }
      for (z4=0; z4<(int)done.size();z4++)
         if (!done[z4])
         {
            z++;
            continue;
         }
      //alle deltas bestimmt
      d=0;
      d2=0;
      for (z4=0; z4<(int)delta.size();z4++)
      {
         d+=fabs(delta[z4]);
         d2+=delta[z4];
      }
      if ((d<min_delta) || (min_delta<0))
      {
         min_delta=d;
         min_z=z;
         min_axis=axis;
         for (z4=0; z4<(int)delta.size(); z4++)
            saved_delta[z4]=delta[z4];
      }
      if ((fabs(d2)>eps) && (sort_angle.size()>2) && ((fabs(d2)<old_delta) || (old_delta<0)))
      {
         axis+=d2/(double)delta.size();
         while (axis<0) axis+=2*DSL_PI;
         while (axis>2*DSL_PI) axis-=2*DSL_PI;
         set_axis=false;
         old_delta=fabs(d2);
      }
      else
      {
         old_delta=-1;
         z++;
      }
   }
   if (min_delta<0)
      return;

   //Verschiebung
   for (z=0; z<(int)saved_delta.size(); z++)
   {
      (*angle)[sort_nr[z]]+=saved_delta[z];
      if ((*angle)[sort_nr[z]]<0) (*angle)[sort_nr[z]]+=2*DSL_PI;
      if ((*angle)[sort_nr[z]]>2*DSL_PI) (*angle)[sort_nr[z]]-=2*DSL_PI;
   }
}

/*
bool CColorMath::ValueIn(CString s, int item)
{
   bool erg;
   CString tmp;

   if (item<9)
      tmp.Format("#0%d",item+1);
	else
      tmp.Format("#%d",item+1);

   erg=(s.Find(tmp,0) != -1);

   return erg;
}
*/
/*
void CColorMath::MaximizeIttenColors(double *r, double *g, double *b)
{
	double m0, m1, m2;

	m2 = maximum(*r,*g,*b);
	m1 = centre(*r,*g,*b);
	m0 = minimum(*r,*g,*b);

   if ((m2>0.0) && (m2<1.0))
   {
      if (fabs(m2-*r)<=eps) (*r)=1.0;
      if (fabs(m2-*g)<=eps) (*g)=1.0;
      if (fabs(m2-*b)<=eps) (*b)=1.0;

      if (fabs(m1-*r)<=eps) (*r)=m1/m2;
      if (fabs(m1-*g)<=eps) (*g)=m1/m2;
      if (fabs(m1-*b)<=eps) (*b)=m1/m2;

      if (fabs(m0-*r)<=eps) (*r)=m0/m2;
      if (fabs(m0-*g)<=eps) (*g)=m0/m2;
      if (fabs(m0-*b)<=eps) (*b)=m0/m2;
   }
}
*/
