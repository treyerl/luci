#pragma once

#include "stdafx.h"
#include "HDRImage.h"
#include "OpenGLContext.h"
#include "Geom3d.h"
#include "FramebufferObject.h"
#include "OpenCL.h"

namespace DSL
{

	#define MAX_DISPLAYLIST_INSTRUCTIONS 2000

	struct DisplayListVector
	{
		typedef std::vector<unsigned int> IntVector;

		DisplayListVector()
		{
			m_counter = 0;
			m_mode = GL_COMPILE;
		}

		~DisplayListVector()
		{
			Clear();
		}

		unsigned int GetNewList()
		{
			unsigned int value = glGenLists(1);
			m_lists.push_back( value );
			return value;
		}

		void Clear()
		{
			IntVector::iterator l, e;
			l = m_lists.begin();
			e = m_lists.end();
			for(; l!=e; l++)
				glDeleteLists(*l, 1);
			m_lists.clear();
		}

		void Show() const
		{
			if (m_lists.empty())
				assert(0);

			IntVector::const_iterator l, e;
			l = m_lists.begin();
			e = m_lists.end();
			for(; l!=e; l++)
				glCallList(*l);
		}

		void Reset()
		{
			Clear();
			ResetCounter();
		}

		void IncCounter()
		{
			++m_counter;
		}

		void IncCounter(int value)
		{
			m_counter += value;
		}

		int GetCounter() const
		{
			return m_counter;
		}

		void ResetCounter()
		{
			m_counter = 0;
		}

		void SetMode(GLenum mode)
		{
			if (!((mode == GL_COMPILE) || (mode == GL_COMPILE_AND_EXECUTE)))
				assert(0);

			m_mode = mode;
		}

		GLenum GetMode() const
		{
			return m_mode;
		}

	private:
		IntVector      m_lists;       //die Listen Nummnern
		unsigned int   m_counter;     //Z�hler f�r Listen Trennung
		GLenum         m_mode;        //GL_COMPLIE || GL_COMPILE_AND_EXECUTE
	};

	class CDirectSunLight
	{
	public:
		struct PolygonRenderInfo
		{
			CGeom3d::TriangleVector m_vTriangles;
			CGeom3d::Vec3d m_normale;
			float m_fTransparency;
			PolygonRenderInfo(CGeom3d::TriangleVector &triangles, CGeom3d::Vec3d &normale, float fTransparency): m_vTriangles(triangles), m_normale( normale ), m_fTransparency( fTransparency ) {};
		};
		typedef std::map<size_t, PolygonRenderInfo*> GeometryMap;
		typedef std::pair<size_t, PolygonRenderInfo*> GeometryPair;

		enum CalculationMode
		{
			Leuchtdichte_kcd_per_square_meter = 0,
			Beleuchtungsstaerke_klux
		};

	private:
		CHDRImage * m_pHDRImage;		// the HDRI
		COpenGLContext * m_pContext;	// the Render Context
		AS::FramebufferObject * m_pFBO;	// the FBO
		float * m_pReadBuffer;			// the FBO read buffer
		size_t m_iCubeSize;				// render cube size
		float * m_pSkyCubeMap;          // sky cube map in 6 x 1 of m_iCubeSize
		GeometryMap m_vGeometryMap;     // id Triangles
		bool m_bGeometryChanged;        // flag: Geometrie �nderungen wurden angewendet
		DisplayListVector m_vDisplayList; // DisplayListe
		GLuint m_iTexName;              // Texturname
		bool m_bTexInited;              // textur initialisiert
		float * m_pWichtung;            // Wichtungsmap
		unsigned int m_iOptiFactorX;    // OpenCL Optimierung in X (beim rendern von vielen Punkten)
		unsigned int m_iOptiFactorY;    // OpenCL Optimierung in Y (beim rendern von vielen Punkten)
		Vec3d m_vNormale;               // Normale f�r Beleuchtungsst�rke (Lux) Berechnung
		CalculationMode m_iCalculationMode; // Berechnungsmodus (Leuchtdichte oder Beleuchtungsst�rke)
		size_t m_iCurrentPolygonID;     // letzte verwendete PolygonID f�r GetNewPolygonID

		float * GetFBOPixel(int x, int y) const;	// reads a pixel from m_pReadBuffer (slow)
		size_t GetNewPolygonID();					// liefert neue Poly ID f�r m_vGeometryMap
		void BuildGeometryDisplayList();            // baut und zeigt m_vGeometryMap als Displayliste
		void ShowGeometryDisplayList();             // zeigt m_vGeometryMap als Displayliste
		void RenderHDRISky();                       // rendert den Sky Cube
		float * GenerateFormfactorMap(int cube_size); // baut Wichtungsmap des Rendercube
		void OpenCLInit();                          // initialisiert die OpenCL Buffer und Variablen
		void OpenCLQuit();                          // gibt OpenCL Variablen frei
		void RunOpenCL(unsigned int iNumber, float *pRGB); // Auswertung der Renderbilder mit OpenCL
		void RenderDirectSunLight(int iOptiPosX, int iOptiPosY, Vec3d position, Vec3d direction);  // Berechungskern Teil 1

		//OpenCL stuff
		cl_float * m_pErg;
		cl_float * m_pErg2;
		cl_float * m_pWichtungOpenCL;
#ifdef _DEBUG_DEEP_OPENCL
		cl_float * m_pDebug; //debug
#endif

		cl_mem * m_dErg;
		cl_mem * m_dErg2;
		cl_mem * m_dWichtungOpenCL;
		cl_mem * m_dImageBuffer;
		cl_mem * m_dImage;
#ifdef _DEBUG_DEEP_OPENCL
		cl_mem * m_dDebug;
#endif

		unsigned int m_iBlockDX;
		unsigned int m_iBlockDY;
		unsigned int m_iBlock2DX;
		unsigned int m_iBlock2DY;

	public:

		CDirectSunLight();
		~CDirectSunLight();
		bool SetHDRImage(const char * name);																		// set the sky HDRI
		void SetResolution(int cubeSize);																			// init the OpenGL Context in this resolution
		void PrepareHDRCubeMap(float fNorthAngle = 0.0f);                                                           // berechnet die CubeMap vor
		void CalculateDirectSunLight(Vec3d position, Vec3d direction, float &fEnergyRed, float &fEnergyGreen, float &fEnergyBlue);	// calculate the energy at the given point
		void CalculateDirectSunLight(size_t iPointCount, Vec3d * pPosition, Vec3d * pDirection, float *pResult);	                    // calculate the energy at the given points
		void SetNormale(const Vec3d& normale);                                                                      // sets the normale for lux calculation
		void SetCalculationMode(CalculationMode mode);                                                              // sets the calculation mode (kcd/m� or klux (using normale cosinus))

		void DeleteAllGeometry();																					// l�scht alle Geometrie
		size_t AddPolygon(CGeom3d::Point_3d_Vector_Vector &points, float fTransparency);							// f�gt ein Polygon hinzu und liefert seine ID
		size_t AddPolygonTriangles(CGeom3d::Vec3d& normale, CGeom3d::TriangleVector &triangles, float fTransparency);// f�gt ein Polygon hinzu und liefert seine ID (schneller ?)
		void DeletePolygon(size_t id);																				// l�scht ein Polygon
		bool MovePolygon(size_t id, CGeom3d::Point_3d_Vector_Vector &newPoints);									// verschiebt ein Polygon

		bool StartOpenCLOnceAndForever();                                                                           // startet OpenCL

	};
}