#include "stdafx.h"
#include "ExtraWGLStuff.h"

//Declare your function pointer in a .cpp file
PFNWGLCREATEPBUFFERARBPROC		wglCreatePbufferARB;    
PFNWGLGETPBUFFERDCARBPROC		wglGetPbufferDCARB;  
PFNWGLRELEASEPBUFFERDCARBPROC	wglReleasePbufferDCARB;  
PFNWGLDESTROYPBUFFERARBPROC		wglDestroyPbufferARB;  
