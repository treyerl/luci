#include "stdafx.h"
#include "geom2d.h"

/*
CGeom2d::CGeom2d(void)
{
}

CGeom2d::~CGeom2d(void)
{
}
*/

/*
PROCEDURE Point_LL_2d (L1,L2:Line_2d; VAR P:Point_2d);
VAR
 Det,DetX,DetY: Real;
 Det1,Det2: Real;
BEGIN
 Det:= L1.a * L2.b - L2.a * L1.b;
 IF Abs (Det) < Eps6 THEN Err_2d:= 0
 ELSE BEGIN
  Det1:= L1.a * L1.y - L1.b * L1.x;
  Det2:= L2.a * L2.y - L2.b * L2.x;
  DetX:= -L1.a * Det2 + L2.a * Det1;
  DetY:= L2.b * Det1 - L1.b * Det2;
  P.x:= Detx/Det;
  P.y:= Dety/Det;
  Err_2d:= 1;
 END
END;
*/

/*
void CGeom2d::Point_LL_2d (Line_2d L1, Line_2d L2, Point_2d & P)
{
   double Det,DetX,DetY;
   double Det1,Det2;

   Det= L1.v.x * L2.v.y - L2.v.x * L1.v.y;
   if (fabs(Det) < Eps6) 
      Err_2d= 0;
   else 
   {
      Det1= L1.v.x * L1.p.y - L1.v.y * L1.p.x;
      Det2= L2.v.x * L2.p.y - L2.v.y * L2.p.x;
      DetX= -L1.v.x * Det2 + L2.v.x * Det1;
      DetY= L2.v.y * Det1 - L1.v.y * Det2;
      P.x= DetX/Det;
      P.y= DetY/Det;
      Err_2d= 1;
   }
}
*/