#pragma once

#pragma comment(lib, "user32.lib")
#pragma comment(lib, "gdi32.lib")

#include "Window.h"
#include "WindowManager.h"
#include <windows.h>
#include <gl\gl.h>
#include <gl\glu.h>


using namespace std;


class MyWin : public GLWUT::Window 
{
	public:
		MyWin(GLWUT::WindowManager *wm, DWORD devnum) : GLWUT::Window(wm,devnum) {}
		void resize(int w, int h) {
			glViewport(0,0,w,h);
		}
		void display() {
			glClearColor(1,0,0,0);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		}
		void idle() {}

		void keydown(unsigned char key) {
			switch(key) {
			case VK_ESCAPE:
				close();
				break;
			}
		}
};

static MyWin * m_pStaticWin = NULL;
static GLWUT::WindowManager  *m_pWinmg = NULL;
static bool m_bDone = false;

class CMyHiddenWindow
{
public:
	CMyHiddenWindow()
		: m_pThread( NULL )
	{
	}

	~CMyHiddenWindow()
	{
		if (m_pThread)
		{
			Close();
			m_pThread = NULL;
		}
	}

	bool Create()
	{
		m_pThread = AfxBeginThread(ThreadFunc, this);
		if(!m_pThread)
		{
			// Could not create thread
			return false;
		}
		return true;              
	}

	HDC GetHDC()
	{
		while (!m_pStaticWin)
			Sleep(10);
		return m_pStaticWin->GetHDC();
	}

	HGLRC GetHGLRC()
	{
		while (!m_pStaticWin)
			Sleep(10);
		return m_pStaticWin->GetHGLRC();
	}

private:
	CWinThread *m_pThread;

	static UINT ThreadFunc(LPVOID pvParam)
	{
		m_bDone = false;

		m_pWinmg = new GLWUT::WindowManager();

		m_pStaticWin = new MyWin( m_pWinmg, 0 );

		m_pWinmg->showAllWindows();
		m_pWinmg->hideAllWindows();
		m_pWinmg->mainLoop();

		delete m_pWinmg;
		m_pWinmg = NULL;
		m_bDone = true;

		return 0;
	}

	void Close()
	{
		m_pStaticWin->close();

		while (!m_bDone)
			Sleep(10);
		//TerminateThread(m_pThread->m_hThread, 0);
		//delete m_pWinmg;
	}

};