#include "stdafx.h"
#include "Isovist3D.h"
#include <opencv/highgui.h>


using namespace DSL;
using namespace ISOVIST3D;

CIsovist3d::CIsovist3d()
	: m_pContext( NULL )
	, m_pFBO( NULL )
	, m_pReadBuffer( NULL )
	, m_bGeometryChanged( true )
	, m_pWichtung( NULL )
	, m_iCurrentPolygonID( 1 )
{
}

CIsovist3d::~CIsovist3d()
{
	if (m_pContext)
	{
		delete m_pContext;
		m_pContext = NULL;
	}
	if (m_pFBO)
	{
		delete m_pFBO;
		m_pFBO = NULL;
	}
	if (m_pReadBuffer)
	{
		free( m_pReadBuffer );
		m_pReadBuffer = NULL;
	}
	if (m_pWichtung)
	{
		free( m_pWichtung );
		m_pWichtung = NULL;
	}
	DeleteAllGeometry();
}

void CIsovist3d::SetResolution(int cubeSize)
{
	if (cubeSize < 64)
		cubeSize = 64;
	cubeSize = (int)ceil(cubeSize / 64.0f) * 64;
	m_iCubeSize = cubeSize;

	//OpenCL Optimierung
	const int iMaxTextureSize = 1024;

	bool reInit = false;
	if (m_pContext)
	{
		//delete m_pContext;
		//m_pContext = NULL;
		reInit = true;
	}
	else
	{
		m_pContext = new COpenGLContext();
		m_pContext->InitContext(cubeSize * 6, cubeSize);
	}

	if (m_pFBO)
	{
		delete m_pFBO;
		m_pFBO = NULL;
	}

	CString info = m_pContext->GetInformation();

	m_pContext->BeginGLCommands();

	m_pFBO = new AS::FramebufferObject("rgba=8t depth=32", 1);
	CheckGLError();
	m_pFBO->initialize(cubeSize * 6, cubeSize);
	CheckGLError();
	bool use_fbo = m_pFBO->isFBOsupported() && m_pFBO->checkFramebufferStatus();
	if (use_fbo)
	{
		m_pContext->OnCreateGL();
		//std::cout << "- using Framebuffer object" << std::endl;
	}
	else
	{
		//delete m_pFBO;
		//m_pFBO = NULL;

		exit(2);
	}

	m_pContext->EndGLCommands();

	if (m_pReadBuffer)
	{
		free( m_pReadBuffer );
		m_pReadBuffer = NULL;
	}
	m_pReadBuffer = (unsigned char *)malloc(sizeof(unsigned char) * cubeSize * 6 * cubeSize * 4);

	if (m_pWichtung)
	{
		free( m_pWichtung );
		m_pWichtung = NULL;
	}
	m_pWichtung = GenerateFormfactorMap(cubeSize);
}

unsigned char * CIsovist3d::GetFBOPixel(int x, int y) const
{
	return m_pReadBuffer + (y * m_pContext->GetDX() + x) * 4;
}

size_t CIsovist3d::GetFBOPixelId(int x, int y)
{
	unsigned char * pPixel = GetFBOPixel(x, y);
	return (size_t)pPixel[0] + (size_t)pPixel[1] * 256 + (size_t)pPixel[2] * 256 * 256;
}

Vec3d CIsovist3d::GetFBOPixelRay(int x, int y)
{
	Vec3d result;
	int y0 = y;
	int x0 = x % m_iCubeSize;
	int side = x / m_iCubeSize;

	result.x = (x0 - (double)m_iCubeSize/2 + 0.5) / (double)(m_iCubeSize);
	result.y = (y0 - (double)m_iCubeSize/2 + 0.5) / (double)(m_iCubeSize);
	result.z = 0.5;

	result.Normalize();

	// side Drehung
	switch (side)
	{
	case 0: // front
		break;
	case 1: // right
		Geom3d.Rot_Y_3d(result, sin(3*DSL_PI/2), cos(3*DSL_PI/2));
		break;
	case 2: // back
		Geom3d.Rot_Y_3d(result, sin(DSL_PI), cos(DSL_PI));
		break;
	case 3: // left
		Geom3d.Rot_Y_3d(result, sin(DSL_PI/2), cos(DSL_PI/2));
		break;
	case 4: // bottom
		Geom3d.Rot_X_3d(result, sin(3*DSL_PI/2), cos(3*DSL_PI/2));
		break;
	case 5: // top
		Geom3d.Rot_X_3d(result, sin(DSL_PI/2), cos(DSL_PI/2));
		break;
	}

	return Vec3d(result.x, result.z, result.y);
}


void CIsovist3d::RenderIsovist3d(int iOptiPosX, int iOptiPosY, Vec3d position)
{
	for(int side = 0; side < 6; ++side)
	{
		glViewport(iOptiPosX * (m_iCubeSize * 6) + side * m_iCubeSize, iOptiPosY * m_iCubeSize, m_iCubeSize, m_iCubeSize);

		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		gluPerspective(90.0, 1.0, 0.01, 100.0); // 90° Sichtfeld, 1.0 Aspektratio, 0.01 m z_near, 100.0 z-far
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();

		switch (side)
		{
		case 0: // front
			break;
		case 1: // right
			glRotated(90.0, 0, -1, 0);
			break;
		case 2: // back
			glRotated(180.0, 0, -1, 0);
			break;
		case 3: // left
			glRotated(270.0, 0, -1, 0);
			break;
		case 4: // bottom
			glRotated(90.0, -1, 0, 0);
			break;
		case 5: // top
			glRotated(-90.0, -1, 0, 0);
			break;
		}

		//Kamera Position berücksichtigen
		glTranslated(-position.x, -position.z, position.y);
		//Architektur Geometrie darüber rendern
		if (m_bGeometryChanged)
		{
			BuildGeometryDisplayList();
			m_bGeometryChanged = false;
		}
		ShowGeometryDisplayList();
	}
}

void CIsovist3d::CalculateIsovist3d(Vec3d position)
{
	if (!m_pContext)
		return;

	m_pContext->BeginGLCommands();

	// Rendern
	CheckGLError();

	m_pFBO->beginCapture();

	glViewport(0, 0, m_iCubeSize * 6, m_iCubeSize);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	CheckGLError();

	RenderIsovist3d(0, 0, Vec3d(position.x, position.y, position.z));
	m_pFBO->endCapture();

	glBindTexture(GL_TEXTURE_RECTANGLE_ARB , m_pFBO->getColorAttachmentID(0) );
	CheckGLError();
	glGetTexImage(GL_TEXTURE_RECTANGLE_ARB, 0, GL_RGBA, GL_UNSIGNED_BYTE, m_pReadBuffer);
	CheckGLError();

#if 0
	//opencv Test
	int cv_res_x = m_pFBO->getWidth();
	int cv_res_y = m_pFBO->getHeight();
	IplImage* tmpimg = cvCreateImage( cvSize(cv_res_x, cv_res_y), IPL_DEPTH_8U, 3);

	int x,y;
	for(y=0; y<cv_res_y; y++)
		for(x=0; x<cv_res_x; x++)
		{
			unsigned char * pRGB = GetFBOPixel(x, y);

			unsigned char & r = *((unsigned char*)tmpimg->imageData + ((cv_res_y-y-1) * cv_res_x + x) * 3 + 0);
			unsigned char & g = *((unsigned char*)tmpimg->imageData + ((cv_res_y-y-1) * cv_res_x + x) * 3 + 1);
			unsigned char & b = *((unsigned char*)tmpimg->imageData + ((cv_res_y-y-1) * cv_res_x + x) * 3 + 2);

			unsigned char fr = *pRGB++;
			unsigned char fg = *pRGB++;
			unsigned char fb = *pRGB;

			b = (fr < 0.0f) ? 0 : ((fr > 255.0f) ? 255 : (unsigned char) fr);
			g = (fg < 0.0f) ? 0 : ((fg > 255.0f) ? 255 : (unsigned char) fg);
			r = (fb < 0.0f) ? 0 : ((fb > 255.0f) ? 255 : (unsigned char) fb);
		}
	char filename[256];
	sprintf_s(filename, "isovist3d_side_all.bmp");
	cvSaveImage(filename,tmpimg);      //temporäre Darstellung
	cvReleaseImage( &tmpimg );
#endif

	m_pContext->EndGLCommands();
}

void CIsovist3d::DeleteAllGeometry()
{
	GeometryMap::iterator l,e;
	l = m_vGeometryMap.begin();
	e = m_vGeometryMap.end();
	for(; l!=e; l++)
	{
		PolygonRenderInfo * pInfo = (*l).second;
		delete pInfo;
	}
	m_vGeometryMap.clear();

	m_bGeometryChanged = true;
	m_iCurrentPolygonID = 1;
}

size_t CIsovist3d::AddPolygon(CGeom3d::Point_3d_Vector_Vector &points)
{
	CGeom3d::TriangleVector triangles;
	Geom3d.TesselatePVV(points, triangles);
	CGeom3d::Vec3d normale = Geom3d.GetNormale(points);

	size_t id = GetNewPolygonID();
	m_vGeometryMap.insert(GeometryPair(id, new PolygonRenderInfo(triangles, normale)));
	m_bGeometryChanged = true;
	return id;
}

size_t CIsovist3d::AddPolygonTriangles(CGeom3d::Vec3d& normale, CGeom3d::TriangleVector &triangles)
{
	size_t id = GetNewPolygonID();
	m_vGeometryMap.insert(GeometryPair(id, new PolygonRenderInfo(triangles, normale)));
	m_bGeometryChanged = true;
	return id;
}

void CIsovist3d::DeletePolygon(size_t id)
{
	GeometryMap::iterator found = m_vGeometryMap.find(id);
	if (found != m_vGeometryMap.end())
	{
		PolygonRenderInfo * pInfo = (*found).second;
		delete pInfo;

		m_vGeometryMap.erase(found);
	}
	m_bGeometryChanged = true;
}

bool CIsovist3d::MovePolygon(size_t id, CGeom3d::Point_3d_Vector_Vector &newPoints)
{
	GeometryMap::iterator found = m_vGeometryMap.find(id);
	if (found != m_vGeometryMap.end())
	{
		CGeom3d::TriangleVector triangles;
		Geom3d.TesselatePVV(newPoints, triangles);
		CGeom3d::Vec3d normale = Geom3d.GetNormale(newPoints);
		(*found).second->m_vTriangles = triangles;
		(*found).second->m_normale = normale;
		m_bGeometryChanged = true;
		return true;
	}
	else
		return false;
}

size_t CIsovist3d::GetNewPolygonID()
{
#if 0
	size_t id(0);

	GeometryMap::const_iterator l,e;
	l = m_vGeometryMap.begin();
	e = m_vGeometryMap.end();
	for(; l!=e; l++)
		if (id == (*l).first)
			++id;
		else
			break;

	return id;
#else
	return m_iCurrentPolygonID++;
#endif
}

void CIsovist3d::BuildGeometryDisplayList()
{
	m_vDisplayList.SetMode(GL_COMPILE);
	m_vDisplayList.Reset();
	glNewList(m_vDisplayList.GetNewList(), m_vDisplayList.GetMode());

	GeometryMap::iterator l,e;
	l = m_vGeometryMap.begin();
	e = m_vGeometryMap.end();

	glBegin(GL_TRIANGLES);
	m_vDisplayList.IncCounter(2); // begin, end

	for(; l!=e; l++)
	{
		unsigned __int32 polyId = (unsigned __int32)(*l).first; 
		const PolygonRenderInfo & info = *(*l).second;
		glNormal3f((GLfloat)info.m_normale.x, (GLfloat)info.m_normale.z, (GLfloat)-info.m_normale.y);

		float r, g, b, a;
		r=(float)((double)LOBYTE(polyId)/255.0);
		g=(float)((double)LOBYTE((unsigned short)(polyId) >> 8)/255.0);
		b=(float)((double)LOBYTE((unsigned short)((polyId)>>16))/255.0);
		a= 1.0f;

		glColor4f(r,g,b,a);

		CGeom3d::TriangleVector::const_iterator l_tri, e_tri;
		l_tri = info.m_vTriangles.begin();
		e_tri = info.m_vTriangles.end();
		for(; l_tri!=e_tri; l_tri++)
		{
			const CGeom3d::Triangle_3d &tri = *l_tri;
			glVertex3f((GLfloat)tri.m_P1.x, (GLfloat)tri.m_P1.z, (GLfloat)-tri.m_P1.y);
			glVertex3f((GLfloat)tri.m_P2.x, (GLfloat)tri.m_P2.z, (GLfloat)-tri.m_P2.y);
			glVertex3f((GLfloat)tri.m_P3.x, (GLfloat)tri.m_P3.z, (GLfloat)-tri.m_P3.y);
		}

		m_vDisplayList.IncCounter(2 + info.m_vTriangles.size() * 3); // normale + color + 3 Punkte je Dreieck
		if (m_vDisplayList.GetCounter() > MAX_DISPLAYLIST_INSTRUCTIONS)
		{
			glEnd();

			glEndList();
			glNewList(m_vDisplayList.GetNewList(), m_vDisplayList.GetMode());
			m_vDisplayList.ResetCounter();

			glBegin(GL_TRIANGLES);
			m_vDisplayList.IncCounter(2); // begin, end
		}
	}
	glEnd();
	glEndList();
}

void CIsovist3d::ShowGeometryDisplayList()
{
	m_vDisplayList.Show();
}

float * CIsovist3d::GenerateFormfactorMap(int cube_size)
{
	float * pMap;
	int x, y, z, x2, y2;
	float value;
	double sum_check=0;
	Vec3d pixel_ecke[4];
	int cube_size_2 = (int)ceil(cube_size/2.0f);

	pMap=(float *)malloc(sizeof(float)*(long)pow((float)cube_size,2.0f));;
	for (x=0; x<cube_size_2; x++)
		for (y=0; y<cube_size_2; y++)
		{
			pixel_ecke[0].x=((double)x    /(double)cube_size)-0.5;
			pixel_ecke[0].y=((double)y    /(double)cube_size)-0.5;
			pixel_ecke[1].x=((double)(x+1)/(double)cube_size)-0.5;
			pixel_ecke[1].y=((double)y    /(double)cube_size)-0.5;
			pixel_ecke[2].x=((double)(x+1)/(double)cube_size)-0.5;
			pixel_ecke[2].y=((double)(y+1)/(double)cube_size)-0.5;
			pixel_ecke[3].x=((double)x    /(double)cube_size)-0.5;
			pixel_ecke[3].y=((double)(y+1)/(double)cube_size)-0.5;
			pixel_ecke[0].z=0.5;
			pixel_ecke[1].z=0.5;
			pixel_ecke[2].z=0.5;
			pixel_ecke[3].z=0.5;
			for (z=0; z<4; z++)
				pixel_ecke[z].Normalize();
			value=(float)(Geom3d.AreaKugelViereck_Normalized(pixel_ecke[0], 
				pixel_ecke[1], 
				pixel_ecke[2], 
				pixel_ecke[3]) / (4.0 * DSL_PI));

			x2 = cube_size - x - 1;
			y2 = cube_size - y - 1;
			pMap[x *(unsigned __int32)cube_size+y ]=value;
			pMap[x2*(unsigned __int32)cube_size+y ]=value;
			pMap[x2*(unsigned __int32)cube_size+y2]=value;
			pMap[x *(unsigned __int32)cube_size+y2]=value;
			if ((x!=x2) && (y!=y2))
				sum_check+=value*4;
			else
				if ((x==x2) && (y==y2))
					sum_check+=value;
				else
					sum_check+=value*2;
		}

		//Einheitskreis Okay ?
		sum_check*=6;
		if (fabs(sum_check-1.0)>Eps3)
			assert(0);
		sum_check = 0;

		return pMap;
}
