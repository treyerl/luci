#pragma once


#include <CL/opencl.h>

#ifdef WIN64
#pragma comment(lib,"OpenCL_x64.lib")
#else
#pragma comment(lib,"OpenCL.lib")
#endif

//#define _DEBUG_DEEP_OPENCL

namespace DSL
{

	class OpenCL;
	static OpenCL * m_pOpenCL = NULL;

	class OpenCL
	{
	private:
		//OpenCL stuff
		cl_uint m_num_platforms;
		cl_kernel *m_clkern;
		cl_kernel *m_clkern2;
		cl_program *m_clpgm;
		cl_command_queue *m_clcmdq;
		cl_context *m_clctx;
		cl_platform_id *m_platforms;

		int m_itemsize_x;
		int m_itemsize_y;

		OpenCL()
		{
			InitOpenCL();
		}

	public:

		const cl_kernel * GetKernel()		{ return m_clkern; }
		const cl_kernel * GetKernel2()		{ return m_clkern2; }
		const cl_program * GetProgram()		{ return m_clpgm; }
		const cl_command_queue * GetQueue() { return m_clcmdq; }
		const cl_context * GetContext()		{ return m_clctx; }
		const int GetItemSizeX()			{ return m_itemsize_x; }
		const int GetItemSizeY()			{ return m_itemsize_y; }

		static OpenCL * GetOpenCL()
		{
			if (!m_pOpenCL)
				m_pOpenCL = new OpenCL();
			return m_pOpenCL;
		}

		static void CloseOpenCLForever()
		{
			if (m_pOpenCL)
			{
				delete m_pOpenCL;
				m_pOpenCL = NULL;
			}
		}

		~OpenCL()
		{
			QuitOpenCL();
		}

		void InitOpenCL();
		void QuitOpenCL();
		int GetPlatformPickId();
		CString GetInformation(cl_platform_id platform, cl_platform_info info);
		CString GetInformation(cl_device_id device, cl_device_info info);
	};

}