#include "stdafx.h"
#include "Window.h"
#include "WindowManager.h"
using namespace GLWUT;

/** Constructor
 * creates a fullscreen window with OpenGL context etc.
 * @param wm WindowManager where this window will be registered at.
 * @param devnum Number of device on which window will be created
 * @param useStereo if true, try to create a stereo context
 * @throws std::string on error, an exception is thrown
 **/
Window::Window(WindowManager *wm, DWORD devnum, bool useStereo, bool hidden) : wm_(wm), devnum_(devnum), open_(true), hidden_(true) {
  device_ = wm->getDevmode(devnum);
  if (hidden)
  {
	  device_->dmPosition.x = 0;
	  device_->dmPosition.y = 0;
	  device_->dmPelsWidth = 1;
	  device_->dmPelsHeight = 1;
  }

  wm->registerWindow(this);

  char num[32];
  std::string bla = "openGL";
  _itoa_s(rand(), num, 16);
  classname_ = bla.append(num);

  GLuint		PixelFormat;
	WNDCLASS	wc;
	DWORD		dwExStyle;
	DWORD		dwStyle;
	RECT		WindowRect;

  for(int i=0; i<256; ++i) keys_[i] = false;
  //ChangeDisplaySettings(device_,CDS_FULLSCREEN);

  WindowRect.left=(long)device_->dmPosition.x;
	WindowRect.right=(long)device_->dmPosition.x+device_->dmPelsWidth;
	WindowRect.top=(long)device_->dmPosition.y;
	WindowRect.bottom=(long)device_->dmPosition.x+device_->dmPelsHeight;

  instance_ = GetModuleHandle(NULL);
	wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
  wc.lpfnWndProc		= (WNDPROC) Window::msgRouter;					// WndProc Handles Messages
	wc.cbClsExtra		= 0;									// No Extra Window Data
	wc.cbWndExtra		= 0;									// No Extra Window Data
	wc.hInstance		= instance_;							// Set The Instance
	wc.hIcon			= LoadIcon(NULL, IDI_WINLOGO);			// Load The Default Icon
	wc.hCursor			= LoadCursor(NULL, IDC_ARROW);			// Load The Arrow Pointer
	wc.hbrBackground	= NULL;									// No Background Required For GL
	wc.lpszMenuName		= NULL;									// We Don't Want A Menu
  wc.lpszClassName	= reinterpret_cast<LPCSTR>(classname_.c_str());								// Set The Class Name

  if (!RegisterClass(&wc)){
		throw "Failed registerclass";
	}

  dwExStyle=WS_EX_APPWINDOW;
	dwStyle=WS_POPUP;

  AdjustWindowRectEx(&WindowRect, dwStyle, FALSE, dwExStyle);
  if (!(wnd_=CreateWindowEx(dwExStyle,
                reinterpret_cast<LPCSTR>(classname_.c_str()),
                reinterpret_cast<LPCSTR>("TITLE"),
								dwStyle | WS_CLIPSIBLINGS | WS_CLIPCHILDREN,
								//xy 0, 0,
                device_->dmPosition.x, device_->dmPosition.y,
								WindowRect.right-WindowRect.left,
								WindowRect.bottom-WindowRect.top,
								NULL,NULL,
								instance_,
								this )))	{
		throw "Window Creation Error.";
	}

  static	PIXELFORMATDESCRIPTOR pfd=				// pfd Tells Windows How We Want Things To Be
	{
		sizeof(PIXELFORMATDESCRIPTOR),				// Size Of This Pixel Format Descriptor
		1,											// Version Number
		PFD_DRAW_TO_WINDOW |						// Format Must Support Window
		PFD_SUPPORT_OPENGL |						// Format Must Support OpenGL
		//PFD_DOUBLEBUFFER   |						// Must Support Double Buffering
		//(useStereo?PFD_STEREO:0),
		PFD_TYPE_RGBA,								// Request An RGBA Format
		24,										// Select Our Color Depth
		0, 0, 0, 0, 0, 0,							// Color Bits Ignored
		0,											// No Alpha Buffer
		0,											// Shift Bit Ignored
		0,											// No Accumulation Buffer
		0, 0, 0, 0,									// Accumulation Bits Ignored
		32,											// 32Bit Z-Buffer (Depth Buffer)  
		0,											// No Stencil Buffer
		0,											// No Auxiliary Buffer
		PFD_MAIN_PLANE,								// Main Drawing Layer
		0,											// Reserved
		0, 0, 0										// Layer Masks Ignored
	};

  if (!(dc_=GetDC(wnd_)))	{
		throw "Can't Create A GL Device Context.";
	}

  if (!(PixelFormat=ChoosePixelFormat(dc_,&pfd))) {
		throw "Can't Find A Suitable PixelFormat.";
	}

	if(!SetPixelFormat(dc_,PixelFormat,&pfd)){
		throw "Can't Set The PixelFormat.";
	}

	if (!(glrc_=wglCreateContext(dc_))) {
		throw "Can't Create A GL Rendering Context.";
	}

	if(!wglMakeCurrent(dc_,glrc_)) {
		throw "Can't Activate The GL Rendering Context.";
	}
  width_=device_->dmPelsWidth;
  height_=device_->dmPelsHeight;
  left_ = device_->dmPosition.x;
  top_ = device_->dmPosition.y;
}

/** Destructor
 * Clean up everything.
 **/
Window::~Window() {
	if(glrc_) {
		wglMakeCurrent(NULL,NULL);
    wglDeleteContext(glrc_);
	}

	if (dc_){
    ReleaseDC(wnd_,dc_);
	}

	if (wnd_) {
    DestroyWindow(wnd_);
	}

	UnregisterClass(reinterpret_cast<LPCSTR>(classname_.c_str()),instance_);
}

/** msgRouter
 * Used as WINPROC callback for every window. Passes every message
 * to the apropriate Window-Class.
 **/
LRESULT CALLBACK Window::msgRouter(HWND hwnd, UINT message,
                                   WPARAM wparam, LPARAM lparam) {
  Window *wnd = 0;

  if(message == WM_NCCREATE) {
    // retrieve Window instance from window creation data and associate
    wnd = reinterpret_cast<Window *>( ((LPCREATESTRUCT)lparam)->lpCreateParams);
    ::SetWindowLongPtr(hwnd, GWLP_USERDATA, reinterpret_cast<long>(wnd));
	  // save window handle
    wnd->setHWND(hwnd);
  } else {
    // retrieve associated Window instance
    wnd = reinterpret_cast<Window *>(::GetWindowLongPtr(hwnd, GWLP_USERDATA));
  }

  // call the windows message handler
  wnd->dispatch(message, wparam, lparam);

  return DefWindowProc(hwnd, message, wparam, lparam);
}
/** setHWND
 **/
void Window::setHWND(HWND wnd) {
  wnd_ = wnd;
}

/** dispatch
 * Called by msgRouter to dispatch window specific messages
 * to the apropriate methos
 **/
void Window::dispatch(UINT message, WPARAM wparam, LPARAM lparam) {
  switch(message) {  
    case WM_KEYDOWN:
			keys_[wparam] = true;
      break;
		case WM_KEYUP:
			keys_[wparam] = false;
			break;
		case WM_SIZE:
			resize(LOWORD(lparam),HIWORD(lparam));  // LoWord=Width, HiWord=Height
			break;
		case WM_MOUSEMOVE:
			mouse_posx_ = LOWORD(lparam); 
			mouse_posy_ = HIWORD(lparam); 
			break;
    case WM_LBUTTONDOWN:
      mouseDown(0, LOWORD(lparam),HIWORD(lparam));
      break;
		}
}

/** show
 * displays the window on the screen and clears it with black
 **/
void Window::show() {
  hidden_ = false;
  ShowWindow(wnd_,SW_SHOW);						// Show The Window
	SetForegroundWindow(wnd_);						// Slightly Higher Priority
	SetFocus(wnd_);	

  glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  SwapBuffers(dc_);
}

void Window::hide() {
  hidden_ = true;
  ShowWindow(wnd_,SW_HIDE);						// hide The Window
}

/** resize
 * called on window resize message. should never happen.
 * NOT called after init. should NOT be overloaded. work
 * in progress...
 * @param width new width of window
 * @param height new height of window
 **/
void Window::resize(int width, int height) {
  width_=width;
  height_=height;
}

/** keydown
 * Called on Key-Down events. Overload to add your own
 * functionality.
 * @param key ASCII-Code of Key that is pressed
 **/
void Window::keydown(unsigned char key) {
}

/** handleEvents
 * used to handle events. Do NOT overload
 **/
void Window::handleEvents() {
  for(int i=0; i<256; i++)
    if(keys_[i]) {
      keydown(i);
      keys_[i] = false;
    }

	Sleep(20);
}

bool Window::isOpen() { return open_; }

/** display
 * called if window need to be (re-)drawn
 **/
void Window::display() {}

/** idle
 * called if idle
 **/
void Window::idle() {}

void Window::setActiveCtx() {
	wglMakeCurrent(dc_,glrc_);
}

/** handleDisplay
 * do NOT overload.
 **/
void Window::handleDisplay() {
  if(hidden_) return;

  wglMakeCurrent(dc_,glrc_);
  display();
  SwapBuffers(dc_);
  wglMakeCurrent(NULL, NULL);
}

/** init
 * Set up your OpenGL stuff in here. Note: this is NOT
 * automatically called. Maybe it will some day
 **/
void Window::init() {
}

/** close
 * closes the window. do NOT overload
 **/
void Window::close() {
  open_ = false;
}

/** mouseDown
 * called, if mouse button is pressen in window
 * @param button Which butten is pressed (0 is left mouse button)
 * @param x x-position of click with 0 being left-most of window
 * @param y y-position of click with 0 being top-most of window
 **/
void Window::mouseDown(int button, int x, int y) {
}

int Window::getWidth() {return width_;}
int Window::getHeight() {return height_;}
int Window::getTop() {return top_;}
int Window::getLeft() {return left_;}

//#include <wingdi.h>
void Window::glShareWith(Window* w) {
	wglShareLists(w->glrc_, glrc_);
}