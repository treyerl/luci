#ifndef GLWUT_WINDOW_H__
#define GLWUT_WINDOW_H__

#include <iostream>
#include <map>
#include <windows.h>
#include <gl\gl.h>
#include <gl\glu.h>
//#include <gl\glaux.h> // Nicht mehr ab VC2008


namespace GLWUT {
class WindowManager;

class Window {
  public:
    Window(WindowManager *wm, DWORD devnum, bool useStereo=false, bool hidden=true);
    ~Window();

    /* you may use these functions,
    but should NOT overload them */
    void show();
    void hide();
    bool isOpen();
    int getWidth();
    int getHeight();
    int getTop();
    int getLeft();
    void close();
	HDC GetHDC() { return dc_; }
	HGLRC GetHGLRC() { return glrc_; }

	inline int mouseX() {return mouse_posx_;}
	inline int mouseY() {return mouse_posy_;}

	void glShareWith(Window* w);

    /* you may overload these functions: */
    virtual void init();
    virtual void display();
    virtual void idle();
    virtual void dispatch(UINT, WPARAM, LPARAM);
    virtual void resize(int width, int height);
    virtual void keydown(unsigned char key);
    virtual void mouseDown(int button, int x, int y);

	void setActiveCtx();

    /* you better NOT TOUCH these functions: */
    static LRESULT CALLBACK msgRouter(HWND hwnd, UINT message, WPARAM wparam, LPARAM lparam);
    void handleEvents();
    void handleDisplay();
  private:
    WindowManager *wm_;
    DWORD         devnum_;

    void setHWND(HWND wnd);
    
    bool open_;
    bool	keys_[256];
    bool hidden_;

    HWND  wnd_;
    PDEVMODE device_;
    HDC			dc_;
    HGLRC		glrc_;
    HINSTANCE	instance_;
    std::string classname_; 

    int width_, height_, top_, left_;
	int mouse_posx_, mouse_posy_;
};


} //end namespace
#endif /* GLWUT_WINDOW_H__ */