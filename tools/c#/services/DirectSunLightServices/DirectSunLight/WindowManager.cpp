#include "stdafx.h"
#include "WindowManager.h"
using namespace std;
namespace GLWUT {

/** Constructor
 * Fetches all display devices and sets up everything
 **/
WindowManager::WindowManager() {
  /* enumerate display devices */
  DWORD devNum = 0;
  DISPLAY_DEVICE dd;
  memset(&dd,0,sizeof(DISPLAY_DEVICE));
  dd.cb = sizeof(DISPLAY_DEVICE);

  while( EnumDisplayDevices(NULL,devNum,&dd,0) ) {
    /* only devices attached to desktop are of use to us */
    if(dd.StateFlags&DISPLAY_DEVICE_ATTACHED_TO_DESKTOP) {
        DEVMODE *dm = new DEVMODE;
        memset(dm, 0, sizeof(DEVMODE));
        dm->dmSize = sizeof(DEVMODE);
        if(EnumDisplaySettings(dd.DeviceName,ENUM_CURRENT_SETTINGS,dm)){
          devmodes_[devNum] = dm;
        }    
    }
    devNum++;
  }
}

/** Destructor
 * cleans up. DELETES all registered windows.
 **/
WindowManager::~WindowManager() {
  for(map<DWORD, DEVMODE*>::const_iterator I=devmodes_.begin(); I!=devmodes_.end();
    ++I) {
      delete (*I).second;
  }
  // delete all registered windows
  for(set<Window*>::const_iterator I=wins_.begin();
        I!=wins_.end(); ++I) {
      delete (*I);
  }
}

/** mainLoop
 * Loops until one(1) window is closed and handles
 * any events and stuff.
 **/
void WindowManager::mainLoop() {
  MSG		msg;
  RUNNING = true;
  QUITTED = false;

  while(RUNNING) {
		if(PeekMessage(&msg,NULL,0,0,PM_REMOVE)) {
			if(msg.message==WM_QUIT) {
				break;
			} else {
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
    } else {
      for(set<Window*>::const_iterator I=wins_.begin();
        I!=wins_.end(); ++I) {
        Window *win = *I;
        if(win->isOpen()) {
          win->handleEvents();
          win->idle();
          win->handleDisplay();
        } else {
          RUNNING = false; //one window close -> quit app
        }
      }
    }
	}

	QUITTED = true;
}

/** yield
 * Handles all GLWUT::Window-Related stuff,
 * this is to be called from the Win32 Main-Loop.
 * @returns the number of windows still open
 **/
int WindowManager::yield() {
	int wopen = 0;

	for(set<Window*>::const_iterator I=wins_.begin(); I!=wins_.end(); ++I) {
        Window *win = *I;
        if(win->isOpen()) {
          win->handleEvents();
          win->idle();
          win->handleDisplay();
		  wopen++;
        }
	}

	return wopen;
}


/** registerWindow
 * adds a Window to the WindowManager whichs events
 * will be handled
 **/
void WindowManager::registerWindow(Window* win) {
  wins_.insert(win);
}

/** getDevmode
 * Returns the PDEVMODE for a given index
 **/
DEVMODE* WindowManager::getDevmode(DWORD idx) {
  return devmodes_[idx];
}

/** showAllWindows
 * show() is called on all registered windows
 **/
void WindowManager::showAllWindows() {
  for(set<Window*>::const_iterator I=wins_.begin();
        I!=wins_.end(); ++I) {
    (*I)->show();
  }
}

void WindowManager::hideAllWindows() {
	for(set<Window*>::const_iterator I=wins_.begin();
		I!=wins_.end(); ++I) {
			(*I)->hide();
	}
}


int WindowManager::numberOfDevices() const {
  return devmodes_.size();
}

} //end namespace