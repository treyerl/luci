#ifndef GLWUT_WINDOWMANAGER_H__
#define GLWUT_WINDOWMANAGER_H__

#include <iostream>
#include <map>
#include <set>
#include <windows.h>
#include "Window.h"

namespace GLWUT {

class WindowManager {
  public:
    WindowManager();
    ~WindowManager();
    
    void registerWindow(Window* w);

    void showAllWindows();
	void hideAllWindows();
    void mainLoop();
    int yield();
	bool finshed() { return QUITTED; }

    int numberOfDevices() const;

    //only used by GLWUT::Window.
    DEVMODE* getDevmode(DWORD idx);
  protected:
  private:
    std::map<DWORD, DEVMODE*> devmodes_;
    std::set<Window*> wins_;
	bool RUNNING;
	bool QUITTED;
};

}
#endif /* GLWUT_WINDOWMANAGER_H__ */