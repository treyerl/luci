﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GeoAPI.Geometries;
using NetTopologySuite.Geometries;

namespace Pg3DNtsInterop
{
    public class Pg3DNtsConverter
    {
        public CGeom3d.Vec_3d[][] PolygonToPureGeom(Polygon p)
        {
            return (new[]
            {
                p.ExteriorRing // first array is exterior
                .Coordinates.Take(p.ExteriorRing.Coordinates.Length - 1) // NTS duplicates the first point
                .Select(c => new CGeom3d.Vec_3d(c.X, c.Y, c.Z)).ToArray()
            })
                .Concat(
                    p.InteriorRings // holes
                    .Select(r =>
                        r.Coordinates.Take(r.Coordinates.Length - 1) // NTS duplicates the first point
                        .Select(c => new CGeom3d.Vec_3d(c.X, c.Y, c.Z)).ToArray()
                        )
                ).ToArray();
        }

        public Polygon PureGeomToPolygon(CGeom3d.Vec_3d[][] vv)
        {
            if (vv == null || vv.Length == 0 || vv[0].Length == 0)
                return null;
            return new Polygon(
                new LinearRing(vv[0].Select(v => new Coordinate(v.x, v.y, v.z))
                    .Concat(new[] { new Coordinate(vv[0][0].x, vv[0][0].y, vv[0][0].z) }) // last point must be the same as first
                    .ToArray()),
                vv.Skip(1).Where(vr => vr.Length > 1)
                    .Select(vr =>
                        new LinearRing(vr.Select(v => new Coordinate(v.x, v.y, v.z))
                            .Concat(new[] { new Coordinate(vr[0].x, vr[0].y, vr[0].z) }) // last point must be the same as first
                            .ToArray()) as ILinearRing
                    ).ToArray()
                );
        }
    }
}
