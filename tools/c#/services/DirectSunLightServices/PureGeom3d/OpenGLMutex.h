#pragma once

using namespace System::Threading;

namespace Visualisation
{
	public ref class COpenGLMutex
	{
	private:
		static Mutex^ m_pMutex = gcnew Mutex();

	public:
		static void EnterMutex()
		{
			//Wait until it is OK to enter.
			m_pMutex->WaitOne();
		}

		static void LeaveMutex()
		{
			// Release the Mutex.
			m_pMutex->ReleaseMutex();
		}
	};
}
