﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DirectSunLightNet;
using Lucy;
using NetTopologySuite.Geometries;
using Newtonsoft.Json.Linq;
using OpenTK;

namespace DirectSL
{
    public class SolarAnalysis : Lucy.SimpleLucyService, ILucyService
    {
        public enum CalculationMode
        {
            Klux,
            KcdPm2
        }

        private readonly CDirectSunLightNet _directSunlight;
        private CalculationMode _calcMode;
        private float _northOrientationAngle = 0;
        private string _pathToHdri;
        private int _resolution = 64;

        public SolarAnalysis()
        {
            _directSunlight = new CDirectSunLightNet();
            CalcMode = CalculationMode.Klux;
            Resolution = 64;
            NorthOrientationAngle = 0f;
        }

        public SolarAnalysis(string pathToHdri)
        {
            _directSunlight = new CDirectSunLightNet();
            CalcMode = CalculationMode.Klux;
            Resolution = 64;
            PathToHdri = pathToHdri;
            NorthOrientationAngle = 0f;
        }

        # region Properties

        /// <summary>
        ///     Regime of calculation for solar analysis
        /// </summary>
        public CalculationMode CalcMode
        {
            get { return _calcMode; }
            set
            {
                _calcMode = value;
                //switch (value)
                //{
                //    case CalculationMode.Klux:
                //        _directSunlight.SetCalculationMode(DirectSunLightNet.CalculationMode.illuminance_klux);
                //        break;
                //    case CalculationMode.KcdPm2:
                //        _directSunlight.SetCalculationMode(
                //            DirectSunLightNet.CalculationMode.luminance_kcd_per_square_meter);
                //        break;
                //    default:
                //        _directSunlight.SetCalculationMode(DirectSunLightNet.CalculationMode.illuminance_klux);
                //        break;
                //}
            }
        }


        /// <summary>
        ///     Size of a cube element when computing
        /// </summary>
        public int Resolution
        {
            get { return _resolution; }
            set { _resolution = value; }
        }

        /// <summary>
        ///     Path to HDRI texture with solar radiation information
        /// </summary>
        public string PathToHdri
        {
            get { return _pathToHdri; }
            set
            {
                if (File.Exists(Path.GetFullPath(value)))
                    _pathToHdri = Path.GetFullPath(value);
                else if (File.Exists(Path.Combine(Environment.CurrentDirectory, value)))
                    _pathToHdri = Path.Combine(Environment.CurrentDirectory, value);
                else
                    throw new FileNotFoundException("HDRI file specified cannot be found " + value);
                //_directSunlight.SetHDRImage(_pathToHdri);
            }
        }

        /// <summary>
        ///     Orientation of the map to the north, in radians [0,2*pi].
        /// </summary>
        public float NorthOrientationAngle
        {
            get { return _northOrientationAngle; }
            set { _northOrientationAngle = value; }
        }

        # endregion

        public void Dispose()
        {
            _directSunlight.Dispose();
        }

        public double[] Calculate(CGeom3d.Vec_3d[] positions, CGeom3d.Vec_3d[] normales)
        {
            var results = new double[positions.Length];

            try
            {
                _directSunlight.SetResolution(Resolution);
                _directSunlight.SetHDRImage(PathToHdri);
                _directSunlight.PrepareHDRCubeMap(NorthOrientationAngle);
                _directSunlight.StartOpenCLOnceAndForever();
                switch (CalcMode)
                {
                    case CalculationMode.Klux:
                        _directSunlight.SetCalculationMode(DirectSunLightNet.CalculationMode.illuminance_klux);
                        break;
                    case CalculationMode.KcdPm2:
                        _directSunlight.SetCalculationMode(
                            DirectSunLightNet.CalculationMode.luminance_kcd_per_square_meter);
                        break;
                    default:
                        _directSunlight.SetCalculationMode(DirectSunLightNet.CalculationMode.illuminance_klux);
                        break;
                }

                _directSunlight.SetNormale(new CGeom3d.Vec_3d(0, 0, 1));

                var erg = new float[results.Length * 3];
                _directSunlight.CalculateDirectSunLight(positions, normales, ref erg);
                float r, g, b;
                for (var i = 0; i < results.Length; i++)
                {
                    r = erg[i * 3];
                    g = erg[i * 3 + 1];
                    b = erg[i * 3 + 2];
                    results[i] = Math.Sqrt(r * r + g * g + b * b);
                }
            }
            catch (Exception ex)
            {
                results = null;
            }

            return results;
        }

        public double[] Calculate(IEnumerable<Vector3d> positions, IEnumerable<Vector3d> normales)
        {
            return Calculate(
                positions.Select(p => new CGeom3d.Vec_3d(p.X, p.Y, p.Z)).ToArray(),
                normales.Select(p => new CGeom3d.Vec_3d(p.X, p.Y, p.Z)).ToArray()
                );
        }

        public void ClearGeometry()
        {
            _directSunlight.DeleteAllGeometry();
        }

        public void RemovePolygon(uint id)
        {
            _directSunlight.DeletePolygon(id);
        }


        public uint AddPolygon(CGeom3d.Vec_3d[][] pvv, float transparency)
        {
            return _directSunlight.AddPolygon(pvv, transparency);
        }


        public uint AddPolygon(Vector3d[][] pvv, float transparency)
        {
            return _directSunlight.AddPolygon(
                pvv.Select(pv => 
                    pv.Select(p => new CGeom3d.Vec_3d(p.X, p.Y, p.Z)
                    ).ToArray()
                ).ToArray(), transparency);
        }

        private string _lastHdrIchecksum = null;

        public override Tuple<Dictionary<string, object>, byte[][]> Execute(Dictionary<string, object> inputData,
            params byte[][] attachments)
        {
            // get parameters
            CalculationMode calculationMode;
            if (Enum.TryParse(inputData["calculationMode"].ToString(), true, out calculationMode))
                CalcMode = calculationMode;
            int resolution;
            if (int.TryParse(inputData["resolution"].ToString(), out resolution))
                Resolution = resolution;

            float northOrientationAngle;
            if (float.TryParse(inputData["northOrientationAngle"].ToString(), out northOrientationAngle))
                NorthOrientationAngle = northOrientationAngle;

            // get information about HDR image of sky
            var hdriSi = (StreamInfo) inputData["HdriImage"];
            if ((_lastHdrIchecksum == null || !_lastHdrIchecksum.Equals(hdriSi.Checksum)) && File.Exists("temp.hdr"))
                File.Delete("temp.hdr");
            if (!File.Exists("temp.hdr"))
            {
                File.WriteAllBytes("temp.hdr", attachments[hdriSi.Order - 1]);
                _lastHdrIchecksum = hdriSi.Checksum;
                PathToHdri = "temp.hdr";
            }
            
            // get actual input points and normales
            var pSi = (StreamInfo)inputData["Points"];
            var nSi = (StreamInfo)inputData["Normales"];
            var points = new double[attachments[pSi.Order - 1].Length / sizeof(double)];
            var normales = new double[attachments[nSi.Order - 1].Length / sizeof(double)];
            Buffer.BlockCopy(attachments[pSi.Order - 1], 0, points, 0, attachments[pSi.Order - 1].Length);
            Buffer.BlockCopy(attachments[nSi.Order - 1], 0, normales, 0, attachments[nSi.Order - 1].Length);
            var pointsV = new CGeom3d.Vec_3d[points.Length / 3];
            var normalesV = new CGeom3d.Vec_3d[normales.Length / 3];
            for (var i = 0; i < pointsV.Length; i++)
            {
                pointsV[i] = new CGeom3d.Vec_3d(points[3 * i], points[3 * i + 1], points[3 * i + 2]);
                normalesV[i] = new CGeom3d.Vec_3d(normales[3 * i], normales[3 * i + 1], normales[3 * i + 2]);
            }

            // calculation and wrapping results
            var results = Calculate(pointsV, normalesV);
            var attachedBytes = new byte[results.Length * sizeof(double)];
            Buffer.BlockCopy(results, 0, attachedBytes, 0, attachedBytes.Length);

            Console.WriteLine("Executed SolarAnalysis; number of points = " + results.Length);
            return new Tuple<Dictionary<string, object>, byte[][]>(new Dictionary<string, object>()
            {
                //{"Radiation", new StreamInfo(attachedBytes, 1, "doubles")},
                {"RadiationList", results}
            }, null); //new[] { attachedBytes });
        }

        public override Dictionary<string, TransferValueType> Inputs
        {
            get { return new Dictionary<string, TransferValueType>()
            {
                {"HdriImage", TransferValueType.STREAMINFO}, // Sky image file
                {"Points", TransferValueType.STREAMINFO}, // array of doubles: XYZ coordinates
                {"Normales", TransferValueType.STREAMINFO}, // array of doubles: XYZ coordinates
                {"calculationMode", TransferValueType.STRING },
                {"resolution", TransferValueType.NUMBER},
                {"northOrientationAngle", TransferValueType.NUMBER }
            }; }
        }

        public override Dictionary<string, TransferValueType> Outputs
        {
            get
            {
                return new Dictionary<string, TransferValueType>()
            {
                //{"Radiation", TransferValueType.STREAMINFO}, // array of double values
                {"RadiationList", TransferValueType.LIST}
            };
            }
        }

        JObject ILucyService.ScenarioObject
        {
            get
            {
                return null;
            }
            set
            {
                // Get the Geometry object that contain all GeoJSON
                var features = (Lucy.Geometry) value.SelectToken("result.geometry.GeoJSON").Value<JObject>();
                // Select all polygons
                var polygons = features.Data.Features.SelectMany(f => ((MultiPolygon) f.Geometry).Geometries).ToArray();
                // reset scenario geometry
                ClearGeometry();
                var converter = new Pg3DNtsInterop.Pg3DNtsConverter();
                foreach (var polygon in polygons)
                    AddPolygon(converter.PolygonToPureGeom((Polygon) polygon), 1f);
                //{
                //    var poly = converter.PolygonToPureGeom((Polygon) polygon);
                //    _polygons.Add(AddPolygon(poly, 1f), poly);
                //}
                Console.WriteLine("Updated Scenario.");
            }
        }
        //private Dictionary<uint, CGeom3d.Vec_3d[][]> _polygons = new Dictionary<uint, CGeom3d.Vec_3d[][]>();
    }
}