﻿using DirectSunLightNet;
using LuciCommunication;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolarAnalysisService
{
    public class Program
    {
        static void Main(string[] args)
        {
            Communication2Luci cl = new Communication2Luci();
            SolarAnalysisCalculator sac = new SolarAnalysisCalculator();

            string result = "Connection to Luci was ";
            string host = "localhost";
            //    host = "129.132.6.4";
            //host = "129.132.6.60";

            if (args.Count() > 0)
                host = args[0];

            if (!cl.connect(host, 7654))
                result += "not ";

            result += "successful! \n";

            Console.Out.WriteLine(result);

            Console.Out.WriteLine("-> authenticate\n");
            LuciCommunication.Communication2Luci.LuciAnswer? a = cl.authenticate("lukas", "1234");

            if (a != null)
            {
                LuciCommunication.Communication2Luci.LuciAnswer answer = (LuciCommunication.Communication2Luci.LuciAnswer)a;

                Console.Out.WriteLine(answer.Message);
                if (answer.State != Communication2Luci.LuciState.Error)
                {

                    Console.Out.WriteLine("-> registering service\n");

                    // Register Service
                    var inputDef = new List<KeyValuePair<string, string>>
                    {
                        new KeyValuePair<string, string>("HdriImage", LuciType.STREAMINFO), // Sky image file
                        new KeyValuePair<string, string>("Points", LuciType.STREAMINFO), // array of doubles: XYZ coordinates
                        new KeyValuePair<string, string>("Normales", LuciType.STREAMINFO), // array of doubles: XYZ coordinates
                        new KeyValuePair<string, string>("calculationMode", LuciType.STRING),
                        new KeyValuePair<string, string>("resolution", LuciType.NUMBER),
                        new KeyValuePair<string, string>("northOrientationAngle", LuciType.NUMBER)
                    };

                    var outputDef = new List<KeyValuePair<string, string>>
                    {
//                        new KeyValuePair<string, string>("RadiationList", LuciType.STREAMINFO)
                        new KeyValuePair<string, string>("RadiationList", LuciType.LIST)
                    };

                    string machinename = System.Net.Dns.GetHostName();

                    answer = cl.registerService("SolarAnalysis", "1.0", "Service for Solar Analysis", host, inputDef, outputDef);
                    Console.Out.WriteLine(answer.Message);

                    while (true)
                    {
                        LuciCommunication.Communication2Luci.LuciAnswer resultRecv = cl.receiveInputMessage();

                        try
                        {
                            sac.ReadInputs(resultRecv);
                            LuciCommunication.Communication2Luci.LuciAnswer scenario = cl.getScenario((int)sac.ScId);
                            sac.ReadScenario(scenario);

                            // calculation and wrapping results
                            var results = sac.Calculate();
                            var attachedBytes = new byte[results.Length * sizeof(double)];
                            Buffer.BlockCopy(results, 0, attachedBytes, 0, attachedBytes.Length);

                            Console.WriteLine("Executed SolarAnalysis; number of points = " + results.Length);
                            LuciCommunication.Communication2Luci.LuciStreamData radiationList = new Communication2Luci.LuciStreamData() { Data = attachedBytes, Format = "doublesXYZ", Name = "RadiationList" };

                            List<KeyValuePair<string, object>> luciResults = new List<KeyValuePair<string, object>>();
                            luciResults.Add(new KeyValuePair<string, object>("RadiationList", results));
        //                    luciResults.Add(new KeyValuePair<string, object>("NumPoints", results.Length));


                            // send output

//                            LuciCommunication.Communication2Luci.LuciAnswer conf = cl.sendResults(sac.ObjId, "0.1", host, sac.Timestamp, luciResults, 1, radiationList);
                            LuciCommunication.Communication2Luci.LuciAnswer conf = cl.sendResults(sac.ObjId, "0.1", host, sac.Timestamp, luciResults, 1);
                            Console.Out.WriteLine(conf.Message);
                        }
                        catch (Exception ex)
                        {
                            Console.Out.WriteLine(ex.Message + "\r\n" + "Stacktrace: " + ex.StackTrace + ex.InnerException == null ? "" :
                                "\r\n" + ex.InnerException.Message + "\r\n" + "Stacktrace: " + ex.InnerException.StackTrace);
                        }
                    }
                }
            }
        }
    }
}