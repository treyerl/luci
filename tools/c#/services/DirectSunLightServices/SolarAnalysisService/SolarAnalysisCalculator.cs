﻿using DirectSunLightNet;
using GeoAPI.Geometries;
using NetTopologySuite.Features;
using NetTopologySuite.Geometries;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OpenTK;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SolarAnalysisService
{
    public class SolarAnalysisCalculator
    {
        public enum CalculationMode
        {
            Klux,
            KcdPm2
        }

        private string _lastHdrIchecksum = null;
        private readonly CDirectSunLightNet _directSunlight2;// = new CDirectSunLightNet();
        private CDirectSunLightNet _directSunlight = null;// new CDirectSunLightNet();
        private CalculationMode _calcMode;
        private float _northOrientationAngle = 0;
        private string _pathToHdri;
        private int _resolution = 64;

        public long Timestamp { get; set; }
        public long ObjId { get; set; }
        public long ScId { get; set; }

        private CGeom3d.Vec_3d[] _pointsV;
        private CGeom3d.Vec_3d[] _normalesV;

        private List<Polygon> _polies = new List<Polygon>();

        #region Properties

        public SolarAnalysisCalculator()
        {
//            _directSunlight = new CDirectSunLightNet();
            CalcMode = CalculationMode.Klux;
            Resolution = 64;
            NorthOrientationAngle = 0f;
        }

        /// <summary>
        ///     Regime of calculation for solar analysis
        /// </summary>
        public CalculationMode CalcMode
        {
            get { return _calcMode; }
            set
            {
                _calcMode = value;
                //switch (value)
                //{
                //    case CalculationMode.Klux:
                //        _directSunlight.SetCalculationMode(DirectSunLightNet.CalculationMode.illuminance_klux);
                //        break;
                //    case CalculationMode.KcdPm2:
                //        _directSunlight.SetCalculationMode(
                //            DirectSunLightNet.CalculationMode.luminance_kcd_per_square_meter);
                //        break;
                //    default:
                //        _directSunlight.SetCalculationMode(DirectSunLightNet.CalculationMode.illuminance_klux);
                //        break;
                //}
            }
        }


        /// <summary>
        ///     Size of a cube element when computing
        /// </summary>
        public int Resolution
        {
            get { return _resolution; }
            set { _resolution = value; }
        }

        /// <summary>
        ///     Path to HDRI texture with solar radiation information
        /// </summary>
        public string PathToHdri
        {
            get { return _pathToHdri; }
            set
            {
                if (File.Exists(Path.GetFullPath(value)))
                    _pathToHdri = Path.GetFullPath(value);
                else if (File.Exists(Path.Combine(Environment.CurrentDirectory, value)))
                    _pathToHdri = Path.Combine(Environment.CurrentDirectory, value);
                else
                    throw new FileNotFoundException("HDRI file specified cannot be found " + value);
                //_directSunlight.SetHDRImage(_pathToHdri);
            }
        }

        /// <summary>
        ///     Orientation of the map to the north, in radians [0,2*pi].
        /// </summary>
        public float NorthOrientationAngle
        {
            get { return _northOrientationAngle; }
            set { _northOrientationAngle = value; }
        }

        # endregion


        public void ReadScenario(LuciCommunication.Communication2Luci.LuciAnswer scenario)
        {
            _polies.Clear();

            if (scenario.State != LuciCommunication.Communication2Luci.LuciState.Error && scenario.Outputs != null)
            {
                var geom = scenario.Outputs.Where(q => q.Key == "geometry").Select(q => q.Value).First();
                Console.Out.WriteLine("geom = " + geom);


                if (geom != null && geom is FeatureCollection)
                {
                    FeatureCollection fc = (FeatureCollection)geom;
                    int numPoly = fc.Count;
                    for (int f = 0; f < numPoly; f++)
                    {
                        MultiPolygon mls = (MultiPolygon)fc.Features[f].Geometry;
                        if (mls != null)
                        {
                            var e = mls.GetEnumerator();
                            var l = mls.ToList();
                            foreach (IGeometry g in l)
                            {
                                if (g is Polygon)
                                    _polies.Add((Polygon)g);
                            }
                            int numCoordinates = mls.NumPoints;
                            for (int i = 0; i < numCoordinates; i++)
                            {
                                Console.Out.WriteLine(mls.Coordinates[i].X + " / " + mls.Coordinates[i].Y + " / " + mls.Coordinates[i].Z);
                            }
                        }
                    }
                }
            }
        }

        public void ReadInputs(LuciCommunication.Communication2Luci.LuciAnswer resultRecv)
        {
            JToken ac = JObject.Parse(resultRecv.Message)["action"];
            JToken sc = JObject.Parse(resultRecv.Message)["ScID"];

            if (ac != null && (ac.ToString().ToLower().Equals("run") && sc != null && (long)sc >= 0))
            {
                JToken ts = JObject.Parse(resultRecv.Message)["timestamp_inputs"];
                JToken obj = JObject.Parse(resultRecv.Message)["SObjID"];
                JToken mode = JObject.Parse(resultRecv.Message)["inputs"]["calculationMode"];
                JToken rs = JObject.Parse(resultRecv.Message)["inputs"]["resolution"];
                JToken angle = JObject.Parse(resultRecv.Message)["inputs"]["northOrientationAngle"];

                JToken hdri = JObject.Parse(resultRecv.Message)["inputs"]["HdriImage"];
                JToken pts = JObject.Parse(resultRecv.Message)["inputs"]["Points"];
                JToken nm = JObject.Parse(resultRecv.Message)["inputs"]["Normales"];

                if (ts != null)
                    Timestamp = (long)ts;

                if (obj != null)
                    ObjId = (long)obj;

                if (sc != null)
                    ScId = (long)sc;


                // get parameters
                CalculationMode calculationMode;
                if (mode != null)
                {
                    Enum.TryParse((string)mode, true, out calculationMode);
                    CalcMode = calculationMode;
                }

                if (rs != null)
                    Resolution = (int)rs;

                if (angle != null)
                    NorthOrientationAngle = (float)angle;

                // get information about HDR image of sky
                LuciCommunication.Communication2Luci.LuciFileData hdriFd = new LuciCommunication.Communication2Luci.LuciFileData();
                if (hdri != null)
                    hdriFd = JsonConvert.DeserializeObject<LuciCommunication.Communication2Luci.LuciFileData>(hdri.ToString());

                LuciCommunication.Communication2Luci.LuciFileData pFd = new LuciCommunication.Communication2Luci.LuciFileData();
                if (pts != null)
                    pFd = JsonConvert.DeserializeObject<LuciCommunication.Communication2Luci.LuciFileData>(pts.ToString());

                LuciCommunication.Communication2Luci.LuciFileData nFd = new LuciCommunication.Communication2Luci.LuciFileData();
                if (nm != null)
                    nFd = JsonConvert.DeserializeObject<LuciCommunication.Communication2Luci.LuciFileData>(nm.ToString());


                if ((_lastHdrIchecksum == null || !_lastHdrIchecksum.Equals(hdriFd.streaminfo.checksum)) && File.Exists("temp.hdr"))
                    File.Delete("temp.hdr");
                if (!File.Exists("temp.hdr"))
                {
                    File.WriteAllBytes("temp.hdr", resultRecv.Attachments[hdriFd.streaminfo.order - 1].Data);
                    _lastHdrIchecksum = hdriFd.streaminfo.checksum;
                    PathToHdri = "temp.hdr";
                }

                // get actual input points and normales
                byte[] ptsData = resultRecv.Attachments[pFd.streaminfo.order - 1].Data;
                byte[] normalsData = resultRecv.Attachments[nFd.streaminfo.order - 1].Data;
                var points = new double[ptsData.Length / sizeof(double)];
                var normales = new double[normalsData.Length / sizeof(double)];
                Buffer.BlockCopy(ptsData, 0, points, 0, ptsData.Length);
                Buffer.BlockCopy(normalsData, 0, normales, 0, normalsData.Length);
                var pointsV = new CGeom3d.Vec_3d[points.Length / 3];
                var normalesV = new CGeom3d.Vec_3d[normales.Length / 3];
                for (var i = 0; i < pointsV.Length; i++)
                {
                    pointsV[i] = new CGeom3d.Vec_3d(points[3 * i], points[3 * i + 1], points[3 * i + 2]);
                    normalesV[i] = new CGeom3d.Vec_3d(normales[3 * i], normales[3 * i + 1], normales[3 * i + 2]);
                }

                _pointsV = pointsV;
                _normalesV = normalesV;
            }
        }

        public double[] Calculate () {
            return Calculate(_pointsV, _normalesV);

            var results = new double[_pointsV.Length];

            try
            {
                /*
                _directSunlight.SetResolution(Resolution);
                _directSunlight.SetHDRImage(PathToHdri);
                _directSunlight.PrepareHDRCubeMap(NorthOrientationAngle);
                _directSunlight.StartOpenCLOnceAndForever();
                switch (CalcMode)
                {
                    case CalculationMode.Klux:
                        _directSunlight.SetCalculationMode(DirectSunLightNet.CalculationMode.illuminance_klux);
                        break;
                    case CalculationMode.KcdPm2:
                        _directSunlight.SetCalculationMode(
                            DirectSunLightNet.CalculationMode.luminance_kcd_per_square_meter);
                        break;
                    default:
                        _directSunlight.SetCalculationMode(DirectSunLightNet.CalculationMode.illuminance_klux);
                        break;
                }

                _directSunlight.SetNormale(new CGeom3d.Vec_3d(0, 0, 1));

                var erg = new float[results.Length * 3];
                _directSunlight.CalculateDirectSunLight(_pointsV, _normalesV, ref erg);
                float r, g, b;
                for (var i = 0; i < results.Length; i++)
                {
                    r = erg[i * 3];
                    g = erg[i * 3 + 1];
                    b = erg[i * 3 + 2];
                    results[i] = Math.Sqrt(r * r + g * g + b * b);
                }*/
            }
            catch (Exception ex)
            {
                results = null;
            }

            return results;

        }


        public void InitDirectSunlight()
        {
            if (_directSunlight == null)
            {
                _directSunlight = new DirectSunLightNet.CDirectSunLightNet();
                _directSunlight.SetResolution(64);


                string pathToHdriFile = Path.GetFullPath("C:\\home\\dev\\eth\\luciCPlan\\Various\\HDRI\\HDRISky_2015_1Year_Singapore.hdr");//"CA Cumulative Sky - Year 2012 23 Minute step.hdr");
                if (!File.Exists(pathToHdriFile))
                {
                    //pathToHdriFile = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "\\CA Cumulative Sky - Year 2012 23 Minute step.hdr"); ;
                    pathToHdriFile = Path.GetFullPath(Environment.CurrentDirectory + "\\CA Cumulative Sky - Year 2012 23 Minute step.hdr");
                    if (!File.Exists(pathToHdriFile))
                    {
                        pathToHdriFile = Path.GetFullPath(Environment.CurrentDirectory + "\\data\\HDRI\\CA Cumulative Sky - Year 2012 23 Minute step.hdr");
                        if (!File.Exists(pathToHdriFile))
                        {
 //                           MessageBox.Show("There is no valid path to a HDRI file!");
                        }
                    }
                }

                //_directSunlight.SetHDRImage("../../../../Various/HDRI/CA Cumulative Sky - Year 2012 23 Minute step.hdr");
                _directSunlight.SetHDRImage(pathToHdriFile);
                _directSunlight.SetCalculationMode(DirectSunLightNet.CalculationMode.illuminance_klux);
                _directSunlight.PrepareHDRCubeMap(0.0f);
                _directSunlight.StartOpenCLOnceAndForever();
            }
        }

        public double[] Calculate(CGeom3d.Vec_3d[] positions, CGeom3d.Vec_3d[] normales)
        {
            InitDirectSunlight();
            _directSunlight.DeleteAllGeometry();

            switch (CalcMode)
            {
                case CalculationMode.Klux:
                    _directSunlight.SetCalculationMode(DirectSunLightNet.CalculationMode.illuminance_klux);
                    break;
                case CalculationMode.KcdPm2:
                    _directSunlight.SetCalculationMode(
                        DirectSunLightNet.CalculationMode.luminance_kcd_per_square_meter);
                    break;
                default:
                    _directSunlight.SetCalculationMode(DirectSunLightNet.CalculationMode.illuminance_klux);
                    break;
            }

            _directSunlight.SetNormale(new CGeom3d.Vec_3d(0, 0, 1));

            int h = 1;
            foreach (Polygon p in _polies)
            {
                CGeom3d.Vec_3d[][] vec3DList = new CGeom3d.Vec_3d[1][];
                CGeom3d.Vec_3d[] loop = new CGeom3d.Vec_3d[p.Coordinates.Length-1];
                for (int j = 0; j < p.Coordinates.Length-1; j++)
                    loop[j] = new CGeom3d.Vec_3d(p.Coordinates[j].X, p.Coordinates[j].Y, (double.IsNaN(p.Coordinates[j].Z) ? h : p.Coordinates[j].Z));
                vec3DList[0] = loop;

                _directSunlight.AddPolygon(vec3DList, 1.0f);
                h++;
            }

            // do the calculation
            var _results = new double[positions.Length];
            float[] erg = new float[_results.Length * 3];
            _directSunlight.CalculateDirectSunLight(positions, normales, ref erg);
            for (int i = 0; i < _results.Length; i++) // TODO: it was ++i . What is correct? Just have no Idea!
            {
                float r = erg[i * 3];
                float g = erg[i * 3 + 1];
                float b = erg[i * 3 + 2];

                _results[i] = Math.Sqrt(r * r + g * g + b * b);
            }

            return _results;
        }


        public double[] Calculate2(CGeom3d.Vec_3d[] positions, CGeom3d.Vec_3d[] normales)
        {
            var results = new double[positions.Length];

            try
            {
                _directSunlight.SetResolution(Resolution);
                _directSunlight.SetHDRImage(PathToHdri);
                _directSunlight.PrepareHDRCubeMap(NorthOrientationAngle);
                _directSunlight.StartOpenCLOnceAndForever();
                switch (CalcMode)
                {
                    case CalculationMode.Klux:
                        _directSunlight.SetCalculationMode(DirectSunLightNet.CalculationMode.illuminance_klux);
                        break;
                    case CalculationMode.KcdPm2:
                        _directSunlight.SetCalculationMode(
                            DirectSunLightNet.CalculationMode.luminance_kcd_per_square_meter);
                        break;
                    default:
                        _directSunlight.SetCalculationMode(DirectSunLightNet.CalculationMode.illuminance_klux);
                        break;
                }

                _directSunlight.SetNormale(new CGeom3d.Vec_3d(0, 0, 1));

                var erg = new float[results.Length * 3];
                _directSunlight.CalculateDirectSunLight(positions, normales, ref erg);
                float r, g, b;
                for (var i = 0; i < results.Length; i++)
                {
                    r = erg[i * 3];
                    g = erg[i * 3 + 1];
                    b = erg[i * 3 + 2];
                    results[i] = Math.Sqrt(r * r + g * g + b * b);
                }
            }
            catch (Exception ex)
            {
                results = null;
            }

            return results;
        }

        public double[] Calculate(IEnumerable<Vector3d> positions, IEnumerable<Vector3d> normales)
        {
            return Calculate(
                positions.Select(p => new CGeom3d.Vec_3d(p.X, p.Y, p.Z)).ToArray(),
                normales.Select(p => new CGeom3d.Vec_3d(p.X, p.Y, p.Z)).ToArray()
                );
        }

    }
}
