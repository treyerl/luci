﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using DirectSL;
using GeoAPI.Geometries;
using Lucy;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NetTopologySuite.Features;
using NetTopologySuite.Geometries;
using NetTopologySuite.IO;
using Newtonsoft.Json.Linq;
using OpenTK;
using Geometry = NetTopologySuite.Geometries.Geometry;

namespace Tests
{
    [TestClass]
    public class SaTest
    {
        private const string HdriFileName = @"resources\HDRI\CA Cumulative Sky - Year 2012 23 Minute step.hdr";
        private const string JsonGeomFileName = @"resources\NtsMultiPolygon.json";
        private readonly CGeom3d.Vec_3d[] _points = { new CGeom3d.Vec_3d(1, 0, 1), new CGeom3d.Vec_3d(1, 0, 31) };
        private readonly CGeom3d.Vec_3d[] _normales = { new CGeom3d.Vec_3d(10, 1, 0), new CGeom3d.Vec_3d(0, 1, 0) };

        [TestMethod]
        public void SaLocalExecution()
        {
            var sa = new DirectSL.SolarAnalysis(HdriFileName);
            Assert.IsNotNull(sa);

            var geometryBuildings = new GeoJsonReader().Read<MultiPolygon>(File.ReadAllText(JsonGeomFileName));
            var converter = new Pg3DNtsInterop.Pg3DNtsConverter();
            foreach (var pvv in geometryBuildings.Geometries.Select(g => converter.PolygonToPureGeom((Polygon) g)))
                sa.AddPolygon(pvv, 1);

            var r = sa.Calculate(_points, _normales);
            foreach (var n in r)
                Console.Out.WriteLine(n);
        }



        [TestMethod]
        public void SaRemoteExecution()
        {
            // ip adress of Lucy to connect
            var LucyIp = "192.168.137.2"; //127.0.0.1 192.168.137.2 129.132.6.36 129.132.6.60

            // image file to send with scenario
            var hdri = File.ReadAllBytes(HdriFileName);
            Assert.IsNotNull(hdri);

            // geometry for the scenario
            var geometryBuildings = new GeoJsonReader().Read<MultiPolygon>(File.ReadAllText(JsonGeomFileName));
            
            // Results of computation
            double[] remoteResults;
            double[] localResults;

            // running connection manager for the service. It is IDisposable, so it's preferable to use "using" syntax for proper resource management
            using (var cms = new ConnectionManager(LucyIp, 7654, "lukas", "1234"))
            {
                var s = new SolarAnalysis();
                // registering service in Lucy
                cms.RegisterService(s);
                // running service in separate thread
                cms.RunServiceLoopAsync(s);

                // running connection for the client
                using (var cmc = new ConnectionManager(LucyIp, 7654, "lukas", "1234"))
                {
                    // Creating a scenario is semi-automatic in current version of library 
                    var scenarioObj = cmc.CreateScenario("SolarAnalysisSampleScenario",
                        // put GeoJSON geometry into scenario
                        JObject.FromObject(new {SiGeometry = 
                        (JObject)(new Lucy.Geometry()
                        {
                            Format = "GeoJSON",
                            Data = new FeatureCollection(new Collection<IFeature>() { new Feature((IGeometry)geometryBuildings, new AttributesTable()) })
                        })
                        }));
                    int? scId;
                    try { scId = scenarioObj.SelectToken("result.ScID").Value<int>(); }
                    catch (Exception ex) { throw new ConnectionManager.LucyCommunicationError("Could not create Scenario. " + ex.Message); }

                    // creating a service instance with input data
                    var points = _points.SelectMany(p => new[] { p.x, p.y, p.z }).ToArray();
                    var normales = _normales.SelectMany(p => new[] { p.x, p.y, p.z }).ToArray();
                    // here we create byte array and StreamInfo based on it (MD5 generated based on bytes)
                    byte[][] attachedBytes = new byte[3][];
                    attachedBytes[0] = new byte[points.Length * sizeof(double)];
                    attachedBytes[1] = new byte[normales.Length * sizeof(double)];
                    attachedBytes[2] = hdri;
                    Buffer.BlockCopy(points, 0, attachedBytes[0], 0, attachedBytes[0].Length);
                    Buffer.BlockCopy(normales, 0, attachedBytes[1], 0, attachedBytes[1].Length);

                    var remoteServiceInstance = cmc.CreateServiceInstance(s.ClassName, scId,
                        new Dictionary<string, object>()
                        {
                            {"Points", new StreamInfo(attachedBytes[0], 1, "doublesXYZ")}, // array of doubles: XYZ coordinates
                            {"Normales", new StreamInfo(attachedBytes[1], 2, "doublesXYZ")}, // array of doubles: XYZ coordinates
                            // Compulsory scenario parameter - HDRI (Information about file)
                            {"HdriImage", new Lucy.StreamInfo(hdri, 3, "hdrImage")},
                            // Optional scenario parameters. If they are not listed in a list of parameters. TODO: Now they are obligatory :(
                            {"calculationMode", SolarAnalysis.CalculationMode.Klux.ToString() },
                            {"resolution", 64},
                            {"northOrientationAngle", 0f}
                        }, attachedBytes);

                    // executing Service
                    // asynchronious execution via mqtt (it is awaitable, so you could use "await remoteAsyncExecTask" to receive result in async context)
                    var remoteAsyncExecTask = remoteServiceInstance.ExecuteAsync();

                    // retreiving result of async execution
                    var remoteAsyncExecResult = remoteAsyncExecTask.Result;
                    var radSi = (StreamInfo)remoteAsyncExecResult.Item1["Radiation"];
                    remoteResults = new double[remoteAsyncExecResult.Item2[radSi.Order - 1].Length/sizeof(double)];
                    Buffer.BlockCopy(remoteAsyncExecResult.Item2[radSi.Order - 1], 0, remoteResults, 0, remoteAsyncExecResult.Item2[radSi.Order - 1].Length);

                } // here you could put a breakpoint, debug test and compare the values of local and remote executions (Use Debug configuration, not release one, otherwise unused variables are optimized out)
                // service before disposing ConnectionManager (actually it will stop it on disposing anyway, so this row is not important)
                cms.StopServiceLoop(s);
            }

            {
                var sa = new DirectSL.SolarAnalysis(HdriFileName);
                Assert.IsNotNull(sa);
                var converter = new Pg3DNtsInterop.Pg3DNtsConverter();
                foreach (var pvv in geometryBuildings.Geometries.Select(g => converter.PolygonToPureGeom((Polygon)g)))
                    sa.AddPolygon(pvv, 1);

                localResults = sa.Calculate(_points, _normales);
            }

            Assert.IsTrue(localResults.Zip(remoteResults, (d, d1) => (d-d1)/(d+d1)).All(x => x < 10e-5));
        }


        //[TestMethod]
        //public void SaContinuousServiceRun()
        //{
        //    // ip adress of Lucy to connect
        //    var LucyIp = "192.168.137.2"; //127.0.0.1 192.168.137.2 129.132.6.36 129.132.6.60
        //    // running connection manager for the service. It is IDisposable, so it's preferable to use "using" syntax for proper resource management
        //    using (var cms = new ConnectionManager(LucyIp, 7654, "lukas", "1234"))
        //    {
        //        var s = new SolarAnalysis();
        //        // registering service in Lucy
        //        cms.RegisterService(s);
        //        // running service in separate thread
        //        cms.RunServiceLoop(s);
        //    }
        //}
    }
}
