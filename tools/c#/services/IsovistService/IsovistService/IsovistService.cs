﻿using System;
using System.Collections.Generic;
using System.Linq;
using CPlan.Geometry;
using CPlan.Isovist2D;
using Lucy;
using NetTopologySuite.Geometries;
using NetTopologySuite.IO;
using Newtonsoft.Json.Linq;
using OpenTK;

namespace IsovistService
{
    /// <summary>
    /// Isovist service inherits SimpleLucyService, that allows to derive name, version, and description from assembly properties (e.g. ClassName = name of the project assembly)
    /// </summary>
    public class IsovistService : SimpleLucyService
    {
        // the only method one needs to override
        public override Tuple<Dictionary<string, object>, byte[][]> Execute(Dictionary<string, object> inputData, params byte[][] attachments)
        {
            // get coordinates stored in input
            var coordinates =
                ((JArray)(((object[])inputData["inputVals"])[0])).Zip(
                ((JArray)(((object[])inputData["inputVals"])[1])),
                (a, b) => new Vector2d(a.Value<double>(), b.Value<double>())
                ).ToList();

            var lines = new List<Line2D>();
            // get geometry from scenario object
            if (ScenarioObject != null)
            {
                // Get the Geometry object that contain all GeoJSON
                var scPolygons = (Lucy.Geometry)ScenarioObject.SelectToken("result.geometry.GeoJSON").Value<JObject>();
                // Select all polygons
                var pols = scPolygons.Data.Features.SelectMany(f => ((MultiPolygon) f.Geometry).Geometries).ToArray();
                // Select all lines in all polygons
                lines =
                    pols.SelectMany(
                        p => p.Coordinates.Zip(
                            p.Coordinates.Skip(1).Concat(new[] { p.Coordinates[0] }), 
                            (a, b) => new Line2D(new Vector2d(a.X, a.Y), new Vector2d(b.X, b.Y))))
                        .ToList();

                
            }

            // create sample iso object on client
            var isoClientOrig = new IsovistField2D(coordinates, lines);
            //perform actual calculations, the precision need to be set explicitly here since we can't do it using isovist constructor
            isoClientOrig.Calculate(isoClientOrig.Precision);
            //wrap the results
            var isoResultsWrapper = new IsovistField2DWrapper(isoClientOrig);
            // send the results back to client
            return new Tuple<Dictionary<string, object>, byte[][]>(new Dictionary<string, object>() { { "outputVals", new GeoJsonWriter().Write(isoResultsWrapper) } }, null);
        }

        // declare input/output as dictionary
        public override Dictionary<string, TransferValueType> Inputs
        {
            get { return new Dictionary<string, TransferValueType>(){{"inputVals", TransferValueType.LIST}}; }
        }

        public override Dictionary<string, TransferValueType> Outputs
        {
            get { return new Dictionary<string, TransferValueType>() { { "outputVals", TransferValueType.STRING }}; }
        }
    }
}
