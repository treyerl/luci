using LuciCommunication;
using GeoAPI.Geometries;
using NetTopologySuite.Features;
using NetTopologySuite.Geometries;
using NetTopologySuite.IO;
using NetTopologySuite.IO.Converters;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LuciSerializers;

namespace IsovistService
{
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    class Program
    {
        static void Main(string[] args)
        {
            GeoJsonWriter GeoJSONWriter = new GeoJsonWriter();
            GeoJsonReader GeoJSONReader = new GeoJsonReader();
            Communication2Luci cl = new Communication2Luci();

            string result = "Connection to Luci was ";
            string host = "localhost";
            //    host = "129.132.6.4";

            if (args.Count() > 0)
                host = args[0];

            if (!cl.connect(host, 7654))
                result += "not ";

            result += "successful! \n";

            Console.Out.WriteLine(result);

            Console.Out.WriteLine("-> authenticate\n");
            LuciCommunication.Communication2Luci.LuciAnswer? a = cl.authenticate("lukas", "1234");

            if (a != null)
            {
                LuciCommunication.Communication2Luci.LuciAnswer answer = (LuciCommunication.Communication2Luci.LuciAnswer)a;

                Console.Out.WriteLine(answer.Message);
                if (answer.State != Communication2Luci.LuciState.Error)
                {

                    Console.Out.WriteLine("-> registering service\n");

                    // Register Service
                    var inputDef = new List<KeyValuePair<string, string>>
                    {
                        new KeyValuePair<string, string>("inputVals", LuciType.JSON)
                    };

                    var outputDef = new List<KeyValuePair<string, string>>
                    {
                        new KeyValuePair<string, string>("outputVals", LuciType.STRING)
                    };

                    string machinename = System.Net.Dns.GetHostName();

                    answer = cl.registerService("Isovist", "0.1", "Simple Demo for Isovist", host, inputDef, outputDef);
                    Console.Out.WriteLine(answer.Message);

                    while (true)
                    {
                        LuciCommunication.Communication2Luci.LuciAnswer resultRecv = cl.receiveMessage();

                        JToken ac = JObject.Parse(resultRecv.Message)["action"];
                        JToken sc = JObject.Parse(resultRecv.Message)["ScID"];

                        if (ac != null && (ac.ToString().ToLower().Equals("run") && sc != null && (long)sc >= 0))
                        {
                            JToken ts = JObject.Parse(resultRecv.Message)["timestamp_inputs"];
                            JToken obj = JObject.Parse(resultRecv.Message)["SObjID"];
                            JToken inputs = JObject.Parse(resultRecv.Message)["inputs"];

                            long timestamp = 0;
                            if (ts != null)
                                timestamp = (long)ts;

                            long objId = 0;
                            if (obj != null)
                                objId = (long)obj;

                            long scId = 0;
                            if (sc != null)
                                scId = (long)sc;

                            List<Vector2d> points = new List<Vector2d>();

                            // prepare input values
                            if (inputs != null && inputs.HasValues)
                            {
                                JArray pts = inputs.Value<JObject>("inputVals").Value<JArray>("coordinates");
                                int numP = pts.Count();
                                for (int j = 0; j < numP; j++)
                                {
                                    points.Add(new Vector2d((double)pts[j][0], (double)pts[j][1]));
                                }
                            }

                            List<CPlan.Geometry.Line2D> lines = new List<CPlan.Geometry.Line2D>();

                            // fetch scenario 
                            LuciCommunication.Communication2Luci.LuciAnswer scenario = cl.getScenario((int)scId);
                            Console.Out.WriteLine(scenario);

                            if (scenario.State != Communication2Luci.LuciState.Error && scenario.Outputs != null)
                            {
                                var geom = scenario.Outputs.Where(q => q.Key == "geometry").Select(q => q.Value).First();
                                if (geom != null && geom is FeatureCollection)
                                {
                                    FeatureCollection fc = (FeatureCollection)geom;
                                    int numPoly = fc.Count;
                                    for (int f = 0; f < numPoly; f++)
                                    {
                                        Geometry g = (Geometry)fc.Features[f].Geometry;
                                        if (g != null)
                                        {
                                            int numCoordinates = g.NumPoints;
                                            for (int i = 0; i < numCoordinates; i++)
                                            {
                                                for (int c = 0; c < numCoordinates - 1; c++)
                                                {
                                                    Coordinate coord = g.Coordinates[c];
                                                    Coordinate coordNext = g.Coordinates[c + 1];

                                                    lines.Add(new CPlan.Geometry.Line2D(new Vector2d(coord.X, coord.Y),
                                                              new Vector2d(coordNext.X, coordNext.Y)));
                                                }
                                            }
                                        }
                                    }

                                    //create sample iso object on client
                                    CPlan.Isovist2D.IsovistField2D iso_client_orig = new CPlan.Isovist2D.IsovistField2D(points, lines, 0.05f);

                                    //perform actual calculations, the precision need to be set explicitly here since we can't do it using isovist constructor
                                    iso_client_orig.Calculate(iso_client_orig.Precision, true);

                                    //wrap the results
                                    IsovistResultsWrapper iso_results_wrapper = new IsovistResultsWrapper(iso_client_orig.Results);
                                    string iso_server_send = GeoJSONWriter.Write(iso_results_wrapper);

                                    // send output
                                    List<KeyValuePair<string, object>> results = new List<KeyValuePair<string, object>>();
                                    results.Add(new KeyValuePair<string, object>("outputVals", iso_server_send));

                                    LuciCommunication.Communication2Luci.LuciAnswer conf = cl.sendResults(objId, "0.1", host, timestamp, results, 1);
                                    Console.Out.WriteLine(conf.Message);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
