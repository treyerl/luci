package org.luci.console;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import jline.console.ConsoleReader;
import jline.console.CursorBuffer;
import jline.console.completer.FileNameCompleter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.luci.connect.DirectStreamWriter;
import org.luci.connect.LcInputStream;
import org.luci.connect.LcJSONObject;
import org.luci.connect.LcOutputStream;
import org.luci.connect.LcString;
import org.luci.connect.LuciConnect;
import org.luci.connect.LuciResponse;
import org.luci.connect.DirectStreamWriter;

public class ClientConsole extends LuciConnect implements Runnable{
	public final static String black = "\u001B[30m";
	public final static String red = "\u001B[31m";
	public final static String green = "\u001B[32m";
	public final static String yellow = "\u001B[33m";
	public final static String blue = "\u001B[34m";
	public final static String pink = "\u001B[35m";
	public final static String cyan = "\u001B[36m";
	public final static String grey = "\u001B[37m";
	public final static String bold = "\u001B[1m";
	public final static String normal = "\u001B[0m";
	public final static String resetColor = "\u001B[39;49m";
	
	
	protected BufferedReader instream;
	protected ConsoleReader reader;
	private String lastconnection = "localhost:7654";
	private String[] lastuser = new String[]{"lukas", "1234"};
	private List<String> actions;
	private boolean isIdle = false;
	protected PrintWriter out;
	protected boolean DEBUG = false;
	protected String prompt;
	protected boolean isColorSupported = System.console() != null;
	protected final ExecutorService pool;
	
	public ClientConsole() throws IOException {
		super();
		reader = new ConsoleReader();
        reader.addCompleter(new FileNameCompleter());
        out = new PrintWriter(reader.getOutput());
		actions = new ArrayList<String>();
		if (isColorSupported){
			String[] prefixes = new String[]{
				bold+resetColor+"--> "+normal,
				bold+red+"--> "+resetColor+normal,
				bold+red+"ERR "+resetColor+normal,
				"no errors"
			};
			LuciResponse.configurePrefixes(prefixes);
			prompt = bold+yellow+"<-- ";
		} else {
			String[] prefixes = new String[]{
					"--> ", //STD_OK
					"--> ", //STD_OK_WITH_ERRORS
					"ERR ", // STD_ERROR
					"no errors"
				};
			LuciResponse.configurePrefixes(prefixes);
			prompt = "<-- ";
		}
		setPrompt();
		reader.setCopyPasteDetection(true);
		pool = Executors.newCachedThreadPool();
	}
	
	private void setPrompt(){
		if (reader != null) reader.setPrompt(prompt);
	}
	
	private String indent(String str, int chars){
		String indent = "";
		for (int i = 0; i < chars; i++) indent += " ";
		return str.replace("\n", "\n"+indent);
	}
	
	private void printAutoAction(String action){
		out.println(prompt+action);
		out.flush();
	}
	
	private void printAction(String action){
		if (isColorSupported) out.println(bold+blue+"<-- "+normal+resetColor+indent(action, 4));
		else out.println("<-- "+indent(action, 4));
		out.flush();
	}
	
	@Override
	protected void print(LuciResponse r){
		CursorBuffer stashed = reader.getCursorBuffer().copy();
		try {
			if (isColorSupported && isIdle){
				reader.getOutput().write("\u001b[1G\u001b[K");
				reader.flush();
			}
	        synchronized(out){
	        	out.println(indent(r.print(3), 4));
	    		out.println();
	    		out.flush();
	        }
		    if (isColorSupported && isIdle) {
		    	reader.resetPromptLine(reader.getPrompt(), stashed.toString(), stashed.cursor);
		    }
		} catch (IOException e){
			e.printStackTrace();
		}
	}
	
	protected void print(LuciResponse r, String s){
		out.print(indent(r.print(s), 4));
		out.println();
		out.flush();
	}
	
	protected void info(String i){
		if (isColorSupported) out.println(indent(blue+"::: "+i, 4));
		else out.println(indent("::: "+i, 4));
		out.println();
		out.flush();
	}
	
	private void connect(String arg, boolean reconnect) 
			throws InterruptedException, JSONException, NoSuchAlgorithmException, IOException{
		String[] args = arg.split(":");
		String server = args[0];
		String port = (args.length > 1) ? args[1]: "7654";
		lastconnection = server+":"+port;
		int p = Integer.parseInt(port);
		try{
			int timeout = 5000;
			print(connect(server, p, timeout));
			if (reconnect) {
				printAutoAction("log in with default credentials");
				login(lastuser);
			}
		} catch (IOException e){
			print(LuciResponse.Error("Host '"+lastconnection+"' not available. "+e.toString(), 0));
		}
	}
	
	private void connect(String arg) throws InterruptedException, JSONException, NoSuchAlgorithmException, IOException{
		connect(arg, false);
	}
	
	private void reconnect() throws JSONException, NoSuchAlgorithmException, InterruptedException, IOException{
		disconnect();
		connect(lastconnection, true);
	}
	
	private void createUser(String name, String email) throws IOException, JSONException, NoSuchAlgorithmException, InterruptedException{
		reader.setPrompt(bold+grey+"::password ");
		String password = reader.readLine("PWD:", '*');
		JSONObject send = new JSONObject("{'action':'create_user'}");
		send.put("username", name);
		send.put("userpasswd", password);
		send.put("email", email);
		sent = send.toString();
		LuciResponse r = sendAndReceive(sent);
		if (DEBUG) printAction(sent);
		print(r);
		setPrompt();
	}
	
	private void login(String name) throws IOException, NoSuchAlgorithmException, InterruptedException{
		reader.setPrompt(bold+grey+"::password ");
		String password = reader.readLine("PWD:", '*');
		JSONObject send = new JSONObject("{'action':'authenticate'}");
		send.put("username", name);
		send.put("userpasswd", password);
		sent = send.toString();
		LuciResponse r = sendAndReceive(sent);
		if (DEBUG) printAction(sent);
		print(r);
		setPrompt();
	}
	
	private void login(String[] creds) throws InterruptedException, NoSuchAlgorithmException, IOException{
		JSONObject send = new JSONObject("{'action':'authenticate'}");
		send.put("username", creds[0]);
		send.put("userpasswd", creds[1]);
		sent = send.toString();
		if (DEBUG) printAction(sent);
		LuciResponse r = sendAndReceive(sent);
		print(r);
	}
	
	private void list(String plugin, boolean print) throws InterruptedException, NoSuchAlgorithmException, IOException{
		JSONObject send = new JSONObject("{'action':'get_list'}")
			.put("of", new JSONArray(new String[]{plugin+"s"}));
		sent = send.toString();
		if (DEBUG) printAction(sent);
		LuciResponse r = sendAndReceive(sent);
		if (r.isOK()){
			JSONObject result = r.json.getJSONObject("result");
			if (result.has("actions")) {
				actions = new ArrayList<String>();
				JSONArray ac = result.getJSONArray("actions");
				for (int i = 0; i < ac.length(); i++) actions.add(ac.getString(i));
			}
			
			for (String key: JSONObject.getNames(result)){
				List<String> plugins = new ArrayList<String>();
				JSONArray pluginnames = result.getJSONArray(key);
				for (int i = 0; i < pluginnames.length(); i++){
					plugins.add(pluginnames.get(i).toString());
				}
				if (print) print(r, LcString.trim(plugins.toString(), 1).replace(", ", "\r\n"));
			}
		} else if (r.isError()){
			if (print) print(r);
		}
	}
	
	private void get_infos_about(String key, String actionname) 
			throws InterruptedException, JSONException, NoSuchAlgorithmException, IOException{
		JSONObject send = new JSONObject("{'action':'get_infos_about'}");
		send.put(key, actionname);
		sent = send.toString();
		if (DEBUG) printAction(sent);
		LuciResponse r = sendAndReceive(sent);
		print(r);
	}
	
	private void get_infos_about_action(String actionname) 
			throws InterruptedException, JSONException, NoSuchAlgorithmException, IOException{
		get_infos_about("actionname", actionname);
	}
	
	private void get_infos_about_service(String servicename) 
			throws InterruptedException, JSONException, NoSuchAlgorithmException, IOException{
		get_infos_about("servicename", servicename);
	}
	
	private void reload(String[] args) throws JSONException, NoSuchAlgorithmException, IOException, InterruptedException {
		for (int i = 0; i < args.length; i++) args[i] = args[i].replace("_", " ");
		List<String> plugins = new ArrayList<String>(Arrays.asList(args));
		plugins.remove(0);
		JSONObject send = new JSONObject("{'action':'reload'}");
		if (plugins.size() > 0) send.put("plugins", plugins);
		sent = send.toString();
		if (DEBUG) printAction(sent);
		LuciResponse r = sendAndReceive(sent);
		print(r);
	}
	
	private void logout() throws JSONException, NoSuchAlgorithmException, IOException, InterruptedException{
		sent = "{'action':'logout'}";
		if (DEBUG) printAction(sent);
		LuciResponse r = sendAndReceive(sent);
		if (r.isOK() && r.json.getBoolean("result")) {
			print(r, "logged out");
			out.println();
			out.flush();
		}
		else print(r);
	}
	
	@Override
	public void run() {
		String in;
		out.print(bold+blue);
		out.println("command: connect [host:port]; default = localhost:7654, user=lukas, passwd=1234");
		out.println("command: reconnect (redo the last connect call)");
		out.println("command: create_user name email {passwd}; password asked separately");
		out.println("command: login name {passwd}; password asked separately");
		out.println("command: PLUGIN list (lists all available luci actions, services, scenarios)");
		out.println("command: action ACTIONNAME; get infos about action. hint: look up action name by calling action list");
		out.println("command: service SERVICENAME; get infos about service. hint: look up service name by calling service list");
		out.println("command: reload PLUGIN; reloads either all plugins or the ones specified; 'geometry converters' = geometry_converters");
		out.println("command: exit (exits the client console)");
		
		out.println("");
		out.flush();

        try {
			while ((in = reader.readLine()) != null) {
				isIdle = false;
				String[] args = in.split("\\s");
				if (args[0].equalsIgnoreCase("connect") || in.equalsIgnoreCase("reconnect")){
					if (args.length > 1) connect(args[1]);
					else reconnect();
				} else if (in.equalsIgnoreCase("quit") || in.equalsIgnoreCase("exit")) {
					disconnect();
			    	out.println(blue+"Exit Luci ClientConsole"+normal+resetColor);
			    	out.close();
			        break;
			    } else if (args[0].equals("debug")) {
					if (DEBUG == true) {
						DEBUG = false;
						info("turn OFF debug printing");
					} else {
						DEBUG = true;
						info("turn ON debug printing");
					}
				} else if (isConnected()){
					try {
						if (args[0].equalsIgnoreCase("create_user")) {
							switch(args.length){
							case 1: print(LuciResponse.Error("create_user: missing arguemnts 'name, email'", 0)); break;
							case 2: print(LuciResponse.Error("create_user: missing arguemnt 'email'", 0)); break;
							case 3: createUser(args[1], args[2]); break;
							}
						} else if(args[0].equalsIgnoreCase("login")) {
							if (args.length < 2) print(LuciResponse.Error("missing username", 0));
							else if (args.length > 2) print(LuciResponse.Error("too many arguments", 0));
							else login(args[1]);
						} else if (args.length == 2 && args[1].equals("list")) list(args[0], true);
						else if (args[0].equals("action") && args.length == 2) get_infos_about_action(args[1]);
						else if (args[0].equals("service") && args.length == 2) get_infos_about_service(args[1]);
						else if (args[0].equals("reload")) reload(args);
						else if (args[0].equals("disconnect")) print(disconnect());
						else if (args[0].equals("logout")) logout();
						else {
							LcJSONObject action = new LcJSONObject(in);
							filenamesToAttachments(action);
							IReceive r = new IReceive(){
								@Override
								public boolean receive(LuciResponse r) {
//									System.out.println("receive");
									r.receiveAttachments();
									
									print(r);
									return true;
								}
							};
							onReceive(r);
							send(action);
							if (DEBUG) printAction(sent);
						}
						
					} catch (JSONException e){
						print(LuciResponse.Error("Message not sent! Invalid JSON string: " + e.getMessage(), 0));
						if (DEBUG) e.printStackTrace();
					} catch (NoSuchAlgorithmException e) {
						e.printStackTrace();
					} catch (InterruptedException e){
						e.printStackTrace();
					}
				} else {
					print(LuciResponse.Error("Not connected. Use 'connect' or 'reconnect' to connect to a luci server.", 0));
				}
				isIdle = true;
			}
			
		} catch (IOException e) {
			if (DEBUG) e.printStackTrace();
			disconnect();
		} catch (JSONException e) {
			if (DEBUG) e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			if (DEBUG) e.printStackTrace();
		} catch (InterruptedException e) {
			if (DEBUG) e.printStackTrace();
		}
	}
	
	/**recursively scan JSONObject for strings where new File(string).exists() is true
	 * and load attachment 
	 * @param j JSONObject
	 */
	protected Map<File,LcInputStream> filenamesToAttachments(JSONObject j) {
		Map<File,LcInputStream> attachments = new HashMap<File,LcInputStream>();
		Map<String, String> crss = new HashMap<String, String>();
		Map<String, JSONObject> attributeMaps = new HashMap<String, JSONObject>();
		
		@SuppressWarnings("unchecked")
		Set<String> keys = j.keySet();
		for (String key: keys){
			if (key.contains("attributeMap")){
				JSONObject map = j.optJSONObject(key);
				if (map != null) attributeMaps.put(key.replace(".attributeMap",""), map);
			}
			if (key.equals("crs")){
				crss.put(key, j.getString(key));
			}
		}
		
		for (String key: attributeMaps.keySet()) j.remove(key+".attributeMap");
		
		for (String key: keys){
			Object v = j.get(key);
			if (v instanceof String){
				File f = new File((String) v);
				if (f.exists()){
					try {
						LcInputStream lis = LcInputStream.load(f, null, crss.get(key), 
								attributeMaps.get(key));
						lis.setReady();
						// setOrder ? 
						attachments.put(f,lis);
						LcOutputStream los = new LcOutputStream(lis);
						pool.submit(new DirectStreamWriter(lis, los, true, false, false, false));
						j.put(key,los);
					} catch (IOException e) {
						// go on
						e.printStackTrace();
					}
				}
			} else if (v instanceof JSONObject){
				attachments.putAll(filenamesToAttachments((JSONObject) v));
			}
		}
		return attachments;
	}

	public static void main(String[] args) throws IOException {
		Thread console = new Thread(new ClientConsole(), "ClientConsole");
		console.start();
	}

}
