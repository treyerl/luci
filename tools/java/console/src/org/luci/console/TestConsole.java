package org.luci.console;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;

import org.json.JSONException;
import org.luci.connect.DirectStreamWriter;
import org.luci.connect.LcInputStream;
import org.luci.connect.LcJSONObject;
import org.luci.connect.LcOutputStream;
import org.luci.connect.LuciResponse;

public class TestConsole extends ClientConsole{
	public TestConsole() throws IOException {
		super();
		if (isColorSupported){
			String[] prefixes = new String[]{
				bold+green+"OK  "+resetColor+normal,
				bold+yellow+"OK~ "+resetColor+normal,
				bold+red+"ERR "+resetColor+normal,
				"no errors"
			};
			LuciResponse.configurePrefixes(prefixes);
			prompt = bold+yellow+"<-- ";
		} else {
			String[] prefixes = new String[]{
				"OK  ", //STD_OK
				"OK ", //STD_OK_WITH_ERRORS
				"ERR ", // STD_ERROR
				"no errors"
			};
			LuciResponse.configurePrefixes(prefixes);
			prompt = "<-- ";
		}
		connect("localhost", 7654, 5000);
	}
	
	HashMap<File, byte[]> sendFiles = new HashMap<File, byte[]>();
	
	public class ByteArrayOutputStream2 extends ByteArrayOutputStream {
		public ByteArrayOutputStream2(int size){
			super(size);
		}
		public byte[] getBuf(){return buf;}
	}
	
	@SuppressWarnings("unused")
	protected void runTest() throws IOException, JSONException, NoSuchAlgorithmException, InterruptedException, ExecutionException{
		// files
		File testfiles = new File(new File(".", "resources"), "testfiles");
		File shp = new File(testfiles, "shp");
		File shpPoint = new File(new File(shp, "VEC25_anl_p_Clip"), "VEC25_anl_p.shp");
		File dbfPoint = new File(new File(shp, "VEC25_anl_p_Clip"), "VEC25_anl_p.dbf");
		File shxPoint = new File(new File(shp, "VEC25_anl_p_Clip"), "VEC25_anl_p.shx");
		File shpPolygon = new File(new File(shp, "VEC25_geb_a_Clip"), "VEC25_geb_a.shp");
		File dbfPolygon = new File(new File(shp, "VEC25_geb_a_Clip"), "VEC25_geb_a.dbf");
		File shxPolygon = new File(new File(shp, "VEC25_geb_a_Clip"), "VEC25_geb_a.shx");
		File shpLine = new File(new File(shp, "VEC25_eis_l_Clip"), "VEC25_eis_l.shp");
		File dbfLine = new File(new File(shp, "VEC25_eis_l_Clip"), "VEC25_eis_l.dbf");
		File shxLine = new File(new File(shp, "VEC25_eis_l_Clip"), "VEC25_eis_l.shx");
		File eco = new File(testfiles, "ecopot");
		File ecoDistance = new File(eco, "distancesSmall.csv");
		File ecoHomes = new File(eco, "homesSmall.csv");
		File ecoWork = new File(eco, "workplacesSmall.csv");
		File pic1 = new File(testfiles, "georef.png");
		File pic2 = new File(testfiles, "phj_schneider.png");
		List<File> allFiles = Arrays.asList(new File[]{shpPoint, dbfPoint, shxPoint, shpLine, dbfLine, shxLine, ecoDistance, ecoHomes, ecoWork, pic1, pic2});
		//for (File f: allFiles) if (!f.exists()) System.err.println("File not found: "+f);
		
		if (!sendAndReceive("{'action':'authenticate','username':'lukas','userpasswd':'1234'}").isOk) return;

		// getlist
		String getList = "{'action':'get_list','of':['converters','actions','services', 'scenarios']}";
		print(sendAndReceive(getList), String.format("getList --- %.3fms", getCallDurationInMilliSeconds()));
		
		// get infos about
		String getInfos = "{'action':'get_infos_about','actionname':'get_infos_about'}";
		print(sendAndReceive(getInfos), String.format("getInfos --- %.3fms", getCallDurationInMilliSeconds()));
		
		// remote register
		String register = "{'action': 'remote_register','machinename': 'rennmobil', 'service': {'inputs':{'in1': 'number'}, 'description': 'string', 'classname': 'string', 'outputs':{'out1': 'number'}, 'version': 'string'}}";
		print(sendAndReceive(register), "register");
		
		// remote unregister
		String unregister = "{'action':'remote_unregister'}";
		print(sendAndReceive(unregister), "unregister");
		
		ArrayList<Integer> deleteScn = new ArrayList<Integer>();
		
		// create scenario 1
		String scn1 = "{'action':'create_scenario','name':'test', 'projection':{'bbox':[ [47.4638,8.3462],[47.2909,8.7273] ]}, 'geometry':{'KEYNAME.geojson':{'format':'GeoJSON','geometry':{'type':'Polygon', 'coordinates':[[[100.0,0.0],[101.0,0.0],[101.0,1.0],[100.0,0.0]]]}, 'crs':'EPSG:21781' }}}}";
		int scn1ID = createAndGetScenario(new LcJSONObject(scn1), 1, "GeoJSON", "with swiss CRS and BBOX");
		deleteScn.add(scn1ID);
		
		// create scenario 2
		String scn2 = "{'action':'create_scenario','name':'test','geometry':{'KEYNAME.geojson':{'format':'GeoJSON','geometry':{'type':'Polygon', 'coordinates':[[[100.0,0.0],[101.0,0.0],[101.0,1.0],[100.0,0.0]]]}, 'crs':'EPSG:21781' }}}}";
		int scn2ID = createAndGetScenario(new LcJSONObject(scn2), 2, "GeoJSON", "with swiss CRS");
		deleteScn.add(scn2ID);

		// create scenario 3
		String scn3 = "{'action':'create_scenario','name':'test','geometry':{'KEYNAME.geojson':{'format':'GeoJSON','geometry':{'type':'Polygon', 'coordinates':[[[100.0,0.0],[101.0,0.0],[101.0,1.0],[100.0,0.0]]]}, }}}}";
		int scn3ID = createAndGetScenario(new LcJSONObject(scn3), 3, "GeoJSON", "");
		deleteScn.add(scn3ID);

		// create scenario 4 with attributes
		String scn4 = "{'action':'create_scenario','name':'attribute_test','geometry':{'KEYNAME.geojson':{'format':'GeoJSON','geometry':{'type':'FeatureCollection','features':[{'type':'Feature','geometry':{'type':'Polygon','coordinates':[[[100.0,0.0],[101.0,0.0],[101.0,1.0],[100.0,0.0]]]},'properties':{'id':3,'height':20}}]}}}}";
		int scn4ID = createAndGetScenario(new LcJSONObject(scn4), 4, "GeoJSON", "with attributes");
		deleteScn.add(scn4ID);

		// create scenario 5 with shp + dbf files + attributemap; points
		List<File> missingScn5 = check(shpPoint, dbfPoint, shxPoint);
		int scn5ID = 0;
		if (missingScn5.size() == 0){
			String scn5 = "{'action':'create_scenario','name':'shp_file_test','geometry':{'anl.shp':'"+shpPoint.getAbsolutePath().replace("\\", "\\\\")+"',"
					+ "'anl.dbf':'"+dbfPoint.getAbsolutePath().replace("\\", "\\\\")+"', 'anl.dbf.attributeMap':{'geomid':'objectid','timestamp':'yearofchan', 'timestampformat':'YYYY'},"
					+ "'anl.shx':'"+shxPoint.getAbsolutePath().replace("\\", "\\\\")+"'}}";
			//		System.out.println(scn5);
			scn5ID = createAndGetScenario(new LcJSONObject(scn5), 5, "Shapefile", "with DBF and attributeMap; type=points");
		} else print(LuciResponse.Error("Could not perform shapefile test with Point geometry;\r\nmissing files: "+missingScn5, 0));
		deleteScn.add(scn5ID);

		// create scenario 6 with shp + dbf files + attributemap; polygons
		String scn6 = "{'action':'create_scenario','name':'shp_file_test','geometry':{'gebäude.shp':'"+shpPolygon.getAbsolutePath().replace("\\", "\\\\")+"', "
					+ "'gebäude.dbf':'"+dbfPolygon.getAbsolutePath().replace("\\", "\\\\")+"', 'gebäude.dbf.attributeMap':{'geomid':'objectid','timestamp':'yearofchan', 'timestampformat':'YYYY'}, "
					+ "'gebäude.shx':'"+shxPolygon.getAbsolutePath().replace("\\", "\\\\")+"'}}";
		int scn6ID = createAndGetScenario(new LcJSONObject(scn6), 6, "Shapefile", "with DBF and attributeMap; type=polygons", "geojson");
		deleteScn.add(scn6ID);

//		// create scenario 7 with shp + dbf files and missing shx file
//		List<File> missingScn7 = check(shpPoint, dbfPoint, shxPoint);
//		int scn7ID = 0;
//		if (missingScn7.size() == 0){
//			String scn7 = "{'action':'create_scenario','name':'shp_file_test','geometry':{'anl.shp':'"+shpPoint.getAbsolutePath().replace("\\", "\\\\")+"',"
//					+ "'anl.dbf':'"+dbfPoint.getAbsolutePath().replace("\\", "\\\\")+"'}}";
//			scn7ID = createAndGetScenario(new LcJSONObject(scn7), 7, "Shapefile", "with DBF and missing shx file; type=points", "geojson");
//		} else print(LuciResponse.Error("Could not perform shapefile test with Point geometry leaving out the shx file;\r\nmissing files: "+missingScn7,0));
//		deleteScn.add(scn7ID);

		// create scenario 8 with shp + dbf files + attributemap; polylines
		List<File> missingScn8 = check(shpLine, dbfLine, shxLine);
		int scn8ID = 0;
		if (missingScn8.size() == 0){
			String scn8 = "{'action':'create_scenario','name':'shp_file_test','geometry':{'eisenbahn.shp':'"+shpLine.getAbsolutePath().replace("\\", "\\\\")+"',"
					+ "'eisenbahn.dbf':'"+dbfLine.getAbsolutePath().replace("\\", "\\\\")+"','eisenbahn.dbf.attributeMap':{'geomid':'objectid','timestamp':'yearofchan', 'timestampformat':'YYYY'},"
					+ "'eisenbahn.shx':'"+shxLine.getAbsolutePath().replace("\\", "\\\\")+"'}}";
			scn8ID = createAndGetScenario(new LcJSONObject(scn8), 8, "Shapefile", "with DBF  and attributeMap; type=polylines", "geojson");
		} else print(LuciResponse.Error("Could not perform shapefile test with Line geometry;\r\nmissing files: "+missingScn8, 0));
		deleteScn.add(scn8ID);

		// update scenario
		if (scn1ID != 0){
			String update = String.format("{'action':'update_scenario','ScID':%d,'name':'my new %d-name for the existing scenario'}", scn1ID, (int)(Math.random()*10000));
			print(sendAndReceive(update), String.format("update Scenario --- %.3fms", getCallDurationInMilliSeconds()));
			printNewLine();
		}
		
		// TODO: update scenario with delete list
		
		// delete scenarios
		deleteScenarios(deleteScn);
		
		// run RandomNumber Service
		String runRand = "{'action':'run', 'service':{'classname':'random_numbers', 'inputs':{'amount':20}}}";
		runServiceOnce(new LcJSONObject(runRand), "RandomNumber", true);
		printNewLine();
		
//		// send and receive files
//		List<File> missingFiles = check(pic1, pic2);
//		if (missingFiles.size() == 0){
//			int j = 0;
//			String looptest = "{'action':'run', 'service':{'classname':'FileEcho', 'inputs':{'n1':'"+pic1.getAbsolutePath().replace("\\", "\\\\")+"', 'n2':'"+pic2.getAbsolutePath().replace("\\", "\\\\")+"'}}}";
//			System.out.println(looptest);
//			LcJSONObject send = new LcJSONObject(looptest);
//			Map<File,LcInputStream>inputStreams = filenamesToAttachments(send);
//			long starttime = new Date().getTime();
//			long time = 0;
//			int sec = 4;
//			int sent = 0;
//			int recv = 0;
//			float mbs = 0;
//			float mbSent = 0;
//			Set<Float> fileSizes = new HashSet<Float>();
//			for (LcInputStream a: inputStreams.values()){
//				float mb = ((float)(int)((float) a.getLength() / 1024 / 1024 * 100)) / 100;
//				fileSizes.add(mb);
//				mbs += mb;
//			}
//			List<LcOutputStream> outputStreams = LcOutputStream.prepareForTransmission(send);
//			Map<String, LcOutputStream> outputsByChck = new HashMap<String, LcOutputStream>();
//			for (LcOutputStream out: outputStreams) outputsByChck.put(out.getChecksum(), out);
//			while(time < starttime + sec * 1000){
//				if (j > 0){
//					// from the second time onwards
//					List<LcInputStream> inputStreamsOW = new ArrayList<LcInputStream>();
//					for (Entry<File, LcInputStream> en: inputStreams.entrySet()){
//						LcInputStream in = LcInputStream.load(en.getKey(), en.getValue().getChecksum(), null, null);
//						LcOutputStream out = outputsByChck.get(in.getChecksum());
//						out.reset();
//						in.setReady();
//						pool.submit(new DirectStreamWriter(in, out, true, false, false, false));
//					}
//				}
//				final CountDownLatch cdl = new CountDownLatch(1);
//				final LuciResponse[] container = new LuciResponse[1];
//				onReceive(new IReceive(){
//					@Override
//					public boolean receive(LuciResponse r) {
//						r.receiveAttachments();
//						container[0] = r;
//						cdl.countDown();
//						return true;
//					}});
//				System.out.println(outputStreams);
//				lcsocket.send(send, outputStreams);
//				System.out.println("send");
//				cdl.await();
//				LuciResponse r = container[0];
//				synchronized(r){
//					if (r.isError) {
//						System.out.println(r);
//						break;
//					}
//					if (r.inputStreams.size() != outputStreams.size()){
//						System.out.println(LuciResponse.Error(
//								String.format("Sent %d files, received %d files!", r.inputStreams.size(), 
//										outputStreams.size()), 0));
//						break;
//					}
//				}
//				
//				j++;
//				time = new Date().getTime();
//				mbSent += mbs;
////				Thread.sleep(12);
//			}
//			if (isColorSupported) out.print(blue);
//			out.println("Sent and received ("+mbSent+"MB each) "+j+" times "+inputStreams.size()+" files ("+fileSizes+" MB) in "+sec+" seconds.");
//			if (isColorSupported) out.println(normal+resetColor);
//			else out.println();
//			out.flush();
//		} else print(LuciResponse.Error("Could not perform send/receive files in 4s test; missing files: "+missingFiles, 0));
	}
	
	private List<File> check(File ... files){
		List<File> notExisting = new ArrayList<File>();
		for (File f: files) if (!f.exists()) notExisting.add(f);
		return notExisting;
	}
	
	private int createAndGetScenario(LcJSONObject send, int id, String type, String info) 
			throws JSONException, IOException, NoSuchAlgorithmException, InterruptedException {
		return createAndGetScenario(send, id, type, info, null);
	}
	
	private int createAndGetScenario(LcJSONObject send, int id, String type, String info, String getFormat) 
			throws JSONException, IOException, NoSuchAlgorithmException, InterruptedException {
		filenamesToAttachments(send);
		LuciResponse r = sendAndReceive(send);
		print(r, String.format("create %s scenario %d, %s --- %.3fms --- %s --- %s", 
				type, id, info, getCallDurationInMilliSeconds(), r.getErrors(), (r.isError()) ? send.toString() : ""));
		if (!r.isError()){
			int ID = r.json.getJSONObject("result").getInt("ScID");
			r = sendAndReceive(String.format("{'action':'get_scenario','ScID':%d}", ID));
			print(r, String.format("get scenario %d --- %.3fms", ID, getCallDurationInMilliSeconds()));
			printNewLine();
			return ID;
		}
		printNewLine();
		return 0;
	}
	
	private void deleteScenarios(ArrayList<Integer> ScIDs) throws IOException, InterruptedException {
		LcJSONObject send;
		for(int ScID:ScIDs){
			if(ScID!=0){
				send = new LcJSONObject("{'action':'delete','ScID':"+Long.toString(ScID)+"}");
				LuciResponse r = sendAndReceive(send);
				print(r, String.format("delete scenario %d", ScID));
			}
		}
		send = new LcJSONObject("{'action':'delete','trash':true}");
		sendAndReceive(send);
		printNewLine();
		out.flush();
	}
	
	private void runServiceOnce(LcJSONObject jsonCommand, String name, boolean print) 
			throws IOException, JSONException, NoSuchAlgorithmException, InterruptedException{
		if (print && DEBUG) out.println(blue+jsonCommand);
		LuciResponse r = sendAndReceive(jsonCommand);
		if (print) {
			print(r, String.format("run %s --- %.3fms --- %s --- %s", 
				name, getCallDurationInMilliSeconds(), r.getErrors(), (r.isError()) ? jsonCommand : ""));
			printNewLine();
		}
	}
	
	private void printNewLine() {
		if (isColorSupported) out.println(normal+resetColor);
		else out.println();
	}
	
	@Override
	public void run(){
		try {
			runTest();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e){
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e){
			e.printStackTrace();
		} catch (InterruptedException e){
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String OSNAME = System.getProperty("os.name").toLowerCase();

		if (!(OSNAME.contains("mac") || OSNAME.contains("win"))) {
			out.println("End Test Run; Thread will sleep for 60s."+normal+resetColor);
			out.flush();
			try {
				Thread.sleep(60000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		System.exit(0);
	}
	
	public static void main(String[] args) throws IOException {
		new Thread(new TestConsole(), "TestConsole").start();
	}
}
