# Created: JUNE 2014
# Author: Lukas Treyer
# License: MIT

import bpy
from .login import *
from .handlers import *
from bpy.props import PointerProperty
from .mosquitto import Mosquitto

bl_info = {
    "name": "Luci Connector",
    "author": "Lukas Treyer, Dani Zünd",
    "version": (0, 1, 0),
    "blender": (2, 7, 1),
    "location": "Info Panel & Node Editor",
    "description": "Login and upload to / download from Luci and register to its events",
    "wiki_url": "https://bitbucket.org/treyerl/luci",
    "tracker_url": "https://bitbucket.org/treyerl/luci/issues?status=new&status=open",
    "category": "System",
    }

original_header = bpy.types.INFO_HT_header

def register():
    bpy.utils.register_class(LC_luci_settings)
    bpy.types.World.luci = PointerProperty(type=LC_luci_settings)
    bpy.utils.register_class(LC_upload)
    bpy.utils.register_class(LC_download)
    bpy.utils.register_class(LC_login)
    bpy.utils.register_class(LC_disconnect)
    bpy.utils.unregister_class(bpy.types.INFO_HT_header)
    bpy.utils.register_class(INFO_HT_headerWithLogin)

    bpy.app.handlers.scene_update_pre.append(BL_notify)

def unregister():
    bpy.app.handlers.scene_update_pre.remove(BL_notify)
    bpy.utils.unregister_class(INFO_HT_headerWithLogin)
    bpy.utils.register_class(original_header)


if __name__ == "__main__":
    register()


