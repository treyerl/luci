# Created: JULY 2014
# Author: Lukas Treyer
# License: MIT

import bpy, time, json
from .geojson import object2features, features2FeatureCollection
from bpy.app.handlers import persistent

DEBUG = True

opcount = None
obcount = None
scene_name = None
last_active_id = None


def isError(dict, action):
    if "error" in dict.keys():
        try:
            print(action + ": " + dict['error']['message'])
        except:
            print(action + ": " + str(dict['error']))
        return True
    return False


def addobject(scn, obj):
    if 'scid' in scn:
        lc = scn.world.luci
        g = features2FeatureCollection(object2features(obj, scn, with_id=False))
        request = {"action": "update_scenario", "ScID": scn['scid'], "geometry": {"format": "GeoJSON", "value": g}}
        answer = lc.sendAndReceive(request)
        if not isError(answer, "update_scenario"):
            obj['timestamp'] = str(answer['result']['timestamp'])
            obj['id'] = answer['result']['new_ids'][0]
            scn['just_updated'] = True
            if DEBUG: print(answer)


def updatedobject(scn, obj):
    if 'scid' in scn:
        if scn.world is not None:
            lc = scn.world.luci
            g = features2FeatureCollection(object2features(obj, scn, with_id=True))
            request = {"action": "update_scenario", "ScID": scn['scid'], "geometry": {"format": "GeoJSON", "value": g}}
            answer = lc.sendAndReceive(request)
            if not isError(answer, "update_scenario"):
                obj['timestamp'] = str(answer['result']['timestamp'])
                scn['just_updated'] = True
                if DEBUG: print(answer)


def newscenarioname(scn):
    lc = scn.world.luci
    if 'scid' in scn:
        request = {"action": "update_scenario", "ScID": scn['scid'], "name": scn.name}
        answer = lc.sendAndReceive(request)
        if not isError(answer, "update_scenario") and DEBUG:
            print(answer)

def deleteobject(scn, id):
    if 'scid' in scn:
        lc = scn.world.luci
        g = {"type": "FeatureCollection", "features": [{"type": "Feature", "properties": {"delete_list": [id]}}]}
        request = {"action": "update_scenario", "ScID": scn['scid'], "geometry": {"format": "GeoJSON", "value": g}}
        answer = lc.sendAndReceive(request)
        if not isError(answer, "update_scenario") and DEBUG:
            scn['just_updated'] = True
            print(answer)

@persistent
def BL_notify(scene):
    global opcount, obcount, scene_name, last_active_id
    if scene.world is not None:
        lc = scene.world.luci
    else:
        return
    o = scene.objects.active
    timestamp =str(int(time.time() * 1000)) # --> milliseconds

    if any((var is None for var in [opcount, obcount, scene_name, last_active_id])):
        opcount = len(bpy.context.window_manager.operators)
        obcount = len(bpy.context.scene.objects)
        scene_name = bpy.context.scene.name
        if o is not None and 'id' in o:
            last_active_id = o['id']
        return

    # operators
    c = len(bpy.context.window_manager.operators) 
    if c != opcount:
        if c > opcount:
            opname = bpy.context.window_manager.operators[-1].bl_idname
            # if UPDATE
            if o is not None and 'TRANSFORM' in opname:
                if DEBUG: print("update")
                if lc is not None and lc.is_auto_sync:
                    updatedobject(scene, o)
                else:
                    o['timestamp'] = timestamp
            # if DELETE
            if o is None and opname == "OBJECT_OT_delete":
                if last_active_id != 0:
                    if lc is not None and lc.is_auto_sync:
                        if DEBUG: print("delete")
                        deleteobject(scene, last_active_id)
                    else:
                        print("delete_list append")
                        lc.delete_list.add(last_active_id)
                print(last_active_id)

        opcount = c
    
    # if NEW OBJECT
    if obcount < len(scene.objects):
        if lc is not None and lc.is_auto_sync:
            if DEBUG: print("add object")
            addobject(scene, o)
            obcount = len(scene.objects)
    
    # if NEW SCENE NAME
    if scene_name != scene.name:
        if DEBUG: print("scene name changed to %s" % scene.name)
        if lc is not None and lc.is_auto_sync:
            if DEBUG: print("new scene name")
            newscenarioname(scene)
        else:
            lc.new_sc_name = scene.name
        scene_name = scene.name
    
    last_active_object = o