# Created: JULY 2014
# Author: Lukas Treyer
# License: MIT

import bpy, json, bmesh
#from . import mosquitto
from .geojson import mesh2geojson, curve2geojson, feature, geojson_2_bl_geometry, object2features, features2FeatureCollection
from bpy.types import Header, Menu, INFO_MT_editor_menus, Operator
from bpy.props import StringProperty, PointerProperty, BoolProperty
from .luciConnect import LuciClient
from .handlers import isError
from .mosquitto import Mosquitto
from mathutils import Matrix

is_auth = False

class LuciBlenderClient(LuciClient):

    def __init__(self, mqtt_prefix):
        super(LuciBlenderClient, self).__init__()
        self.mqtt = Mosquitto()
        self.mqtt.on_message = self.__on_message
        self.prefix = mqtt_prefix

    def __on_message(self, mosq, obj, msg):
        scn = bpy.context.scene
        t = msg.topic
        m = msg.payload
        lc = scn.world.luci

        print("LC_BLENDER")

        if 'just_updated' in scn:
            if scn['just_updated'] == True:
                scn['just_updated'] = False
                return

        if 'scid' in scn and 'timestamp' in scn:
            pref = self.prefix if self.prefix.endswith("/") else self.prefix + "/"
            if t == pref + str(scn['scid']):
                request = {'action': 'get_scenario', 'ScID': scn['scid']}
                request['versionSelection'] = {'timerange': {'newerThan': scn['timestamp']}}
                answer = lc.sendAndReceive(request)
                if not isError(answer, "get_scenario"):
                    import_scenario(answer, scn)

def collect(j):
    t = j['type']
    if t == "FeatureCollection":
        return j['features']
    if t == "GeometryCollection":
        return j['geometries']
    else:
        return [j]

def replace_objects(json, scn):
    import_scenario(json, scn)

def import_scenario(json, scn):
    update = json['result']
    scn['timestamp'] = update['timestamp']
    scn['hashcode'] = update['hashcode']
    scn['scid'] = update['ScID']
    scno = scn.objects

    for feature in collect(update['GeoJSON']):
        try:
            p = feature['properties']
            layer = None
            if 'layer' in p:
                layer = p['layer']
            if 'id' in p:
                ID = p['id']
                name = str(ID)
                d = geojson_2_bl_geometry(feature['geometry'], name)
                if name in scno:
                    print("*")
                    o = scno[name]
                    o['timestamp'] = str(p['timestamp'])
                    #o.matrix_world = Matrix()
                    o.data = d
                    if layer is not None:
                        swaplayer(o, layer)
                else:
                    print("+")
                    o = bpy.data.objects.new(name, d)
                    o['id'] = ID
                    o['timestamp'] = str(p['timestamp'])
                    if layer is not None:
                        swaplayer(o, layer)
                    scno.link(o)
            elif 'delete_list' in p:
                for item in p['delete_list']:
                    if item in scno:
                        scno.unlink(item)

        except Exception as e:
            print(e)
            d = geojson_2_bl_geometry(feature['geometry'], "geom")
            o = bpy.data.objects.new("geom", d)
            scno.link(o)


def getGroup(name):
    if name in bpy.data.groups:
        return bpy.data.groups[name]
    else:
        return bpy.data.groups.new(name)

def swaplayer(o, layer):
    for group in o.users_group:
        group.objects.unlink(o)
    group = getGroup(layer)
    group.objects.link(o)

class ErrorCheckedOperator(Operator):
    def isError(operator, dict, action):
        if "error" in dict.keys():
            try:
                operator.report({'ERROR'}, action + ": " + dict['error']['message'])
            except:
                operator.report({'ERROR'}, action + ": " + str(dict['error']))
            return True
        return False

class LC_luci_settings(bpy.types.PropertyGroup):
    bl_idname = "luci.settings"
    bl_label = "Luci Settings"

    def turn_on_sync(self, context):
        bpy.ops.luci.upload()

    server = StringProperty(
        name="Server IP: Port No",
        description="IP address and port number of Luci server (default=localhost)",
        maxlen=128,
        default="localhost:7654")

    username = StringProperty(
        name="User name",
        description="Username to log into Luci",
        maxlen=128,
        default="username")

    is_auto_sync = BoolProperty(
        name="AutoSync",
        description="Automatically sync scenes to Luci",
        default=False,
        update=turn_on_sync
        )

    luci = LuciBlenderClient("/Luci/")
    #luci = LuciClient()
    #luci.subscribe("/Luci/#")

    delete_list = set([])
    new_sc_name = None

    def connect(self):
        try:
            host, port = self.server.split(":")
        except:
            host = self.server
            port = 7654
        self.luci.connect(host, int(port))

    def sendAndReceive(self, d):
        global is_auth
        try:
            return self.luci.sendAndReceive(d)
        except ConnectionResetError:
            is_auth = False
            return '{"error": "No connection to Luci!"}'

    def authenticate(self, password):
        global is_auth
        success, answer = self.luci.authenticate(self.username, password)
        is_auth = success
        return success, answer

class LC_disconnect(Operator):
    bl_idname = "luci.disconnect"
    bl_label = "Disconnect from Luci"

    def execute(self, context):
        global is_auth
        lc = context.scene.world.luci
        lc.luci.disconnect()
        is_auth = False
        return {'FINISHED'}

class LC_login(Operator):
    bl_idname = "luci.login"
    bl_label = "Login"

    password = StringProperty()

    def draw(self, context):
        layout = self.layout
        row = layout.row()

        #row.label("password: ")
        row.prop(self, "password")
        #row.operator("wm.luci_login")

    def execute(self, context):
        lc = context.scene.world.luci
        try:
            lc.connect()
        except:
            self.report({'ERROR'}, "Connection to Luci failed!")

        success, answer = lc.authenticate(self.password)
        if success:
            self.report({'INFO'}, "Luci: Authentication successful.")
        else:
            self.report({'ERROR'}, str(answer['error']))
        return {'FINISHED'}

    def invoke(self, context, event):
        wm = context.window_manager
        return wm.invoke_props_dialog(self)


class LC_download(ErrorCheckedOperator):
    bl_idname = "luci.download"
    bl_label = "Download"

    def execute(self, context):
        scn = context.scene
        lc = context.scene.world.luci
        request = {'action': 'exists', 'scenarioname': scn.name}
        answer = lc.sendAndReceive(request)
        if not self.isError(answer, "exists"):
            if answer['result'] == 1:
                if 'scid' in scn.keys():
                    request = {'action': 'get_scenario', 'ScID': scn['scid']}
                else:
                    request = {'action': 'get_scenario', 'scenarioname': scn.name}

                if 'timestamp' in scn.keys():
                    request['versionSelection'] = {'timerange': {'newerThan': scn['timestamp']}}

                answer = lc.sendAndReceive(request)
                if not self.isError(answer, "get_scenario"):
                    import_scenario(answer, scn)

            if answer['result'] == 0:
                self.report({'WARNING', 'INFO'}, "Scenario " + context.scene.name + " does not exist. "
                                                                                    "Create it with upload!")
        return {'FINISHED'}

class LC_upload(ErrorCheckedOperator):
    bl_idname = "luci.upload"
    bl_label = "Upload"

    def execute(self, context):
        global new_sc_name
        lc = context.scene.world.luci
        scn = context.scene
        scno = context.scene.objects
        exists = False

        def getUncommitedGeometry(new_objects):
            features = []
            for o in scno:
                print(o.name)
                if o.hide == True:
                    continue
                if 'timestamp' in o and 'timestamp' in scn:
                    t = int(scn['timestamp'])
                    if int(o['timestamp']) > t:
                        features.extend(object2features(o, scn))
                else:
                    if 'timestamp' in o: del o['timestamp']
                    if 'timestamp' in scn: del scn['timestamp']
                    new_objects.append(o)
                    features.extend(object2features(o, scn, False))

            if lc.delete_list is not None and len(lc.delete_list) > 0:
                features.append(feature(None, delete_list=lc.delete_list))

            if len(features) > 0:
                return {'format': 'GeoJSON', 'geometry': features2FeatureCollection(features)}
            else:
                return None

        if 'scid' in scn:
            request = {'action': 'exists', 'ScID': scn['scid']}
            answer = lc.sendAndReceive(request)
            if not self.isError(answer, "exists"):
                if answer['result'] == 1:
                    exists = True
                    request = {'action': 'get_infos_about', 'ScID': scn['scid']}
                    answer = lc.sendAndReceive(request)
                    if not self.isError(answer, "get_infos_about"):
                        scn.name = answer['result']['name']
                        if lc.new_sc_name is not None:
                            request = {'action': 'update_scenario', 'ScID': scn['scid'], 'name': new_sc_name}
                        else:
                            request = {'action': 'update_scenario', 'ScID': scn['scid']}
                if answer['result'] == 0:
                    request = {'action': 'create_scenario', 'name': scn.name}
        else:
            request = {'action': 'exists', 'scenarioname': context.scene.name}
            answer = lc.sendAndReceive(request)
            if not self.isError(answer, "exists"):
                if answer['result'] == 1:
                    exists = True
                    request = {'action': 'get_infos_about', 'scenarioname': scn.name}
                    answer = lc.sendAndReceive(request)
                    if not self.isError(answer, "get_infos_about"):
                        scid = answer['result']['ScID']
                        scn['scid'] = scid
                        request = {'action': 'update_scenario', 'ScID': scid}
                if answer['result'] == 0:
                    request = {'action': 'create_scenario', 'name': scn.name}

        new_objects = []
        geometry = getUncommitedGeometry(new_objects)
        if geometry is not None:
            request['geometry'] = {'Scene': geometry}
            json.dump(geometry['geometry'], open("json_export.json", 'w'), indent=3)
        answer = lc.sendAndReceive(request)
        if not self.isError(answer, request['action']):
            print(answer)
            r = answer['result']
            t = str(r['timestamp'])
            if 'new_ids' in r:
                for o, ID in zip(new_objects, r['new_ids']):
                    o['id'] = int(ID)
                    o.name = str(ID)
                    o['timestamp'] = t
            scn['timestamp'] = t
            scn['scid'] = r['ScID']
            message = "Scenario '%s'(% has been %s " % (r['name'], "updated" if exists else "created")
            if 'errors' in r:
                message += " with errors!"
                self.report({'WARNING'}, message)
            else:
                message += " successfully!"
                self.report({'INFO'}, message)

        lc.new_sc_name = None
        return {'FINISHED'}

class INFO_HT_headerWithLogin(Header):
    bl_space_type = 'INFO'

    def draw(self, context):
        layout = self.layout

        window = context.window
        scene = context.scene
        rd = scene.render
        if scene.world is not None:
            lc = context.scene.world.luci

        row = layout.row(align=True)
        row.template_header()

        INFO_MT_editor_menus.draw_collapsible(context, layout)

        if window.screen.show_fullscreen:
            layout.operator("screen.back_to_previous", icon='SCREEN_BACK', text="Back to Previous")
            layout.separator()
        else:
            layout.template_ID(context.window, "screen", new="screen.new", unlink="screen.delete")
            layout.template_ID(context.screen, "scene", new="scene.new", unlink="scene.delete")

        layout.separator()

        if rd.has_multiple_engines:
            layout.prop(rd, "engine", text="")

        layout.separator()

        layout.template_running_jobs()

        layout.template_reports_banner()

        row = layout.row(align=True)

        if bpy.app.autoexec_fail is True and bpy.app.autoexec_fail_quiet is False:
            layout.operator_context = 'EXEC_DEFAULT'
            row.label("Auto-run disabled: %s" % bpy.app.autoexec_fail_message, icon='ERROR')
            if bpy.data.is_saved:
                props = row.operator("wm.open_mainfile", icon='SCREEN_BACK', text="Reload Trusted")
                props.filepath = bpy.data.filepath
                props.use_scripts = True

            row.operator("script.autoexec_warn_clear", text="Ignore")
            return

        if scene.world is not None:
            if is_auth:
                row.prop(lc, "server", text="", icon="WORLD")
                row.operator("luci.disconnect", text="", icon="CANCEL")
                row = layout.row(align=True)
                row.prop(lc, "is_auto_sync", text="", icon="FILE_REFRESH")
                row = layout.row(align=True)
                if lc.is_auto_sync:
                    row.enabled = False
                row.operator("luci.download", text="", icon="TRIA_DOWN")
                row.operator("luci.upload", text="", icon="TRIA_UP")
            else:
                row.prop(lc, "server", text="", icon="WORLD")
                row.prop(lc, "username", text="", icon="MESH_MONKEY")
                row.operator("luci.login")

        row = layout.row(align=True)
        row.operator("wm.splash", text="", icon='BLENDER', emboss=False)
        row.label(text=scene.statistics(), translate=False)
