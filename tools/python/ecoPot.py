#!/usr/bin/python

""" 
Implements a spatial analysis, for where people would spend their money. 

It needs the following libraries: Numpy, CSV, socket, json

#######
The input json structure should have a similar structure than the following
one, and all the inputs:
                 ======

{'action':'run', 'service':{ 
    'classname':'ecoPot', 
    'inputs':{ 
        'potentialStrength': {'format':'number', 'value':'1'}, 
        'resolution':{'format':'number', 'value':'2'}, 
        'residents':{'format':'csv', 'value':'103.8;1.4;4\n 103.9;1.3;4\n 103.9;1.3;4\n 103.9;1.3;4\n'}, 
        'workers':{'format':'csv', 'value':'103.8;1.2;4\n 103.7;1.4;4\n 103.9;1.3;4\n 103.8;1.3;4\n'}, 
        }, 
    }, 
}

travelCost ... from;to;cost
residents ... xLocation;yLocation;amount
workers ... xLocation;yLocation;amount
potentialStrength ... elasticity of travelcost for residents
resolution ... parameter for the n x n grid (with n = resolution, in the case
    above it is a 2x2 grid)
#######

#######
The output json looks like the following:
    ======

{"result": {
    "outputs": {
        "minMaxXCoords": {"value":"(103.778736529708, 103.94350617555502)", "format":"tuple"},
        "potentialStrength": {"value":"1.0", "format":"number"},
        "minMaxYCoords": {"value":"(1.27756801679165, 1.4444124040115602)", "format":"tuple"},
        "resolution": {"value":"2.0", "format":"number"},
        "potential": {"value":"[[1.3e+00   0.2e+00] [3.2e+00   2.0e+00]]", "format":"matrix"},
    },
    "hashcode_inputs": "0D 4D 86 7B 0E B7 F6 B6 16 82 A8 C0 ED 91 99 2F"
    "created": 28531102,
    "mqtt": "null0/83/",
    "name": "ecoPot",
    "status": "DONE"
}}

minMaxXCoords ... left and right coordinates of the grid
minMaxYCoords ... top and bottom coordinates of the grid
resolution ... resolution of the grid
potentialStrength ... elasticity of travelcost for residents
potential ... how many people do the regions attract for spending
hashes, created, mqtt, name, status ... see Luci doc
#######

"""


import csv
import numpy as np
#import matplotlib.pylab as pl
import sys
#import StringIO
sys.setcheckinterval(100000)

import luciConnect as lc

# read input
# TODO convert the byte files to string?
def jsonInputReader(string):
    res = [s.split(';') for s in string.split()]
    result = []
    for r in res:
        result.append([float(e) for e in r])
    return result

def readCSV(csvdata, encoding="UTF-8"):
    #f = StringIO.StringIO(csvfile)
    #f = csvdata.decode(encoding=encoding)
    try:
        import StringIO
        f = StringIO.StringIO(csvdata.decode(encoding=encoding))
    except:
        import io
        f = io.StringIO(csvdata.decode(encoding=encoding))
    reader = csv.reader(f, delimiter=';')
    return [[float(e) for e in row] for row in reader]

def readResidents(con):
    raw = con.getInput('residents')
    residents = readCSV(raw)
    return residents

def readWorkers(con):
    raw = con.getInput('workers')
    workers = readCSV(raw)
    return workers

def readTravelCost(con):
    raw = con.getInput('travelCost')
    travelCost = readCSV(raw)
    return travelCost

def readVariance(con):
    return float(con.getInput('potentialStrength'))

def readResolution(con):
    return float(con.getInput('resolution'))

# helpers
def getMinMax(data, dim):
    minD = np.finfo(data[0][0]).max
    maxD = np.finfo(data[0][0]).min

    for e in data:
        if minD > e[dim]:
            minD = e[dim]
        if maxD < e[dim]:
            maxD = e[dim]
    #print('Dim: ' + str(dim) + ' | Min: ' + str(minD) + ' | Max: ' + str(maxD))
    return [minD, np.nextafter(maxD, maxD+1)]

# def testPlot(areas, potentials):
#   a = []
#   b = []
#   v = []
#   vf = []
#   for key, item in areas.items():
#       a.append(item.llx)
#       b.append(item.lly)
#       v.append(item.residents)
#       vf.append(np.log(item.workers))
#
#   potentials = np.rot90(potentials)
#   potentials /= np.max(potentials)
#   potentials[potentials == 0] = np.nan
#   f = pl.figure(1)
#
#   ax1 = f.add_subplot(131)
#   ax1.set_title('Residents')
#   cax = ax1.scatter(a, b, c=v, s=50, alpha=0.5)
#   f.colorbar(cax,  orientation='horizontal')
#
#   ax2 = f.add_subplot(132)
#   ax2.set_title('Potentials')
#   cax = ax2.imshow(potentials, interpolation='nearest')
#   f.colorbar(cax,  orientation='horizontal')
#
#   ax3 = f.add_subplot(133)
#   ax3.set_title('Employees')
#   cax = ax3.scatter(a, b, c=vf, s=50, alpha=0.5)
#   f.colorbar(cax,  orientation='horizontal')
#
#   pl.show()


# area Class
class Area:
    def __init__(self, x, y, resolution, indX, indY):
        self.llx = x
        self.lly = y
        self.indX = indX
        self.indY = indY
        self.residents = 0
        self.workers = 0
        self.travelCost = np.zeros((resolution,resolution))
        self.influences = []
        print(indX, indY)

    def addResidents(self, inResi):
        self.residents = max(0, self.residents + inResi)

    def addWorkers(self, inWork):
        self.workers = max(0, self.workers + inWork)

    def calculateTravelcost(self):
        for i in range(self.travelCost.shape[0]):
            for j in range(self.travelCost.shape[1]):
                self.travelCost[i, j] = np.linalg.norm(np.array([float(self.indX), float(self.indY)]) -
                        np.array([float(i), float(j)]))

    def calculateInfluences(self, workers):
        self.influences = np.log(np.array(workers)) * self.gaussian(self.travelCost, variance)

    def gaussian(self, x, sigma, mu=0.0):
        return 1./(np.sqrt(2*np.pi)*sigma)*np.exp(-0.5 * (1./sigma*(x - mu))**2)

    def getLocalPotentials(self):
        return self.influences * self.residents
        


"""
 main loop
"""
# setup connection
inputs = {'residents':'csv' , 'workers':'csv', 'potentialStrength':'number'
        , 'resolution':'number'}

outputs = {'potential':'list', 'minMaxXCoords':'list' , 'minMaxYCoords':'list'
        , 'resolution':'number' , 'potentialStrength':'number'}

con = lc.LuciRemoteService('ecoPot', inputs, outputs, 'PC' , 'calculates the economic potential')
#print con.connect('129.132.6.35',7654)
print(con.connect('localhost', 7654))
print(con.authenticate('lukas', '1234'))
print(con.register())


while 1:

    con.waitForTask()
    print('(: got one :)')
    resolution = readResolution(con)
    variance = readVariance(con)


# setup residents and workers
    residents = readResidents(con)
    workers = readWorkers(con)
    minMaxX = getMinMax(residents + workers, 0)
    minMaxY = getMinMax(residents + workers, 1)

    xCoords = np.linspace(minMaxX[0], minMaxX[1], resolution)
    yCoords = np.linspace(minMaxY[0], minMaxY[1], resolution)

    areas = {}

    multX = (resolution)/(minMaxX[1]-minMaxX[0])
    multY = (resolution)/(minMaxY[1]-minMaxY[0])
    for r in residents:
        indX = (r[0]-minMaxX[0])*multX
        indY = (r[1]-minMaxY[0])*multY

        llx = xCoords[indX]
        lly = yCoords[indY]

        key = (llx, lly)
        if not key in areas:
            areas[key] = Area(llx, lly, resolution, indX, indY)

        areas[key].addResidents(r[-1])

    workersMat = np.zeros((resolution,resolution)).tolist()
    for w in workers:
        indX = np.floor((w[0]-minMaxX[0])*multX)
        indY = np.floor((w[1]-minMaxY[0])*multY)

        llx = xCoords[indX]
        lly = yCoords[indY]

        key = (llx, lly)
        if not key in areas:
            areas[key] = Area(llx, lly, resolution, indX, indY)

        areas[key].addWorkers(w[-1])
        workersMat[int(indX)][int(indY)] += w[-1]

    workersMat = np.array(workersMat)

# set travel costs
    for key in areas:
        areas[key].calculateTravelcost()

# Calculate the potentials
    potentials = np.zeros((resolution, resolution))
    for a in areas.values():
        a.calculateInfluences(workersMat)
        currPot = a.getLocalPotentials()
        currPot[np.isnan(currPot)] = 0
        potentials += currPot

#    np.set_printoptions(threshold=np.nan)
    potentials[potentials < 0] = -1
    print(potentials)
    con.sendOutputs({
        'potential': potentials.tolist(),
        'minMaxXCoords': minMaxX,
        'minMaxYCoords': minMaxY,
        'resolution': resolution,
        'potentialStrength': variance
    })
    #testPlot(areas, potentials)
