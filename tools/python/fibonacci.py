import luciConnect as lc

inputs = {'amount': 'number'}

outputs = {'fibonacci_sequence': 'list'}

connection = lc.LuciRemoteService('fibonacci_remote_service', inputs, outputs, 'PC', 'gives out the fibonacci sequence')
print(connection.connect('localhost', 7654))
print(connection.authenticate('lukas', '1234'))
print(connection.register())

while 1:
	connection.waitForTask()
	print('got a task')
	
	amount = connection.getInput('amount')
	fibonacci_sequence = [0,1]
	for i in range(2, amount):
		fibonacci_sequence.append((fibonacci_sequence[i-1]+fibonacci_sequence[i-2]))

	connection.sendOutputs({'fibonacci_sequence': fibonacci_sequence})
	
#{'action':'run', 'service':{'classname':'fibonacci_remote_service', 'inputs':{'amount':30}}}
