import json
import sys

filename = sys.argv[0]

json.dump(json.load(open(filename, 'r')), open(filename.replace(".json", "_nice.json"), 'w'), indent=3)