import luciConnect as lC
import socket
from subprocess import call
from os.path import exists

# example call
example = "{'action':'run', 'service':{'classname':'somoclu', 'inputs':{'vectorFile':'/Users/ia/Developer/bitbucket/luci/clients/python/somoclu_test/SOM_31-01_232712.lrn'}}}"

# setup connection
inputs = {'vectorFile': 'streaminfo', 'OPT numThreads': 'number', 'OPT codebook': 'streaminfo',
          'OPT maxEpochs': 'number', 'OPT kernelType': 'number', 'OPT mapType': ['planar', 'toroid'],
          'OPT coolingStrategy': ['linear', 'exponential'], 'OPT startRadius': 'number', 'OPT endRadius': 'number',
          'OPT learningStrategy': ['linear', 'exponential'], 'OPT startLearningRate': 'number',
          'OPT endLearningRate': 'number', 'OPT saveInterimFiles': 'number', 'OPT columns': 'number',
          'OPT rows': 'number'}

outputs = {'map': 'streaminfo', 'OPT umatrix': 'streaminfo', 'OPT codebook': 'streaminfo'}

con = lC.LuciRemoteService('somoclu', inputs, outputs, socket.gethostname(),
                           'service accessing a local somoclu installation via console')

print(con.connect('localhost', 7654))
print(con.authenticate('lukas', '1234'))
print(con.register())

hasMpi = False

try:
    call(["mpi"])
    hasMpi = True
except FileNotFoundError:
    print("mpi not installed")

while 1:
    con.waitForTask()
    task_inputs = con.getInputs()

    args = []

    if hasMpi and 'numThreads' in task_inputs:
        args.append("mpirun")
        args.append( "-np")
        args.append(task_inputs['numThreads'])

    args.append("somoclu")

    if "codebook" in task_inputs:
        args.append("-c")
        args.append("codebook")
        with open("codebook", "wb") as cb:
            cb.write(task_inputs['codebook'])
    if "maxEpochs" in task_inputs:
        args.append("-e")
        args.append(task_inputs["maxEpochs"])
    if "kernelType" in task_inputs:
        args.append("-k")
        args.append(task_inputs["kernelType"])
    if "mapType" in task_inputs:
        args.append("-m")
        args.append(task_inputs["mapType"])
    if "coolingStrategy" in task_inputs:
        args.append("-t")
        args.append(task_inputs["coolingStrategy"])
    if "startRadius" in task_inputs:
        args.append("-r")
        args.append(task_inputs["startRadius"])
    if "endRadius" in task_inputs:
        args.append("-R")
        args.append(task_inputs["endRadius"])
    if "learningStrategy" in task_inputs:
        args.append("-T")
        args.append(task_inputs["learningStrategy"])
    if "startLearningRate" in task_inputs:
        args.append("-l")
        args.append(task_inputs["startLearningRate"])
    if "endLearningRate" in task_inputs:
        args.append("-L")
        args.append(task_inputs["endLearningRate"])
    if "saveInterimFiles" in task_inputs:
        args.append("-s")
        args.append(task_inputs["saveInterimFiles"])
    if "columns" in task_inputs:
        args.append("-x")
        args.append(task_inputs["columns"])
    if "rows" in task_inputs:
        args.append("-y")
        args.append(task_inputs["rows"])

    args.append("tmp.lrn")
    with open("tmp.lrn", 'wb') as tmp:
        tmp.write(task_inputs["vectorFile"])

    args.append("tmp")

    print(args)

    call(args)

    outputs = {}
    with open("tmp.bm", 'rb') as bm:
        outputs["map"] = bm.read()
    if exists("tmp.umx"):
        with open("tmp.umx", 'rb') as umx:
            outputs["umatrix"] = umx.read()
        with open("tmp.wts", 'rb') as wts:
            outputs["codebook"] = wts.read()

    print(con.sendOutputs(outputs))