<html>
<head>
  <title>iMap: Map what you see!</title>
  <link rel="icon" type="image/png" href="../static/favicon.png">
  <link href="css/common.css" media="all" rel="stylesheet" type="text/css" />
  <link href="css/buttons.less" media="all" rel="stylesheet" type="text/css" />
  <link href="css/vertical.css" media="only screen and (max-device-width: 480px)" rel="stylesheet" type="text/css" />
  <link href="css/horizontal.css" media="only screen and (min-device-width: 480px)" rel="stylesheet" type="text/css" />
  <script type="text/javascript" src="../js/jquery-2.1.3.min.js"></script>
  <script type="text/javascript" src="../js/jquery.csv-0.71.min.js"></script>
  <script type="text/javascript" src="../js/Iterator.js"></script>
  <script type="text/javascript" src="../js/typeahead.bundle.min.js"></script>
<!--   <script type="text/javascript" src="../js/utf8.js"></script> -->
  <script type="text/javascript" src="../js/luciConnect.js"></script>
  <script type="text/javascript" src="../js/IP.jsp"></script>
  <script type="text/javascript" src="../js/faultylabs.md5.js"></script>
  <script type="text/javascript" src="../js/FileSaver.min.js"></script>
  <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB-oV3kNStHtk89erhN884MXqPx47gcUxM"></script>
  <script type="text/javascript" src="js/infobox_packed.js"></script>
  <script type="text/javascript" src="js/typeAhead.js"></script>
  <script type="text/javascript" src="js/map_callbacks.js"></script>
  <script type="text/javascript" src="js/map.js"></script>
  <script type="text/javascript" src="js/buttons.js"></script>
 </head>
<body>
	<div id="main">
		<div id="menu">
			<table id="scale" class="button binaryChoiceButton">
				<tr>
					<th class="selected">Global</th>
					<th>City</th>
				</tr>
			</table>
			<form action="#"><input type="text" id="searchPlace"></form>
			<div id="cityLabel"></div>
			<table id="new" class="button pressbutton">
				<tr>
					<th>New Game</th>
				</tr>
			</table>
			<table id="round_display">
				<tr>
					<th>Round: <span id="round"></span></th>
				</tr>
			</table>
			<table id="mc" class="button switchbutton">
				<tr>
					<th>Multiple Choice</th>
				</tr>
			</table>
			<table id="mode" class="button binaryChoiceButton">
				<tr>
					<th class="selected"><img draggable="false" src="images/mode-01.png"></th>
					<th><img draggable="false" src="images/mode-02.png"></th>
				</tr>
			</table>
			<table style="clear: both;"></table>
		</div>
		<div id="content">
			<div id="image-display">
				<img id="image" alt="" src="images/empty.gif">
			</div>
			<div id="map-canvas"></div>
			<div id="distance"></div>
			<div id="time"></div>
			<div id="debug">
				<table style="font-size:8px; visibility: hidden;">
					<tr>
						<td>round:<span id="round"></span>
						</td>
						<td>search iterations:<span id="searchiterations"></span>
						</td>
						<td>total search iterations:<span id="totaliterations"></span>
						</td>
					</tr>
				</table>
			</div>
			<div id="gap"></div>	
		</div>
		<div id="footer">
			<table id="no_streetview_cities">
			
			</table>
		</div>
	</div>
</body>
</html>
