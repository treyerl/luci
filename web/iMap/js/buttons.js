var binaryToggle = function( event ) {
	$this = $( this );
	$this.addClass( "selected" );
	$this.unbind("click", binaryToggle);
	$this.siblings().each( function(){
		$this = $( this );
		$this.removeClass( "selected" );
		$this.click(binaryToggle);
	})
}

var toggle = function( event ) {
	$this = $( this );
	if ($this.hasClass( "selected" )) $this.removeClass( "selected" );
	else $this.addClass( "selected" );
}

var binaryToggleScale = function( event ){
	$this = $( this );
	var text = $this.text();
	if (text == "Global"){
		state_scale = GLOBAL;
		slideBackImageDisplay();
		hideMarkers(selectionMarkers);
		initialize();
		$("#cityLabel").css("display", "none");
	} else {
		state_scale = CITY;
		city = null;
		setCity();
	}
	$this.addClass( "selected" );
	$this.unbind("click", binaryToggleScale);
	$this.siblings().each( function(){
		$this = $( this );
		$this.removeClass( "selected" );
		$this.click(binaryToggleScale);
	})
	
	
}

$( document ).ready(function(){
	$( "#scale th:not(.selected)" ).click(binaryToggleScale);
	$( "#mode th:not(.selected)" ).click(binaryToggle);
	$( ".pressbutton th" ).mousedown(function(){ $(this).addClass("selected") });
	$( ".pressbutton th" ).mouseup(function(){ $(this).removeClass("selected") });
//	$( ".switchbutton th" ).click(toggle);
	$( "#new" ).click(function(){
		newGame();
		initialize();
	});
//	$( "#mc th" ).unbind("click", toggle);
	$mc = $( "#mc th" );
	$mc.click(function( event ){
		$this = $( this );
		if ($this.hasClass( "selected" )) {
			$this.removeClass( "selected" );
			state_multipleChoice = false;
			hideMarkers(choiceMarkers);
			google.maps.event.addListenerOnce(map, 'click', processWildGuess);
		} else {
			$this.addClass( "selected" );
			state_multipleChoice = true;
			setupMultipleChoice(panorama.getPosition());
			google.maps.event.clearListeners(map, 'click');
		}
	});
	if (state_multipleChoice) 
		if (!$mc.hasClass("selected"))
			$mc.addClass("selected")
});