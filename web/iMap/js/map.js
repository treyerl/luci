var DEBUG = true;

var d = 0.05;
var sv = new google.maps.StreetViewService();
var panorama;
var map;
var city;

var time;
var searchIterations = 0;
var totalsearchIterations = 0;
var cities;
var cities_names;
var cities_dict;
var round = 0;

//multiple choice markers
var correctMarker;
var choiceMarkers = new Array();

//distance markers
var locCircle;
var distanceLabel;
var locPointer;
var guessLoc;

//citySelection markers
var selectionMarkers = new Array();
var is_selecting_city = false;
var is_map_changing = false;

var GLOBAL = 0;
var CITY = 1;
var MACHINE = 2;
var TWOPLAYER = 3;
var MULTIPLECHOICES = 5;

var GLOBAL_ZOOM = 1;
var CITY_ZOOM = 12;

var global_cities;

var state_scale = GLOBAL;
var state_mode = MACHINE;
var state_multipleChoice = true;

var VERTICAL = $(window).width() < 480;
var specialZoom = {
		"Chicago": 10,
		"Los Angeles": 10,
		"Tokyo": 10,
		"Shenzen": 9,
		"Rio de Janeiro": 10,
		"Sao Paolo": 10,
		"Buenos Aires": 10
}

function d_fromZoom(z){
	if (z == 12) return 0.05
	if (z == 11) return 0.1;
	if (z == 10) return 0.4
	if (z == 9) return 0.6;
	if (z == 8) return 0.8;
	return 0.05;
}

function initialize() {
//	document.getElementById("no_streetview_cities").innerHTML = "";
	initMap();
	setCity();
	initStreetView();
}

function setCity(){
	if (state_scale == GLOBAL){
		city = cities[getRandomInt(0, cities.length)];
	} else if (state_scale == CITY){
		if (map != null && city == null){
			slideAwayImageDisplay();
			$("#searchPlace").css("display", "table-row");
			$(".tt-hint").css("display", "table-row");
		}
	}
}

function widthOrHeight(){
	if (VERTICAL){
		return "height";
	} else {
		return "width";
	}
}

function slideAwayImageDisplay(){
	if (!VERTICAL) $("#map-canvas").css("max-width", "1280px");
	$("#empty").css(widthOrHeight(), 0);
	$("#image-display").css(widthOrHeight(), "0%");
//	$("#map-canvas:before").css('padding-top', '50%');
	$('head').append('<style id="hack">#map-canvas:before{padding-top:50% !important;}</style>');
	$("#map-canvas").css(widthOrHeight(), "100%");

	is_selecting_city = true;
	var mapOptions = {
			center: getMapCenter(),
			zoom: 2,
			streetViewControl: false
	};
	map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
	displaySelecteableCities();
}

function slideBackImageDisplay(){
	if (!VERTICAL) $("#map-canvas").css("max-width", "640px");
	$("#empty").css(widthOrHeight(), "50%");
	$("#image-display").css(widthOrHeight(), "50%");
	$("#hack").remove();
	$("#map-canvas").css(widthOrHeight(), "50%");
	$("#searchPlace").css("display", "none");
	$(".tt-hint").css("display", "none");
	newMap();
	initialize();
}

function redisplaySelecteableCities( event ){
	displaySelecteableCities();
	is_map_changing = false;
}

function idleDispatcher( event ) {
	if (!is_map_changing){
		google.maps.event.addListenerOnce(map, 'idle', redisplaySelecteableCities);
		is_map_changing = true;
	}

}

function displaySelecteableCities(){
	if (selectionMarkers.length > 0){
		hideMarkers(selectionMarkers);
	}
	selectionMarkers = new Array();
	var cityPool = cityfilter_bbox();
	while(cityPool.hasNext()){
		var c = cityPool.next();
		var m = getClickableMarker(getLatLng(c), selectCity, map);
		m.city = c;
		selectionMarkers.push(m);
	}
	google.maps.event.addListenerOnce(map, 'zoom_changed', idleDispatcher);
	google.maps.event.addListenerOnce(map, 'center_changed', idleDispatcher);

}

function getMapCenter(){
	if (state_scale == CITY) {
		if (city != null){
			return getLatLng(city);
		}
	}
	// else if global or city == null:
	return {lat: 0, lng: 0};
}

function getMapZoom(){
	if (state_scale == GLOBAL){
		return GLOBAL_ZOOM;
	}
	if (state_scale == CITY){
		if (city != null){
			var zoom = specialZoom[city[0]];
			if (zoom != null) return zoom;
		}
		return CITY_ZOOM;
	}
}
function newMap(){
	round = 0;
	var mapOptions = {
			center: getMapCenter(),
			zoom: getMapZoom(),
			streetViewControl: false
	};
	map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
}
function initMap(){
	searchIterations = 0;

	if (map == null){
		newMap();
	} else {
		map.setZoom(getMapZoom());
		map.setCenter(getMapCenter());
	}

	if (!state_multipleChoice){
		google.maps.event.addListenerOnce(map, 'click', processWildGuess);
		if (locPointer != null) locPointer.setMap(null);
		if (locCircle != null) locCircle.setMap(null);
		if (distanceLabel != null) distanceLabel.setMap(null);
		if (guessLoc != null) guessLoc.setMap(null);
	} else if (choiceMarkers.length > 0){
		hideMarkers(choiceMarkers);
	}
	if (DEBUG){
		document.getElementById("distance").textContent = "";
		document.getElementById("time").textContent = "";
		document.getElementById("round").textContent = "0";
		document.getElementById("searchiterations").textContent = "0";
		$( "debug" ).css("visibility", "visible");
	}
}

function initStreetView(){
	if (city != null) {
		searchIterations = 0;
		round++;
		document.getElementById("round").textContent = round;
		if (round < 10){
			sv.getPanoramaByLocation(randomizedLoc(city), 49, findPanorama);
		} else {
			alert("(the) game (is) over");
		}

	}
}

function cityfilter_pop(){
	var pop = getPopulationAccordingToZoom();
	return new FilterArray(cities, function(city){
		return parseInt(city[2]) > pop;
	});
}

function cityfilter_bbox(){
//	console.log("bbox");
	var citiesAccordingToZoom = cityfilter_pop();
	var SW = map.getBounds().getSouthWest();
	var NE = map.getBounds().getNorthEast();
	var is_overlap = NE.lng() < 0 && SW.lng() > 0 || NE.lng() < 0 && SW.lng() > NE.lng();
	var NE_lng = is_overlap ? NE.lng() + 360 : NE.lng();
//	console.log(NE.lat()+", "+NE.lng()+" | "+ SW.lat()+", "+SW.lng() + " | "+map.getZoom() + " | " + is_overlap + " | " + NE_lng);
	return new FilterIterator(citiesAccordingToZoom, function(city){
		var lat = parseFloat(city[4]);
		var lng = parseFloat(city[3]);
		if (is_overlap && lng < 0) lng += 360; 
		return  lat < NE.lat() && lng < NE_lng && 
		lat > SW.lat() && lng > SW.lng();
	});
}

function load(){
	// TODO: use cookies to store city / state_scale / state_mulitpleChoice / state_mode;
	// atm initialize global scale
}

function create_global_cities_list(){
	if (map.getZoom() != GLOBAL_ZOOM) {
		console.log("WARNING: map zoom not set to GLOBAL_ZOOM for generating list of global cities");
	}
	var gCities = cityfilter_pop();
	global_cities = new Array();
	global_cities_names = new Array();
	global_cities_dict = {};
	while(gCities.hasNext()){
		var c = gCities.next();
		global_cities.push(c);

	}
}

function newGame(){
	round = 0;
	// once implemented also the score counters need to be reset here
}


//utilities

function getClickableMarker(latLng, callback, map){
	var m = new google.maps.Marker({
		position: latLng,
		map: map,
		draggable: false,
	});
	google.maps.event.addListener(m, 'click', callback);
	return m;
}

function getPopulationAccordingToZoom(){
	if (map != null){
		var zoom = map.getZoom();
		if (zoom < 2) return 2000000;
		if (zoom < 4) return 1000000;
		if (zoom < 6) return 500000;
		if (zoom < 8) return 100000;
		return 0;
	}
	return 2000000;
}
function getRandomInt(min, max) {
	return Math.floor(Math.random() * (max - min)) + min;
}

function getRandom(min, max) {
	return Math.random() * (max - min) + min;
}

function getLatLng(city){
	var lt = parseFloat(city[4]);
	var ln = parseFloat(city[3]);
	return {lat: lt, lng: ln};
}

function randomizedLoc(city){
	var lat = parseFloat(city[4]);
	var lon = parseFloat(city[3]);
	var dd = d_fromZoom(getMapZoom());
	return new google.maps.LatLng(getRandom(lat - dd, lat + dd), getRandom(lon - dd, lon + dd));
}

Array.prototype.contains = function ( needle ) {
	for (i in this) {
		if (this[i] == needle) return true;
	}
	return false;
}

//Array.prototype.eventPush = function(obj, handler){
//this.push(obj);
//handler(obj);
//}

//bind to window load event
google.maps.event.addDomListener(window, 'load', function(){
	$.ajax({
		type: "GET",
		url: "data/streetviewCities.csv",
		dataType: "text",
		success: function(data) {
			cities = $.csv.toArrays(data);
			cities_names = new Array();
			cities_dict = {};
			load();
			newGame();
			initialize();
			for (var i = 0; i < cities.length; i++){
				cities_dict[cities[i][0]] = cities[i];
				cities_names.push(cities[i][0]);
			}

			var cityB = new Bloodhound({
				datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
				queryTokenizer: Bloodhound.tokenizers.whitespace,
				local: $.map(cities_names, function(city) { return { value: city }; })
			});

			// kicks off the loading/processing of `local` and `prefetch`
			cityB.initialize();

			$searchPlace = $("#searchPlace");
			$searchPlace.typeahead({
				hint: true,
				highlight: true,
				minLength: 1,
				autoselect: true
			},
			{
				name: 'cities',
				displayKey: 'value',
				source: cityB.ttAdapter()
			});
			$(".tt-hint").css("display", "none");
			$searchPlace.bind("typeahead:autocompleted", function(event, sugg, dataset){
				$(this).typeahead('close');
				processCity(cities_dict[sugg.value]);
			});
			$searchPlace.bind("typeahead:selected", function(event, sugg, dataset){
				$(this).typeahead('close');
				processCity(cities_dict[sugg.value]);
			});
			$searchPlace.submit(function(event){alert("submit")});
			$searchPlace.keypress(function (e) {
				if (e.which == 13) {
					$this = $(this);
					var sugg = $(".tt-hint").val();
					if (sugg != "") $this.val(sugg)
//					console.log($this.val());
					$this.typeahead('close');
					e.preventDefault();
					processCity(cities_dict[$this.val()]);
				}
			});
		}
	});

});

