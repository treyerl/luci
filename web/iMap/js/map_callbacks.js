//var no_imagery_here_md5 = "aa36b23e5d0dbaf23f0d9f9519afcebb";
var no_imagery_here_md5 = "AA36B23E5D0DBAF23F0D9F9519AFCEBB";
var loc;
var image_hash;
var image;
var h;
var heading;
var pitch = 0;
var fov = 90;
var attempts = 1;
var image_displayed_since;
var lc = new LuciClient("localhost", 8080, "ws/", function(){
	lc.authenticate("lukas","1234", function(l){console.log(l.getMessage().result)})
});

//@callback for getPanoramaByLocation()
function findPanorama(data, status){
	if (status == "OK"){
		//google has a street view image for this location, so attach it to the street view div
		var panoramaOptions = {
				pano: data.location.pano,
				addressControl: false,
				navigationControl: false,
				linksControl: false,
				panControl: false,
				scrollWheel: false,
				zoomControl: false,
				disableDefaultUI: true,
				navigationControlOptions: {
					style: google.maps.NavigationControlStyle.SMALL
				},
				pov: {
					heading: 45 * getRandomInt(0, 7),
					pitch: 0
				}
		};

		loc = data.location.latLng;
		heading = panoramaOptions.pov.heading;

		// TODO: replace streetview with image
		// http://maps.googleapis.com/maps/api/streetview?size=640x640&location=40.720032,%20-73.988354&fov=90&heading=235&pitch=10&sensor=false
		var Url = "http://maps.googleapis.com/maps/api/streetview?size=640x640&location=";
		var locString = + loc.lat() + ",%20" + loc.lng();
		var params = "&fov="+fov+"&heading="+heading+"&pitch="+pitch+"&sensor=false";
		var xhr = new XMLHttpRequest();
		xhr.onreadystatechange = function(){
			if (this.readyState == 4 && this.status == 200){
				image = this.response;
				var hash = faultylabs.MD5(image);
//				console.log("image (faultylabs/arraybuf): " + hash);
				if (hash == no_imagery_here_md5){
					findNext(" (but normal streetview)");
				} else {
					var img = document.getElementById('image');
					var url = window.URL || window.webkitURL;
					img.src = url.createObjectURL(new Blob([image]));
					image_hash = hash;
					image_displayed_since = (new Date()).getTime();
				}

			}
		}
		xhr.open('GET', Url+locString+params);
		xhr.responseType = 'arraybuffer';
		xhr.send(); 

//		panorama = new google.maps.StreetViewPanorama(document.getElementById('image-display'), panoramaOptions);
//		console.log(panorama.getPov().heading);
//		map.setStreetView(panorama);
//		panorama.setVisible(true);
		time = new Date();
		if (state_multipleChoice) setupMultipleChoice(data.location.latLng);
	} else {
		if (status == google.maps.StreetViewStatus.UNKNOWN_ERROR){
			console.log("unknown_error");
			return;
		}
		findNext("");
	}
}


function findNext(add){
	if (searchIterations++ < 15){
		totalsearchIterations++;
		document.getElementById("searchiterations").textContent = searchIterations;
		document.getElementById("totaliterations").textContent = totalsearchIterations;
		sv.getPanoramaByLocation(randomizedLoc(city), 49*searchIterations, findPanorama);
	} else if (DEBUG){
		var table = document.getElementById("no_streetview_cities");
		var tr = document.createElement('tr');
		var td = tr.appendChild(document.createElement('td'));
		td.innerHTML = "Not available from streetview image api"+add+": ";
		for (var i = 0; i < 5; i++){
			var td = tr.appendChild(document.createElement('td'));
			td.innerHTML = city[i];

		}
		table.appendChild(tr);
		if (state_scale == GLOBAL){
			hideMarkers(choiceMarkers);
			choiceMarkers = new Array();
			setCity();
			initStreetView();
		}
	}
}

//@callback for click on map if not in multiple choice mode
function processWildGuess(event) {
	document.getElementById("time").textContent = (new Date() - time) / 1000 + " sec";
	// guessed location
	guessLoc = new google.maps.Marker({
		position: event.latLng,
		map: map,
		draggable: true,
	});
	google.maps.event.addListener(guessLoc, 'drag', drag);

	// image location
	locPointer = new google.maps.Marker({
		position: loc,
		map: map,
		icon: {
			path: google.maps.SymbolPath.BACKWARD_CLOSED_ARROW,
			scale: 5,
			strokeColor: 'black'
		},
	});
	var result = {
			strokeColor: 'gold',
			strokeOpacity: 0.8,
			strokeWeight: 2,
			fillColor: 'gold',
			fillOpacity: 0.35,
			map: map,
			center: loc,
			radius: 1
	};
	locCircle = new google.maps.Circle(result);

	// distance label
	var myOptions = {
			content: "",
			disableAutoPan: true,
			pixelOffset: new google.maps.Size(-22, -54),
			position: loc,
			closeBoxURL: "",
			isHidden: false,
			pane: "mapPane",
			enableEventPropagation: true,
			boxStyle: {'z-index': 200}
	};

	distanceLabel = new InfoBox(myOptions);
	distanceLabel.open(map);
	setDistance(event);
	setTimeout(initialize, 2000);
}

function setDistance(event){
	if (!state_multipleChoice){
		var dist = google.maps.geometry.spherical.computeDistanceBetween(event.latLng, loc);
		dist = Math.round(dist);
		var distText = dist / 1000 + " km";
		document.getElementById("distance").textContent = distText;
		locCircle.setRadius(dist);
		distanceLabel.setContent(distText)
	}
}

//@callback for selecting a multiplechoice marker
function processChoice(event){
	if (this == correctMarker){
		this.setIcon({
			path: google.maps.SymbolPath.CIRCLE,
			scale: 10,
			fillColor: "#00F",
			fillOpacity: 0.8,
			strokeWeight: 1
		})
		var result = {"heading":heading,"pitch":pitch,"fov":fov,"attempts":attempts,"city":city[0],
			"image":{"format":"jpg","bytes":image},"ip":IP,"duration":(new Date()).getTime() - image_displayed_since};
		var geometry = {"type":"Feature","geometry":{"type":"Point","coordinates":[loc.lat(),loc.lng()]},"properties":result};
		var toLuci = {"action":"update_scenario", "ScID":2, "geometry": {"image_location":{"format": "GeoJSON", "geometry": geometry}}};
		lc.sendAndReceive(toLuci,[function(l){
			var m = l.getMessage();
			if ("result" in m) console.log(m.result.message);
			else throw new Error(m.error);
		}]);
		setTimeout(initialize, 2000);
	} else {
		attempts++;
		this.setMap(null);
	}
}

//@callback for deactivating / deselecting "multipleChoice"
function hideMarkers(m){
	for (var i = 0; i < m.length; i++){
		m[i].setMap(null);
	}
}

function showMarkers(m){
	for (var i = 0; i < m.length; i++){
		m[i].setMap(map);
	}
}

//@callback for activating / selecting "multipleChoice"
function setupMultipleChoice(latLng){
	if (correctMarker != null && !latLng.equals(correctMarker.getPosition())){
		hideMarkers(choiceMarkers);
		choiceMarkers = new Array();
		correctMarker = getClickableMarker(latLng, processChoice, null);
	} else if (correctMarker == null){
		correctMarker = getClickableMarker(latLng, processChoice, null);
	}
	if (choiceMarkers.length == 0){
		choiceMarkers.push(correctMarker);
		if (state_scale == GLOBAL){
			if (global_cities == null){
				create_global_cities_list();
			}
			for (var i = 0; i < MULTIPLECHOICES; i++){
				var c = global_cities[getRandomInt(0, global_cities.length)];
				choiceMarkers.push(getClickableMarker(getLatLng(c), processChoice, null));
			}
			showMarkers(choiceMarkers);
		} else if (state_scale == CITY){
			for (var i = 0; i < MULTIPLECHOICES; i++){
				sv.getPanoramaByLocation(randomizedLoc(city), 49, processAlternative);
			}
			// just spawned 5 (or the amount of MULTIPLECHOICES) threads; no (simple) concurrency in javascript
			// --> we just use the timer:
			setTimeout(showChoices, 20);
		}
	} else {
		// show choices
		showMarkers(choiceMarkers);
	}
}

function showChoices(){
	if (choiceMarkers.length == MULTIPLECHOICES){
		showMarkers(choiceMarkers);
	} else {
		setTimeout(showChoices, 20);
	}
}

function processAlternative(data, status){
	if (status == "OK"){
		choiceMarkers.push(getClickableMarker(data.location.latLng, processChoice, null));
	} else {
		sv.getPanoramaByLocation(randomizedLoc(city), 49, processAlternative);
	}
}

//@callback for dragging a wildguess marker
function drag(event){
	setDistance(event);
}

//@callback for selecting a city on scale change to city
function selectCity(event){
	processCity(this.city);
}

function processCity(c){
	city = c;
	hideMarkers(selectionMarkers);
	selectionMarkers = new Array();
	slideBackImageDisplay();
	$cityLabel = $("#cityLabel");
	$cityLabel.text(c[0]);
	$cityLabel.css("display", "inline-block");
	console.log("pass");
	$cityLabel.click(function(){
		console.log("clicked city label");
		c = null;
		setCity();
		$cityLabel.text("");
		$cityLabel.css("display", "none");
	});
}
