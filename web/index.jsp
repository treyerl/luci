<html>
<head>
  <title>Embedded Jetty: JSP Examples</title>
  <link href="static/main.css" media="all" rel="stylesheet" type="text/css" />
</head>
<body>
  <h1>Jetty: Luci, ActiveMQ & JSP Examples</h1>
  <p>
    Examples of JSP within ActiveMQ's Jetty (v9.2.8). Adapted from <a href="https://github.com/jetty-project/embedded-jetty-jsp">Embedded Jetty<a>.
  </p>
  <ul>
    <li><a href="test/dump.jsp">JSP 1.2 embedded java</a></li>
    <li><a href="test/bean1.jsp">JSP 1.2 Bean demo</a></li>
    <li><a href="test/tag.jsp">JSP 1.2 BodyTag demo</a></li>
    <li><a href="test/tag2.jsp">JSP 2.0 SimpleTag demo</a></li>
    <li><a href="test/tagfile.jsp">JSP 2.0 Tag File demo</a></li>
    <li><a href="test/expr.jsp?A=1">JSP 2.0 Tag Expression</a></li>
    <li><a href="test/jstl.jsp">JSTL Expression</a></li>
    <li><a href="test/foo/">Mapping to &lt;jsp-file&gt;</a></li>
    <li><a href="date/">Servlet Forwarding to JSP demo</a></li>
  </ul>
  <p>
    Mooc Map (based on Mapbox):
  </p>
  <ul>
  	<li><a href="tMap/">Overview</a></li>
  	<li><a href="tMap/concept_web.pdf">Concept</a></li>
  	<li><a href="tMap/copy.jsp/DONT_CLICK_HERE_JUST_ACCIDENTIALLY">Import Data From Artem</a></li>
  	<li><a href="tMap/select_city.jsp">Select City</a></li>
  	<li><a href="tMap/select_image.jsp">Select Images</a></li>
  	<li><a href="tMap/guess.jsp">The Selection Game</a></li>
  	<li><a href="tMap/score.jsp">Score of people's image selections</a></li>
  </ul>
  <p>
  	Luci Examples:
  </p>
  <ul>
  	<li><a href="iMap/">iMap (based on Google Maps)</a></li>
  	<li><a href="mqtt/">MQTT</a></li>
  	<li><a href="mqtt/simple.html">Simple MQTT</a></li>
  	<li><a href="ws.html">Luci on Websocket</a></li>
  	<li><a href="luciConnect.html">LuciConnect</a></li>
  	<li><a href="webSocketFileTransferExample.html">Web Socket FileTransfer Example.html</a></li>
  </ul>
</body>
</html>