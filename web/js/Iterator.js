var Iterator = function(arr){ return {
    i : 0,
    hasNext : function(){ return this.i + 1 < arr.length; },

    next : function(){
        if(this.hasNext()){
        	this.i++;
        	return arr[this.i];
        } 
        return false;
    }
}   
};

var FilterArray = function(arr, filterFunc){ return {
    i : -1,
    valid_next: null,
    next: function(){
    	if (this.valid_next == null){
    		var ii = this.i + 1;
        	while(ii < arr.length){
        		var itm = arr[ii];
        		if (filterFunc(itm)){
        			this.valid_next = itm;
        			this.i = ii;
        			
        			return this.valid_next;
        		}
        		ii++;
        	}
        	this.i = ii; // so that we don't re-test the last elements
        	return false;
    	} else {
    		var tmp = this.valid_next;
    		this.valid_next = null;
    		return tmp;
    	}
    },
    hasNext: function(){
    	if (this.valid_next == null){
    		if (this.next() != false) return true;
    		return false;
    	}
    	return true;
    }
}   
};

var FilterIterator = function(it, filterFunc){ return {
    i : 0,
    valid_next: null,
    next: function(){
    	if (this.valid_next == null){
    		var ii = this.i;
        	while(it.hasNext()){
        		this.valid_next = it.next();
        		ii++;
        		if (filterFunc(this.valid_next)){
        			this.i = ii;
        			return this.valid_next;
        		}
        	}
        	this.i = ii; // so that we don't re-test the last elements
        	return false;
    	} else {
    		var tmp = this.valid_next;
    		this.valid_next = null;
    		return tmp;
    	}
    },
    hasNext: function(){
    	if (this.valid_next == null){
    		if (this.next() != false) return true;
    		return false;
    	}
    	return true;
    }
}   
};