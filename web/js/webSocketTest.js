var ws = new WebSocket("ws://localhost:8080/ws/");

ws.onopen = function() {
	console.log("Opened!");
    ws.send("Hello Server");
};

ws.onmessage = function (evt) {
	console.log("Message: " + evt.data);
};

ws.onclose = function() {
	console.log("Closed!");
};

ws.onerror = function(err) {
//    alert("Error: " + err);
    console.log("Error: " + err);
};