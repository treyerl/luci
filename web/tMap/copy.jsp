<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>tMap: copy</title>
<script type="text/javascript" src='../js/luciConnect.js'></script>
<!-- <script src='../js/jquery-2.1.3.min.js'></script> -->
<script type="text/javascript" src="../js/faultylabs.md5.js"></script>
<script src='../js/mapbox.js-bower-2.2.1/mapbox.js'></script>
<link href='../js/mapbox.js-bower-2.2.1/mapbox.css' rel='stylesheet' />
<style>
  body { margin:0; padding:0; }
  #map{ position:absolute; top:0; bottom:0; }
  #map { z-index:3; width:100%;}
  p, h1 {margin: 50px;}
  .leaflet-container {background: transparent;}
</style>
</head>
<body>
<div id='map'></div>
<h1>Mooc Images Copy</h1>
<p>This is a webpage containing a javascript that remote controls Luci and make it download all 
images (hosted in its webfolder) into its database.</p>
<script>
	// Provide your access token
	L.mapbox.accessToken = 'pk.eyJ1IjoidHJleWVybCIsImEiOiJiY2ExZTQ4NTNmYTA2MDRjNTdjZDQ0ZDZkZjU0MzhjNCJ9.nGBCvtCwkSs1NzOE6DQnRA';
	// Create a map in the div #map
	var map = L.mapbox.map('map', 'treyerl.1c1947cc').setView([40, 8.50], 2);

	var data;
	var dataKeys;
	var ScID;
	var mqtt_topic;
	var scenarioName;
	window.imgCount = 0;
	
	var copy = function(e){
// 		console.log(k.getMessage());
		mqtt_topic = e.getMessage().result.mqtt_topic;
		ScID = e.getMessage().result.ScID;
		scenarioName = e.getMessage().result.name
		
// 		console.log("mqtt_topic:", mqtt_topic, ", ScID:", ScID, ", scenarioName:", scenarioName);
		
		var xhr = new XMLHttpRequest();
		var currentMarker;
		xhr.open('GET', "dataSource/mooc.json", true); // parameter 3 = asynchronous
		xhr.responseType = 'json'
		xhr.onload = function(e) {
			data = xhr.response;
			dataKeys = Object.keys(data);
			next();
		};
		xhr.send();
	}
	
	var next = function(){
		if (window.imgCount < dataKeys.length){
			var cityname = dataKeys[window.imgCount++];
			var city = data[cityname];
// 			console.log(city);
			if ("location" in city){
				var loc = city["location"];
				var features = [];
				for (var i in city.images){
					var img = city.images[i];
					var href = location.href;
					var url = href.substring(0, href.lastIndexOf("/"))+"/dataSource/images/"+img.url;
// 					console.log(url);
					delete img.url;
					var feature = {
						"type":"Feature",
						"geometry":{
							"type":"Point",
							"coordinates":[loc.lat,loc.lng]
							},
						"properties":img
					}
					img.image = {"url": url, "format":url.substring(url.lastIndexOf(".")+1)};
					img.city = cityname;
					img.country = city.country;
					features.push(feature);
				}
				var geometry = {"type": "FeatureCollection", "features": features};
				geometry = {cityname:{"format": "GeoJSON", "geometry": geometry, "switchLatLon":true}};
				var name = null;
				var cllbck = function(e){/*console.log(e.getMessage()); */next()};
// 				console.log(city.images.length, geometry);
				console.log(window.imgCount+"/"+dataKeys.length);
				lc.updateScenario(ScID, cllbck, name, geometry);
			} else {
				console.log(cityname, "has no location");
				next();
			}
		} else console.log("DONE", i);
	}
	
	var lc = new LuciClient("localhost", 8080, "ws/", function(){
// 		lc.on("onmessage", function(e){console.log("message: ",e.getMessage());});
// 		lc.on("onsent", function(e, xhrBck){console.log("onsent: ", e.getMessage())});
		lc.authenticate("lukas", "1234", function(){
			var projection = {'crs':'EPSG:4326'};
			var geometry = null;
			lc.createScenario("mooc_images", copy, geometry, projection)});
	});
</script>
</body>
</html>