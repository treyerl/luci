<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>tMap</title>
<script type="text/javascript" src="../js/luciConnect.js"></script>
<script type="text/javascript" src="../js/faultylabs.md5.js"></script>
<script src='https://api.tiles.mapbox.com/mapbox.js/v2.2.1/mapbox.js'></script>
<link href='https://api.tiles.mapbox.com/mapbox.js/v2.2.1/mapbox.css' rel='stylesheet' />
<style>
  body { margin:0; padding:0; }
  #map,img { position:absolute; top:0; bottom:0; }
  img { z-index:1; height:100%}
  #map { z-index:3; width:100%;}
  .leaflet-container {background: transparent;}
  p, h1 {margin: 50px; position: absolute; top:0; left:0; z-index: 4;}
  p {background: white; padding: 3px; font-family: Helvetica,Arial,Sans-Serif}
</style>
</head>
<body>
<div id='map'></div>
<img id='bck' src="" alt="" />
<p><span id="time"></span> sec left</p>
<script>
	// Provide your access token
	L.mapbox.accessToken = 'pk.eyJ1IjoidHJleWVybCIsImEiOiJiY2ExZTQ4NTNmYTA2MDRjNTdjZDQ0ZDZkZjU0MzhjNCJ9.nGBCvtCwkSs1NzOE6DQnRA';
	// Create a map in the div #map
	var map = L.mapbox.map('map', 'treyerl.1c1947cc').setView([40, 8.50], 2);
	
	var currentMarker;
	var ScID;
	var cities;
	var cityNames;
	var numCities;
	var theCityName;
	var theCity;
	var theImageKey;
	var theImgID;
	var the;
	var markers = []
	var guessed = false;
	var t = 30;
	
	var step1 = function(e){
		console.log("luci.onauthenticated");
		var xhr = new XMLHttpRequest();
		xhr.open('GET', "data/moocCities.json", true); // parameter 3 = asynchronous
		xhr.responseType = 'json'
		xhr.onload = function(e) {
// 			console.log("xhr.onload")
			var data = xhr.response;
			ScID = data.ScID;
			delete data.ScID;
// 			DBG_imageCheck(data);
			cities = data;
			cityNames = Object.keys(data);
			numCities = cityNames.length;
			setup();
		};
		xhr.send();
	}
	
	var setup = function(){
// 		console.log("setup");
		guessed = false;
		for (var index in markers) map.removeLayer(markers[index]);
		theCityName = cityNames[Math.round(Math.random()*(numCities-1))];
		theCity = cities[theCityName];
		theImageKey = Math.round(Math.random()*(theCity.images.length-1));
		theImgID = theCity.images[theImageKey];
		var action = {"action":"get_scenario","ScID":ScID,"geomIDs":[theImgID],"withAttachments":false};
		console.log("luci.send(",action,")");

		// load image
		lc.sendAndReceive(action, [function(e){
			console.log("luci.onreceive");
			var msg = e.getMessage();
			console.log(msg);
			the = msg.result.geometry.GeoJSON.geometry.features[0].properties;
			document.getElementById('bck').src = "/download/"+the.image.streaminfo.checksum+"."+the.image.format;
		}]);
		
		for (var cityname in cities){
			var city = cities[cityname];
			var loc = city["location"];
			var imgCount = city.images.length;
			// create a mapbox marker
			var cmp = L.popup({maxWidth:550});
			var cm = L.marker(new L.LatLng(loc.lat, loc.lon), {
			    icon: L.mapbox.marker.icon({
			        'marker-color': '88f',
			    	}),
			    'opacity': 0.8}).bindPopup(cmp).addTo(map);
			cm.cityname = cityname;
			cm.city = city;
			cm.off('click');
			cm.on('click', function(e){
				if (guessed) {
					theCity.cm.closePopup();
// 					window.setTimeout(setup, 25);
					return;
				}
				guessed = true;
				window.currentMarker = this;
				var userComment = "";
				if (the.comments.length > 0) userComment = "User '"+the.user+"' writes:</br></br>"+the.comments[0]
				var answer = "";
				var properties = {"geomID":the.geomID};
				if(this.city == theCity) {
					answer = "Concratulations! You guessed right!</br> This is ";
					properties.self = the.self = the.self + 1 || 1;
				} else {
					answer = "Wrong! This is ";
					properties.others = the.others = the.others + 1 || 1;
					properties.mistakes = the.mistakes || {};
					properties.mistakes[this.cityname] = properties.mistakes[this.cityname] + 1 || 1;
				}
				theCity.cm.setPopupContent(answer+theCity.cm.cityname+", "+theCity.cm.city.country+". "+userComment);
				theCity.cm.openPopup();
// 				properties.self = the.self;
// 				properties.others = the.others;
				console.log(properties);
				var geometry = {"type":"Feature","properties":properties};
				var action = {"action":"update_scenario","ScID":ScID,"geometry":{"guess":{"format":"GeoJSON","geometry":geometry}}};
				lc.sendAndReceive(action, [function(e){console.log(e.getMessage());}]);
// 				window.setTimeout(function(){theCity.cm.closePopup();}, 60000);
			});
// 			cm.on('popupclose', setup);
			cmp.on('close', setup);
			markers.push(cm)
			if (city == theCity) {
// 				console.log(cityname);
				theCity.cm = cm;
			}
		}
		countDown(t);
	}
	
	var countDown = function(ti){
// 		console.log("countDown")
		document.getElementById("time").textContent = ti;
		if (ti > 0) {
			if (!guessed) window.setTimeout('countDown('+(ti-1)+')', 1000);
		} else setup();
	};
	
	var DBG_imageCheck = function(data){
		for (var cityname in data){
			var city = data[cityname];
			if(city.images.length == 0) console.log(cityname);
		}
	}
	
	var step0 = function(e){
		console.log("websocket.onopen");
		lc.on("onmessage", function(e){console.log("message: ",e.getMessage());});
		window.setTimeout('lc.authenticate("lukas", "1234", step1)',100);
		
	};
	
	var lc;
	window.onload = function(){
// 		console.log("window.onload");
		lc = new LuciClient(location.hostname, 8080, "ws/", step0);
		lc.downloadAttachments = false;
	}
</script>
</body>
</html>
