<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>tMap</title>
<script type="text/javascript" src="../js/luciConnect.js"></script>
<!-- <script type="text/javascript" src="../js/printStackTrace.js"></script> -->
<script type="text/javascript" src="../js/faultylabs.md5.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.5/d3.min.js"></script>
<script type="text/javascript" src='../js/mapbox.js-bower-2.2.1/mapbox.js'></script>
<script type="text/javascript" src="js/overview.js"></script>
<script type="text/javascript" src="js/guess.js"></script>
<script type="text/javascript" src="js/score.js"></script>
<link href='../js/mapbox.js-bower-2.2.1/mapbox.css' rel='stylesheet' />
<link href='style_bottom.css' rel='stylesheet' />
<link href="buttons.less" media="all" rel="stylesheet" type="text/css">

</head>
<body>
<div id='map'></div>
<img id='bck'/>
<div id='rank'></div>
<div id="buttons">
	<table id="mode" class="button">
		<tr>
			<th id="overviewButton" class="selected">All Messages</th>
			<th id="gameButton" >Game</th>
			<th id="scoreButton" >Game Statistics</th>
		</tr>
	</table>
</div>
<div id="gameControl" style="display:None;">
	<table id="mode" class="button">
		<tr>
			<th id="nextRound">Next Round</th>
		</tr>
	</table>
	
	<p id="roundDisplay">Round <span id="round"></span> - <span id="time"></span></p>
</div>
<div id="gameExplanation">
	<p>within one minute</br>select the city </br>in which you think the image</br>in the background was taken</br>&gt;click here to start&lt;</p>
</div>
<div id="gameDone">
	<p>Thank you for playing!</br>Check out your score at the bottom right</br>and click on one of the circles to revisit your images.</br>Click on the small circles to see the cities it was mistaken with.</br>&gt;ok&lt;</p>
</div>
<div id="highscore"></div>
<div id="key">
<!-- 	<div> -->

<!-- 		By playing this game you help separate the characteristic from the generic visual qualities of the city.  -->
<!-- 		Each time the game is played, the results are added to a statistical database which will evolve  -->
<!-- 		throughout the duration of the exhibition. -->
<!-- 	</div> -->
	
	
	<svg width="500" height="48">
		<g>
			<circle r="5" cx="20" cy="20.5" style="stroke: black; fill: white; stroke-width: 1px;"></circle>
			<rect x="30" y="12" rx="4" ry="4" width="135" height="32" style="fill: #ddd; fill-opacity: 1;"/>
			<text x="33" y="23">city not appearing in the game</text>
			
		</g>
		<g>
			<circle r="5" cx="20" cy="35" style="stroke: black; fill: black; stroke-width: 1px;"></circle>
			<text x="33" y="38">city appearing in the game</text>
		</g>
		<g>
			<rect x="184" y="12" rx="4" ry="4" width="201" height="32" style="fill: #ddd; fill-opacity: 1;"/>
			<circle r="5" cx="175" cy="20.5" style="stroke: black; fill: #F50; stroke-width: 1px;"></circle>
			<text x="173" y="23" style="font-size:8px;">1</text>
			<text x="188" y="23">image 1; appearing in the game (</text>
			<circle r="5" cx="330" cy="20.5" style="stroke: black; fill: none; stroke-width: 1px;"></circle>
			<text x="340" y="23">)</text>
			
		</g>
		<g>
			<circle r="6" cx="175" cy="35" style="stroke: white; fill: #FC0; stroke-width: 1px;"></circle>
			<text x="173" y="38" style="font-size:8px;">3</text>
			<text x="188" y="38"">image 3;  not appearing in the game (</text>
			<circle r="5" cx="345" cy="35.5" style="stroke: white; fill: none; stroke-width: 1px;"></circle>
			<text x="355" y="38">)</text>
			<rect x="5" y="9" rx="4" ry="4" width="401" height="38" style="stroke: black;fill: none; fill-opacity: 1;"/>
		</g>
	</svg>
</div>
<div id="info">
	<svg height="22" width="22" panel="overviewInstructionPanel" style="margin-left: 50px;">
		<g>
			<circle r="10" cx="11" cy="11" style="stroke:black; fill: white; stroke-width: 1px;"></circle>
			<text x="3" y=14 style="font-size: 10px; font-family: 'Helvetica Neue', Arial, sans-serif;">info</text>
		</g>
	</svg>
	<div id="overviewInstructionPanel">
		The images / messages you see here are contributed by roughly 1000 world-wide students participating
		the Massive Open Online Course (MOOC) held by Prof. Dr. Gerhard Schmitt. While on one side he presented
		news from the Future Cities Lab (FCL) they on the other side sent in their view in form of excercises.
		In this interactive presentation you can a) explore the messages of the students, 
		b) participate in a game in which you are asked to guess where the sent-in images are from, 
		and c) survey how all the visitors made a contribution to distinguish iconic or unique cityscapes from
		rather generic ones by playing this very game.
	</div>
	<svg height="22" width="22" panel="gameInstructionPanel" style="margin-left: 58px;">
		<g>
			<circle r="10" cx="11" cy="11" style="stroke:black; fill: white; stroke-width: 1px;"></circle>
			<text x="3" y=14 style="font-size: 10px; font-family: 'Helvetica Neue', Arial, sans-serif;">info</text>
		</g>
	</svg>
	<div id="gameInstructionPanel">
		Being asked to locate the presented image on the provided map try to get as close as possible. A game
		consists of 5 rounds. Try to beat the all-time best 3 players shown at the top right.
	</div>
	<svg height="22" width="22" panel="statInstructionPanel" style="margin-left: 70px;">
		<g>
			<circle r="10" cx="11" cy="11" style="stroke:black; fill: white; stroke-width: 1px;"></circle>
			<text x="3" y=14 style="font-size: 10px; font-family: 'Helvetica Neue', Arial, sans-serif;">info</text>
		</g>
	</svg>
	<div id="statInstructionPanel">
		<p>
			This game separates the specific from the generic visual qualities of the city. 
			Each time the game is played, the results are added to a statistical database which will 
			evolve throughout the duration of the exhibit.</p>  
		<p>
			Correctly identified images, further support the image’s specific quality. Incorrectly identified 
			images are not only characterized as generic, but they reveal similarities among the images. 
			Thus they indicate where an image seems to have originated from. 
		</p> 
		<p>
			This type of game allows users to connect with one another through scenes gathered around 
			the world and is only a starting point to how we could share our common experiences.
		</p>
	</div>
</div>
<div id="logos">
<!-- 	<a href="http://www.ethz.ch" target="_blank"> --><img class="logo" src="img/eth_logo.png" alt="" /><!-- </a> -->
<!-- 	<a href="http://ia.arch.ethz.ch" target="_blank" > --><img class="logo" src="img/ia_logo.png" alt="" /><!-- </a> -->
</div>

<script>
	// Provide your access token
	L.mapbox.accessToken = 'pk.eyJ1IjoidHJleWVybCIsImEiOiJiY2ExZTQ4NTNmYTA2MDRjNTdjZDQ0ZDZkZjU0MzhjNCJ9.nGBCvtCwkSs1NzOE6DQnRA';
	// Create a map in the div #map
	var map = L.mapbox.map('map', 'treyerl.1c1947cc', {keyboard:false});
	var lc;
	var currentView = "overview";
	var idleTime = 0;
	
	var resetMap = function(){
		map.setView([40, 8.50], 2);
	}
	resetMap();
	
	var authenticate = function(e){
		lc.downloadAttachments = false;
// 		lc.on("onmessage", function(e){console.log("message: ",e.getMessage());});
		lc.authenticate("lukas", "1234", function(e){loadAllCities([setupOverview])})
// 		lc.authenticate("lukas", "1234", function(e){loadAllCities([setupGuessingGame])}) 
// 		lc.authenticate("lukas", "1234", function(e){loadAllCities([showScore])}) 
	};
	
	var show = function(){
		console.log("show", this.getAttribute("panel"));
		var _otherPanels = d3.selectAll("#info div");
		_otherPanels.style("display", "none");
		var _others = d3.selectAll("#info svg");
		_others.on("click", null);
		_others.on("click", show);
		d3.select("#"+this.getAttribute("panel")).style("display", "block");
		var _this = d3.select(this);
		_this.on("click", null);
		_this.on("click", hide);
	}
	
	var hide = function(){
		console.log("hide", this.getAttribute("panel"));
		d3.select("#"+this.getAttribute("panel")).style("display", "none");
		var _this = d3.select(this)
		_this.on("click", null);
		_this.on("click", show);
	}
	
	window.onload = function(){
// 		console.log(location.hostname);
		lc = new LuciClient(location.hostname, 8080, "ws/", authenticate);
		lc.downloadAttachments = false;
		d3.selectAll("#info svg").on("click", show);
		//Increment the idle time counter every minute.
	    var idleInterval = setInterval(timerIncrement, 60000); // 1 minute
	    document.addEventListener("mousemove", function(e){idleTime = 0;})
	}
	
	function timerIncrement() {
	    idleTime++;
	    if (idleTime > 1) { // 2 minutes
	        window.location.reload();
	    }
	}
	
	
	map.whenReady(function(){
		document.getElementById("overviewButton").addEventListener("click", function(){location.reload();});
		document.getElementById("gameButton").addEventListener("click", setupGuessingGame);
		document.getElementById("scoreButton").addEventListener("click", showScore); // show score of the last 5 rounds
		document.getElementById("key").addEventListener("click", function(){d3.select(this).style("display", "none")});
	});
</script>
</body>
</html>