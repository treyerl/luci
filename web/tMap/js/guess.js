var guessed = false;
var t = 60;
var ti = 0;
var timeout;
var timeoutNextRound;
var timeoutDrawDistance;
var deselectedCityNames = [];
var correctCity;
var currentImgID;
var currentImageIndex;
var currentRound = 0;
var HEC = halfEarthCircumference = 20037508;
var gameScores = [];
var avgScore = 0;
var polyline;
var polyline2;
var answerMarker;
var bestRank = [];
var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
var roundHtml = 'Round <span id="round"></span> - <span id="time">';
var tickingNextRound = false;

Math.avg = function() {
	var cnt, tot, i;
	cnt = arguments.length;
	if (cnt == 0) return 0;
	else if (cnt == 1){
		if (typeof_o(arguments[0]) == "Array") 
			return Math.avg.apply(Math, arguments[0]);
	}
	tot = i = 0;
	while (i < cnt) tot+= arguments[i++];
	return tot / cnt;
}

var clearTimeouts = function(){
	clearTimeout(timeout);
	clearTimeout(timeoutNextRound);
	clearTimeout(timeoutDrawDistance);
}

var findDeselectedCityNames = function(){
	for (var cityname in cities){
		if (selectedCityNames.indexOf(cityname) == -1) deselectedCityNames.push(cityname)
	}
}

var removeDeselectedCities = function(){
	if (deselectedCityNames.length == 0) findDeselectedCityNames();
//	console.log("removeDeselctedcities", deselectedCityNames);
	if (Object.keys(markers).length > numSelectedCities){
		var dcl = deselectedCityNames.length;
		for (var i = 0; i < dcl; i++){
			map.removeLayer(markers[deselectedCityNames[i]]);
		}
	}
}

var restoreDeselectedCities = function(){
	if (deselectedCityNames.length == 0) findDeselectedCityNames();
	var dcl = deselectedCityNames.length;
	for (var i = 0; i < dcl; i++){
		map.addLayer(cities[deselectedCityNames[i]].marker);
	}
}

var addSelectedCities = function(){
	for (var i = 0; i < numSelectedCities; i++){
		map.addLayer(markers[selectedCityNames[i]]);
	}
}

var unbindGuessPopups = function(){
	for (var i = 0; i < numSelectedCities; i++){
		markers[selectedCityNames[i]].unbindPopup();
	}
}

var replaceClickCallbackOfSelectedCities = function(){
	for (var i = 0; i < numSelectedCities; i++){
		var m = markers[selectedCityNames[i]];
		m.off("click");
		m.on("click", guessClick);
	}
//	var cm = markers[correctCity.name];
//	var cmp = L.popup({maxWidth:550});
//	cm.bindPopup(cmp).off('click').on("click", guessClick);
}

var restoreClickCallbackOfSelectedCities = function(){
	for (var i = 0; i < numSelectedCities; i++){
		var m = markers[selectedCityNames[i]];
		m.off("click");
		m.on("click", setupSelectedCity);
	}
}

var resetGameMarkers = function(){
	var cm = markers[correctCity.name];
	cm.setIcon(getIcon(true));
	cm.off("click").on("click", guessClick)
	if (answerMarker !== undefined) map.removeLayer(answerMarker);
}

var removePolylines = function(){
	if (polyline !== undefined) map.removeLayer(polyline);
	if (polyline2 !== undefined) map.removeLayer(polyline2);
}


var k = 0;

var nextRound = function(e){
	clearTimeouts();
	if (e !== undefined) {
		console.log(e.target)
		this.off('close');
	}
	if (currentRound > 0){
		if (!guessed) {
			gameScores[currentRound - 1] = {"score":0, "city": correctCity.name, 'imgID':currentImgID};
			drawHighScore();
		}
		resetGameMarkers();
		if (currentRound == 5){
			showScore();
			return;
		}
	}
	guessed = false;
	currentRound++;
	document.getElementById("round").textContent = currentRound;
	removePolylines();
	
	// ask luci to tell us the city / image with the least votes
	var action = {'action':'run', 'service':{'classname':'NextMoocCity', 'inputs':{'ScID':ScID}}};
	lc.sendAndReceive(action, [function(e){
		var msg = e.getMessage();
		console.log("next mooc city: msg: ", msg);
		if ("error" in msg) {
			alert(msg.error);
			return;
		}
		var cityname = msg.result.outputs.city;
		currentImgID = msg.result.outputs.geomID;
		correctCity = cities[cityname];
		console.log("cityname",cityname,"currentImgID",currentImgID);
		
		fetchImageURLs(correctCity, function(){
//			console.log(correctCity.imageURLs[currentImgID]);
			currentImageIndex = correctCity.images.indexOf(currentImgID);
			console.log(correctCity.properties);
			document.getElementById('bck').src = correctCity.imageURLs[currentImgID];
			d3.select("#bck").style("display", "block");
			countDown(t);
		});
		map.setView([40, 8.50], 2)
	}]);
//	
//	var cityname = selectedCityNames[Math.round(Math.random()*(numSelectedCities-1))];
////	var cityname = "Cairo";
//	correctCity = cities[cityname];
//	var i = Math.round(Math.random()*(correctCity.selectedImages.length-1));
//	currentImgID = correctCity.selectedImages[i];
//	currentImageIndex = correctCity.images.indexOf(currentImgID);
////	console.log("currentImageIndex nextRound", currentImageIndex, currentImgID, correctCity.selectedImages.length-1);
////	var action = {"action":"get_scenario","ScID":ScID,"geomIDs":[currentImgID],"withAttachments":false};
////	console.log("luci.send(",action,")");
//	fetchImageURLs(correctCity, function(){
////		console.log(correctCity.imageURLs[currentImgID]);
//		document.getElementById('bck').src = correctCity.imageURLs[currentImgID];
//		d3.select("#bck").style("display", "block");
//		countDown(t);
//	});
//	map.setView([40, 8.50], 2)
//
////	// load image
////	console.log(k, "sendAndReceive from nextRound")
////	if (k++ > 0) throw new Error("sendAndReceive from nextRound"); //console.log("sendAndReceive from nextRound");
////	lc.sendAndReceive(action, [function(e){
////		
////		var msg = e.getMessage();
////		the = msg.result.geometry.GeoJSON.geometry.features[0].properties;
////		document.getElementById('bck').src = "/download/"+the.image.streaminfo.checksum+"."+the.image.format;
////		countDown(t);
////		console.log("received all geometries")
////	}]);
}

var drawHighScore = function(){
	console.log("drawHighScore", gameScores)
	var minR = 10;
	var maxR = 20;
	var padR = 35;
	var x = 0;
	var intro = 180;
	var g;
	var scores = [];
	var numScores = gameScores.length;
	for (var i = 0; i < numScores; i++) scores.push(gameScores[i].score)
	avgScore = Math.avg(scores);
	var avgScoreInverted = 1- avgScore;
	var allCirclesWidth = (numScores + 1) * padR * 2;
	var bestOfWidth = 160;
	var fullWidth = intro + allCirclesWidth + bestOfWidth;
	var labelY = 2*padR+5;
	var highScore = d3.select("#highscore");
	highScore.select("svg").remove();
	var svg = highScore.append("svg")
		.attr("width", fullWidth)
		.attr("height", 80);
	svg.append("rect")
		.attr("width", fullWidth)
		.attr("height", 80)
		.attr("rx", 5)
		.attr("ry", 5)
		.style("fill", "white")
		.style("fill-opacity", 0.8)
		.style("stroke", "white")
		.style("stroke-width", 2);
	g = svg.append("g");
	g.append("text")
		.attr("x", 5)
		.attr("y", 42)
		.style("font-size", 50)
		.style("font-weight", "bold")
		.style("fill", yellow)
		.style("fill-opacity", 0.1)
		.style("stroke", "grey")
		.style("stroke-width", "0.5px")
		.text("YOUR");
	g.append("text")
		.attr("x", 5)
		.attr("y", 75)
		.style("font-size", 41)
		.style("font-weight", "bold")
		.style("fill", yellow)
		.style("fill-opacity", 0.1)
		.style("stroke", "grey")
		.style("stroke-width", "0.5px")
		.text("SCORE");
	
	for (var i = 0; i < numScores; i++){
		var score = scores[i];
		var cityname = gameScores[i].city;
		var imgID = gameScores[i].imgID;
		var scoreInverted = 1 - score;
		x = intro - padR + (i+1) * padR * 2;
		g = svg.append("g");
		g.attr("city", cityname);
		g.attr("imgID", imgID);
//		g.attr("width", maxR).attr("height", maxR);
		g.append("circle")
			.attr("r", minR + score *maxR)
			.attr("cx", x)
			.attr("cy", padR)
			.style("fill-opacity", 0.6)
			.style("fill", yellow);
		if (score < 1 ){
			g.append("circle")
				.attr("r", minR + scoreInverted *maxR)
				.attr("cx", x)
				.attr("cy", padR)
				.style("fill-opacity", 0.5)
				.style("fill", "red");
	//			.style("stroke-width", 3);
		}
		var text = g.append("text")
			.attr("x", x)
			.attr("y", padR + 5)
			.style("text-anchor", "middle");
		if (score == 0){
			text.text("no guess");
		} else {
			text.text(Math.round(scoreInverted * HEC / 1000)+"km");
		}
		g.append("text")
			.attr("x", x)
			.attr("y", labelY)
			.style("text-anchor", "middle")
			.text(cityname);
	}
	
	// average
	x = intro - padR + allCirclesWidth;
	var lx1 = x - padR;
	var lx2 = x + padR;
	g = svg.append("g");
	g.attr("width", maxR).attr("height", maxR);
	g.append("line")
		.attr("x1", lx1)
		.attr("y1", 5)
		.attr("x2", lx1)
		.attr("y2", 2*padR+5)
		.style("stroke-width", 1)
		.style("stroke", "grey")
		.style("stroke-dasharray", "5,5,2,5");
	g.append("circle")
		.attr("r", minR + avgScore * maxR)
		.attr("cx", x)
		.attr("cy", padR)
		.style("fill-opacity", 0.6)
		.style("fill", yellow);
	g.append("circle")
		.attr("r", minR + avgScoreInverted *maxR)
		.attr("cx", x)
		.attr("cy", padR)
		.style("fill-opacity", 0.5)
		.style("fill", "red");
	var text = g.append("text")
		.attr("x", x)
		.attr("y", padR + 5)
		.style("text-anchor", "middle");
	if (avgScore == 0){
		text.text("no guesses");
	} else {
		text.text(Math.round(avgScoreInverted * HEC / 1000)+"km");
	}
	g.append("text")
		.attr("x", x)
		.attr("y", labelY)
		.style("text-anchor", "middle")
		.text("on average");
	g.append("line")
		.attr("x1", lx2)
		.attr("y1", 5)
		.attr("x2", lx2)
		.attr("y2", 2*padR+5)
		.style("stroke-width", 1)
		.style("stroke", "grey")
		.style("stroke-dasharray", "5,5,2,5");
	
	// best 3
	g = svg.append("g").attr("transform", "translate("+(lx2+8)+")").attr("id", "playerScore");
	g.append("text")
		.attr("y", 20)
		.style("font-size", 19)
		.style("font-weight", "bold")
		.style("fill", yellow)
		.style("fill-opacity", 0.1)
		.style("stroke", "grey")
		.style("stroke-width", "0.5px")
		.text("PLAYER SCORE");
	
	for (var i = 0; i < 3; i++){
		drawPlayerScoreBar(i);
	}
//	console.log("best3", bestRank)
}

var drawPlayerScoreBar = function(i){
	if (i < bestRank.length) {
		var gap = 28;
		var g = d3.select("#playerScore");
		var rank = bestRank[i].properties;
		var distance = Math.round( HEC * (1-rank.score) / 1000);
		var date = new Date(rank.timestamp);
		g.append("text")
			.attr("id", "t"+(i+1))
			.attr("y", i*17+10+gap)
			.attr("x", 5)
			.text((i+1)+" - "+date.getDate()+"."+months[date.getMonth()]+" "
					+date.getHours()+":"+("0"+date.getMinutes()).substr(-2,2)+" - "+distance+" km");
		g.append("rect")
			.attr("id", "r"+(i+1))
			.attr("y", i*17+gap)
			.attr("width", 147)
			.attr("height", 12)
			.attr("rx", 3)
			.attr("ry", 3)
			.style("fill", "grey")
			.style("fill-opacity", 0.1);
	}
}

var guessClick = function(e, m){
	console.log("guessClick", correctCity.properties, currentImgID);
	var cm = markers[correctCity.name];
	if (m === undefined) m  = this;
	if (guessed) {
//		cm.closePopup();
//			window.setTimeout(nextRound, 25);
		console.log("GUESSED");
		return;
	}
	guessed = true;
	var i = currentRound-1;
	var userComment = "";
	var the = correctCity.properties[currentImgID];
	if (the.comments.length > 0) userComment = "User '"+the.user+"' writes:</br></br>"+the.comments[0]
	var answer = "";
	var properties = {"geomID":the.geomID};
	d3.select(".leaflet-popup-pane").style("display","none");
	setupSelectedCity(null, cm, currentImageIndex, /*minimized = */true );
	
	var divIcon = L.divIcon({className: 'answerMarkerDisplay'});
	answerMarker = L.marker(m.getLatLng(), {}).addTo(map);
	answerMarker.city = m.city;
	answerMarker.setIcon(divIcon);
	answerMarker.setZIndexOffset(1001);
	
	if(m.city == correctCity) {
		gameScores.push({"score":1, "city":correctCity.name, "imgID":currentImgID});
		drawCorrectScore(answerMarker);
//		answer = "Concratulations! You guessed ("+Math.round((gameScores[i]*100))+"%) right!</br> This is ";
		properties.self = the.self = the.self + 1 || 1;
		waitForNextRound();
//		cm.openPopup();
	} else {
		
		drawWrongScore(answerMarker);
		var score = drawDistance(answerMarker, cm);
		gameScores.push({"score":score, "city":correctCity.name, "imgID":currentImgID});
//		console.log("gameScores", gameScores);
		
//		answer = "Wrong! ("+Math.round((gameScores[i]*100))+"% right) This is ";
		// feed luci
		properties.others = the.others = the.others + 1 || 1;
		properties.mistakes = the.mistakes || {};
		properties.mistakes[m.city.name] = properties.mistakes[m.city.name] + 1 || 1;
		the.mistakes = properties.mistakes;
	}
	drawHighScore();
//	console.log("currentImageIndex guessClick", currentImageIndex, currentImgID);
	
	
//	cm.setPopupContent(answer+cm.city.name+", "+cm.city.country+". "+userComment);
//	cm.openPopup();
	var geometry = {"type":"Feature","properties":properties};
	var action = {"action":"update_scenario","ScID":ScID,"geometry":{"guess":{"format":"GeoJSON","geometry":geometry}}};
	console.log("sendAndReceive from guessClick", JSON.stringify(action));
	lc.sendAndReceive(action, [function(e){
		var msg = e.getMessage();
		if ("error" in msg) alert(msg);
	}]);
}

var drawCorrectScore = function(marker){
	var cd = marker.answerDisplay = L.DomUtil.create("div", "answerDisplay green");
	cd.innerHTML = "<b>CORRECT!</b>";
	marker._icon.appendChild(cd);
	var dcd = d3.select(cd);
	dcd.on("click", function(e){
		console.log("hide", dcd);
		dcd.style("display", "none");
	});
}

var drawWrongScore = function(marker){
	var cd = marker.answerDisplay = L.DomUtil.create("div", "answerDisplay red");
	cd.innerHTML = "<b>WRONG!</b>";
	marker._icon.appendChild(cd);
	var wrongCityLabel = L.DomUtil.create("div", "wrongCity");
	wrongCityLabel.innerHTML = "<b>"+marker.city.name+"</b>, <small>"+marker.city.country+"</small>";
	marker._icon.appendChild(wrongCityLabel);
	var dcd = d3.select(cd);
	dcd.on("click", function(e){
		console.log("hide", dcd);
		dcd.style("display", "none");
	});
}

var clickNextRound = function(){
	clearTimeout(timeoutNextRound);
	var rd = document.getElementById("roundDisplay");
	rd.innerHTML = roundHtml;
//	console.log(rd);
	d3.select(".leaflet-overlay-pane").style("display","none");
	tickingNextRound = false;
	nextRound();
}

var waitForNextRound = function(){
	var rd = document.getElementById("roundDisplay");
	rd.innerHTML = "starting in <span id='time'>00:00</span>";
	var t = 10;
	tickingNextRound = true;
	var timerTick = function(){
		t--;
		if (t > 0) {
			if (t % 2 == 0) d3.select("#roundDisplay").style("background-color", "white");
			else d3.select("#roundDisplay").style("background-color", yellow);
			timeoutNextRound = window.setTimeout(timerTick, 1000);
			document.getElementById("time").textContent = displaySeconds(t);
		} else {
			clickNextRound();
		}
	}
	timeoutNextRound = window.setTimeout(timerTick, 1000);
	return timeoutNextRound;
}

var preventNextRound = function(){
	clearTimeout(timeoutNextRound);
	var rd = document.getElementById("roundDisplay");
	if (tickingNextRound){
		rd.innerHTML = "starting upon click";
		d3.select("#roundDisplay").style("background-color", yellow);
	}
}

var drawDistance = function(answerMarker, correctMarker, currentStep){
	var from = answerMarker.getLatLng();
	var to = correctMarker.getLatLng();
	var currentStep = currentStep || 0;
	var distance = from.distanceTo(to);
	var ofHEC = distance / HEC;
	var score100 = Math.round((1-ofHEC)*100);
	var currentScore100 = 100;
	var step = 1/Math.round(ofHEC*100);
	var inter = d3.geo.interpolate([from.lng, from.lat], [to.lng, to.lat]);
	polyline = L.polyline([], {opacity: 1});
	polyline.addTo(map);
	d3.select(".leaflet-overlay-pane").style("display","block");
//	correctMarker.answerDisplay.setAttribute("class", "answerDisplay red");
	map.fitBounds(L.featureGroup([answerMarker, correctMarker]).getBounds(), globalFitBoundsOptions);
	map.whenReady(timeoutHandler);
	function timeoutHandler(){
		if (currentStep == 0){
			polyline.addLatLng(from);
		} else {
			var d3LngLat = inter(currentStep);
			var l = L.latLng(d3LngLat[1], d3LngLat[0]);
			var lastPoint = polyline._latlngs[polyline._latlngs.length-1];
			
			if (lastPoint.lng < -100 && l.lng > 100 || lastPoint.lng > 100 && l.lng < -100){
				polyline2 = polyline;
				polyline = L.polyline([], {opacity: 1}).addTo(map);
			}
			polyline.addLatLng(l);
//			console.log("timeoutHandler", l, currentStep, polyline.toGeoJSON());
//			answerMarker.setPopupContent((currentScore100--)+"%");
			answerMarker.answerDisplay.innerHTML = 
				"<b>WRONG!</b><br>"+Math.round(from.distanceTo(l)/1000)+"km distance to correct answer";
//			polyline.bringToFront();
		}
		
		if ((currentStep + step) <= 1){
			timeoutDrawDistance = window.setTimeout(timeoutHandler, 40);
		} else {
			polyline.addLatLng(to);
			waitForNextRound();
			
//			answerMarker.setPopupContent(score100+"%");
//			answerMarker.getPopup().on('close', nextRound);
//			correctMarker.openPopup();
		}
		currentStep += step;
	}
//	window.setTimeout(timeoutHandler, 40);
	return 1-ofHEC;
}

var setupSelectedCities = function(clickFun){
	for (var i = 0; i < numSelectedCities; i++){
		var cityname = selectedCityNames[i];
		var city = cities[cityname];
		city.name = cityname;
		var loc = city["location"];
		var imgCount = city.images.length;
		// create a mapbox marker
//		var cmp = L.popup({maxWidth:550});
		var cm = createMarker(city, "selectedImages" in city);
//		cm.bindPopup(cmp).off('click').on('click', guessClick);
		if (clickFun !== undefined) cm.off('click').on('click', clickFun);
//		cmp.on('close', nextRound);
		markers[cityname] = cm;
	}
}

var removeAllMarkers = function(){
	for (var name in markers) map.removeLayer(markers[name]);
	markers = {};
}

var removeAllLabels = function(){
//	d3.selectAll(".leaflet-popup-pane div").remove();
	for (var i in allLabels) map.removeLayer(allLabels[i]);
	allLabels = [];
}

var setupGuessingGame = function(){
	clearTimeout(randomCityTimeout);
	var from = d3.select("#buttons th.selected");
	var fromID = from.attr("id");
	from.classed("selected", false)
	d3.select("#gameButton").classed("selected", true);
	d3.select("#highscore").select("svg").remove();
	removeAllMarkers(); // force the *ç%&/ to be reset
	document.getElementById("roundDisplay").innerHTML = roundHtml;
	
	if (Object.keys(markers).length == 0){
		setupSelectedCities(guessClick);
	} else {
		if (fromID == "gameButton") return;
		if (fromID == "overviewButton") {
			removeDeselectedCities();
		}
		if (fromID == "scoreButton") {
			addSelectedCities();
		}
		replaceClickCallbackOfSelectedCities();
	}
	if (fromID == "scoreButton"){
		removeAllMistakeMarkers();
		document.getElementById("scoreButton").addEventListener("click", showScore);
	}
	d3.select("#rank svg").remove();
	resetCurrentMarker();
	gameScores = [];
	map.on("zoomend", showHideLabels);
	d3.select("#nextRound").on('click', null);
	d3.select("#bck").style("display", "none");
	d3.select("#gameControl").style("display", "block");
	d3.select("#gameExplanation").style("display", "block");
	d3.select("#key").style("display", "none");
	d3.selectAll("#info").selectAll("div").classed("moveInfoPanelsForStats", false);
	var action = {"action":"get_scenario","ScID":1,"geomIDs":[5000],"limit":3,"orderBy":["-score"], "history":true}
	console.log(JSON.stringify(action));
	lc.sendAndReceive(action, [function(e){
		var msg = e.getMessage();
		console.log(msg);
		bestRank = msg.result.geometry.GeoJSON.geometry.features;
		d3.select("#gameExplanation").on("click", function(){
			d3.select(this).style("display", "none");
			d3.select("#nextRound").on('click', clickNextRound);
			nextRound();
		});
		addLabels();
	}]);
}

var showHideLabels = function(e){
	var zoom = map.getZoom();
	console.log("zoom level: ", zoom);
	if (zoom > 3 && !guessed) d3.select(".leaflet-popup-pane").style("display", "block");
	else d3.select(".leaflet-popup-pane").style("display", "none");
}

var countDown = function(time){
	ti = parseInt(time);
	document.getElementById("time").textContent = displaySeconds(ti);
	if (ti > 0) {
		if (ti < 16) {
			if (ti % 2 == 0) d3.select("#roundDisplay").style("background-color", "white");
			else d3.select("#roundDisplay").style("background-color", "red");
		} else {
			d3.select("#roundDisplay").style("background-color", "white");
		}
		if (!guessed) timeout = window.setTimeout('countDown('+(ti-1)+')', 1000);
	} else nextRound();
};

var displaySeconds = function(t){
	return ("0"+Math.floor(t/60)).substr(-2,2)+":"+("0"+t%60).substr(-2,2);
}

var hideGameControls = function(){
	ti = 0;
	currentRound = 0;
	clearTimeout(timeout);
	d3.select("#bck").style("display", "block");
	d3.select("#gameExplanation").style("display", "none");
	d3.select("#gameControl").style("display", "none");
	d3.select(".leaflet-popup-pane").style("display", "none");
	map.off("zoomend", showHideLabels);
	tickingNextRound = false;
}

var CityNameLabel = L.Class.extend({
    initialize: function (latlng, name) {
//    	console.log(marker);
//    	this._offset = L.latLng(6,6);
        this._latlng = latlng
        this._name = name;
    },
    
    onAdd: function (map) {
        this._map = map;

        // create a DOM element and put it into one of the map panes
        var panel = this._panel = L.DomUtil.create('div', 'cityNameLabel leaflet-zoom-hide');
        var content = L.DomUtil.create('div', 'cityNameLabel-text');
        var cityname = panel.textContent = this._name;
        panel.addEventListener("click", function(){
        	var m = markers[cityname];
        	console.log(cityname, markers);
        	console.log("marker by click: ", m);
        	guessClick(null, m);
        });
//        panel.appendChild(content);
        map.getPanes().popupPane.appendChild(panel);

        // register for map events
        map.on('viewreset', this._reset, this);
        this._reset();
    },

    onRemove: function (map) {
        // remove layer's DOM elements and listeners
        map.getPanes().popupPane.removeChild(this._panel);
        map.off('viewreset', this._reset, this);
    },
    
    _reset: function () {
        // update layer's position
        var pos = this._map.latLngToLayerPoint(this._latlng);
        L.DomUtil.setPosition(this._panel, pos);
    },
});

var allLabels = [];
var addLabels = function(){
	for (var i = 0; i < numSelectedCities; i++){
		var cityname = selectedCityNames[i];
		var city = cities[cityname];
		var loc = city.location;
		var label = new CityNameLabel(new L.latLng(loc.lat, loc.lon), cityname);
		map.addLayer(label);
		allLabels.push(label);
	}
}
