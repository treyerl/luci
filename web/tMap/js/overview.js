var ScID = 0;
var cities;
var cityNames;
var numCities;
var selectedCityNames;
var numSelectedCities;
var imageIndex = 0;
var currentMarker;
var markers = {};
var randomCityTimeout;

var globalFitBoundsOptions = {paddingTopLeft: L.point(50,-50), paddingBottomRight: L.point(50,50), 'maxZoom':4};
var yellow = "#FC0";
var orange = "#F50";

var InfoPanel = (function(){
	var marker;
	var panel;
	var currentContent;
	var minimized;
	var minimizedDisplay;
	var options = {
		showTags: true,
		showNavigation: true,
		minimizedOnCreate: false
	}
	
	InfoPanel = function(m, options){
		marker = m;
		panel = L.DomUtil.create('div', 'opanel-container');
		minimizedDisplay = L.DomUtil.create('div', 'minimizedDisplay');
		d3.select(minimizedDisplay).on("click", this.maximize);
		m._icon.appendChild(panel);
		m._icon.appendChild(minimizedDisplay);
		setOptions(options || {});
	}
	
	function setOptions(o){
		options.showTags = (o.showTags === undefined) ? options.showTags : o.showTags;
		options.showNavigation = (o.showNavigation === undefined) ? options.showNavigation : o.showNavigation;
		options.minimizedOnCreate = (o.minimizedOnCreate === undefined) ? options.minimizedOnCreate : o.minimizedOnCreate ;
	}
	
	InfoPanel.prototype.minimize = function(){
		minimized = true;
		clearTimeout(randomCityTimeout);
		d3.select(panel).style("display", "None");
		d3.select(minimizedDisplay).style("display", "block");
	}
	
	InfoPanel.prototype.maximize = function(){
		minimized = false;
		preventNextRound();

		d3.select(minimizedDisplay).style("display", "None");
		d3.select(panel).style("display", "block");
	}
	
	InfoPanel.prototype.isMinimized = function(){
		return minimized;
	}
	
	InfoPanel.prototype.setContent = function(){
		var content = L.DomUtil.create('div', 'opanel-content');
        if (panel.children.length == 0) {
        	panel.appendChild(content);
        	if (options.minimizedOnCreate) this.minimize();
        	else this.maximize();
        } else {
        	if (options.showNavigation){
        		document.getElementById("prev").removeEventListener("click", selectPreviousImage);
            	document.getElementById("next").removeEventListener("next", selectNextImage);
            	window.removeEventListener("keydown", _keypress);
        	}
        	panel.replaceChild(content, currentContent);
        }
        currentContent = content;
    	var index = marker.index;
    	var geomID = marker.city.images[index];
    	var properties = marker.city.properties[geomID];
    	
    	minimizedDisplay.textContent = "handed in by "+properties.user;
    	
    	var minimizeLabel = L.DomUtil.create('p', 'opanel-minimize');
    	minimizeLabel.textContent = "minimize";
    	d3.select(minimizeLabel).on("click", this.minimize);
    	content.appendChild(minimizeLabel);
    	
    	if (options.showNavigation){
    		var prevNext = "<a href='#' id='prev'>Previous</a> < - - - > <a href='#' id='next'>Next</a>";
        	var header = L.DomUtil.create('p', 'opanel-header');
        	header.innerHTML = prevNext;
        	content.appendChild(header);
        	document.getElementById("prev").onclick = selectPreviousImage;
    		document.getElementById("next").onclick = selectNextImage;
    		window.onkeydown = _keypress; 
    	}
    	
    	if (options.showTags){
    		var tags = "tags: ", c = "";
        	for (var i = 0; i < properties.tags.length; i++){
        		tags += c+properties.tags[i];
        		c = ", ";
        	}
    		var tagElement = L.DomUtil.create('p', 'opanel-tags');
        	tagElement.textContent = tags;
        	content.appendChild(tagElement);
    	}
    	
    	var userline = L.DomUtil.create('p', 'opanel-userline');
    	var taken = !("md.DateTaken" in properties) ? "" : "taken on: "+ properties["md.DateTaken"]+", ";
    	var utext = taken + " user: "+properties.user + (properties.comments.length == 0 ? "": " writes: ");
    	userline.textContent = utext;
    	content.appendChild(userline);
    	
    	var comment = L.DomUtil.create('p', 'opanel-comment');
    	comment.textContent = properties.comments[0];
    	content.appendChild(comment);
	}
	return InfoPanel;
})();

var showImage = function(e){
	var msg = e.getMessage();
//	console.log(msg);
	var img = msg.result.geometry.GeoJSON.geometry.features[0].properties;
	img.url = "/download/"+img.image.streaminfo.checksum+"."+img.image.format;
//	console.log(img.url);
	document.getElementById('bck').src = img.url;
	var taken = "";
	if ("mdDateTaken" in img) taken = "taken on "+img.mdDateTaken;
	window.currentMarker.setPopupContent(window.currentMarker.header+"image "+(imageIndex+1)+"' by '"+img.user+"'"+taken);
};

var loadAllCities = function(callbacks){
	if (cities == undefined){
		var xhr = new XMLHttpRequest();
		xhr.open('GET', "data/allMoocCities.json", true); // parameter 3 = asynchronous
		xhr.responseType = 'json'
		xhr.onload = function(e) {
			var data = xhr.response;
			ScID = data.ScID;
			delete data.ScID;
			selectedCityNames = data.selectedCities;
			delete data.selectedCities;
			
			cities = data;
			cityNames = Object.keys(data);
			numCities = cityNames.length;
			numSelectedCities = selectedCityNames.length;
			for (var i = 0; i < callbacks.length; i++){
				callbacks[i]();
			}
		};
		xhr.send();
	}
}

var resetCurrentMarker = function(){
	if (currentMarker !== undefined){
		var cm = currentMarker;
		if ("overpanel" in cm){
//			map.removeLayer(cm.overpanel);
			delete cm.overpanel;
		}
		cm.setIcon(getIcon(cm.selected));
		cm.on('click', setupSelectedCity);
		cm.setZIndexOffset(0);
	}
}

var setupSelectedCity = function(e, marker, imageIndex, mini){
//	console.log("setupSelectedCity");
	resetCurrentMarker();
	if (e != null) {
		console.log(e);
		clearTimeout(randomCityTimeout);
	}
	var cm = currentMarker = (marker === undefined ? this: marker);
	var ii = (imageIndex === undefined) ? 0: imageIndex;
	var minimized = (mini == undefined) ? false : mini;
	var city = cm.city;
	
	// create d3/svg icon
	var titleHeight = 24;
	var dy = 40;
	var cx = dy / 2; //yes, dy!
	var cy = dy / 2 + titleHeight;
	var r  = cx - 4;
	var ri = r*0.5;
	var di = 2*ri;
	var ai = di + 4;
	var dx = dy + ai * city.images.length;
	var height = dy + titleHeight;
	var width = 0;
	
	var divIcon = L.divIcon({
		className: 'currentMarker',
	    iconSize: L.point(dx, height),
	    iconAnchor: L.point(cx, cy)
	});
	cm.setIcon(divIcon);
	
	var id = city.name.replace(/\s/g, "_") + "_";
	var c = cm.selected ? "black": "white";
	var s = cm.selected ? "white": "black";
	var dcm = d3.select(cm._icon);
	var svg = dcm.append("svg").attr("height", height).attr("class", "svgMarker");
	var g = svg.append("g");
	g.append("circle")
		.attr("r", r).attr("cx", cx).attr("cy", cy)
		.style("fill",c).style("stroke", s).style("stroke-width", 3);
	
	g = svg.append("g");
	g.append("text")
		.attr("id", id+"text")
		.attr("x", dy + 2)
		.attr("y", titleHeight - 8)
		.style("font-weight", "bold")
		.style("font-size", "15")
		.text(city.name);
	g.append("text")
		.attr("x", dy + 2)
		.attr("y", titleHeight +1)
		.style("font-size", 8)
		.text(city.country);
	
	var textWidth = 0;
	var textHeight = 0;
	g.selectAll("text").each(function() {
		var box = this.getBBox();
	    textWidth = Math.max(box.width, textWidth);
	    textHeight += box.height;
	});
	g.insert("rect", "#"+id+"text")
		.attr("x", dy + 1)
		.attr("rx", 3)
		.attr("ry", 3)
		.attr("y", 1 )
		.attr("width", textWidth + 2)
		.attr("height", textHeight  )
		.style("fill", "white");
	svg.attr("width", Math.max(dy + 5 + textWidth, dx));
	
	var cl = city.images.length;
	for (var i = 0; i < cl; i++){
		var selected = false;
		if (i == 0) selected = true;
		var imgID = city.images[i];
		var ccx = dy - 2 - ri + (i+1) * (di + 4);
		var ccy = titleHeight + ri + 6;
		var cg = svg.append("g")
			.attr("id", "c"+imgID)
			.attr("i", i)
			.classed("selected", selected)
			.on("click", function(){
				preventNextRound();
				clearTimeout(randomCityTimeout);
				selectImage(this.getAttribute("i"));
			});
		var crc = cg.append("circle")
			.attr("r", ri).attr("cx", ccx).attr("cy", ccy)
			.style("stroke", "white")
			.style("fill", yellow);
		if(cl > 1){
			cg.append("text")
			.attr("x", ccx)
			.attr("y", ccy+3)
			.attr("text-decoration", "underline")
			.style("text-anchor", "middle")
			.style("font-size", 8)
			.text(i+1);
		}
		

		if (cm.selected) {
			if (city.selectedImages.indexOf(imgID) != -1) {
				crc.style("stroke","black").attr("r", ri );
			}
		}
	}
	fetchImageURLs(city, (function(ii, minimized){
		var callback = function(){
			setupPanel(ii, minimized);
		}
		return callback;
	})(ii, minimized))
	cm.off('click'); //!! gives you quite some troubles if you forget this!
	cm.setZIndexOffset(1000);
//	cm.bindPopup("");
}

var fetchImageURLs = function(city, callback){
	var action = {"action":"get_scenario","ScID":ScID,"geomIDs":city.images,"withAttachments":false};
	// load image
	if (!("imageURLs" in city)) {
		lc.sendAndReceive(action, [function(e){
//			console.log("fetched");
			var msg = e.getMessage();
			if (!("error" in msg)){
				var features = msg.result.geometry.GeoJSON.geometry.features;
				city.imageURLs = {};
				city.properties = {};
				for (var i = 0; i < features.length; i++){
					var the = features[i].properties;
					var url = "/download/"+the.image.streaminfo.checksum+"."+the.image.format;
					city.imageURLs[the.geomID] = url;
					city.properties[the.geomID] = the;
				}
				callback();
			} else {
				if ("error" in msg) throw new Error(msg.error);
				else console.log(msg);
			}
		}]);
	} else {
		callback();
	}
	
}

var setupPanel = function(index, minimized){
	var cm = currentMarker;
	var mini = (minimized === undefined) ? false : minimized;
	cm.infopanel = new InfoPanel(cm, {minimizedOnCreate: mini});
//	map.addLayer(cm.overpanel)
	selectImage(index);
}

var selectImage = function(index){
	var cm = currentMarker;
	cm.index = parseInt(index);
	var geomID = cm.city.images[index];
	d3.selectAll(".svgMarker .selected").classed("selected", false).style("stroke-width", 1);
	d3.select("#c"+geomID).classed("selected", true).style("stroke-width", 3);
	document.getElementById('bck').src = cm.city.imageURLs[geomID];
	cm.infopanel.setContent();
}

var selectPreviousImage = function(){
	selectImage(Math.max(currentMarker.index - 1, 0));
}

var selectNextImage = function(){
	selectImage(Math.min(currentMarker.index + 1, currentMarker.city.images.length -1));
}

var _keypress = function(e){
	var k = e.keyCode;
	if (k == 37) selectPreviousImage();
	if (k == 39) selectNextImage();
}

var createMarker = function(city, isSelected){
	var loc = city.location;
	var cm = L.marker(new L.LatLng(loc.lat, loc.lon), {icon: getIcon(isSelected), opacity: 0.8}).addTo(map);
	cm.city = city;
	city.marker = cm;
	cm.selected = isSelected;
	cm.off('click');
	return cm
}

var setupOverview = function(){
	var from = d3.select("#buttons th.selected");
	var fromID = from.attr("id");
	from.classed("selected", false)
	d3.select("#key").style("display", "block");
	d3.select("#highscore").select("svg").remove();
	d3.select("#overviewButton").classed("selected", true);
	if (fromID == "scoreButton") {
		d3.select("#rank svg").remove();
		d3.select("#map").style("top", "0px");
		map.fire("resize");
		restoreAllCities();
		removeAllMistakeMarkers();
		document.getElementById("scoreButton").addEventListener("click", showScore);
	} else if (fromID == "gameButton"){
		restoreDeselectedCities();
		hideGameControls();
		resetCurrentMarker();
		removePolylines();
		if (answerMarker !== undefined) map.removeLayer(answerMarker);
	}
	resetMap();
	d3.selectAll("#info").selectAll("div").classed("moveInfoPanelsForStats", false);
	for (var index in markers) {
		markers[index].off("click").on("click", setupSelectedCity);
	}
	for (var i = 0; i < numCities; i++){
		var cityname = cityNames[i];
		var city = cities[cityname];
		try {
			city.name = cityname;
		} catch (e){
			console.log(cityname);
		}
		if ("location" in city){
			if (!(cityname in markers)){
				var loc = city["location"];
				var imgCount = city.images.length;
				var cm = createMarker(city, "selectedImages" in city);
				cm.on('click', setupSelectedCity);
				markers[cityname] = cm;
			}
		} else {
			console.log("has no location: ", city);
		}
	}
	// load a random city
//	setupSelectedCity(null, markers[cityNames[Math.round(Math.random()*numCities)]]);
	// load Zurich
	setupSelectedCity(null, markers["Zurich"]);
	randomCityTimeout = window.setTimeout(randomCity, 30000);
}

var randomCityTimeout;
var randomCity = function(){
	console.log("randomCity")
	setupSelectedCity(null, markers[cityNames[Math.round(Math.random()*cityNames.length)]]);
	randomCityTimeout = window.setTimeout(randomCity, 8000);
}

var getIcon = function(isSelected){
	if (isSelected){
		return L.icon({
			iconUrl: "img/selected.png",
		    iconRetinaUrl: "img/selected@2x.png",
		    iconSize: [26, 26],
		    iconAnchor: [7, 7],
		});
	} else {
		return L.icon({
			iconUrl: "img/deselected.png",
		    iconRetinaUrl: "img/deselected@2x.png",
		    iconSize: [22, 22],
		    iconAnchor: [5, 5],
		});
	}
}