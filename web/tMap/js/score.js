var ratios = [];
var ratiosByCity = {};
var scoreMarkers = {};
var mistakeMarkers = [];
var gameMarkers = [];

var showScore = function(){
	clearTimeout(randomCityTimeout);
	var from = d3.select("#buttons th.selected");
	var fromID = from.attr("id");
	from.classed("selected", false)
	d3.select("#scoreButton").classed("selected", true);
	
	document.getElementById("scoreButton").removeEventListener("click", showScore);
	d3.select("#key").style("display", "none");
	for (var index in markers) {
		var m = markers[index];
		map.removeLayer(m);
		m.off("click");
	}
	clearTimeouts();
//	d3.select("#map").style("top", "240px");
//	map.fire("resize");
	if (fromID == "gameButton"){
		hideGameControls();
		resetCurrentMarker();
		removePolylines();
		if (gameScores.length > 0) d3.select("#gameDone").style("display", "block");
	}
	if (answerMarker !== undefined) map.removeLayer(answerMarker);
	removeAllLabels();
	d3.selectAll("#info").selectAll("div").classed("moveInfoPanelsForStats", true);
	
	d3.selectAll("#highscore g[city]").on("click", function(e){
		var cityname = this.getAttribute("city");
		var imgID = parseInt(this.getAttribute("imgID"));
		var i = ratiosByCity[cityname];
		console.log("barIndex", cityname, ratiosByCity, i, imgID);
		selectBar(null, i, imgID);
	}).style("cursor", "pointer");
	
	var action = {"action":"run","service":{"classname":"RankImages",
		"inputs":{'ScID':ScID,'byCity':true}}};
// 		console.log("luci.send(",action,")");
	
	// load image
	lc.sendAndReceive(action, [function(e){
		var msg = e.getMessage();
		ratios = msg.result.outputs.ratios;
		gameMarkers = [];
		for (var i = 0; i < ratios.length; i++){
			var cityname = ratios[i].city;
			ratiosByCity[cityname] = i;
			gameMarkers.push(markers[cityname]);
		}
		
		if (gameScores.length > 0){
			var properties = {"geomID":5000, "score": avgScore};
			var geometry = {"type":"Feature","properties":properties};
			action = {"action":"update_scenario","ScID":ScID,"geometry":{"score":{"format":"GeoJSON","geometry":geometry}}};
			console.log("update player scores to db", JSON.stringify(action));
			lc.sendAndReceive(action, [function(e){
				var msg = e.getMessage();
//				console.log(msg);
				if ("error" in msg) alert(msg);
				else {
					var action = {'action':'run','service':{'classname':'HistoricRelation',
						'inputs':{'ScID':ScID,'relevantAttributes':['score'], 'geomIDs':[5000]}}}
					lc.sendAndReceive(action, [function(e){
						var msg = e.getMessage();
						if ("error" in msg) alert(msg);
						else {
							var gt_lt = msg.result.outputs.geomIDs[0].score;
							var playerScore = d3.select("#playerScore");
							var numKids = 0, numBars = 0;
							var yourRank = 1;
							if (gt_lt !== undefined){
								numKids = playerScore[0][0].childElementCount;
								numBars = (numKids == 3) ? 1: (numKids == 5) ? 2 : 3;
								yourRank = gt_lt[0]+1 ;
							}
							var yourBar = "#t3", yourBarBack = "#r3";
							if (yourRank < 3) {
								yourBar = "#t"+yourRank;
								yourBarBack = "#r"+yourRank;
								if (numBars < 3){
									var yourScore = {'properties':{'score':avgScore, 'timestamp': new Date().getTime()}};
									var i = yourRank - 1;
									bestRank.splice(i,0,yourScore);
									drawPlayerScoreBar(i);
								} else {
									d3.select("#t3").text("3"+d3.select("#t2").text().substr(1));
									if (yourRank < 2){
										d3.select("#t2").text("2"+d3.select("#t1").text().substr(1));
									}
								}
							}
							playerScore.select(yourBar).text(yourRank+" - YOU - "+Math.round((1-avgScore)*HEC/1000)+" km");
							playerScore.select(yourBarBack).style("fill","red");
							setupRatios();
							
						}
						var first = gameScores[0];
//						console.log("first",first);
						var i = ratiosByCity[first.city];
						selectBar(null, i, first.imgID);
					}]);
				}
			}]);
			d3.select("#gameDone").on("click", function(){
				d3.select(this).style("display", "none");
			});
		} else {
			setupRatios();
			selectBar(null, 0); //select the first bar
			
		}
	}]);
	
}

var setupRatios = function(){
	var intro = 188;
	var yAxisWidth = 40;
	var fullWidth = 1550;
    var width = fullWidth - intro - yAxisWidth;
    var height = 110;
    var barHeight = 80;
	var numBars = 0;
	for (var i = 0; i < ratios.length; i++) {
		if (ratios[i].numGuesses > 0) numBars++;
	}
	var numRatios = 120;
	var dynWidth = intro + yAxisWidth + numBars * 12 - 30;
	console.log("numBars", numBars);
//	var fullWidth = 60 + numBars * 12;
	var x = d3.scale.ordinal()
	    .rangeRoundBands([0, width+140], 0.2)
	    .domain(d3.range(numRatios));

	var y = d3.scale.linear()
	    .range([barHeight, 0])
	    .domain([0,1]);

	var xAxis = d3.svg.axis()
		.scale(x)
		.orient("bottom");

	var yAxis = d3.svg.axis()
	    .scale(y)
	    .orient("left")
	    .ticks(5, "%");

	var svg = d3.select("#rank").append("svg")
	    .attr("width", dynWidth)
	    .attr("height", height);
	
	svg.append("rect")
		.attr("width", dynWidth)
		.attr("height", height)
		.attr("x", 0)
		.attr("y", 0)
		.attr("rx", 5)
		.attr("ry", 5)
		.style("fill", "white")
		.style("fill-opacity", 0.8)
		.style("stroke", "white");
	
	g = svg.append("g");
	g.append("text")
		.attr("x", 5)
		.attr("y", 52)
		.style("font-size", 60)
		.style("font-weight", "bold")
		.style("fill", yellow)
		.style("fill-opacity", 0.1)
		.style("stroke", "grey")
		.style("stroke-width", "0.5px")
		.text("CITY");
	g.append("text")
		.attr("x", 5)
		.attr("y", 88)
		.style("font-size", 39)
		.style("font-weight", "bold")
		.style("fill", yellow)
		.style("fill-opacity", 0.1)
		.style("stroke", "grey")
		.style("stroke-width", "0.5px")
		.text("SCORE");
	
	svg.append("g")
		.attr("class", "y axis")
		.attr("transform", "translate("+(intro+5)+",10)")
		.call(yAxis)
	.append("text")
		.attr("transform", "rotate(-90)")
		.attr("x", 0)
		.attr("y", 2)
		.attr("dy", ".71em")
		.style("text-anchor", "end")
		.text("uniqueness");
	
	svg.append("g").append("text")
		.attr("class", "x axis")
		.attr("y", barHeight+24)
		.attr("x", 110)
		.text("number of guesses:");
	
	var i = 0, j = 0, h = 0;
	var d ;
	var bar = svg.selectAll("g").data(ratios).enter().append("g").filter(function() {return ratios[h++].numGuesses > 0.0 }) 
		.attr("width", x.rangeBand())
		.attr("class", "bar")
		.attr("i", function(){return i++;})
		.attr("transform", function(d) { return "translate(" + (intro + x(j++) + 10) + ",0)"; })
		.on("click", selectBar);
	i = j = 0;
	bar.append("rect")
		.attr("width", x.rangeBand())
		.attr("y", function() { return y(ratios[j++].ratio) + 10;})
		.attr("height", function(){
			var d = ratios[i++];
			if (d.ratio == 0.0) return 18;
		  	else return y(1-d.ratio);
		})
	i = 0;
	bar.append("text")
		.attr("transform", "rotate(-90)")
		.attr("y", function(d){return x.rangeBand() * 0.9;})
	  	.attr("x", -barHeight-5)
	  	.attr("font-size", "8")
	  	.attr("opacity", 0.5)
		.text(function(){return ratios[i++].city;});
	i = 0;
	bar.append("text")
		.attr("transform", "rotate(-90)")
		.attr("y", 8)
		.attr("x", -barHeight-18)
		.style("text-anchor", "end")
		.style("font-size", 8)
		.text(function(){return ratios[i++].numGuesses});
}

var removeAllMistakeMarkers = function(){
	for (var index in mistakeMarkers) map.removeLayer(mistakeMarkers[index]);
}

var selectBar = function(e, i, imgID){
	if (imgID == 0) imgID = null;
//	console.log("selectBar", e, i, imgID);
//	console.log(arguments);
	d3.selectAll("g.selected").classed("selected", false);
	for (var index in scoreMarkers) map.removeLayer(scoreMarkers[index]);
	removeAllMistakeMarkers();
	
	d = (i === undefined) ? d3.select(this) : d3.select(".bar[i='"+i+"']");
	d.attr("class", d.attr("class")+" selected");
	currentRatio = ratios[d.attr("i")];
	var city = cities[currentRatio.city];
//	if (imgID == undefined) imgID = city.selectedImages[0];
//	console.log(d, imgID);
	fetchImageURLs(city, function(){
		var cityname = currentRatio.city
		var city = cities[cityname];
//		console.log("fetched", city);
		
		var loc = city.location;
		// create a mapbox marker
		var divIcon = L.divIcon({
			className: 'svgMarker',
		    iconSize: L.point(300,300),
		    iconAnchor: L.point(150,150)
		});
//		hideCities();
		var cm = currentMarker = markers[cityname].setIcon(divIcon).addTo(map);
		cm.off("click");
		cm.setZIndexOffset(0);
//		console.log("imgID", imgID);
		cm.city = city;
		scoreMarkers[cityname] = cm;
		createSVGIcon(imgID);
		map.fitBounds(L.featureGroup([cm]).getBounds(), {'maxZoom':3});
	});
}
var createSVGIcon = function(selectedImgID){
//	console.log("selectedImgID", selectedImgID);
	var allGuesses = 0;
	var a = -Math.PI/4;
	var inter = d3.interpolateRound(15,35);
	var mainR = 60;
	var mainX = 150;
	var mainY = 150;
	var svg = d3.select(".svgMarker").append("svg")
	.attr("width", 300)
	.attr("height", 300);
	var mainG = svg.append("g").attr("id", "mainG");
//	mainG.on("click", showCities);
	mainG.append('circle')
    .attr('cx', mainX)
    .attr('cy', mainY)
    .attr('r', mainR)
    .style("stroke-opacity", 1)
    .style("stroke", "#888")
    .attr('fill', '#FC0')
//		.attr('fill', '#CCC');
//	var bckImageSet = false;
	var city = cities[currentRatio.city];
	var hasSelectedImgID = selectedImgID != undefined;
	var selectedG;
	for (var i = 0, ai = 0; i < city.selectedImages.length; i++){
		var j = i+1;
		var imgID = city.selectedImages[i];
//		console.log("selectedImgID", j, selectedImgID, imgID);
		var img = city.properties[imgID];
		var self = ("self" in img) ? img.self : 0;
		var others = ("others" in img) ? img.others : 0;
		var ratio = (self + others > 0 ) ? self / (self + others) : 0;
		var guesses = self + others
		if (!hasSelectedImgID) selectedImgID = imgID;
		allGuesses += guesses;
		if (guesses > 0 ){
			var radius = inter(ratio);
			var g = svg.append('g');
//					.attr("transform", function(d) { return "translate(" + 100*i + ",0)"; })
			g.attr("geomID", img.geomID);
			g.attr("index", i);
			g.attr("ratio", ratio);
			if (selectedG == null && selectedImgID == imgID){
				g.attr("class", "c"+j+" selected");
				document.getElementById('bck').src = city.imageURLs[imgID];
				selectedG = g;
//				bckImageSet = true;
			} else {
//				selectedImgID = 0;
				g.attr("class", "c"+j);
			}
			
			var angle = a+ (Math.PI/4)*ai;
//				console.log(angle);
			var cx = mainX + Math.cos(angle) * (mainR + radius);
			var cy = mainY + Math.sin(angle) * (mainR + radius);
			var circle1 = g.append('circle')
		    .attr('cx', cx)
		    .attr('cy', cy)
		    .attr('r', radius)
		    .attr('fill', '#FC0');
			var num = g.append('text')
			.attr("x", cx)
			.attr("y", cy+10)
			.style("text-anchor", "middle")
			.style("font-size", "8")
			.text("# "+guesses);
			var percentage = g.append('text')
			.attr("x", cx)
			.attr("y", cy)
			.style("text-anchor", "middle")
			.style("font-weight", "bold")
			.text(Math.round(ratio*100)+"%")
			g.on("click", clickImage);
			
			ai++;
		} //else selectedImgID = 0;
	}
	mainG.append('text')
	.attr("class", "info")
	.attr("x", 152)
	.attr("y", 158)
	.style("text-anchor", "middle")
	.style("font-weight", "bold")
	.style("font-size", "23")
	.text(Math.round(currentRatio.ratio*100) + "%");
	mainG.append('text')
	.attr("x", 150)
	.attr("y", 133)
	.style("font-size", "15")
	.style("text-anchor", "middle")
	.style("font-weight", "bold")
	.text(currentRatio.city);
	mainG.append('text')
	.attr("class", "info")
	.attr("x", 150)
	.attr("y", 175)
	.style("text-anchor", "middle")
	.text("# guesses: "+ allGuesses);
	
	var delayClick = function(){
		clickImage(selectedG);
	}
	
	if (selectedG != null) setTimeout(delayClick, 1500);
	else throw new Error("no image could be selected!");
}

var hideInfo = function(){
	var mainG = d3.select("#mainG");
	mainG.select("circle").style("fill-opacity", 0.5);
	mainG.selectAll(".info").style("opacity", 0);
	mainG.append("circle").attr("id", "smallmarker").attr("r", 5).attr("cx", 150).attr("cy", 150).style("fill", "#F50");
//	mainG.on("click", null);
	mainG.on('click', showInfo);
}

var showInfo = function(){
	var mainG = d3.select("#mainG");
	d3.select(".svgMarker .selected").on("click", clickImage);
	mainG.select("circle").style("fill-opacity", 1);
	mainG.selectAll(".info").style("opacity", 1);
	mainG.select("#smallmarker").remove();
	mainG.on("click", null);
//	mainG.on('click', showCities);
	removeAllMistakeMarkers();
	currentMarker.setZIndexOffset(0);
}

//var showCities = function(){
//	console.log("showCities");
//	if (allLabels.length == 0) addLabels();
//	map.removeLayer(currentMarker);
//	for (var i = 0; i < gameMarkers.length; i++) {
//		var m = gameMarkers[i];
//		map.addLayer(m);
//		m.on("click", function(){selectBar(null, ratiosByCity[this.city.name], this.city.selectedImages[0])});
//	}
//	map.on("zoomend", showHideLabels);
//}

//var hideCities = function(){
//	for (var i = 0; i < gameMarkers.length; i++) map.removeLayer(gameMarkers[i]);
//	map.off("zoomend", showHideLabels);
//	d3.select(".leaflet-popup-pane").style("display", "none");
//}

var removeAllMistakeMarkers = function(){
	for (var index in mistakeMarkers) map.removeLayer(mistakeMarkers[index]);
	mistakeMarkers = [];
}

var clickImage = function(d){
	console.log("clickImage", last, arguments);
	removeAllMistakeMarkers();
	console.log("this", this, "d", d);
	if (d === undefined){
		d = d3.select(this);
		var last = d3.select(".svgMarker .selected");
		last.classed("selected", false);
		if (last.on("click") === undefined) last.on("click", clickImage);
	}
//	console.log("d", d);
//	console.log("geomID", d.attr("geomID"));
	var maincityname = currentRatio.city;
	var city = cities[maincityname];
	var the = city.properties[parseInt(d.attr("geomID"))]
	var ratio = parseInt(d.attr("ratio"));
//	console.log(the, city);
	document.getElementById('bck').src = "/download/"+the.image.streaminfo.checksum+"."+the.image.format;
	d.classed("selected", true);
	d.on("click", null);
//	console.log("mistakes", the.mistakes, city.properties);
	if (the.mistakes !== undefined){
		hideInfo();
//		console.log("bo")
		var inter = d3.interpolateRound(15,28);
		if (the.others === undefined || the.others == 0) return;
//		console.log(mistakeMarkers, scoreMarkers[cityname], cityname);
		for(var cityname in the.mistakes){
//			console.log(cityname, cities[cityname]);
			var rat = the.mistakes[cityname]/the.others;
			var loc = cities[cityname].location;
			var dx = 100;
			var dy = 100;
			var cx = dx / 2;
			var cy = dy / 2;
			var divIcon = L.divIcon({
				className: 'mistakeMarker',
			    iconSize: L.point(dx, dy),
			    iconAnchor: L.point(cx, cy)
			});
			var cm = L.marker(new L.LatLng(loc.lat, loc.lon), {
		    	icon: divIcon,
		    	'opacity': 0.8
			}).addTo(map);
//			console.log("divIcon", divIcon);
			var g = d3.select(cm._icon).append("svg").append("g");
			g.append("circle")
				.attr("r", 33).attr("cx", cx).attr("cy", cy)
				.style("fill", "white")
				.style("fill-opacity", 0.6)
				.style("stroke", "red")
				.style("stroke-opacity", "0.5")
				.style("stroke-width", "4px");
			g.append("circle")
				.attr("r", inter(rat)).attr("cx", cx).attr("cy", cy)
				.style("fill","#75A").style("opacity",0.5);
			g.append("text")
				.attr("x", cx).attr("y",cy-10)
//				.style("font-weight", "bold")
				.style("text-anchor", "middle")
				.text(Math.round(rat*100) + "%");
			g.append("text")
			.attr("x", cx).attr("y",cy)
//			.style("font-weight", "bold")
			.style("text-anchor", "middle")
			.style("font-size", "8")
			.text("mistaken with");
			g.append("text")
				.attr("x", cx).attr("y",cy+12)
				.style("font-weight", "bold")
				.style("text-anchor", "middle")
				.text(cityname);
			mistakeMarkers.push(cm);
		}
		currentMarker.setZIndexOffset(1000);
		var group = new L.featureGroup(mistakeMarkers);
		group.addLayer(scoreMarkers[maincityname]);
		map.fitBounds(group.getBounds(), globalFitBoundsOptions);
	} else {
		if (ratio < 1) throw new Error("no mistakes!");
		else showInfo();
	}
}

var restoreAllCities = function(){
	var cnl = cityNames.length;
	for (var i = 0; i < cnl; i++){
		map.addLayer(cities[cityNames[i]].marker);
	}
}
