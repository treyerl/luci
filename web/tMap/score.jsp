<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>tMap</title>
<script type="text/javascript" src="../js/luciConnect.js"></script>
<script type="text/javascript" src="../js/faultylabs.md5.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.5/d3.min.js"></script>
<script src='https://api.tiles.mapbox.com/mapbox.js/v2.2.1/mapbox.js'></script>
<link href='https://api.tiles.mapbox.com/mapbox.js/v2.2.1/mapbox.css' rel='stylesheet' />
<style>
  body { margin:0; padding:0; background-color: #eee }
   #bck { position:absolute; top:0; bottom:0; left:0; right:0; z-index:1; height:auto; max-height:100%; margin: 0 auto; max-width:1920px; display: block; } 
/*   #imgContainer { position:absolute; top:0; bottom:0; z-index:1; height:100%; width:100% } */
  #map { position:absolute; top:180px; bottom:0; z-index:3; width:100%;}
  #rank { position:absolute; top:0; height: 180px; z-index:4;}
  .leaflet-container {background: transparent;}
  p, h1 {margin: 50px; position: absolute; top:0; left:0; z-index: 4;}
  p {background: white; padding: 3px; font-family: Helvetica,Arial,Sans-Serif}
 #rank {
    background-image: url('gradient_0.5.png');
    background-repeat: repeat-x;
    background-position: top right;
    background-opacity: 0.4;
}
  .bar rect{
  fill: white;
/*   stroke: #ccc; */
  fill-opacity: 0.9;
  stroke-opacity: 0.5;
  shape-rendering: crispEdges;
}
.bar {
	cursor: pointer;
}

.bar rect {
	stroke: black;
	stroke-width: 1px;
}
.bar:hover rect, .bar:hover text{
  fill: #FC0;
}
.bar:hover text{
	font-size: 12px;
	opacity: 1;
	fill:#333;
}

.bar text {
  font-family: Helvetica;
  text-shadow: 0 0 3px white;
}

.bar[height="10"]{
  fill-opacity: 0.2;
  stroke-opacity: 0.5;
}

.selected rect, .selected circle {
	fill: #F50;
}

.axis {
  font: 10px sans-serif;
}

.axis path,
.axis line {
  fill: none;
  stroke: #000;
  shape-rendering: crispEdges;
}

.x.axis path {
  display: none;
}
.leaflet-div-icon {
	background: None;
	border: 0px solid black;
}
</style>
</head>
<body>
<div id='map'></div>
<img id='bck' src="" alt="" />
<div id='rank'></div>

<script>
	// http://blog.webkid.io/replacing-jquery-with-d3
	
	
	// Provide your access token
	L.mapbox.accessToken = 'pk.eyJ1IjoidHJleWVybCIsImEiOiJiY2ExZTQ4NTNmYTA2MDRjNTdjZDQ0ZDZkZjU0MzhjNCJ9.nGBCvtCwkSs1NzOE6DQnRA';
	// Create a map in the div #map
	var map = L.mapbox.map('map', 'treyerl.1c1947cc').setView([40, 8.50], 2);
	
	var currentMarker;
	var currentRatio;
	var ScID;
	var cities;
	var cityNames;
	var numCities;
	var theCityName;
	var theCity;
	var theImageKey;
	var theImgID;
	var the;
	var markers = [];
	var ratios = [];
	var cityImages;
	var hasPopup = false;
	
	var step1 = function(e){
// 		console.log("luci.onauthenticated");
		var xhr = new XMLHttpRequest();
		xhr.open('GET', "data/moocCities.json", true); // parameter 3 = asynchronous
		xhr.responseType = 'json'
		xhr.onload = function(e) {
// 			console.log("xhr.onload")
			var data = xhr.response;
			ScID = data.ScID;
			delete data.ScID;
// 			DBG_imageCheck(data);
			cities = data;
			cityNames = Object.keys(data);
			numCities = cityNames.length;
			show_score();
		};
		xhr.send();
	}
	
	var show_score = function(){
// 		console.log("setup");
		for (var index in markers) map.removeLayer(markers[index]);
		var action = {"action":"run","service":{"classname":"RankImages",
			"inputs":{'ScID':ScID,'byCity':true}}};
// 		console.log("luci.send(",action,")");

		// load image
		lc.sendAndReceive(action, [function(e){
// 			console.log("luci.onreceive image rank");
			var msg = e.getMessage();
			ratios = msg.result.outputs.ratios;
// 			console.log(ratios);
			setupRatios();
// 			the = msg.result.geometry.GeoJSON.geometry.features[0].properties;
// 			document.getElementById('bck').src = "/download/"+the.image.streaminfo.checksum+"."+the.image.format;
		}]);
	}
	
	var DBG_imageCheck = function(data){
		for (var cityname in data){
			var city = data[cityname];
			if(city.images.length == 0) console.log(cityname);
		}
	}
	
	var step0 = function(e){
// 		console.log("websocket.onopen");
// 		lc.on("onmessage", function(e){console.log("message: ",e.getMessage());});
		window.setTimeout('lc.authenticate("lukas", "1234", step1)',100);
		
	};
	
	var lc;
	window.onload = function(){
// 		console.log("window.onload");
		lc = new LuciClient(location.hostname, 8080, "ws/", step0);
		lc.downloadAttachments = false;
	}
	
	var setupRatios = function(){
		var margin = {top: 20, right: 20, bottom: 30, left: 40},
	    width = 1780 - margin.left - margin.right,
	    height = 180 - margin.top - margin.bottom;
// 		console.log(width);
		var x = d3.scale.ordinal()
		    .rangeRoundBands([0, width], 0.2)
		    .domain(d3.range(ratios.length));
	
		var y = d3.scale.linear()
		    .range([height, 0])
		    .domain([0,1]);
	
		var xAxis = d3.svg.axis()
			.scale(x)
			.orient("bottom");
	
		var yAxis = d3.svg.axis()
		    .scale(y)
		    .orient("left")
		    .ticks(5, "%");
	
		var svg = d3.select("#rank").append("svg")
		    .attr("width", width + margin.left + margin.right)
		    .attr("height", height + margin.top + margin.bottom )
		  .append("g")
		    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
		
		svg.append("g")
			.attr("class", "y axis")
			.call(yAxis)
		.append("text")
			.attr("transform", "rotate(-90)")
			.attr("y", 6)
			.attr("dy", ".71em")
			.style("text-anchor", "end")
			.text("correct guesses");
		
		svg.append("g").append("text")
			.attr("class", "x axis")
			.attr("y", height+14)
			.attr("x", -30)
			.text("exposure:");
		
		var i = 0, j = 0;
		var d ;
		var bar = svg.selectAll("g").data(ratios).enter().append("g")
			.attr("width", x.rangeBand())
			.attr("class", "bar")
			.attr("i", function(){return i++;})
			.attr("transform", function(d) { return "translate(" + x(j++) + ",0)"; })
			.on("click", function(e){
				d3.selectAll(".selected").classed("selected", false);
				for (var index in markers) map.removeLayer(markers[index]);
				d = d3.select(this);
				d.attr("class", d.attr("class")+" selected");
				currentRatio = ratios[d.attr("i")];
// 				console.log("currentRatio", currentRatio);
				
				var action = {"action":"get_scenario","ScID":ScID,"geomIDs":cities[currentRatio.city].images,"withAttachments":false};
				// load city
				lc.sendAndReceive(action, [function(e){
// 					console.log("luci.onreceive current city");
					var msg = e.getMessage();
// 					console.log("currentRatio", currentRatio);
					cityImages = msg.result.geometry.GeoJSON.geometry.features;
					var cityname = currentRatio.city
					var city = cities[cityname];
					
					var loc = city["location"];
					// create a mapbox marker
					var divIcon = L.divIcon({
						className: 'svgMarker',
					    iconSize: L.point(300,300),
					    iconAnchor: L.point(150,150)
					});
					var cm = L.marker(new L.LatLng(loc.lat, loc.lon), {
				    	icon: divIcon,
				    	'opacity': 0.8
					}).addTo(map);
					createSVGIcon();
					cm.cityname = currentRatio.city;
					cm.city = city;
					markers.push(cm);
					currentMarker = cm;
					map.fitBounds(L.featureGroup([cm]).getBounds(), {'maxZoom':3});
					
					// select the first image
					the = cityImages[0].properties;
// 					console.log("mistakes", the.mistakes, the.mistakes !== undefined);
					document.getElementById('bck').src = "/download/"+the.image.streaminfo.checksum+"."+the.image.format;

				}]);
			});
		i = j = 0;
		bar.append("rect")
			.attr("width", x.rangeBand())
			.attr("y", function() { return y(ratios[j++].ratio);})
			.attr("height", function(){
				var d = ratios[i++];
				if (d.ratio == 0.0) return 18;
			  	else return y(1-d.ratio);
			})
		i = 0;
		bar.append("text")
			.attr("transform", "rotate(-90)")
			.attr("y", function(d){return x.rangeBand() / 6 * 5;})
		  	.attr("x", -height+15)
		  	.attr("font-size", "8")
		  	.attr("opacity", 0.5)
			.text(function(){return ratios[i++].city;});
		i = 0;
		bar.append("text")
			.attr("transform", "rotate(-90)")
			.attr("y", 9)
			.attr("x", -height-6)
			.style("text-anchor", "end")
			.style("font-size", 8)
			.text(function(){return ratios[i++].numGuesses});
	}
	
	var createSVGIcon = function(){
		
		var allGuesses = 0;
		var a = -Math.PI/4;
		var inter = d3.interpolateRound(15,35);
		var mainR = 60;
		var mainX = 150;
		var mainY = 150;
		var svg = d3.select(".svgMarker").append("svg")
		.attr("width", 300)
		.attr("height", 300);
		var mainG = svg.append("g").attr("id", "mainG");
		mainG.append('circle')
	    .attr('cx', mainX)
	    .attr('cy', mainY)
	    .attr('r', mainR)
	    .style("stroke-opacity", 1)
	    .style("stroke", "#888")
	    .attr('fill', '#FC0');
// 		.attr('fill', '#CCC');
		
		for (var i in cityImages){
			var j = parseInt(i)+1;
			var img = cityImages[i].properties;
			var self = ("self" in img) ? img.self : 0;
			var others = ("others" in img) ? img.others : 0;
			var ratio = (self + others > 0 ) ? self / (self + others) : 0;
			var guesses = self + others
			allGuesses += guesses;
			var radius = inter(ratio);
			
			var g = svg.append('g');
// 				.attr("transform", function(d) { return "translate(" + 100*i + ",0)"; })
				
			if (i == 0) g.attr("class", "c"+j+" selected");
			else g.attr("class", "c"+j);
			g.attr("geomID", img.geomID);
			g.attr("index", i);
			var angle = a+ (Math.PI/4)*i;
// 			console.log(angle);
			var cx = mainX + Math.cos(angle) * (mainR + radius);
			var cy = mainY + Math.sin(angle) * (mainR + radius);
			var circle1 = g.append('circle')
		    .attr('cx', cx)
		    .attr('cy', cy)
		    .attr('r', radius)
		    .attr('fill', '#FC0');
			var num = g.append('text')
			.attr("x", cx)
			.attr("y", cy+10)
			.style("text-anchor", "middle")
			.style("font-size", "8")
			.text("# "+guesses);
			var percentage = g.append('text')
			.attr("x", cx)
			.attr("y", cy)
			.style("text-anchor", "middle")
			.style("font-weight", "bold")
			.text(Math.round(ratio*100)+"%")
			g.on("click", function(e){
				d3.select(".svgMarker .selected").classed("selected", false);
				var d = d3.select(this);
				the = cityImages[parseInt(d.attr("index"))].properties;
				document.getElementById('bck').src = "/download/"+the.image.streaminfo.checksum+"."+the.image.format;
				d.classed("selected", true);
// 				console.log("mistakes", the.mistakes);
				if (the.mistakes !== undefined){
					var mainG = d3.select("#mainG");
					mainG.select("circle").style("fill-opacity", 0);
					mainG.selectAll(".info").style("opacity", 0)
					mainG.append("circle").attr("r", 5).attr("cx", 150).attr("cy", 150).style("fill", "#F50").attr("id", "smallmarker");
					console.log("bo")
					setupMistakes();
				}
				
			});
		}
		
		
		mainG.append('text')
		.attr("class", "info")
		.attr("x", 152)
		.attr("y", 158)
		.style("text-anchor", "middle")
		.style("font-weight", "bold")
		.style("font-size", "23")
		.text(Math.round(currentRatio.ratio*100) + "%");
		mainG.append('text')
		.attr("x", 150)
		.attr("y", 133)
		.style("font-size", "15")
		.style("text-anchor", "middle")
		.style("font-weight", "bold")
		.text(currentRatio.city);
		mainG.append('text')
		.attr("class", "info")
		.attr("x", 150)
		.attr("y", 175)
		.style("text-anchor", "middle")
		.text("# guesses: "+ allGuesses);
	}
	
	var setupMistakes = function(){
		var inter = d3.interpolateRound(15,28);
		if (the.others === undefined || the.others == 0) return;
		for(var cityname in the.mistakes){
			var rat = the.mistakes[cityname]/the.others;
			var loc = cities[cityname].location;
			var dx = 100;
			var dy = 100;
			var cx = dx / 2;
			var cy = dy / 2;
			var divIcon = L.divIcon({
				className: 'mistakeMarker',
			    iconSize: L.point(dx, dy),
			    iconAnchor: L.point(cx, cy)
			});
			var cm = L.marker(new L.LatLng(loc.lat, loc.lon), {
		    	icon: divIcon,
		    	'opacity': 0.8
			}).addTo(map);
			console.log("divIcon", divIcon);
			var g = d3.select(cm._icon).append("svg").append("g");
			g.append("circle")
				.attr("r", inter(rat)).attr("cx", cx).attr("cy", cy)
				.style("fill","#FCF").style("opacity",0.8);
			g.append("text")
				.attr("x", cx).attr("y",cy-10)
// 				.style("font-weight", "bold")
				.style("text-anchor", "middle")
				.text(Math.round(rat*100) + "%");
			g.append("text")
			.attr("x", cx).attr("y",cy)
//				.style("font-weight", "bold")
			.style("text-anchor", "middle")
			.style("font-size", "8")
			.text("mistaken with");
			g.append("text")
				.attr("x", cx).attr("y",cy+12)
				.style("font-weight", "bold")
				.style("text-anchor", "middle")
				.text(cityname);
			markers.push(cm);
		}
		var group = new L.featureGroup(markers);		
		map.fitBounds(group.getBounds());
	}
</script>
</body>
</html>
