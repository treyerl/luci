<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>tMap: Select Cities / Edit Country Names</title>
<script type="text/javascript" src="../js/luciConnect.js"></script>
<script type="text/javascript" src="../js/faultylabs.md5.js"></script>
<script type="text/javascript" src='../js/mapbox.js-bower-2.2.1/mapbox.js'></script>
<link href='../js/mapbox.js-bower-2.2.1/mapbox.css' rel='stylesheet' />
<style>
  body { margin:0; padding:0; }
  #map,img { position:absolute; top:0; bottom:0; }
  img { z-index:1; height:100%}
  #map { z-index:3; width:100%;}
  p, h1 {margin: 50px;}
  .leaflet-container {background: transparent;}
</style>
</head>
<body>
<div id='map'></div>
<h1>Select Mooc Cities</h1>
<p>Press ESC if you accidentially moved a city marker to put it back where it was.</p>
<script>
	// Provide your access token
	L.mapbox.accessToken = 'pk.eyJ1IjoidHJleWVybCIsImEiOiJiY2ExZTQ4NTNmYTA2MDRjNTdjZDQ0ZDZkZjU0MzhjNCJ9.nGBCvtCwkSs1NzOE6DQnRA';
	// Create a map in the div #map
	var map = L.mapbox.map('map', 'treyerl.1c1947cc').setView([40, 8.50], 2);
	var ScID = 1;
	
	var success = function(e){
		var msg = e.getMessage();
		if ("result" in msg) console.log("success");
		else console.log(msg);
	}
	
	var editCountry = function(e){
		var newCountry = e.target.textContent;
		var ids = e.target.attributes.images.value.split(",");
		var geometry = {"editCountry":{"format":"GeoJSON", "geometry":{
			"type":"FeatureCollection", "features":[]
		}}}
		var features = geometry.editCountry.geometry.features;
		for (var index in ids){
			var id = parseInt(ids[index]);
			var feature = {"type":"Feature","properties":{"country":newCountry, "id":id}}
			features.push(feature);
		}
		window.newcountry = newCountry;
		console.log("going to save new country "+newCountry);
		lc.sendAndReceive({"action":"update_scenario","ScID":ScID,"geometry":geometry}, [success, function(e){
			window.alert("country successfully changed to "+window.newcountry);
		}]);
	}
	
	var select = function(e){
		var geometry = {"selectedCity":{"format":"GeoJSON", "geometry":{
			"type":"FeatureCollection", "features":[]
		}}}
		var features = geometry.selectedCity.geometry.features;
		var images = this.city.images;
		for (var index in images){
			var id = images[index];
			var feature = {"type":"Feature","properties":{"selected":true, "id":id}}
			features.push(feature);
		}
// 		console.log("select");
// 		console.log(geometry);
		window.currentMarker = this;
		lc.sendAndReceive({"action":"update_scenario","ScID":ScID,"geometry":geometry}, [success, function(e){
			window.currentMarker.setIcon(L.mapbox.marker.icon({
	        	'marker-color': '88f',
	    	}));
			window.currentMarker.setOpacity(0.8);
			window.currentMarker.on('contextmenu', deselect)
			window.currentMarker.off('click', select);
		}]);
	}
	
	var deselect = function(e){
		console.log(e);
		var geometry = {"deselectedCity":{"format":"GeoJSON", "geometry":{
			"type":"FeatureCollection", "features":[]
		}}}
		var features = geometry.deselectedCity.geometry.features;
		var images = this.city.images;
		for (var index in images){
			var id = images[index];
			var feature = {"type":"Feature","properties":{"selected":false, "id":id}}
			features.push(feature);
		}
// 		console.log("deselect");
		console.log(geometry);
		window.currentMarker = this;
		lc.sendAndReceive({"action":"update_scenario","ScID":ScID,"geometry":geometry}, [success, function(e){
			var cm = window.currentMarker;
			cm.setIcon(L.mapbox.marker.icon({
	        	'marker-color': '888',
	    	}));
			cm.setOpacity(0.4);
			cm.on('click', select)
			cm.off('contextmenu', deselect);
			cm.on('contextmenu', function(e){/*JUST DO NOTHING*/});
		}]);
	}
	
	var ondragend = function(e, t){
		var t = t || this;
		var ll = t.getLatLng();
		var geometry = {"relocateCity":{"format":"GeoJSON", "geometry":{
			"type":"FeatureCollection", "features":[]
		}}}
		var features = geometry.relocateCity.geometry.features;
		var images = t.city.images;
		for (var index in images){
			var id = images[index];
			var feature = {"type":"Feature","properties":{"id":id}, "geometry":{"type":"Point","coordinates":[ll.lat, ll.lng]}}
			features.push(feature);
		}
		window.currentMarker = t;
		lc.sendAndReceive({"action":"update_scenario","ScID":ScID,"geometry":geometry}, [success, function(e){
			alert(window.currentMarker.cityname+" has been relocated!");
		}]);
	}
	
	var step3 = function(e){
		var data = e.currentTarget.response;
		delete data.ScID;
		for (var cityname in data){
			var city = data[cityname];
			if ("location" in city){
				var loc = city["location"];
				var imgCount = city.images.length;
				var header = cityname+", <span contenteditable='true' images='"+city.images+"' class='country'>"+city.country+"</span>, "+imgCount+" images</br>"
				// create a mapbox marker
				var cm;
				if (city.selected > 0){
					cm = L.marker(new L.LatLng(loc.lat, loc.lon), {
					    icon: L.mapbox.marker.icon({
					        'marker-color': '88f',
					    	}),
					    'opacity': 0.8,
					    draggable:true}).bindPopup(header).addTo(map);
					cm.off('click');
					cm.on('contextmenu',deselect);
				} else {
					cm = L.marker(new L.LatLng(loc.lat, loc.lon), {
					    icon: L.mapbox.marker.icon({
					        'marker-color': '888',
					    	}),
					    'opacity': 0.4,
					    draggable:true}).bindPopup(header).addTo(map);
					cm.off('click');
					cm.on('click', select);
					cm.on('contextmenu', function(e){/*JUST DO NOTHING*/});
				}
				
				cm.cityname = cityname;
				cm.city = city;
				cm.count = imgCount;
				cm.index = 1;
				cm.header = header;
				cm.on('click', function(e){
					if (!this._popup._isOpen) {
						this.openPopup();
						// editing the country function
						var countryFields = document.getElementsByClassName("country");
						for (var index in countryFields){
							var cf = countryFields[index];
							cf.onblur = editCountry;
						}
						
					} else {
						this.closePopup();
					}
				});
				cm.on('dragend', ondragend);
				cm.on('dragstart', function(e){
					this.oldLatLng = this.getLatLng();
				});
				
			} else {
				console.log(city);
			}
			
		}
	}
	
	var step2 = function(e){
		var msg = e.getMessage();
		var streaminfos = lc.get_streaminfos(msg)
		if (streaminfos !== undefined && streaminfos.length > 0) {
			var streaminfo = streaminfos[0]["streaminfo"]
			var url = "http://"+location.host+"/download/"+streaminfo["checksum"]+"?clientID="+lc.getClientID();
			console.log(url)
			// download the output ourselves:);
			var xhr = new XMLHttpRequest();
			xhr.open('GET', url, true); // parameter 3 = asynchronous
			xhr.responseType = 'json'
			xhr.onload = step3;
			xhr.send();
		} else throw new Error(JSON.stringify(msg));
	}
	
	var step1 = function(e){
		// download the output ourselves:
		lc.downloadAttachments = false;
		lc.sendAndReceive({'action':'run', 'service':{
			'classname':'aggregatemooccities', 'inputs':{'ScID':ScID,'switchLatLon':false}
		}}, [step2]);
	};
	
	var step0 = function(e){
// 		lc.on("onmessage", function(e){console.log("message: ",e.getMessage());});
		lc.authenticate("lukas", "1234", step1)
	};
	
	var lc;
	window.onload = function(){
		document.onkeydown = function(evt) {
		    evt = evt || window.event;
		    if (evt.keyCode == 27) {
		        var cm = window.currentMarker;
		        var ol = cm.oldLatLng;
		        console.log(ol);
		        cm.setLatLng(new L.LatLng(ol.lat, ol.lng));
		        ondragend(null, cm);
		    }
		};
		lc = new LuciClient(location.hostname, 8080, "ws/", step0);
	}
</script>
</body>
</html>