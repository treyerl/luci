<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>tMap: Select Images per City</title>
<script type="text/javascript" src="../js/luciConnect.js"></script>
<script type="text/javascript" src="../js/faultylabs.md5.js"></script>
<script type="text/javascript" src='../js/mapbox.js-bower-2.2.1/mapbox.js'></script>
<link href='../js/mapbox.js-bower-2.2.1/mapbox.css' rel='stylesheet' />
<style>
  body { margin:0; padding:0; }
  #map,img { position:absolute; top:0; bottom:0; }
  img { z-index:1; height:100%}
  #map { z-index:3; width:100%;}
  p, h1 {margin: 50px; position: absolute; top:0; left:0; z-index: 2;}
  #PtotalSelected, #PtotalCities {margin-top: 35px;}
  #PtotalCities {left: 150px;}
  .leaflet-container {background: transparent;}
</style>
</head>
<body>
<div id='map'></div>
<img id='bck' alt="" />
<h1>Select Mooc Images</h1>
<p id = "PtotalSelected">selected images: <span id="totalSelected">0</span></p>
<p id = "PtotalCities">selected cities: <span id="totalCities">0</span></p>
<script>
	// Provide your access token
	L.mapbox.accessToken = 'pk.eyJ1IjoidHJleWVybCIsImEiOiJiY2ExZTQ4NTNmYTA2MDRjNTdjZDQ0ZDZkZjU0MzhjNCJ9.nGBCvtCwkSs1NzOE6DQnRA';
	// Create a map in the div #map
	var map = L.mapbox.map('map', 'treyerl.1c1947cc').setView([40, 8.50], 2);
	var ScID = 1;
	
	var totalSelect = function(number){
		var disp = document.getElementById("totalSelected");
		disp.textContent = number;
	}
	
	var totalSelected = 0;
	
	var success = function(e){
		var msg = e.getMessage();
		if ("result" in msg) console.log("success");
		else console.log(msg);
	}
	
	var select = function(e){
		var geometry = {"selectedCity":{"format":"GeoJSON", "geometry":{
			"type":"FeatureCollection", "features":[]
		}}}
		var features = geometry.selectedCity.geometry.features;
		var images = this.city.images;
		for (var index in images){
			var id = images[index];
			var feature = {"type":"Feature","properties":{"selected":true, "geomID":id}}
			features.push(feature);
		}
// 		console.log("select");
// 		console.log(geometry);
		window.currentMarker = this;
		lc.sendAndReceive({"action":"update_scenario","ScID":ScID,"geometry":geometry}, [success, function(e){
			window.currentMarker.setIcon(L.mapbox.marker.icon({
	        	'marker-color': '88f',
	    	}));
			window.currentMarker.setOpacity(0.8);
			window.currentMarker.on('contextmenu', deselect)
			window.currentMarker.off('click', select);
		}]);
	}
	
	var deselect = function(e){
		var geometry = {"deselectedCity":{"format":"GeoJSON", "geometry":{
			"type":"FeatureCollection", "features":[]
		}}}
		var features = geometry.deselectedCity.geometry.features;
		var images = this.city.images;
		for (var index in images){
			var id = images[index];
			var feature = {"type":"Feature","properties":{"selected":false, "geomID":id}}
			features.push(feature);
		}
// 		console.log("deselect");
// 		console.log(geometry);
		window.currentMarker = this;
		lc.sendAndReceive({"action":"update_scenario","ScID":ScID,"geometry":geometry}, [success, function(e){
			window.currentMarker.setIcon(L.mapbox.marker.icon({
	        	'marker-color': '888',
	    	}));
			window.currentMarker.setOpacity(0.4);
			window.currentMarker.on('click', select)
			window.currentMarker.off('contextmenu', deselect);
			window.currentMarker.on('contextmenu', function(e){/*JUST DO NOTHING*/});
		}]);
	}
	
	var step3 = function(e){
		var data = e.currentTarget.response;
		delete data.ScID;
		var totalCities = 0;
		for (var cityname in data){
			var city = data[cityname];
			if ("location" in city){
				var loc = city["location"];
				var imgCount = city.images.length;
// 				var header = cityname+", "+city.country+", "+imgCount+" images"+", "+city.selected+" selected</br>";
				// create a mapbox marker
				var cs = "selectedImages" in city;
				
				if (cs > 0){
					totalSelected += cs;
					totalCities += 1;
					var color = cs == 1 ? 'f88' : '88f';
					var cm = L.marker(new L.LatLng(loc.lat, loc.lon), {
					    icon: L.icon({
					    	iconUrl: "http://a.tiles.mapbox.com/v4/marker/pin-m+"+color+".png?access_token=pk.eyJ1IjoidHJleWVybCIsImEiOiJiY2ExZTQ4NTNmYTA2MDRjNTdjZDQ0ZDZkZjU0MzhjNCJ9.nGBCvtCwkSs1NzOE6DQnRA",
// 					        'marker-color': color,
					        iconSize:[18+cs, 18*3+(cs*3)],
					    	}),
					    'opacity': 0.8}).bindPopup("").addTo(map);
					cm.cityname = cityname;
					cm.city = city;
					cm.count = imgCount;
					cm.index = 0;
					cm.off('click');
					cm.on('click', function(){
						window.currentMarker = this;
						if (!this._popup._isOpen) this.openPopup();
						next();
					});
					cm.getHeader = function(){
						return this.cityname+", "+this.city.country+", "+this.city.images.length+" images"+", "+this.city.selected+" selected</br>";
					}
				}
			} else {
				console.log(cityname, city);
			}
			
		}
		totalSelect(totalSelected);
		document.getElementById("totalCities").textContent = totalCities;
	}
	
	var next = function(e){
		var cm = window.currentMarker;
		cm.index += 1;
		i = cm.index - 1;
		console.log(i)
		if (cm.index <= cm.count) {
			var imgID = cm.city.images[i];
			var action = {"action":"get_scenario","ScID":ScID,"geomIDs":[imgID],
					"withAttachments":false};
			console.log(JSON.stringify(action));
			lc.sendAndReceive(action, [showImage]);
		} else {
			cm.closePopup();
			cm.index = 0;
		}
	}
	
	var prev = function(e){
		var cm = window.currentMarker;
		cm.index -= 1;
		i = cm.index - 1;
		if (cm.index > 0) {
			var imgID = cm.city.images[i];
			var action = {"action":"get_scenario","ScID":ScID,"geomIDs":[imgID],
					"withAttachments":false};
			console.log(JSON.stringify(action));
			lc.sendAndReceive(action, [showImage]);
		} else {
			cm.closePopup();
// 			this.index = 0;
		}
	}
	
	var showImage = function(e){
		console.log("showImage");
		var msg = e.getMessage();
		console.log(msg);
		var img = msg.result.geometry.GeoJSON.geometry.features[0].properties;
		img.url = "/download/"+img.image.streaminfo.checksum+"."+img.image.format;
		console.log(img.url);
		document.getElementById('bck').src = img.url;
		var taken = "";
		if ("mdDateTaken" in img) taken = "taken on "+img.mdDateTaken;
		img.popupText = "image "+(i+1)+"' by '"+img.user+"'"+taken;
		window.currentMarker.img = img;
		if ( img.selected) setupSelected();
		if (!img.selected) setupUnselected();
	};
	
	var selectedText = function(id){return "</br><b>Selected</b>, <a id='unselect' href='#'>Deselect</a>"+prevNext;}
	var unselectedText=function(id){return "</br><a id='select' href='#'>Select</a>, <b>Deselected</>"+prevNext;}
	
	var updateGeometry = function(id, selected){return {"deSelectImage":{"format":"GeoJSON", "geometry":{
		"type":"Feature", "properties":{"id":id, "selected": selected}
	}}}
	}
	
	var prevNext = "</br><a href='#' id='prev'>Previous</a> < - - - > <a href='#' id='next'>Next</a>";
	
	var setupSelected = function(){
		var cm = window.currentMarker;
		cm.setPopupContent(cm.getHeader()+cm.img.popupText + selectedText(cm.img.id));
		document.getElementById("unselect").onclick = function(e){
			var id = window.currentMarker.img.id;
			lc.sendAndReceive({"action":"update_scenario","ScID":ScID,"geometry":updateGeometry(id,false)}, [success, function(e){
				window.currentMarker.city.selected -= 1;
				totalSelected -= 1;
				totalSelect(totalSelected);
				setupUnselected();
			}]);
		}
		document.getElementById("prev").onclick = prev;
		document.getElementById("next").onclick = next;
	}
	
	var setupUnselected = function(){
		var cm = window.currentMarker;
		cm.setPopupContent(cm.getHeader()+cm.img.popupText + unselectedText(cm.img.id));
		document.getElementById("select").onclick = function(e){
			var id = window.currentMarker.img.id;
			lc.sendAndReceive({"action":"update_scenario","ScID":ScID,"geometry":updateGeometry(id,true)}, [success, function(e){
				window.currentMarker.city.selected += 1;
				totalSelected += 1;
				totalSelect(totalSelected);
				setupSelected();
			}]);
		}
		document.getElementById("prev").onclick = prev;
		document.getElementById("next").onclick = next;
	}
	
	
	var step2 = function(e){
		var msg = e.getMessage();
		var streaminfos = lc.get_streaminfos(msg);
		if (streaminfos !== undefined && streaminfos.length > 0){
			var streaminfo = streaminfos[0]["streaminfo"]
			var url = "http://"+location.host+"/download/"+streaminfo["checksum"]+"?clientID="+lc.getClientID();
			console.log(url)
			// download the output ourselves:);
			var xhr = new XMLHttpRequest();
			xhr.open('GET', url, true); // parameter 3 = asynchronous
			xhr.responseType = 'json'
			xhr.onload = step3;
			xhr.send();
		} else {
			throw new Error("Error: "+JSON.stringify(msg));
		}
		
	}
	
	var step1 = function(e){
		// download the output ourselves:
		lc.downloadAttachments = false;
		var action = {'action':'run', 'service':{
			'classname':'aggregatemooccities', 'inputs':{'ScID':ScID,'switchLatLon':false}
		}};
		console.log(action);
		lc.sendAndReceive(action, [step2]);
	};
	
	var step0 = function(e){
// 		lc.on("onmessage", function(e){console.log("message: ",e.getMessage());});
		lc.authenticate("lukas", "1234", step1)
	};
	
	var lc;
	window.onload = function(){
		lc = new LuciClient(location.hostname, 8080, "ws/", step0);
	}
	
</script>
</body>
</html>